#*******************************************************************************
#
#                       Copyright 1991, FIT Systems Inc.
#
#   All rights reserved.  The following source code, or any part there-of,
#   may not be duplicated, modified, or resold without the express written
#   consent of FIT Systems Inc.
#
#   Fit Systems Inc. makes no claims as to the correctness or applicability of
#   this source code.  The source code is supplied as is.  Fit Systems Inc.
#   is not responsible for damages, consequential or otherwise, caused by the
#   use of this source code.
#
#*******************************************************************************
#
#*******************************************************************************
#Module Name : spsim
#
#Function : Provides simulated data for demoing/testing spot.
#
#Author : A. Fregly
#
#Description: This function performs the function of moving files from
#	a source directory to a destination directory one at a time, every
#	n seconds. This is usual for putting data into a spot initial input
#	directory to simulate the placement of files there by a stream feeder,
#	such as the spot asynch daemon.
#
#Syntax:
#
#	spsim src_path dest_path interval 
#
#Parameters:
#
#	src_path	Source path from which files will be moved.
#	dest_path	Destination path into which file will be moved.
#	interval	Delay between file moves in seconds.
#
#Example:
#
#	spsim /usr/spotdata/demodata /usr/spotdata/apin 10
#
#		Files will be moved from /usr/spotdata/demodata to 
#		/usr/spotdata/apin at 10 seconds intervals.
#
#Notes: 
#
#Change log : 
#000	23-SEP-91  AMF	Created.
#*******************************************************************************

if [ $# -ne 3 ]
then
  echo "Usage: $0 in_path dest_path interval"
  exit 0
fi

cd $1
if [ $? -ne 0 ]
then
  for file in *
  do
    mv $file $2
    sleep $3
  done 
else
  echo "Unable to locate directory $1"
fi
