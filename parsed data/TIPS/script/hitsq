
#*******************************************************************************
#
#                       Copyright 1991, FIT Systems Inc.
#
#   All rights reserved.  The following source code, or any part there-of,
#   may not be duplicated, modified, or resold without the express written
#   consent of FIT Systems Inc.
#
#   Fit Systems Inc. makes no claims as to the correctness or applicability of
#   this source code.  The source code is supplied as is.  Fit Systems Inc.
#   is not responsible for damages, consequential or otherwise, caused by the
#   use of this source code.
#
#*******************************************************************************
#
#*******************************************************************************
#Module Name : hitsq
#
#Function : Creates new copy of result file with deleted records removed.
#
#Author : A. Fregly
#
#Description:
#
#       This script is meant for use by the super-user as an aid in
#       recovering space used for deleted records in TIPS result files. Result
#       files for ongoing searches against streams are the most likely
#       candidates for use with this script. The example below shows how
#       hitsqueez may be used in conjunction with  the drctio command to
#       squeeze all of the result files for queries running against a
#       particular stream.
#
#       This command is normally used in periodic maintenance procedures
#       executed by the super-user either interactively or in batch. Note that
#       result files for active stream searches should not be squeezed,
#       so the stream should be shut down during periods of squeezing.
#
#        This utility creates new compressed versions of the files, attempting
#       to set the owner, group, and protections for the new version the same
#       as for the old version. These functions are normally restricted to the
#       super-user.
#
#Syntax:
#
#       hitsq searchname {searchname...}
#
#Parameters:
#
#       searchname
#
#               Absolute pathname for search. This must be an absolute path
#               name for the result file which is to be squeezed. The
#               result file to be squeezed is determined by setting the file
#               extension on searchname to ".hit". If an extension has been
#               specified on searchname, it is replaced.
#
#Example:
#
#       # Shut down UPI stream
#       /usr/spotsys/script/spotadm shutdown profiling upi
#       # Remove results for deleted documents for the searches against UPI
#       /usr/spotsys/bin/hitclean `/usr/spotsys/bin/drctio -h \
#	/usr/spotsys/streams/upi.qlb`
#       # Squeeze the result files for the searches against UPI
#       /usr/spotsys/script/hitsq `/usr/spotsys/bin/drctio -h \
#	/usr/spotsys/streams/upi.qlb`
#       # Restart the UPI stream
#       /usr/spotsys/script/spotadm startup profiling upi
#
#Notes:
#
#Change log :
#000 23-NOV-91  AMF     Created.
#*******************************************************************************

# Set temp file name as <month name><day of month><hh><mm><ss>
tempname=`date | cut -c5-6,9-10,12-13,15-16,18-19`
tempname="/usr/tmp/$tempname"

# Process each command line arguement
for hitnames
do
# Set result file name extension to ".hit"
  hitfilename=`echo $hitnames | cut -d. -f1`

# Get current stats for result file
  stats=`/usr/spotsys/bin/drctio -i $hitfilename.hit | cut -c12-`
  retstat=$?
  if [ $retstat -eq 0 ]
  then
    if [ -f "$tempname.hit" ]
    then
#     remove leftover version of temp file
      rm $tempname.hit 2>/dev/null
      rm $tempname.drc 2>/dev/null
      /usr/spotsys/bin/drctio -c $tempname.hit
    fi
#   Create a compressed copy of the result file into the temp file
    /usr/spotsys/bin/drctio -h $hitfilename.hit | \
    /usr/spotsys/bin/hitw $tempname.hit >/dev/null
    retstat=$?

#   Get the stats for the temp file
    if [ $? -eq 0 ]
    then
      newstats=`/usr/spotsys/bin/drctio -i $tempname.hit | cut -c12-`
    else
      newstats="$stats"
    fi

    if [ "$stats" != "$newstats" ]
    then
#     stats are different, replace the original result file with the temp file

#     Get protection mask for the original
      prot=`/usr/spotsys/bin/getmask $hitfilename.hit`

#     overwrite the original with the temp file
      owner=`ls -l $hitfilename.hit | cut -c16-23`
      group=`ls -l $hitfilename.hit | cut -c25-32`
      newowner=`ls -l $tempname.hit | cut -c16-23`
      newgroup=`ls -l $tempname.hit | cut -c25-32`

      mv $tempname.hit $hitfilename.hit
      mv $tempname.drc $hitfilename.drc

#     set the owner and protections for the result file
      if [ "$owner" != "$newowner" ]
      then
        chown $owner $hitfilename.hit $hitfilename.drc
      fi

      if [ "$group" != "$newgroup" ]
      then
        chgrp $group $hitfilename.hit $hitfilename.drc
      fi

      chmod $prot $hitfilename.*

      echo "$hitfilename: $stats to $newstats"
    else
      echo "$hitfilename: does not need squeezing"
    fi
  else
    echo "Unable to access $hitfilename.hit"
  fi
done

if [ -f "$tempname.hit" ]
then
  rm $tempname.hit 2>/dev/null
  rm $tempname.drc 2>/dev/null
fi

#*******************************************************************************
#
#                       Copyright 1991, FIT Systems Inc.
#
#   All rights reserved.  The following source code, or any part there-of,
#   may not be duplicated, modified, or resold without the express written
#   consent of FIT Systems Inc.
#
#   Fit Systems Inc. makes no claims as to the correctness or applicability of
#   this source code.  The source code is supplied as is.  Fit Systems Inc.
#   is not responsible for damages, consequential or otherwise, caused by the
#   use of this source code.
#
#*******************************************************************************
#
#*******************************************************************************
#Module Name : hitsq
#
#Function : Creates new copy of result file with deleted records removed.
#
#Author : A. Fregly
#
#Description:
#
#       This script is meant for use by the super-user as an aid in
#       recovering space used for deleted records in TIPS result files. Result
#       files for ongoing searches against streams are the most likely
#       candidates for use with this script. The example below shows how
#       hitsqueez may be used in conjunction with  the drctio command to
#       squeeze all of the result files for queries running against a
#       particular stream.
#
#       This command is normally used in periodic maintenance procedures
#       executed by the super-user either interactively or in batch. Note that
#       result files for active stream searches should not be squeezed,
#       so the stream should be shut down during periods of squeezing.
#
#        This utility creates new compressed versions of the files, attempting
#       to set the owner, group, and protections for the new version the same
#       as for the old version. These functions are normally restricted to the
#       super-user.
#
#Syntax:
#
#       hitsq searchname {searchname...}
#
#Parameters:
#
#       searchname
#
#               Absolute pathname for search. This must be an absolute path
#               name for the result file which is to be squeezed. The
#               result file to be squeezed is determined by setting the file
#               extension on searchname to ".hit". If an extension has been
#               specified on searchname, it is replaced.
#
#Example:
#
#       # Shut down UPI stream
#       /usr/spotsys/script/spotadm shutdown profiling upi
#       # Remove results for deleted documents for the searches against UPI
#       /usr/spotsys/bin/hitclean `/usr/spotsys/bin/drctio -h \
#	/usr/spotsys/streams/upi.qlb`
#       # Squeeze the result files for the searches against UPI
#       /usr/spotsys/script/hitsq `/usr/spotsys/bin/drctio -h \
#	/usr/spotsys/streams/upi.qlb`
#       # Restart the UPI stream
#       /usr/spotsys/script/spotadm startup profiling upi
#
#Notes:
#
#Change log :
#000 23-NOV-91  AMF     Created.
#*******************************************************************************

# Set temp file name as <month name><day of month><hh><mm><ss>
tempname=`date | cut -c5-6,9-10,12-13,15-16,18-19`
tempname="/usr/tmp/$tempname"

# Process each command line arguement
for hitnames
do
# Set result file name extension to ".hit"
  hitfilename=`echo $hitnames | cut -d. -f1`

# Get current stats for result file
  stats=`/usr/spotsys/bin/drctio -i $hitfilename.hit | cut -c12-`
  retstat=$?
  if [ $retstat -eq 0 ]
  then
    if [ -f "$tempname.hit" ]
    then
#     remove leftover version of temp file
      rm $tempname.hit 2>/dev/null
      rm $tempname.drc 2>/dev/null
      /usr/spotsys/bin/drctio -c $tempname.hit
    fi
#   Create a compressed copy of the result file into the temp file
    /usr/spotsys/bin/drctio -h $hitfilename.hit | \
    /usr/spotsys/bin/hitw $tempname.hit >/dev/null
    retstat=$?

#   Get the stats for the temp file
    if [ $? -eq 0 ]
    then
      newstats=`/usr/spotsys/bin/drctio -i $tempname.hit | cut -c12-`
    else
      newstats="$stats"
    fi

    if [ "$stats" != "$newstats" ]
    then
#     stats are different, replace the original result file with the temp file

#     Get protection mask for the original
      prot=`/usr/spotsys/bin/getmask $hitfilename.hit`

#     overwrite the original with the temp file
      owner=`ls -l $hitfilename.hit | cut -c16-23`
      group=`ls -l $hitfilename.hit | cut -c25-32`
      newowner=`ls -l $tempname.hit | cut -c16-23`
      newgroup=`ls -l $tempname.hit | cut -c25-32`

      mv $tempname.hit $hitfilename.hit
      mv $tempname.drc $hitfilename.drc

#     set the owner and protections for the result file
      if [ "$owner" != "$newowner" ]
      then
        chown $owner $hitfilename.hit $hitfilename.drc
      fi

      if [ "$group" != "$newgroup" ]
      then
        chgrp $group $hitfilename.hit $hitfilename.drc
      fi

      chmod $prot $hitfilename.*

      echo "$hitfilename: $stats to $newstats"
    else
      echo "$hitfilename: does not need squeezing"
    fi
  else
    echo "Unable to access $hitfilename.hit"
  fi
done

if [ -f "$tempname.hit" ]
then
  rm $tempname.hit 2>/dev/null
  rm $tempname.drc 2>/dev/null
fi
