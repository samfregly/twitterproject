/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_getqrylib					qcgetqry.c

Function : Gets next query name from object query library.

Author : A. Fregly

Abstract : This function is used to sequentially read the query names in an
	object query library.

Calling Sequence : long entrynum = qc_getqrylib(struct drct_s_file *lib,
  char qryname[FIT_FULLFSPECLEN+1]);

  lib		Query object library handle returned by qc_openlib.
  qryname	Returned query name.
  entrynum	Returned entry number for query, or -1L if an EOF is found
		or an error occurs.

Notes:

Change log :
000	30-JUL-91  AMF	Created.
******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>
#include <qc.h>

#if FIT_ANSI
long qc_getqrylib(struct drct_s_file *lib, char *qryname)
#else
long qc_getqrylib(lib, qryname)
  struct drct_s_file *lib;
  char *qryname;
#endif
{

  long reclen;
  int namelen;

  *qryname = 0;
  if (!(namelen = drct_rdseq(lib, qryname, FIT_FULLFSPECLEN, &reclen)))
    return -1L;
  drct_cancelio(lib);
  qryname[namelen] = '\n';
  namelen = strchr(qryname,'\n') - qryname;
  qryname[namelen] = 0;
  return lib->currec;
}

