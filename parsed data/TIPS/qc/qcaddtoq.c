/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_addtoqry                                       addtoqry.c

Function : Adds next element from source query being compile to object query.

Author : A. Fregly

Abstract : Token is parsed and internally maintained object query is updated.

Calling Sequence : stat = qc_addtoqry(int tokentype,char *nexttoken, int len)

  tokentype     Token type, possible values are defined in tx.h
  nexttoken     Character string containing text making up token
  len           Length of nexttoken

Notes :

Change log :
000     21-MAR-90  AMF  Created
001     25-MAR-93  AMF  Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
int qc_addtoqry(struct qc_s_token *token)
#else
int qc_addtoqry(token)
  struct qc_s_token *token;
#endif
{

  struct qc_s_logicentry *logicptr;
  struct qc_s_termentry *termptr;
  int stat,nderivations,vldcoffset,i,j,substitute,step,vldc,operators;
  char *cptr, *newterm;
  /*                       n/a  (    ^  ANY  ~ PROX  &   |   ,   ) BOQ EOQ */
  static int inprec[12] =  {0, -1,  -2, -2, -2, -4, -5, -6, -7, -8, 0, -9};
  static int outprec[12] = {0, -10, -3, -3, -3, -4, -5, -6, -7,  1,-11, 1};
  struct qc_s_token *temptoken;
  static char comma[2] = {0,0};
  static char rparen[2] = {0,0};
  static char lparen[2] = {0,0};

  if (!qc_firsttoken) {
    /* initialize logic stack with beginning of query marker */
    qc_toplogic = (struct qc_s_logicentry *) malloc(sizeof(*qc_toplogic));
    if (!qc_toplogic) goto ALLOCERR;
    qc_toplogic->token = qc_newtoken(QC_BOQ, 0, 0, NULL, 0, 0);
    if (!qc_toplogic->token) goto ALLOCERR;
    qc_firsttoken = qc_toplogic->token;
    qc_lasttoken = qc_firsttoken;
    qc_toplogic->prev = (struct qc_s_logicentry *) NULL;
  }

  if (token->type == QC_TERM) {
    if (qc_isvldc(token,&nderivations,&vldcoffset)) {
      /* Add threshhold list representing all derivations of the term */

      /* Start by adding "ANY" */
      temptoken = qc_newtoken(QC_THRESHHOLD,0,1L,qc_reserved[QC_OFF_ANY],
	strlen(qc_reserved[QC_OFF_ANY]),token->offset);
      if (!temptoken) goto ALLOCERR;
      if (!(stat = qc_addtoqry(temptoken))) return(stat);

      /* Add left paren */
      lparen[0] = qc_delims[QC_OFF_OPENPAREN];
      temptoken = qc_newtoken(QC_LPAREN,0,0L,lparen,1,token->offset);
      if (!temptoken) goto ALLOCERR;
      if (!(stat = qc_addtoqry(temptoken))) return(stat);
      if (!stat) return(stat);

      /* Add all derivations */

      vldc = token->value[vldcoffset];
      if (vldc == qc_delims[QC_OFF_AVLWC] || vldc == qc_delims[QC_OFF_AFLDC])
	substitute = qc_delims[QC_OFF_SCAWC];
      else
	substitute = qc_delims[QC_OFF_SCNWC];

      if (vldc == qc_delims[QC_OFF_AVLWC] || vldc == qc_delims[QC_OFF_NVLWC])
	step = 1;
      else
	step = nderivations - 1;

      for (i=nderivations-1; i >= 0; i -= step) {
	newterm = (char *) malloc(token->len + 1 - nderivations + 1 + i);
	if (newterm == NULL) goto ALLOCERR;
	if (vldcoffset)
	  strncpy(newterm,token->value,vldcoffset);
	for (j=vldcoffset; j < vldcoffset+i; ++j)
	  newterm[j] = substitute;
	strcpy(newterm+vldcoffset+i,token->value+vldcoffset+nderivations-1);

	temptoken = qc_newtoken(QC_TERM,0,0L,newterm,strlen(newterm),
	  token->offset);
	if (!temptoken) goto ALLOCERR;
	if (!(stat = qc_addtoqry(temptoken))) return(stat);
	free(newterm);

	if (i) {
	  comma[0] = qc_delims[QC_OFF_COMMA];
	  temptoken = qc_newtoken(QC_COMMA,0,0L,comma,1,token->offset);
	  if (!temptoken) goto ALLOCERR;
	  if (!(stat = qc_addtoqry(temptoken))) return(stat);
	}
      }

      rparen[0] = qc_delims[QC_OFF_CLOSEPAREN];
      temptoken = qc_newtoken(QC_RPAREN,0,0L,rparen,1,token->offset);
      if (!(stat = qc_addtoqry(temptoken))) return(stat);
      free(token->value);
      free(token);
      return(1);
    }
  }

  qc_lasttoken->next = token;
  qc_lasttoken = token;

  stat = 1;

  if (token->type == QC_TERM) {
    termptr = (struct qc_s_termentry *) malloc(sizeof(*qc_lastterm));
    if (!termptr) goto ALLOCERR;

    for (cptr = token->value; *cptr; ++cptr)
      if (*cptr == qc_delims[QC_OFF_QUOTE]) {
	if (*(cptr+1)) cptr += 1;
      }
      else if (*cptr < ' ')
	*cptr = ' ';

    termptr->token = token;
    termptr->next = (struct qc_s_termentry *) NULL;

    /* Add token to end of list of query terms */
    if (qc_firstterm) {
      /* link to old last element */
      qc_lastterm->next = termptr;
      qc_lastterm = termptr;

      /* Increment term index */
      qc_nterms += 1;
    }
    else {
      /* Initialize list, get first node and set first pointer */
      qc_firstterm = termptr;

      /* Set last term pointer same as first term pointer */
      qc_lastterm = qc_firstterm;

      /* Initialize term index */
      qc_nterms = 1;
    }

    /* Bump the cumulative term length counter */
    qc_textsize += token->len;

    /* Set token value to number of terms -1, this forcing it to be added
       immediately to end of object query as it will have a higher value than
       any of the operators on the work stack */
    token->type = qc_nterms - 1;
  }

  if (token->type == QC_FIELD) {
    qc_nfields += 1;
    qc_fieldsize += token->len;
  }

  /* Pop all equal or higher precedence operators than incoming operator off
     stack and add to end of logic equation */
  if (token->type < 0) {
    for (operators=0; qc_toplogic &&
	outprec[-qc_toplogic->token->type] >= inprec[-token->type];) {

      operators |= (1 << -qc_toplogic->token->type);
      logicptr = qc_toplogic;
      qc_toplogic = qc_toplogic->prev;

      stat = qc_addobj(logicptr->token);

      if (stat)
	free (logicptr);
      else
	return (stat);
    }

    if (token->type == QC_RPAREN || token->type == QC_EOQ) {
      if (qc_toplogic && ((token->type == QC_RPAREN &&
	  qc_toplogic->token->type == QC_LPAREN) ||
	  (token->type == QC_EOQ && qc_toplogic->token->type == QC_BOQ))) {
	logicptr = qc_toplogic;
	qc_toplogic = qc_toplogic->prev;
	free(logicptr);

	if (operators & (1 << -(QC_COMMA))) {
	  /* Comma list popped, see if threshhold specifier needs to
	     be put into object query */
	  for (logicptr=qc_toplogic; logicptr &&
	      (logicptr->token->type == QC_NOT ||
	       logicptr->token->type == QC_FIELD);
	      logicptr=logicptr->prev)
	    ;
	  if (!logicptr || logicptr->token->type != QC_THRESHHOLD) {
	    temptoken = qc_newtoken(QC_THRESHHOLD,0,1L,
	      qc_reserved[QC_OFF_ANY],strlen(qc_reserved[QC_OFF_ANY]),
		token->offset);
	    if (!temptoken) goto ALLOCERR;

	    stat = qc_addobj(temptoken);
	    if (!stat) return (stat);
	  }
	}
      }
      else
	qc_adderror(FIT_ERR_PAREN,token->offset);
    }

    else {
      /* Push onto logic stack */
      logicptr = (struct qc_s_logicentry *) malloc(sizeof(*qc_toplogic));
      if (!logicptr) goto ALLOCERR;
      logicptr->prev = qc_toplogic;
      logicptr->token = token;
      qc_toplogic = logicptr;
    }
  }

  else {
    /* Add operand to end of object query */
    stat = qc_addobj(token);
  }

  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  stat = 0;

DONE:
  return (stat);
}
