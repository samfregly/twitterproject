/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_nextlib						qcnextlib.c

Function : Reads next entry from object library.

Author : A. Fregly

Abstract : This function is used to sequentially read the next entry from
	a query object library.

Calling Sequence : long entrynum = qc_nextlib(struct drct_s_file *lib,
  char *qryname, struct qc_s_cmpqry *cmpqry)

  lib		Object library handle as returned by qc_openlib.
  qryname	Returned query name.
  cmpqry	Returned compiled query. If *cmpqry is not NULL at entry, it
		is assumed to point at a compiled query, and said compiled
		query is deallocated.
  entrynum	Returned entry number of query, or -1 if EOF found or error.

Notes:

Change log :
000	30-JUL-91  AMF	Created.
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>
#include <qc.h>

#if FIT_TURBOC
#define PACKEDOBJQRYMAX 2048
#else
#define PACKEDOBJQRYMAX 16384
#endif

#if FIT_ANSI
long qc_nextlib(struct drct_s_file *lib, char *qryname,
  struct qc_s_cmpqry *(*cmpqry))
#else
long qc_nextlib(lib, qryname, cmpqry)
  struct drct_s_file *lib;
  char *qryname;
  struct qc_s_cmpqry *(*cmpqry);
#endif
{

#define BUFMAX (PACKEDOBJQRYMAX + FIT_FULLFSPECLEN + 2)

  char *packedobjqry, *packedobjptr;
  long reclen;
  int namelen;
  int stat = 0;

  if (*cmpqry) qc_dealloc(cmpqry);

  packedobjqry = (char *) malloc(BUFMAX+1);
  if (!packedobjqry) goto DONE;

  if (!drct_rdseq(lib, packedobjqry, BUFMAX, &reclen)) goto DONE;

  if (reclen > BUFMAX) {
    drct_cancelio(lib);
    goto DONE;
  }
  drct_unlock(lib);

  packedobjptr = strchr(packedobjqry,'\n');
  if (!packedobjptr) goto DONE;

  namelen = packedobjptr - packedobjqry;
  if (namelen > FIT_FULLFSPECLEN) namelen = FIT_FULLFSPECLEN;
  strncpy(qryname, packedobjqry, namelen);
  qryname[namelen] = 0;

  ++packedobjptr;
  reclen -= (packedobjptr - packedobjqry);

  qc_unpackob(packedobjptr, (int) reclen, cmpqry);

  stat = (cmpqry && *cmpqry);

DONE:
  if (packedobjqry) {
    free(packedobjqry);
    packedobjqry = (char *) NULL;
  }

  return (stat) ? lib->currec : -1L;
}
