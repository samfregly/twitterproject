/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_dealloc					dealloc.c

Function : Deallocates compiled query.

Author : A. Fregly

Abstract : This is the public version of the compiled query deallocation
	function.

Calling Sequence : qc_dealloc(struct qc_s_cmpqry *(*cmpqry));


Notes: 

Change log : 
000	15-JUL-91  AMF	Created.
001	14-DEC-92  AMF	Compile under COHERENT.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
void qc_dealloc(struct qc_s_cmpqry *(*cmpqry))
#else
void qc_dealloc(cmpqry)
  struct qc_s_cmpqry *(*cmpqry);
#endif
{
  qc_dealobj(cmpqry);
}
