/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_putlib						putlib.c

Function : Put opbject query into library.

Author : A. Fregly

Abstract : This functions is used to insert or replace an entry in a query
	object library.

Calling Sequence : long entrynum = qc_putlib(qc_libhandle *lib,
	char *qryname, struct qc_s_cmpqy *cmpqry);

  lib  		Handle for library returned by qc_openlib.
  queryname	Query file name. The query file name will be expanded to
		a full path specification and inserted into the library as
		a key by which the query may be referenced.  If an entry
		with the same name is already in the library, it will be
		replaced.
		be used with "spotter" for profiling, queryname should be
		a path.
  cmpqry	Compiled query to be put into the library.
  entrynum	Returned entry number assigned to query, or -1 if an error
		occurrs.

Notes:

Change log :
000	30-JUL-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>
#include <qc.h>

#if FIT_TURBOC
#define PACKEDOBJMAX 2048
#else
#define PACKEDOBJMAX 16384
#endif

#if FIT_ANSI
long qc_putlib(struct drct_s_file *lib, char *queryname,
  struct qc_s_cmpqry *cmpqry)
#else
long qc_putlib(lib, queryname, cmpqry)
  struct drct_s_file *lib;
  char *queryname;
  struct qc_s_cmpqry *cmpqry;
#endif
{
  long entrynum, buflen_l;
  char *objbuf, expqryname[FIT_FULLFSPECLEN+2];
  int buflen, explen, stat;

  stat = 0;
  objbuf = (char *) malloc(PACKEDOBJMAX);
  if (objbuf == NULL) goto DONE;

  if (!qc_packobj(cmpqry, objbuf, PACKEDOBJMAX, &buflen)) goto DONE;

  fit_expfspec(queryname, "", "", expqryname);
  explen = strlen(expqryname);
  expqryname[explen++] = '\n';
  expqryname[explen] = 0;

  buflen_l = explen + buflen;

  entrynum = qc_findlib(lib, expqryname);
  drct_unlock(lib);
  if (entrynum < 0) {
    if (!drct_overwrite(lib, expqryname, explen, buflen_l)) goto DONE;
    if (!drct_overwrite(lib, objbuf, buflen, buflen_l)) goto DONE;
    entrynum = lib->currec;
  }
  else {
    if (!drct_setpos(lib, entrynum)) goto DONE;
    if (!drct_rewrite(lib, expqryname, explen, buflen_l)) goto DONE;
    if (!drct_rewrite(lib, objbuf, buflen, buflen_l)) goto DONE;
  }

  stat = 1;

DONE:
  if (objbuf != NULL) free(objbuf);
  drct_unlock(lib);
  return (stat) ? entrynum : -1L;
}
