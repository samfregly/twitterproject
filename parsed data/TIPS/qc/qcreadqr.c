/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_readqry                                        readqry.c

Function : Reads query file.

Author : A. Fregly

Abstract : This routine is used to read a query file.  It returns the
 source text for the query and a pointer to an object query structure
 if the query file contains an object query.

Calling Sequence : stat = qc_readqry(char *fname, char *(*qrytext),
  struct cmpqry *(*cmpqry));

  fname         File name for query file.
  qrytext       Source text for query.  This is allocated by this routine.
  cmpqry        Pointer at object query created when query was compiled.
		cmpqry may be NULL.  This is allocated by this routine.

Notes :

Change log :
000  05-MAY-90  AMF     Created
001  25-MAR-93  AMF     Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>
#if FIT_DOS
#define READ "r"
#else
#define READ "r"
#endif


#if FIT_ANSI
int qc_readqry(char *fname, char *(*qrytext), struct qc_s_cmpqry *(*cmpqry))
#else
int qc_readqry(fname, qrytext, cmpqry)
  char *fname, *(*qrytext);
  struct qc_s_cmpqry *(*cmpqry);
#endif
{

  FILE *handle;
  int stat,i,j,vlen;
  char linein[81];

  fit_status = FIT_STAT_NORMAL;

  stat = 0;
  handle = fopen(fname, READ);
  if (!handle) {
    fit_status = FIT_ERR_OPEN;
    goto DONE;
  }

  if (*qrytext) free(*qrytext);

  /* skip over comment line */
  fgets(linein, (size_t) 81, handle);

  /* read in query text */
  fscanf(handle,"%d", &vlen);
  fgetc(handle);
  *qrytext = (char *) malloc((size_t) (vlen+1));
  if (!*qrytext) goto ALLOCERR;
  fread(*qrytext, (size_t) 1, (size_t) vlen, handle);
  *(*qrytext+vlen) = 0;
  fgetc(handle);

  stat = 0;
  if (feof(handle)) {
    fit_status = FIT_ERR_READ;
    goto DONE;
  }

  stat = 0;
  if (*cmpqry) qc_dealobj(cmpqry);

  *cmpqry = (struct qc_s_cmpqry *) calloc (1, sizeof(*(*cmpqry)));
  if (!*cmpqry) goto ALLOCERR;

  (*cmpqry)->qrytext = (char *) NULL;
  (*cmpqry)->fields = (char *) NULL;
  (*cmpqry)->qrylogic = (int *) NULL;
  (*cmpqry)->termlen = (int *) NULL;
  (*cmpqry)->fieldlen = (int *) NULL;
  (*cmpqry)->fieldmap = (long *) NULL;
  (*cmpqry)->offsets = (int *) NULL;
  (*cmpqry)->fldoffsets = (int *) NULL;
  (*cmpqry)->nproximity = 0;
  (*cmpqry)->proximity = (struct qc_s_proximity *(*)) NULL;

  /* skip over comment lines */
  fgets(linein, (size_t) 81, handle);
  fgets(linein, (size_t) 81, handle);

  fscanf(handle,"%d %d %d %d %d %*[ \n\r]", &(*cmpqry)->nterms, 
    &(*cmpqry)->terms_L, &(*cmpqry)->nlogic, &(*cmpqry)->nfields,
    &(*cmpqry)->fields_L);
  
  fgets(linein, (size_t) 81, handle);
  fscanf(handle,"%d %d %*[ \n\r]",&(*cmpqry)->noperands,
      &(*cmpqry)->nproximity);

  fgets(linein, (size_t) 81, handle);
  for (i=0; i < QC_MAXDELIM; ++i)
    (*cmpqry)->delims[i] = fgetc(handle);
  fgetc(handle);

  fgets(linein, (size_t) 81, handle);
  for (i=0; i < QC_MAXRESERVED; ++i) {
    fgets(linein, (size_t) 81, handle);
    vlen = strlen(linein);
    linein[vlen-1] = 0;
    (*cmpqry)->reserved[i] = (char *) malloc((size_t) vlen);
    if (!(*cmpqry)->reserved[i]) goto ALLOCERR;
    strcpy((*cmpqry)->reserved[i],linein);
  }

  /* Initialize compiled query structure */
  if ((*cmpqry)->nterms > 0) {
    (*cmpqry)->qrytext = (char *) malloc((size_t) (*cmpqry)->terms_L);
    if (!(*cmpqry)->qrytext) goto ALLOCERR;

    (*cmpqry)->termlen = (int *) calloc(sizeof(*(*cmpqry)->termlen),
      (size_t) (*cmpqry)->nterms);
    if (!(*cmpqry)->termlen) goto ALLOCERR;

    (*cmpqry)->fieldmap = (long *) calloc(sizeof(*(*cmpqry)->fieldmap),
      (size_t) (*cmpqry)->nterms);
    if (!(*cmpqry)->fieldmap) goto ALLOCERR;

    (*cmpqry)->offsets = (int *) calloc(sizeof(*(*cmpqry)->offsets),
      (size_t) (*cmpqry)->nterms);
    if (!(*cmpqry)->offsets) goto ALLOCERR;
  }

  if ((*cmpqry)->nlogic) {
    (*cmpqry)->qrylogic = 
      (int *) calloc((size_t) (*cmpqry)->nlogic,
      sizeof(*(*cmpqry)->qrylogic));
    if (!(*cmpqry)->qrylogic) goto ALLOCERR;
  }

  if ((*cmpqry)->nfields > 0) {
    (*cmpqry)->fields = (char *) malloc((size_t) (*cmpqry)->fields_L);
    if (!(*cmpqry)->fields) goto ALLOCERR;

    (*cmpqry)->fieldlen = (int *) calloc(sizeof(*(*cmpqry)->fieldlen),
      (size_t) (*cmpqry)->nfields);
    if (!(*cmpqry)->fieldlen) goto ALLOCERR;

    (*cmpqry)->fldoffsets = (int *) calloc(sizeof(*(*cmpqry)->fldoffsets),
      (size_t) (*cmpqry)->nfields);
    if (!(*cmpqry)->fldoffsets) goto ALLOCERR;
  }

  if ((*cmpqry)->nproximity > 0) {
    (*cmpqry)->proximity =
      (struct qc_s_proximity *(*)) calloc((size_t) (*cmpqry)->nproximity,
      sizeof(*((*cmpqry)->proximity)));
    if (!(*cmpqry)->proximity) goto ALLOCERR;
    for (i=0; i < (*cmpqry)->nproximity; ++i) {
      (*cmpqry)->proximity[i] =
	(struct qc_s_proximity *) calloc((size_t) 1,
	sizeof(*((*cmpqry)->proximity[i])));
      if (!(*cmpqry)->proximity[i]) goto ALLOCERR;
      (*cmpqry)->proximity[i]->list0 = (struct qc_s_termlist *)
	calloc((size_t) 1,sizeof(*((*cmpqry)->proximity[i]->list0)));
      if (!(*cmpqry)->proximity[i]->list0) goto ALLOCERR;
      (*cmpqry)->proximity[i]->list1 = (struct qc_s_termlist *)
	calloc((size_t) 1,sizeof(*((*cmpqry)->proximity[i]->list1)));
      if (!(*cmpqry)->proximity[i]->list1) goto ALLOCERR;
    }
  }


  /* Skip over comment line */
  fgets(linein, 81, handle);

  /* Read in terms */
  for (i=0; i < (*cmpqry)->nterms; ++i) {
    fscanf(handle,"%d %d %lx %*[ \n\r]",(*cmpqry)->offsets + i,
	(*cmpqry)->termlen + i, (*cmpqry)->fieldmap + i);
    fread((*cmpqry)->qrytext + *((*cmpqry)->offsets + i), (size_t) 1,
      (size_t) abs(*((*cmpqry)->termlen + i)), handle);
    *((*cmpqry)->qrytext + *((*cmpqry)->offsets + i) +
      abs(*((*cmpqry)->termlen + i))) = 0;
    fscanf(handle,"%*[ \n\r]");
  }

  /* Skip over comment line */
  fgets(linein, (size_t) 81, handle);

  /* Read in fields */
  for (i=0; i < (*cmpqry)->nfields; ++i) {
    fscanf(handle,"%d %d %*[ \n\r]", (*cmpqry)->fldoffsets + i,
	(*cmpqry)->fieldlen + i);
    fread((*cmpqry)->fields + *((*cmpqry)->fldoffsets + i), (size_t) 1,
      (size_t) *((*cmpqry)->fieldlen + i), handle);
    *((*cmpqry)->fields + *((*cmpqry)->fldoffsets + i) +
      *((*cmpqry)->fieldlen + i)) = 0;
    fscanf(handle,"%*[ \n\r]");
  }

  if ((*cmpqry)->nproximity) {
    /* Skip over comment lines */
    fgets(linein,(size_t) 81,handle);
    fgets(linein,(size_t) 81,handle);
    fgets(linein,(size_t) 81,handle);
    for (i=0; i < (*cmpqry)->nproximity; ++i) {
      fscanf(handle,"%d %X %ld %d %d %*[ \n\r]",
	&(*cmpqry)->proximity[i]->termnum,
	&(*cmpqry)->proximity[i]->relation,
	&(*cmpqry)->proximity[i]->distance,
	&(*cmpqry)->proximity[i]->list0->nelements,
	&(*cmpqry)->proximity[i]->list1->nelements);
      (*cmpqry)->proximity[i]->list0->element =
	(int *) calloc((size_t) (*cmpqry)->proximity[i]->list0->nelements,
	  sizeof(*((*cmpqry)->proximity[i]->list0->element)));
      if (!(*cmpqry)->proximity[i]->list0->element) goto ALLOCERR;

      (*cmpqry)->proximity[i]->list1->element =
	(int *) calloc((size_t) (*cmpqry)->proximity[i]->list1->nelements,
	  sizeof(*((*cmpqry)->proximity[i]->list1->element)));
      if (!(*cmpqry)->proximity[i]->list1->element) goto ALLOCERR;

      for (j=0; j < (*cmpqry)->proximity[i]->list0->nelements; ++j)
	fscanf(handle,"%d ",&(*cmpqry)->proximity[i]->list0->element[j]);
      fscanf(handle,"%*[ \n\r]");
      for (j=0; j < (*cmpqry)->proximity[i]->list1->nelements; ++j)
	fscanf(handle,"%d ",&(*cmpqry)->proximity[i]->list1->element[j]);
      fscanf(handle,"%*[ \n\r]");
    }
  }


  /* Skip over comment line */
  fgets(linein, (size_t) 81, handle);

  if ((*cmpqry)->nlogic)
    for (i=0; i < (*cmpqry)->nlogic; ++i)
      fscanf(handle,"%d",(*cmpqry)->qrylogic+i);

  stat = 1;
  goto DONE;

ALLOCERR:
  stat = 0;
  fit_status = FIT_FATAL_MEM;

DONE:
  if (handle) fclose(handle);
  return(stat);
}
