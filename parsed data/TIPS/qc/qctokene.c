/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  qc_tokenerr					tokenerr.c

Function : Writes error message for invalid token.

Author : A. Fregly

Abstract :

Calling Sequence : qc_tokenerr(int tokentype);

Notes :

Change log :
000	20-MAR-90  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
void qc_tokenerr(int tokentype, int offset)
#else
void qc_tokenerr(tokentype, offset)
  int tokentype, offset;
#endif
{

  switch (abs(tokentype)) {

    /* Misplaced term */
    case 0 : {
      qc_adderror(FIT_ERR_OPERAND,offset);
      break;
    }

    /* Misplaced parenthesis */
    case 1 :
    case 9 : {
      qc_adderror(FIT_ERR_PAREN,offset);
      break;
    }

    case 2 : {
      qc_adderror(FIT_ERR_FIELD,offset);
      break;
    }

    case 3 : {
printf("qc_tokenerr: threshhold error\n");
      qc_adderror(FIT_ERR_THRESHHOLD,offset);
      break;
    }

    case 5 : {
      qc_adderror(FIT_ERR_PROXIMITY,offset); 
      break;
    }

    /* Misplaced operator or query ended with operator */
    default :
      qc_adderror(FIT_ERR_OPERATOR,offset);
  }
}
