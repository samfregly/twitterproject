/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_unpackob					unpackob.c

Function : Unpacks an an object query which was pack by qc_packobj.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = qc_unpackob(char *objbuf, int buflen, 
	struct qc_s_cmpqry *(*objqry));

	objbuf	Buffer containing packed object query.
	buflen	Length of objbuf
	objqry	Returned compile query buffer.

Notes : 

Change log : 
001	07-FEB-92  AMF	Do type cast on calloc.
002	14-DEC-92  AMF	Make it work with a K&R compiler.
003	26-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_UNIX
#include <memory.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
void qc_unpackob_get(char *(*source), void *dest, int destlen);
#else
void qc_unpackob_get();
#endif

#if FIT_ANSI
void qc_unpackob(char *objbuf, int buflen, struct qc_s_cmpqry *(*objqry))
#else
void qc_unpackob(objbuf, buflen, objqry)
  char *objbuf;
  int buflen;
  struct qc_s_cmpqry *(*objqry);
#endif
{

  char *bufptr;
  struct qc_s_cmpqry *cmpqry;
  int i,len;

  cmpqry = (struct qc_s_cmpqry *) calloc ((size_t) 1,(sizeof(*cmpqry)));
  *objqry = cmpqry;
  if (!cmpqry) goto ALLOCERR;

  cmpqry->qrytext = (char *) NULL;
  cmpqry->fields = (char *) NULL;
  cmpqry->qrylogic = (int *) NULL;
  cmpqry->termlen = (int *) NULL;
  cmpqry->fieldlen = (int *) NULL;
  cmpqry->fieldmap = (long *) NULL;
  cmpqry->offsets = (int *) NULL;
  cmpqry->fldoffsets = (int *) NULL;
  cmpqry->nproximity = 0;
  cmpqry->proximity = (struct qc_s_proximity *(*)) NULL;

  bufptr = objbuf;

  qc_unpackob_get(&bufptr,void_caste &cmpqry->nterms,sizeof(cmpqry->nterms));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->terms_L,sizeof(cmpqry->terms_L));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->nlogic,sizeof(cmpqry->nlogic));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->nfields,sizeof(cmpqry->nfields));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->fields_L,sizeof(cmpqry->fields_L));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->noperands,sizeof(cmpqry->noperands));
  qc_unpackob_get(&bufptr,void_caste &cmpqry->nproximity,sizeof(cmpqry->nproximity));

  qc_unpackob_get(&bufptr,void_caste cmpqry->delims,QC_MAXDELIM);

  for (i=0; i < QC_MAXRESERVED; ++i) {
    len = strlen(bufptr)+1;
    cmpqry->reserved[i] = (char *) calloc(len,1);
    if (!cmpqry->reserved[i]) goto ALLOCERR;
    qc_unpackob_get(&bufptr,void_caste cmpqry->reserved[i],len);
  }

  /* Initialize compiled query structure */
  if (cmpqry->nterms > 0) {
    cmpqry->qrytext = (char *) malloc((size_t) cmpqry->terms_L);
    if (cmpqry->qrytext == NULL) goto ALLOCERR;

    cmpqry->termlen = (int *) calloc(sizeof(*cmpqry->termlen),
      (size_t) cmpqry->nterms);
    if (!cmpqry->termlen) goto ALLOCERR;

    cmpqry->fieldmap = (long *) calloc(sizeof(*cmpqry->fieldmap),
      (size_t) cmpqry->nterms);
    if (!cmpqry->fieldmap) goto ALLOCERR;

    cmpqry->offsets = (int *) calloc(sizeof(*cmpqry->offsets),
      (size_t) cmpqry->nterms);
    if (!cmpqry->offsets) goto ALLOCERR;
  }

  if (cmpqry->nlogic) {
    cmpqry->qrylogic = 
      (int *) calloc((size_t) cmpqry->nlogic, sizeof(*cmpqry->qrylogic));
    if (!cmpqry->qrylogic) goto ALLOCERR;
  }

  if (cmpqry->nfields > 0) {
    cmpqry->fields = (char *) malloc((size_t) cmpqry->fields_L);
    if (!cmpqry->fields) goto ALLOCERR;

    cmpqry->fieldlen = (int *) calloc(sizeof(*cmpqry->fieldlen),
      (size_t) cmpqry->nfields);
    if (!cmpqry->fieldlen) goto ALLOCERR;

    cmpqry->fldoffsets = (int *) calloc(sizeof(*cmpqry->fldoffsets),
      (size_t) cmpqry->nfields);
    if (!cmpqry->fldoffsets) goto ALLOCERR;
  }

  if (cmpqry->nproximity > 0) {
    cmpqry->proximity = 
      (struct qc_s_proximity *(*)) calloc((size_t) cmpqry->nproximity,
      sizeof(*(cmpqry->proximity)));
    if (!cmpqry->proximity) goto ALLOCERR;
    for (i=0; i < cmpqry->nproximity; ++i) {
      cmpqry->proximity[i] = (struct qc_s_proximity *) calloc((size_t) 1,
	sizeof(*(cmpqry->proximity[i])));
      if (!cmpqry->proximity[i]) goto ALLOCERR;
      cmpqry->proximity[i]->list0 = 
	(struct qc_s_termlist *) calloc((size_t) 1,
        sizeof(*(cmpqry->proximity[i]->list0)));
      if (!cmpqry->proximity[i]->list0) goto ALLOCERR;
      cmpqry->proximity[i]->list1 = 
	(struct qc_s_termlist *) calloc((size_t) 1,
        sizeof(*(cmpqry->proximity[i]->list1)));
      if (!cmpqry->proximity[i]->list1) goto ALLOCERR;
    }
  }

  qc_unpackob_get(&bufptr, void_caste cmpqry->offsets, 
    cmpqry->nterms * sizeof(*cmpqry->offsets));

  qc_unpackob_get(&bufptr, void_caste cmpqry->termlen, 
    cmpqry->nterms * sizeof(*cmpqry->termlen));

  qc_unpackob_get(&bufptr, void_caste cmpqry->fieldmap, 
    cmpqry->nterms * sizeof(*cmpqry->fieldmap));

  qc_unpackob_get(&bufptr, void_caste cmpqry->qrytext, cmpqry->terms_L); 

  if (cmpqry->nfields) {
    qc_unpackob_get(&bufptr, void_caste cmpqry->fldoffsets, 
      cmpqry->nfields * sizeof(*cmpqry->fldoffsets));

    qc_unpackob_get(&bufptr, void_caste cmpqry->fieldlen,
      cmpqry->nfields * sizeof(*cmpqry->fieldlen));

    qc_unpackob_get(&bufptr, void_caste cmpqry->fields, cmpqry->fields_L);
  }

  for (i=0; i < cmpqry->nproximity; ++i) {
    qc_unpackob_get(&bufptr, void_caste &cmpqry->proximity[i]->termnum, 
      sizeof(cmpqry->proximity[i]->termnum));
    qc_unpackob_get(&bufptr, void_caste &cmpqry->proximity[i]->relation, 
      sizeof(cmpqry->proximity[i]->relation));
    qc_unpackob_get(&bufptr,void_caste &cmpqry->proximity[i]->distance, 
      sizeof(cmpqry->proximity[i]->distance));
    qc_unpackob_get(&bufptr,
      void_caste &cmpqry->proximity[i]->list0->nelements, 
      sizeof(cmpqry->proximity[i]->list0->nelements));
    qc_unpackob_get(&bufptr,
      void_caste &cmpqry->proximity[i]->list1->nelements, 
      sizeof(cmpqry->proximity[i]->list1->nelements));

    cmpqry->proximity[i]->list0->element = (int *) calloc(
      cmpqry->proximity[i]->list0->nelements,
      sizeof(*cmpqry->proximity[i]->list0->element));
    if (!cmpqry->proximity[i]->list0->element) goto ALLOCERR;

    qc_unpackob_get(&bufptr,
      void_caste cmpqry->proximity[i]->list0->element,
      cmpqry->proximity[i]->list0->nelements * 
      sizeof(*cmpqry->proximity[i]->list0->element));

    cmpqry->proximity[i]->list1->element = (int *) calloc(
      cmpqry->proximity[i]->list1->nelements,
      sizeof(*cmpqry->proximity[i]->list1->element));
    if (!cmpqry->proximity[i]->list1->element) goto ALLOCERR;

    qc_unpackob_get(&bufptr,
      void_caste cmpqry->proximity[i]->list1->element,
      cmpqry->proximity[i]->list1->nelements * 
      sizeof(*cmpqry->proximity[i]->list1->element));
  }

  qc_unpackob_get(&bufptr, void_caste cmpqry->qrylogic,
      cmpqry->nlogic * sizeof(*cmpqry->qrylogic));

  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;

DONE:
  ;
}


#if FIT_ANSI
void qc_unpackob_get(char *(*source), void *dest, int destlen)
#else
void qc_unpackob_get(source, dest, destlen)
  char *(*source);
  char *dest;
  int destlen;
#endif
{
    memmove(dest, *source, destlen);
    *source += destlen;
}
