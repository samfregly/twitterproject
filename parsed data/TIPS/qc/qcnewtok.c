/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_newtoken                                       newtoken.c

Function : Allocates and initializes new token descriptor.

Author : A. Fregly

Abstract : 

Calling Sequence : token = qc_newtoken(int tokentype, int subtype,
  unsigned long nvalue, char *value, int vlen);

  tokentype     Token type.  Types are defined in QC.H.
  subtype       Token subtype.  Bit defs are defined in QC.H.
  nvalue        Numeric value associated with token.
  value         Character string containing value of token.
  vlen          Length of "value", excluding terminating 0 byte.

Notes : 

Change log : 
000     22-DEC-90  AMF  Created.
001     23-MAR-93  AMF  Compile under Coherent/GNU C.
002     08-AUG-94  AMF  Handle NULL token.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
struct qc_s_token *qc_newtoken(int tokentype, int subtype,
  unsigned long nvalue, char *value, int vlen, int offset)
#else
struct qc_s_token *qc_newtoken(tokentype, subtype, nvalue, value, vlen,
  offset)
  int tokentype, subtype;
  unsigned long nvalue;
  char *value;
  int vlen, offset;
#endif
{

  struct qc_s_token *token;

  token = (struct qc_s_token *) calloc((size_t) 1,sizeof(*token));
  if (!token)
    goto ALLOCERR;
  else {
    token->value = (char *) malloc((size_t) (vlen+1));
    if (!token->value) goto ALLOCERR;
    token->type = tokentype;
    token->subtype = subtype;
    token->nvalue = nvalue;
    if (vlen) {
      strncpy(token->value,value,(size_t) vlen);
    }
    token->value[vlen] = 0;
    token->len = vlen;
    token->offset = offset;
  }
  goto DONE;

ALLOCERR:
  token = (struct qc_s_token *) NULL;
  fit_status = FIT_FATAL_MEM;

DONE:
  return(token);

}
