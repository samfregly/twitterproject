/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_dealobj					dealobj.c

Function : Deallocates compiled query.

Author : A. Fregly

Abstract : This routine is used to deallocate the storage currently allocated
	to a compiled query.

Calling Sequence : stat = qc_dealobj(struct cmpqry_struc *(*cmpqry));

	cmpqry	Pointer at compiled query structure pointer.  The space
		allocated to the compiled query is deallocated and the
		compiled query structure pointer is set to NULL.

Notes :

Change log :
000	22-MAR-90  AMF	Created
001	30-APR-91  AMF  Deallocate "element" pointers in profile term lists.
002	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>

#if FIT_ANSI
int qc_dealobj(struct qc_s_cmpqry *(*cmpqry))
#else
int qc_dealobj(cmpqry)
  struct qc_s_cmpqry *(*cmpqry);
#endif
{
  int i;

  if (*cmpqry) {
    if ((*cmpqry)->qrytext)
      free ((*cmpqry)->qrytext);
    if ((*cmpqry)->fields)
      free ((*cmpqry)->fields);
    if ((*cmpqry)->qrylogic)
      free ((*cmpqry)->qrylogic);
    if ((*cmpqry)->termlen)
      free ((*cmpqry)->termlen);
    if ((*cmpqry)->fieldlen)
      free ((*cmpqry)->fieldlen);
    if ((*cmpqry)->fieldmap)
      free ((*cmpqry)->fieldmap);
    if ((*cmpqry)->offsets)
      free ((*cmpqry)->offsets);
    if ((*cmpqry)->fldoffsets)
      free ((*cmpqry)->fldoffsets);

    for (i = 0; i < QC_MAXRESERVED; ++i)
      if ((*cmpqry)->reserved[i]) {
	free((*cmpqry)->reserved[i]);
	(*cmpqry)->reserved[i] = (char *) NULL;
      }

    if ((*cmpqry)->nproximity) {
      for (i=0; i < (*cmpqry)->nproximity; ++i) {
	free((*cmpqry)->proximity[i]->list0->element);
	free((*cmpqry)->proximity[i]->list0);
	free((*cmpqry)->proximity[i]->list1->element);
	free((*cmpqry)->proximity[i]->list1);
	free((*cmpqry)->proximity[i]);
      }
      free((*cmpqry)->proximity);
    }

    free (*cmpqry);
    *cmpqry = (struct qc_s_cmpqry *) NULL;
  }
  return (1);
}
