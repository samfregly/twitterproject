/******************************************************************************
Module Name : qc_dumpqry					dumpqry.c

Function : Dumps compiled query

Author : A. Fregly

Abstract :

Calling Sequence : qc_dumpqru(FILE *stream, struct qc_s_cmpqry *cmpqry);

Notes :

Change log :
000	24-MAR-90  AMF	Created
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
void qc_dumpqry(FILE *stream, struct qc_s_cmpqry *cmpqry)
#else
void qc_dumpqry(stream, cmpqry)
  FILE *stream;
  struct qc_s_cmpqry *cmpqry;
#endif
{
  int i,j;

  static char *operators[9] = {"","(","range","fieldspec","not","and","or",")",
	"" };

  if (cmpqry) {
    fprintf(stream, "Number of terms: %d, Number of logic elements %d\n",
      cmpqry->nterms,cmpqry->nlogic);
    fprintf(stream, "Terms:\n");
    for (i=0; i < cmpqry->nterms; ++i) {
      fprintf(stream,"  %3d\t{%s}",i+1,
	cmpqry->qrytext + *(cmpqry->offsets+i));
      if (*(cmpqry->termlen+i) < 0)
	fprintf(stream,":{%s}", cmpqry->qrytext + *(cmpqry->offsets + ++i));
      fprintf(stream,"\n");
      fprintf(stream,"     \t  fields: ");
      if (*(cmpqry->fieldmap + i)) {
	for (j = 0; j < cmpqry->nfields; ++j)
	  if (*(cmpqry->fieldmap + j) | (1L << i))
	    fprintf(stream,"{%s} ",cmpqry->fields + *(cmpqry->fldoffsets+j));
      }
      else
	fprintf(stream,"all");
      fprintf(stream,"\n");
    }
    fprintf(stream, "Logic:\n");
    for (i=0; i < cmpqry->nlogic; ++i) {
      if (*(cmpqry->qrylogic+i) >= 0) {
	fprintf(stream,"{%s}",
	  (cmpqry->qrytext + *(cmpqry->offsets+*(cmpqry->qrylogic+i))));
	if (*(cmpqry->termlen + *(cmpqry->qrylogic+i)) < 0)
	  fprintf(stream,":{%s}",
	    cmpqry->qrytext + *(cmpqry->offsets + *(cmpqry->qrylogic+i) + 1));
      }
      else
	fputs(operators[-*(cmpqry->qrylogic+i)],stream);
      fputc(' ',stream);
    }
  }
  else
    fprintf(stream,"*** Invalid or null query");

  fprintf(stream,"\n");
}
