/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_inittemp					inittemp.c

Function : Initializes internal data structures used by query compiler.

Author : A. Fregly

Abstract :

Calling Sequence : qc_inittemp();

Notes :

Change log :
000	22-MAR-90  AMF	Created
001	08-MAY-91  AMF	Don't allocate reserved delimiters twice.
002	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tips.h>
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>


#if FIT_ANSI
int qc_inittemp(char *srcqry, int nreserved, char *reserved[],char *delims)
#else
int qc_inittemp(srcqry, nreserved, reserved, delims)
  char *srcqry;
  int nreserved;
  char *reserved[], *delims;
#endif
{

  struct qc_s_error *errptr;

  static char *def_reserved[TIPS_NRESERVED] = TIPS_DEFRESERVED;

  int i, stat;
  size_t vlen;
  char *reservedptr;

  qc_qrystart = srcqry;
  qc_qryidx = -1;
  qc_nerrs = 0;
  qc_textsize = 0;
  qc_nfields = 0;
  qc_fieldsize = 0;

  /* Release errors from last compile */
  while (qc_firsterr) {
    errptr = qc_firsterr;
    qc_firsterr = qc_firsterr->next;
    free (errptr);
  }

  qc_firsttoken = (struct qc_s_token *) NULL;
  qc_lasttoken = (struct qc_s_token *) NULL;

  qc_nterms = 0;
  qc_firstterm = (struct qc_s_termentry *) NULL;
  qc_lastterm = (struct qc_s_termentry *) NULL;

  qc_toplogic = (struct qc_s_logicentry *) NULL;

  qc_nobjentries = 0;
  qc_firstobj = (struct qc_s_objentry *) NULL;
  qc_lastobj = (struct qc_s_objentry *) NULL;

  if (delims) 
    vlen = strlen(delims);
  else
    vlen = 0;

  for (i=0; i < sizeof(TIPS_DEFAULTDELIMS)-1; ++i)
    if (i < vlen)
      qc_delims[i] = delims[i];
    else
      qc_delims[i] = TIPS_DEFAULTDELIMS[i];

  for (i=0; i < QC_MAXRESERVED; ++i) {
    qc_reserved[i] = (char *) NULL;
    if (i != QC_OFF_LPAREN && i != QC_OFF_RPAREN && i != QC_OFF_FIELD &&
	i != QC_OFF_COMMA) {
      if (i < nreserved)
        reservedptr = reserved[i];
      else
        reservedptr = def_reserved[i];
      vlen = strlen(reservedptr);
      qc_reserved[i] = (char *) malloc(vlen+1);
      if (qc_reserved[i] == NULL) goto ALLOCERR;
      strcpy(qc_reserved[i],reservedptr);
      fit_supcase(qc_reserved[i]);
    }
  }

  qc_reserved[QC_OFF_LPAREN] = (char *) calloc((size_t) 2, (size_t) 1);
  if (qc_reserved[QC_OFF_LPAREN] == NULL) goto ALLOCERR;
  *qc_reserved[QC_OFF_LPAREN] = qc_delims[QC_OFF_OPENPAREN];

  qc_reserved[QC_OFF_RPAREN] = (char *) calloc((size_t) 2, (size_t) 1);
  if (qc_reserved[QC_OFF_RPAREN] == NULL) goto ALLOCERR;
  *qc_reserved[QC_OFF_RPAREN] = qc_delims[QC_OFF_CLOSEPAREN];

  qc_reserved[QC_OFF_FIELD] = (char *) calloc((size_t) 2, (size_t) 1);
  if (qc_reserved[QC_OFF_FIELD] == NULL) goto ALLOCERR;
  *qc_reserved[QC_OFF_FIELD] = qc_delims[QC_OFF_FIELDDELIM];

  qc_reserved[QC_OFF_COMMA] = (char *) calloc((size_t) 2,(size_t) 1);
  if (qc_reserved[QC_OFF_COMMA] == NULL) goto ALLOCERR;
  *qc_reserved[QC_OFF_COMMA] = qc_delims[QC_OFF_DELIM_COMMA];

  stat = 1;
  goto DONE;

ALLOCERR:
  stat = 0;
  fit_status = FIT_FATAL_MEM;

DONE:
  return(stat);
}
