/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_proximty				     	proxmty.c

Function : Creates a proximity relationship.

Author : A. Fregly

Abstract : This routine will add a proximity relationship to the compiled
	query.

Calling Sequence : stat = qc_proximty(struct qc_s_cmpqry *cmpqry, 
  int subtype, long distance, struct qc_s_termlist *list1,
    struct qc_s_termlist *list2);

  cmpqry	Compiled query to which proximity relationship is added.
  subtype	Bit mask defining type of proximity relationship.
  distance	Distance component of the relationship.
  list0		Term list containing components of first operand of
		proximity relationship.
  list1		Term list containing components of second operand of
		proximity relationship.

Notes : 

Change log : 
000	09-DEC-90  AMF	Created.
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
int qc_proximty(struct qc_s_token *token, struct qc_s_termlist *list0,
  struct qc_s_termlist *list1, struct qc_s_cmpqry *cmpqry)
#else
int qc_proximty(token, list0, list1, cmpqry)
  struct qc_s_token *token;
  struct qc_s_termlist *list0, *list1;
  struct qc_s_cmpqry *cmpqry;
#endif
{

  int stat,i;
  struct qc_s_proximity *(*newproximity);

  /* Allocate new list of proximity relationships */
  newproximity =
    (struct qc_s_proximity *(*)) calloc((size_t) (cmpqry->nproximity+1),
    sizeof(*newproximity));
  if (!newproximity) goto ALLOCERR;

  /* Copy old list into new list */
  if (cmpqry->nproximity) {
    for (i=0; i < cmpqry->nproximity; ++i)
      newproximity[i] = cmpqry->proximity[i];
    free(cmpqry->proximity);
  }
  cmpqry->proximity = newproximity;

  /* Allocate proximity relationship structure for new one being added */
  cmpqry->proximity[cmpqry->nproximity] = (struct qc_s_proximity *)
    malloc(sizeof(*(cmpqry->proximity[cmpqry->nproximity])));
  if (!cmpqry->proximity[cmpqry->nproximity]) goto ALLOCERR;

  /* Add in the new proximity descriptor */
  cmpqry->proximity[cmpqry->nproximity]->termnum = cmpqry->noperands++;
  cmpqry->proximity[cmpqry->nproximity]->relation = token->subtype;
  cmpqry->proximity[cmpqry->nproximity]->distance = token->nvalue;
  cmpqry->proximity[cmpqry->nproximity]->list0 = list0;
  cmpqry->proximity[cmpqry->nproximity]->list1 = list1;
  cmpqry->nproximity += 1;
  stat = 1;
  goto DONE;

ALLOCERR:
  stat =  0;
  fit_status = FIT_FATAL_MEM;

DONE:
  return(stat);
}
