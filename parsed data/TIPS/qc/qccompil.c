/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  qc_compile

Function :  Compiles text search query.

Author : A. Fregly

Abstract : This routine will compile a text search query into compiled form.
	The input search query is a character string containing search terms
	and operators in an infix form.  The output compiled query contains
	a list of terms contained in the query, a list of descriptors of those
	terms, and the logic of the query in postfix form so that the query
	may easily be evaluated.

	Query terms may be any consecutive string of characters not containing
	any blanks or parenthesis or curly brackets, or any string of
	characters embedded between matching curly brackets, {}.

	Query operators in order of precedence from highest to lowest are
	"not", "and", and "or".  Query logic is evaluated from right to
	left.  Parenthesis may be used to force a desired sequence of
	evaluation.

Calling Sequence :  nerrors = tx_compile(char *srcqry,
					 struct qc_s_cmpqry *cmpqry);

	srcqry          Character string containing input source query.
			All control characters and other non-printables
			are ignored.

	cmpqry          Returned compiled query structure.


Notes :

Change log :
000     18-MAR-90  AMF  Created.
001     25-MAR-93  AMF  Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <qcalloc.h>
#include <qcstat.h>
#include <fit_stat.h>


#if FIT_ANSI
int qc_compile(char *srcqry, int nfields, char *fieldnames[],
  int nreserved, char *reserved[], char *delims,
  struct qc_s_cmpqry *(*cmpqry))
#else
int qc_compile(srcqry, nfields, fieldnames, nreserved, reserved,
    delims, cmpqry)
  char *srcqry;
  int nfields;
  char *fieldnames[];
  int nreserved;
  char *reserved[];
  char *delims;
  struct qc_s_cmpqry *(*cmpqry);
#endif
{
  int stat, expected;
  struct qc_s_token *nexttoken;
  char *srcptr;

  /* Set success code */
  fit_status = FIT_STAT_NORMAL;

  /* Inititialize data strutures */
  srcptr = srcqry;
  qc_inittemp(srcptr,nreserved,reserved,delims); /* temp data structures */
  expected = QC_OPERAND;        /* First token expected */
  qc_dealobj(cmpqry);           /* Compiled query */
  /* Parse query and produce compiled query */
  stat = 1;
  do {
    nexttoken = qc_gettoken(&srcptr,&expected);
    if (!nexttoken) {
      stat = 0;
    }
    else {
      stat = qc_addtoqry(nexttoken);
   }

  } while (stat && nexttoken->len != 0);

  if (stat) stat = qc_makeobj(cmpqry,nfields,fieldnames);

  /* Release temporary data structures */
  qc_dealtemp();

  if (!stat && fit_lasterr() != FIT_STAT_NORMAL) qc_adderror(fit_status,0);

  return(qc_nerrs);
}
