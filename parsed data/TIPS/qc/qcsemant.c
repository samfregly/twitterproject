/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : qc_semantic					semantic.c

Function : Performs semantic checks and productions.

Author : A. Fregly

Abstract : This routine is used to perform semantic checks on an object
	query.  During this process, it will condense the field list for
	the query to contain only unique elements, fill in the field bit
	maps for each term in the query.  A new query logic equation is
	created minus the range and field operators.

Calling Sequence :
	stat = qc_semantic(struct qc_s_cmpqry *cmpqry);

Notes :

Change log :
000	01-MAY-90  AMF	Created.
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#define MAXFLD BITS_IN_LONG

#if FIT_ANSI
int qc_semantic(struct qc_s_cmpqry *cmpqry, struct qc_s_objentry *qrylogic[])
#else
int qc_semantic(cmpqry, qrylogic)
  struct qc_s_cmpqry *cmpqry;
  struct qc_s_objentry *qrylogic[];
#endif
{

  struct operand_stack {
    int operators;
    int threshhold;
    int nelements;
    int nterms;
    int newidx;
    int sidx;
    int eidx;
    struct operand_stack *prev;
  };

  struct operand_stack *stack, *top;

  struct qc_s_termlist *termlist1, *termlist2;

  int *fldmap;
  char *nextfld;
  int i,j,nunique,off,fldnum,stat,element,*newlogic,*newelement,fidx;
  int nelements,offset,threshhold,*cmplogicptr,*newptr;
  long fmap;

  fldmap = (int *) NULL;
  stack = (struct operand_stack *) NULL;
  top = (struct operand_stack *) NULL;
  stat = 1;
  nunique = 0;
  nelements = qc_nobjentries;
  newlogic = (int *) calloc((size_t) (nelements*3),sizeof(*newlogic));
  newelement = newlogic;

  /* Create field map and compress field defs to unique fields */
  if (cmpqry->nfields) {
    fldmap = (int *) malloc((size_t) (cmpqry->nfields * sizeof(*fldmap)));
    if (!fldmap) goto ALLOCERR;

    off = 0;
    for (i = 0; i < cmpqry->nfields; ++i) {
      nextfld = fit_supcase(cmpqry->fields + *(cmpqry->fldoffsets+i));
      for (j = 0; j < nunique; ++j)
	if (strcmp(nextfld, cmpqry->fields + *(cmpqry->fldoffsets+j)) == 0)
	  break;
      if (j >= nunique) {
	if (i != nunique) {
	  memmove(cmpqry->fields + off, nextfld,
	    (size_t) (*(cmpqry->fieldlen+i) + 1));
	  *(cmpqry->fldoffsets + nunique) = off;
	  *(cmpqry->fieldlen + nunique) = *(cmpqry->fieldlen + i);
	}
	off += *(cmpqry->fieldlen+i) + 1;
	nunique += 1;
      }
      *(fldmap+i) = j;
    }
    cmpqry->nfields = nunique;
  }
  else
    fldmap = (int *) NULL;


  /* Evaluate logic equation, assigning fields to terms, checking for
     semantic errors */
  fldnum = -1;
  cmpqry->noperands = cmpqry->nterms;
  cmpqry->proximity = (struct qc_s_proximity *(*)) NULL;
  for (i = 0; i < qc_nobjentries; ++i) {
    element = qrylogic[i]->token->type;
    offset = qrylogic[i]->token->offset;

    if (element >= 0) {
     /* Push operand onto top of stack, start by allocating entry */
      top = (struct operand_stack *) malloc(sizeof(*top));
      if (!top) goto ALLOCERR;

      /* fill in the entry */
      top->operators = 0;
      top->threshhold = 1;
      top->nelements = 1;
      top->nterms = 1;
      top->newidx = ptr_dif(newelement,newlogic);
      top->sidx = i;
      top->eidx = i;
      *(newelement++) = element;
      top->prev = stack;	/* Link to old top */
      stack = top;              /* Push onto stack */
    }

    else {
      /* Evaluate the operator */
      switch (element) {
	case QC_COMMA :
	case QC_AND :
	case QC_OR : {
	  if (top) {
	    stack = top->prev;
	    if (stack) {
	      stack->operators |= (top->operators | (1 << abs(element)));
	      stack->threshhold = max(stack->threshhold,top->threshhold);
	      if (element == QC_COMMA)
		stack->nelements += 1;
	      stack->nterms += top->nterms;
	      stack->eidx = top->eidx;
	      free(top);
	      top = stack;
	      *(newelement++) = element;
	    }
	    else {
	      qc_adderror(FIT_ERR_OPERATOR,offset);
	    }
	  }
	  else {
	    qc_adderror(FIT_ERR_OPERATOR,offset);
	  }
	  break;
	}

	case QC_THRESHHOLD : {
	  if (top) { 
	    threshhold = qrylogic[i]->token->nvalue;
	    if (threshhold == -1) threshhold = top->nelements;

	    if (threshhold > top->nelements) {
	      qc_adderror(FIT_ERR_THRESHHOLD,offset);
	      threshhold = top->nelements;
	    }
	    top->operators |= (1 << abs(element));
	    top->threshhold = max(top->threshhold,threshhold);
	    top->nelements = 1;
	    *(newelement++) = element;
	    *(newelement++) = threshhold;
	  }
	  else {
	    qc_adderror(FIT_ERR_OPERATOR,offset);
	  }
	  break;
	}   

	case QC_PROXIMITY : {
	  if (top) {
	    if (top->prev) {
	      /* Combine list of top into next to top */
	      stack = top->prev;
	      stack->operators |= top->operators;
	      stack->threshhold = max(stack->threshhold,top->threshhold);

	      if ((stack->operators & 
		  ((1 << abs(QC_AND)) | (1 << abs(QC_PROXIMITY)) |
		   (1 << abs(QC_NOT)))) || stack->threshhold > 1)
		/* Proximity operands may only have COMMA or OR logic,
		   flag error */
		qc_adderror(FIT_ERR_PROXIMITY,offset);

	      /* Create term list for first operand */
	      stat = qc_makelist(qrylogic,stack->sidx,stack->eidx,
		  stack->nterms, &termlist1);
	      if (stat) {
		/* Create term list for second operand */
		stat = qc_makelist(qrylogic,top->sidx,top->eidx,
		    top->nterms, &termlist2);
		if (stat) {
		  /* Create proximity relationship */
		  stat = qc_proximty(qrylogic[i]->token,termlist1,termlist2,
		      cmpqry);
		  if (stat) {
		    /* Add proximity relationship to output logic equation */
		    newelement = newlogic + stack->newidx;
		    *(newelement++) = cmpqry->noperands-1;
		  }
		}
	      }
	      /* Pop top off stack */
	      stack->operators = (1 << abs(element));
	      stack->threshhold = 1;
	      stack->nelements = 1;
	      stack->nterms += top->nterms;
	      stack->eidx = top->eidx;
	      free(top);
	      top = stack;
	    }
	    else {
	      qc_adderror(FIT_ERR_OPERATOR,offset);
	    }
	  }
	  else {
	    qc_adderror(FIT_ERR_OPERATOR,offset);
	  }
	  break;
	}

	case QC_NOT : {
	  if (top)
	    /* Add operator to modified logic */
	    top->operators |= (1 << abs(element));
	  else {
	    qc_adderror(FIT_ERR_OPERATOR,offset);
	  }
	  *(newelement++) = element;
	  break;
	}

	case QC_FIELD : {
	  fldnum += 1;
	  if (top) {
	    for (j = top->sidx; j <= top->eidx; ++j)
	      if (qrylogic[j]->token->type >= 0) {
		/* Set bit corresponding to field in field map for term */
		fidx = qrylogic[j]->token->type;
		fmap = 1L << fldmap[fldnum];
		cmpqry->fieldmap[fidx] |= fmap;
	      }
	  }
	  else {
	    qc_adderror(FIT_ERR_OPERATOR,offset);
	  }
	  break;
	}
      }
    }
  }
  stat = 1;
  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  stat = 0;


DONE:
  /* Release evaluation stack */
  for (top = stack; stack; stack = top) {
    top = stack->prev;
    free(stack);
  }

  /* Release field map */
  if (fldmap) free(fldmap);

  /* Free the old logic equation and plug in the new one */
  if (newlogic) {
    cmpqry->nlogic = ptr_dif(newelement,newlogic);
    if (cmpqry->nlogic) {
      cmpqry->qrylogic = (int *) calloc((size_t) cmpqry->nlogic,
	sizeof(*cmpqry->qrylogic));
      if (!cmpqry->qrylogic) return(0);
      for (newptr=newlogic, cmplogicptr=cmpqry->qrylogic; 
	  newptr != newelement;)
	*(cmplogicptr++) = *(newptr++);
    }
    free(newlogic);
  }

  return(stat);
}
