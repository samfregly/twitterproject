/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  qc_tokntype					tokntype.c

Function : Determines token type

Author : A. Fregly

Abstract : This routine will determine the type of token based on the
  character string supplied.

Calling Sequence : tokentype = qc_tokentype(char *token)

Notes :

Change log :
000	20-MAR-90  AMF	Created.
001	15-AUG-91  AMF	Put in "proximity type bit set" fix as done on VMS
			version.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
int qc_tokntype(char *token, int len, int *subtype, int offset)
#else
int qc_tokntype(token, len, subtype, offset)
  char *token;
  int len, *subtype, offset;
#endif
{

  int i, idx;
  char *t;               

  *subtype = 0;
  t = (char *) malloc((size_t) (len+1));
  if (t == NULL) {
    qc_adderror(FIT_FATAL_MEM,offset);
    fit_status = FIT_FATAL_MEM;
    return(QC_EOQ);
  }
  if (len) strncpy(t, token, (size_t) len);
  *(t+len)=0;
  fit_supcase(t);

  if (*t && *t == qc_delims[QC_OFF_FIELDDELIM]) *(t+1) = 0;

  if (*t) {
    for (i=0, idx=0; i < QC_MAXRESERVED; ++i) {
      if (!strcmp(qc_reserved[i],t)) {
	idx = qc_reserved_tokennum[i];
	i = QC_MAXRESERVED;
      }
    }
  }
  else
    idx = QC_EOQ;

  if (idx == QC_THRESHHOLD) {
    if (!strcmp(qc_reserved[QC_OFF_ALL],t))
      *subtype = -1;
  }

  else if (idx == QC_PROXIMITY) {
    if (!strcmp(qc_reserved[QC_OFF_AHEAD],t))
      *subtype = (1 << QC_BIT_UNIDIRECTIONAL);
  }

  free(t);

  return (idx);
}
