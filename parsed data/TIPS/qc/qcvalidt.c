/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  qc_validtok					validtok.c

Function : Determines if token is valid

Author : A. Fregly

Abstract : This routine will determine if the supplied token is valid based
  on the expected token class (QC_OPERAND or QC_OPERATOR) and input token
  type.  Based on the token type, it will update the value of the expected
  token type.

Calling Sequence : tokentype = qc_tokentype(char *token, int *expected)

Notes :

Change log :
000	20-MAR-90  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
int qc_validtok(int tokentype, int *expected)
#else
int qc_validtok(tokentype, expected)
  int tokentype, *expected;
#endif
{

  /*
  This table is used to look up the validity of a token.  It is a 2 rows by
  N columns table.  The expected token class is used to index into the proper
  row of the table, and the token type indexes the "next expected" value in
  the row.  If the token type is valid at this point in time, the table value
  is a postive value equaling the token class expected next.  If the token
  is invalid, the value is the negation of the token class expected next.
  The following shows how the mapping of tokens to columns

	operand,        (,       ^field,       threshhold,      NOT
	proximity,    NOT,          AND,           OR,          ",",            ),
	Begin query,  End query
  */

  static int state_table [2] [12] = {
    { QC_OPERATOR,  QC_OPERAND,   QC_OPERAND, QC_OPERAND,   QC_OPERAND,
     -QC_OPERAND, -QC_OPERAND,  -QC_OPERAND, -QC_OPERAND,  -QC_OPERATOR, 
     -QC_OPERATOR,-QC_OPERAND},

    {-QC_OPERATOR, -QC_OPERAND,  -QC_OPERAND,  -QC_OPERAND, -QC_OPERAND, 
      QC_OPERAND,   QC_OPERAND,   QC_OPERAND,   QC_OPERAND,  QC_OPERATOR,
      QC_OPERATOR,  QC_OPERATOR}
  };


  int stat;

  stat = ((*expected = state_table[*expected-1][abs(tokentype)]) >= 0);
  *expected = abs(*expected);
  return (stat);
}
