/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_tokenlen					tokenlen.c

Function : Determines length of next token starting at input char pointer.

Author : A. Fregly

Abstract :

Calling Sequence : len = qc_tokenlen(char *srcptr);

Notes :

Change log :
000  27-APR-90  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
int qc_tokenlen(char *srcptr)
#else
int qc_tokenlen(srcptr)
  char *srcptr;
#endif
{

  char *sptr;

  sptr = srcptr;


  if (!*srcptr)
    return(0);

  else if (*srcptr == qc_delims[QC_OFF_OPENPAREN] || 
      *srcptr == qc_delims[QC_OFF_CLOSEPAREN] ||
      *srcptr == *qc_reserved[QC_OFF_COMMA])
    srcptr += 1;

  else if (*srcptr == qc_delims[QC_OFF_FIELDDELIM]) {
    for (++srcptr; *srcptr && *srcptr > ' ' && 
	*srcptr != qc_delims[QC_OFF_FIELDDELIM] &&
	*srcptr != qc_delims[QC_OFF_OPENPAREN];)
      if (*srcptr == qc_delims[QC_OFF_QUOTE]) {
	srcptr += 1;
	if (*srcptr) srcptr += 1;
      }
      else
	srcptr += 1;
  }

  else {
    for (;*srcptr && *srcptr > ' ' && 
	*srcptr != qc_delims[QC_OFF_OPENPAREN] &&
	*srcptr != qc_delims[QC_OFF_CLOSEPAREN] && 
	*srcptr != *qc_reserved[QC_OFF_COMMA];)
      if (*srcptr == qc_delims[QC_OFF_QUOTE]) {
	srcptr += 1;
	if (*srcptr) srcptr += 1;
      }
      else
	srcptr += 1;
  }

  return (ptr_dif(srcptr,sptr));
}
