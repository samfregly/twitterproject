/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_geterr						geterr.c

Function : Returns error message from last query compilation.

Author : A. Fregly

Abstract :

Calling Sequence : stringptr = qc_geterr(int erridx, int *errnum, int *offset);

	erridx	Error index, between 0 and the number of errors in the query
		minus 1.
	errnum	Returned error id.
	offset	Offset in query at which error occurred.

Notes :

Change log :
000	25-MAR-90  AMF	Created.
001	07-FEB-92  AMF	Set errnum and offset to zero if error occurs.
002	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
char *qc_geterr(int erridx, int *errnum, int *offset)
#else
char *qc_geterr(erridx, errnum, offset)
  int erridx, *errnum, *offset;
#endif
{
  int i;
  struct qc_s_error *errptr;

  for (i = 0, errptr = qc_firsterr; i < erridx && errptr;
      ++i, errptr = errptr->next)
    ;
  if (i == erridx && errptr) {
    *offset = errptr->idx;
    *errnum = errptr->errortype;
    return (fit_getmsg(*errnum));
  }
  else {
    *offset = 0;
    *errnum = 0;
    return((char *) NULL);
  }
}

