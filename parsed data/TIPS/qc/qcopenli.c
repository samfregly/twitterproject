/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_openlib					qcopenli.c

Function : Opens query library

Author : A. Fregly

Abstract : This function is used to open a query library. A query library
	serves as a repository of compiled queries. Certain TIPS programs,
	such as SPOTTER require a query library as input.

Calling Sequence : qc_libhandle *libhandle = qc_openlib(char *libname,
	int openmode, access);

  libname	Query library name. A default file extension of .qlb is
		used if the caller does not supply one.
  openmode	File open mode. Allowed values for this are the same as those
		used for the C "open" library function. These values are
		usually found in the C include file, fcntl.h.
  access	Access bit map for use with O_CREAT function. See bit
		definitions in fcntl.h.
  libhandle	Returned handle for use in accessing the library.

Notes:

Change log :
000	30-JUL-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <qc.h>
#include <drct.h>

#if FIT_ANSI
struct drct_s_file *qc_openlib(char *libname, int openmode, int access)
#else
struct drct_s_file *qc_openlib(libname, openmode, access)
  char *libname;
  int openmode, access;
#endif
{
  char lib[FIT_FULLFSPECLEN+1], *ftype;

  ftype = strrchr(libname,'.');
  if (ftype == NULL || (strcmp(ftype,".qlb") && strcmp(ftype,".QLB")))
    fit_expfspec(libname, ".qlb", "", lib);
  else
    strcpy(lib, libname);

  return drct_open(lib, openmode, (unsigned) access, -1, -1);
}
