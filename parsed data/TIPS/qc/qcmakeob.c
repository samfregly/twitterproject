/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_makeobj					makeobj.c

Function : Converts internally buffered object query to output object
	query format.

Author : A. Fregly

Abstract : This routine will convert the object query internally buffered
	in the query compiler into the output object query format defined
	in cmpqry.h.

Calling Sequence :  stat = qc_makeobj(struct cmpqry_struc *(*objqry),
  int nfields, char *fieldnames[]);

	objqry	Pointer at object query pointer.  It is assumed that
		the object query pointer in null at entry as this routine
		will allocate new storage for it.
	nfields	Number of field names in field name verification list. A
		value of 0 is used to indicate that field name verification
		is not to be performed.
	fieldnames Field name verification list.

Notes :

Change log :
000	23-MAR-90  AMF	Created
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#define K_MAXFLDNAMELEN 64

#if FIT_ANSI
int qc_makeobj(struct qc_s_cmpqry *(*objqry), int nfields,
  char *fieldnames[])
#else
int qc_makeobj(objqry, nfields, fieldnames)
  struct qc_s_cmpqry *(*objqry);
  int nfields;
  char *fieldnames[];
#endif
{

  struct qc_s_termentry *termptr;
  int *lenptr, *offsetptr;
  char *textptr;
  struct qc_s_objentry *intlogptr;
  int nfieldsfound;
  int fldidx,i,stat, fldlen;
  struct qc_s_objentry *(*qrylogic), *(*outlogptr);
  size_t vlen;
  char tempstr[K_MAXFLDNAMELEN+1], *fldptr;

  if (qc_nobjentries == 0 || qc_toplogic || qc_nerrs > 0)
    return (0);

  *objqry = (struct qc_s_cmpqry *) malloc(sizeof(*(*objqry)));
  if (!*objqry) goto ALLOCERR;

  (*objqry)->nterms = qc_nterms;  	/* set number of terms */
  (*objqry)->terms_L = qc_textsize + qc_nterms;	/* Length of term buffer */
  (*objqry)->nfields = qc_nfields;	/* set number of fields */
  (*objqry)->fields_L = qc_fieldsize + qc_nfields; /* Len of field name buf */
  (*objqry)->nlogic = 0; /* Set number of logic equations */
  (*objqry)->qrylogic = (int *) NULL;
  (*objqry)->nproximity = 0;
  (*objqry)->proximity = (struct qc_s_proximity *(*)) NULL;


  /* Fill in delimiters */
  for (i=0; i < QC_MAXDELIM; ++i)
    (*objqry)->delims[i] = qc_delims[i];

  /* Fill in reserved words */
  for (i = 0; i < QC_MAXRESERVED; ++i) {
    (*objqry)->reserved[i] = (char *) malloc(strlen(qc_reserved[i])+1);
    if (!(*objqry)->reserved[i]) goto ALLOCERR;
    strcpy((*objqry)->reserved[i],qc_reserved[i]);
  }

  /* Allocate storage for terms in object query */
  (*objqry)->qrytext = (char *) malloc((size_t) (qc_textsize+qc_nterms));
  if (!(*objqry)->qrytext) goto ALLOCERR;

  /* Allocate storage for term lengths */
  (*objqry)->termlen =
    (int *) calloc((size_t) qc_nterms, sizeof(*((*objqry)->termlen)));
  if (!(*objqry)->termlen) goto ALLOCERR;

  /* Allocate storage for term offsets */
  (*objqry)->offsets =
    (int *) calloc((size_t) qc_nterms, sizeof(*((*objqry)->offsets)));
  if (!(*objqry)->offsets) goto ALLOCERR;

  /* Allocate storage for field lengths */
  (*objqry)->fieldmap = (long *) calloc((size_t) qc_nterms,
     sizeof(*((*objqry)->fieldmap)));
  if (!(*objqry)->fieldmap) goto ALLOCERR;

  for (i=0; i < qc_nterms; ++i)
    *((*objqry)->fieldmap + i) = 0;

  /* Allocate storage for field names */
  (*objqry)->nfields = qc_nfields;
  if (qc_nfields) {
    (*objqry)->fields = (char *) malloc((size_t) (qc_fieldsize+qc_nfields));
    if (!(*objqry)->fields) goto ALLOCERR;

    (*objqry)->fieldlen =
      (int *) calloc((size_t) qc_nfields, sizeof(*((*objqry)->fieldlen)));
    if (!(*objqry)->fieldlen) goto ALLOCERR;

    (*objqry)->fldoffsets =
      (int *) calloc((size_t) qc_nfields, sizeof((*(*objqry)->fldoffsets)));
    if (!(*objqry)->fldoffsets) goto ALLOCERR;
  }

  else {
    (*objqry)->fields = (char *) NULL;
    (*objqry)->fieldlen = (int *) NULL;
    (*objqry)->fldoffsets = (int *) NULL;
  }

  /* Allocate storage for logic */
  qrylogic = 
    (struct qc_s_objentry*(*)) calloc((size_t) qc_nobjentries,
      sizeof(*qrylogic));
  if (!qrylogic) goto ALLOCERR;

  /* Fill in terms, term lengths, offsets and zero fieldmap */
  for (termptr = qc_firstterm, textptr = (*objqry)->qrytext,
       lenptr = (*objqry)->termlen, offsetptr = (*objqry)->offsets;
       termptr; termptr = termptr->next)
  {
    strncpy(textptr,termptr->token->value,(size_t) termptr->token->len);
    *(textptr+termptr->token->len) = 0;
    *offsetptr = ptr_dif(textptr,(*objqry)->qrytext);
    textptr += (termptr->token->len + 1);
    *lenptr = termptr->token->len;
    lenptr += 1;
    offsetptr += 1;
  }

  nfieldsfound = 0;
  fldidx = 0;

  /* Fill in logic */
  for (outlogptr = qrylogic, intlogptr = qc_firstobj;
      intlogptr; intlogptr = intlogptr->next) {

    *(outlogptr++) = intlogptr;

    if (intlogptr->token->type == QC_FIELD) {
      fldptr = (*objqry)->fields+fldidx;
      fldlen = intlogptr->token->len-1;

      if (fldlen > 1)
	strncpy(fldptr,intlogptr->token->value + 1, (size_t) fldlen);
      (*objqry)->fldoffsets[nfieldsfound] = fldidx;
      (*objqry)->fieldlen[nfieldsfound] = fldlen;
      fldptr[fldlen] = 0;
      fit_supcase(fldptr);

      if (nfields) {
	/* Verify field name is valid */
	for (stat=0, i = 0; i < nfields && !stat; ++i) {
	  vlen = strlen(fieldnames[i]);
	  strncpy(tempstr,fieldnames[i],(size_t) min(vlen,K_MAXFLDNAMELEN));
	  tempstr[min(vlen,K_MAXFLDNAMELEN)] = 0;
	  stat = !strcmp(fldptr,fit_supcase(tempstr));
	}
	if (!stat) qc_adderror(FIT_ERR_FIELD,intlogptr->token->offset);
      }

      fldidx += intlogptr->token->len-1;
      ++fldidx;
      ++nfieldsfound;
    }
  }

  /* return status after performing semantic processing functions */
  stat = qc_semantic(*objqry,qrylogic);
  free(qrylogic);
  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  stat = 0;

DONE:
  return(stat);
}
