/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_dealtemp					dealtemp.c

Function : Deallocates temporary work areas used by query compiler

Author : A. Fregly

Abstract :

Calling Sequence : qc_dealtemp();

Notes :

Change log :
000	23-MAR-90  AMF	Created.
001	07-MAY-91  AMF	Free token values.
002	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>

void qc_dealtemp() {

  struct qc_s_token *tokenptr;
  struct qc_s_logicentry *logicptr;
  struct qc_s_objentry *objptr;
  struct qc_s_termentry *termptr;
  int i;

  while (qc_firsttoken) {
    tokenptr = qc_firsttoken;
    if (tokenptr->value) free(tokenptr->value);
    qc_firsttoken = qc_firsttoken->next;
    free (tokenptr);
  }

  while (qc_toplogic) {
    logicptr = qc_toplogic;
    qc_toplogic = qc_toplogic->prev;
    free (logicptr);
  }

  while (qc_firstobj) {
    objptr = qc_firstobj;
    qc_firstobj = qc_firstobj->next;
    free (objptr);
  }

  while (qc_firstterm) {
    termptr = qc_firstterm;
    qc_firstterm = qc_firstterm->next;
    free(termptr);
  }

  for (i = 0; i < QC_MAXRESERVED; ++i)
    if (qc_reserved[i] != NULL) {
      free(qc_reserved[i]);
      qc_reserved[i] = NULL;
    }
}
