/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_packobj					packobj.c

Function : Packs object query contiguously into a buffer.

Author : A. Fregly

Abstract : 

Calling Sequence : stat = qc_packobj(struct qc_s_cmpqry *objqry, char *objbuf,
	int bufmax, int *retlen);

	stat	Returned status, 0 if an error occurred.
	objqry	Compiled query to be packed.
	objbuf	Buffer into which object query is stuffed.
	bufmax	Size of object buffer.
	retlen	Returned number of bytes of objbuf which were used.

Notes : 

Change log : 
000	16-APR-91  AMF	Created.
001	14-DEC-92  AMF	Compile under COHERENT.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_UNIX
#include <memory.h>
#else
#include <string.h>
#endif
#include <qc.h>

#if FIT_ANSI
void qc_packobj_add(char *(*dest), char *enddest, void *source, int sourcelen);
#else
void qc_packobj_add();
#endif

#if FIT_ANSI
int qc_packobj(struct qc_s_cmpqry *objqry, char *objbuf, int bufmax, 
  int *retlen)
#else
int qc_packobj(objqry, objbuf, bufmax, retlen)
  struct qc_s_cmpqry *objqry;
  char *objbuf; 
  int bufmax, *retlen;
#endif
{
  char *endbuf, *bufptr;
  int i;

  endbuf = objbuf + bufmax;
  bufptr = objbuf;

  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->nterms, 
    sizeof(objqry->nterms));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->terms_L, 
    sizeof(objqry->terms_L));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->nlogic,
    sizeof(objqry->nlogic));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->nfields,
    sizeof(objqry->nfields));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->fields_L,
    sizeof(objqry->fields_L));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->noperands, 
    sizeof(objqry->noperands));
  qc_packobj_add(&bufptr,endbuf,void_caste &objqry->nproximity,
    sizeof(objqry->nproximity));
  qc_packobj_add(&bufptr,endbuf,void_caste objqry->delims,QC_MAXDELIM);

  for (i=0; i < QC_MAXRESERVED; ++i)
    qc_packobj_add(&bufptr,endbuf,void_caste objqry->reserved[i],
      strlen(objqry->reserved[i])+1);

  qc_packobj_add(&bufptr,endbuf,void_caste objqry->offsets, 
    objqry->nterms * sizeof(*objqry->offsets));
  qc_packobj_add(&bufptr,endbuf,void_caste objqry->termlen, 
    objqry->nterms * sizeof(*objqry->termlen));
  qc_packobj_add(&bufptr,endbuf,void_caste objqry->fieldmap,
    objqry->nterms * sizeof(*objqry->fieldmap));
  qc_packobj_add(&bufptr,endbuf, void_caste objqry->qrytext, objqry->terms_L);

  if (objqry->nfields) {
    qc_packobj_add(&bufptr,endbuf,void_caste objqry->fldoffsets,
      objqry->nfields * sizeof(*objqry->fldoffsets));
    qc_packobj_add(&bufptr,endbuf,void_caste objqry->fieldlen,
      objqry->nfields * sizeof(*objqry->fieldlen));
    qc_packobj_add(&bufptr,endbuf, void_caste objqry->fields, objqry->fields_L);
  }

  for (i=0; i < objqry->nproximity; ++i) {
    qc_packobj_add(&bufptr,endbuf,void_caste &objqry->proximity[i]->termnum, 
      sizeof(objqry->proximity[i]->termnum));
    qc_packobj_add(&bufptr,endbuf,void_caste &objqry->proximity[i]->relation, 
      sizeof(objqry->proximity[i]->relation));
    qc_packobj_add(&bufptr,endbuf,void_caste &objqry->proximity[i]->distance, 
      sizeof(objqry->proximity[i]->distance));
    qc_packobj_add(&bufptr,endbuf,
      void_caste &objqry->proximity[i]->list0->nelements, 
      sizeof(objqry->proximity[i]->list0->nelements));
    qc_packobj_add(&bufptr,endbuf,
      void_caste &objqry->proximity[i]->list1->nelements, 
      sizeof(objqry->proximity[i]->list1->nelements));

    qc_packobj_add(&bufptr,endbuf,
      void_caste objqry->proximity[i]->list0->element,
      objqry->proximity[i]->list0->nelements * 
      sizeof(*objqry->proximity[i]->list0->element));

    qc_packobj_add(&bufptr,endbuf,
      void_caste objqry->proximity[i]->list1->element,
      objqry->proximity[i]->list1->nelements * 
      sizeof(*objqry->proximity[i]->list1->element));
  }

  qc_packobj_add(&bufptr,endbuf, void_caste objqry->qrylogic,
      objqry->nlogic * sizeof(*objqry->qrylogic));

  *retlen = bufptr - objbuf;
  return 1;
}

#if FIT_ANSI
void qc_packobj_add(char *(*dest), char *enddest, void *source, int sourcelen)
#else
void qc_packobj_add(dest, enddest, source, sourcelen)
  char *(*dest), *enddest; 
  char *source;
  int sourcelen;
#endif
{
  if (*dest + sourcelen <= enddest) {
    memmove(*dest, source, sourcelen);
    *dest += sourcelen;
  }
}
