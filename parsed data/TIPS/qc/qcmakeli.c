/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_makelist				    	makelist.c

Function : Adds threshhold list to compiled query.

Author : A. Fregly

Abstract : This functions will create a term list consisting of all
	of the terms which are in the query logic equation and fall between
	the supplied bounds.

Calling Sequence : stat = qc_makelist(struct qc_s_objentry *qrylogic, 
	int sidx, int eidx, int nterms, struct qc_s_termlist *(*list));

	qrylogic	Query logic which is being constructed.
	sidx		Index of starting term in logic equation of cmpqry.
	eidx		Index of ending term in logic equation of cmpqry.
	nterms		Number of terms which will be put in list.
	list		Pointer at term list structure pointer.  A
			term list structure is allocated and filled
			in. List should point at an available term list
			structure pointer.

Notes : 

Change log : 
000	09-DEC-90  AMF	Created.
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
int qc_makelist(struct qc_s_objentry *qrylogic[], int sidx, int eidx,
  int nterms,struct qc_s_termlist *(*list))
#else
int qc_makelist(qrylogic, sidx, eidx, nterms, list)
  struct qc_s_objentry *qrylogic[];
  int sidx, eidx, nterms;
  struct qc_s_termlist *(*list);
#endif
{

  int i, nfound;

  /* Allocate the threshhold group */
  *list = (struct qc_s_termlist *) malloc(sizeof(*(*list)));
  if (!*list) goto ALLOCERR;

  (*list)->element = (int *) calloc((size_t) nterms,
    sizeof(*((*list)->element)));
  if (!(*list)->element) goto ALLOCERR;

  /* Fill in the threshhold list */
  for (i=sidx, nfound=0; i <= eidx; ++i)
    if (qrylogic[i]->token->type == QC_THRESHHOLD)
      ++i;
    else if (qrylogic[i]->token->type >= 0)
      (*list)->element[nfound++] = qrylogic[i]->token->type;

  (*list)->nelements = nfound;

  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
