/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_addobj						addobj.c

Function : Adds next value to object query under construction.

Author : A. Fregly

Abstract : This routine will add the supplied value to the object query
	being constructed within the temporary query compiler data
	structures.

Calling Sequence : stat = qc_addobj(struct qc_s_token *token);

	token		Value to be added.

Notes :

Change log :
000	22-MAR-90  AMF	Created
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
int qc_addobj(struct qc_s_token *token)
#else
int qc_addobj(token)
  struct qc_s_token *token;
#endif
{
  if (!qc_firstobj) {
    /* Initialize object query data structures for first entry */
    qc_firstobj = (struct qc_s_objentry *) malloc(sizeof(*qc_firstobj));
    qc_lastobj = qc_firstobj;
    if (!qc_lastobj) goto ALLOCERR;
    qc_nobjentries = 0;
  }
  else if (qc_lastobj) {
    /* Add new object entry descriptor to end of list */
    qc_lastobj->next = (struct qc_s_objentry *) malloc(sizeof(*qc_firstobj));
    if (!qc_lastobj->next) goto ALLOCERR;
    qc_lastobj = qc_lastobj->next;
  }

  /* Insert token into newly created last entry */
  qc_lastobj->token = token;
  qc_lastobj->next = (struct qc_s_objentry *) NULL;
  qc_nobjentries += 1;
  return (1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
