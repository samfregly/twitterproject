/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_skipspce					skipspce.c

Function : Skips over blank characters

Author : A. Fregly

Abstract :

Calling Sequence : qc_skipspce(char *(*srcptr));

Notes :

Change log :
000  27-APR-90  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_ANSI
void qc_skipspce(char *(*srcptr))
#else
void qc_skipspce(srcptr)
  char *(*srcptr);
#endif
{
  while (*(*srcptr) && *(*srcptr) <= ' ') 
    *srcptr += 1;
}
