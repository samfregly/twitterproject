/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_isvldc						isvldc.c

Function : Checks term to see if it contains an VLDC characters and if it
	does, returns the offset of the first VLDC and the length plus one
	(the number of derivations of the term based on the first VLDC).

Author : A. Fregly

Abstract : 

Calling Sequence : isvldc = qc_isvldc(struct qc_s_token *token, 
  int *nderivations, int *vldcoffset);

  token		Token descriptor structure for token being checked.
  nderivations	Returned number of derivations of the token.
  vldcoffset	Returned offset of the first VLDC in the term.

Notes : 

Change log : 
000	22-DEC-90  AMF	Created.
001	19-NOV-91  AMF	Expand embedded ASWC and NSWCs.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>

#if FIT_ANSI
int qc_isvldc(struct qc_s_token *token, int *nderivations, int *vldcoffset)
#else
int qc_isvldc(token, nderivations, vldcoffset)
  struct qc_s_token *token;
  int *nderivations, *vldcoffset;
#endif
{
  int i,j, e, s, len, newtokenlen, replacelen;
  char *newval, fillchar;

  *nderivations = 1;
  *vldcoffset = 0;

  for (i = 0; i < token->len; ++i)
    if (token->value[i] == qc_delims[QC_OFF_QUOTE])
      i += 1;
    else {
      if (token->value[i] == qc_delims[QC_OFF_ASWC] || 
	  token->value[i] == qc_delims[QC_OFF_NSWC]) {
	/* See if embedded vldc */
	for (e=i+1; e < token->len; ++e)
	  if (token->value[e] == ' ') break;

	if (e != i + 1) {
	  /* Found embedded wild card, replace with variable length wild
	     card */
	  /* Find start of word */
	  for (s = i - 1; s >= 0; --s)
	    if (token->value[s] == ' ') break;
	  s = s + 1;
	  len = e - s;
	  replacelen = max(1, QC_MAXTERMWORDLEN - len + 1);
	  newtokenlen = token->len - 1 + replacelen;
	  newval = (char *) calloc(newtokenlen+1, sizeof(*newval));
	  if (newval != NULL) {
	    if (i) strncpy(newval, token->value, i);
	    if (token->value[i] == qc_delims[QC_OFF_ASWC])
	      fillchar = qc_delims[QC_OFF_AVLWC];
	    else
	      fillchar = qc_delims[QC_OFF_NVLWC];
	    for (j = i; replacelen; ++j, --replacelen)
	      newval[j] = fillchar;
	    strcat(newval, token->value+i+1);
	    free(token->value);
	    token->value = newval;
	    token->len = newtokenlen;
	  }
	}
      }
    
      if (token->value[i] == qc_delims[QC_OFF_AVLWC] || 
	  token->value[i] == qc_delims[QC_OFF_NVLWC] ||
	  token->value[i] == qc_delims[QC_OFF_AFLDC] ||
	  token->value[i] == qc_delims[QC_OFF_NFLDC]) {
        for (j=i+1; token->value[j] == token->value[i]; ++j)
	  ;
        *nderivations = j - i + 1;
        *vldcoffset = i;
        return(1);
      }
    }
  return(0);
}
