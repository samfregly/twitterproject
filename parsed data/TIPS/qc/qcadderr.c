/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_adderror					adderror.c

Function : Adds error to error list for current query.

Author : A. Fregly

Abstract : This routine maintains the error list for the query currently
	being compiled.  In order for this routine to work properly,
	qc_compile must initialize the global error list pointers
	qc_firsterr and qc_lasterr to null.  This routine will assume
	the offset of the error is as specified in the global qc_qryidx.
	If is also the responsibility of qc_compile to deallocate the error
	list when no longer needed.

Calling Sequence : qc_adderror(int errtype);

Notes :

Change log :
000	21-MAR-90  AMF	Created.
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
void qc_adderror (int errtype, int offset)
#else
void qc_adderror (errtype, offset)
  int errtype, offset;
#endif
{
  qc_nerrs += 1;

  /* Allocate new last error node */
  if (!qc_firsterr) {
    /* Error list is empty, initialize first entry */
    qc_firsterr = (struct qc_s_error *) malloc(sizeof(*qc_firsterr));
    qc_lasterr = qc_firsterr;
  }
  else {
    /* Add to end of error list */
    qc_lasterr->next = (struct qc_s_error *) malloc(sizeof(*qc_lasterr));
    qc_lasterr = qc_lasterr->next;
  }

  if (!qc_lasterr) {
    fit_status = FIT_FATAL_MEM;
    return;
  }

  /* Fill in the value of the error descriptor node */
  qc_lasterr->next = (struct qc_s_error *) NULL;
  qc_lasterr->errortype = errtype;
  qc_lasterr->idx = offset;
}
