/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_writeqry                                       writeqry.c

Function : Writes query file.

Author : A. Fregly

Abstract : This routine is used to write a query file.  The source text
 for the query and optionally the object query are written to the specified
 file.

Calling Sequence : stat = qc_writeqry(char *fname, char *qrytext,
  struct cmpqry *cmpqry);

  fname         File name for query file.
  qrytext       Source text for query.
  cmpqry        Pointer at object query created when query was compiled.
		cmpqry may be NULL.

Notes :

Change log :
000  05-MAY-90  AMF     Created
001     26-MAR-93  AMF  Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <qc.h>
#include <fit_stat.h>
#if FIT_DOS
#define WRITE "w"
#else
#define WRITE "w"
#endif

#if FIT_ANSI
int qc_writeqry(char *fname, char *qrytext, struct qc_s_cmpqry *cmpqry)
#else
int qc_writeqry(fname, qrytext, cmpqry)
  char *fname, *qrytext;
  struct qc_s_cmpqry *cmpqry;
#endif
{

  FILE *handle;
  int stat,i,j;
  char outfname[FIT_FULLFSPECLEN+1];

  stat = 0;
  fit_setext(fname,".qc",outfname);
  handle = fopen(outfname, WRITE);
  if (!handle) {
    fit_status = FIT_ERR_OPEN;
    goto DONE;
  }

  fprintf(handle, "Source Query: length, text\n");
  fprintf(handle,"%d %s\n",strlen(qrytext), qrytext);

  stat = 1;
  if (!cmpqry)
    stat = 1;
  else {
    fprintf(handle,"Object Query:\n");
    fprintf(handle,
      "Header: nterms, terms len, nlogic, nfields, fields len\n");
    fprintf(handle,"%d %d %d %d %d\n", cmpqry->nterms,
      cmpqry->terms_L, cmpqry->nlogic, cmpqry->nfields, cmpqry->fields_L);
    fprintf(handle,
      "Header continued: noperands, nproximities\n");
    fprintf(handle,"%d %d\n", cmpqry->noperands, cmpqry->nproximity);

    fprintf(handle,"Delimiters:\n");
    for (i=0; i < QC_MAXDELIM; ++i)
      fputc((int) cmpqry->delims[i], handle);
    fputc('\n',handle);

    fprintf(handle,"Reserved words:\n");
    for (i=0; i < QC_MAXRESERVED; ++i)
      fprintf(handle,"%s\n",cmpqry->reserved[i]);

    fprintf(handle,"Terms: offset, length, fieldmap, text\n");
    for (i=0; i < cmpqry->nterms; ++i)
      fprintf(handle,"%d %d %lx %s\n",*(cmpqry->offsets + i),
	*(cmpqry->termlen + i), *(cmpqry->fieldmap + i),
	cmpqry->qrytext + *(cmpqry->offsets + i));

    fprintf(handle,"Fields: offset, length, text\n");
    for (i=0; i < cmpqry->nfields; ++i)
      fprintf(handle,"%d %d %s\n", *(cmpqry->fldoffsets + i),
	*(cmpqry->fieldlen + i), cmpqry->fields + *(cmpqry->fldoffsets + i));

    if (cmpqry->nproximity) {
      fprintf(handle,
	"Proximities: term_number, type_mask, distance, nterms1, nterms2\n");
      fprintf(handle,"  operand_1_term_number_list\n");
      fprintf(handle,"  operand_2_term_number_list\n");
      for (i=0; i < cmpqry->nproximity; ++i) {
	fprintf(handle,"%d %x %ld %d %d\n  ",cmpqry->proximity[i]->termnum,
	  cmpqry->proximity[i]->relation,cmpqry->proximity[i]->distance,
	  cmpqry->proximity[i]->list0->nelements,
	  cmpqry->proximity[i]->list1->nelements);
	for (j=0; j < cmpqry->proximity[i]->list0->nelements; ++j)
	  fprintf(handle,"%d ",cmpqry->proximity[i]->list0->element[j]);
	fprintf(handle,"\n  ");
	for (j=0; j < cmpqry->proximity[i]->list1->nelements; ++j)
	  fprintf(handle,"%d ",cmpqry->proximity[i]->list1->element[j]);
	fprintf(handle,"\n");
      }
    }

    fprintf(handle,"Query logic: n >= 0 ==> term, n < 0 ==> logic operator\n");
    for (i=0; i < cmpqry->nlogic; ++i)
      fprintf(handle,"%d ",*(cmpqry->qrylogic + i));
    fprintf(handle,"\n");
  }

DONE:
  if (handle) fclose(handle);
  return(stat);
}
