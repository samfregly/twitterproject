/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_findlib					qcfindli.c

Function : Attempts to locate query object library entry.

Author : A. Fregly

Abstract : This function is used to find the entry number of a query in a
	query object library.

Calling Sequence : long entrynum = qc_findlib(struct drct_s_file *lib,
	char *queryname);

  lib		Object library handle returned by qc_openlib.
  queryname	Query name to look up in library. qc_findlib will expand
		queryname to a full path name prior to attempting the lookup.
  entrynum	Returned query entry number, or -1 if query is not located.

Notes:

Change log :
000  30-JUL-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>
#include <qc.h>

#if FIT_ANSI
long qc_findlib(struct drct_s_file *lib, char *queryname)
#else
long qc_findlib(lib, queryname)
  struct drct_s_file *lib;
  char *queryname;
#endif
{
  char expqryname[FIT_FULLFSPECLEN+1], inname[FIT_FULLFSPECLEN+3], *endname;
  long reclen;
  int explen, namelen;

  fit_expfspec(queryname, "", "", expqryname);
  explen = strlen(expqryname);
  if (!explen) return -1L;
  if (expqryname[explen-1] != '\n') {
    expqryname[explen++] = '\n';
    expqryname[explen] = 0;
  }

  drct_rewind(lib);

  inname[FIT_FULLFSPECLEN+1] = '\n';
  while (drct_rdseq(lib, inname, FIT_FULLFSPECLEN+1, &reclen)) {
    drct_cancelio(lib);

    if ((endname = strchr(inname, '\n')) != NULL) {
      namelen = endname - inname + 1;
      if (namelen == explen && !strncmp(expqryname, inname, namelen))
        return lib->currec;
    }
  }
  return -1L;
}
