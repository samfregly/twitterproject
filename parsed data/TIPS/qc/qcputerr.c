/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qc_puterr						puterr.c

Function : Writes query error message to specified output file.

Author :  A. Fregly

Abstract : An error message in the following format is written to the
	specified output stream.
		[text at point of error], error message
			 ^

Calling Sequence : qc_puterr(FILE *stream, char *srcqry, char *errmsg,
	int offset);

	stream	File to which error message should be written.
	srcqry	Source query
	errmsg	Error message to display
	offset	Offset in query at which error message occurred.


Notes :

Change log :
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_ANSI
void qc_puterr(FILE *stream, char *srcqry, char *errmsg, int offset)
#else
void qc_puterr(stream, srcqry, errmsg, offset)
  FILE *stream;
  char *srcqry, *errmsg;
  int offset;
#endif
{
  char *sptr,*eptr;
  if (ptr_lt(sptr = srcqry + offset - 10,srcqry)) sptr = srcqry;
  eptr = sptr + 19;
  fputc('[',stream);
  for (offset -= ptr_dif(sptr,srcqry); *sptr && ptr_le(sptr,eptr); ++sptr)
    if (*sptr >= ' ' && *sptr <= '~')
      fputc(*sptr,stream);
    else
      fputc(' ', stream);
  fprintf(stream,"], %s\n ",errmsg);
  for (;offset;--offset)
    fputc(' ',stream);
  fprintf(stream,"%s\n","^");
}

