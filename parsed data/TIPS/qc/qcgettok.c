/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : qc_gettoken					gettoken.c

Function : Gets next token string from query.

Author : A. Fregly

Abstract : This routine will return a pointer at a character string containing
	the next token value from the supplied source query.

Calling Sequence : nexttoken = qc_gettoken(char *(*srcptr));
	srcptr		Pointer at pointer which is loc of char to start
			parsing at.
	nexttoken	Returned character string containing token value.

Notes :

Change log :
000	20-MAR-90  AMF	Created
001	25-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <fit_stat.h>

#if FIT_ANSI
struct qc_s_token *qc_gettoken(char *(*srcptr), int *expected)
#else
struct qc_s_token *qc_gettoken(srcptr, expected)
  char *(*srcptr);
  int *expected;
#endif
{

  struct term {
    char *val;
    int len;
    struct term *next;
  };

  struct qc_s_token *token;
  char *sptr, *eptr, *term, *termptr, *valptr, *tptr;
  int src_L, eptr_L, tokentype, temptype, termlen, stat = 0, val_L, subtype;
  int offset,tempv;
  struct term *termlist, *termlist_end, *termlistptr;
  long nvalue;

  tokentype = QC_ERRTOKEN;

  while (tokentype == QC_ERRTOKEN) {
    term = NULL;

    /* Find start of next token */
    qc_skipspce(srcptr);

    /* Remember where it started */
    qc_qryidx = ptr_dif(*srcptr,qc_qrystart);
    offset = qc_qryidx;
    sptr = *srcptr;

    /* Determine length and token type */
    src_L = qc_tokenlen(*srcptr);
    tokentype = qc_tokntype(*srcptr,src_L,&subtype,offset);
    tempv = strlen(qc_qrystart) - 1;
    if (tokentype == QC_EOQ) offset = max(0,tempv);

    /* Find last of a series of terms so they can be combined into single term */
    if (tokentype == QC_TERM) {
      termlist = (struct term *) NULL;
      termlistptr = (struct term *) NULL;
      termlen = 0;
      stat = 1;

      /* Will use eptr, eptr_L, temptype for examining succeeding tokens */
      eptr = *srcptr;
      eptr_L = src_L;
      temptype = tokentype;

      while (temptype == QC_TERM) {

	valptr = eptr;
	val_L = eptr_L;
	*srcptr = eptr;
	src_L = eptr_L;

	if (val_L) {
	  /* make term entry in term list for current token */
	  termlistptr = (struct term *) malloc(sizeof(*termlistptr));

	  if (!termlistptr) {
	    offset = ptr_dif(eptr,qc_qrystart);
	    goto ALLOCERR;
	  }

	  termlistptr->val = valptr;
	  termlistptr->len = val_L;
	  termlistptr->next = (struct term *) NULL;

	  if (!termlist) {
	    termlist = termlistptr;
	    termlist_end = termlist;
	  }
	  else {
	    termlist_end->next = termlistptr;
	    termlist_end = termlistptr;
	  }

	  termlen += termlistptr->len + 1;
	}

	/* Get following token for succeeding check */
	eptr += eptr_L;
	qc_skipspce(&eptr);
	eptr_L = qc_tokenlen(eptr);
	temptype = qc_tokntype(eptr, eptr_L, &subtype, eptr - qc_qrystart);
      }

      if (termlen) {
	term = (char *) malloc((size_t) (termlen+1));
	if (!term) goto ALLOCERR;

	for (termptr = term; termlist;) {
	  if (term) {
	    strncpy(termptr, termlist->val, (size_t) termlist->len);
	    termptr += termlist->len;
	    *termptr = ' ';
	    termptr += 1;
	  }
	  termlistptr = termlist;
	  termlist = termlist->next;
	  free(termlistptr);
	}

	if (term != NULL) {
	  if (termptr != term) termptr -= 1;
	  *termptr = 0;
	}
      }
      else {
	qc_adderror(FIT_ERR_NULLTERM,offset);
	tokentype = QC_ERRTOKEN;
      }

      if (!stat) return (struct qc_s_token *) NULL;
    }

    /* Bump source pointer to point past just located token */
    *srcptr += src_L;

    if (tokentype == QC_PROXIMITY || 
	(tokentype == QC_THRESHHOLD && subtype != -1)) {
      /* Parse the numeric component of the operator */
      qc_skipspce(srcptr);
      tptr = *srcptr;
      while (*(*srcptr) && *(*srcptr) >= '0' && *(*srcptr) <= '9')
	*srcptr += 1;
      if (*(*srcptr - 1) < '0' || *(*srcptr - 1) > '9') {
	if (tokentype == QC_PROXIMITY)
	  nvalue = 0xFFFFFFFFL;
	else
	  nvalue = 1;
	*srcptr = tptr;
      }
      else {
	sscanf(tptr,"%ld",&nvalue);
	if (!nvalue)
	  if (tokentype == QC_PROXIMITY)
	    qc_adderror(FIT_ERR_PROXIMITY,tptr - qc_qrystart);
	  else {
	    qc_adderror(FIT_ERR_THRESHHOLD,tptr - qc_qrystart);
	  }
      }
    }
    else if (tokentype == QC_THRESHHOLD) {
      nvalue = -1;
    }
  }

  /* See if token is valid */
  if (!qc_validtok(tokentype, expected))
    /* Add error to error list when it is not */
    qc_tokenerr(tokentype,offset);

  /* Allocate a structure for storing the token */
  token = (struct qc_s_token *) malloc(sizeof(*token));
  if (!token) goto ALLOCERR;

  else {
    /* Fill in the tokens values */
    token->type = tokentype;
    token->subtype = subtype;
    token->offset = offset;
    if (tokentype == QC_PROXIMITY || tokentype == QC_THRESHHOLD)
      token->nvalue = nvalue;
    else
      token->nvalue = 0;

    if (tokentype != QC_TERM) {
      token->len = *srcptr - sptr;
      token->value = (char *) malloc((size_t) (token->len + 1));
      if (token->value == NULL)
	goto ALLOCERR;
      else {
	if (token->len > 0)
	  strncpy(token->value, sptr, (size_t) token->len);
	*(token->value + token->len) = 0;
	token->next = (struct qc_s_token *) NULL;
      }
    }
    else {
      token->value = term;
      token->len = (term != NULL) ? ptr_dif(termptr,term) : 0;
    }
  }

  if (!stat && term != NULL) free(term);

  return (token);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return (struct qc_s_token *) NULL;

}
