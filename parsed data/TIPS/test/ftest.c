#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_UNIX || FIT_COHERENT
#include <sys/types.h>
#include <sys/stat.h>
#define ATTRIB S_IFREG
#else
#define ATTRIB 0
#endif

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *(*argv);
#endif
{
  char curdir [FIT_FSPECLEN+1];
  char fspec[FIT_FSPECLEN+1];
  char *context;

  getcwd(curdir, sizeof(curdir) - 1);

  for (--argc, ++argv; argc; --argc, ++argv) {
    printf("%s\n", *argv);
    for (context = NULL, *fspec = 0; *fit_findfile(&context, *argv, ATTRIB,
	  fspec, curdir, fspec);)
      printf("...%s\n", fspec);
  }
  return 0;
}
