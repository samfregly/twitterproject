#include <stdio.h>
int main(argc, argv)
  int argc;
  char *argv[];
{
  int i, j;
  printf("     ");
  for (i=0; i < 16; ++i)
    printf("%2d ", i);
  putchar('\n');
  for (i=128; i < 256; i += 16) {
    printf("%d: ", i);
    for (j=i; j < i + 16; ++j)
      printf(" %c ", j);
    putchar('\n');
  }
}
