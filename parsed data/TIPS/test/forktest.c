#include <stdio.h>
#include <signal.h>
#include <errno.h>

static int parentPid;

void SIGhandler(int signal);

int main(int argc, char *argv[]) {
  parentPid=getpid();
  printf("Parent pid: %d\n",parentPid);
  int pid;

  signal(SIGALRM,SIGhandler);
  pid=fork();

  if (!pid) {
    printf("Child pid: %d\n",getpid());
    sleep(10);
    printf("child awoke, signaling parent\n");
    kill(parentPid,SIGALRM); 
    exit(errno);
  } 
  else {
    printf("Parent, returned pid from fork: %d\n",pid);
    getchar();
    kill(pid,SIGALRM);
    printf("Continuing\n");
  }
  return 0;
}

void SIGhandler(int signal) {
  int myPid;

  myPid=getpid();
  printf("process: %d received signal\n",myPid);
  if (myPid != parentPid) {
    printf("child killed by parent\n");
    exit(0);
  } 
  printf("parent received signal from child\n");
  return;
}
