/* Testing messaging protocol, server process */

#include <stdio.h>
#include <errno.h>
#include  <fitsydef.h>
#include  <tips.h>

#define BUFLEN 80
#define NAMEMAX 64

int main(int argc, char *(*argv))
{
  int server_sock,fd,retlen;
  char buf[BUFLEN+1],retbuf[BUFLEN+1];
  int msglen;
  int msgavail;
  int msgreceived;
  char s_port[NAMEMAX];

  printf("server started\n");
  if (--argc) {
    strncpy(s_port, argv[1], NAMEMAX);
    s_port[NAMEMAX] = (char) 0;
  }
  else {
    strcpy(s_port, "7999");
  }

  server_sock = tips_sockServer(s_port);
  if (server_sock < 0) {
    fprintf(stderr,"Error setting up connect socket: %s\n",s_port);
    exit(errno);
  }
  printf("Set up connect socket: %s\n",s_port);

  for (buf[0] = 0; strcmp(buf,"exit\n");) {
    if ((fd = tips_sockAccept(server_sock,10000)) >= 0) {
      printf("Accepted connection, fd: %d\n",fd);
      while ((retlen = tips_sockRead(fd,5000,buf,BUFLEN)) >= 0) {
        printf("Read from socket completed\n");
        if (retlen) {
          buf[retlen] = 0; 
          printf("read: %s",buf);
          if (!fgets(retbuf,BUFLEN,stdin)) {
            close(fd);  
          }
          else {
            printf("writing to client: %s",retbuf);
            write(fd,retbuf,strlen(retbuf));
            printf("write complete\n");
          }
        } 
        else {
          printf("read timed out\n");
        }
      }
      printf("received negative length message from client\n");
      close(fd);
    }
    else {
      printf("timed out waiting on client connection attempt\n");
    }
  } 
}
