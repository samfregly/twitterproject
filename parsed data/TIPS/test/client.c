/* Testing messaging protocol, server process */

#include <stdio.h>
#include <errno.h>
#include  <fitsydef.h>
#include  <tips.h>

#define BUFLEN 80
#define NAMEMAX 64

int main(int argc, char *(*argv)) {

  int fd,retlen;
  char buf[BUFLEN+1],respbuf[BUFLEN+1];
  int bytesread;
  char serverName[NAMEMAX+1],s_port[NAMEMAX+1];

  printf("client started\n");
  if (--argc > 0) {
    strncpy(serverName, argv[1], NAMEMAX);
  }
  else {
    strcpy(serverName,"localhost");
  }
  if (--argc > 0) {
    strncpy(s_port, argv[2], NAMEMAX);
	s_port[NAMEMAX] = (char) 0;
  }
  else {
    strcpy(s_port, "7999");
  }

  if ((fd = tips_sockConnect(serverName,s_port)) < 0) {
    printf("Unable to connect to: %s:%s\n",serverName,s_port);
    exit(0);
  }
  printf("Set up connection to server: %s:%s\n",serverName,s_port);

  for (buf[0] = 0; strcmp(buf,"exit\n");) {
    if (!fgets(buf,BUFLEN,stdin)) {
      close(fd);
      exit(1);
    }
    else if (strlen(buf) == 1) {
      close(fd);  
      printf("Closed socket\n");
      if ((fd = tips_sockConnect(serverName,s_port)) < 0) {
        printf("Unable to reconnect to: %s:%s\n",serverName,s_port);
        exit(0);
      }
      printf("Set up connection to server: %s:%s\n",serverName,s_port);
    }
    else {
      write(fd,buf,strlen(buf));
      if ((bytesread = read(fd,respbuf,BUFLEN-1)) > 0) {
        respbuf[bytesread] = 0;
        printf("read: %s",respbuf);
      }
      else {
        printf("read returned length of: %d\n",bytesread);
        printf("server appears to have closed connection, quitting\n");
        close(fd);
        exit(0); 
      }
    }
  } 
  return 0;
}
