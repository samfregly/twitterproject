/* Testing messaging protocol, server process */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
/* #include <fitsydef.h> */
#include <direct.h>
#include <io.h>
#include <windows.h>

#define MAXMSGSIZE 80
#define FILENAMEMAX 255
#define NAMEMAX 64

int main(int argc, char *(*argv))
{
  HANDLE hPipe;
  FILE *pipefile;
  enum PIPES { READ, WRITE }; /* Constants 0 and 1 for READ and WRITE */
  char msgbuf[MAXMSGSIZE+1];
  int msglen;
  char pipefilename[FILENAMEMAX+1];
  char pipename[NAMEMAX+1];
  int retlen;
  
  if (--argc) {
    strncpy(pipename, *(++argv), NAMEMAX);
	pipename[NAMEMAX] = (char) 0;
  }
  else {
    strcpy(pipename, "default");
  }
  strcpy(pipefilename, "\\\\.\\pipe\\");
  strcat(pipefilename, pipename);
  printf("pipe file name is %s\n", pipefilename);

  
  while (1) {
	putchar('>');
    gets(msgbuf + sizeof(int));
    if ((hPipe = CreateFile(pipefilename, GENERIC_WRITE, FILE_SHARE_WRITE, NULL,
	    OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM, (HANDLE) NULL)) == INVALID_HANDLE_VALUE) {
      DWORD errnum = GetLastError();
      printf("Error from CreateFile: %u\n", errnum);
	  exit(1);
    }
	printf("Created connection to pipe: %s\n", pipefilename);
    msglen = strlen(msgbuf+sizeof(int));
    memcpy(msgbuf, &msglen, sizeof(msglen));
    WriteFile(hPipe, msgbuf, msglen+sizeof(int), &retlen, NULL);
	CloseHandle(hPipe);
  }
  return 1;
}
