#include <stdio.h>
#include <fitsydef.h>
#include <win.h>
#include <men.h>

static char *cmdopts[] = {"File", "Exit",""};
static char *pulldownopts[] = {"First","Second",""};
FILE *fit_debug_handle;

int main(argc, argv)
  int argc;
  char *argv[];
{
  struct men_s_menu *cmdmenu, *pulldownmenu;
  WINDOW *dispwin, *cmdwin, *pulldownwin;
  long ropt, copt;
  int timedout, optselected;
  int keyin;

#if (FIT_DEBUG)
  fit_debug_handle = fopen("mentest.dbg","w");
#endif

  initscr();
  noecho();
  raw();

#if (FIT_DEBUG)
  fprintf(fit_debug_handle,"LINES: %d, COLS: %d\n",LINES,COLS);
  fflush(fit_debug_handle);
#endif
  dispwin = newwin(LINES-3, COLS, 1, 0);
  leaveok(dispwin, TRUE);
  idlok(dispwin, TRUE);
  scrollok(dispwin, FALSE);
  wattrset(dispwin, 0);
  win_fill(dispwin, 0, 0, 0, 0, ' ');
  wrefresh(dispwin);

  cmdwin = newwin(1, COLS, 0, 0);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: created command window\n");
  fflush(fit_debug_handle);
#endif
  wattrset(cmdwin, A_STANDOUT);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: set command window attributes\n");
  fflush(fit_debug_handle);
#endif
  scrollok(cmdwin, FALSE);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: turned off scroll in command window\n");
  fflush(fit_debug_handle);
#endif
  cmdmenu = men_init(cmdwin, MEN_ATTR_HORIZONTAL | MEN_ATTR_HORIZONTALWRAP,
	NULL, cmdopts, A_STANDOUT, 0, 0, 0, ACS_RARROW, ACS_DIAMOND, 1, 0);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: initialized command menu\n");
  fflush(fit_debug_handle);
#endif
  men_draw(cmdmenu);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: drew command menu\n");
  fflush(fit_debug_handle);
#endif
  men_put(cmdmenu, NULL);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "mentest: put command menu\n");
  fflush(fit_debug_handle);
#endif

  pulldownwin = newwin(4,10,0,0);
  if (pulldownwin)
  {
    scrollok(pulldownwin, FALSE);
    pulldownmenu = men_init(pulldownwin, MEN_ATTR_BOXED | MEN_ATTR_HOTKEYS |
	MEN_ATTR_INSTANT, NULL, pulldownopts, 0, A_STANDOUT, A_STANDOUT, 
	A_STANDOUT, ACS_RARROW, ACS_DIAMOND, 1, 0);
    if (!pulldownmenu) {
      printf("Error creating menu\n");
#if (FIT_DEBUG)
      fprintf(fit_debug_handle, "Error creating pulldown menu\n");
      fflush(fit_debug_handle);
#endif
      goto DONE;
    }
  }
  else {
      printf("Error creating puldown window\n");
#if (FIT_DEBUG)
    fprintf(fit_debug_handle, "Error creating pulldown window\n");
    fflush(fit_debug_handle);
#endif
    goto DONE;
  }

  men_overlay(pulldownmenu, cmdmenu, 0L, 0);
#if (FIT_DEBUG)
  fprintf(fit_debug_handle, "overlayed pulldownmenu on cmdmenu\n");
  fflush(fit_debug_handle);
#endif

  ropt=0;
  copt=0;
  while (ropt != 1) {
//wgetch(cmdmenu->win);
    men_select(cmdmenu, 0, &ropt, &optselected, &timedout);  
#if (FIT_DEBUG)
    fprintf(fit_debug_handle, "mentest: received user option for command window \
[%d]\n", ropt);
    fflush(fit_debug_handle);
#endif
    if (ropt == 0) {
      men_draw(pulldownmenu);
#if FIT_DEBUG
      fprintf(fit_debug_handle, "mentest, drew menu\n");
#endif
      men_put(pulldownmenu, dispwin);
#if FIT_DEBUG
      fprintf(fit_debug_handle, "mentest, put menu\n");
#endif
      for (optselected = 0; !optselected;) {
        men_select(pulldownmenu, 0, &copt, &optselected, &timedout);  
#if FIT_DEBUG
        fprintf(fit_debug_handle, "mentest, menu selection completed, option: %ld, optselected: %d\n",copt, optselected);
#endif
      }
      men_remove(pulldownmenu,dispwin);
    }
  }

DONE:
  endwin();
  return 0;
}
