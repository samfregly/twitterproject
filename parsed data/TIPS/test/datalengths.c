#include <stdio.h>
#include <stdlib.h>
#include <fitsydef.h>

int main(int argc, char *(*argv))
{
  short shortnum;
  int intnum;
  long longnum;
  printf("short: %d, int: %d, long %d\n", sizeof shortnum, sizeof intnum, sizeof longnum);
  printf("FIT_BITS_IN_INT: %d, FIT_INT_EXP: %d, FIT_BYTES_IN_INT: %d\n", FIT_BITS_IN_INT, FIT_INT_EXP, FIT_BYTES_IN_INT);
  printf("FIT_BITS_IN_LONG: %d, FIT_BYTES_IN_LONG: %d, FIT_LONG_EXP: %d\n", FIT_BITS_IN_LONG, FIT_BYTES_IN_LONG, FIT_LONG_EXP);
  FIT_WORD fitword;
  FIT_UWORD fituword;
  FIT_DWORD fitdword;
  FIT_UDWORD fitudword;
  FIT_BYTE fitbyte;
  FIT_UBYTE fitubyte;
  printf("FIT_WORD: %d, FIT_UWORD: %d, FIT_DWORD: %d, FIT_UDWORD: %d, FIT_BYTE: %d, FIT_UBYTE: %d\n", sizeof fitword, sizeof fituword, sizeof fitdword, sizeof fitudword, sizeof fitbyte, sizeof fitubyte);
  FIT_REG fitreg;
  printf("FIT_REG: %d, FIT_BITS_IN_REG: %d, FIT_REG_EXP: %d\n", sizeof fitreg, FIT_BITS_IN_REG, FIT_REG_EXP); 
  return 0;
}
