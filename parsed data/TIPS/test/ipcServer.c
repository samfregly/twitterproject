/* Testing messaging protocol, server process */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
/* #include <fitsydef.h> */
#include <direct.h>
#include <io.h>
#include <windows.h>


#define MAXMSGSIZE 80
#define FILENAMEMAX 255
#define NAMEMAX 32

char msgbuf[MAXMSGSIZE+1];
int msglen;
int msgavail;
int msgreceived;
char pipename[NAMEMAX+1];
char pipefilename[FILENAMEMAX+1];
char msgcopy[MAXMSGSIZE+1];
HANDLE hPipe;

void msghandler( void *ch );

int main(int argc, char *(*argv))
{

  printf("server started\n");
  if (--argc) {
    strncpy(pipename, ++*argv, NAMEMAX);
	pipename[NAMEMAX] = (char) 0;
  }
  else {
    strcpy(pipename, "default");
  }
  strcpy(pipefilename,"\\\\.\\pipe\\");
  strcat(pipefilename,pipename);

  hPipe = CreateNamedPipe( 
          pipefilename,             /* pipe name          */ 
          PIPE_ACCESS_INBOUND |     /* read access        */ 
		  FILE_FLAG_WRITE_THROUGH,  /* byte type pipe     */ 
          PIPE_TYPE_BYTE |          
          PIPE_READMODE_BYTE |      /* byte read mode     */ 
          PIPE_WAIT,                /* blocking mode      */ 
          1,						/* max. of 1 instances*/ 
          256,                      /* output buffer size */ 
          256,                      /* input buffer size  */ 
          60000,                    /* client time-out    */ 
          NULL);                    /* no security attr.  */ 
  if (hPipe == INVALID_HANDLE_VALUE) 
    exit(1); 
  printf("created pipe %s\n",pipefilename);

  /* Create thread for receiving messages */
  msgavail = 0;
  msgreceived = 1;
  
  /* Create thread to receive messages */
  _beginthread(msghandler, 0, NULL );
  printf("initiated thread and waiting for first message\n");

  for (msgcopy[0] = (char) 0; strcmp(msgcopy, "exit");) {
	if (msgavail) {   
      msgavail = 0;
      strncpy(msgcopy, msgbuf, MAXMSGSIZE);
      msgcopy[MAXMSGSIZE] = (char) 0;
      msgreceived = 1;
      printf("%s\n", msgbuf);
    }
    _sleep(1);
  }
  
  return 1;
}

void msghandler(void *ch)
{
  int inlen;
  printf("msghandler initiated\n");
  
  while (1) {
    /* Wait for message buffer to be freed */
    printf("Waiting for last message to be processed\n");
    while (!msgreceived) _sleep(1);
    printf("Initiating next pipe read\n");
    if (ConnectNamedPipe(hPipe, NULL)) {
     printf("Connection request received\n");
       /* read next message */
      if (ReadFile(hPipe,&msglen,sizeof(msglen),&inlen,NULL)) {
        printf("Next message length: %d\n", msglen);
        if (ReadFile(hPipe, msgbuf, msglen, &inlen, NULL)) {
		  printf("Received message from client, length: %d\n", msglen);
          msgreceived = 0;
		  msgavail=1;
		}
		else {
		  printf("Error getting message from client\n");
		}
	  }
	  else {
		printf("Error reading message length\n");
	  }
	  DisconnectNamedPipe(hPipe); 
	}
	else {
	  printf("Error getting connection\n");
	}
  }
}