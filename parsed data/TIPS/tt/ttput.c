/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_put						PUT.C

Function : Puts string to display

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_put(char *string)

Notes :

Change log :

******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>
#include <fit_stat.h>

#if FIT_VMS
#include <iodef.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_put(char *str)
#else
int tt_put(str)
  char *str;
#endif

{
  static char padstr[3] = {0,' ',0};

#if FIT_CURSES

  if (!tt_double)
    addstr(str);
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      addstr(padstr);
    }
  }
  refresh();

#else
#if FIT_VMS
#define NODEFAULT 1
  UDWORD ss,sys$qiow();
  struct iosb_struc {
    UWORD stat;
    WORD term_offset;
    WORD term;
    WORD term_size;
  };
  struct iosb_struc iosb;

  if (tt_chan == 0) {
    if (!(ss = tt_ttinit())) {
      fit_status = FIT_ERR_WRITE;
      return(0);
    }
  }

  if (!tt_double) {
    if (ss = ((UDWORD) 1 & sys$qiow(0, tt_chan, (UWORD) IO$_WRITEVBLK, &iosb,
        0,0,str,strlen(str),0,0,0,0)))
      if (iosb.stat) ss = iosb.stat;
    if (!(ss & (UDWORD) 1)) {
      fit_status = FIT_ERR_WRITE;
      return(0);
    }
  }

  else
    while (*str != 0) {
      padstr[0] = *str;
      if (ss = (1 & sys$qiow(0, tt_chan, (UWORD) IO$_WRITEVBLK, &iosb,
          0,0,padstr,2,0,0,0,0)))
	if (iosb.stat) ss = iosb.stat;
     if (!(ss & (UDWORD) 1)) {
	fit_status = FIT_ERR_WRITE;
	return(0);
      }
    }
#endif

#if FIT_TURBOC
#define NODEFAULT 1

  if (!tt_double)
    cputs(str);
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      cputs(padstr);
    }
  }
#endif

#ifndef NODEFAULT
  if (!tt_initialized) tt_ttinit();

  if (!tt_double)
    return(write(tt_fdout,str,strlen(str)));
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      write(tt_fdout,padstr,2);
    }
  }
#endif
#endif
  return (1);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_put						PUT.C

Function : Puts string to display

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_put(char *string)

Notes :

Change log :

******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>
#include <fit_stat.h>

#if FIT_VMS
#include <iodef.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_put(char *str)
#else
int tt_put(str)
  char *str;
#endif

{
  static char padstr[3] = {0,' ',0};

#if FIT_CURSES

  if (!tt_double)
    addstr(str);
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      addstr(padstr);
    }
  }
  refresh();

#else
#if FIT_VMS
#define NODEFAULT 1
  UDWORD ss,sys$qiow();
  struct iosb_struc {
    UWORD stat;
    WORD term_offset;
    WORD term;
    WORD term_size;
  };
  struct iosb_struc iosb;

  if (tt_chan == 0) {
    if (!(ss = tt_ttinit())) {
      fit_status = FIT_ERR_WRITE;
      return(0);
    }
  }

  if (!tt_double) {
    if (ss = ((UDWORD) 1 & sys$qiow(0, tt_chan, (UWORD) IO$_WRITEVBLK, &iosb,
        0,0,str,strlen(str),0,0,0,0)))
      if (iosb.stat) ss = iosb.stat;
    if (!(ss & (UDWORD) 1)) {
      fit_status = FIT_ERR_WRITE;
      return(0);
    }
  }

  else
    while (*str != 0) {
      padstr[0] = *str;
      if (ss = (1 & sys$qiow(0, tt_chan, (UWORD) IO$_WRITEVBLK, &iosb,
          0,0,padstr,2,0,0,0,0)))
	if (iosb.stat) ss = iosb.stat;
     if (!(ss & (UDWORD) 1)) {
	fit_status = FIT_ERR_WRITE;
	return(0);
      }
    }
#endif

#if FIT_TURBOC
#define NODEFAULT 1

  if (!tt_double)
    cputs(str);
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      cputs(padstr);
    }
  }
#endif

#ifndef NODEFAULT
  if (!tt_initialized) tt_ttinit();

  if (!tt_double)
    return(write(tt_fdout,str,strlen(str)));
  else {
    while (*str != 0) {
      padstr[0] = *(str++);
      write(tt_fdout,padstr,2);
    }
  }
#endif
#endif
  return (1);
}
