/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_clrlin						CLRLIN.C
 Function: Clears specified area(s) of spefied line
 Author: A. Fregly
 Description:
	This subroutine may be used to clear various part of a terminal
	line, including: the entire line, the part of the line preceeding
	the current column, or the part of the line following the current
	column.
 Parameters:
	AREA	I*2	If zero, the entire line is cleared.  If negative,
			the line up to the current cursor position is cleared.
			If positive, the part of the line following the
			current cursor position is cleared.
 Change Log:
000  25-MAY-88  AMF	Created.
001  20-NOV-89  AMF	Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC
#include <conio.h>
#include <alloc.h>
#endif

#include <tt.h>


#if FIT_ANSI
int tt_clrlin(int area)
#else
int tt_clrlin(area)
  int area;
#endif
{
#if FIT_CURSES
  char *buf;
  int cpos, y;

  if (area != 0)
    if (area > 0)
      clrtoeol();
    else {
      getyx(stdscr, y, cpos);
      if (cpos > 0) {
        buf = (char *) malloc(cpos);
	fit_sassign(buf,0,cpos-1," ",0,0);
	move(y,0);
	tt_putbuf(buf,0,cpos-1);
	free(buf);
      }
    }
  else {
    getyx(stdscr, y, cpos);
    move(y,0);
    clrtoeol();
  }
  refresh();
  return (1);
#else
#if FIT_TURBOC
  char *buf;
  int cpos;

  if (area != 0)
    if (area > 0)
      clreol();
    else {
      cpos = wherex();
      if (cpos > 1) {
        buf = malloc(cpos-1);
	fit_sassign(buf,0,cpos-2," ",0,0);
	tt_setpos(0,1);
	tt_putbuf(buf,0,cpos-2);
	free(buf);
      }
    }
  else {
    tt_setpos(0,1);
    clreol();
  }
  return (1);
#else
  static char clrcmd[] = {TT_K_ESC,'[',0,'K',0};

  if (area != 0)
    if (area > 0)
      clrcmd[2] = '0';
    else
      clrcmd[2] = '1';
  else
    clrcmd[2] = '2';

  return tt_put(clrcmd);
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_clrlin						CLRLIN.C
 Function: Clears specified area(s) of spefied line
 Author: A. Fregly
 Description:
	This subroutine may be used to clear various part of a terminal
	line, including: the entire line, the part of the line preceeding
	the current column, or the part of the line following the current
	column.
 Parameters:
	AREA	I*2	If zero, the entire line is cleared.  If negative,
			the line up to the current cursor position is cleared.
			If positive, the part of the line following the
			current cursor position is cleared.
 Change Log:
000  25-MAY-88  AMF	Created.
001  20-NOV-89  AMF	Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC
#include <conio.h>
#include <alloc.h>
#endif

#include <tt.h>


#if FIT_ANSI
int tt_clrlin(int area)
#else
int tt_clrlin(area)
  int area;
#endif
{
#if FIT_CURSES
  char *buf;
  int cpos, y;

  if (area != 0)
    if (area > 0)
      clrtoeol();
    else {
      getyx(stdscr, y, cpos);
      if (cpos > 0) {
        buf = (char *) malloc(cpos);
	fit_sassign(buf,0,cpos-1," ",0,0);
	move(y,0);
	tt_putbuf(buf,0,cpos-1);
	free(buf);
      }
    }
  else {
    getyx(stdscr, y, cpos);
    move(y,0);
    clrtoeol();
  }
  refresh();
  return (1);
#else
#if FIT_TURBOC
  char *buf;
  int cpos;

  if (area != 0)
    if (area > 0)
      clreol();
    else {
      cpos = wherex();
      if (cpos > 1) {
        buf = malloc(cpos-1);
	fit_sassign(buf,0,cpos-2," ",0,0);
	tt_setpos(0,1);
	tt_putbuf(buf,0,cpos-2);
	free(buf);
      }
    }
  else {
    tt_setpos(0,1);
    clreol();
  }
  return (1);
#else
  static char clrcmd[] = {TT_K_ESC,'[',0,'K',0};

  if (area != 0)
    if (area > 0)
      clrcmd[2] = '0';
    else
      clrcmd[2] = '1';
  else
    clrcmd[2] = '2';

  return tt_put(clrcmd);
#endif
#endif
}
