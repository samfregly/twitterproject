/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_scrlup						SCRLDOWN.C

 Function: Scrolls scrolling region down one line.

 Author: A. Fregly

 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_scrlup() {
#if FIT_CURSES
  int x, y;
  getyx(stdscr, y, x);
  move(0,0);
  deleteln();
  move(y,x);
  refresh();
  return 1;

#else
#if FIT_TURBOC
  struct text_info txstat;

  gettextinfo(&txstat);
  if (tt_srow == 0) {
    tt_srow = 1;
    tt_erow = txstat.screenheight;
  }

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,tt_srow,txstat.screenwidth,tt_erow);

  tt_setpos(1,1);
  delline();

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,1,txstat.screenwidth,txstat.screenheight);

  return (1);

#else
  static char NEXTLINE[] = {TT_K_ESC,'D',0};

  tt_savepos();
  tt_setpos(tt_erow,1);
  tt_put(NEXTLINE);
  tt_restorepos();
  return(1);
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_scrlup						SCRLDOWN.C

 Function: Scrolls scrolling region down one line.

 Author: A. Fregly

 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_scrlup() {
#if FIT_CURSES
  int x, y;
  getyx(stdscr, y, x);
  move(0,0);
  deleteln();
  move(y,x);
  refresh();
  return 1;

#else
#if FIT_TURBOC
  struct text_info txstat;

  gettextinfo(&txstat);
  if (tt_srow == 0) {
    tt_srow = 1;
    tt_erow = txstat.screenheight;
  }

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,tt_srow,txstat.screenwidth,tt_erow);

  tt_setpos(1,1);
  delline();

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,1,txstat.screenwidth,txstat.screenheight);

  return (1);

#else
  static char NEXTLINE[] = {TT_K_ESC,'D',0};

  tt_savepos();
  tt_setpos(tt_erow,1);
  tt_put(NEXTLINE);
  tt_restorepos();
  return(1);
#endif
#endif
}
