/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_parsekey                                    PARSEKEY.C

 Function: Returns value of key corresponding to STRING as defined in
	TT_DEF, -1 if invalid.

 Author: A. Fregly

 Calling Sequence:
	STRING  KEY specification, PF1,PF2,PF3,PF4,F0,...F9,
		UP,DOWN,LEFT,RIGHT,^A,...^Z,number,char value
		if single char.
 Change Log:
 000  01-DEC-89  AMF  Converted to C.
 001  27-JUL-94  AMF  Include stdlib.h
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <stdio.h>
#include <tt.h>

#if FIT_ANSI
int tt_parsekey(char *string)
#else
int tt_parsekey(string)
  char *string;
#endif
{

#if FIT_CURSES
#define MAXFUNCKEY 60 
  static int keynum[MAXFUNCKEY] = { TT_K_LEFT, TT_K_RIGHT, TT_K_UP,
    TT_K_DOWN, TT_K_F1, TT_K_F2, TT_K_F3,
    TT_K_F4, TT_K_K0, TT_K_K1, TT_K_K2, TT_K_K3, TT_K_K4, TT_K_K5,
    TT_K_K6, TT_K_K7, TT_K_K8, TT_K_K9, TT_K_KMINUS, TT_K_KCOMMA, TT_K_KPERIOD,
    TT_K_KENTER, TT_K_FIND, TT_K_INSERT, TT_K_REMOVE, TT_K_SELECT, TT_K_PGUP,
    TT_K_PGDN, TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10, TT_K_F11,
    TT_K_F12, TT_K_F13, TT_K_F14, TT_K_F15, TT_K_F16, TT_K_F17, TT_K_F18,
    TT_K_F19, TT_K_F20, TT_K_DELETE, TT_K_DO, TT_K_HELP, TT_K_HOME, TT_K_END,
    TT_K_CHGCASE, TT_K_DELBCK, TT_K_DELFWD, TT_K_EOL, TT_K_FWDWRD, TT_K_BCKWRD,
    TT_K_BOL, TT_K_DELREST, TT_K_REDRAWFLD, TT_K_DELBEFORE, 0 };

  static char *keyname[MAXFUNCKEY] ={"LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4", "K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9", "KMINUS",
    "KCOMMA", "KPERIOD", "KENTER", "FIND", "INSERT", "REMOVE", "SELECT", "PGUP",
    "PGDN", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14",
    "F15", "F16", "F17", "F18", "F19", "F20", "DELETE", "DO", "HELP", "HOME",
    "END", "CHGCASE", "DELBCK", "DELFWD", "EOL", "FWDWRD", "BCKWRD",
    "BOL", "DELREST", "REDRAWFLD", "DELBEFORE", "" };

  int i;

  if (*string == '^' && *(string+1) && !*(string+2))
    if (*(string+1) >= 'a')
      return (*(string+1) - 'a' + 1);
    else
      return (*(string+1) - 'A' + 1);
  else if (!*(string+1) && *string >= ' ' && *string <= '~')
    return (*string);
  else
    for (i=0; *(keyname[i]); ++i)
      if (strcmp(keyname[i],string) == 0)
	return keynum[i];

  return (-1);

#else
#if FIT_TURBOC

  static char *keyname[] = {"DEL","LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","INSERT","DELETE","HOME","END","PGUP","PGDN","REMOVE",
    "DO","F5","F6","F7","F8","F9","F10",""};
  int i;

  if (*string == '^' && *(string+1) && !*(string+2))
    if (*(string+1) >= 'a')
      return (*(string+1) - 'a' + 1);
    else
      return (*(string+1) - 'A' + 1);
  else if (!*(string+1) && *string >= ' ' && *string <= '~')
    return (*string);
  else
    for (i=0; *(keyname[i]); ++i)
#if !FIT_TURBOC
    if (strcmpi(keyname[i],string) == 0)
#else
    if (stricmp(keyname[i],string) == 0)
#endif
	return(i + 127);
  return (-1);
#else
#define K_KEYSMAX 22

  int bit, parsekey;

  static char *KEYS[K_KEYSMAX] = {"LEFT","RIGHT","UP","DOWN","PF1","PF2",
    "PF3","PF4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS",
    "KCOMMA","KPERIOD","KENTER"};

  
  parsekey = -1;

  if (strlen(string) == 1)
    /* Key is already specified as single char value, convert to integer */
    parsekey = *string;

  else if (*string == '^')
    /* Control character specified */
    parsekey = max(-1,toupper(string[1]) - 64);

  else if (*string >= '0' && *string <= '9') {
    /* ASCII numeric value of key specified */
    sscanf(string,"%d",&bit);
    if (bit <= 255) parsekey = bit;
  }

  else {
    for (bit=0; bit < K_KEYSMAX; ++bit)
      if (!strcmp(string,KEYS[bit])) break;
    if (bit < K_KEYSMAX) parsekey = bit + TT_K_LEFT;
  }

  return(parsekey);
#endif
#endif
}
/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_parsekey                                    PARSEKEY.C

 Function: Returns value of key corresponding to STRING as defined in
	TT_DEF, -1 if invalid.

 Author: A. Fregly

 Calling Sequence:
	STRING  KEY specification, PF1,PF2,PF3,PF4,F0,...F9,
		UP,DOWN,LEFT,RIGHT,^A,...^Z,number,char value
		if single char.
 Change Log:
 000  01-DEC-89  AMF  Converted to C.
 001  27-JUL-94  AMF  Include stdlib.h
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <stdio.h>
#include <tt.h>

#if FIT_ANSI
int tt_parsekey(char *string)
#else
int tt_parsekey(string)
  char *string;
#endif
{

#if FIT_CURSES
#define MAXFUNCKEY 60 
  static int keynum[MAXFUNCKEY] = { TT_K_LEFT, TT_K_RIGHT, TT_K_UP,
    TT_K_DOWN, TT_K_F1, TT_K_F2, TT_K_F3,
    TT_K_F4, TT_K_K0, TT_K_K1, TT_K_K2, TT_K_K3, TT_K_K4, TT_K_K5,
    TT_K_K6, TT_K_K7, TT_K_K8, TT_K_K9, TT_K_KMINUS, TT_K_KCOMMA, TT_K_KPERIOD,
    TT_K_KENTER, TT_K_FIND, TT_K_INSERT, TT_K_REMOVE, TT_K_SELECT, TT_K_PGUP,
    TT_K_PGDN, TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10, TT_K_F11,
    TT_K_F12, TT_K_F13, TT_K_F14, TT_K_F15, TT_K_F16, TT_K_F17, TT_K_F18,
    TT_K_F19, TT_K_F20, TT_K_DELETE, TT_K_DO, TT_K_HELP, TT_K_HOME, TT_K_END,
    TT_K_CHGCASE, TT_K_DELBCK, TT_K_DELFWD, TT_K_EOL, TT_K_FWDWRD, TT_K_BCKWRD,
    TT_K_BOL, TT_K_DELREST, TT_K_REDRAWFLD, TT_K_DELBEFORE, 0 };

  static char *keyname[MAXFUNCKEY] ={"LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4", "K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9", "KMINUS",
    "KCOMMA", "KPERIOD", "KENTER", "FIND", "INSERT", "REMOVE", "SELECT", "PGUP",
    "PGDN", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14",
    "F15", "F16", "F17", "F18", "F19", "F20", "DELETE", "DO", "HELP", "HOME",
    "END", "CHGCASE", "DELBCK", "DELFWD", "EOL", "FWDWRD", "BCKWRD",
    "BOL", "DELREST", "REDRAWFLD", "DELBEFORE", "" };

  int i;

  if (*string == '^' && *(string+1) && !*(string+2))
    if (*(string+1) >= 'a')
      return (*(string+1) - 'a' + 1);
    else
      return (*(string+1) - 'A' + 1);
  else if (!*(string+1) && *string >= ' ' && *string <= '~')
    return (*string);
  else
    for (i=0; *(keyname[i]); ++i)
      if (strcmp(keyname[i],string) == 0)
	return keynum[i];

  return (-1);

#else
#if FIT_TURBOC

  static char *keyname[] = {"DEL","LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","INSERT","DELETE","HOME","END","PGUP","PGDN","REMOVE",
    "DO","F5","F6","F7","F8","F9","F10",""};
  int i;

  if (*string == '^' && *(string+1) && !*(string+2))
    if (*(string+1) >= 'a')
      return (*(string+1) - 'a' + 1);
    else
      return (*(string+1) - 'A' + 1);
  else if (!*(string+1) && *string >= ' ' && *string <= '~')
    return (*string);
  else
    for (i=0; *(keyname[i]); ++i)
#if !FIT_TURBOC
    if (strcmpi(keyname[i],string) == 0)
#else
    if (stricmp(keyname[i],string) == 0)
#endif
	return(i + 127);
  return (-1);
#else
#define K_KEYSMAX 22

  int bit, parsekey;

  static char *KEYS[K_KEYSMAX] = {"LEFT","RIGHT","UP","DOWN","PF1","PF2",
    "PF3","PF4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS",
    "KCOMMA","KPERIOD","KENTER"};

  
  parsekey = -1;

  if (strlen(string) == 1)
    /* Key is already specified as single char value, convert to integer */
    parsekey = *string;

  else if (*string == '^')
    /* Control character specified */
    parsekey = max(-1,toupper(string[1]) - 64);

  else if (*string >= '0' && *string <= '9') {
    /* ASCII numeric value of key specified */
    sscanf(string,"%d",&bit);
    if (bit <= 255) parsekey = bit;
  }

  else {
    for (bit=0; bit < K_KEYSMAX; ++bit)
      if (!strcmp(string,KEYS[bit])) break;
    if (bit < K_KEYSMAX) parsekey = bit + TT_K_LEFT;
  }

  return(parsekey);
#endif
#endif
}
