/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************

  MODULE: tt_inkey						TT_INKEY.C

  FUNCTION: Read next key stroke, interpreting ANSI/VT100/VT52 function keys

  AUTHOR: Andrew M. Fregly

  DESCRIPTION:
    This routine will parse the next buffered input key stroke.  If the
    key stroke is a ANSI/VT100/VT200/VT52 escape sequence, the escape sequence
    is parsed and the corresponding key code found in tt.h is returned.
    If a failure occurs in parsing an escape sequence, the character at
    which the parse failed is returned.

  Change Log:
 000  12-JAN-88  AMF	Created
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_inkey(int *ch)
#else
int tt_inkey(ch)
  int *ch;
#endif
{

#if FIT_CURSES
  for (*ch = 0; !*ch && *ch != ERR;)
    *ch = getch();
  if (*ch == ERR)
    return 0;
  else
    return *ch;

#else
#if FIT_TURBOC
#define K_LOW 59
#define K_HIGH 83

  static unsigned char funckey[25] = {TT_K_F1, TT_K_F2, TT_K_F3, TT_K_F4,
    TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10,0,0,
    TT_K_HOME, TT_K_UP, TT_K_PGUP, 0, TT_K_LEFT, 0,
    TT_K_RIGHT, 0, TT_K_END, TT_K_DOWN, TT_K_PGDN, TT_K_INSERT, TT_K_DELFWD};

  *ch = 0;

  /* read next char only on startup */
  while (*ch == 0) {
    tt_get(ch);

    if (*ch == 0) {
      tt_get(ch);
      if (*ch != 0 && *ch >= K_LOW && *ch <= K_HIGH) *ch = funckey[*ch - K_LOW];
    }
  }
  return (1);
#else
  int ss;
  unsigned int c, kidx;
  int wc;

  /* Escape sequence terminator list, indexed by terminator char-64 */
  static unsigned char TT_INKEY_KEYTYPE[64] = {
     TT_K_UP,  TT_K_DOWN,TT_K_RIGHT,TT_K_LEFT,TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,TT_K_NULL,
     TT_K_CR,  TT_K_NULL,TT_K_NULL, TT_K_PF1, TT_K_PF2, TT_K_PF3,
     TT_K_PF4, TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,      ',',      '-',      '.', TT_K_NULL,      '0',
           '1',      '2',      '3',      '4',       '5',      '6',
           '7',      '8',      '9',TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL};

#define MAXVT200KEY  24
  static unsigned char VT200KEY[MAXVT200KEY] = {
    TT_K_FIND, TT_K_INSERT, TT_K_DELETE, TT_K_SELECT, TT_K_PGUP, TT_K_PGDN,
    TT_K_F6,       TT_K_F7,     TT_K_F8,     TT_K_F9,  TT_K_F10, TT_K_NULL,
    TT_K_F11,     TT_K_F12,    TT_K_F13,    TT_K_F14, TT_K_NULL, TT_K_F15,
    TT_K_F16,    TT_K_NULL,    TT_K_F17,    TT_K_F18,  TT_K_F19, TT_K_F20
  };

  c = TT_K_NULL;
  *ch = c;

  if (!(ss = tt_get(&c))) goto DONE;

  /* If char is not an escape sequence char, return its 'raw' value */
  if (c == TT_K_ESC || c == TT_K_CSI) {

    if (c == TT_K_ESC) {
      /* Started with ESC, look for CSI indicator */
      if (!(ss = tt_get(&c))) goto DONE;

      if (c < '1' || c > '6')
        /* Parsed if CSI indicator not found */
          if (c != '[' && c != 'O' && c != '?' && c != 'P' && c != 'Q' && 
            c != 'R' && c != 'S' && c != TT_K_CSI) goto DONE;
    }

    if (((c < 'P' || c > 'S') && (c < '1' || c > '6')) || c == TT_K_CSI )
      if (!(ss = tt_get(&c))) goto DONE;

    if (c > '0' && c < '7') {
      /* See if VT200 key */
      kidx = c - '1';
      if (!(ss = tt_get(&c))) goto DONE;
      if (c >= '0' && c <= '9') {
        c -= '0';
        kidx = (kidx+1) * 10 + c - 11;
        if (!(ss = tt_get(&c))) goto DONE;
      }
      if (c == '~' && kidx >= 0 && kidx < MAXVT200KEY)
        c = VT200KEY[kidx];
    }

    else {
      /* Lookup key which generated escape sequence based on terminator char. */
      wc = c - 65;
      if (wc >= 0 && wc <= 63) {
        c = TT_INKEY_KEYTYPE[wc];
      }
    }
  }

DONE:
  *ch = c;
  return ss;
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************

  MODULE: tt_inkey						TT_INKEY.C

  FUNCTION: Read next key stroke, interpreting ANSI/VT100/VT52 function keys

  AUTHOR: Andrew M. Fregly

  DESCRIPTION:
    This routine will parse the next buffered input key stroke.  If the
    key stroke is a ANSI/VT100/VT200/VT52 escape sequence, the escape sequence
    is parsed and the corresponding key code found in tt.h is returned.
    If a failure occurs in parsing an escape sequence, the character at
    which the parse failed is returned.

  Change Log:
 000  12-JAN-88  AMF	Created
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_inkey(int *ch)
#else
int tt_inkey(ch)
  int *ch;
#endif
{

#if FIT_CURSES
  for (*ch = 0; !*ch && *ch != ERR;)
    *ch = getch();
  if (*ch == ERR)
    return 0;
  else
    return *ch;

#else
#if FIT_TURBOC
#define K_LOW 59
#define K_HIGH 83

  static unsigned char funckey[25] = {TT_K_F1, TT_K_F2, TT_K_F3, TT_K_F4,
    TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10,0,0,
    TT_K_HOME, TT_K_UP, TT_K_PGUP, 0, TT_K_LEFT, 0,
    TT_K_RIGHT, 0, TT_K_END, TT_K_DOWN, TT_K_PGDN, TT_K_INSERT, TT_K_DELFWD};

  *ch = 0;

  /* read next char only on startup */
  while (*ch == 0) {
    tt_get(ch);

    if (*ch == 0) {
      tt_get(ch);
      if (*ch != 0 && *ch >= K_LOW && *ch <= K_HIGH) *ch = funckey[*ch - K_LOW];
    }
  }
  return (1);
#else
  int ss;
  unsigned int c, kidx;
  int wc;

  /* Escape sequence terminator list, indexed by terminator char-64 */
  static unsigned char TT_INKEY_KEYTYPE[64] = {
     TT_K_UP,  TT_K_DOWN,TT_K_RIGHT,TT_K_LEFT,TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,TT_K_NULL,
     TT_K_CR,  TT_K_NULL,TT_K_NULL, TT_K_PF1, TT_K_PF2, TT_K_PF3,
     TT_K_PF4, TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,      ',',      '-',      '.', TT_K_NULL,      '0',
           '1',      '2',      '3',      '4',       '5',      '6',
           '7',      '8',      '9',TT_K_NULL, TT_K_NULL,TT_K_NULL,
     TT_K_NULL,TT_K_NULL,TT_K_NULL,TT_K_NULL};

#define MAXVT200KEY  24
  static unsigned char VT200KEY[MAXVT200KEY] = {
    TT_K_FIND, TT_K_INSERT, TT_K_DELETE, TT_K_SELECT, TT_K_PGUP, TT_K_PGDN,
    TT_K_F6,       TT_K_F7,     TT_K_F8,     TT_K_F9,  TT_K_F10, TT_K_NULL,
    TT_K_F11,     TT_K_F12,    TT_K_F13,    TT_K_F14, TT_K_NULL, TT_K_F15,
    TT_K_F16,    TT_K_NULL,    TT_K_F17,    TT_K_F18,  TT_K_F19, TT_K_F20
  };

  c = TT_K_NULL;
  *ch = c;

  if (!(ss = tt_get(&c))) goto DONE;

  /* If char is not an escape sequence char, return its 'raw' value */
  if (c == TT_K_ESC || c == TT_K_CSI) {

    if (c == TT_K_ESC) {
      /* Started with ESC, look for CSI indicator */
      if (!(ss = tt_get(&c))) goto DONE;

      if (c < '1' || c > '6')
        /* Parsed if CSI indicator not found */
          if (c != '[' && c != 'O' && c != '?' && c != 'P' && c != 'Q' && 
            c != 'R' && c != 'S' && c != TT_K_CSI) goto DONE;
    }

    if (((c < 'P' || c > 'S') && (c < '1' || c > '6')) || c == TT_K_CSI )
      if (!(ss = tt_get(&c))) goto DONE;

    if (c > '0' && c < '7') {
      /* See if VT200 key */
      kidx = c - '1';
      if (!(ss = tt_get(&c))) goto DONE;
      if (c >= '0' && c <= '9') {
        c -= '0';
        kidx = (kidx+1) * 10 + c - 11;
        if (!(ss = tt_get(&c))) goto DONE;
      }
      if (c == '~' && kidx >= 0 && kidx < MAXVT200KEY)
        c = VT200KEY[kidx];
    }

    else {
      /* Lookup key which generated escape sequence based on terminator char. */
      wc = c - 65;
      if (wc >= 0 && wc <= 63) {
        c = TT_INKEY_KEYTYPE[wc];
      }
    }
  }

DONE:
  *ch = c;
  return ss;
#endif
#endif
}
