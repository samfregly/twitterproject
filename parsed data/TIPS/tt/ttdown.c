/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_down						down.c

Function : Moves cursor down one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_down();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if !FIT_TURBOC && !FIT_CURSES
char down[4] = {TT_K_ESC,'[','B',0};
#endif

int tt_down() {
#if FIT_CURSES
  int row,col;
  getyx(stdscr, row, col);
  if (row < tt_nrows-1)
    move(row+1, col);
  return 1;

#else
#if FIT_TURBOC
  int row,col;
  tt_getpos(&row,&col);
  return(tt_setpos(row+1,col));
#else
  return tt_put(down);
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_down						down.c

Function : Moves cursor down one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_down();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if !FIT_TURBOC && !FIT_CURSES
char down[4] = {TT_K_ESC,'[','B',0};
#endif

int tt_down() {
#if FIT_CURSES
  int row,col;
  getyx(stdscr, row, col);
  if (row < tt_nrows-1)
    move(row+1, col);
  return 1;

#else
#if FIT_TURBOC
  int row,col;
  tt_getpos(&row,&col);
  return(tt_setpos(row+1,col));
#else
  return tt_put(down);
#endif
#endif
}
