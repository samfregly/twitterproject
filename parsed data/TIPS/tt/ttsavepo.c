/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_savepos					savepos.c

Function : Saves current cursor position for possible restoration with
	tt_restorepos.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_savepos();

Notes : 

Change log : 
000	15-NOV-89  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_savepos()
{
#if FIT_CURSES
  getyx(stdscr, tt_savrow, tt_savcol);
  return 1;
#else
#if FIT_TURBOC
  tt_savrow = wherey();
  tt_savcol = wherex();
  return (1);
#else
  static char SAVEPOS[3] = {TT_K_ESC,'7',0};
  return(tt_put(SAVEPOS));
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_savepos					savepos.c

Function : Saves current cursor position for possible restoration with
	tt_restorepos.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_savepos();

Notes : 

Change log : 
000	15-NOV-89  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_savepos()
{
#if FIT_CURSES
  getyx(stdscr, tt_savrow, tt_savcol);
  return 1;
#else
#if FIT_TURBOC
  tt_savrow = wherey();
  tt_savcol = wherex();
  return (1);
#else
  static char SAVEPOS[3] = {TT_K_ESC,'7',0};
  return(tt_put(SAVEPOS));
#endif
#endif
}
