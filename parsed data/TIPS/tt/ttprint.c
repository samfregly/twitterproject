/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_print						PRINT.C

Function : Determines if a character is an ASCII printable character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_print(int key);

	key	ASCII value being checked.

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_print(int key)
#else
int tt_print(key)
  int key;
#endif
{
  return (key >= ' ' && key <='~');
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_print						PRINT.C

Function : Determines if a character is an ASCII printable character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_print(int key);

	key	ASCII value being checked.

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_print(int key)
#else
int tt_print(key)
  int key;
#endif
{
  return (key >= ' ' && key <='~');
}
