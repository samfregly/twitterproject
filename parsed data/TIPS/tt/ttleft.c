/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_left						LEFT.C

Function : Moves cursor left one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_left();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if !FIT_TURBOC && !FIT_CURSES
  char left[4] = {TT_K_ESC,'[','D',0};
#endif

int tt_left()
{
#if FIT_CURSES
  int x,y;
  getyx(stdscr,y,x);
  if (x) move(y,x-1);
  return 1;

#else
#if FIT_TURBOC
  return (tt_setpos(0,wherex()-1));
#else
  return(tt_put(left));
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_left						LEFT.C

Function : Moves cursor left one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_left();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if !FIT_TURBOC && !FIT_CURSES
  char left[4] = {TT_K_ESC,'[','D',0};
#endif

int tt_left()
{
#if FIT_CURSES
  int x,y;
  getyx(stdscr,y,x);
  if (x) move(y,x-1);
  return 1;

#else
#if FIT_TURBOC
  return (tt_setpos(0,wherex()-1));
#else
  return(tt_put(left));
#endif
#endif
}
