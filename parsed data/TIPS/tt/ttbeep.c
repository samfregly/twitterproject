/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tt_beep						beep.c

Function : Makes the terminal beep.

Author : A. Fregly

Abstract : 

Calling Sequence : stat = tt_beep();

	stat	Returned status code, 0 indicates error.

Notes : 

Change log :
000	1989?  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if !FIT_CURSES
char bell[2] = {7,0};
#endif

#if FIT_ANSI
int tt_beep(void)
#else
int tt_beep()
#endif
{
#if !FIT_CURSES
  return tt_put(bell);
#else
  beep();
  refresh();
#endif
}
