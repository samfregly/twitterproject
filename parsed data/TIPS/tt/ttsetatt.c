/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_setattr						TT_SETATTR.C

 Function: Sets terminal attributes

 Author: A. Fregly

 Calling Sequence: ss = tt_setattr(int attr);

	attr	Attribute mask specifying which attributes to set.
		Bit definitions for attributes are found in TT.H.

 Change Log:
 000  29-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_setattr(int attr)
#else
int tt_setattr(attr)
  int attr;
#endif
{

#if FIT_TURBOC
  struct text_info txstat;
  gettextinfo(&txstat);

  if (attr & (1 << TT_BIT_BOLD))
    highvideo();

  if (attr & ((1 << TT_BIT_REVERSE) | (1 << TT_BIT_UNDERLINE)))
    textattr((int) ((txstat.normattr & 7) << 4) ^
           (((txstat.normattr & 0x70) >> 4) & 7) ^
	   (txstat.attribute & 0x80));

  if (attr & (1 << TT_BIT_BLINK))
    textattr((int) (txstat.attribute ^ 0x80));

  if (attr & (1 << TT_BIT_DOUBLE))
    tt_double = 1;

  return (1);

#else
  static char BOLD[5] = {TT_K_ESC,'[','1','m',0};
  static char REVERSE[5] = {TT_K_ESC,'[','7','m',0};
  static char BLINK[5] = {TT_K_ESC,'[','5','m',0};
  static char UNDERLINE[5] = {TT_K_ESC,'[','4','m',0};
  static char DOUBLE[4] = {TT_K_ESC,'#','6',0};

  int ss;

  ss = 1;

  if (attr & (1 << TT_BIT_BOLD)) ss = tt_put(BOLD);
  if (ss && (attr & (1 << TT_BIT_REVERSE))) ss = tt_put(REVERSE);
  if (ss && (attr & (1 << TT_BIT_BLINK))) ss = tt_put(BLINK);
  if (ss && (attr & (1 << TT_BIT_UNDERLINE))) ss = tt_put(UNDERLINE);
  if (ss && (attr & (1 << TT_BIT_DOUBLE))) ss = tt_put(DOUBLE);

  return ss;
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_setattr						TT_SETATTR.C

 Function: Sets terminal attributes

 Author: A. Fregly

 Calling Sequence: ss = tt_setattr(int attr);

	attr	Attribute mask specifying which attributes to set.
		Bit definitions for attributes are found in TT.H.

 Change Log:
 000  29-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_setattr(int attr)
#else
int tt_setattr(attr)
  int attr;
#endif
{

#if FIT_TURBOC
  struct text_info txstat;
  gettextinfo(&txstat);

  if (attr & (1 << TT_BIT_BOLD))
    highvideo();

  if (attr & ((1 << TT_BIT_REVERSE) | (1 << TT_BIT_UNDERLINE)))
    textattr((int) ((txstat.normattr & 7) << 4) ^
           (((txstat.normattr & 0x70) >> 4) & 7) ^
	   (txstat.attribute & 0x80));

  if (attr & (1 << TT_BIT_BLINK))
    textattr((int) (txstat.attribute ^ 0x80));

  if (attr & (1 << TT_BIT_DOUBLE))
    tt_double = 1;

  return (1);

#else
  static char BOLD[5] = {TT_K_ESC,'[','1','m',0};
  static char REVERSE[5] = {TT_K_ESC,'[','7','m',0};
  static char BLINK[5] = {TT_K_ESC,'[','5','m',0};
  static char UNDERLINE[5] = {TT_K_ESC,'[','4','m',0};
  static char DOUBLE[4] = {TT_K_ESC,'#','6',0};

  int ss;

  ss = 1;

  if (attr & (1 << TT_BIT_BOLD)) ss = tt_put(BOLD);
  if (ss && (attr & (1 << TT_BIT_REVERSE))) ss = tt_put(REVERSE);
  if (ss && (attr & (1 << TT_BIT_BLINK))) ss = tt_put(BLINK);
  if (ss && (attr & (1 << TT_BIT_UNDERLINE))) ss = tt_put(UNDERLINE);
  if (ss && (attr & (1 << TT_BIT_DOUBLE))) ss = tt_put(DOUBLE);

  return ss;
#endif
}
