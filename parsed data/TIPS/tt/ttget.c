/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_get						get.c

Function : Get character from default input device without echoing.

Author : A. Fregly

Abstract :

Calling Sequence : stat = tt_get(ch)

	ch	*int	Returned next character
	stat	long	Returned status code

Notes :

Change log :
000	02-NOV-89  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_VMS
#include <iodef.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#endif

#include <tt.h>
#include <fit_stat.h>

#if FIT_ANSI
int tt_get(int *ch)
#else
int tt_get(ch)
  int *ch;
#endif
{

#if FIT_VMS
#define HAVECODE 1
  UDWORD ss,sys$qiow();
  struct iosb_struc {
    unsigned short stat;
    short term_offset;
    short term;
    short term_size;
  };
  struct iosb_struc iosb;
  DWORD termmask[2] = {0,0};

  if (tt_chan == 0) {
    if (!(ss=tt_ttinit())) {
      fit_status = FIT_ERR_READ;
      return(0);
    }
  }

  if (ss = sys$qiow(0L, tt_chan, IO$_READVBLK | IO$M_NOFILTR | IO$M_NOECHO,
      &iosb, 0L,0L,ch,1,0L,termmask,0L,0L))
    if (iosb.stat) ss = iosb.stat;

  if (!(ss & 1)) {
    fit_status = FIT_ERR_READ;
    return(0);
  }
  else
    return(1);
#else

#if FIT_CURSES
  *ch = getch();
  return (*ch != ERR);

#else
#if FIT_TURBOC
  *ch = getch();
  return (1);

#else
  char c;
  int stat;

  if (!tt_initialized) tt_ttinit();

  stat = read(tt_fdin, &c, 1);
  *ch = c;
  return(stat);
#endif
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_get						get.c

Function : Get character from default input device without echoing.

Author : A. Fregly

Abstract :

Calling Sequence : stat = tt_get(ch)

	ch	*int	Returned next character
	stat	long	Returned status code

Notes :

Change log :
000	02-NOV-89  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_VMS
#include <iodef.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#endif

#include <tt.h>
#include <fit_stat.h>

#if FIT_ANSI
int tt_get(int *ch)
#else
int tt_get(ch)
  int *ch;
#endif
{

#if FIT_VMS
#define HAVECODE 1
  UDWORD ss,sys$qiow();
  struct iosb_struc {
    unsigned short stat;
    short term_offset;
    short term;
    short term_size;
  };
  struct iosb_struc iosb;
  DWORD termmask[2] = {0,0};

  if (tt_chan == 0) {
    if (!(ss=tt_ttinit())) {
      fit_status = FIT_ERR_READ;
      return(0);
    }
  }

  if (ss = sys$qiow(0L, tt_chan, IO$_READVBLK | IO$M_NOFILTR | IO$M_NOECHO,
      &iosb, 0L,0L,ch,1,0L,termmask,0L,0L))
    if (iosb.stat) ss = iosb.stat;

  if (!(ss & 1)) {
    fit_status = FIT_ERR_READ;
    return(0);
  }
  else
    return(1);
#else

#if FIT_CURSES
  *ch = getch();
  return (*ch != ERR);

#else
#if FIT_TURBOC
  *ch = getch();
  return (1);

#else
  char c;
  int stat;

  if (!tt_initialized) tt_ttinit();

  stat = read(tt_fdin, &c, 1);
  *ch = c;
  return(stat);
#endif
#endif
#endif
}
