/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tt_ttreset			 		ttreset.c

Function : Used to reset terminal on program exit.

Author : A. Fregly

Abstract : This routine is used to reset the terminal on program exit
	for programs using the FIT Systems TT utilities.

Calling Sequence : tt_ttreset();

Notes : 

Change log :
000	06-APR-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_COHERENT
#include <sgtty.h>
#endif

int tt_ttreset() {
  tt_setscrl(1,tt_nrows+1);
  tt_noattr();
#if FIT_COHERENT || FIT_UNIX
  ioctl(tt_fdin,TIOCSETP,&tt_iosaveparams);
#endif

#if !FIT_TURBOC
  close(tt_fdin);
  close(tt_fdout);
#endif
  return 1;
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tt_ttreset			 		ttreset.c

Function : Used to reset terminal on program exit.

Author : A. Fregly

Abstract : This routine is used to reset the terminal on program exit
	for programs using the FIT Systems TT utilities.

Calling Sequence : tt_ttreset();

Notes : 

Change log :
000	06-APR-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_COHERENT
#include <sgtty.h>
#endif

int tt_ttreset() {
  tt_setscrl(1,tt_nrows+1);
  tt_noattr();
#if FIT_COHERENT || FIT_UNIX
  ioctl(tt_fdin,TIOCSETP,&tt_iosaveparams);
#endif

#if !FIT_TURBOC
  close(tt_fdin);
  close(tt_fdout);
#endif
  return 1;
}
