/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_getpos						GETPOS.C

 Function: Gets current cursor position

 Author: A. Fregly

 Calling Sequence : ss = tt_getpos(int *row, int *col);

	row	Returned row of cursor, 1 is top of screen.
	col	Returned col of cursor, 1 is left edge of screen.
 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#include <tt.h>

#if FIT_ANSI
int tt_getpos(int *row, int *col)
#else
int tt_getpos(row,col)
  int *row,*col;
#endif
{
#if FIT_TURBOC
  *row = wherey();
  *col = wherex();
#else
#if FIT_CURSES
  getyx(stdscr, (*row), (*col));
  *row += 1;
  *col += 1;
#endif
#endif
  return (1);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_getpos						GETPOS.C

 Function: Gets current cursor position

 Author: A. Fregly

 Calling Sequence : ss = tt_getpos(int *row, int *col);

	row	Returned row of cursor, 1 is top of screen.
	col	Returned col of cursor, 1 is left edge of screen.
 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#include <tt.h>

#if FIT_ANSI
int tt_getpos(int *row, int *col)
#else
int tt_getpos(row,col)
  int *row,*col;
#endif
{
#if FIT_TURBOC
  *row = wherey();
  *col = wherex();
#else
#if FIT_CURSES
  getyx(stdscr, (*row), (*col));
  *row += 1;
  *col += 1;
#endif
#endif
  return (1);
}
