/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_noattr						TT_NOATTR.C

 Function: Turns off display attributes (reverse, bold, blink, bright,...)

 Author: A. Fregly

 Change Log:
 000  29-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_noattr() {
#if FIT_CURSES
  attroff(0);
#else
#if FIT_TURBOC
  normvideo();
  tt_double = 0;
  return 1;
#else
  static char NOATTR[] = {TT_K_ESC,'[','0','m',0};
  tt_double = 0;
  return(tt_put(NOATTR));
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_noattr						TT_NOATTR.C

 Function: Turns off display attributes (reverse, bold, blink, bright,...)

 Author: A. Fregly

 Change Log:
 000  29-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_noattr() {
#if FIT_CURSES
  attroff(0);
#else
#if FIT_TURBOC
  normvideo();
  tt_double = 0;
  return 1;
#else
  static char NOATTR[] = {TT_K_ESC,'[','0','m',0};
  tt_double = 0;
  return(tt_put(NOATTR));
#endif
#endif
}
