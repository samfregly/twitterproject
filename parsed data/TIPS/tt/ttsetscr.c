/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_setscrl						SETSCRL.C

 Function: Set scrolling region on terminal

 Author: A. Fregly

 Calling Sequence: ss = tt_setscrl(int srow, int erow)

	SROW	Start row of scrolling region
	EROW	End row of scrolling region

 Change Log:
000  30-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if !FIT_TURBOC
  char setscrl[11] = {TT_K_ESC,'[',0,0,0,0,0,0,0,0,0};
#endif

#if FIT_ANSI
int tt_setscrl(int srow,int erow)
#else
int tt_setscrl(srow, erow)
  int srow, erow;
#endif
{

#if FIT_TURBOC
  tt_srow = srow;
  tt_erow = erow;
  return (1);

#else

  int off;

  off = 2;
  if (srow != 0) {
    sprintf(setscrl+off,"%d",srow);
    off = strlen(setscrl);
  }
  setscrl[off++] = ';';
  if (erow != 0) {
    sprintf(setscrl+off,"%d",erow);
    off = strlen(setscrl);
  }

  setscrl[off++] = 'r';
  setscrl[off++] = 0;

  tt_srow = srow;
  tt_erow = erow;
  return tt_put(setscrl);
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_setscrl						SETSCRL.C

 Function: Set scrolling region on terminal

 Author: A. Fregly

 Calling Sequence: ss = tt_setscrl(int srow, int erow)

	SROW	Start row of scrolling region
	EROW	End row of scrolling region

 Change Log:
000  30-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if !FIT_TURBOC
  char setscrl[11] = {TT_K_ESC,'[',0,0,0,0,0,0,0,0,0};
#endif

#if FIT_ANSI
int tt_setscrl(int srow,int erow)
#else
int tt_setscrl(srow, erow)
  int srow, erow;
#endif
{

#if FIT_TURBOC
  tt_srow = srow;
  tt_erow = erow;
  return (1);

#else

  int off;

  off = 2;
  if (srow != 0) {
    sprintf(setscrl+off,"%d",srow);
    off = strlen(setscrl);
  }
  setscrl[off++] = ';';
  if (erow != 0) {
    sprintf(setscrl+off,"%d",erow);
    off = strlen(setscrl);
  }

  setscrl[off++] = 'r';
  setscrl[off++] = 0;

  tt_srow = srow;
  tt_erow = erow;
  return tt_put(setscrl);
#endif
}
