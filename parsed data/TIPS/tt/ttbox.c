/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_box							BOX.C

 Function: Draws box on terminal

 Author: A. Fregly

 Calling Sequence: ss = tt_box(int trow,int lcol,int brow,int rcol);

	trow	Row of top of box
	lcol	Column of left side of box
	brow	Row of bottom of box
	rcol	Column of right side of box

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

char VLINE[] = "\0\0";

#if FIT_ANSI
int tt_box(int trow, int lcol, int brow, int rcol)
#else
int tt_box(trow, lcol, brow, rcol)
  int trow, lcol, brow, rcol;
#endif
{
  int j,i,curmode;
  char hline[135];

  VLINE[0] = TT_K_VLINE;

  curmode = tt_vmode;
  if (curmode != TT_K_LINEMODE)
    tt_linemode();

/* do top of box */
  hline[0] = TT_K_TOPLEFT;
  for (i=1; i < rcol - lcol; ++i)
    hline[i] = TT_K_HLINE;
  hline[i++] = TT_K_TOPRIGHT;
  hline[i] = 0;
  tt_setpos(trow,lcol);
  tt_put(hline);

/* do sides */
  for (j=trow+1; j <= brow-1; ++j) {
    tt_setpos(j,lcol);
    tt_put(VLINE);
    tt_setpos(j,rcol);
    tt_put(VLINE);
  }

/* do bottom */
  hline[0] = TT_K_BOTLEFT;
  hline[i-1] = TT_K_BOTRIGHT;
  tt_setpos(brow,lcol);
  tt_put(hline);

  if (tt_vmode != TT_K_TEXTMODE && curmode != TT_K_LINEMODE)
    tt_textmode();

  return (1);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_box							BOX.C

 Function: Draws box on terminal

 Author: A. Fregly

 Calling Sequence: ss = tt_box(int trow,int lcol,int brow,int rcol);

	trow	Row of top of box
	lcol	Column of left side of box
	brow	Row of bottom of box
	rcol	Column of right side of box

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

char VLINE[] = "\0\0";

#if FIT_ANSI
int tt_box(int trow, int lcol, int brow, int rcol)
#else
int tt_box(trow, lcol, brow, rcol)
  int trow, lcol, brow, rcol;
#endif
{
  int j,i,curmode;
  char hline[135];

  VLINE[0] = TT_K_VLINE;

  curmode = tt_vmode;
  if (curmode != TT_K_LINEMODE)
    tt_linemode();

/* do top of box */
  hline[0] = TT_K_TOPLEFT;
  for (i=1; i < rcol - lcol; ++i)
    hline[i] = TT_K_HLINE;
  hline[i++] = TT_K_TOPRIGHT;
  hline[i] = 0;
  tt_setpos(trow,lcol);
  tt_put(hline);

/* do sides */
  for (j=trow+1; j <= brow-1; ++j) {
    tt_setpos(j,lcol);
    tt_put(VLINE);
    tt_setpos(j,rcol);
    tt_put(VLINE);
  }

/* do bottom */
  hline[0] = TT_K_BOTLEFT;
  hline[i-1] = TT_K_BOTRIGHT;
  tt_setpos(brow,lcol);
  tt_put(hline);

  if (tt_vmode != TT_K_TEXTMODE && curmode != TT_K_LINEMODE)
    tt_textmode();

  return (1);
}
