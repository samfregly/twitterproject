/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_rvs								RVS.C

 Function: Sets display to reverse video.

 Author: A. Fregly

 Calling Sequence: ss = tt_rvs();

 Change Log:
 000  25-MAY-88  AMF	Created.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_rvs()
{
  return tt_setattr((int) (1 << TT_BIT_REVERSE));
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_rvs								RVS.C

 Function: Sets display to reverse video.

 Author: A. Fregly

 Calling Sequence: ss = tt_rvs();

 Change Log:
 000  25-MAY-88  AMF	Created.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_rvs()
{
  return tt_setattr((int) (1 << TT_BIT_REVERSE));
}
