/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_parseattr					PARSEATTR.C

 Function: Returns appropriate bit to set for attribute specified in string,
	-1 if invalid

 Author: A. Fregly

 Calling Sequence: bit = tt_parseattr(char *string);

	string		C*(*)	Atribute string, one of BOLD, UNDERLINE,
				REVERSE, BLINK, or DOUBLE.
 Change Log:
 000  21-JUL-88  AMF	Created
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_parseattr(char *string)
#else
int tt_parseattr(string)
  char *string;
#endif
{
  int i;

  static char *attributes[] = {"BOLD","UNDERLINE","REVERSE","BLINK","DOUBLE",
    ""};
  static int bitval[] = {TT_BIT_BOLD,TT_BIT_UNDERLINE,TT_BIT_REVERSE,
    TT_BIT_BLINK,TT_BIT_BOLD};

  for (i=0; *(attributes[i]); ++i)
#if !FIT_TURBOC
    if (strcmpi(attributes[i],string) == 0)
#else
    if (stricmp(attributes[i],string) == 0)
#endif
      return(bitval[i]);
  return(-1);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_parseattr					PARSEATTR.C

 Function: Returns appropriate bit to set for attribute specified in string,
	-1 if invalid

 Author: A. Fregly

 Calling Sequence: bit = tt_parseattr(char *string);

	string		C*(*)	Atribute string, one of BOLD, UNDERLINE,
				REVERSE, BLINK, or DOUBLE.
 Change Log:
 000  21-JUL-88  AMF	Created
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_parseattr(char *string)
#else
int tt_parseattr(string)
  char *string;
#endif
{
  int i;

  static char *attributes[] = {"BOLD","UNDERLINE","REVERSE","BLINK","DOUBLE",
    ""};
  static int bitval[] = {TT_BIT_BOLD,TT_BIT_UNDERLINE,TT_BIT_REVERSE,
    TT_BIT_BLINK,TT_BIT_BOLD};

  for (i=0; *(attributes[i]); ++i)
#if !FIT_TURBOC
    if (strcmpi(attributes[i],string) == 0)
#else
    if (stricmp(attributes[i],string) == 0)
#endif
      return(bitval[i]);
  return(-1);
}
