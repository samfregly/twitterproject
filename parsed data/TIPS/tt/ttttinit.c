/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_ttinit

Function : Initializes terminal handling package

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_ttinit();

Notes :

Change log :
000	18-FEB-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>

#if FIT_VMS
#include <iodef.h>
#endif
#if FIT_UNIX
#include <termio.h>
#endif

#if !FIT_TURBOC
#include <fcntl.h>
#else
#include <sys/fcntl.h>
#endif

#include <tt.h>
#include <fit_stat.h>

int tt_srow = 1;
#if FIT_TURBOC
int tt_erow = 25;
int tt_nrows = 25;
int tt_ncols = 80;
#else
int tt_erow = 24;
int tt_nrows = 24;
int tt_ncols = 80;
#endif

int tt_savrow, tt_savcol;
int tt_double = 0;

int tt_row, tt_col;

#if FIT_COHERENT | FIT_UNIX
struct sgttyb tt_ioparams,tt_iosaveparams;
#endif

short tt_chan;
int tt_vmode = -1;
int tt_kmode = -1;
int tt_initialized = 0;
int tt_fdin = 0;
int tt_fdout = 0;
int tt_hascolor = 0;

int tt_ttinit() {

  if (tt_initialized)
    return 1;

  tt_initialized = 1;

#if FIT_VMS
  struct fit_descriptor *tempstr;
  DWORD ss,sys$assign();

  if (tt_chan == 0) {
    if (!(1L & (ss=sys$assign(tempstr = fit_descript("TT:",3),&tt_chan,0,0)))) {
      fit_status = FIT_ERR_ASSIGN;
    }
    free(tempstr);
    return(ss & 1);
  }

  return 1;
#else
#if FIT_TURBOC
  return(1);
#else
#if FIT_CURSES
  initscr();
  tt_hascolor = has_colors();
  if (tt_hascolor) start_color();
  cbreak();
  nobreak();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  tt_erow = LINES;
  tt_nrows = LINES;
  tt_ncols = COLS;

#else
  tt_fdin = open("/dev/tty", O_RDONLY);
  tt_fdout = open("/dev/tty", O_WRONLY);

  ioctl(tt_fdin,TIOCGETP,&tt_ioparams);
  tt_iosaveparams = tt_ioparams;
  tt_ioparams.sg_flags |= RAW;
  tt_ioparams.sg_flags &= ~ECHO;
  ioctl(tt_fdin,TIOCSETP,&tt_ioparams);
};
  
#endif
#endif
  return 1;
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_ttinit

Function : Initializes terminal handling package

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_ttinit();

Notes :

Change log :
000	18-FEB-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>

#if FIT_VMS
#include <iodef.h>
#endif
#if FIT_UNIX
#include <termio.h>
#endif

#if !FIT_TURBOC
#include <fcntl.h>
#else
#include <sys/fcntl.h>
#endif

#include <tt.h>
#include <fit_stat.h>

int tt_srow = 1;
#if FIT_TURBOC
int tt_erow = 25;
int tt_nrows = 25;
int tt_ncols = 80;
#else
int tt_erow = 24;
int tt_nrows = 24;
int tt_ncols = 80;
#endif

int tt_savrow, tt_savcol;
int tt_double = 0;

int tt_row, tt_col;

#if FIT_COHERENT | FIT_UNIX
struct sgttyb tt_ioparams,tt_iosaveparams;
#endif

short tt_chan;
int tt_vmode = -1;
int tt_kmode = -1;
int tt_initialized = 0;
int tt_fdin = 0;
int tt_fdout = 0;
int tt_hascolor = 0;

int tt_ttinit() {

  if (tt_initialized)
    return 1;

  tt_initialized = 1;

#if FIT_VMS
  struct fit_descriptor *tempstr;
  DWORD ss,sys$assign();

  if (tt_chan == 0) {
    if (!(1L & (ss=sys$assign(tempstr = fit_descript("TT:",3),&tt_chan,0,0)))) {
      fit_status = FIT_ERR_ASSIGN;
    }
    free(tempstr);
    return(ss & 1);
  }

  return 1;
#else
#if FIT_TURBOC
  return(1);
#else
#if FIT_CURSES
  initscr();
  tt_hascolor = has_colors();
  if (tt_hascolor) start_color();
  cbreak();
  nobreak();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  tt_erow = LINES;
  tt_nrows = LINES;
  tt_ncols = COLS;

#else
  tt_fdin = open("/dev/tty", O_RDONLY);
  tt_fdout = open("/dev/tty", O_WRONLY);

  ioctl(tt_fdin,TIOCGETP,&tt_ioparams);
  tt_iosaveparams = tt_ioparams;
  tt_ioparams.sg_flags |= RAW;
  tt_ioparams.sg_flags &= ~ECHO;
  ioctl(tt_fdin,TIOCSETP,&tt_ioparams);
};
  
#endif
#endif
  return 1;
#endif
#endif
}
