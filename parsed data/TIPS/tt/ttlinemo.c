/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: TT_LINEMODE						LINEMODE.C

 Function: Makes terminal use line drawing character set

 Author: A. Fregly

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_linemode()
{
#if FIT_TURBOC || FIT_CURSES
  tt_vmode = TT_K_LINEMODE;
  return 1;
#else
  static char LINEMODE[] = {TT_K_ESC,'(','0',0};
  tt_vmode = TT_K_LINEMODE;
  return(tt_put(LINEMODE));
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: TT_LINEMODE						LINEMODE.C

 Function: Makes terminal use line drawing character set

 Author: A. Fregly

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_linemode()
{
#if FIT_TURBOC || FIT_CURSES
  tt_vmode = TT_K_LINEMODE;
  return 1;
#else
  static char LINEMODE[] = {TT_K_ESC,'(','0',0};
  tt_vmode = TT_K_LINEMODE;
  return(tt_put(LINEMODE));
#endif
}
