/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: TT_TEXTMODE						TEXTMODE.C

 Function: Makes terminal use line text character set

 Author: A. Fregly

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_textmode()
{
#if FIT_TURBOC
  tt_vmode = TT_K_TEXTMODE;
  return 1;
#else
  static char TEXTMODE[] = {TT_K_ESC,'(','B',0};
  tt_vmode = TT_K_TEXTMODE;
  return(tt_put(TEXTMODE));
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: TT_TEXTMODE						TEXTMODE.C

 Function: Makes terminal use line text character set

 Author: A. Fregly

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_textmode()
{
#if FIT_TURBOC
  tt_vmode = TT_K_TEXTMODE;
  return 1;
#else
  static char TEXTMODE[] = {TT_K_ESC,'(','B',0};
  tt_vmode = TT_K_TEXTMODE;
  return(tt_put(TEXTMODE));
#endif
}
