/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_clrscrn                                             TT_CLRSCRN.FOR

 Function: Clears specified area(s) of the terminal screen.

 Author: A. Fregly

 Description:
	This subroutine may be used to clear various part of the terminal
	screen including: the entire screen, from a specified start line to
	a specified end line, all lines up to a specified line and/or all lines
	following a specified line.

 Calling Sequence: ss = tt_clrscrn(int sptr, int eptr)

	sptr    Start line pointer.  If 0 and eptr is zero, entire
		screen is cleared.  If negative, screen is cleared up
		to but not including sptr.  If not zero and eptr is 
		not zero, lines between sptr and eptr inclusize are
		cleared.
	eptr    End line pointer.  If 0 and sptr is zero, entire screen
		is cleared.  If sptr is less than 1, lines on the
		screen following eptr are cleared.
 Change Log:
000  23-NOV-89  AMF  Implemented in C.
001  27-JUL-93  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#include <stdlib.h>
#endif

#include <tt.h>

#if !FIT_TURBOC && !FIT_CURSES
static char clrcmd[] = {TT_K_ESC,'[',0,'J',0};
#endif

#if FIT_ANSI
int tt_clrscrn(int sptr, int eptr)
#else
int tt_clrscrn(sptr, eptr)
  int sptr, eptr;
#endif
{
#if FIT_CURSES
  int i, nrows, ncols;

  if (sptr > 0) /* Clear between sptr and eptr inclusive */
    for (i=sptr; i <= (int) max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);

    }
  else if (sptr < 0) { /* clear before sptr and possibly after eptr */
    
    for (i=1, sptr=abs(sptr); i < sptr; ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
    if (eptr > 0) { /* clear after eptr */
      getdim(stdscr, &nrows, &ncols); 
      for (i=eptr+1; i <= nrows; i++) {
	tt_setpos(i,1);
	tt_clrlin(0);
      }
    }
  }
  else if (eptr > 0) { /* clear after eptr */
    getdim(stdscr, &nrows, &ncols);
    for (i=eptr+1; i <= nrows; i++) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
  }
  else /* clear entire screen */
    clear();

  refresh();
  return(1);

#else
#if FIT_TURBOC
  int i;
  struct text_info txstat;

  if (sptr > 0) /* Clear between sptr and eptr inclusive */
    for (i=sptr; i <= (int) max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);

    }
  else if (sptr < 0) { /* clear before sptr and possibly after eptr */
    
    for (i=1, sptr=abs(sptr); i < sptr; ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
    if (eptr > 0) { /* clear after eptr */
      gettextinfo(&txstat);
      for (i=eptr+1; i <= txstat.winbottom; i++) {
	tt_setpos(i,1);
	tt_clrlin(0);
      }
    }
  }
  else if (eptr > 0) { /* clear after eptr */
    gettextinfo(&txstat);
    for (i=eptr+1; i <= txstat.winbottom; i++) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
  }
  else /* clear entire screen */
    clrscr();

  return(1);

#else
  int i;

  if (sptr > 0)
    for (i=sptr; i <= max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }

  else if (sptr != 0) {
    tt_setpos(abs(sptr),1);
    clrcmd[2] = 'J';
    tt_put(clrcmd);
    if (eptr > 0) {
      tt_setpos(eptr+1,1);
      clrcmd[2] = '0';
      tt_put(clrcmd);
    }
  }

  else if (eptr > 0) {
     tt_setpos(eptr+1,1);
     clrcmd[2] = '0';
     tt_put(clrcmd);
  }

  else {
    clrcmd[2] = '2';
    tt_put(clrcmd);
  }
  return(1);
#endif
#endif
}
/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_clrscrn                                             TT_CLRSCRN.FOR

 Function: Clears specified area(s) of the terminal screen.

 Author: A. Fregly

 Description:
	This subroutine may be used to clear various part of the terminal
	screen including: the entire screen, from a specified start line to
	a specified end line, all lines up to a specified line and/or all lines
	following a specified line.

 Calling Sequence: ss = tt_clrscrn(int sptr, int eptr)

	sptr    Start line pointer.  If 0 and eptr is zero, entire
		screen is cleared.  If negative, screen is cleared up
		to but not including sptr.  If not zero and eptr is 
		not zero, lines between sptr and eptr inclusize are
		cleared.
	eptr    End line pointer.  If 0 and sptr is zero, entire screen
		is cleared.  If sptr is less than 1, lines on the
		screen following eptr are cleared.
 Change Log:
000  23-NOV-89  AMF  Implemented in C.
001  27-JUL-93  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_TURBOC
#include <conio.h>
#include <stdlib.h>
#endif

#include <tt.h>

#if !FIT_TURBOC && !FIT_CURSES
static char clrcmd[] = {TT_K_ESC,'[',0,'J',0};
#endif

#if FIT_ANSI
int tt_clrscrn(int sptr, int eptr)
#else
int tt_clrscrn(sptr, eptr)
  int sptr, eptr;
#endif
{
#if FIT_CURSES
  int i, nrows, ncols;

  if (sptr > 0) /* Clear between sptr and eptr inclusive */
    for (i=sptr; i <= (int) max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);

    }
  else if (sptr < 0) { /* clear before sptr and possibly after eptr */
    
    for (i=1, sptr=abs(sptr); i < sptr; ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
    if (eptr > 0) { /* clear after eptr */
      getdim(stdscr, &nrows, &ncols); 
      for (i=eptr+1; i <= nrows; i++) {
	tt_setpos(i,1);
	tt_clrlin(0);
      }
    }
  }
  else if (eptr > 0) { /* clear after eptr */
    getdim(stdscr, &nrows, &ncols);
    for (i=eptr+1; i <= nrows; i++) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
  }
  else /* clear entire screen */
    clear();

  refresh();
  return(1);

#else
#if FIT_TURBOC
  int i;
  struct text_info txstat;

  if (sptr > 0) /* Clear between sptr and eptr inclusive */
    for (i=sptr; i <= (int) max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);

    }
  else if (sptr < 0) { /* clear before sptr and possibly after eptr */
    
    for (i=1, sptr=abs(sptr); i < sptr; ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
    if (eptr > 0) { /* clear after eptr */
      gettextinfo(&txstat);
      for (i=eptr+1; i <= txstat.winbottom; i++) {
	tt_setpos(i,1);
	tt_clrlin(0);
      }
    }
  }
  else if (eptr > 0) { /* clear after eptr */
    gettextinfo(&txstat);
    for (i=eptr+1; i <= txstat.winbottom; i++) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }
  }
  else /* clear entire screen */
    clrscr();

  return(1);

#else
  int i;

  if (sptr > 0)
    for (i=sptr; i <= max(sptr,eptr); ++i) {
      tt_setpos(i,1);
      tt_clrlin(0);
    }

  else if (sptr != 0) {
    tt_setpos(abs(sptr),1);
    clrcmd[2] = 'J';
    tt_put(clrcmd);
    if (eptr > 0) {
      tt_setpos(eptr+1,1);
      clrcmd[2] = '0';
      tt_put(clrcmd);
    }
  }

  else if (eptr > 0) {
     tt_setpos(eptr+1,1);
     clrcmd[2] = '0';
     tt_put(clrcmd);
  }

  else {
    clrcmd[2] = '2';
    tt_put(clrcmd);
  }
  return(1);
#endif
#endif
}
