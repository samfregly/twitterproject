/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_putbuf						putbuf.c

Function : Writes specified characters from buffer to screen.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_putbuf(char *buf, int s, int e);

	buf	Pointer at buffer
	s	Offset of first character to be written
	e	Offset of last character to be written
Notes : 

Change log :
000	15-NOV-89  AMF	Creaed.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tt.h>
#include <fit_stat.h>

#if FIT_ANSI
int tt_putbuf(char *buf, int s, int e)
#else
int tt_putbuf(buf, s, e)
  char *buf;
  int s, e;
#endif
{

  char *outbuf;
  int ss;

  if ((outbuf = (char *) malloc(e-s+2)) != NULL) {
    fit_sassign(outbuf,0,e-s,buf,s,e);
    *(outbuf+e-s+1) = 0;
    ss = tt_put(outbuf);
    free(outbuf);
  }
  else {
    fit_status = FIT_FATAL_MEM;
    ss = 0;
  }

  return (ss);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_putbuf						putbuf.c

Function : Writes specified characters from buffer to screen.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_putbuf(char *buf, int s, int e);

	buf	Pointer at buffer
	s	Offset of first character to be written
	e	Offset of last character to be written
Notes : 

Change log :
000	15-NOV-89  AMF	Creaed.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tt.h>
#include <fit_stat.h>

#if FIT_ANSI
int tt_putbuf(char *buf, int s, int e)
#else
int tt_putbuf(buf, s, e)
  char *buf;
  int s, e;
#endif
{

  char *outbuf;
  int ss;

  if ((outbuf = (char *) malloc(e-s+2)) != NULL) {
    fit_sassign(outbuf,0,e-s,buf,s,e);
    *(outbuf+e-s+1) = 0;
    ss = tt_put(outbuf);
    free(outbuf);
  }
  else {
    fit_status = FIT_FATAL_MEM;
    ss = 0;
  }

  return (ss);
}
