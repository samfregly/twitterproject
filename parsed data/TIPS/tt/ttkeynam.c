/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
 Module: tt_keyname					KEYNAME.C

 Function: Returns name of key corresponding to "key" as defined in
	TT.H, -1 if invalid.

 Author: A. Fregly

 Calling Sequence: ss = tt_keyname(int key, char *keyname, int *klen)

	key	key value
	keyname	Returned key name, length of keyname should be at lease 10
		characters.
	klen	Returned length of key name.

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tt.h>

#if FIT_ANSI
void tt_keyname(int key, char *keyname, int *klen)
#else
void tt_keyname(key, keyname, klen)
  int key;
  char *keyname;
  int *klen;
#endif
{
#if FIT_CURSES
#define MAXFUNCKEY 59
  static int keynums[MAXFUNCKEY] = { TT_K_LEFT, TT_K_RIGHT, TT_K_UP,
    TT_K_DOWN, TT_K_F1, TT_K_F2, TT_K_F3,
    TT_K_F4, TT_K_K0, TT_K_K1, TT_K_K2, TT_K_K3, TT_K_K4, TT_K_K5,
    TT_K_K6, TT_K_K7, TT_K_K8, TT_K_K9, TT_K_KMINUS, TT_K_KCOMMA, TT_K_KPERIOD,
    TT_K_KENTER, TT_K_FIND, TT_K_INSERT, TT_K_REMOVE, TT_K_SELECT, TT_K_PGUP,
    TT_K_PGDN, TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10, TT_K_F11,
    TT_K_F12, TT_K_F13, TT_K_F14, TT_K_F15, TT_K_F16, TT_K_F17, TT_K_F18,
    TT_K_F19, TT_K_F20, TT_K_DELETE, TT_K_DO, TT_K_HELP, TT_K_HOME, TT_K_END,
    TT_K_CHGCASE, TT_K_DELBCK, TT_K_DELFWD, TT_K_EOL, TT_K_FWDWRD, TT_K_BCKWRD,
    TT_K_BOL, TT_K_DELREST, TT_K_REDRAWFLD, TT_K_DELBEFORE };

  static char *keynames[MAXFUNCKEY] ={"LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4", "K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9", "KMINUS",
    "KCOMMA", "KPERIOD", "KENTER", "FIND", "INSERT", "REMOVE", "SELECT", "PGUP",
    "PGDN", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", 
    "F15", "F16", "F17", "F18", "F19", "F20", "DELETE", "DO", "HELP", "HOME", 
    "END", "CHGCASE", "DELBCK", "DELFWD", "EOL", "FWDWRD", "BCKWRD",
    "BOL", "DELREST", "REDRAWFLD", "DELBEFORE" };

  int idx;

  if (!key)
    strcpy(keyname,"NULL");
  else if (key == ERR) 
    strcpy(keyname,"ERR");
  else if (key > 127) {
    for (idx = 0; idx < MAXFUNCKEY; ++idx)
      if (keynums[idx] == key)
        break;
    if (idx < MAXFUNCKEY) 
      strcpy(keyname, keynames[idx]);
    else
      (void) itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }
  else if (key < 32) {
    *keyname = '^';
    *(keyname+1) = key + 64;
    *(keyname+2) = 0;
    *klen = 2;
  }
  else if (key < 127) {
    *keyname = key;
    *(keyname+1) = 0;
    *klen = 1;
  }
  else {
    itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }

#else
#if FIT_TURBOC
  int idx;
  #define K_KEYSMAX 37
  static char *keys[] = {"DEL","LEFT","RIGHT","UP","DOWN","F1","F2","F3","F4",
    "K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","INSERT","DELETE","HOME","END","PGUP","PGDN","REMOVE",
    "DO","F5","F6","F7","F8","F9","F10"};

  idx = key - 127;

  if (idx >= 0 && idx < K_KEYSMAX) {
    strcpy(keyname,keys[idx]);
    *klen = strlen(keyname);
  }
  else if (key < 32) {
    *keyname = '^';
    *(keyname+1) = key + 64;
    *(keyname+2) = 0;
    *klen = 2;
  }
  else if (key < 127) {
    *keyname = key;
    *(keyname+1) = 0;
    *klen = 1;
  }
  else {
    (void) itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }
#else
#define K_KEYSMAX 44
  static char *KEYS[] = {"LEFT","RIGHT","UP","DOWN","PF1","PF2","PF3",
    "PF4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","FIND","INSERT","REMOVE","SELECT","PGUP","PGDN","F5",
    "F6","F7","F8","F9","F10","F11","F12","F13","F14","HELP","DO","F17",
    "F18","F19","F20"};

  int idx;

  idx = key - TT_K_LEFT;

  if (idx >= 0 && idx < K_KEYSMAX) {
    strcpy(keyname,KEYS[idx]);
    *klen = strlen(keyname);
  }

  else if (key < 32) {
    keyname[0] ='^';
    keyname[1] = key + 64;
    keyname[2] = 0;
    *klen=2;
  }

  else if (key < 127) {
    keyname[0] = key;
    keyname[1] = 0;
    *klen=1;
  }

  else {
    sprintf(keyname,"%3d",key);
    *klen=3;
  }
#endif
#endif
  return;
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
 Module: tt_keyname					KEYNAME.C

 Function: Returns name of key corresponding to "key" as defined in
	TT.H, -1 if invalid.

 Author: A. Fregly

 Calling Sequence: ss = tt_keyname(int key, char *keyname, int *klen)

	key	key value
	keyname	Returned key name, length of keyname should be at lease 10
		characters.
	klen	Returned length of key name.

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tt.h>

#if FIT_ANSI
void tt_keyname(int key, char *keyname, int *klen)
#else
void tt_keyname(key, keyname, klen)
  int key;
  char *keyname;
  int *klen;
#endif
{
#if FIT_CURSES
#define MAXFUNCKEY 59
  static int keynums[MAXFUNCKEY] = { TT_K_LEFT, TT_K_RIGHT, TT_K_UP,
    TT_K_DOWN, TT_K_F1, TT_K_F2, TT_K_F3,
    TT_K_F4, TT_K_K0, TT_K_K1, TT_K_K2, TT_K_K3, TT_K_K4, TT_K_K5,
    TT_K_K6, TT_K_K7, TT_K_K8, TT_K_K9, TT_K_KMINUS, TT_K_KCOMMA, TT_K_KPERIOD,
    TT_K_KENTER, TT_K_FIND, TT_K_INSERT, TT_K_REMOVE, TT_K_SELECT, TT_K_PGUP,
    TT_K_PGDN, TT_K_F5, TT_K_F6, TT_K_F7, TT_K_F8, TT_K_F9, TT_K_F10, TT_K_F11,
    TT_K_F12, TT_K_F13, TT_K_F14, TT_K_F15, TT_K_F16, TT_K_F17, TT_K_F18,
    TT_K_F19, TT_K_F20, TT_K_DELETE, TT_K_DO, TT_K_HELP, TT_K_HOME, TT_K_END,
    TT_K_CHGCASE, TT_K_DELBCK, TT_K_DELFWD, TT_K_EOL, TT_K_FWDWRD, TT_K_BCKWRD,
    TT_K_BOL, TT_K_DELREST, TT_K_REDRAWFLD, TT_K_DELBEFORE };

  static char *keynames[MAXFUNCKEY] ={"LEFT","RIGHT","UP","DOWN","F1","F2","F3",
    "F4", "K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9", "KMINUS",
    "KCOMMA", "KPERIOD", "KENTER", "FIND", "INSERT", "REMOVE", "SELECT", "PGUP",
    "PGDN", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", 
    "F15", "F16", "F17", "F18", "F19", "F20", "DELETE", "DO", "HELP", "HOME", 
    "END", "CHGCASE", "DELBCK", "DELFWD", "EOL", "FWDWRD", "BCKWRD",
    "BOL", "DELREST", "REDRAWFLD", "DELBEFORE" };

  int idx;

  if (!key)
    strcpy(keyname,"NULL");
  else if (key == ERR) 
    strcpy(keyname,"ERR");
  else if (key > 127) {
    for (idx = 0; idx < MAXFUNCKEY; ++idx)
      if (keynums[idx] == key)
        break;
    if (idx < MAXFUNCKEY) 
      strcpy(keyname, keynames[idx]);
    else
      (void) itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }
  else if (key < 32) {
    *keyname = '^';
    *(keyname+1) = key + 64;
    *(keyname+2) = 0;
    *klen = 2;
  }
  else if (key < 127) {
    *keyname = key;
    *(keyname+1) = 0;
    *klen = 1;
  }
  else {
    itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }

#else
#if FIT_TURBOC
  int idx;
  #define K_KEYSMAX 37
  static char *keys[] = {"DEL","LEFT","RIGHT","UP","DOWN","F1","F2","F3","F4",
    "K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","INSERT","DELETE","HOME","END","PGUP","PGDN","REMOVE",
    "DO","F5","F6","F7","F8","F9","F10"};

  idx = key - 127;

  if (idx >= 0 && idx < K_KEYSMAX) {
    strcpy(keyname,keys[idx]);
    *klen = strlen(keyname);
  }
  else if (key < 32) {
    *keyname = '^';
    *(keyname+1) = key + 64;
    *(keyname+2) = 0;
    *klen = 2;
  }
  else if (key < 127) {
    *keyname = key;
    *(keyname+1) = 0;
    *klen = 1;
  }
  else {
    (void) itoa(key, keyname, 10);
    *klen = strlen(keyname);
  }
#else
#define K_KEYSMAX 44
  static char *KEYS[] = {"LEFT","RIGHT","UP","DOWN","PF1","PF2","PF3",
    "PF4","K0","K1","K2","K3","K4","K5","K6","K7","K8","K9","KMINUS","KCOMMA",
    "KPERIOD","KENTER","FIND","INSERT","REMOVE","SELECT","PGUP","PGDN","F5",
    "F6","F7","F8","F9","F10","F11","F12","F13","F14","HELP","DO","F17",
    "F18","F19","F20"};

  int idx;

  idx = key - TT_K_LEFT;

  if (idx >= 0 && idx < K_KEYSMAX) {
    strcpy(keyname,KEYS[idx]);
    *klen = strlen(keyname);
  }

  else if (key < 32) {
    keyname[0] ='^';
    keyname[1] = key + 64;
    keyname[2] = 0;
    *klen=2;
  }

  else if (key < 127) {
    keyname[0] = key;
    keyname[1] = 0;
    *klen=1;
  }

  else {
    sprintf(keyname,"%3d",key);
    *klen=3;
  }
#endif
#endif
  return;
}
