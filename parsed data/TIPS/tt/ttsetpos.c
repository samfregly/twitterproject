/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_setpos						setpos.c

Function : Sets cursor position on screen.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_setpos(int row, int col);

	row	Row at which to put cursor, 0 for current row.
	col	Column at which to put cursor, 0 for current column. 

Notes : 

Change log : 
000	15-NOV-89  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_setpos(int row, int col)
#else
int tt_setpos(row, col)
int row, col;
#endif
{
#if FIT_TURBOC
  if (row == 0) row = wherey();
  if (col == 0) col = wherex();
  gotoxy(col,row);
  return (1);

#else
  static char poscmd[11] = {TT_K_ESC,'[',0,0,0,0,0,0,0,0,0};
  int off;

  if (row <= 0 && col <= 0) return 1;

  off = 2;
  if (row != 0) {
    sprintf(poscmd+off,"%d",row);
    off = strlen(poscmd);
  }
  poscmd[off++] = ';';
  if (col != 0) {
    sprintf(poscmd+off,"%d",col);
    off = strlen(poscmd);
  }
  poscmd[off++] = 'H';
  poscmd[off++] = 0;
  return tt_put(poscmd);
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_setpos						setpos.c

Function : Sets cursor position on screen.

Author : A. Fregly

Abstract : 

Calling Sequence : ss = tt_setpos(int row, int col);

	row	Row at which to put cursor, 0 for current row.
	col	Column at which to put cursor, 0 for current column. 

Notes : 

Change log : 
000	15-NOV-89  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

#if FIT_ANSI
int tt_setpos(int row, int col)
#else
int tt_setpos(row, col)
int row, col;
#endif
{
#if FIT_TURBOC
  if (row == 0) row = wherey();
  if (col == 0) col = wherex();
  gotoxy(col,row);
  return (1);

#else
  static char poscmd[11] = {TT_K_ESC,'[',0,0,0,0,0,0,0,0,0};
  int off;

  if (row <= 0 && col <= 0) return 1;

  off = 2;
  if (row != 0) {
    sprintf(poscmd+off,"%d",row);
    off = strlen(poscmd);
  }
  poscmd[off++] = ';';
  if (col != 0) {
    sprintf(poscmd+off,"%d",col);
    off = strlen(poscmd);
  }
  poscmd[off++] = 'H';
  poscmd[off++] = 0;
  return tt_put(poscmd);
#endif
}
