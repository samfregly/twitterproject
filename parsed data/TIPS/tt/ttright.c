/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_right						RIGHT.C

Function : Moves cursor right one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_right();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_right() {
#if FIT_CURSES
  int x,y;
  getyx(stdscr, y, x);
  if (x < tt_ncols - 1)
    move(y,x+1);
  return 1;

#else
#if FIT_TURBOC
  return (tt_setpos(0,wherex()+1));
#else
  static char right[4] = {TT_K_ESC,'[','C',0};
  return tt_put(right);
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_right						RIGHT.C

Function : Moves cursor right one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_right();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#endif

int tt_right() {
#if FIT_CURSES
  int x,y;
  getyx(stdscr, y, x);
  if (x < tt_ncols - 1)
    move(y,x+1);
  return 1;

#else
#if FIT_TURBOC
  return (tt_setpos(0,wherex()+1));
#else
  static char right[4] = {TT_K_ESC,'[','C',0};
  return tt_put(right);
#endif
#endif
}
