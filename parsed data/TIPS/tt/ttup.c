/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_up						up.c

Function : Moves cursor up one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_up();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_up() {
#if FIT_TURBOC
  int row,col;
  tt_getpos(&row,&col);
  if (row > 1)
    tt_setpos(row-1,col);
  return 1;
#else
  static char up[4] = {TT_K_ESC,'[','A',0};
  return tt_put(up);
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : tt_up						up.c

Function : Moves cursor up one character.

Author : A. Fregly

Abstract :

Calling Sequence : ss = tt_up();

Notes :

Change log :
000  29-NOV-89  AMF	Converted to C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_up() {
#if FIT_TURBOC
  int row,col;
  tt_getpos(&row,&col);
  if (row > 1)
    tt_setpos(row-1,col);
  return 1;
#else
  static char up[4] = {TT_K_ESC,'[','A',0};
  return tt_put(up);
#endif
}
