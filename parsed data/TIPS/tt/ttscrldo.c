/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_scrldown						SCRLDOWN.C

 Function: Scrolls scrolling region down one line.

 Author: A. Fregly

 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#else
#if !FIT_CURSES
static char PREVLINE[3] = {TT_K_ESC,'M',0};
#endif
#endif

int tt_scrldown() {

#if FIT_CURSES
  int x, y;
  getyx(stdscr, y, x);
  move(0,0);
  insertln();
  move(y,x);
  refresh();
  return 1;

#else 
#if FIT_TURBOC
  struct text_info txstat;

  gettextinfo(&txstat);
  if (tt_srow == 0) {
    tt_srow = 1;
    tt_erow = txstat.screenheight;
  }

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,tt_srow,txstat.screenwidth,tt_erow);

  tt_setpos(1,1);
  insline();

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,1,txstat.screenwidth,txstat.screenheight);

  return (1);

#else
  tt_savepos();
  tt_setpos(tt_srow,1);
  tt_put(PREVLINE);
  tt_restorepos();
  return(1);
#endif
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_scrldown						SCRLDOWN.C

 Function: Scrolls scrolling region down one line.

 Author: A. Fregly

 Change Log:
 000  23-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_TURBOC
#include <conio.h>
#else
#if !FIT_CURSES
static char PREVLINE[3] = {TT_K_ESC,'M',0};
#endif
#endif

int tt_scrldown() {

#if FIT_CURSES
  int x, y;
  getyx(stdscr, y, x);
  move(0,0);
  insertln();
  move(y,x);
  refresh();
  return 1;

#else 
#if FIT_TURBOC
  struct text_info txstat;

  gettextinfo(&txstat);
  if (tt_srow == 0) {
    tt_srow = 1;
    tt_erow = txstat.screenheight;
  }

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,tt_srow,txstat.screenwidth,tt_erow);

  tt_setpos(1,1);
  insline();

  if (tt_srow != 1 || tt_erow != txstat.screenheight)
    window(1,1,txstat.screenwidth,txstat.screenheight);

  return (1);

#else
  tt_savepos();
  tt_setpos(tt_srow,1);
  tt_put(PREVLINE);
  tt_restorepos();
  return(1);
#endif
#endif
}
