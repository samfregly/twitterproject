/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_exit(int key)
#else
int tt_exit(key)
  int key;
#endif
{
return ((key < ' ' || key > '~') &&
  key != TT_K_LEFT && key != TT_K_RIGHT && key != TT_K_INSERT &&
  key != TT_K_DELFWD && key != TT_K_DELBCK && key != TT_K_BOL &&
  key != TT_K_EOL && key != TT_K_CHGCASE && key != TT_K_FWDWRD &&
  key != TT_K_BCKWRD && key != TT_K_REDRAWFLD && key != TT_K_DELBEFORE &&
  key != TT_K_DELREST);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

#if FIT_ANSI
int tt_exit(int key)
#else
int tt_exit(key)
  int key;
#endif
{
return ((key < ' ' || key > '~') &&
  key != TT_K_LEFT && key != TT_K_RIGHT && key != TT_K_INSERT &&
  key != TT_K_DELFWD && key != TT_K_DELBCK && key != TT_K_BOL &&
  key != TT_K_EOL && key != TT_K_CHGCASE && key != TT_K_FWDWRD &&
  key != TT_K_BCKWRD && key != TT_K_REDRAWFLD && key != TT_K_DELBEFORE &&
  key != TT_K_DELREST);
}
