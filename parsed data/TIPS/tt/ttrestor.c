/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

   MODULE NAME : tt_restorepos				RESTOREP.C

   FUNCTION : Tells terminal to restore saved cursor position. The cursor
	position can be stored with tt_savepos.

   AUTHOR : A. Fregly

   ABSTRACT : 

   CALLING SEQUENCE : 
	ss = tt_restorepos();

   NOTES : 

   CHANGE LOG : 
000  30-NOV-89  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_restorepos()
{
#if FIT_TURBOC || FIT_CURSES
  return (tt_setpos(tt_savrow,tt_savcol));
#else
  static char RESTOREPOS[3] = {TT_K_ESC,'8',0};
  return tt_put(RESTOREPOS);
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

   MODULE NAME : tt_restorepos				RESTOREP.C

   FUNCTION : Tells terminal to restore saved cursor position. The cursor
	position can be stored with tt_savepos.

   AUTHOR : A. Fregly

   ABSTRACT : 

   CALLING SEQUENCE : 
	ss = tt_restorepos();

   NOTES : 

   CHANGE LOG : 
000  30-NOV-89  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_restorepos()
{
#if FIT_TURBOC || FIT_CURSES
  return (tt_setpos(tt_savrow,tt_savcol));
#else
  static char RESTOREPOS[3] = {TT_K_ESC,'8',0};
  return tt_put(RESTOREPOS);
#endif
}
