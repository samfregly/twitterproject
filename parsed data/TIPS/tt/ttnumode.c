/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_numode						NUMODE.C

 Function: Puts terminal keypad into numeric mode.

 Author: A. Fregly

 Notes:
   This routine is a dummy for now.

 Change Log:
 000  26-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_numode()
{
#if FIT_TURBOC || FIT_CURSES
  tt_kmode = TT_K_NUMMODE;
  return 1;
#else
  static unsigned char NUMMODE[3] = {TT_K_ESC,'=',0};
  tt_kmode = TT_K_NUMMODE;
  return tt_put(NUMMODE);
#endif
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: tt_numode						NUMODE.C

 Function: Puts terminal keypad into numeric mode.

 Author: A. Fregly

 Notes:
   This routine is a dummy for now.

 Change Log:
 000  26-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tt.h>

int tt_numode()
{
#if FIT_TURBOC || FIT_CURSES
  tt_kmode = TT_K_NUMMODE;
  return 1;
#else
  static unsigned char NUMMODE[3] = {TT_K_ESC,'=',0};
  tt_kmode = TT_K_NUMMODE;
  return tt_put(NUMMODE);
#endif
}
