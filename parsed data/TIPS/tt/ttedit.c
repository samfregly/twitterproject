/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************

  Module: tt_edit                                               EDIT.C
  Function: Field edit input routine
  Author: A. Fregly
  Description:
    Allows user to enter/edit data in a fixed length field on the screen.
    Supports both insert and overtype mode text entry.  Editing keys supported
    include:

	Insert/Overtype mode toggle
	Back word
	Delete current character
	End of field
	Forward a word
	Beginning of field
	Delete from cursor to end of field
	Change case of character under cursor
	Redraw field
	Delete from beginning of field to cursor
	Left one character
	Right one character
	Delete previous character

  The following keys will cause this routine to terminate editing of the field.
	up arrow
	down arrow
	return
	tab
	line feed
	home
	end
	page up
	page down
	F1..F10

  Parameters:
	ss=tt_edit(buf,bufl,imode,dmode,emode,row,col,off,key)
	buf     *char   Buffer containing data to be edited
	bufl    int     Length of data in buffer to be edited
	imode   *int    Insert mode toggle
	dmode   int     Draw field before initiating edit flag
	emode   int     Exit field flag, .true. allow user to type out of
			field or use arrows to exit field.  KEY will contain
			value of last key entered.
	row     int     Row in display
	col     int     Start COL for field in display
	off     *int    Offset at which editing starts/ends.  1 is start.  If
			user exits by typing or arrowing out of field because
			EMODE is enabled, OFF will have a value of either 0 or
			BUFLEN+1.
	key     *int    Last key entered by user
	ss      *int    Returned status code

  Change Log:
000  11-JAN-88  AMF     Created.
001  27-JUL-94  AMF     Include stdlib.h
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC || FIT_VMS
#include <stdlib.h>
#endif
#include <tt.h>

#if FIT_ANSI
int tt_edit(char *inbuf, int bufl, int *imode, int dmode, int emode,
	int row, int col, int *off, int *key)
#else
int tt_edit(inbuf, bufl, imode, dmode, emode, row, col, off, key)
  char *inbuf;
  int bufl, *imode, dmode, emode, row, col, *off, *key;
#endif
{
  int ss, oldoff, e, cpos, ispace, ecol;
  char *buf;

/*  Redraw if DMODE set */
  buf = (char*) malloc(bufl+1);
  fit_sassign(buf,0,bufl-1,inbuf,0,bufl-1);
  *(buf+bufl) = 0;
  ss = 1;
  if (dmode) {
    tt_setpos(row,col);
    tt_putbuf(buf,0,bufl);
    ecol = col + bufl - 1;
    *off = (int) min( (int) max(0,*off), bufl);
    cpos = (int) min( (int) max(col+*off, 1), ecol+1);
    tt_setpos(row, cpos);
  }

  while (1) {
    *off = (cpos - col);
    ss=tt_inkey(key);
    if (!ss) goto done;
    if (tt_exit(*key)) goto done;
    if (tt_print(*key))
      if (cpos <= ecol) {
	tt_putbuf((char*) key,0,0);
	if (*imode && cpos < ecol) {
	  tt_lastch(buf, &e);
	  if (e >= *off) {
	    fit_sassign(buf,*off+1,(int) min(e+1,bufl-1), buf,*off,e);
	    tt_putbuf(buf,*off+1,(int) min(e+1,bufl-1));
	    tt_setpos(row,cpos+1);
	  }
	}      *(buf+*off) = (char) *key;
	cpos += 1;
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_LEFT)
      if (cpos > col)
	if (cpos <= ecol) {
	  tt_left();
	  cpos=cpos-1;
	}
	else {
	  tt_setpos(row,ecol);
	  cpos=ecol;
	}

      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	tt_beep();

    else if (*key == TT_K_RIGHT)
      if (cpos <= ecol) {
	cpos=cpos+1;
	tt_right();
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_DELBCK)
      if (cpos > col) {
	tt_lastch(buf,&e);
	if (cpos <= ecol) {
	  tt_left();
	  cpos=cpos-1;
	}
	else {
	  tt_setpos(row,ecol);
	  cpos=ecol;
	}
	if (*off <= e+1) {
	  if (*off <= e && *off < bufl)
	    fit_sassign(buf,*off-1,e,buf,*off,e);
	  else
	    *(buf+*off-1)=' ';
	  tt_putbuf(buf,*off-1,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) {
	  *off= -1;
	  goto done;
	}
	tt_beep();
      }

    else if (*key == TT_K_DELFWD)
      if (cpos <= ecol) {
	tt_lastch(buf,&e);
	if (*off < e)
	  fit_sassign(buf,*off,e,buf,*off+1,e);
	else
	  *(buf+e) = ' ';
	if (e >= *off) {
	  tt_putbuf(buf,*off,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_BOL)
      if (cpos > col) {
	cpos = col;
	tt_setpos(row,col);
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else {
	tt_beep();
      }

    else if (*key == TT_K_EOL) {
      *off=bufl;
      ispace=1;
      while (*off > 0 && ispace) {
	if (ispace = (*(buf+(*off)-1) == ' '))
	  *off -= 1;
      }
      cpos = col + *off;
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_FWDWRD) {
      ispace=1;
      if (*off < bufl) ispace= (*(buf+*off) == ' ');
      while (*off < bufl && !ispace ) {
	*off += 1;
	if (*off < bufl) ispace= *(buf+*off) == ' ';
      }
      while (*off < bufl && ispace) {
	*off += 1;
	if (*off < bufl) ispace = *(buf+*off) == ' ';
      }

      if (*off >= bufl) {
	if (emode) goto done;
	*off = bufl - 1;
	if (ecol - cpos == 0) tt_beep();
      }
      cpos = col + *off;
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_BCKWRD) {
      oldoff = *off;
      if (*off > 0)
	while (*off > 0 && *(buf+max(0,*off-1)) != ' ')
	  *off -= 1;
      if (*off > 0)
	while (*off > 0 && *(buf+max(*off-1,0)) == ' ')
	  *off -= 1;

      if (oldoff != *off) {
	cpos = col + *off;
	tt_setpos(row,cpos);
	if (*(buf+*off) != ' ')
	  tt_beep();
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	tt_beep();
    }

    else if (*key == TT_K_REDRAWFLD) {
      tt_setpos(row,col);
      tt_put(buf);
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_INSERT)
      *imode=!(*imode);

    else if (*key == TT_K_CHGCASE)
      if (*off < bufl) {
	if (*(buf+*off) >= 'A' && *(buf+*off) <= 'Z')
	  *(buf+*off) = *(buf+*off)+32;
	else if (*(buf+*off) >= 'a' && *(buf+*off) <= 'z')
	  *(buf+*off) = *(buf+*off)-32;
	tt_putbuf(buf,*off,*off);
	cpos=cpos+1;
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_DELBEFORE)
      if (*off > 0) {
	tt_lastch(buf,&e);
	if (*off < bufl)
	  fit_sassign(buf,0,e,buf,*off,bufl-1);
	else
	  fit_sassign(buf,0,bufl-1," ",0,0);
	tt_setpos(row,col);
	tt_putbuf(buf,0,e);
	tt_setpos(row,col);
	cpos = col;
      }
      else if (emode) {
	*off = -1;
	goto done;
      }
      else {
	tt_beep();
      }
    else if (*key == TT_K_DELREST)
      if (*off < bufl) {
	tt_lastch(buf,&e);
	if (e >= *off) {
	  fit_sassign(buf,*off,e," ",0,0);
	  tt_putbuf(buf,*off,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else
      tt_beep();
  }
done:
  fit_sassign(inbuf,0,bufl,buf,0,bufl);
  free(buf);
  return (ss);
}
/*******************************************************************************

		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************

  Module: tt_edit                                               EDIT.C
  Function: Field edit input routine
  Author: A. Fregly
  Description:
    Allows user to enter/edit data in a fixed length field on the screen.
    Supports both insert and overtype mode text entry.  Editing keys supported
    include:

	Insert/Overtype mode toggle
	Back word
	Delete current character
	End of field
	Forward a word
	Beginning of field
	Delete from cursor to end of field
	Change case of character under cursor
	Redraw field
	Delete from beginning of field to cursor
	Left one character
	Right one character
	Delete previous character

  The following keys will cause this routine to terminate editing of the field.
	up arrow
	down arrow
	return
	tab
	line feed
	home
	end
	page up
	page down
	F1..F10

  Parameters:
	ss=tt_edit(buf,bufl,imode,dmode,emode,row,col,off,key)
	buf     *char   Buffer containing data to be edited
	bufl    int     Length of data in buffer to be edited
	imode   *int    Insert mode toggle
	dmode   int     Draw field before initiating edit flag
	emode   int     Exit field flag, .true. allow user to type out of
			field or use arrows to exit field.  KEY will contain
			value of last key entered.
	row     int     Row in display
	col     int     Start COL for field in display
	off     *int    Offset at which editing starts/ends.  1 is start.  If
			user exits by typing or arrowing out of field because
			EMODE is enabled, OFF will have a value of either 0 or
			BUFLEN+1.
	key     *int    Last key entered by user
	ss      *int    Returned status code

  Change Log:
000  11-JAN-88  AMF     Created.
001  27-JUL-94  AMF     Include stdlib.h
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>

#if FIT_TURBOC || FIT_VMS
#include <stdlib.h>
#endif
#include <tt.h>

#if FIT_ANSI
int tt_edit(char *inbuf, int bufl, int *imode, int dmode, int emode,
	int row, int col, int *off, int *key)
#else
int tt_edit(inbuf, bufl, imode, dmode, emode, row, col, off, key)
  char *inbuf;
  int bufl, *imode, dmode, emode, row, col, *off, *key;
#endif
{
  int ss, oldoff, e, cpos, ispace, ecol;
  char *buf;

/*  Redraw if DMODE set */
  buf = (char*) malloc(bufl+1);
  fit_sassign(buf,0,bufl-1,inbuf,0,bufl-1);
  *(buf+bufl) = 0;
  ss = 1;
  if (dmode) {
    tt_setpos(row,col);
    tt_putbuf(buf,0,bufl);
    ecol = col + bufl - 1;
    *off = (int) min( (int) max(0,*off), bufl);
    cpos = (int) min( (int) max(col+*off, 1), ecol+1);
    tt_setpos(row, cpos);
  }

  while (1) {
    *off = (cpos - col);
    ss=tt_inkey(key);
    if (!ss) goto done;
    if (tt_exit(*key)) goto done;
    if (tt_print(*key))
      if (cpos <= ecol) {
	tt_putbuf((char*) key,0,0);
	if (*imode && cpos < ecol) {
	  tt_lastch(buf, &e);
	  if (e >= *off) {
	    fit_sassign(buf,*off+1,(int) min(e+1,bufl-1), buf,*off,e);
	    tt_putbuf(buf,*off+1,(int) min(e+1,bufl-1));
	    tt_setpos(row,cpos+1);
	  }
	}      *(buf+*off) = (char) *key;
	cpos += 1;
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_LEFT)
      if (cpos > col)
	if (cpos <= ecol) {
	  tt_left();
	  cpos=cpos-1;
	}
	else {
	  tt_setpos(row,ecol);
	  cpos=ecol;
	}

      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	tt_beep();

    else if (*key == TT_K_RIGHT)
      if (cpos <= ecol) {
	cpos=cpos+1;
	tt_right();
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_DELBCK)
      if (cpos > col) {
	tt_lastch(buf,&e);
	if (cpos <= ecol) {
	  tt_left();
	  cpos=cpos-1;
	}
	else {
	  tt_setpos(row,ecol);
	  cpos=ecol;
	}
	if (*off <= e+1) {
	  if (*off <= e && *off < bufl)
	    fit_sassign(buf,*off-1,e,buf,*off,e);
	  else
	    *(buf+*off-1)=' ';
	  tt_putbuf(buf,*off-1,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) {
	  *off= -1;
	  goto done;
	}
	tt_beep();
      }

    else if (*key == TT_K_DELFWD)
      if (cpos <= ecol) {
	tt_lastch(buf,&e);
	if (*off < e)
	  fit_sassign(buf,*off,e,buf,*off+1,e);
	else
	  *(buf+e) = ' ';
	if (e >= *off) {
	  tt_putbuf(buf,*off,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_BOL)
      if (cpos > col) {
	cpos = col;
	tt_setpos(row,col);
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else {
	tt_beep();
      }

    else if (*key == TT_K_EOL) {
      *off=bufl;
      ispace=1;
      while (*off > 0 && ispace) {
	if (ispace = (*(buf+(*off)-1) == ' '))
	  *off -= 1;
      }
      cpos = col + *off;
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_FWDWRD) {
      ispace=1;
      if (*off < bufl) ispace= (*(buf+*off) == ' ');
      while (*off < bufl && !ispace ) {
	*off += 1;
	if (*off < bufl) ispace= *(buf+*off) == ' ';
      }
      while (*off < bufl && ispace) {
	*off += 1;
	if (*off < bufl) ispace = *(buf+*off) == ' ';
      }

      if (*off >= bufl) {
	if (emode) goto done;
	*off = bufl - 1;
	if (ecol - cpos == 0) tt_beep();
      }
      cpos = col + *off;
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_BCKWRD) {
      oldoff = *off;
      if (*off > 0)
	while (*off > 0 && *(buf+max(0,*off-1)) != ' ')
	  *off -= 1;
      if (*off > 0)
	while (*off > 0 && *(buf+max(*off-1,0)) == ' ')
	  *off -= 1;

      if (oldoff != *off) {
	cpos = col + *off;
	tt_setpos(row,cpos);
	if (*(buf+*off) != ' ')
	  tt_beep();
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	tt_beep();
    }

    else if (*key == TT_K_REDRAWFLD) {
      tt_setpos(row,col);
      tt_put(buf);
      tt_setpos(row,cpos);
    }
    else if (*key == TT_K_INSERT)
      *imode=!(*imode);

    else if (*key == TT_K_CHGCASE)
      if (*off < bufl) {
	if (*(buf+*off) >= 'A' && *(buf+*off) <= 'Z')
	  *(buf+*off) = *(buf+*off)+32;
	else if (*(buf+*off) >= 'a' && *(buf+*off) <= 'z')
	  *(buf+*off) = *(buf+*off)-32;
	tt_putbuf(buf,*off,*off);
	cpos=cpos+1;
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else if (*key == TT_K_DELBEFORE)
      if (*off > 0) {
	tt_lastch(buf,&e);
	if (*off < bufl)
	  fit_sassign(buf,0,e,buf,*off,bufl-1);
	else
	  fit_sassign(buf,0,bufl-1," ",0,0);
	tt_setpos(row,col);
	tt_putbuf(buf,0,e);
	tt_setpos(row,col);
	cpos = col;
      }
      else if (emode) {
	*off = -1;
	goto done;
      }
      else {
	tt_beep();
      }
    else if (*key == TT_K_DELREST)
      if (*off < bufl) {
	tt_lastch(buf,&e);
	if (e >= *off) {
	  fit_sassign(buf,*off,e," ",0,0);
	  tt_putbuf(buf,*off,e);
	  tt_setpos(row,cpos);
	}
      }
      else {
	if (emode) goto done;
	tt_beep();
      }

    else
      tt_beep();
  }
done:
  fit_sassign(inbuf,0,bufl,buf,0,bufl);
  free(buf);
  return (ss);
}
