/*******************************************************************************
Change Log:
001     12-DEC-92  AMF  Compile under Coherent.
*******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif

#if FIT_ANSI
unsigned long fit_seconds(fit_s_timebuf *timebuf)
#else
unsigned long fit_seconds(timebuf)
  fit_s_timebuf *timebuf;
#endif
{
  unsigned long retseconds;
#if FIT_VMS
  unsigned long remainder;
  lib$ediv(&10000000,timebuf->timesnap,&retseconds,&remainder);
#else
  retseconds = (unsigned long) timebuf->time;
#endif
  return retseconds;
}
