/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_delfilelist                                   fitdelfi.c

Function : Deletes a file list which was created by fit_filelist.

Author : A. Fregly

Abstract : 

Calling Sequence : fit_delfilelist(char *(*(*filelist)));

  filelist      Pointer at file list created with fit_filelist.

Notes: 

Change log : 
000     04-SEP-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
void fit_delfilelist(char *(*(*filelist)))
#else
void fit_delfilelist(filelist)
  char *(*(*filelist));
#endif
{
  char *(*curfile);

  if (*filelist) {
    for (curfile = *filelist; *curfile != NULL; ++curfile)
      free(*curfile);
#if !FIT_ZORTECH
    free(*filelist);
#else
    free((char *) *filelist);
#endif
    *filelist = (char *(*)) NULL;
  }
}
