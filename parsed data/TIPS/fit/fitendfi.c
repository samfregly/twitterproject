/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_endfind                                       endfind.c

Function : Ends file search and deallocates state data for search.

Author : A. Fregly

Abstract : This routine is used to prematurely end a file scan being
	conducted using fit_findfile.

Calling Sequence : fit_endfind(void *context);

Notes :

Change log :
000     30-SEP-90  AMF  Created.
001     15-FEB-91  AMF  Modified for VMS.
002     12-DEC-92  AMF  Compile under Coherent.
003     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <findstat.h>

#if FIT_ANSI
void fit_endfind(void *(*context))
#else
void fit_endfind(context)
  char *(*context);
#endif
{

  struct fit_findstat *findstat;

  findstat = (struct fit_findstat *) *context;

  if (findstat) {
    /* Deallocate state storage */
#if FIT_VMS
    lib$find_file_end(&findstat->context);
#else
    if (findstat->searchlist) free(findstat->searchlist);
    if (findstat->filelist) fit_delfilelist(&findstat->filelist);
    free(findstat);
  }
#endif
  *context = (char *) NULL;
}

