/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_supcase                                       upcase.c

Function : Upcases a string

Author : A. Fregly

Abstract : This routine will upcase the supplied string. Note that this
	routine upcases the buffer pointed at by the passed in character
	pointer, and not a copy of it, so it should not be used with
	constants or buffers you do not wish to modify.

Calling Sequence :  char *strptr = fit_supcase(char *str);

	str     String to be upcased.
	strptr  Returned pointer at start of upcased string.

Notes :

Change log :
000     21-MAR-90  AMF  Created
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <ctype.h>

#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
char *fit_supcase(char *str)
#else
char *fit_supcase(str)
  char *str;
#endif
{
  char *s;

  s = str;
#if FIT_COHERENT
  for (; *str; ++str)
    if (*str >= 'A' && *str <= 'Z')
      *str += 32;
#else
  for (; *str ; ++str)
    *str = toupper(*str);
#endif
  return (s);
}
