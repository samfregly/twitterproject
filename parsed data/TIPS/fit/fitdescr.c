/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_descript                                      descript.c

Function : Creates VAX descriptor for supplied data item.

Author : A. Fregly

Abstract : This routine is used to create descriptors for the supplied
	data item. It is up to the caller to free the descriptor with the
	"free" function once the descriptor is no longer needed.

Calling Sequence : descriptor = (fit_descriptor *) 
	fit_descript(char *obj, unsigned int len);

	descriptor      Returned descriptor pointer.
	obj             Object for which descriptor is being created.
	len             Length of object in bytes.

Notes : 

Change log : 
000     15-FEB-90  AMF  Created.
001     12-DEC-92  AMF  Compile under Coherent.
002     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <fit_stat.h>

#if FIT_ANSI
fit_descriptor *fit_descript(void *obj, FIT_UWORD len)
#else
fit_descriptor *fit_descript(obj,len)
  char *obj; FIT_UWORD len;
#endif
{

  fit_descriptor *descript;

  descript = (fit_descriptor *) calloc((size_t) 1, sizeof(*descript));
  if (!descript)
    fit_status = FIT_FATAL_MEM;
  else {
    descript->len = len;
    descript->type = 0;
#if !FIT_KR
    descript->addr = (void *) obj;
#else
    descript->addr = obj;
#endif
  }
  return(descript);
}
