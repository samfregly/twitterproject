/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  fit_prsfspec                                     prsfspec.c

Function : Parses file specification, returning the components.

Author : A. Fregly

Abstract : This routine will return the components of the supplied file
  specification.  Any components which are missing are returned as zero
  length strings.

Calling Sequence : fit_prsfspec(char *inspec, char *node, char *dev,
  char *path, char *name, char *ext, char *version);

  inspec        File specification to be parsed.
  dev           Returned disk device name, includes the ':'.
  path          Returned path, includes the trailing backslash.
  name          Returned name portion of file specification.
  ext           Returned file extension.
  version       Returned file version (VMS only).

Notes :

Change log :
000     29-SEP-90  AMF  Created.
000	18-JAN-04  AMF  Make extension start at last "." for Unix and DOS file
                        names.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
void fit_prsfspec(char *inspec, char *node, char *dev, char *path, char *name, char *ext,
  char *version)
#else
void fit_prsfspec(inspec, node, dev, path, name, ext, version)
  char *inspec, *node, *dev, *path, *name, *ext, *version;
#endif
{

  char t_node[FIT_NODELEN+1], t_dev[FIT_DEVLEN+1], t_path[FIT_PATHLEN+1];
  char t_name[FIT_NAMELEN+1], t_ext[FIT_EXTLEN+1], t_version[FIT_VERSIONLEN+1];
  char *eptr;
  size_t len;

  *t_node = 0;
  *t_dev = 0;
  *t_path = 0;
  *t_name = 0;
  *t_ext = 0;
  *t_version = 0;

#if FIT_VMS
  if (inspec == NULL) return;

  eptr = strchr(inspec,':');
  if (eptr != NULL) {
    len = (eptr-inspec) + 1;
    strncpy(t_dev, inspec, len);
    t_dev[len] = 0;
    inspec = eptr+1;
  }

  eptr = strrchr(inspec,']');
  if (eptr != NULL) {
    len = (eptr-inspec) + 1;
    strncpy(t_path,inspec,len);
    t_path[len] = 0;
    inspec = eptr+1;
  }

  eptr = strchr(inspec,'.');
  if (eptr != NULL) {
    len = eptr-inspec;
    if (len) {
      strncpy(t_name,inspec,len);
      t_name[len] = 0;
    }
    strcpy(t_ext,eptr);
  }
  else
    strcpy(t_name,inspec);
#endif

#if FIT_DOS

  eptr = strchr(inspec,':');
  if (eptr != NULL) {
    len = (eptr-inspec) + 1;
    strncpy(t_dev, inspec, len);
    t_dev[len] = 0;
    inspec = eptr+1;
  }

  eptr = strrchr(inspec,'\\');
  if (eptr != NULL) {
    len = (eptr-inspec) + 1;
    strncpy(t_path,inspec,len);
    t_path[len] = 0;
    inspec = eptr+1;
  }

  eptr = strrchr(inspec,'.');
  if (eptr != NULL) {
    len = eptr-inspec;
    if (len) {
      strncpy(t_name,inspec,len);
      t_name[len] = 0;
    }
    strcpy(t_ext,eptr);
  }
  else
    strcpy(t_name,inspec);
#endif

#if FIT_UNIX || FIT_COHERENT || FIT_XENIX

  eptr = strrchr(inspec,'/');
  if (eptr != NULL) {
    len = (eptr-inspec) + 1;
    strncpy(t_path,inspec,len);
    t_path[len] = 0;
    inspec = eptr+1;
  }

  eptr = strrchr(inspec,'.');
  if (eptr != NULL) {
    len = eptr-inspec;
    if (len) {
      strncpy(t_name,inspec,len);
      t_name[len] = 0;
    }
    strcpy(t_ext,eptr);
  }
  else
    strcpy(t_name,inspec);
#endif

  strcpy(node, t_node);
  strcpy(dev, t_dev);
  strcpy(path, t_path);
  strcpy(name, t_name);
  strcpy(ext, t_ext);
  strcpy(version, t_version);
}
