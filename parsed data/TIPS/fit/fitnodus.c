/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_nodu_slist                                    fitnodus.c

Function : Removes duplicates from sorted string list.

Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function, fit_sort_slist, is used to remove duplicate entries
	from a list which is in sorted order. The fit_sort_slist function
	may be used to sort a string list.

Calling Sequence : void fit_nodu_slist(struct fit_s_slist *slist);

  slist         String list to be sorted.

Notes: 

Change log : 
000     17-JUL-92  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
void fit_nodu_slist(struct fit_s_slist *slist)
#else
void fit_nodu_slist(slist)
  struct fit_s_slist *slist;
#endif
{

  struct fit_s_snode *prev, *next;

  if (!slist) return;

  for (prev = slist->first; prev != slist->last;) {
    next = prev->next;
    if (!strcmp(prev->val, next->val)) {
      prev->next = next->next;
      slist->nelements -= 1;
      slist->nbytes -= (next->len + 1);
      if (next == slist->last) slist->last = prev;
      free(next->val);
      free(next);
    }
    else
      prev = next;
  }
}
