/*******************************************************************************
Change Log:
000     12-DEC-92  AMF  Compile under Coherent.
*******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif


#if FIT_ANSI
unsigned int fit_millisecs(fit_s_timebuf *timebuf)
#else
unsigned int fit_millisecs(timebuf)
  fit_s_timebuf *timebuf;
#endif
{
  unsigned int retmillisecs;
#if FIT_VMS
  unsigned long remainder;
  lib$ediv(&10000,timebuf->timesnap,&retmillisecs,&remainder);
  retmillisecs %= 1000;
#else
  retmillisecs = timebuf->millitm;
#endif
  return retmillisecs;
}
