/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_add_slist                                     fitaddsl.c

Function : Adds an element to a linked list of strings.

Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function, fit_add_slist, is used to add a string to the end of
	a list.

Calling Sequence : int retstat = fit_add_slist(struct fit_s_slist *slist,
	char *value);

  slist         Pointer at the string list to which value should be added.
  value         Value to be added.
  retstat       A non-zero value indicate success, while a value of 0
		indicates failure, in which case errno should contain
		the error status code.

Notes: 

Change log : 
000     16-JUL-92  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
int fit_add_slist(struct fit_s_slist *slist, char *value)
#else
int fit_add_slist(slist, value)
  struct fit_s_slist *slist;
  char *value;
#endif
{
  struct fit_s_snode *entry;
  int retstat = 0;

  if (slist)
    if (entry = (struct fit_s_snode *) calloc(1, sizeof(*entry)))
      if (entry->val = (char *) calloc((entry->len =
	  strlen(value))+1, 1)) {
	if (slist->last) {
	  slist->last->next = entry;
	  slist->last = entry;
	}
	else
	  slist->first = slist->last = entry;
	entry->next = (struct fit_s_snode *) NULL;
	slist->nelements += 1;
	slist->nbytes += (entry->len + 1);
	strcpy(entry->val, value);
	retstat = 1;
      }

  return retstat;
}
