/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************
 Module: fit_sljust                                             sljust.c

 Function: Left justifies string and returns loc of last non-blank.

 Calling Sequence: n = fit_sljust(char *str, int len);

	str     Character string to be left justified.
	len     Length of data area allocated to string.
	n       Returned offset of last non-blank in "str".

 Author: A. Fregly

 Change Log:
 000  24-NOV-89  AMF  Converted to C.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
int fit_sljust(char *str, int len)
#else
int fit_sljust(str, len)
  char *str;
  int len;
#endif
{
  int i,n, slen;

  i = 0;
  n = len;
  slen = strlen(str);
  if (slen < len) fit_sblank(str+slen, len - slen);

  for (i=0; i < len && *(str+i) == ' '; ++i)
    ;

  if (i && (i < len))
    fit_sassign(str,0,len,str,i,len);

  if (len > 0)
    for (n = len-1; *(str+n) == ' ' && n > 0; --n)
      ;
  return(n);
}
