/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_findfile                                      findfile.c

Function : Finds next file matching supplied file name list.

Author : A. Fregly

Abstract : This routine is used to scan a list of file specifications,
	returning the names of all matching files.  The routine allocates
	status info for doing the search on the initial call.  This info
	is automatically deallocated by the routine when the search for
	more matching files fails.  The caller may also deallocate this
	internal storage with a call to fit_endfind.

Calling Sequence : retspec = *fit_findfile(void *(*context), char *searchspec,
  unsigned attrib, char *defaultspec, char *defpath, char *retfile);

  context       Pointer at context pointer.  Context pointer should be set
		to NULL prior to the initial call.  Context pointer will be
		reset to NULL when no more files are to be found.
  searchspec    File name or list of file names to be searched for.
		Searchspec may contain wild cards.  If searchspec is a list,
		it should contain comma separated values with no embedded
		spaces.  Search spec is ignored after the initial call to
		fit_findfile.
  attrib        Attribute for file to be selected. For UNIX, this should
		be a combination of the following values from sys/stat.h:
			S_IFDIR  - Directory
			S_IFCHR  - Character special file.
			S_IFBLK  - Block special file.
			S_IFREG  - Regular file.
			S_IFIFO  - Named pipe.
		For DOS, this should be ONE of the following values from
		dos.h:
			FA_RDONLY - Read-only
			FA_HIDDEN - Hidden file
			FA_SYSTEM - System file
			FA_LABEL  - Volume label
			FA_DIREC  - Directory
			FA_ARCH   - Archive bit set
			0         - Regular file
  defaultspec   Default file specification.  If the caller wishes trailing
		items in the list to inherit missing parts from previous
		entries in the list, they should supply the most recently
		returned value from fit_findfile to the second and succeeding
		calls when processing the list.  Note that defaultspec is
		only used on the first search for files matching the next
		arguement in the list, so arbitrary changing of its value
		in the middle of successive calls to  fit_findfile will
		have unpredictable results, except in the case noted above
		where defaultspec is the file name returned by the preceeding
		call.
  defpath       Default path specification. If supplied, this must be an
		absolute path specification. Note that under UNIX, the
		performance of this function is greatly speeded up if the
		caller supplies this parameter. The C library routine,
		getcwd, can be used to retrieve the current working directory
		under both UNIX and DOS.
  retfile       Returned file which was found.  The caller should allocate
		at list FIT_FULLFSPECLEN+1 bytes for retfile.  If no file is
		found, a zero length string is returned (first byte is zero).
  retspec       Sames as retfile.

Notes :

Change log :
000     30-SEP-90  AMF  Created.
001     31-JUL-91  AMF  Added defpath paramter.
002     12-DEC-92  AMF  Compile under Coherent.
003     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_UNIX
#if !FIT_COHERENT
#include <sys/errno.h>
#endif
#include <fcntl.h>
#include <sys/stat.h>
#endif
#include <fit_stat.h>
#include <findstat.h>

extern int errno;

#if FIT_ANSI
char *fit_findfile(void *(*context), char *searchspec, unsigned attrib,
  char *defaultspec, char *defpath, char *retfile)
#else
char *fit_findfile(context, searchspec, attrib, defaultspec, defpath,
  retfile)
  char *(*context), *searchspec;
  unsigned attrib;
  char *defaultspec, *defpath, *retfile;
#endif
{
#if FIT_VMS
  long lib$find_file();

  struct fit_findstat *findstat;
  int done;
  unsigned int len;
  struct fit_descriptor *tempdescr1, *tempdescr2, *tempdescr3;

  fit_sblank(retfile,FIT_FULLFSPECLEN);

  if (!*context) {
    findstat = (struct fit_findstat *) NULL;
    if (!searchspec) goto WRAPUP;

    /* Initial call, allocate storage for status */
    findstat = (struct fit_findstat *) malloc(sizeof(*findstat));
    if (!findstat) goto ALLOCERR;

    findstat->searchlist = malloc(strlen(searchspec)+1);
    if (!findstat->searchlist) goto ALLOCERR;

    strcpy(findstat->searchlist,searchspec);

    findstat->context = 0;

    /* Set returned context */
    *context = (void *) findstat;

    /* Pull off the first arguement in the list, expand it, and then search */
    findstat->remaininglist =
      fit_nextarg(findstat->searchlist,findstat->curfspec);

    fit_expfspec(defaultspec,findstat->curfspec,defpath,findstat->curdefault);
    fit_expfspec(findstat->curfspec,defaultspec,defpath,findstat->curfspec);

    done = (int) !(1L & lib$find_file(
      tempdescr1 = fit_descript(findstat->curfspec,strlen(findstat->curfspec)),
      tempdescr2 = fit_descript(retfile, FIT_FULLFSPECLEN), &findstat->context,
      tempdescr3 = fit_descript(defaultspec, strlen(defaultspec))));
  }

  else {
    /* Continuing a previous search, set context and search for next match on
	current file in list */
    findstat = (struct fit_findstat *) *context;
    done = (int) !(1L & lib$find_file(
      tempdescr1 = fit_descript(findstat->curfspec,strlen(findstat->curfspec)),
      tempdescr2 = fit_descript(retfile,FIT_FULLFSPECLEN), &findstat->context,
      tempdescr3 = fit_descript(defaultspec,strlen(defaultspec))));
  }

  while (done && *(findstat->remaininglist)) {
    lib$find_file_end(&findstat->context);
    /* Done with current element in list, try succeeding elements until
       a matching file is found or we run out of list. */
    findstat->remaininglist =
	fit_nextarg(findstat->remaininglist,findstat->curfspec);
    fit_expfspec(findstat->curfspec,findstat->curdefault,defpath,
      findstat->curfspec);
    strcpy(findstat->curdefault,findstat->curfspec);
    findstat->context = 0;
    done = (int) !(1L & lib$find_file(
      tempdescr1 = fit_descript(findstat->curfspec,strlen(findstat->curfspec)),
      tempdescr2 = fit_descript(retfile, FIT_FULLFSPECLEN), &findstat->context,
      tempdescr3 = fit_descript(defaultspec,strlen(defaultspec))));

    free(tempdescr1);
    free(tempdescr2);
    free(tempdescr3);
  }
  goto WRAPUP;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  done = 1;

WRAPUP:
  if (done) {
    /* Deallocate status storage */
    *context = (struct fit_findstat *) findstat;
    fit_endfind(context);

    /* Set return file to a zero length string */
    *retfile = 0;
  }

  len = 0;
  return(fit_strim(retfile,&len));
#endif

#if FIT_UNIX || FIT_COHERENT || FIT_DOS
  struct fit_findstat *findstat;
  int done;
  char fullpath[FIT_FULLFSPECLEN+1], dummy[FIT_FULLFSPECLEN+1];
  char wild[FIT_FULLFSPECLEN+1], dir[FIT_FULLFSPECLEN+1];
  char ext[FIT_FULLFSPECLEN+1], ver[FIT_FULLFSPECLEN+1];

 /* printf("fit_findfile:\n..searchspec: %s\n..defaultspec: %s\n..defpath: %s\n",
	searchspec, defaultspec, defpath); */
  *retfile = 0;

  if (!*context) {
	  /* printf("fit_findfile, context was null, creating new context\n"); */
    if (!searchspec) goto WRAPUP;

    /* Initial call, allocate storage for status */
    findstat = (struct fit_findstat *) calloc(1, sizeof(*findstat));
    done = !findstat;
    if (done) goto WRAPUP;
    findstat->searchlist = (char *) calloc(strlen(searchspec)+1, 1);
    strcpy(findstat->searchlist,searchspec);
    done = !findstat->searchlist;
    if (done) goto WRAPUP;

    /* Set returned context */
#if FIT_ANSI
    *context = (void *) findstat;
#else
    *context = (char *) findstat;
#endif
    /* Pull off the first arguement in the list, expand it, and then search */
    findstat->remaininglist =
      fit_nextarg(findstat->searchlist,findstat->curfspec);
    fit_expfspec(findstat->curfspec,defaultspec,defpath,findstat->curfspec);
    strcpy(findstat->curdefault, findstat->curfspec);

    done = 1;
    fit_prsfspec(findstat->curfspec, dummy, dummy, dir, wild, ext, ver);
    strcat(wild, ext);
    strcat(wild, ver);
	/* printf("fit_findfile, calling fit_wildlist:\n..dir: %s\n..wild: %s\n..curdefault: %s\n",
		dir,wild,findstat->curdefault); */
    findstat->filelist = fit_wildlist(dir, wild, attrib,
      findstat->curdefault, &findstat->nfiles, fullpath);
	/* printf("fit_findfile, return from fit_wildlist:\n..nfiles: %d\n",findstat->nfiles); */
  }

  else {
    /* Continuing a previous search, set context */
    findstat = (struct fit_findstat *) *context;
  }

  /* Scan for next file */
  for (*retfile = 0, done = !*findstat->curfspec; !done;
      done = !findstat->nfiles) {

    if (findstat->idx < findstat->nfiles) {
      fit_prsfspec(findstat->curfspec,dummy,dummy,dir,dummy,dummy,dummy);
      strcpy(retfile, *(findstat->filelist + findstat->idx++));
		/* printf("fit_findfile, found file in current arg expanded list:\n..retfile: %s\n",
			retfile); */
      break;
    }

    fit_delfilelist(&findstat->filelist);
    findstat->nfiles = 0;
    findstat->idx = 0;

    for (done = !*findstat->remaininglist; !done;
	done = (!*findstat->remaininglist || findstat->nfiles)) {
/* printf("fit_findfile, scanning arg list for following file matches\n"); */
      fit_delfilelist(&findstat->filelist);

      /* Done with current element in list, try succeeding elements until
	 a matching file is found or we run out of list. */
      findstat->remaininglist =
	fit_nextarg(findstat->remaininglist,findstat->curfspec);
      fit_expfspec(findstat->curfspec,findstat->curdefault,defpath,
	findstat->curfspec);

      fit_prsfspec(findstat->curfspec, dummy, dummy, dir, wild, ext, ver);
      strcat(wild, ext);
      strcat(wild, ver);
	  /* printf("fit_findfile, calling fit_wildlist for next arg:\n..dir: %s\n..wild: %s\n..curfspec: %s\n",
		  dir, wild, findstat->curfspec); */
      findstat->filelist = fit_wildlist(dir, wild, attrib,
	findstat->curfspec, &findstat->nfiles, findstat->curdefault);
	  /* printf("fit_findfile, fit_wildlist located %d files\n", findstat->nfiles); */
    }
  }

WRAPUP:
  if (done) {
    /* Deallocate status storage */
#if FIT_ANSI
    *context = (void *) findstat;
#else
    *context = (char *) findstat;
#endif
    fit_endfind(context);

    /* Set return file to a zero length string */
    *retfile = 0;
  }
  else
    fit_expfspec(retfile, dir, defpath, retfile);

  return(retfile);
#endif
}
