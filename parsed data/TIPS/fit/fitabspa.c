/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_abspath                                       fitabspa.c

Function : Converts relative paths to absolute paths.

Author : A. Fregly

Abstract : This function is used to convert relative paths to absolute paths.
	This must be done if a path name is to be stored as a pointer so that
	the path name can be determined accurately at a later time.

Calling Sequence : fit_abspath(char *path, char *abspath,
	char retpath[FIT_FULLFSPECLEN+1]);

  path          Path name to be converted to an absolute path name. If path
		is not a relative path specification, the returned value is
		just a copy of path.
  abspath       Absolute path name to use as a start point in resolving the
		relative path name. The C library function, getcwd, may be
		used to return the absolute path name of the current working
		directory.
  retpath       Returned absolute path name.

Notes:

Change log :
000     01-AUG-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_DOS
#define SEP '\\'
#else
#if FIT_UNIX || FIT_COHERENT
#define SEP '/'
#endif
#endif

#if FIT_ANSI
void fit_abspath(char *path, char *abspath, char *retpath)
#else
void fit_abspath(path, abspath, retpath)
  char *path, *abspath, *retpath;
#endif
{

  char *pptr, *wptr, *tptr;

#if !FIT_DOS && !FIT_UNIX && !FIT_COHERENT
  strcpy(retpath, path);
#else

  char workpath[FIT_FULLFSPECLEN+1];

#if FIT_DOS
  if (abspath == NULL || !*abspath || strchr(path,':') != NULL) {

#else
  if (abspath == NULL || !*abspath || (path != NULL && *path == '/')) {
#endif
    strcpy(workpath, path);
    wptr = strchr(workpath,'\0') - 1;
    if (wptr >= workpath && *wptr != SEP) {
      *(++wptr) = SEP;
      *(++wptr) = 0;
    }
    goto DONE;
  }

  strcpy(workpath, abspath);

#if FIT_DOS
  if (*path == SEP) {
    if ((wptr = strchr(workpath,':')) == NULL)
      wptr = workpath - 1;
    strcpy(wptr+1,path);
    goto DONE;
  }
#endif

  wptr = strchr(workpath,'\0');
  if (wptr > workpath && *(wptr-1) == SEP)
    *(--wptr) = 0;

  for (pptr = path; *pptr == '.';) {
    if (*++pptr == '.') {
      tptr = strrchr(workpath,SEP);
      if (tptr != NULL) {
	wptr = tptr;
	*wptr = 0;
      }
      if (*++pptr != SEP)
	break;
      else
	++pptr;
    }
    else {
      if (*pptr == SEP)
	++pptr;
      break;
    }
  }

  *wptr = SEP;
  strcpy(wptr+1,pptr);
  wptr += strlen(pptr);
  if (*wptr != SEP) {
    *(++wptr) = SEP;
    *(++wptr) = 0;
  }

DONE:
  strcpy(retpath, workpath);
#endif
}
