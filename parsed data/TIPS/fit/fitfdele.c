/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_fdelete                                       fdelete.c

Function : Deletes a file (or under UNIX, removes a file link).

Author : A. Fregly

Abstract : This routine is used to delete files. In a UNIX environment, the
	file is unlinked, and is not actually be deleted if other links still
	exist.

Calling Sequence : stat = fit_fdelete(char *filespec);

	stat            Returned status, 0 is an error.
	filespec        File to be unlinked.

Notes: 

Change log : 
000     22-MAY-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_ZORTECH
#include <io.h>
#endif

#if FIT_ANSI
int fit_fdelete(char *filespec)
#else
int fit_fdelete(filespec)
  char *filespec;
#endif
{
  return (!unlink(filespec));
}
