/******************************************************************************
Change Log:
001  12-DEC-92  AMF     Compile under Coherent
******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#if !FIT_COHERENT
#include <stdlib.h>
#endif
#endif

/***
#if FIT_COHERENT
#include <sys/const.h>
#endif
***/

#if FIT_UNIX
#include <sys/param.h>
#endif

#if FIT_ANSI
void fit_addtime(fit_s_timebuf *time1, fit_s_timebuf *time2,
    fit_s_timebuf *rettime)
#else
void fit_addtime(time1, time2, rettime)
  fit_s_timebuf *time1, *time2, *rettime;
#endif
{
#if FIT_VMS
  lib$addx(time1->timesnap,time2->timesnap,rettime->timesnap);
#else
  rettime->time = time1->time + time2->time;
  rettime->millitm = time1->millitm + time2->millitm;
  if (rettime->millitm >= 1000) {
    ++rettime->time;
    rettime->millitm %= 1000;
  }
#endif
}
