/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_sblank                                        sblank.c

Function : Fills string with blanks.

Author : A. Fregly

Abstract : This routine is used to fill a string with blanks and put a
	terminating null character in the byte which is at offset len.

Calling Sequence : char *resultstr = str_blank(char *str, int len);

	resultstr       Pointer at string which was blanked.
	str             Pointer at string to be blanked.
	len             Number of bytes to be blanked. A zero byte is put
			in the byte at offset len.

Notes : 

Change log :
000     1989?  AMF      Created
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
char *fit_sblank(char *str, int len)
#else
char *fit_sblank(str,len)
  char *str;
  int len;
#endif
{
  if (len)
    memset(str,' ',(size_t) len);
  *(str+len) = 0;
  return(str);
}
