/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
void fit_sadd(char *dest, int len, char *src, int *offset)
#else
void fit_sadd(dest, len, src, offset)
  char *dest;
  int len;
  char *src;
  int *offset;
#endif
{
  int srcl;
  int soff;

  if (src == NULL) return;

  soff = *offset+1;
  if (soff < len) {
    srcl = strlen(src);
    if (srcl) {
      *offset = min(*offset + srcl,len);
      fit_sassign(dest, soff, *offset, src, 0, srcl-1);
    }
  }
}
