/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_frename                                       frename.c

Function : Renames a file.

Author : A. Fregly

Abstract : This routine is used to rename a file.

Calling Sequence : stat = fit_rename(oldname,newname);

Notes : 

Change log : 
000     02-MAY-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_ANSI
int fit_frename(char *oldname, char *newname)
#else
int fit_frename(oldname,newname)
  char *oldname;
  char *newname;
#endif
{
#if FIT_UNIX || FIT_COHERENT
  if (!link(oldname,newname))
    if (!unlink(oldname)) return 1;
  return 0;
#else
#if FIT_DOS
  return (!rename(oldname,newname));
#endif
#endif
}
