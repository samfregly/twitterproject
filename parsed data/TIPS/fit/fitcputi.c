/****************************************************************************
Change Log:
000     12-DEC-92  AMF  Compile under Coherent
*****************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX
#if !FIT_COHERENT
#include <sys/param.h>
#endif
#include <sys/times.h>
#endif

long fit_cputime() {
#if FIT_UNIX || FIT_COHERENT
  struct tms tbuf;
  times(&tbuf);
  return (long) (tbuf.tms_utime + tbuf.tms_stime);

#else
#if !FIT_ZORTECH
  /* VAX or DOS/Turbo-C
  fit_s_timebuf timebuf;
  fit_gettime(&timebuf);
  return (long) (fit_seconds(&timebuf) * 1000 + fit_millisecs(&timebuf));

#else
/* Zortech */
#include <time.h>
  return clock() * 10;
#endif
#endif
}

