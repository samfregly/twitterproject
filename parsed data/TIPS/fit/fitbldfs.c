/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_bldfspec                                      bldfspec.c

Function : Builds file specification from component parts.

Author : A. Fregly

Abstract : This routine combines the supplied file name components into
	a file specification.  No error checking is performed as to the
	validity of the constructed file specification.

Calling Sequence : char *retfspec = fit_bldfspec(char *node, char *dev,
  char *path, char *name, char *ext, char *version, char *outspec);

  node          Node name
  dev           Device name.
  path          Path.
  name          File name.
  ext           File extension.
  outspec       Returned file spec.  Caller must allocate at least
		FIT_FULLFSPECLEN+1 bytes.

Notes :

Change log :
000     30-SEP-90  AMF  Created.
001     14-FEB-91  AMF  Modified for VMS.
002     27-MAR-91  AMF  Merged PC and VMS versions.
003     27-JUL-93  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
char *fit_bldfspec(char *node, char *dev, char *dir, char *name, char *ext,
  char *version, char *outspec)
#else
char *fit_bldfspec(node, dev, dir, name, ext, version, outspec)
  char *node, *dev, *dir, *name, *ext, *version, *outspec;
#endif
{
  int offset;
  offset = -1;
  fit_sadd(outspec,FIT_FULLFSPECLEN,node,&offset);
  fit_sadd(outspec,FIT_FULLFSPECLEN,dev,&offset);
  fit_sadd(outspec,FIT_FULLFSPECLEN,dir,&offset);
  fit_sadd(outspec,FIT_FULLFSPECLEN,name,&offset);
  fit_sadd(outspec,FIT_FULLFSPECLEN,ext,&offset);
  fit_sadd(outspec,FIT_FULLFSPECLEN,version,&offset);
  outspec[min(offset+1,FIT_FULLFSPECLEN)] = 0;
  return(outspec);
}
