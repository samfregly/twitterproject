/******************************************************************************
Change Log:
001     12-DEC-92  AMF  Compile under Coherent.
******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif
#if FIT_SUN
#include <sys/types.h>
#include <sys/time.h>
#include <sys/timeb.h>
#endif

#if FIT_ANSI
void fit_gettime(fit_s_timebuf *timbuf)
#else
void fit_gettime(timbuf)
  fit_s_timebuf *timbuf;
#endif
{
#if FIT_VMS
  sys$gettim(timbuf->timesnap);
#else
#if !FIT_ZORTECH
  ftime(timbuf);
#else
  unsigned long millitm;

  timbuf->time = clock();
  millitm = (timbuf->time % 100);
  timbuf->millitm = (short) millitm;
  timbuf->time /= 100;
#endif
#endif
}
