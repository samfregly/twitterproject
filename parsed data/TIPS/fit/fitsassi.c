/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : fit_sassign                                       ASSIGN.C

Function : Copies specified portion of one string into another.

Author : A. Fregly

Abstract :
	This routine is used to copy one string into another, blank filling
	if the destination is larger than the source, and truncating if it
	is too small.  Unlike the C library functions, this routine ignores
	the null byte normally used to terminate a string.  This routine can
	also be used to copy a portion of a string into itself without worry
	about part of the source being clobbered before it is copied.

Calling Sequence :  fit_sassign(char *dest, int ds, int de, char *src,
	int ss, int se);

	dest    pointer at start of destination string.
	ds      offset of start of destination to be copied into.
	de      offset of end of destination.
	src     pointer at start of source string.
	ss      offset of start of source to be copied.
	se      offset of end of source to be copied.

Notes :

Change log :
000     14-NOV-89  AMF  Created.
000     18-JAN-04  AMF  Fix for case where source end pointer is less than source
                        start pointer.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
void fit_sassign(char *dest, int ds, int de, char *src, int ss, int se)
#else
void fit_sassign(dest, ds, de, src, ss, se)
  char *dest;
  int ds, de;
  char *src;
  int ss, se;
#endif
{
  if (de >= ds) {
    se = max(ss-1,se);
    if ( se >= ss) {
      memmove(dest+ds, src+ss, (size_t) ((int) min (de-ds, se-ss) + 1));
    }
    if (de-ds > se-ss) {
      memset(dest + ds + (se-ss) + 1, ' ', (size_t) ((de-ds) - (se-ss)));
    }
  }
}
