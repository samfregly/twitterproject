/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_expfspec                                      expfspec.c

Function : Expands file spec.

Author : A. Fregly

Abstract : This routine is used to expand the given file specification so
	that it contains device name, path, and if supplied by the caller,
	a file name and extension.  The expanded file spec will used the
	current default device and directory if they are not specified in
	the input spec or default spec.

Calling Sequence : char *retfspec = *fit_expfspec(char *inspec,
  char *defaultspec, char *defaultpath, char *outspec);

  inspec        File specification to be expanded.
  defaultspec   Defaults for missing parts of inspec.
  defaultpath   Default path specification. If supplied, this must be an
		absolute path specification. Note that under UNIX, the
		performance of this function is greatly speeded up if the
		caller supplies this parameter. The C library routine,
		getcwd, can be used to retrieve the current working directory
		under both UNIX and DOS.
  outspec       Returned expanded file specification.  At least FIT_FULLFSPECLEN+1
		bytes should be allocated for outspec.

Notes :
001     defaultpath is ignored under VMS.

Change log :
000     29-SEP-90  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_TURBOC
#include <dir.h>
#endif
#if FIT_ZORTECH || FIT_MSC
#include <direct.h>
#endif
#if FIT_UNIX
#if FIT_ANSI
char *getcwd(char *buf, int size);
#else
char *getcwd();
#endif
#endif

#if FIT_ANSI
char *fit_expfspec(char *inspec, char *defaultspec, char *defpath,
  char *outspec)
#else
char *fit_expfspec(inspec, defaultspec, defpath, outspec)
  char *inspec, *defaultspec, *defpath, *outspec;
#endif
{

#if FIT_VMS
  struct fit_descriptor *tempdescr1, *tempdescr2, *tempdescr3, *tempdescr4;
  int i;

  inquire(tempdescr1 = fit_descript(defaultspec)
  inquire(tempdescr1 = fit_descript(inspec,strlen(inspec)),
     tempdescr2 = fit_descript(defaultspec,strlen(defaultspec)),
     tempdescr3 = fit_descript(outspec, FIT_FULLFSPECLEN));
  free(tempdescr1);
  free(tempdescr2);
  free(tempdescr3);

  outspec[FIT_FULLFSPECLEN] = 0;

  for (i = FIT_FULLFSPECLEN-1; i >= 0 && (outspec[i] == ' ' ||
      outspec[i] == 0); --i)
    ;
  outspec[max(0,i+1)] = 0;
#else
#if FIT_DOS || FIT_UNIX || FIT_COHERENT

  char node[FIT_NODELEN+1], version[FIT_VERSIONLEN+1];
  char ispec_dev[FIT_DEVLEN+1],ispec_path[FIT_PATHLEN+1];
  char ispec_name[FIT_NAMELEN+1],ispec_ext[FIT_EXTLEN+1];

  char dspec_dev[FIT_DEVLEN+1],dspec_path[FIT_PATHLEN+1];
  char dspec_name[FIT_NAMELEN+1],dspec_ext[FIT_EXTLEN+1];

  char *curpath, curpathname[FIT_FULLFSPECLEN+1];

#if FIT_DOS
  char *startpath;
#endif

  if (defpath != NULL && *defpath)
    curpath = defpath;
  else
    curpath = getcwd(curpathname, FIT_FULLFSPECLEN+1);

  /* Get parts of inspec */
  fit_prsfspec(inspec,node,ispec_dev,ispec_path,ispec_name,ispec_ext,
    version);

  if (ispec_path[0]) {
    fit_abspath(ispec_path, curpath, ispec_path);

#if FIT_DOS
    if ((startpath = strchr(ispec_path,':')) != NULL) {
      if (!*ispec_dev) {
	strncpy(ispec_dev, ispec_path, startpath - ispec_path + 1);
	ispec_dev[startpath - ispec_path + 1] = 0;
      }
      strcpy(ispec_path, startpath+1);
    }
#endif
  }

  /* Get parts of defaultspec */
  fit_prsfspec(defaultspec,node,dspec_dev,dspec_path,dspec_name,dspec_ext,
    version);

  fit_abspath(dspec_path, curpath, dspec_path);

#if FIT_DOS
    if ((startpath = strchr(dspec_path,':')) != NULL) {
      if (!*dspec_dev) {
	strncpy(dspec_dev, dspec_path, startpath - dspec_path + 1);
	dspec_dev[startpath - dspec_path + 1] = 0;
      }
      strcpy(dspec_path, startpath+1);
    }
#endif

  /* Replace null parts of inspec with non-null parts of default */
  if (!ispec_dev[0]) strcpy(ispec_dev,dspec_dev);
  if (!ispec_path[0]) strcpy(ispec_path,dspec_path);
  if (!ispec_name[0]) strcpy(ispec_name,dspec_name);
  if (!ispec_ext[0]) strcpy(ispec_ext,dspec_ext);

  fit_bldfspec(node,ispec_dev,ispec_path,ispec_name,ispec_ext,version,
    outspec);
#else
  strcpy(outspec,inspec);
#endif
#endif

  return(outspec);
}
