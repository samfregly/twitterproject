/*******************************************************************************
Change Log:
001     12-DEC-92  AMF  Compile under Coherent.
*******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif

#if FIT_ANSI
void fit_timedif(fit_s_timebuf *time1, fit_s_timebuf *time2,
    fit_s_timebuf *rettime)
#else
void fit_timedif(time1, time2, rettime)
  fit_s_timebuf *time1, *time2, *rettime;
#endif
{
#if FIT_VMS
  lib$subx(time1->timesnap,time2->timesnap,rettime->timesnap);
#else
/**************
#if !FIT_SCO
  double dtime1, dtime2;

  dtime1 = (double ) time1->time * (double) 1000.0 + (double) time1->millitm;
  dtime2 = (double ) time2->time * (double) 1000.0 + (double) time2->millitm;

  if (dtime1 > dtime2) {
    rettime->time = (unsigned long) ((dtime1 - dtime2) / 1000.0);
    rettime->millitm =
      (unsigned short) ((unsigned long) (dtime1 - dtime2) % 1000);
  }
  else {
    rettime->time = (unsigned long) ((dtime2 - dtime1) / 1000.0);
    rettime->millitm =
      (unsigned short) ((unsigned long) (dtime2 - dtime1) % 1000);
  }
#else
************************/
  unsigned long t1;
  unsigned long t2;
  double dtime1, dtime2, timeresult;

  t1 = (unsigned long) time1->time;
  t2 = (unsigned long) time1->millitm;
  dtime1 = t1;
  dtime1 *= 1000.0;
  dtime1 += t2;

  t1 = (unsigned long) time2->time;
  t2 = (unsigned long) time2->millitm;
  dtime2 = t1;
  dtime2 *= 1000.0;
  dtime2 += t2;

  dtime1 -= dtime2;
  if (dtime1 < 0.0) dtime1 = -dtime1;
  timeresult = dtime1 / 1000.0;
  rettime->time = timeresult;
  dtime2 = rettime->time * 1000.0;
  dtime1 -= dtime2;
  rettime->millitm = dtime1;
#endif
}
