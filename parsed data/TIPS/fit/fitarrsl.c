/******************************************************************************
Module Name : fit_arr_slist                                     fitarrsl.c

Function : Creates a string array from a string list.

Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function, fit_arr_slist, is used to create a string array from
	a string list. The string array will have two additional entries
	appended to the end, the second to the last being a zero length
	string, and the last being a NULL pointer.

Calling Sequence : char *(*sarray) = 
			fit_arr_slist(struct fit_s_slist *slist);

  slist         String list structure from which array is to be created.
  sarray        Returned array of strings.

Notes: 

Change log : 
000     16-JUL-92  AMF  Created.
001     12-DEC-92  AMF  Compile under Coherent.
002     25-JUN-93  AMF  Compile under SUN
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
char *(*fit_arr_slist(struct fit_s_slist *slist))
#else
char *(*fit_arr_slist(slist))
  struct fit_s_slist *slist;
#endif
{
  char *(*sarray) = (char *(*)) NULL;
  struct fit_s_snode *entry;
  int i;

  if (!slist || !(sarray = (char *(*)) calloc(slist->nelements + 2,
      sizeof(*sarray)))) return sarray;

  for (entry = slist->first, i=0; i < slist->nelements && entry;
      entry = entry->next, ++i) {
    if (!(sarray[i] = (char *) calloc(entry->len + 1, 1)))
      goto ERREXIT;
    strcpy(sarray[i], entry->val);
  }
  if (!(sarray[i++] = (char *) calloc(2,1))) goto ERREXIT;
  sarray[i] = (char *) NULL;
  goto DONE;

ERREXIT:
  fit_delfilelist(&sarray);

DONE:
  return sarray;
}
