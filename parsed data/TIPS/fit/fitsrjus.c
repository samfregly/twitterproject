/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_srjust                                        srjust.c

Function : Right justifies text in buffer.

Author : A. Fregly

Abstract : 

Calling Sequence : fit_srjust(char *str, int width);

	str     Buffer containing text to be right justified.
	width   Lenght of "str".

Notes : 

Change log :
000     1990?  AMF      Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
void fit_srjust(char *str, int width)
#else
void fit_srjust(str,width)
  char *str;
  int width;
#endif
{
  int n,destn;
  for (n=width-1; n >= 0 && *(str+n) == ' '; --n)
    ;
  if (n >= 0 && n != width-1)
    for (destn = width-1; n >= 0; --n, --destn) {
      *(str+destn) = *(str+n);
      *(str+n) = ' ';
    }
}
