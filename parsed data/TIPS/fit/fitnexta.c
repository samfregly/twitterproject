/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_nextarg                                       nextarg.c

Function : Returns next arguement from list of arguements.

Author : A. Fregly

Abstract : This routine is used to retrieve successive arguements from
	a list of comma separated arguements.

Calling Sequence : newlist = fit_nextarg(char *list, char *retarg);

	list    List of comma separated arguements.
	retarg  Returned next arguement.
	newlist Returned list with retarg removed.

Notes :

Change log :
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
char *fit_nextarg(char *list, char *retarg)
#else
char *fit_nextarg(list, retarg)
  char *list, *retarg;
#endif
{
  char *enditem, *delimptr;

  *retarg = 0;

  if (list != NULL) {
    enditem=strchr(list, ',');
    if (enditem == NULL) enditem=strchr(list, ' ');
    if (enditem == NULL) enditem=strchr(list, 0);
    delimptr = enditem;
    while (enditem != NULL && *enditem && (*enditem == ',' || *enditem == ' '))
      ++enditem;

    if (delimptr != NULL && delimptr != list) {
      strncpy(retarg, list, (int) (delimptr-list));
      *(retarg+ (int) (delimptr - list)) = 0;
      return enditem;
    }
    else
      return NULL;
  }
  else
    return NULL;
}
