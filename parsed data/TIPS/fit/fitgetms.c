/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_getmsg                                        getmsg.c

Function : Returns status message corresponding to status code.

Author : A. Fregly

Abstract : Returns a pointer at an error or status message corresponding
	to the supplied message number. Typically this routine is used to
	display error messages after an error return from a SeekWare
	library function.


Calling Sequence : charptr = fit_getmsg(int statuscode);

Notes : 

Change log : 
000     06-FEB-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
char *fit_getmsg(int stat)
#else
char *fit_getmsg(stat)
  int stat;
#endif
{
  static char *normal_msg[] = {
    "Success"
  };

  static char *warning_msg[] = {
    "Warning Placeholder"
  };

  static char *error_msg[] = {
    "Invalid query term",
    "Misplaced or invalid query logic operator",
    "Missing or misplaced parenthesis",
    "Null term",
    "Invalid use of proximity operator",
    "Misplaced or invalid field specification",
    "Invalid use of threshhold operator",
    "File open error",
    "Read error",
    "Write error",
    "File close error",
    "Channel assign error"
  };

  static char *fatal_msg[] = {
    "Memory allocation error"
  };

  int msgno;

  msgno = stat >> 4;

  if ((stat & 0x1) && msgno == 0)
    return(normal_msg[msgno]);
  else if ((stat & 0x2) && msgno >= 0 && msgno < 1)
    return(warning_msg[msgno]);
  else if ((stat & 0x4) && msgno >= 0 && msgno < 11)
    return(error_msg[stat >> 4]);
  else if ((stat & 0x8) && msgno >= 0 && msgno < 1)
    return(fatal_msg[stat >> 4]);
  else
    return("Invalid status code");
}
