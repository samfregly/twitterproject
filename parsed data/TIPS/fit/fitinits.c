/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_init_slist                                    fitinits.c

Function : Initializes a string list.

Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function, fit_init_slist, is used to initialize a new list.

Calling Sequence : struct fit_s_slist *slist = fit_init_slist();

  slist         Returned pointer at the string list container structure.

Notes: 

Change log : 
000     16-JUL-92  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
struct fit_s_slist *fit_init_slist(void)
#else
struct fit_s_slist *fit_init_slist()
#endif
{
  struct fit_s_slist *slist;

  if (slist = (struct fit_s_slist *) calloc(1, sizeof(*slist)))
    slist->first = slist->last = (struct fit_s_snode *) NULL;

  return slist;
}
