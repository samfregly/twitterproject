/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_ssort                                         ssort.c

Function : 
Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function is used to sort a list of strings. The strings will be
	sorted by swapping the values of the pointers and lengths within
	the supplied nodes of the list.

Calling Sequence : int retstat = fit_sort_slist(struct fit_s_slist *slist);

  slist         List of strings to be sorted.
  ss            Returned status.

Notes: 

Change log : 
000     15-OCT-91  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
int fit_sort_slist(struct fit_s_slist *slist)
#else
int fit_sort_slist(slist)
  struct fit_s_slist *slist;
#endif
{
  char *temps;
  int i, didswap, templ, compare, n;
  struct fit_s_snode *curentry, *prev;

  if (!slist || !slist->nelements) return 0;

  n = slist->nelements;

  /* Do a sort on the list */
  for (didswap = 1; n > 1 && didswap; --n) {
    didswap = 0;
    for (i=0, prev=slist->first; i < n-1; ++i, prev = curentry) {
      curentry = prev->next;
      if ((compare = strncmp(prev->val, curentry->val, 
	  min(prev->len, curentry->len))) >= 0)
	if (compare || !compare && prev->len > curentry->len) {
	  didswap = 1;
	  temps = prev->val;
	  templ = prev->len;
	  prev->val = curentry->val;
	  prev->len = curentry->len;
	  curentry->val = temps;
	  curentry->len = templ;
	}
    }
  }
  return 1;
}
