/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_scenter                                       scenter.c

Function : Centers text in string.

Author : A. Fregly

Abstract : This routine is used to center the non-blank text in the
	string. The string is padded with blanks and a zero byte is put
	at the end of the string at offset "width".

Calling Sequence : int endoffset = fit_scenter(char *str, int width);

	endoffset       Offset of last non-blank in str.
	str             String to have text centered in.
	width           Usable length of string buffer.

Notes : 

Change log :
000     1989?  AMF      Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
int fit_scenter(char *str, int width)
#else
int fit_scenter(str,width)
  char *str;
  int width;
#endif
{
  int i, offset, eotoff;
  eotoff = fit_sljust(str,width);
  offset = (width - eotoff)/2;
  if (offset) {
    for (i=eotoff; i >= 0; --i) {
      *(str+i+offset) = *(str+i);
      *(str+i) = ' ';
    }
  }
  return(eotoff+offset);
}
