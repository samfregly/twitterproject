/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_dirlist                                       fitfilel.c

Function : Creates in memory list of directories in specified directory.

Author : A. Fregly

Abstract : This function is used to create an in memory list of directories
	who have entries in the supplied directory. This list should always
	include the specified directory as ".", and it's parent as "..".

Calling Sequence : char *(*dirlist) = fit_dirlist(char *dirname, int *ndirs);

  dirname       Name of directory to scan. This name must be a fully expanded
		name. Note that fit_abspath may be used to expand relative path
		names to full path names.
  nfiles        Returned number of entries in returned directory list.
  dirlist       Returned directory list. The list has two terminating entries,
		the first (dirlist[*nfiles]) is a zero length string, and the 
		second (dirlist[*nfiles + 1]) is a NULL pointer.

Notes: 
000     fit_delfilelist may be used to delete a directory list.

Change log : 
000     19-SEP-91  AMF  Created
001     12-JUL-92  AMF  Modified to use new version of fit_filelist.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_UNIX
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#endif

#if FIT_TURBOC
#include <dir.h>
#include <dos.h>
#endif

#include <findstat.h>

#if FIT_ANSI
char *getcwd(char *buffer, int length);
#else
char *getcwd();
#endif

extern int errno;


#if FIT_ANSI
char *(*fit_dirlist(char *dirname, int *ndirs))
#else
char *(*fit_dirlist(dirname, ndirs))
  char *dirname;
  int *ndirs;
#endif
{

  static char *curpath = NULL;
  char fullpath[FIT_FULLFSPECLEN+1];

  if (curpath == NULL) curpath = getcwd(curpath, 0);

#if FIT_UNIX
  return fit_filelist(dirname, "", S_IFDIR, curpath, ndirs, fullpath);
#endif
#if FIT_DOS
#if !FIT_MSC
  return fit_filelist(dirname, "", FA_DIREC, curpath, ndirs, fullpath);
#else
  return fit_filelist(dirname, "", _A_SUBDIR, curpath, ndirs, fullpath);
#endif
#endif
}
