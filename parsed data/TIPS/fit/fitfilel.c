/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_filelist                                      fitfilel.c

Function : Creates in memory list of qualifying files from specified path.

Author : A. Fregly

Abstract : This function is used to create an in memory list of qualifying
	files from a specified directory. All "regular" files, or only
	"regular" files with a specified extension may be selected.

Calling Sequence : char *(*filelist) = fit_filelist(char *dirname, char *ftype, 
    unsigned attrib, char *curpath, int *nfiles, char *fullpath);

  dirname       Name of directory containing files. Dirname may be a relative
		path name.
  ftype         File type of files to be selected, or "" if no files are to
		be selected.
  attrib        Attribute for file to be selected. For UNIX, this should
		be a combination of the following values from sys/stat.h:
			S_IFDIR  - Directory
			S_IFCHR  - Character special file.
			S_IFBLK  - Block special file.
			S_IFREG  - Regular file.
			S_IFIFO  - Named pipe.
		For DOS, this should be ONE of the following values from
		dos.h:
			FA_RDONLY - Read-only
			FA_HIDDEN - Hidden file
			FA_SYSTEM - System file
			FA_LABEL  - Volume label
			FA_DIREC  - Directory
			FA_ARCH   - Archive bit set
			0         - Regular file
  curpath       Absolute path to use in resolving relative path names.
  nfiles        Returned number of matching files placed in returne file list.
  fullpath      Returned "absolute" path.
  filelist      Returned file list. File list has two terminating entries,
		the first (filelist[*nfiles]) is a zero length string, and the 
		second (filelist[*nfiles + 1]) is a NULL pointer.

Notes: 
000     fit_delfilelist may be used to delete a file list.

Change log : 
000     04-SEP-91  AMF  Created
001     12-DEC-92  AMF  Compile under Coherent
002     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#include <findstat.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_UNIX || FIT_COHERENT
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#endif
#if FIT_TURBOC
#include <dir.h>
#include <dos.h>
#endif
#if FIT_ZORTECH
#include <dos.h>
#endif

#if FIT_COHERENT
extern int errno;
#endif

#if FIT_ANSI
char *(*fit_filelist(char *dirname, char *ftype, unsigned attrib,
   char *curpath, int *nfiles, char *fullpath))
#else
char *(*fit_filelist(dirname, ftype, attrib, curpath, nfiles, fullpath))
  char *dirname, *ftype;
  unsigned attrib;
  char *curpath;
  int *nfiles;
  char *fullpath;
#endif
{


#if FIT_UNIX || FIT_COHERENT
  DIR *dir;
  struct dirent *direntry;
  char *(*filelist);
  char fullspec[FIT_FULLFSPECLEN+1], *extension;
  struct stat statbuf;
  struct fit_s_slist *wlist;

  /* Initialize locals so that error handling work correctly */
  *nfiles = 0;
  fit_abspath(dirname, curpath, fullpath);
  filelist = (char *(*)) NULL;

  if (!(wlist = fit_init_slist())) goto DONE;

  /* Open directory */
  if (!(dir = opendir(dirname))) goto DONE;

  /* Put matching entries from directory into temp file */
  while (direntry = readdir(dir)) {
    fit_expfspec(direntry->d_name, "", fullpath, fullspec);
    if (!stat(fullspec, &statbuf) && (statbuf.st_mode & S_IFMT & attrib)) {
      if (!*ftype || ((extension = strrchr(direntry->d_name,'.')) &&
	  extension != direntry->d_name && !strcmp(extension,ftype))) {
	if (!fit_add_slist(wlist, direntry->d_name)) goto DONE;
	*nfiles += 1;
      }
    }
  }

  /* Close directory and temp file */
  closedir(dir);
  dir = (DIR *) NULL;

#else

#if FIT_DOS
  FIND_CONTEXT context;
#endif
#if FIT_VMS
  void *context = NULL;
#endif
  int done;
  char *(*filelist);
  char fullspec[FIT_FULLFSPECLEN+1];
  struct fit_s_slist *wlist;

#if FIT_VMS
  char fname[FIT_FULLFSPECLEN+1], extension[FIT_FULLFSPECLEN+1], ver[FIT_FULLFSPECLEN+1];
  char dummy[FIT_FULLFSPECLEN+1];
#else
  char *extension;
#endif

  /* printf("fit_filelist:\n..dirname: %s\n..ftype: %s\n..attrib: %d\n..curpath: %s\n",
	  dirname,ftype,attrib,curpath); */
  /* Initialize locals so that error handling work correctly */
  *nfiles = 0;
  fit_abspath(dirname, curpath, fullpath);
  strcpy(fullspec, fullpath);
#if !FIT_VMS
  strcat(fullspec, "*.*");
#else
  if (attrib & S_IFDIR)
    strcat(fullspec, "*.DIR");
  else
    strcat(fullspec, "*.*;*");
#endif

  filelist = NULL;

  if ((wlist = fit_init_slist()) == NULL) {
	  perror("fit_filelist, error from fit_init_slist");
	  goto DONE;
  }

#if FIT_TURBOC
  for (done = findfirst(fullspec, &context, attrib); !done;
      done = findnext(&context)) {
    if (!*ftype || ((extension = strrchr(context.ff_name,'.')) != NULL &&
	  extension != context.ff_name && !strcmp(extension,ftype))) {
      if (!fit_add_slist(wlist, context.ff_name)) break;
      *nfiles += 1;
    }
  }
#endif
#if FIT_ZORTECH
  for (context = findfirst(fullspec, attrib); context != NULL;
      context = findnext()) {
    if (!*ftype || ((extension = strrchr(context->name,'.')) != NULL &&
	  extension != context->name && !strcmp(extension,ftype))) {
      if (!fit_add_slist(wlist, context->name)) break;
      *nfiles += 1;
    }
  }
#endif
#if FIT_MSC
{
  long handle;
  if ((handle = _findfirst(fullspec, &context)) != -1L) {
		/* printf("fit_filelist, found first file: %s\n",context.name); */
    do {
		/* printf("fit_filelist, checking file: %s\n",context.name); */
	  if ((!*ftype || ((extension = strrchr(context.name,'.')) != NULL &&
	      extension != context.name && !strcmp(extension,ftype))) &&
	      (attrib & context.attrib || (!attrib && !(context.attrib & _A_SUBDIR)))) {
		  /* printf("fit_filelist, file matches search criteria\n"); */
        if (!fit_add_slist(wlist, context.name)) break;
        *nfiles += 1;
	  }
	  else {
		  ; /* printf("fit_filelist, file did no match search criteria\n"); */
	  }
	}
    while (!_findnext(handle, &context));
	_findclose(handle);
  }
  else {
	  perror("fit_filelist, call to _findfirst failed");
  }
}
#endif
#if FIT_VMS
  while (*fit_findfile(&context, fullspec, "", curpath, fullspec, fname)) {
    fit_prsfspec(fname, dummy, dummy, dummy, fname, extension, ver);
    if (!*ftype || !strcmp(extension,ftype)) {
      strcat(fname, extension);
      strcat(fname, ver);
      if (!fit_add_slist(wlist, fname)) break;
      *nfiles += 1;
    }
  }
#endif
#endif

  if (*nfiles) {
    if (!fit_sort_slist(wlist)) goto DONE;
    filelist = fit_arr_slist(wlist);
  }

DONE:
  if (wlist) fit_del_slist(&wlist);

  return filelist;
}
