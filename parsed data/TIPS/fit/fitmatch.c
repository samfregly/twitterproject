/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
MODULE NAME : fit_match                                         fitmatch.c
 
FUNCTION : Match wildcarded string

AUTHOR : B. Wolfe                                               25-FEB-85
 
ABSTRACT : This routine will match a character string against a
	   wild carded string. VAX wild cards * % can be used in
	   the wild card string. Trailing blanks are ignored.
 
CALLING SEQUENCE : SS = fit_match(unsigned char *string, unsigned char *wild, 
	int nchar)

  string        String to match
  wild          Wildcarded match string null terminated
  nchar         Number of characters to match (if 0 match to NULL)
  ss            Status (1 or 0)

NOTES : 
 
CHANGE LOG : 
000     24-FEB-92  AMF  Adapted from Bryan Wolfes famous match function.
001		18-JAN-99  AMF	Fixed index bounds error into "stack".
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
int fit_match (unsigned char *string, unsigned char *wild, int nchar)
#else
int fit_match (string, wild, nchar)
  unsigned char *string, *wild;
  int nchar;
#endif
{
  int ss=0, idx, jdx, slen=nchar, wlen, top, stack[9][2];

  /* Find lengths and remove trailing space*/
  if (!slen) {
    for (slen = strlen((char *) string); slen>0 && string[slen-1] <= ' '; 
	--slen)
      ;
  }
  for (wlen = strlen((char *) wild); wlen>0 && wild[wlen-1] <= ' ';  --wlen)
    ;

  /* Null strings match */
  if (wlen == 0 && slen == 0) return(1);

  /* Compare strings first */
  if ( slen > 0 && wlen > 0 && slen == wlen) {

    ss = !strncmp((char *) string, (char *) wild, slen); /* 0 means equal */

    /*If no wildcards, result of string comparison is the answer */
    if (strchr((char *) wild, '*') == NULL && 
	strchr((char *) wild, '%') == NULL) return(ss);
  }

  /* Has wildcards */
  idx = 0;
  jdx = 0;
  top = 0;

  while (jdx < wlen ) {
    
    if ( wild[jdx] == '*' ) {
      if (jdx == wlen-1) return (1);
      stack[top][0] = idx;
      stack[top][1] = jdx;
      top++;
      if (top >= 9) return (0);
    }
    else if (wild[jdx] == '%' ) {
      if (idx >= slen) goto SKIP;
      idx++;
    }
    else {
       if ( idx >= slen || string[idx] != wild[jdx] ) goto SKIP;
       idx++;
    }
    jdx++;
    if ( jdx < wlen || idx >= slen ) continue;

SKIP:
    idx = 9999;
    while (idx >= slen ) {
      if ( top <= 0 ) return (ss);
      --top;
      idx = stack[top][0];
      jdx = stack[top][1];
      idx++;
    }
  }
  
  return (1);
}


