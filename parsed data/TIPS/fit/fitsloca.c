/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_slocate                                       slocate.c

Function : Finds offset of a substring.

Author : A. Fregly

Abstract : This routine is use to find the offset of a substring within
	another string. If the substring cannot be found, a value of -1
	is returned.

Calling Sequence : offset = fit_slocate(char *str, char *substr);

	str     String to be searched.
	substr  Substring to search for.
	offset  Location of substring in string.

Notes : 

Change log :
000     1990? AMF       Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

#if FIT_ANSI
int fit_slocate(char *str, char *substr)
#else
int fit_slocate(str, substr)
  char *str, *substr;
#endif
{
  char *startstr, *subptr, *startsubstr;

  for (startstr = str, startsubstr = substr; *substr && *str; str++)
    if (*str == *substr)
      for (subptr = str++, substr = startsubstr+1;
	   *substr && (*substr == *str);
	   ++substr, ++str)
	;

  if (!*substr)
    return((int) (ptr_dif(subptr,startstr)));
  else
    return(-1);
}
