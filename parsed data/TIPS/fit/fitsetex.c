/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_setext                                        setext.c

Function : Set file name extension.

Author : A. Fregly

Abstract : This routine is used to set the file name extension to the
  specified value.

Calling Sequence : char *retfspec = fit_setext(char *inname, char *ext,
  char *outspec);

  inname        Input file specification.
  ext           Extension to be set, should start with a '.'.
  outspec       Returned file name with new extension.  Caller must allocate
		at list FIT_FULLFSPECLEN+1 bytes for outspec.

Notes :

Change log :
000     30-SEP-90  AMF  Created.
001     27-JUL-93  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
char *fit_setext(char *inname, char *ext, char *outspec)
#else
char *fit_setext(inname,ext,outspec)
  char *inname,*ext,*outspec;
#endif
{
  int offset;
  char *curext, *endpath;

  if (inname != NULL) {
    curext = strrchr(inname,'.');
#if FIT_VMS
    endpath = strrchr(inname,']');
    if (endpath == NULL) endpath = strrchr(inname,':');
#else
#if FIT_DOS || FIT_TURBOC
    endpath = strrchr(inname,'\\');
    if (endpath == NULL) endpath = strrchr(inname,':');
    if (endpath == NULL) endpath = strchr(inname,'.');
    if (endpath != NULL && *(endpath+1) == '.') endpath = endpath+1;
#else
    endpath = strrchr(inname,'/');
#endif
#endif
    if (endpath != NULL && endpath > curext) curext = NULL;

    if (curext == NULL) curext = strchr(inname,' ');
    if (curext == NULL) curext = strchr(inname,'\0');

    offset = min(FIT_FULLFSPECLEN-1, ptr_dif(curext,inname));
    if (offset)
      strncpy(outspec,inname,offset);
    offset -= 1;
    fit_sadd(outspec,FIT_FULLFSPECLEN,ext,&offset);
    outspec[min(FIT_FULLFSPECLEN,offset+1)] = 0;
  }
  else
    strcpy(outspec,ext);

  return(outspec);
}
