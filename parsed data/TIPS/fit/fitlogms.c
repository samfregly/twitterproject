/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_logmsg                                        logmsg.c

Function : Writes log message.

Author : A. Fregly

Abstract : This function writes a time stampted log message in a standard 
	format to the specified output file.

Calling Sequence : fit_logmsg(FILE *out, char *msg, int stat);

	out     FILE handle for output file.
	msg     Log message to be written.
	stat    If it is not 1, then the SeekWare status message 
		corresponding to the stat value is printed on a line 
		following the message line.

Notes : 

Change log : 
000     04-MAY-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <time.h>
#include <string.h>

#if FIT_ANSI
void fit_logmsg(FILE *out, char *msg, int stat)
#else
void fit_logmsg(out, msg, stat)
  FILE *out;
  char *msg;
  int stat;
#endif
{
  char timestr[30];

#if FIT_UNIX
  long systime;
#else
  time_t systime;
#endif

  time(&systime);
  strcpy(timestr,ctime(&systime));
  timestr[strlen(timestr)-1] = 0;
  fprintf(out,"%s: %s\n",timestr+4,msg);
  if (stat != 1) fprintf(out,"%s\n",fit_getmsg(stat));
}
