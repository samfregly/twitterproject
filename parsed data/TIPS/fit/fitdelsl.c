/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_del_slist                                     fitdelsl.c

Function : Deletes a string list.

Author : A. Fregly

Abstract : Several of the  fit_xxxx functions have been designed for the
	manipulation of linked lists of strings. These functions, have
	names formed from the following template: fit_xxxxx_slist. This
	function, fit_del_slist, is used to delete the string list,
	free up any storage allocated to the list.

Calling Sequence : fit_del_slist(struct fit_s_slist *(*slist));

  slist         Pointer at string list to be deleted. *slist will be
		set to null by this function.

Notes: 

Change log : 
000     17-JUL-92  AMF  Created.
001     12-DEC-92  AMF  Compile under Coherent.
002     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
void fit_del_slist(struct fit_s_slist *(*slist))
#else
void fit_del_slist(slist)
  struct fit_s_slist *(*slist);
#endif
{
  struct fit_s_snode *prev, *next;

  if (slist && *slist) {
    for (next = (*slist)->first; next;) {
      prev = next;
      next = next->next;
      if (prev->val)
	free(prev->val);
      free(prev);
    }
    free (*slist);
    *slist = (struct fit_s_slist *) NULL;
  }
}
