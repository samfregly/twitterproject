#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>

int fit_sockServer(int port) {
  struct sockaddr_in servAddr;
  int sd;

  /* create socket */
  if (sd = socket(AF_INET, SOCK_STREAM, 0)) {
    return sd;
  }

  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(port);

  if(bind(sd, (struct sockaddr *) &servAddr, sizeof(servAddr))<0) {
    return -1;
  }
}
