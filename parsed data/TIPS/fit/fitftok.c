/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_ftok                                          ftok.c

Function : Emulates ftok function of UNIX System V.

Author : A. Fregly

Abstract : This routine is used to generate a token based on the supplied
	file specification. The algorithm used by this routine does not
	use the i-node for the file, so the file spec can actually be any
	user selected string.

Calling Sequence : int token = fit_ftok(char *fspec, char modifier);

Notes: 

Change log : 
000     12-JUN-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
int fit_ftok(char *fspec, char modifier)
#else
int fit_ftok(fspec, modifier)
  char *fspec;
  char modifier;
#endif
{
  long accumulator;
  srand((unsigned) modifier * 256);
  for (accumulator = rand(); *fspec; ++fspec)
    accumulator += rand() * *fspec;
  return (int) accumulator;
}
