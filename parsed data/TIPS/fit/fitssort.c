/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_ssort                                         ssort.c

Function : This function is used to sort a list of strings. The strings will
	be sorted by swapping the values of the pointers and lengths within
	the supplied array.

Author : A. Fregly

Abstract : 

Calling Sequence : int retstat = fit_ssort(char *(*inlist), unsigned len[],
    int n);

  inlist        List of strings to be sorted.
  len           Array of string lengths.
  n             Optional number of entries to be sorted. If n is supplied as
		zero, ssort will calculate it's value by looking for a 
		terminating 0 length or NULL entry in "inlist".

Notes: 

Change log : 
000     15-OCT-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
int fit_ssort(char *(*inlist), unsigned len[], int n)
#else
int fit_ssort(inlist, len, n)
  char *(*inlist);
  unsigned len[];
  int n;
#endif
{
  char *temps;
  int i, didswap, templ, compare;

  if (!n)
    /* Determine number of elements in list */
    for (n=0; inlist[n] != NULL && inlist[n][0]; ++n)
      ;

  /* Do a sort on the list */
  for (didswap = 1; n > 1 && didswap; --n) {
    didswap = 0;
    for (i=1; i < n-1; ++i)
      if ((compare = strncmp(inlist[i], inlist[i+1], 
	  min(len[i], len[i+1]))) >= 0)
	if (compare || !compare && len[i] > len[i+1]) {
	  didswap = 1;
	  temps = inlist[i];
	  templ = len[i];
	  inlist[i] = inlist[i+1];
	  len[i] = len[i+1];
	  inlist[i+1] = temps;
	  len[i+1] = templ;
	}
  }
  return 1;
}
