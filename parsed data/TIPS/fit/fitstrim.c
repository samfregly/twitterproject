/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_strim                                         strim.c

Function : Trims string to last non-blank, non-null character.

Author : A. Fregly

Abstract : 

Calling Sequence : char *strptr = fit_strim(char *str, unsigned int *len);

	strptr  Points at beginning of trimmed string.
	str     String to trim.
	len     On input, if 0 the fit_strim will compute the input length of
		str, otherwise, it is assumed to point at the point at which
		to start looking backwards during the trim. On return, len
		will contain the length of the trimmed string.

Notes : 

Change log : 
000     15-FEB-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
char *fit_strim(char *str, unsigned int *len)
#else
char *fit_strim(str, len)
  char *str;
  unsigned int *len;
#endif
{

  char *endstr;

  if (!*len) *len = strlen(str);

  for (endstr = str + *len - 1; ptr_ge(endstr,str) && (!*endstr ||
      *endstr == ' '); --endstr)
    ;

  *(++endstr) = 0;
  *len = ptr_dif(endstr,str);
  return(str);
}
