/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_wildlist                                      fitwildl.c

Function : Creates in memory list of qualifying files from specified path.

Author : A. Fregly

Abstract : This function is used to create an in memory list of qualifying
	files from a specified directory. Only files which match the supplied
	list of file name filters will be put in the list. 

Calling Sequence : char *(*filelist) = fit_wildlist(char *dirname, 
    char *wildlist, unsigned attrib, char *curpath, int *nfiles, 
    char *fullpath);

  dirname       Name of directory containing files. Dirname may be a relative
		path name.
  wildlist      Filter to use in selecting files for list. wildlist may be
		a single file name, a wild carded file name, or a comma
		separated list of file names and/or wild carded file names.
		The valid wild cards are "*", which matches on 0 or more
		characters, and "%", which matches on a single character.
  attrib        Attribute for file to be selected. For UNIX, this should
		be a combination of the following values from sys/stat.h:
			S_IFDIR  - Directory
			S_IFCHR  - Character special file.
			S_IFBLK  - Block special file.
			S_IFREG  - Regular file.
			S_IFIFO  - Named pipe.
		For DOS, this should be ONE of the following values from
		dos.h:
			FA_RDONLY - Read-only
			FA_HIDDEN - Hidden file
			FA_SYSTEM - System file
			FA_LABEL  - Volume label
			FA_DIREC  - Directory
			FA_ARCH   - Archive bit set
			0         - Regular file
  curpath       Absolute path to use in resolving relative path names.
  nfiles        Returned number of matching files placed in returned file list.
  fullpath      Returned "absolute" path.
  filelist      Returned file list. File list has two terminating entries,
		the first (filelist[*nfiles]) is a zero length string, and the 
		second (filelist[*nfiles + 1]) is a NULL pointer.

Notes: 
000     fit_delfilelist may be used to delete a file list.

Change log : 
000     04-SEP-91  AMF  Created
001     22-MAR-93  AMF  Compile under GNU C.
002     18-JAN-99  AMF	Fixed build of "workname" to include zero length string
                        at end.
003     19-FEB-07  AMF  Removed bug put in when making the program case
                        insensitive under DOS.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

extern int errno;

#if FIT_ANSI
char *(*fit_wildlist(char *dirname, char *wildlist, unsigned attrib,
   char *curpath, int *nfiles, char *fullpath))
#else
char *(*fit_wildlist(dirname, wildlist, attrib, curpath, nfiles,
    fullpath))
  char *dirname, *wildlist;
  unsigned attrib;
  char *curpath;
  int *nfiles;
  char *fullpath;
#endif
{
  char *(*filelist), wild[FIT_FULLFSPECLEN+1], *wlist;
  char *(*worklist), *(*workname);
  int *workname_L;
  char fullspec[FIT_FULLFSPECLEN+1], fname[FIT_FULLFSPECLEN+1];
  char ext[FIT_FULLFSPECLEN+1], ver[FIT_FULLFSPECLEN+1];
  char dummy[FIT_FULLFSPECLEN+1];
  struct fit_s_slist *slist;
  int i, nwork;

  /* printf("fit_wildlist:\n..dirname: %s\n..wildlist: %s\n..curpath: %s\n",
	  dirname,wildlist,curpath); */
  /* Initialize locals so that error handling work correctly */
  *nfiles = 0;
  fit_abspath(dirname, curpath, fullpath);
  filelist = (char *(*)) NULL;
  worklist = (char *(*)) NULL;
  workname = (char *(*)) NULL;
  workname_L = (int *) NULL;
  slist = (struct fit_s_slist *) NULL;
  nwork = 0;

  if (!(worklist = fit_filelist(fullpath, "", attrib, curpath,
      &nwork, fullspec)) || !nwork) {
    /* printf("fit_wildlist, no files found by call to fit_filelist, nwork: %d\n",nwork); */
    goto DONE;
  }

  if (nwork) {
    /* printf("fit_wildlist, fit_filelist found %d files\n",nwork); */
    if (!(workname = (char *(*)) calloc(nwork+2, sizeof(*workname)))) {
      /* printf("fit_wildlist, error allocating workname array\n"); */
      goto DONE;
    }
    if (!(workname_L = (int *) calloc(nwork+2, sizeof(*workname_L)))) {
      /* printf("fit_wildlist, error allocating workname_L array\n"); */
      goto DONE;
    }
    for (i=0; i < nwork; ++i) {
      fit_prsfspec(worklist[i], dummy, dummy, dummy, fname, ext, ver);
      strcat(fname, ext);
      strcat(fname, ver);
      workname_L[i] = strlen(fname);
	  /* printf("fit_wildlist, adding file: %s to workname array\n"); */
      if (!(workname[i] =
           (char *) calloc(workname_L[i] + 1, sizeof(*workname[i])))) {
        /* printf("fit_filelist, error allocating storage in workname array\n"); */
        goto DONE;
      }
      strcpy(workname[i], fname);
    }
    if (!(workname[i] = (char *) calloc(2,1))) goto DONE;
    fit_delfilelist(&worklist);

    if (!(slist = fit_init_slist())) {
      /* printf("fit_filelist, error allocating slist array\n"); */
     goto DONE;
    }

    for (wlist = fit_nextarg(wildlist, wild); *wild;
	    wlist = fit_nextarg(wlist, wild)) {
#if !FIT_UNIX && !FIT_COHERENT
      fit_supcase(wild);
#endif
      /* printf("fit_wildlist, matching files against wild spec: %s\n",wild); */
      /* Put matching entries from directory into temp list */
      for (i=0; i < nwork; ++i) {
        char *f;
        #if FIT_DOS
        strcpy(fname, workname[i]);
        fit_supcase(fname);
        f=fname;
        #else
        f=workname[i];
        #endif
	if (fit_match((unsigned char *) f, (unsigned char *) wild,
	  workname_L[i])) {
          /* printf("fit_wildlist, found match: %s\n",workname[i]); */
	  if (!fit_add_slist(slist, workname[i])) {
            /* printf("fit_wildlist, error adding match to slist\n"); */
            goto DONE;
          }
        }
        else {
          ; /* printf("fit_wildlist, not a match: %s\n",workname[i]); */
        }
      }
    }

    fit_delfilelist(&workname);
    free(workname_L);
    workname_L = (int *) NULL;

    /* Sort the list of matching names */
    fit_sort_slist(slist);

    /* Remove duplicates from list */
    fit_nodu_slist(slist);

    /* Create return file list and fill it in */
    filelist = fit_arr_slist(slist);

    *nfiles = slist->nelements;
  }
  goto DONE;

DONE:
  if (worklist) fit_delfilelist(&worklist);
  if (workname) fit_delfilelist(&workname);
  if (workname_L) free(workname_L);
  if (slist) fit_del_slist(&slist);

  return filelist;
}
