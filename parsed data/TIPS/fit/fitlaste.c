/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_lasterr                                       lasterr.c

Function : Returns FIT systems internal error code for last occurring
	error.

Author : A. Fregly

Abstract : This function returns the error number for the last error 
  occurring in a call to a SeekWare library routine. This error number
  may then be used for retrieving a corresponding error message using the
  fit_getmsg function. Symbolic constants for all error codes returned by
  this function are found in the include file "fit_stat.h".

Calling Sequence : int_val = fit_lasterr();

Notes : 

Change log : 
000     06-FEB-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <fit_stat.h>

int fit_status;

int fit_lasterr() {
  return(fit_status);
}
