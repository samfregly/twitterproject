/*
  alarm setting function. Used to work around fact that "alarm" function does
  not work correctly under cygwin.
*/

#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <fitsydef.h>

#if FIT_UNIX
#if ! FIT_CYGWIN
int fit_alarm(int timeout) {
  return alarm(timeout);
}
#else
static char childPid;
void fit_alarmhandler(int signal);

int fit_alarm(int timeout) {
  int pid,parentPid;
  if (timeout != 0) {
    parentPid=getpid();
    pid=fork();
    if (!pid) {
      signal(SIGINT,fit_alarmhandler);
      sleep(timeout);
      kill(parentPid,SIGALRM); 
      exit(errno);
    } 
    else {
      childPid=pid;
      return 0;
    }
  }
  else {
    kill(childPid,SIGINT);
    return 0;
  }
}

void fit_alarmhandler(int signal) {
  exit(0);
  return;
}
#endif
#endif
