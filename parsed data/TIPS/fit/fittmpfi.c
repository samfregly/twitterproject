/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_tmpfile                                       fittmpfi.c

Function : Emulates UNIX tmpfile function on system which don't have it.

Author : A. Fregly

Abstract : This function is used to emulate the UNIX tmpfile function on
	systems which don't possess it. The fit_tempclose function should
	be used to close the temp file.

Calling Sequence : FILE *handle = fit_tmpfile();

Notes: 

Change log : 
000     14-DEC-92  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
002     01-JUN-94  AMF  Fix allocation bug.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif

extern int errno;

#if FIT_COHERENT
struct fit_tmpfile_element {
  FILE *handle;
  struct fit_tmpfile_element *next;
  char fspec[FIT_FULLFSPECLEN+1];
};

static struct fit_tmpfile_element *fit_tmpfile_head = 
  (struct fit_tmpfile_element *) NULL;
static struct fit_tmpfile_element *fit_tmpfile_last = 
  (struct fit_tmpfile_element *) NULL;
#endif


FILE *fit_tmpfile()
{
#if !FIT_COHERENT
  return tmpfile();
#else
  struct fit_tmpfile_element *curelement;

  if ((curelement = 
      (struct fit_tmpfile_element *) calloc(1, sizeof(*fit_tmpfile_head))) &&
      tmpnam(curelement->fspec) &&
      (curelement->handle = fopen(curelement->fspec, "w+"))) {
    curelement->next = (struct fit_tmpfile_element *) NULL;
    
    if (!fit_tmpfile_head) {
      fit_tmpfile_head = curelement; 
      fit_tmpfile_last = curelement;
    }
    else {
      fit_tmpfile_last->next = curelement;
      fit_tmpfile_last = curelement;
    }
  }
  else {
    return (FILE *) NULL;
  }
  
  return curelement->handle; 
#endif
}


/******************************************************************************
Module Name : fit_tempclose                                     fittmpfi.c

Function : This function is used to close and delete a temp file created with
	fit_tmpfile.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = fit_tempclose(FILE *handle);

  handle        Handle for temporary file returned by fit_tmpfile.
  stat          Returned status. A value of 0 indicates an error.

Notes: 

Change log : 
000     15-DEC-92  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#if FIT_ANSI
int fit_tempclose(FILE *handle)
#else
int fit_tempclose(handle)
  FILE *handle;
#endif
{
#if FIT_COHERENT
  struct fit_tmpfile_element *prev, *cur;

  for (cur = fit_tmpfile_head, prev = (struct fit_tmpfile_element *) NULL;
      cur && cur->handle != handle;
      prev = cur)
    cur = cur->next;

  if (cur) {
    if (prev) prev->next = cur->next;
    if (cur == fit_tmpfile_head) fit_tmpfile_head = cur->next;
    if (cur == fit_tmpfile_last) fit_tmpfile_last = prev;
    free(cur);
  }
#endif

  return (handle) ? fclose(handle) : 0;
}
