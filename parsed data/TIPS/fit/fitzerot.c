/*******************************************************************************
Change Log:
001     12-DEC-92  AMF  Compile under Coherent.
*******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif

#if FIT_ANSI
void fit_zerotime(fit_s_timebuf *timbuf)
#else
void fit_zerotime(timbuf)
  fit_s_timebuf *timbuf;
#endif
{
#if FIT_VMS
  timbuf->timesnap[0] = 0;
  timbuf->timesnap[1] = 0;
#else
  timbuf->time = 0;
  timbuf->millitm = 0;
#endif
}
