/*******************************************************************************
Change Log:
001     12-DEC-92  AMF  Compile under Coherent.
*******************************************************************************/
#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_UNIX && !FIT_COHERENT
#include <sys/param.h>
#endif

#if FIT_ANSI
void fit_timeunpk(fit_s_timebuf *timebuf, unsigned long *secs,
  unsigned int *millisecs)
#else
void fit_timeunpk(timebuf, secs, millisecs)
  fit_s_timebuf *timebuf;
  unsigned long *secs;
  unsigned int *millisecs;
#endif
{
  *secs = fit_seconds(timebuf);
  *millisecs = fit_millisecs(timebuf);
  return;
}
