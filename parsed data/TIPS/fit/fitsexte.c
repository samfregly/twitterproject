/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_sextend                                       sextend.c

Function : Extends string with blanks.

Author : A. Fregly

Abstract : This routine is used to pad a string with blanks, and a terminating
  zero byte at offset "width"

Calling Sequence : fit_sextend(char *str, int width);

Notes : 

Change log :
000     1989?  AMF      Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <string.h>

#if FIT_ANSI
void fit_sextend(char *str, int width)
#else
void fit_sextend(str, width)
  char *str;
  int width;
#endif
{
  int n;
  for (n = strlen(str); n < width; ++n)
    *(str+n) = ' ';
  *(str+width) = 0;
}
