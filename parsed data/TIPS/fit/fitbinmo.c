/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_binmode                                       binmode.c

Function : Set file access mode to "binary"

Author : A. Fregly

Abstract : This function is used in PC executables compiled with Borland C.
	Borland C opens files by default in "text mode", and performs
	cr/lf translations. This function is used to change the access mode
	of a file to "binary", turning off the cr/lf translation.

Calling Sequence : stat = fit_binmode(FILE *handle);

	handle          FILE structure for file whose mode is to be changed.
	stat            Returned status, 0 for error.

Notes:

Change log :
000     22-JUL-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_DOS
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <io.h>
#include <fcntl.h>
#endif

#if FIT_ANSI
int fit_binmode(FILE *handle)
#else
int fit_binmode(handle)
  FILE *handle;
#endif
{
#if FIT_TURBOC
  return !setmode(handle->fd, O_BINARY);
#else
#if FIT_ZORTECH
  handle->_flag &= ~0x100;
#endif
#if FIT_MSC
  return (_setmode( _fileno( handle ), _O_BINARY ) != -1);
#endif
  return 1;
#endif
}


