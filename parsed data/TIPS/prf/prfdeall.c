/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_dealloc					dealloc.c

Function : Deallocates a profile executable.

Author : A. Fregly

Abstract : This routine release all space allocated to the supplied profile
	executable.

Calling Sequence : stat = prf_dealloc(char *exe);

Notes :

Change log :
000	08-MAY-91  AMF	Deallocate field names as a single block.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>

#if FIT_ANSI
int prf_dealloc(char *(*xe))
#else
int prf_dealloc(xe)
  char *(*xe);
#endif
{

  struct prf_s_exe *exe;
  int i;

  if (*xe == NULL) goto  DONE;

  exe = (struct prf_s_exe *) *xe;

  /* term translation table */
  if (exe->term_xlate)
    free(exe->term_xlate);

  /* search character translation table */
  if (exe->tx_xlate)
    free(exe->tx_xlate);

  /* match registers */
  if (exe->table.registers) {
    for (i=0; i < exe->regwidth; ++i)
      if (*(exe->table.registers+i))
	free(*(exe->table.registers+i));
    free(exe->table.registers);
    exe->table.registers = (FIT_REG *(*)) NULL;
  }

  /* End of term masks */
  if (exe->table.eotflag) {
    free(exe->table.eotflag);
    exe->table.eotflag = (FIT_REG *) NULL;
  }

  /* phrase word match registers */
  if (exe->phrasetbl.registers) {
    for (i=0; i < exe->phraseregwidth; ++i)
      if (*(exe->phrasetbl.registers+i))
	free(*(exe->phrasetbl.registers+i));
    free(exe->phrasetbl.registers);
    exe->phrasetbl.registers = (FIT_REG *(*)) NULL;
  }

  /* End of phrase word masks */
  if (exe->phrasetbl.eotflag) {
    free(exe->phrasetbl.eotflag);
    exe->phrasetbl.eotflag = (FIT_REG *) NULL;
  }

  /* mask registers */
  if (exe->phrasemask) {
    free(exe->phrasemask);
    exe->phrasemask = (FIT_REG *) NULL;
  }

  if (exe->prox1mask) {
    free(exe->prox1mask);
    exe->prox1mask = (FIT_REG *) NULL;
  }

  if (exe->phraseprox1mask) {
    free(exe->phraseprox1mask);
    exe->phraseprox1mask = (FIT_REG *) NULL;
  }

  if (exe->prox2mask) {
    free(exe->prox2mask);
    exe->prox2mask = (FIT_REG *) NULL;
  }

  if (exe->phraseprox2mask) {
    free(exe->phraseprox2mask);
    exe->phraseprox2mask = (FIT_REG *) NULL;
  }

  if (exe->phraseproxmask) {
    free(exe->phraseproxmask);
    exe->phraseproxmask = (FIT_REG *) NULL;
  }

  /* list of terms in profile executable */
  if (exe->term) {
    for (i = 0; i < exe->nterms; ++i) {
      if (*(exe->term+i)) {
	/* deallocate storage for term value */
	if ((*(exe->term+i))->fieldmap)
	  free((*(exe->term+i))->fieldmap);
	if ((*(exe->term+i))->logicid)
	  free((*(exe->term+i))->logicid);
	if ((*(exe->term+i))->val)
	  free((*(exe->term+i))->val);
	free(*(exe->term+i));
      }
    }
    /* free the term list structures */
    free(exe->term);
    exe->term = (struct prf_s_term *(*)) NULL;
  }

  /* list of phrase words in profile executable */
  if (exe->phraseterm) {
    for (i = 0; i < exe->nphraseterms; ++i) {
      if (*(exe->phraseterm+i)) {
	/* deallocate storage for term value */
	if ((*(exe->phraseterm+i))->val)
	  free((*(exe->phraseterm+i))->val);
	free(*(exe->phraseterm+i));
      }
    }
    /* free the phrase word list structures */
    free(exe->phraseterm);
    exe->phraseterm = (struct prf_s_phraseterm *(*)) NULL;
  }

  /* Proximity descriptors */
  if (exe->proximity) {
    free(exe->proximity);
    exe->proximity = (struct prf_s_proximity *) NULL;
  }

  /* Proximity first term map */
  if (exe->proxterm1map) {
    free(exe->proxterm1map);
    exe->proxterm1map = (int *) NULL;
  }

  /* Proximity second term map */
  if (exe->proxterm2map) {
    free(exe->proxterm2map);
    exe->proxterm2map = (int *) NULL;
  }

  if (exe->termtype) {
    free(exe->termtype);
    exe->termtype = (int *) NULL;
  }


  /* logic equations, query names, hit file names */
  for (i=0; i < exe->nqueries; ++i) {
    /* Free logic equation values */
    if (*(exe->logic+i))
      free(*(exe->logic+i));

    /* query name values */
    if (*(exe->qfiles+i))
      free(*(exe->qfiles+i));
  }

  /* Logic equation lengths */
  if (exe->logiclen) free (exe->logiclen);

  /* the list of pointers at logic equations */
  if (exe->logic) free(exe->logic);

  /* the list of pointers at query names */
  if (exe->qfiles) free(exe->qfiles);

  /* list of field names in profile executable */
  if (exe->fields) {
    free(*exe->fields);
    free(exe->fields);
  }

  free(exe);
  *xe = (char *) NULL;

DONE:
  return 1;
}
