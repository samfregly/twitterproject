/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_expterms					expterms.c

Function : Expands profile exe to accomodate more terms.

Author : A. Fregly

Abstract : This routine is called to expand a profile executable so that
	it can accomodate new terms.  The number of entries by which the
	profile executable is expandded is equal to the number of bits
	in a long integer.  The elements of the executable which are
	expanded include the match table and the term list.

Calling Sequence :
	stat = prf_expterms(struct prf_s_exe *exe, int nlong);

	exe	Profile executable to be expanded.
	nlong	Number of longwords by which match table register widths
		should be increased.  The storage for unique terms in the
		profile executable is expanded by this amount times the
		number of bits in a longword.

Notes :

Change log :
000	24-MAY-90  AMF	Created
001	29-APR-91  AMF  Removed reference to "newproximitymask".
002	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_expterms(struct prf_s_exe *exe, int nlong)
#else
int prf_expterms(exe, nlong)
  struct prf_s_exe *exe;
  int nlong;
#endif
{

  int newwidth,i,j,k,tblidx;
  FIT_REG *(*newregisters), *neweotflags, *regptr;
  FIT_REG *newphrasemask;
  struct prf_s_term *(*newterms);
  FIT_REG *newprox1mask, *newprox2mask, *newphraseproxmask;
  int *newproxterm1map, *newproxterm2map, *newtermtype;

  /* Calculate new allocation */
  newwidth = exe->regwidth + nlong;

/* Expand match table registers */
  /* Allocate new match table and phrase mask and proximity mask */
  newregisters = (FIT_REG *(*)) calloc((size_t) newwidth,
    sizeof(*newregisters));
  if (!newregisters) goto ALLOCERR;

  newphrasemask = (FIT_REG *) calloc((size_t) newwidth,
    sizeof(*newphrasemask));
  if (!newphrasemask) goto ALLOCERR;

  newprox1mask = (FIT_REG *) calloc((size_t) newwidth, sizeof(*newprox1mask));
  if (!newprox1mask) goto ALLOCERR;

  newprox2mask = (FIT_REG *) calloc((size_t) newwidth, sizeof(*newprox2mask));
  if (!newprox2mask) goto ALLOCERR;

  newphraseproxmask = (FIT_REG *) calloc((size_t) newwidth,
    sizeof(*newphraseproxmask));
  if (!newphraseproxmask) goto ALLOCERR;

  for (i=0; i < newwidth; ++i)
    if (i < exe->regwidth) {
      newregisters[i] = exe->table.registers[i];
      newphrasemask[i] = exe->phrasemask[i];
      newprox1mask[i] = exe->prox1mask[i];
      newprox2mask[i] = exe->prox2mask[i];
      newphraseproxmask[i] = exe->phraseproxmask[i];
    }
    else
      newregisters[i] = (FIT_REG *) NULL;

  for (i = exe->regwidth; i < newwidth; ++i) {
    newregisters[i] =
      (FIT_REG *) calloc((size_t) ((exe->nrows + 1) * exe->ncols),
	sizeof(*newregisters[i]));
    if (!newregisters[i]) goto ALLOCERR;
  }

  /* Free old match table pointers */

  if (exe->regwidth) {
    free(exe->table.registers);
    free(exe->phrasemask);
    free(exe->prox1mask);
    free(exe->prox2mask);
    free(exe->phraseproxmask);
  }

  /* Put new tables into profile executable */
  exe->table.registers = newregisters;
  exe->phrasemask = newphrasemask;
  exe->prox1mask = newprox1mask;
  exe->prox2mask = newprox2mask;
  exe->phraseproxmask = newphraseproxmask;

  /* Set eot table offset in match table */
  for (tblidx = 0; tblidx < newwidth; ++tblidx)
    for (i=0, regptr = newregisters[tblidx] + exe->ncols - 1;
	i < exe->nrows + 1; regptr += exe->ncols, ++i)
      *regptr = (i * newwidth) + tblidx;

/* Expand match table EOT flags */
  /* Allocate storage for new EOT table */
  neweotflags = 
    (FIT_REG *) calloc((size_t) (newwidth * (exe->nrows + 1)),
    sizeof(*neweotflags));
  if (!neweotflags) goto ALLOCERR;
  if (newwidth > 1) {
    /* Copy old EOT table into new table */
    for (i = 0; i < exe->nrows + 1; ++i)
      for (j = i * exe->regwidth, k = i * newwidth; j < (i+1) * exe->regwidth;)
	neweotflags[k++] = exe->table.eotflag[j++];

    /* Free up storage used by old table */
    free(exe->table.eotflag);
  }

  /* Insert new table into profile executable */
  exe->table.eotflag = neweotflags;

/* Expand term list */
  /* Allocate storage for new term list */
  newterms = 
    (struct prf_s_term *(*)) calloc((size_t) (newwidth * FIT_BITS_IN_LONG),
    sizeof(*newterms));
  if (!newterms) goto ALLOCERR;

  if (newwidth > 1) {
    /* Copy old term list into new term list */
    for (i=0; i < exe->nterms; ++i)
      newterms[i] = exe->term[i];

    /* Free storage used by the old list */
    free(exe->term);
  }

  /* Insert new term list into profile executable */
  exe->term = newterms;

  newproxterm1map = 
    (int *) calloc((size_t) (newwidth * FIT_BITS_IN_LONG),
    sizeof(*newproxterm1map));
  if (!newproxterm1map) goto ALLOCERR;

  newproxterm2map = (int *) calloc((size_t) (newwidth * FIT_BITS_IN_LONG),
    sizeof(*newproxterm2map));
  if (!newproxterm2map) goto ALLOCERR;

  newtermtype = (int *) calloc((size_t) (newwidth * FIT_BITS_IN_LONG),
    sizeof(*newtermtype));
  if (!newtermtype) goto ALLOCERR;

  if (newwidth > 1) {
    for (i=0; i < exe->nterms; ++i) {
      newproxterm1map[i] = exe->proxterm1map[i];
      newproxterm2map[i] = exe->proxterm2map[i];
      newtermtype[i] = exe->termtype[i];
    }
    free(exe->proxterm1map);
    free(exe->proxterm2map);
    free(exe->termtype);
  }

  exe->proxterm1map = newproxterm1map;
  exe->proxterm2map = newproxterm2map;
  exe->termtype = newtermtype;


  /* Remember the new match table register width (in long words) */
  exe->regwidth = newwidth;
  exe->termalloc = newwidth * FIT_BITS_IN_LONG;

  return 1;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return 0;
}
