/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_alloctrm					alloctrm.c

Function : Allocates and initializes term descriptor and match table row.

Author : A. Fregly

Abstract :

Calling Sequence : stat = prf_alloctrm(struct prof_s_exe *exe,
     char delims[QC_MAXDELIM], char *term, struct prf_s_termmap *termmap,
     int parent, int compare, int *retidx)

  exe           Profile executable
  delims        Delimiters (wild cards and other reserved characters)
  term          Value of term to be added
  termmap       Term map entry. Used in tracking term and their type.
  parent        Parent term descriptor node of term to be added
  compare	< 1 ==> term is lson of parent, > 1 ==> term is rson
  retidx	Returned index of term node /column in match table used for
		the term.

Notes :

Change log :
000	12-AUG-90  AMF	Created.
001     15-AUG-91  AMF  Updated header comment to match VMS version.
002	15-AUG-91  AMF	Fix error in setting phrase proximity mask when
			termidx is 0, set leading mask rather than following.
003	15-AUG-91  AMF	Put in proximity mask setup as fixed in VMS version.
004	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_alloctrm(struct prf_s_exe *exe, char delims[QC_MAXDELIM],
  char *term, struct prf_s_termmap *termmap, int parent, int compare, 
  int *idx)
#else
int prf_alloctrm(exe, delims,term, termmap, parent, compare, idx)
  struct prf_s_exe *exe;
  char delims[QC_MAXDELIM], *term;
  struct prf_s_termmap *termmap;
  int parent, compare, *idx;
#endif
{

  struct prf_s_term *termptr;
  int i,stat,off,pidx,prevpidx,wordlen,tblidx,ptblidx, wlen;
  FIT_REG mask,pmask;

  /* Initialize node for term */
  *idx = exe->nterms++;
  tblidx = *idx / FIT_BITS_IN_LONG;
  mask = (FIT_REG) (1L << (*idx % FIT_BITS_IN_LONG));
  termptr = (struct prf_s_term *) calloc((size_t) 1,sizeof(*termptr));
  if (!termptr) goto ALLOCERR;
  *(exe->term + *idx) = termptr;
  termptr->ttype = 0;
  termptr->nid = 0;
  termptr->fieldmap = 0;
  termptr->logicid = 0;
  termptr->len = strlen(term);
  termptr->val = (char *) malloc((size_t) (termptr->len+1));
  if (termptr->val == NULL) goto ALLOCERR;
  strcpy(termptr->val,term);
  termptr->lson = -1;
  termptr->rson = -1;
  termptr->nwords = 0;
  termptr->nextword = 0;

  /* link term node to parent */
  if (*idx)
    if (compare < 0)
      (*(exe->term+parent))->lson = *idx;
    else
      (*(exe->term+parent))->rson = *idx;

  for (i=0; i < termptr->len;) {
    if (prf_findword(termptr->val, termptr->len, &i, exe->term_xlate,
	delims,	exe->tx_xlate,&off,&wordlen)) {
      wlen = (wordlen < exe->nrows) ? wordlen : exe->nrows;
      if (termptr->nwords == 0)
	prf_filltbl(*(exe->table.registers+tblidx), exe->table.eotflag,
	  exe->nrows, exe->ncols, exe->regwidth, exe->term_xlate, delims,
	  exe->tx_xlate, *idx, termptr->val+off, wlen);
      else {
	stat = prf_addphrse(exe, delims, termptr->val+off, wlen, &pidx);
	if (stat == 0) return(stat);
	if (termptr->nwords == 1)
	  termptr->nextword = pidx;
	else
	  (*(exe->phraseterm + prevpidx))->nextword = pidx;

	(*(exe->phraseterm + pidx))->termnum = *idx;
	(*(exe->phraseterm + pidx))->termmask = mask;
	prevpidx = pidx;
      }
      termptr->nwords += 1;
    }
  }

  if (termmap->termtype == QC_PROXIMITY) {
    termptr->ttype |= (1 << PRF_BIT_WPROXIMITY);
    if (termmap->termidx >= 0) {
      exe->prox1mask[tblidx] |= mask;
      exe->proxterm1map[*idx] = termmap->listnum;
    }
    else {
      exe->prox2mask[tblidx] |= mask;
      exe->proxterm2map[*idx] = termmap->listnum;
    }
    exe->phraseproxmask[tblidx] |= mask;
    exe->termtype[*idx] = QC_PROXIMITY;
  }

  if (termptr->nwords > 1) {
    termptr->ttype |= (1 << PRF_BIT_PHRASE);
    exe->phrasemask[tblidx] |= mask;
    exe->phraseproxmask[tblidx] |= mask;
    ptblidx = pidx / FIT_BITS_IN_LONG;
    pmask = (FIT_REG) (1L << (pidx % FIT_BITS_IN_LONG));
    if (termptr->ttype & (1 << PRF_BIT_WPROXIMITY)) {
      if (termmap->termidx >= 0)
	exe->phraseprox1mask[ptblidx] |= pmask;
      else
	exe->phraseprox2mask[ptblidx] |= pmask;
    }
  }
  else
    termptr->ttype |= (1 << PRF_BIT_WORD);

  return 1;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return 0;
}
