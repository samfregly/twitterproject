/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : prf_lookup					lookup.c

Function : Looks up term in list of terms for profile executable

Author : A. Fregly

Abstract :

Calling Sequence :  stat = prf_lookup(char *term, struct prf_s_exe *exe,
  int *idx);

  term		Value of term being looked up.
  exe		Profile executable.
  idx		Returned index for term.
  compare	Status of last string compare in looking up term.
  stat		Returned status of lookup


Notes :

Change log :
000	12-AUG-90  AMF
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>

#define ISPROX (1 << PRF_BIT_WPROXIMITY)

#if FIT_ANSI
int prf_lookup(char *term, FIT_WORD termtype, struct prf_s_exe *exe, int *idx,
  int *compare)
#else
int prf_lookup(term, termtype, exe, idx, compare)
  char *term;
  FIT_WORD termtype;
  struct prf_s_exe *exe;
  int *idx, *compare;
#endif
{
  static struct prf_s_term *node;
  static int newidx;

  for (*idx = 0, *compare = exe->nterms,
      node = (*compare) ? *(exe->term) : (struct prf_s_term *) NULL;
      *compare;) {

    if ((*compare = strcmp(term, node->val)) || (node->ttype & ISPROX)) {

      if (!*compare) *compare = -1;

      newidx = (*compare < 0) ? node->lson : node->rson;

      if (newidx >= 0) {
        node = *(exe->term + newidx);
        *idx = newidx;
      }
      else
        break;
    }
  }

  if (exe->nterms)
    return(!termtype && !*compare);
  else
    return 0;
}
