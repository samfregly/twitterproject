/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_setflds					setflds.c

Function : Sets field definitions for profile executable

Author : A. Fregly

Abstract : This routine is used to set the field names used within a profile
  executable.  Implicit in the order of the caller supplied field names are
  the field numbers of the fields.  These field numbers must match the
  corresponding field markers in text being profiled.  A field marker in
  scanned text consists of three bytes, the first of which has a value of
  0, the second of which is the  field number, and the third which is the
  logical negation of the field number.  For example, a field marker for
  the first defined field would have a value of {0, 1, 254}.

Calling Sequence :
  stat = prf_setflds(int nfields, char *fields[], char *exe);

  nfields	Number of defined fields.
  fields	Array of pointers at field names.
  exe		Profile executable.

Notes :

Change log :
000	19-MAY-90  AMF	Created.
001	29-APR-91  AMF	Fixed field deallocation.
002	08-MAY-91  AMF	Fix field value overwrite error.
003	24-MAR-93  AMF	Compile under Coherent/GNU C.
*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

/* Set field defs */
#if FIT_ANSI
int prf_setflds(char *xe, int nfields, char *fields[])
#else
int prf_setflds(xe, nfields, fields)
  char *xe;
  int nfields;
  char *fields[];
#endif
{

  struct prf_s_exe *exe;
  int i,fldlen;
  char *valptr;

  exe = (struct prf_s_exe *) xe;
  /* See if current fields are already defined */
  if (exe->fields) {
    /* Deallocate current field definitions */
    if (*exe->fields) free(*exe->fields);
    free(exe->fields);
    exe->fields = (char *(*)) NULL;
  }

  if (!nfields) return(1);

  /* Allocate storage for field pointer array */
  exe->fields = (char *(*)) calloc((size_t) nfields, sizeof(*(exe->fields)));
  if (!exe->fields) goto ALLOCERR;

  /* Determine amount of storage needed for field name pointers */
  for (fldlen = 0, i=0; i < nfields; ++i)
    fldlen += strlen(*(fields+i))+1;

  /* Allocate storage for values */
  valptr = (char *) malloc((size_t) fldlen);
  if (valptr == NULL) goto ALLOCERR;

  /* Add the fields to the field name structure */
  for (i=0; i < nfields; ++i) {
    strcpy(valptr, fields[i]);
    fit_supcase(valptr);
    *(exe->fields+i) = valptr;
    valptr += strlen(fields[i])+1;
  }

  exe->nfields = nfields;
  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
