/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_getxlate					prfgetxl.c

Function : Returns text translate table being used be search executable.

Author : A. Fregly

Abstract : This function is used to return the translate table being used
	by a search executable. The translate table is an integer array
	which is used to map the characters in the text being searched to
	the internal values used in the search executable. The table maps
	by taking the numeric value of each possible byte value (0 to 255),
	and using that as an index into the table of the mapped character
	value. Postiive values in the table are used to represent each unique 
	searchable character. Values of 0 and -1 are used to indicate 
	characters which are not searched and are considered as spaces.
	Values less than -1 are used to map characters which are used as
	the start of a field delimiter.

Calling Sequence : int stat = prf_getxlate(char *exe, FIT_WORD *tx_xlate);

  exe		Search executable returned by a call to prf_init.
  tx_xlate	Returned translation table. tx_xlate should be a buffer of 
		at least 256 words in length.

Notes: 

Change log : 
000	01-FEB-91  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_MSC || FIT_BSD
#include <string.h>
#endif
#include <prf.h>
#include <prfprvte.h>

/* Get current xlate table */
#if FIT_ANSI
int prf_getxlate(char *xe, FIT_WORD *tx_xlate)
#else
int prf_getxlate(xe, tx_xlate)
  char *xe;
  FIT_WORD *tx_xlate;
#endif
{
  struct prf_s_exe *exe;
  exe = (struct prf_s_exe *) xe;
  if (!exe) return 0;
  memmove(tx_xlate, exe->tx_xlate, sizeof(*(exe->tx_xlate)) * 256);
  return 1;
}
