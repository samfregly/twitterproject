/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_init							init.c

Function : Initializes profile executable.

Author : A. Fregly

Abstract : This  routine is used to initialize or re-initailize a profile
	executable.  This must be done prior to linking any queries to
	the  profile executable.

Calling Sequence :
  st = prf_init(int maxtermlen, FIT_WORD *tx_xlate, int nfields, 
	char *(*fields), char *(*exe));

  maxtermlen	This determines the maximum query term length for a single
		word.  Note that a phrase term may be composed of many
		words, each of which may be up to this value in length.

  tx_xlate	Optional translation table used to map characters to be
		searched to a possibly smaller number of values.  The
		searchable characters should all map to a consecutive
		sequence of numbers starting at 0.  The default translation
		table will map all upper and lower case letters so that
		searching is case insensitive, and will also map numbers.
		All other characters are mapped as word delimiters (see
		below).  Characters mapping to a value
		of 0 or -1 are considered as white space (word delimiters),
		and any other negative value may be used as a field marker,
		which in the search text is followed by the field number.
		If tx_xlate is null, it will default to values
		corresponding to those in term_xlate.

  nfields	Number of defined fields, maximum is 32.

  fields	Array of pointers at field names.  Implicit in the
		offset of the field name in the array is the field
		number.

  exe		Returned initialized profile executable.  Note that if the
		profile executable pointer is non-null at entry
		(*exe != NULL), the profile executable is deallocated before
		a new one is created and initialized.

Notes :

Change log :
000	12-MAY-90  AMF	Created.
001     01-JUN-91  AMF  Updated header comment.
002	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <tips.h>
#include <fit_stat.h>

  static FIT_WORD deftx_xlate[256] = {

 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1

/* ' '     '!'     '"'     '#'     '$'     '%'     '&'     ''' */
  ,-1      ,-1      ,-1    ,0      ,0      ,0      ,0      ,0

/* '('     ')'     '*'     '+'     ','     '-'     '.'     '/' */
  ,-1     ,-1     ,0      ,0      ,-1     ,-1     ,-1     ,0

/* '0'     '1'     '2'     '3'     '4'     '5'     '6'    '7'  */
  , 1     ,2      ,3      ,4      ,5      ,6      ,7     ,8

/* '8'     '9'    ,':'     ';'     '<'     '='     '>'     '?' */
  ,9      ,10     ,-1     ,-1     ,0      ,0      ,0      ,-1

/* '@'     'A'     'B'     'C'     'D'     'E'     'F'     'G' */
  ,0      ,11     ,12     ,13     ,14     ,15     ,16     ,17

/* 'H'     'I'     'J'     'K'     'L'     'M'     'N'     'O' */
  ,18     ,19     ,20     ,21     ,22     ,23     ,24     ,25

/* 'P'     'Q'     'R'     'S'     'T'     'U'     'V'     'W' */
  ,26     ,27     ,28     ,29     ,30     ,31     ,32     ,33

/* 'X'     'Y'     'Z'     '['     '\'     ']'     '^'     '_' */
  ,34     ,35     ,36     ,0      ,0      ,0      ,0      ,0

/* '`'     'a'     'b'     'c'     'd'     'e'     'f'     'g' */
  ,-1     ,11     ,12     ,13     ,14     ,15     ,16     ,17

/* 'h'     'i'     'j'     'k'     'l'     'm'     'n'     'o' */
  ,18     ,19     ,20     ,21     ,22     ,23     ,24     ,25

/* 'p'     'q'     'r'     's'     't'     'u'     'v'     'w' */
  ,26     ,27     ,28     ,29     ,30     ,31     ,32     ,33

/* 'x'     'y'     'z'     '{'     '|'     '}'     '~'    del  */
  ,34     ,35     ,36     ,0      ,0      ,0      ,0      ,0,

 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2
};

#if FIT_ANSI
int prf_init(int nrows, FIT_WORD *tx_xlate, int nfields, char *(*fields),
  char *(*xe))
#else
int prf_init(nrows, tx_xlate, nfields, fields, xe)
  int nrows;
  FIT_WORD *tx_xlate;
  int nfields;
  char *(*fields);
  char *(*xe);
#endif
{

  struct prf_s_exe *exe;
  int stat, i;
  FIT_WORD *charxlateptr;

  stat = 0;

  /* deallocate any current profile exe */
  if (xe && *xe) {
      prf_dealloc(xe);
  }

  /* allocate new exe header structure */
  exe = (struct prf_s_exe *) calloc(1, sizeof(*exe));
  if (!exe) goto ALLOCERR;
  *xe = (char *) exe;

  /* initialize the header with NULL values */
  exe->term_xlate = (FIT_UWORD *) NULL;
  exe->tx_xlate = (FIT_WORD *) NULL;
  exe->table.registers = (FIT_REG *(*)) NULL;
  exe->table.eotflag = (FIT_REG *) NULL;
  exe->phrasetbl.registers = (FIT_REG *(*)) NULL;
  exe->phrasetbl.eotflag = (FIT_REG *) NULL;
  exe->phrasemask = (FIT_REG *) NULL;
  exe->prox1mask = (FIT_REG *) NULL;
  exe->phraseprox1mask = (FIT_REG *) NULL;
  exe->prox2mask = (FIT_REG *) NULL;
  exe->phraseprox2mask = (FIT_REG *) NULL;
  exe->phraseproxmask = (FIT_REG *) NULL;
  exe->nterms = 0;
  exe->nphraseterms = 0;
  exe->nproximity = 0;
  exe->nlogicids = 0;
  exe->nqueries = 0;
  exe->qryalloc = 0;
  exe->nfields = 0;
  exe->nrows = nrows;
  exe->ncols = 0;
  exe->regwidth = 0;
  exe->phraseregwidth = 0;
  exe->term = (struct prf_s_term *(*)) NULL;
  exe->phraseterm = (struct prf_s_phraseterm *(*)) NULL;
  exe->proximity = (struct prf_s_proximity *) NULL;
  exe->proxterm1map = (int *) NULL;
  exe->proxterm2map = (int *) NULL;
  exe->termtype = (int *) NULL;
  exe->logiclen = (int *) NULL;
  exe->logic = (int *(*)) NULL;
  exe->qfiles = (char *(*)) NULL;
  exe->fields = (char *(*)) NULL;

  /* Fill in the term character translation table */
  exe->term_xlate =
      (FIT_UWORD *) calloc((size_t) 256, sizeof(*(exe->term_xlate)));
  if (!exe->term_xlate) goto ALLOCERR;

  /* Fill in the search text character translation table */
  exe->tx_xlate =
      (FIT_WORD *) calloc((size_t) 256,sizeof(*(exe->tx_xlate)));
  if (!exe->tx_xlate) goto ALLOCERR;

  if (tx_xlate) {
    /* Create copy of caller supplied table */
    charxlateptr = tx_xlate;
  }
  else {
    /* Use default table */
    charxlateptr = deftx_xlate;
  }

  for (i=0; i<256; ++i)
      *(exe->tx_xlate + i) = *(charxlateptr+i);

  /* Set number of columns in match table */
  for (i=0; i<256; ++i)
    exe->ncols=max(exe->ncols,max(0,(int) *(exe->tx_xlate+i)) % 256);
  exe->ncols += 2;

  if (nfields) {
    /* Create field list */
    stat = prf_setflds(*xe, nfields, fields);
  }
  else
    stat = 1;
  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  stat = 0;

DONE:
  if (!stat && xe && *xe)
    prf_dealloc(xe);
  return(stat);
}
