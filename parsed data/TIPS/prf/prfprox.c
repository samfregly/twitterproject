/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prox.c

Function : Contains routines used in resolving proximity relationships.

Author : A. Fregly

Abstract : This file contains functions used by prf_search in keeping track
	of possible proximity matching terms, and in resolving those possible
	terms.

Calling Sequence : 

Notes : 

Change log :
000	23-APR-91  AMF	Header comment.
001	06-MAY-91	Changed freeprox "exe" parameter to "nproximity".
002	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <prox.h>
#include <fit_stat.h>

#if FIT_ANSI
void prf_search_freeprox(register prf_s_proxmatchlist *proxmatchlist,
    int nproximity)
#else
void prf_search_freeprox(proxmatchlist,nproximity)
#if FIT_USEREGISTERS
  register prf_s_proxmatchlist *proxmatchlist;
#else
  prf_s_proxmatchlist *proxmatchlist;
#endif
  int nproximity;
#endif
{

#if FIT_USEREGISTERS
  register int i;

  register prf_s_proxmatch *curproxmatch, *tempproxmatch;
#else
  static int i;
  static prf_s_proxmatch *curproxmatch, *tempproxmatch;
#endif

  for (i=0; i < nproximity; ++i) {
    for (curproxmatch = proxmatchlist[i].first_1;
	 curproxmatch;) {
      tempproxmatch = curproxmatch->next;
      free(curproxmatch);
      curproxmatch = tempproxmatch;
    }
    proxmatchlist[i].first_1 = (prf_s_proxmatch *) NULL;
    proxmatchlist[i].last_1 = (prf_s_proxmatch *) NULL;
    proxmatchlist[i].n_1 = 0;
    for (curproxmatch = proxmatchlist[i].first_2;
	curproxmatch;) {
      tempproxmatch = curproxmatch->next;
      free(curproxmatch);
      curproxmatch = tempproxmatch;
    }
    proxmatchlist[i].first_2 = (prf_s_proxmatch *) NULL;
    proxmatchlist[i].last_2 = (prf_s_proxmatch *) NULL;
    proxmatchlist[i].n_2 = 0;
  }
}


#if FIT_ANSI
void prf_search_addprox(register prf_s_proxmatchlist *proxmatchlist,
  long swordnum, long ewordnum, int preceeding)
#else
void prf_search_addprox(proxmatchlist, swordnum, ewordnum, preceeding)
#if FIT_USEREGISTERS
  register prf_s_proxmatchlist *proxmatchlist;
#else
  prf_s_proxmatchlist *proxmatchlist;
#endif
  long swordnum, ewordnum;
  int preceeding;
#endif
{
#if FIT_USEREGISTERS
  register struct prf_s_proxmatch *match;
#else
  static struct prf_s_proxmatch *match;
#endif

  match = (prf_s_proxmatch *) malloc(sizeof(*match));
  if (!match) return;
  match->swordnum = swordnum;
  match->ewordnum = ewordnum;
  match->next = (prf_s_proxmatch *) NULL;

  if (preceeding) {
    if (proxmatchlist->n_1) proxmatchlist->last_1->next = match;
    proxmatchlist->last_1 = match;
    if (!proxmatchlist->n_1) proxmatchlist->first_1 = match;
    proxmatchlist->n_1 += 1;
  }
  else {
    if (proxmatchlist->n_2) proxmatchlist->last_2->next = match;
    proxmatchlist->last_2 = match;
    if (!proxmatchlist->n_2) proxmatchlist->first_2 = match;
    proxmatchlist->n_2 += 1;
  }
  return;
}

#if FIT_ANSI
void prf_search_resolveprox(prf_s_exe *exe, int idx,
  register prf_s_proxmatchlist *matchlist, char *idmatches)
#else
void prf_search_resolveprox(exe, idx, matchlist, idmatches)
  prf_s_exe *exe;
  int idx;
#if FIT_USEREGISTERS
  register prf_s_proxmatchlist *matchlist;
#else
  prf_s_proxmatchlist *matchlist;
#endif
  char *idmatches;
#endif
{

#if FIT_USEREGISTERS
  register long distance;
  register int matched;
  register prf_s_proxmatch *preceeding, *follower;
#else
  static long distance;
  static int matched;
  static prf_s_proxmatch *preceeding, *follower;
#endif

  distance = exe->proximity[idx].distance;
  matched = 0;
  if (exe->proximity[idx].relation) {
    for (preceeding = matchlist->first_1; !matched && preceeding;
      preceeding = preceeding->next)
      for (follower = matchlist->first_2; !matched && follower;
	  follower = follower->next)
	if (preceeding->swordnum < follower->swordnum)
	  matched = follower->swordnum - preceeding->ewordnum <= distance;
  }

  else {
    for (preceeding = matchlist->first_1; !matched && preceeding;
      preceeding = preceeding->next)
      for (follower = matchlist->first_2; !matched && follower;
	  follower = follower->next)
	if (preceeding->swordnum < follower->swordnum)
	  matched = follower->swordnum - preceeding->ewordnum <= distance;
	else if (follower->swordnum < preceeding->swordnum)
	  matched = preceeding->swordnum - follower->ewordnum <= distance;
  }

  if (matched)
    idmatches[exe->proximity[idx].id] = 1;
}