/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_write                                 write.c

Function : Writes out profile executable

Author : A. Fregly

Abstract : This routine writes out a profile executable.

Calling Sequence : stat = prf_write(char *fname, char *exe)

	fname   Name of profile executable
	exe     The profile executable to be written out.
	stat    Returned status, 0 - failure, 1 - success.

Notes :

Change log :
000     20-AUG-90  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
002     24-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_write(char *fname, char *xe)
#else
int prf_write(fname, xe)
  char *fname, *xe;
#endif
{
  struct prf_s_exe *exe;
  FILE *handle;
  int stat,i,j,len,matched, rowwidth, tblidx;
  FIT_REG *startreg, *regptr, *endreg;
  char outfname[FIT_FULLFSPECLEN+1];

  exe = (struct prf_s_exe *) xe;
  fit_setext(fname,".prf",outfname);
  handle = fopen(outfname, FIT_WRITE);
  if (!handle) {
    fit_status = FIT_ERR_OPEN;
    return(0);
  }

  fwrite(&(exe->nterms),sizeof(exe->nterms),(size_t) 1,handle);
  fwrite(&(exe->termalloc),sizeof(exe->termalloc),(size_t) 1,handle);
  fwrite(&(exe->nlogicids),sizeof(exe->nlogicids),(size_t) 1,handle);
  fwrite(&(exe->nqueries),sizeof(exe->nqueries),(size_t) 1,handle);
  fwrite(&(exe->qryalloc),sizeof(exe->qryalloc),(size_t) 1,handle);
  fwrite(&(exe->nfields),sizeof(exe->nfields),(size_t) 1,handle);
  fwrite(&(exe->nrows),sizeof(exe->nrows),(size_t) 1,handle);
  fwrite(&(exe->ncols),sizeof(exe->ncols),(size_t) 1,handle);
  fwrite(&(exe->regwidth),sizeof(exe->regwidth),(size_t) 1,handle);
  fwrite(&(exe->phraseregwidth),sizeof(exe->phraseregwidth),(size_t) 1,
    handle);
  fwrite(&(exe->nphraseterms),sizeof(exe->nphraseterms),(size_t) 1,handle);
  fwrite(&(exe->nproximity),sizeof(exe->nproximity),(size_t) 1,handle);
  fwrite(&(exe->nlogicids),sizeof(exe->nlogicids),(size_t) 1,handle);
  for (i=0; i<exe->nqueries; ++i) {
    fputs(exe->qfiles[i],handle);
    fputc('\n',handle);
  }

  for (i=0; i<exe->nfields; ++i) {
    fputs(exe->fields[i],handle);
    fputc('\n',handle);
  }

  fwrite(exe->phrasemask,sizeof(*exe->phrasemask),(size_t) exe->regwidth,
    handle);
  fwrite(exe->prox1mask,sizeof(*exe->prox1mask),(size_t) exe->regwidth,
    handle);
  fwrite(exe->prox2mask,sizeof(*exe->prox2mask),(size_t) exe->regwidth,
    handle);
  if (exe->phraseregwidth) {
    fwrite(exe->phraseprox1mask,sizeof(*exe->phraseprox1mask),
	(size_t) exe->phraseregwidth,handle);
    fwrite(exe->phraseprox2mask,sizeof(*exe->phraseprox2mask),
	(size_t) exe->phraseregwidth,handle);
  }
  fwrite(exe->phraseproxmask,sizeof(*exe->phraseproxmask),
    (size_t) exe->regwidth,handle);
  fwrite(exe->proxterm1map,sizeof(*exe->proxterm1map),(size_t) exe->nterms,
    handle);
  fwrite(exe->proxterm2map,sizeof(*exe->proxterm2map),(size_t) exe->nterms,
    handle);
  fwrite(exe->termtype,sizeof(*exe->termtype),(size_t) exe->nterms,handle);

  for (i=0; i < exe->nterms; ++i) {
    fwrite(&exe->term[i]->ttype,sizeof(exe->term[i]->ttype),(size_t) 1,
      handle);
    fwrite(&exe->term[i]->nid,sizeof(exe->term[i]->nid),(size_t) 1,handle);
    fwrite(&exe->term[i]->len,sizeof(exe->term[i]->len),(size_t) 1,handle);
    fwrite(&exe->term[i]->lson,sizeof(exe->term[i]->lson),(size_t) 1,handle);
    fwrite(&exe->term[i]->rson,sizeof(exe->term[i]->rson),(size_t) 1,handle);
    fwrite(&exe->term[i]->nwords,sizeof(exe->term[i]->nwords),(size_t) 1,
      handle);
    fwrite(&exe->term[i]->nextword,sizeof(exe->term[i]->nextword),(size_t) 1,
      handle);
    fputs(exe->term[i]->val,handle);
    fputc('\n',handle);
    for (j=0; j < exe->term[i]->nid; ++j)
      fwrite(&exe->term[i]->fieldmap[j],sizeof(exe->term[i]->fieldmap[j]),
	(size_t) 1,handle);
    for (j=0; j < exe->term[i]->nid; ++j)
      fwrite(&exe->term[i]->logicid[j],sizeof(exe->term[i]->logicid[j]),
	(size_t) 1,handle);
  }

  for (i=0; i < exe->nphraseterms; ++i) {
    fwrite(&exe->phraseterm[i]->len,sizeof(exe->phraseterm[i]->len),
      (size_t) 1,handle);
    fwrite(&exe->phraseterm[i]->nextword,
      sizeof(exe->phraseterm[i]->nextword),(size_t) 1,handle);
    fwrite(&exe->phraseterm[i]->termnum,
      sizeof(exe->phraseterm[i]->termnum),(size_t) 1,handle);
    fwrite(&exe->phraseterm[i]->termmask,
      sizeof(exe->phraseterm[i]->termmask),(size_t) 1,handle);
    fputs(exe->phraseterm[i]->val,handle);
    fputc('\n',handle);
  }

  for (i=0; i < exe->nproximity; ++i) {
    fwrite(&exe->proximity[i].id,sizeof(exe->proximity[i].id),(size_t) 1,
      handle);
    fwrite(&exe->proximity[i].relation,sizeof(exe->proximity[i].relation),
      (size_t) 1,handle);
    fwrite(&exe->proximity[i].distance,sizeof(exe->proximity[i].distance),
      (size_t) 1,handle);
  }

  for (i=0; i<exe->nqueries; ++i) {
    fwrite(&exe->logiclen[i],sizeof(exe->logiclen[i]),(size_t) 1,handle);
    for (j=0; j < exe->logiclen[i]; ++j)
      fwrite(&(exe->logic[i][j]),sizeof(exe->logic[i][j]),(size_t) 1,handle);
  }

  fwrite(exe->term_xlate,sizeof(*(exe->term_xlate)),(size_t) 256,handle);
  fwrite(exe->tx_xlate,sizeof(*(exe->tx_xlate)),(size_t) 256,handle);

  /* Clear eot table offsets in match table */
  rowwidth = exe->ncols;
  for (tblidx = 0; tblidx < exe->regwidth; ++ tblidx)
    for (i=0, regptr = exe->table.registers[tblidx] + rowwidth - 1;
      i < exe->nrows; regptr += rowwidth, ++i)
    *regptr = *(regptr - exe->regwidth);

  for (tblidx = 0; tblidx < exe->regwidth; ++tblidx)
    for (startreg = exe->table.registers[tblidx],
      endreg = startreg + exe->nrows * exe->ncols;
	startreg != endreg;) {
      for (regptr = startreg + 1, matched = 1;
	  matched && (regptr != endreg);) {
	matched &= (*startreg == *regptr);
	if (matched)
	  ++regptr;
      }
      len = regptr - startreg;
      fwrite(&len,sizeof(len),(size_t) 1,handle);
      fwrite(startreg,sizeof(*startreg),(size_t) 1,handle);
      startreg = regptr;
    }

  /* Set eot table offsets in match table */
  for (tblidx = 0; tblidx < exe->regwidth; ++tblidx)
    for (i=0, regptr = exe->table.registers[tblidx] + rowwidth - 1;
	i < exe->nrows; regptr += rowwidth, ++i)
      *regptr = (i * exe->regwidth) + tblidx;

  fwrite(exe->table.eotflag,sizeof(*(exe->table.eotflag)),
    (size_t) (exe->nrows * exe->regwidth), handle);


  /* Clear eot table offsets in phrase match table */
  rowwidth = exe->ncols;
  for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
    for (i=0, regptr = exe->phrasetbl.registers[tblidx] + rowwidth - 1;
      i < exe->nrows; regptr += rowwidth, ++i)
    *regptr = *(regptr - exe->phraseregwidth);

  for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
    for (startreg = exe->phrasetbl.registers[tblidx],
      endreg = startreg + exe->nrows * exe->ncols;
	startreg != endreg;) {
      for (regptr = startreg + 1, matched = 1;
	  matched && (regptr != endreg);) {
	matched &= (*startreg == *regptr);
	if (matched)
	  ++regptr;
      }
      len = regptr - startreg;
      fwrite(&len,sizeof(len),(size_t) 1,handle);
      fwrite(startreg,sizeof(*startreg),(size_t) 1,handle);
      startreg = regptr;
    }

  /* Set eot table offsets in phrase match table */
  for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
    for (i=0, regptr = exe->phrasetbl.registers[tblidx] + rowwidth - 1;
	i < exe->nrows; regptr += rowwidth, ++i)
      *regptr = (i * exe->phraseregwidth) + tblidx;

  if (exe->phraseregwidth)
    fwrite(exe->phrasetbl.eotflag,sizeof(*(exe->phrasetbl.eotflag)),
      (size_t) (exe->nrows * exe->phraseregwidth), handle);

  stat=1;

  if (handle) fclose(handle);
  return(stat);
}
