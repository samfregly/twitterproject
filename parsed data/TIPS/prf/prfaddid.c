/******************************************************************************
Module Name : prf_addid					addid.c

Function : Adds new field map and logic id to list for term.

Author : A. Fregly

Abstract :

Calling Sequence : stat = prf_addid(struct prf_s_term *term,
  unsigned int logicid, unsigned long fieldmap);

  term		Term structure.
  logicid	Logic ID for entry being added to term.
  fieldmap	Field map for entry being added to term.
  stat		Returned status.

Notes :

Change log :
000	12-AUG-90  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_addid(struct prf_s_term *term, unsigned int logicid,
    unsigned long fieldmap)
#else
int prf_addid(term, logicid, fieldmap)
  struct prf_s_term *term;
  unsigned int logicid;
  unsigned long fieldmap;
#endif
{

  FIT_REG *newfieldmap;
  int *newlogicid;
  int i;

  /* expand ID allocation for term */
  newfieldmap = (FIT_REG *) calloc((size_t) (term->nid+1),
    sizeof(*newfieldmap));
  if (!newfieldmap) goto ALLOCERR;
  newlogicid = (int *) calloc((size_t) (term->nid+1),
    sizeof(*newlogicid));
  if (!newlogicid) goto ALLOCERR;
  for (i=0; i < term->nid; ++i) {
    *(newfieldmap+i) = *(term->fieldmap+i);
    *(newlogicid+i) = *(term->logicid+i);
  }
  if (term->nid) {
    free(term->fieldmap);
    free(term->logicid);
  }
  term->fieldmap = newfieldmap;
  term->logicid = newlogicid;

  /* Add logic id and field map to end of list for term */
  *(term->fieldmap + term->nid) = fieldmap;
  *(term->logicid + term->nid) = logicid;
  term->nid += 1;
  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
