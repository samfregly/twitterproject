#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <prox.h>
#include <fit_stat.h>



/* Searches text with search executable containing multiple queries and
returns match information */
#if FIT_ANSI
int prf_search_matchinfo(int searchstatus, unsigned int opts, char *xe,
  unsigned char *text, unsigned int textlen, char *(*cmatchinfo))
#else
int prf_search_matchinfo(searchstatus, opts, xe,text, textlen, cmatchinfo)
  int searchstatus;
  unsigned int opts;
  char *xe;
  unsigned char *text;
  unsigned int textlen;
  char *(*cmatchinfo);
#endif

/*  paramters:
        searchstate     see prf_search for description.
        opts            see prf_search for description.
        exe             see prf_search for description.
        text            see prf_search for description.
        textlen         see prf_search for description.
        matchinfo       Returned pointer at match information. The
                        prf_matchinfo_* functions are used to extract desired
                        elements of match information from the match
                        information. 
                                        
                        *matchinfo should be NULL on the initial call to this 
                        function. *matchinfo is filled in at completion of 
                        searching of each document as indicated by the value of 
                        the "searchstate" parameter. This function will 
                        allocate/reallocate *matchinfo as necessary when being 
                        called repetitively to search a sequence of documents. 
                        The function prf_matchinfo_deallocate may be called to 
                        explicitly deallocate *matchinfo once searching is
                        completed.

        returns:
                        0 indicates an error of some sort.
*/

  {
    
#if FIT_USEREGISTERS
    register FIT_REG reg;
    register FIT_WORD *xlate;
    register FIT_WORD c;
    register FIT_REG *row;
    register FIT_REG *curtable;
#else
    static FIT_REG reg;
    static FIT_WORD *xlate;
    static FIT_WORD c;
    static FIT_REG *row;
    static FIT_REG *curtable;
#endif
    
    static int ncols;
    
    static FIT_REG *eottable;
    static long startwordnum, curwordnum;
    static int inphrase;
    static unsigned char *begword, *endtext;
    static FIT_REG wordmatched;
    static int *top, *token, *t;
    static unsigned int i,j;
    
    static FIT_REG *preg;
    static struct prf_s_exe *exe;
    static FIT_REG *(*table), *endmatchreg;
    static int startphrase;
    static FIT_REG notphraseproxmask, notphrasemask;
    static FIT_REG *curfldreg, mask, *startfldreg;
    static int regwidth, *(*logic),*endtoken, *(*endlogic),nrows;
    static int *matched, *logiclen, nfields;
    
    static int stack[1024];
    static unsigned int tablesize, k;
    static FIT_REG *fieldmap, *temppmask;
    static unsigned char *begtext;
    static struct prf_s_term *term;
    static int listidx;
    static FIT_REG *phrasemask, *(*phrasetable);
    static FIT_REG *peottable,curphrasemask,curfldmask;
    static int termbase, termidx, nterms;
    static struct prf_s_phraseterm *(*phraseterm);
    static FIT_REG phraseproxmask, prox1mask, prox2mask;
    static FIT_REG phraseprox1mask,phraseprox2mask;
    static FIT_REG startprox, notproxmask, notinproximity;
    static unsigned int savefldnum, nextfldnum;
    static FIT_REG savefldmask, nextfldmask;
    
    static FIT_REG *matchreg = (FIT_REG *) NULL;
    static FIT_REG *fieldreg = (FIT_REG *) NULL;
    static int phraseregwidth = 0;
    static FIT_REG *pmask = (FIT_REG *) NULL;
    static FIT_REG *altpmask = (FIT_REG *) NULL;
    static FIT_REG *phraseactive = (FIT_REG *) NULL;
    static FIT_REG proxmatch;
    static struct prf_s_proxmatchlist *proxmatchlist =
      (struct prf_s_proxmatchlist *) NULL;
    static int nproximity=0;
    static unsigned long matchoffset, buffoffset;
    static FIT_REG matchedwordmask;
    static prf_s_matchinfo *matchinfo;
    static prf_s_match *curmatch, *prevmatch, *nextmatch;
    
    
    /* Deallocate data structures */
    if (searchstatus & 8) {
      if (matchreg) {
	free(matchreg);
	matchreg = (FIT_REG *) NULL;
      }
      if (fieldreg) {
	free(fieldreg);
	fieldreg = (FIT_REG *) NULL;
      }
      
      if (phraseregwidth) {
	if (pmask) {
	  free(pmask);
	  pmask = (FIT_REG *) NULL;
	}
	if (altpmask) {
	  free(altpmask);
	  altpmask = (FIT_REG *) NULL;
	}
	if (phraseactive) {
	  free(phraseactive);
	  phraseactive = (FIT_REG *) NULL;
	}
	phraseregwidth = 0;
      }
      
      if (proxmatchlist) {
	prf_search_freeprox(proxmatchlist, exe->nproximity);
	free(proxmatchlist);
	proxmatchlist = (struct prf_s_proxmatchlist *) NULL;
	nproximity = 0;
      }
      
      prf_matchinfo_deallocate(cmatchinfo);
      
      return(1);
    }
    
    if (searchstatus & 1) {
      /* Initialize for search of new document */
      exe = (struct prf_s_exe *) xe;
      nrows = exe->nrows;
      ncols = exe->ncols;
      tablesize = nrows * ncols;
      regwidth = exe->regwidth;
      phraseregwidth = exe->phraseregwidth;
      eottable = exe->table.eotflag;
      peottable = exe->phrasetbl.eotflag;
      nfields = min(max(1,exe->nfields),FIT_BITS_IN_LONG);
      nterms = exe->nterms;
      startwordnum = 0;
      buffoffset = 0;
      
      matchreg = (FIT_REG *) calloc((size_t) regwidth, sizeof(*matchreg));
      if (!matchreg) goto ALLOCERR;
      endmatchreg = matchreg + regwidth;
      
      table = exe->table.registers;
      phrasetable = exe->phrasetbl.registers;
      phrasemask = exe->phrasemask;
      phraseterm = exe->phraseterm;
      
      if (phraseregwidth) {
	pmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	  sizeof(*pmask));
	if (!pmask) goto ALLOCERR;
	altpmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	  sizeof(*altpmask));
	if (!altpmask) goto ALLOCERR;
	phraseactive =
	  (FIT_REG *) calloc((size_t) (phraseregwidth * regwidth),
	  sizeof(*phraseactive));
	if (!phraseactive) goto ALLOCERR;
      }
      
      fieldreg = (FIT_REG *) calloc((size_t) ((nfields+1) * regwidth),
	sizeof(*fieldreg));
      if (!fieldreg) goto ALLOCERR;
      curfldreg = fieldreg;
      savefldnum = 0;
      nextfldnum = 0;
      curfldmask = 1;
      savefldmask = 1;
      nextfldmask = 1;
      
      
      inphrase = 0;
      
      if (nproximity) {
	prf_search_freeprox(proxmatchlist,nproximity);
	free(proxmatchlist);
	proxmatchlist = (struct prf_s_proxmatchlist *) NULL;
	nproximity = 0;
      }
      
      if (exe->nproximity) {
	proxmatchlist = (struct prf_s_proxmatchlist *)
	  calloc((size_t) exe->nproximity,sizeof(*proxmatchlist));
	if (!proxmatchlist) goto ALLOCERR;
	nproximity = exe->nproximity;
	for (i=0; i < (unsigned) nproximity; ++i) {
	  proxmatchlist[i].n_1 = 0;
	  proxmatchlist[i].first_1 = (struct prf_s_proxmatch *) NULL;
	  proxmatchlist[i].last_1 = (struct prf_s_proxmatch *) NULL;
	  proxmatchlist[i].n_2 = 0;
	  proxmatchlist[i].first_2 = (struct prf_s_proxmatch *) NULL;
	  proxmatchlist[i].last_2 = (struct prf_s_proxmatch *) NULL;
	}
      }
      if (!prf_matchinfo_allocate(exe, cmatchinfo))
	goto ALLOCERR;
      matchinfo = (prf_s_matchinfo *) *cmatchinfo;
    }
    
    if (searchstatus & 4) {
      memset(void_caste fieldreg, 0, 
	(size_t) (regwidth * nfields * sizeof(*fieldreg)));
      savefldnum = 0;
      nextfldnum = 0;
      savefldmask = 1;
      nextfldmask = 1;
      if (phraseregwidth)
	memset(void_caste phraseactive, 0,
        (size_t) (sizeof(*phraseactive) * phraseregwidth * regwidth));
      startwordnum = 0;
      if (nproximity) prf_search_freeprox(proxmatchlist,nproximity);
      prf_matchinfo_reset(exe, cmatchinfo);
    }
    
    begtext = text;
    endtext = text + textlen;
    
    /* Initialize register variables */
    xlate = exe->tx_xlate;
    
    /* Main loop of term matching, executes once for each table */
    for (i=0;i < (unsigned) regwidth; ++i) {
      curtable = *(table+i);
      text = begtext;
      curfldreg = fieldreg + (savefldnum * regwidth) + i;
      curphrasemask = exe->phrasemask[i];
      curfldmask = savefldmask;
      curphrasemask = exe->phrasemask[i];
      notphrasemask = ~curphrasemask;
      phraseproxmask = exe->phraseproxmask[i];
      notphraseproxmask = ~phraseproxmask;
      prox1mask = exe->prox1mask[i];
      prox2mask = exe->prox2mask[i];
      notproxmask = ~prox1mask & ~prox2mask;
      curwordnum = startwordnum;
      
      if (phraseregwidth) {
        for (j = 0, inphrase = 0, preg = phraseactive + (i * phraseregwidth);
        j < (unsigned) phraseregwidth; ++j) {
          *(pmask+j) = *(preg);
          *(altpmask+j) = 0;
          inphrase |= (*(preg++) != 0);
        }
      }
      
      /* Match terms in current match table against words in buffer */
      for (; text < endtext; ) {
        if ((c = *(xlate + *(text++))) > 0) {
          begword = text - 1;
          curwordnum += 1;
          
          if (inphrase) {
          /* Phrase term matching is active, check latest word in text
            against enabled words in phrase match table */
            
            inphrase = 0;
            
            /* Run through phrase tables */
            for (j=0; j < (unsigned) phraseregwidth; ++j) {
              if (*(pmask+j)) {
                /* Next table in phrase tables has enabled terms, check them */
                row = *(phrasetable+j);
                text = begword;
                c = *(xlate + *(text++));
                /* Run through characters in word */
                for (reg = *(pmask+j); reg && c > 0; row += ncols) {
                  reg &= *(row + c);
                  c = *(xlate + *(text++));
                }
                
                /* Find any words which matched */
                wordmatched = (reg & *(peottable + *(row - 1)));
                if (wordmatched) {
                  phraseprox1mask = exe->phraseprox1mask[j];
                  phraseprox2mask = exe->phraseprox2mask[j];
                  notinproximity = ~phraseprox1mask & ~phraseprox2mask;
                  for (termbase = j << FIT_LONG_EXP, k = 0, mask = 1;
                      wordmatched && mask; ++k, mask <<= 1) {
                    if (wordmatched & mask) {
                      termidx = termbase + k;
                      if ((*(phraseterm+termidx))->nextword < 0) {
                        /* Matched on final word of phrase, set match bit in
                           current field collector register */
                        prf_search_addphraseterm(matchinfo, exe, 
                          (*(phraseterm+termidx))->termnum, 
                          curwordnum, buffoffset + (unsigned long)(text - begtext) - 1L);
                        if (mask & notinproximity) {
                          *curfldreg |= (*(phraseterm+termidx))->termmask;
                        }
                        else if (mask & phraseprox1mask) {
                          termidx = (*(phraseterm+termidx))->termnum;
                          listidx = exe->proxterm1map[termidx];
                          prf_matchinfo_addprox(proxmatchlist+listidx,
                            curwordnum - exe->term[termidx]->nwords+1,
                            curwordnum,1, matchinfo->term[termidx]);
                        }
                        else if (mask & phraseprox2mask) {
                          termidx = (*(phraseterm+termidx))->termnum;
                          listidx = exe->proxterm2map[termidx];
                          prf_matchinfo_addprox(proxmatchlist+listidx,
                            curwordnum - exe->term[termidx]->nwords+1,
                            curwordnum,0, matchinfo->term[termidx]);
                        }
                      }
                      
                      else {
                        /* Enable phrase searching for next word in phrase term */
                        inphrase = 1;
                        termidx = (*(phraseterm+termidx))->nextword;
                        *(altpmask + (termidx >> FIT_LONG_EXP)) |=
                          1L << (termidx % FIT_BITS_IN_LONG);
                      }
                    }
                    wordmatched &= ~mask;
                  }
                }
                /* Clean out phrase mask for next time it gets used to collect
                matches */
                *(pmask+j) = 0;
              }
            }
            /* Swap current phrase enable mask over to the just collected mask */
            temppmask = altpmask;
            altpmask = pmask;
            pmask = temppmask;
            text = begword + 1;
            c = *(xlate + *begword);
          }
          
          /* Do normal term matching */
          for (row = curtable, reg=~(unsigned long) 0L; reg && c > 0; row +=ncols) {
            reg &= *(row + c);
            c = *(xlate + *(text++));
          }
          
          if (reg) {
            if (reg & *(eottable + *(row - 1))) {
              wordmatched = (reg & *(eottable + *(row - 1)));
              matchoffset = buffoffset + (begword - begtext);
              if (wordmatched & phraseproxmask) {
                if (wordmatched & curphrasemask) {
                  startphrase = (wordmatched & curphrasemask);
                  /* For each enabled phrase in startphrase */
                  if (startphrase) {
                    for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
                        startphrase && mask; ++j, mask <<= 1) {
                      if (startphrase & mask) {
                        startphrase &= ~mask;
                        termidx = termbase + j;
                        if ((*(*(exe->term+termidx))->fieldmap) & curfldmask) {
                          if (!prf_matchinfo_addmatch(matchinfo, PRF_ISPHRASEWORD, 
                              termidx, matchoffset, (text - begword) - 1, 
                              curwordnum, nextfldnum)) {
                            return 0;
                          }
                          inphrase = 1;
                          termidx = (*(exe->term+termidx))->nextword;
                          *(pmask + termidx/FIT_BITS_IN_LONG) |=
                            1L << (termidx % FIT_BITS_IN_LONG);
                        }
                      }
                    }
                  }
                }
                if (wordmatched & prox1mask & notphrasemask) {
                  proxmatch = 0L;
                  startprox = wordmatched & prox1mask & notphrasemask;
                  for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
                  startprox && mask; ++j, mask <<= 1) {
                    if (startprox & mask) {
                      startprox &= ~mask;
                      proxmatch |= mask;
                      if (*exe->term[termbase+j]->fieldmap & curfldmask) {
                        if (!prf_matchinfo_addmatch(matchinfo, PRF_ISPROX1WORD, 
                          termbase + j, matchoffset, 
                          (text - begword) - 1, curwordnum, nextfldnum)) {
                          return 0;
                        }
                        listidx = exe->proxterm1map[termbase+j];
                        prf_matchinfo_addprox(proxmatchlist+listidx,
                          curwordnum,curwordnum,1, 
                          matchinfo->term[termbase * FIT_BITS_IN_LONG + j]);
                      }
                    }
                  }
                }
                if (wordmatched & prox2mask & notphrasemask) {
                  proxmatch = 0L;
                  startprox = wordmatched & prox2mask & notphrasemask;
                  for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
                  startprox && mask; ++j, mask <<= 1) {
                    if (startprox & mask) {
                      startprox &= ~mask;
                      proxmatch |= mask;
                      if (matchedwordmask = *exe->term[termbase+j]->fieldmap & curfldmask) {
                        if (!prf_matchinfo_addmatch(matchinfo, PRF_ISPROX2WORD, 
                          termbase + j, matchoffset, 
                          (text - begword) - 1, curwordnum, nextfldnum)) {
                          return 0;
                        }
                        listidx = exe->proxterm2map[termbase+j];
                        prf_matchinfo_addprox(proxmatchlist+listidx,
                          curwordnum,curwordnum,0, 
                          matchinfo->term[termbase * FIT_BITS_IN_LONG + j]);
                      }
                    }
                  }
                }
              }
              if (matchedwordmask = (wordmatched & notphraseproxmask)) {
                *curfldreg |= matchedwordmask;
                for (termbase = i << FIT_LONG_EXP, j=0, mask = 1; 
                    matchedwordmask && j < FIT_BITS_IN_LONG; ++j, mask <<= 1) {
                  if (mask & matchedwordmask) {
                    if (!prf_matchinfo_addmatch(matchinfo, PRF_MATCHED, 
                        termbase + j, matchoffset, (text - begword) - 1, curwordnum, 
                        nextfldnum)) {
                      return 0;
                    }
                    matchedwordmask &= ~mask;
                  }
                }
              }
            }
          }
          while (c > 0)
            c = *(xlate + *(text++));
        }
        if (c < -1)  {
          /* Must be a field marker */
          nextfldnum = max(0, min(nfields-1, ((int) *text)));
          nextfldmask = (long) (1L << (int) nextfldnum);
          curfldreg = fieldreg + (nextfldnum * regwidth) + i;
          curfldmask = nextfldmask;
          ++text;
        }   
      }
    
      /* Update phrase status matrix */
      if (phraseregwidth)
        for (j = 0, preg = phraseactive + (i * phraseregwidth);
            j < (unsigned) phraseregwidth; ++j)
          *(preg++) = *(pmask+j);
    }
  
    startwordnum = curwordnum;
    savefldnum = nextfldnum;
    savefldmask = nextfldmask;
    buffoffset += textlen;
    
    /* Return if not last buffer */
    if (!(searchstatus & 2)) return(1);

    matchinfo->nwords = curwordnum;

    for (i=0; i < (unsigned) nproximity; ++i)
      prf_matchinfo_resolveprox(exe,i,proxmatchlist+i, matchinfo);
    
    /* Set ID matched flags for all term ids which matched */
    for (i=0; i < (unsigned) nterms; ++i)
      if (matchinfo->term[i])
        if (!exe->termtype[i])
          for (term=exe->term[i], j = 0, fieldmap = term->fieldmap; 
              j < (unsigned) term->nid; 
              ++j, ++fieldmap) {
            matchinfo->idmatch[term->logicid[j]] = 
              matchinfo->fldmap[i] & *fieldmap;
            matchinfo->idmap[term->logicid[j]] = i;
          }
          
          
    /* Clean out word matches from match list that didn't really match */
    for (i=0; i < (unsigned) nterms; ++i) {
      for (; matchinfo->term[i] && matchinfo->term[i]->matchtype != PRF_MATCHED; 
          matchinfo->term[i] = nextmatch) {
        nextmatch = matchinfo->term[i]->nextmatch;
        free(matchinfo->term[i]);
      }
      if (matchinfo->term[i]) {
        for (curmatch = matchinfo->term[i]->nextmatch, prevmatch=matchinfo->term[i]; 
            curmatch; prevmatch = curmatch, curmatch = curmatch->nextmatch) {
          if (curmatch->matchtype != PRF_MATCHED) {
            prevmatch->nextmatch = curmatch->nextmatch;
            free(curmatch);
            curmatch = prevmatch;
          }
        }
      }
    }
    
    /* Evaluate logic equations to determine query matches */
    for (i = 0, logic = exe->logic,
        endlogic = exe->logic + exe->nqueries,
        logiclen = exe->logiclen; logic < endlogic; ++logic, ++logiclen, ++i) {
      for (top = stack-1, *stack = 0, token = *logic,
          endtoken = token + *logiclen; token < endtoken; ++token, t = top)
        if (*token >= 0) {
          *(++top) = (matchinfo->idmatch[*token] != 0L);
        }
        else if ((top > stack) || *token == QC_NOT || *token == QC_THRESHHOLD)
          switch (*token) {
            case QC_OR : {
              *(--top) = (*t || *(t-1));
              break;
                       }
            case QC_AND : {
              *(--top) &= (*t && *(t-1));
              break;
                        }
            case QC_NOT : {
              *top = !(*top);
              break;
                        }
            case QC_COMMA : {
              *(--top) += *t;
              break;
                          }
            case QC_THRESHHOLD : {
              *top = (*top >= *(++token));
              break;
            }
          }
		else
		  fprintf(stderr,"Prf_match, stack pointer underflow in logic evaluation\n");
              
        if (*stack) {
          matchinfo->qrymatch[matchinfo->nqrymatches++] = i; 
        }
    }
          
    if (proxmatchlist) prf_search_freeprox(proxmatchlist,exe->nproximity);
      return(1);
          
ALLOCERR:
    fit_status = FIT_FATAL_MEM;
    return(0);
  }


/* Returns number of queries described within matchinfo */
int prf_matchinfo_nqueries(char *cmatchinfo) {
  prf_s_matchinfo *matchinfo = (prf_s_matchinfo *) cmatchinfo;
  return matchinfo->nqueries;
}

/*  parameters:
        matchinfo       matchinfo returned by a prior call to prf_search_matchinfo.

    returns:
        number of matching queries defined within matchinfo.
*/

/**************************************************************************/

/* Returns ID's of queries described in matchinfo. prf_qname may be used to
   retieve the query name based on a query ID. Other prf_matchinfo_* functions
   are used to get more detailed match information for specific query IDs. */

int prf_matchinfo_qrymatches(char *cmatchinfo, int *nmatched, int *(*qryIDs)) {
/*
  Parameters:
      matchinfo       matchinfo returned by call to prf_search_matchinfo.
      nmatched        Returned number of queries that matched.
      qryIDs          An array of query IDs for the matching queries. *qryIDs is
    		      allocated by this function. If *qryIDs is not NULL at entry,
		      it is deallocated by this function at entry. The "free" 
		      function may be used to deallocate *qryIDs.
  returns:
      0 indicates an error occurred.
*/

  prf_s_matchinfo *matchinfo;
  int i;

  if (*qryIDs) {
    free(*qryIDs);
    *qryIDs = (int *) NULL;
  }
  *nmatched = 0;

  if (cmatchinfo) {
    matchinfo = (prf_s_matchinfo *) cmatchinfo;
    *nmatched = matchinfo->nqrymatches;
    if (nmatched) {
      if (!(*qryIDs = (int *) calloc(*nmatched, sizeof(int))))
	return 0;
      for (i=0; i < *nmatched; ++i)
	(*qryIDs)[i] = matchinfo->qrymatch[i];
    }
    return 1;
  }
  else {
    return 0;
  }
}


/**************************************************************************/


/* Returns information needed for highlighting words that matched the
   query or queries use in a search by prf_search_matchinfo. */

int prf_matchinfo_highlightinfo(char *cmatchinfo, unsigned *nmatches, 
  unsigned long *(*offsets), unsigned *(*lengths)) {

/*  parameters:
	matchinfo	matchinfo returned by call to prf_search_matchinfo.
	nmatches	Number of offset/length pairs being returned.
	offsets 	An array of offsets into the document for strings that
			matched. Multiple offsets may point at the same location 
                        due to the fact that an entry in offsets is created for 
                        each term matching a string within the query, and 
                        multiple terms may match on the same or overlapping 
                        strings.*offsets is allocated by this function. If 
			*offsets is not NULL at entry, it is deallocated by this 
			function at entry. The "free" function may be used to 
			deallocate *offsets.
	lengths 	An array of lengths of matching strings within the document.
			offset/length pairs of form offset[i], length[i] identify
			the matching strings and their length. *lengths is
			allocated by this function. If *lengths is not NULL at
			entry, it is deallocated by this function at entry. The
			"free" function may be used to deallocate *lengths.
			
  returns:
	0 indicates an error occurred.
*/

  prf_s_matchinfo *matchinfo = (prf_s_matchinfo *) cmatchinfo;
  prf_s_matchdescriptor *descriptor;
  typedef struct {
    unsigned *nstringsmatched;
    unsigned long *(* offsets);
    unsigned *(* lengths);
  } qmatch;

  qmatch qmatches;

  int *qryids, retstat;
  unsigned long *qmatchoffsets;
  unsigned *qmatchlengths;
  unsigned nstringsmatched, nmatchableterms, ntermsmatched;
  unsigned matchidx, i, j, nqrymatches;

  retstat = 0;
  *nmatches = 0;
  if (*offsets) free(*offsets); *offsets = (unsigned long *) NULL;
  if (*lengths) free(*lengths); *lengths = (unsigned *) NULL;
  qmatches.nstringsmatched = (unsigned *) NULL;
  qmatches.offsets = (unsigned long *(*)) NULL;
  qmatches.lengths = (unsigned *(*)) NULL;
  descriptor = (prf_s_matchdescriptor *) NULL;
  qmatchoffsets = (unsigned long *) NULL;
  qmatchlengths = (unsigned *) NULL;

  qryids = (int *) NULL;

  if (prf_matchinfo_qrymatches(cmatchinfo, (int *) &nqrymatches, &qryids)) {
    if (!(qmatches.nstringsmatched = (unsigned *) calloc(nqrymatches,
	 sizeof(unsigned))))
      goto DONE;
    if (!(qmatches.offsets = (unsigned long *(*)) calloc(nqrymatches,
	 sizeof(unsigned long *))))
      goto DONE;
    if (!(qmatches.lengths = (unsigned *(*)) calloc(nqrymatches,
	 sizeof(unsigned *))))
      goto DONE;
    for (i=0; i < nqrymatches; ++i) {
      if (prf_matchinfo_getqrymatchinfo(cmatchinfo, qryids[i], 
	  (int *) &nstringsmatched, (int *) &nmatchableterms, (int *) &ntermsmatched, &qmatchoffsets,
	  &qmatchlengths)) {
	qmatches.nstringsmatched[i] = nstringsmatched;
	qmatches.offsets[i] = qmatchoffsets;
	qmatchoffsets = (unsigned long *) NULL;
	qmatches.lengths[i] = qmatchlengths;
	qmatchlengths = (unsigned *) NULL;
	*nmatches += nstringsmatched;
      }
      else {
	qmatches.nstringsmatched[i] = 0;
	qmatches.offsets[i] = (unsigned long *) NULL;
	qmatches.lengths[i] = (unsigned *) NULL;
      }
    }
  }

  if (*nmatches) {
     if (!(descriptor = (prf_s_matchdescriptor *) 
          calloc(*nmatches, sizeof(prf_s_matchdescriptor))))
       goto DONE;
     if (!(*offsets = (unsigned long *) calloc(*nmatches, sizeof(*offsets))))
       goto DONE;
     if (!(*lengths = (unsigned *) calloc(*nmatches, sizeof(*lengths))))
       goto DONE;
  }
  
  matchidx = 0;
  for (i=0; i < nqrymatches; ++i) {
    for (j=0; j < qmatches.nstringsmatched[i]; ++j) {
      descriptor[matchidx].offset = qmatches.offsets[i][j];
      descriptor[matchidx++].length = qmatches.lengths[i][j];
    }
  }

  if (nqrymatches > 1)
    qsort((void *) descriptor, (size_t) *nmatches, sizeof(*descriptor), 
      prf_compare_matches);

  for (i=0; i < *nmatches; ++i) {
    (*offsets)[i] = descriptor[i].offset;
    (*lengths)[i] = descriptor[i].length;
  }
  retstat = 1;

DONE:
  if (descriptor) free(descriptor);
  if (qmatches.nstringsmatched) free(qmatches.nstringsmatched);
  if (qmatches.offsets) {
    for (i=0; i < nqrymatches; ++i)
      if (qmatches.offsets[i]) free(qmatches.offsets[i]);
    free(qmatches.offsets);
  }
  if (qmatches.lengths) {
    for (i=0; i < nqrymatches; ++i)
      if (qmatches.lengths[i]) free(qmatches.lengths[i]);
    free(qmatches.offsets);
  }
    
  return retstat;
}


int prf_compare_matches(const void *p1, const void *p2) {
  prf_s_matchdescriptor *i1 = (prf_s_matchdescriptor *) p1;
  prf_s_matchdescriptor *i2 = (prf_s_matchdescriptor *) p2;
  return i1->offset - i2->offset;
}


/**************************************************************************/

/* Returns match information for a particular matching query */
int prf_matchinfo_getqrymatchinfo(char *cmatchinfo, int qryID, int *nstringsmatched,
    int *nmatchableterms, int *ntermsmatched, unsigned long *(*offsets),
    unsigned *(*lengths)) {

/*  parameters:
	matchinfo       matchinfo returned by call to prf_search_matchinfo.
	qryID           query ID for query whose match information is to be returned.
		        prf_matchinfo_qrymatches is used to get the query IDs for
			queries that matched a document.
	nstrngsmatched  Returned number of strings within the document that were 
			matched.
	nmatchableterms Returned total number of terms in query that could have
	  		have matched against the document.
	ntermsmatched   Returned number of terms in query that matched against the
			document. Information about each matching term may be 
			accessed with the prf_matchinfo_gettermmatches function.
	offsets 	An array of offsets into the document for strings that
			matched. Multiple offsets may point at the same location 
                        due to the fact that an entry in offsets is created for 
                        each term matching a string within the query, and 
                        multiple terms may match on the same or overlapping 
                        strings.*offsets is allocated by this function. If 
			*offsets is not NULL at entry, it is deallocated by this 
			function at entry. The "free" function may be used to 
			deallocate *offsets.
	lengths 	An array of lengths of matching strings within the document.
			offset/length pairs of form offset[i], length[i] identify
			the matching strings and their length. *lengths is
			allocated by this function. If *lengths is not NULL at
			entry, it is deallocated by this function at entry. The
			"free" function may be used to deallocate *lengths.
				  
    returns:
	  0 indicates an error occurred.
*/

  prf_s_matchinfo *matchinfo = (prf_s_matchinfo *) cmatchinfo;
  prf_s_matchdescriptor *matchdescr;
  prf_s_match *match;
  int i, matchidx;

  if (!matchinfo) return 0;
  if (!prf_getqrysearchmatches(matchinfo, qryID))
    return 0;
  
  if (*offsets) free(*offsets);
  if (*lengths) free(*lengths);

  *nstringsmatched = matchinfo->qrysearchmatches->nstringsmatched;
  *nmatchableterms = matchinfo->qrysearchmatches->nmatchableterms;
  *ntermsmatched = matchinfo->qrysearchmatches->ntermsmatched;
  if (*ntermsmatched) {
    if (!(matchdescr = (prf_s_matchdescriptor *) calloc(*nstringsmatched, sizeof(*matchdescr))))
      return 0; 
    if (!(*offsets = (unsigned long *) calloc(*nstringsmatched, sizeof(*(*offsets)))))
      return 0;
    if (!(*lengths = (unsigned *) calloc(*nstringsmatched, sizeof(*(*lengths)))))
      return 0;

    for (i=0, matchidx = 0; i < *ntermsmatched; ++i) {
      for (match=matchinfo->qrysearchmatches->term[i].match; match; match=match->nextmatch, ++matchidx) {
        matchdescr[matchidx].offset = match->offset;
        matchdescr[matchidx].length = match->length;
      }
    }

    qsort((void *) matchdescr, (size_t) *nstringsmatched, sizeof(*matchdescr), 
      prf_compare_matches);

    for (i=0; i < *nstringsmatched; ++i) {
      (*offsets)[i] = matchdescr[i].offset;
      (*lengths)[i] = matchdescr[i].length;
    }
    free(matchdescr);
    return 1;
  }
  else {
    *offsets = (unsigned long *) NULL;
    *lengths = (unsigned *) NULL;
  }
  return 1;
}

/* Makes sure that the current qrysearchmatches structure within
   matchinfo is filled in for the specified query. If the current
   qrysearchmatches is for a different query, it is deallocated
   prior to creation of a qrysearchmatches for the specified query. */
int prf_getqrysearchmatches(prf_s_matchinfo *matchinfo, int qryID)
{
/*
  Parameters:
      matchinfo     matchinfo structure filled in previously with a call
                    to prf_search_matchinfo.
      qryID         Query ID for query whose qrysearchmatches is to be built.

  Returns:
       0 if an error occurs. This would typically be due to a memory
         allocation error.
*/
  if (matchinfo->qrysearchmatches && matchinfo->qrysearchmatches->qryID == qryID)
    return 1;
  if (matchinfo->qrysearchmatches) {
    prf_deallocateqrysearchmatches(matchinfo->qrysearchmatches);
    matchinfo->qrysearchmatches = (prf_s_qrysearchmatches *) NULL;
  }
  return prf_setqrysearchmatches(matchinfo, qryID, 
    &matchinfo->qrysearchmatches);
}

/* Deallocates query search matches structure. Query search match 
   structures are created by calls to prf_getqrysearchmatches and 
   prf_setqrysearchmatches. */
int prf_deallocateqrysearchmatches(prf_s_qrysearchmatches *qrysearchmatches) {
/*
  Parameters:
    qrysearchmatches  Query search matches to be deallocated.

  Returns:
    1
*/
  unsigned i;
  prf_s_match *match, *nextmatch;
  if (qrysearchmatches) {
    if (qrysearchmatches->term) {
      for (i=0; i < qrysearchmatches->ntermsmatched; ++i)
        for (match = qrysearchmatches->term[i].match; match; nextmatch=match->nextmatch,
            free(match), match = nextmatch)
          ;
      free(qrysearchmatches->term);
    }
    free(qrysearchmatches);
  }
  return 1;
}


/* fills in query search matches structure for specified query based
   on term matches and query logic found in matchinfo structure. */
int prf_setqrysearchmatches(prf_s_matchinfo *matchinfo, int qryID, 
  prf_s_qrysearchmatches *(*retsearchmatches)) {
/* 
  Parameters:
      matchinfo     matchinfo structure filled in previously with a call
                    to prf_search_matchinfo.
      qryID         Query ID for query whose qrysearchmatches is to be built.
      retsearchmatches Returned search match structure. If *retsearchmatches
                    is not NULL at entry, it is deallocated and rebuilt for
                    the specified qryID.
                    

  Returns:
       0 if an error occurs. This would typically be due to a memory
         allocation error.
*/


#define prf_matchinfo_setoperands(stackitem, v) \
{ \
  prf_s_operand *operand; \
  for (operand = stackitem->first; operand; operand = operand->next) { \
      operand->val = v; \
  } \
}
  int op1val, op2val, retstat, termidx;
  unsigned i;
  prf_s_logicstack *stack, *top;
  int *token, *endtoken;
  prf_s_operand *operand, *prevoperand;
  prf_s_match *match;
  prf_s_qrysearchmatches *qrysearchmatches;
  prf_s_proxtoterm *proximity;

  if (*retsearchmatches) {
    prf_deallocateqrysearchmatches(*retsearchmatches);
    *retsearchmatches = (prf_s_qrysearchmatches *) NULL;
  }

  if (matchinfo->logiclen[qryID] <= 0)
    return 0;

  if (!(stack = (prf_s_logicstack *) calloc(matchinfo->logiclen[qryID] + 1,
      sizeof(*stack))))
    return 0;

  /* Evaluate logic equations to determine term id matches */
  for (token = matchinfo->logic[qryID], 
      endtoken = token + matchinfo->logiclen[qryID], stack->val = 0,top = stack-1,
      stack->first = (prf_s_operand *) NULL, stack->last = (prf_s_operand *) NULL;
      token < endtoken; 
      ++token) {
    if (*token >= 0) {
      (++top)->val = (matchinfo->idmatch[*token] != 0L);
      if (!(top->first = (prf_s_operand *) calloc(1, sizeof(prf_s_operand))))
        return 0;
      top->last = top->first;
      top->first->logicid = *token;
      top->first->val = (matchinfo->idmatch[*token] != 0);
      top->first->next = (prf_s_operand *) NULL;
    }
    else if (*token == QC_OR || *token == QC_AND || *token == QC_COMMA) {
      op2val = (top--)->val;
      op1val = (top)->val;
      switch (*token) {
        case QC_OR : {
          op1val = (op1val || op2val);
          break;
        }
        case QC_AND : {
          op1val = (op1val && op2val);
          break;
        }
        case QC_COMMA : {
          op1val += op2val;
          break;
        }
      }
      /* 
      if (op1val != op2val) {
        prf_matchinfo_setoperands((top+1), op1val);
      }
      if (op1val != (top)->val) {
        prf_matchinfo_setoperands(top, op1val);
      } 
      */
      top->last->next = (top+1)->first;
	  top->last = (top+1)->last;
      top->val = op1val;
    }
    else {
      op1val = top->val;
      switch (*token) {
        case QC_NOT : {
          op1val = !(top->val);
          prf_matchinfo_setoperands(top, op1val);
          break;
        }
        case QC_THRESHHOLD : {
          op1val = (op1val >= *(++token));
          /*
          if ((op1val != 0) != (top->val != 0)) {
            prf_matchinfo_setoperands(top, op1val);
          }
          */
          break;
        }
      }
      top->val = op1val;
    }
  }  
  
  if (!(qrysearchmatches = (prf_s_qrysearchmatches *) 
      calloc(1, sizeof(prf_s_qrysearchmatches))))
    return 0;
  if (!(qrysearchmatches->term = (prf_s_qrytermmatch *) 
      calloc(matchinfo->nterms, sizeof(prf_s_qrytermmatch))))
    return 0;

  qrysearchmatches->nmatchableterms = 0;

  if (top != stack)
    return 0;
  else {
    /* Iterate through the logic equation operands that matched */
    for (operand = stack->first; operand; prevoperand = operand, operand = operand->next,
        free(prevoperand)) {
      termidx = matchinfo->idmap[operand->logicid];
      if (termidx >= 0) {
        qrysearchmatches->nmatchableterms += 1;
        if (operand->val) {
        /* Iterate through the matching term strings looking for those within the 
           enabled fields for the operand */
          for (match = matchinfo->term[termidx];
                match; match = match->nextmatch) {
            if (matchinfo->idmatch[operand->logicid] & (1L << match->fldnum)) {
                /* add pointer at match to list for query */
              if (!prf_s_addqrytermmatch(qrysearchmatches, match, termidx))
                return 0;
            }
          }
        }
      }
      else {
        /* Operand pointed at a proximity relationship, track through
           proximity relationship and add term matches to list for query */
        termidx = -termidx - 1;
        qrysearchmatches->nmatchableterms += matchinfo->proxtoterm[termidx].termidx;
        for (proximity = matchinfo->proxtoterm[termidx].next; proximity; 
            proximity = proximity->next) {
          termidx = proximity->termidx;
          for (match = matchinfo->term[termidx]; match; match = match->nextmatch) {
            if (!prf_s_addqrytermmatch(qrysearchmatches, match, termidx))
              return 0;
          }
        }        
      }
    }
    retstat = 1;
  }

  free(stack);

  /* Pack terms in qrysearchmatches */
  for (i=0, termidx = 0; i < matchinfo->nterms; ++i) {
    if (qrysearchmatches->term[i].used) {
      if (i != (unsigned) termidx)
        qrysearchmatches->term[termidx++] = qrysearchmatches->term[i];
      else
        ++termidx;
    }
  }
  for (i=termidx; i < matchinfo->nterms; ++i) {
    qrysearchmatches->term[i].used = 0;
  }
  *retsearchmatches = qrysearchmatches;
  return retstat;
}


/* Adds a match to query search matches structure */
int prf_s_addqrytermmatch(prf_s_qrysearchmatches *qrysearchmatches, prf_s_match *match,
  int termid) {
/* Parameters:
    qrysearchmatches    Structure into which search matches for the query
                        are stored.
    match               Match structure containing information about the
                        match.
    termid              Term ID for term that created the match. This ID is
                        the index of the term within the search executable used
                        during search processing.

  Returns:
     0 if an error occurs. This is usually due to an unrecoverable memory 
     allocation error.
*/

  prf_s_match *newmatch;
  if (!(newmatch = (prf_s_match *) calloc(1, sizeof(prf_s_match))))
    return 0;
  *newmatch = *match;
  if (!qrysearchmatches->term[termid].nmatched) {
    qrysearchmatches->term[termid].match = (prf_s_match *) NULL;
    qrysearchmatches->term[termid].termid = termid;
    qrysearchmatches->term[termid].used = 1;
    qrysearchmatches->ntermsmatched += 1;
  }
  qrysearchmatches->nstringsmatched += 1;
  newmatch->nextmatch = qrysearchmatches->term[termid].match;
  qrysearchmatches->term[termid].match = newmatch;
  qrysearchmatches->term[termid].nmatched += 1;
  return 1;
}

/**************************************************************************/

/* Returns match information for a query term that matched against a document */
int prf_matchinfo_gettermmatches(char *cmatchinfo, int qryID, int termidx,
    int *nmatches, int *termID, int *(*fldmatched), unsigned long *(*offsets),
    unsigned *(*lengths)) {

/*  parameters:
	matchinfo       matchinfo returned by call to prf_search_matchinfo.
	qryID           query ID for query whose match information is to be returned.
			prf_matchinfo_qrymatches is used to get the query IDs for
			queries that matched a document.
	termidx         Index for term that matched for which information is to
			be returned. termidx may have a value ranging from 0 to
			ntermsmatched as returned by a call to
			prf_matchinfo_getqrymatchinfo.
	nmatches        Returned number of strings that the term matched against 
			within the document.
	termID          Returned Term ID for the term. The prf_gettermvalue
			function may be used to return the search string that
			is the value of the term.
	fldmatched      Returned array of field numbers. There is one entry in this
			array for each string matched by the term. *fldmatched is
			allocated by this function. If *fldmatched is not NULL at
			entry, it is deallocated at entry. *fldmatched may be
			deallocated with the cfree function.
	offsets         Returned array of matching string offsets. There is one entry 
			in this array for each string matched by the term. *offsets is
			allocated by this function. If *offsets is not NULL at
			entry, it is deallocated at entry. *offsets may be
			deallocated with the cfree function.
	lengths         Returned array of matching string lengths. There is one entry 
			in this array for each string matched by the term. *lengths is
			allocated by this function. If *lengths is not NULL at
			entry, it is deallocated at entry. *lengths may be
			deallocated with the cfree function.
				 
   note: fldmatched, offsets, and lengths form a set of triplets of form
	 fldmatched[i], offsets[i], lengths[i] that define a matching
	 string.
				   
				     
   returns:
       0 indicates an error occurred.
*/

/**************************************************************************/


  prf_s_matchinfo *matchinfo = (prf_s_matchinfo *) cmatchinfo;
  prf_s_matchdescriptor *matchdescr;
  prf_s_match *match;
  int i, matchidx;
  
  if (*offsets) free(*offsets); *offsets = (unsigned long *) NULL;
  if (*lengths) free(*lengths); *lengths = (unsigned *) NULL;
  if (*fldmatched) free(*fldmatched); *fldmatched = (int *) NULL;

  if (!matchinfo) return 0;
  if (!prf_getqrysearchmatches(matchinfo, qryID))
    return 0;
  if (termidx < 0 || termidx >= (int) matchinfo->qrysearchmatches->ntermsmatched) {
    return 0;
  }

  *termID = matchinfo->qrysearchmatches->term[termidx].termid;
  *nmatches = matchinfo->qrysearchmatches->term[termidx].nmatched;

  if (*nmatches) {
    if (!(matchdescr = (prf_s_matchdescriptor *) calloc(*nmatches, sizeof(*matchdescr))))
      return 0; 
    if (!(*offsets = (unsigned long *) calloc(*nmatches, sizeof(*(*offsets)))))
      return 0;
    if (!(*lengths = (unsigned *) calloc(*nmatches, sizeof(*(*lengths)))))
      return 0;
    if (!(*fldmatched = (int *) calloc(*nmatches, sizeof(*(*fldmatched)))))
      return 0;

    for (match=matchinfo->qrysearchmatches->term[termidx].match, matchidx = 0; 
        match;
        match=match->nextmatch, ++matchidx) {
      matchdescr[matchidx].offset = match->offset;
      matchdescr[matchidx].length = match->length;
      matchdescr[matchidx].fldnum = match->fldnum;
    }

    qsort((void *) matchdescr, (size_t) *nmatches, sizeof(*matchdescr), 
      prf_compare_matches);

    for (i=0; i < *nmatches; ++i) {
      (*offsets)[i] = matchdescr[i].offset;
      (*lengths)[i] = matchdescr[i].length;
      (*fldmatched)[i] = matchdescr[i].fldnum;
    }
    free(matchdescr);
    return 1;
  }
  else {
    *offsets = (unsigned long *) NULL;
    *lengths = (unsigned *) NULL;
    *fldmatched = (int *) NULL;
  }
  return 1;
}


/* Returns character string value of term specified with termID */
char *prf_gettermvalue(char *xe, int termID) {


/*  parameters:
	exe		Profile executable.
	termID	        Term ID for which string is to be returned. The function
			prf_matchinfo_gettermmatches returns termIDs to identify
			the term within a query that matched against a document.

    returns:
        Character string that is the value of the specified term.
	This character string should never be deallocated by the
	calling function as it is a pointer at a string stored
        within the profile executable.
*/

  prf_s_exe *exe = (prf_s_exe *) xe;
  return exe->term[termID]->val;
}

/**************************************************************************/


/*  Allocates a match information structure and initializes constant
    fields within it based on contents of search execuatble. */
int prf_matchinfo_allocate(prf_s_exe *exe, char *(*cmatchinfo))
/*
  Parameters:
      exe         Search executable for whom match information structure is
                  being created.
      cmatchinfo  Returned initialized match information structure. Elements
                  of cmatchinfo that are derived from the contents of the 
                  search execuatble are created and filled in. If cmatchinfo
                  is not NULL at entry, it is assumed to be an existing match
                  information structure and is deallocated.

  Returns:
      0 indicates an error, usually an unrecoverable memory allocation error.
*/
{
  prf_s_matchinfo *matchinfo;
  prf_s_proxtoterm *newproxtoterm;
  unsigned i, regidx, bitidx;
  unsigned long mask;
  
  if (!exe) return 0;
  
  if (*cmatchinfo)
    prf_matchinfo_deallocate(cmatchinfo);
  
  if (!(matchinfo = (prf_s_matchinfo *) calloc(1, sizeof(prf_s_matchinfo))))
    return 0;
  matchinfo->nterms = exe->nterms;
  matchinfo->nqueries = exe->nqueries;
  matchinfo->nlogicids = exe->nlogicids;
  matchinfo->nproximity = exe->nproximity;
  matchinfo->nqrymatches = 0;
  matchinfo->nwords = 0;
  if (matchinfo->nterms) {
    if (!(matchinfo->term = (prf_s_match *(*))
      calloc(exe->nterms, sizeof(prf_s_match(*)))))
      return 0;
    for (i=0; i < matchinfo->nterms; ++i)
      (matchinfo->term)[i] = (prf_s_match *) NULL;
    if (!(matchinfo->fldmap = (FIT_REG *) calloc(exe->nterms, sizeof(FIT_REG))))
      return 0;
  }
  if (matchinfo->nlogicids) {
    if (!(matchinfo->idmatch = (FIT_REG *) calloc(exe->nlogicids, sizeof(FIT_REG))))
      return 0;
    if (!(matchinfo->idmap = (int *) calloc(exe->nlogicids, sizeof(unsigned))))
      return 0;
  }
  if (matchinfo->nqueries) {
    if (!(matchinfo->logiclen = (int *) calloc(exe->nqueries, sizeof(int))))
      return 0;
    for (i=0; i < matchinfo->nqueries; ++i)
      matchinfo->logiclen[i] = exe->logiclen[i];
    if (!(matchinfo->logic = (int *(*)) calloc(exe->nqueries, sizeof(int *))))
      return 0;
    for (i=0; i < matchinfo->nqueries; ++i) {
      if (!(matchinfo->logic[i] = (int *) calloc(matchinfo->logiclen[i], sizeof(int))))
	return 0;
      memcpy(matchinfo->logic[i], exe->logic[i], matchinfo->logiclen[i] * sizeof(int));
    }
    if (!(matchinfo->qrymatch = (int *) calloc(exe->nqueries, sizeof(int))))
      return 0;
    memset(matchinfo->qrymatch, -1, sizeof(*matchinfo->qrymatch) *
	matchinfo->nqueries);
  }
  if (matchinfo->nproximity) {
    if (!(matchinfo->proxtoterm = (prf_s_proxtoterm *) calloc(matchinfo->nproximity,
        sizeof(prf_s_proxtoterm))))
      return 0;
    for (i=0; i < exe->nterms; ++i) {
      if (exe->termtype[i]) {
        regidx = i / FIT_BITS_IN_LONG;
        bitidx = i % FIT_BITS_IN_LONG;
        mask = 1 << bitidx;
        if (exe->prox1mask[regidx] & mask) {
          if (!(newproxtoterm = (prf_s_proxtoterm *) calloc(1, sizeof(prf_s_proxtoterm))))
            return 0;
          newproxtoterm->termidx = i;
          newproxtoterm->next = matchinfo->proxtoterm[exe->proxterm1map[i]].next;
          matchinfo->proxtoterm[exe->proxterm1map[i]].next = newproxtoterm;
          matchinfo->proxtoterm[exe->proxterm1map[i]].termidx += 1;
        }
        else if (exe->prox2mask[regidx] & mask) {
          if (!(newproxtoterm = (prf_s_proxtoterm *) calloc(1, sizeof(prf_s_proxtoterm))))
            return 0;
          newproxtoterm->termidx = i;
          newproxtoterm->next = matchinfo->proxtoterm[exe->proxterm2map[i]].next;
          matchinfo->proxtoterm[exe->proxterm2map[i]].next = newproxtoterm;
          matchinfo->proxtoterm[exe->proxterm2map[i]].termidx += 1;
        }
      }
    }
  }
  matchinfo->qrysearchmatches = (prf_s_qrysearchmatches *) NULL;
  *cmatchinfo = (char *) matchinfo;
  return 1;
}

/* Deallocates storage utilized by matchinfo that was created with a 
   call to prf_search_matchinfo. This function only needs to be called at
   the completion of searching and/or when it is desired to recover the
   memory utilized by matchinfo. */

int prf_matchinfo_deallocate(char *(*cmatchinfo))
{
/*  parameters:
        matchinfo       matchinfo returned by a prior call to prf_search_matchinfo.

    returns:
        0 indicates an error of some sort occurred.
*/
  
  unsigned i;
  prf_s_matchinfo *matchinfo;
  prf_s_match *curmatch, *nextmatch;
  prf_s_proxtoterm *proximity, *nextproximity;
  
  if (cmatchinfo) {
    if ((matchinfo = (prf_s_matchinfo *) *cmatchinfo)) {
      if (matchinfo->fldmap) free(matchinfo->fldmap);
      if (matchinfo->idmatch) free(matchinfo->idmatch);
      if (matchinfo->idmap) free(matchinfo->idmap);
      if (matchinfo->logic) {
	for (i=0; i < matchinfo->nqueries; ++i)
	  free(matchinfo->logic[i]);
	free(matchinfo->logic);
      }
      if (matchinfo->logiclen) free(matchinfo->logiclen);
      if (matchinfo->qrymatch) free(matchinfo->qrymatch);
      if (matchinfo->term) {
        for (i=0; i < matchinfo->nterms; ++i) {
          for (curmatch = matchinfo->term[i]; curmatch; curmatch = nextmatch) {
            nextmatch = curmatch->nextmatch;
            free(curmatch);
          }
        }
        free(matchinfo->term);
      }
      if (matchinfo->proxtoterm) {
        for (i=0; i < matchinfo->nproximity; ++i) {
          for (proximity=matchinfo->proxtoterm[i].next; proximity; proximity=nextproximity) {
            nextproximity = proximity->next;
            free(proximity);
          }
        }
        free(matchinfo->proxtoterm);
      }
      if (matchinfo->qrysearchmatches) 
        prf_deallocateqrysearchmatches(matchinfo->qrysearchmatches);
      free(matchinfo);
    }
    *cmatchinfo = (char *) NULL;
  }
  return 1;
}

/* Resets match information structure. All elements that record
   search match information are deallocated and reinitialized.
   Elements derived from the search executable and which can remain
   constant across searches are left as is. */
int prf_matchinfo_reset(struct prf_s_exe *exe, char *(*cmatchinfo))
{
/*  parameters:
        matchinfo       matchinfo returned by a prior call to prf_search_matchinfo.

    returns:
	0 indicates an error of some sort occurred.
*/
  
  struct prf_s_match *curmatch, *nextmatch;
  unsigned i;
  struct prf_s_matchinfo *matchinfo;
  
  if (cmatchinfo) {
	if ((matchinfo = (struct prf_s_matchinfo *) *cmatchinfo)) {
	  if (matchinfo->nterms != exe->nterms || matchinfo->nqueries != exe->nqueries ||
		  matchinfo->nlogicids != exe->nlogicids) {
		prf_matchinfo_deallocate(cmatchinfo);
		return prf_matchinfo_allocate(exe, cmatchinfo);
	  }
	  else {
		if (matchinfo->term) {
		  for (i=0; i < exe->nterms; ++i) {
			for (curmatch = matchinfo->term[i]; curmatch; curmatch = nextmatch) {
			  nextmatch = curmatch->nextmatch;
			  free(curmatch);
			}
			matchinfo->term[i] = (prf_s_match *) NULL;
			matchinfo->fldmap[i] = 0L;
		  }
		}
		if (matchinfo->idmatch) {
		  memset(matchinfo->idmatch, 0, matchinfo->nlogicids * 
			sizeof(*matchinfo->idmatch));
		  memset(matchinfo->qrymatch, -1, sizeof(*matchinfo->qrymatch) *
		    matchinfo->nqueries);
		}
		if (matchinfo->qrysearchmatches) {
		  prf_deallocateqrysearchmatches(matchinfo->qrysearchmatches);
		  matchinfo->qrysearchmatches = (prf_s_qrysearchmatches *) NULL;
		}
		matchinfo->nqrymatches = 0;
		matchinfo->nwords = 0;
		matchinfo->nproximity = 0;
	  }
	}
  }
  return 1;
}

/* Adds string match for specified term to match information
  structure */
int prf_matchinfo_addmatch(prf_s_matchinfo *matchinfo, 
  int matchtype, int termidx, unsigned long matchoffset, 
  int matchlen, long curwordnum, int fldnum) {

/*
  Parameters:
      matchinfo     Match information stucture to which match
                    is added.
      matchtype     Indicates type of match. If match is known to
                    be complete, it may have the value of PRF_MATCHED.
                    In progress matches for proximity terms and
                    phrases are specified with values of PRF_ISPROX1WORD,
                    PRF_ISPROX2WORD, and PRF_ISPHRASEWORD. These
                    constants are defined within prfprvte.h.
      termidx       Term index for term that created the match. This
                    is the index of the term within the search executable
                    for which the match information structure was
                    created.
      matchoffset   Byte offset of first character of matching string. Offsets
                    start at 0.
      matchlen      Length of matching string.
      curwordnum    Word offset at which matching string was located.
      fldnum        Field number for field containing matching string. Fields
                    names can corresponding to field numbers can be retrieved
                    with the prf_getflds function.

  Returns:
      0 if an error occurs, usually an unrecoverable memory allocation error.
*/

  struct prf_s_match *match;
  if (!matchinfo) return 0;
  
  if (!(match = (prf_s_match *) calloc(1,sizeof(prf_s_match))))
    return 0;
  match->termidx = termidx;
  match->matchtype = matchtype;
  match->offset = matchoffset;
  match->length = (matchtype != PRF_ISPHRASEWORD) ? matchlen : 0;
  match->wordnum = curwordnum;
  match->fldnum = fldnum;
  match->nextmatch = matchinfo->term[termidx];
  matchinfo->term[termidx] = match;
  matchinfo->fldmap[termidx] |= (1L << fldnum);
  return 1;
}    


/* Used to complete the addition of a phrase term match into a
   match information structure. This function is called when the
   final word of a phrase has matched. */
int prf_search_addphraseterm(prf_s_matchinfo *matchinfo,
    prf_s_exe *exe, int termidx, int endwordnum, unsigned long endoffset) {
/*
  Parameters:
     matchinfo      Match information structure to which phrase match is
                    added. The phrase match was started when the first word
                    of the phrase matched via a call to prf_matchinfo_addmatch
                    function with the match type set to PRF_ISPHRASEWORD.
      exe           Search executable.
      termidx       Term index within the search executable for the first word 
                    of the phrase.
      endwordnum    Word offset for the final matching word of the phrase string
                    that has matched.
      endoffset     Byte offset of the last character of the matching phrase.

  Returns:
      0 indicates an error, usually an unrecoverable memory allocation error.
*/
  
  struct prf_s_match *match;
  
  if (!matchinfo || !exe) return 0;
  for (match = matchinfo->term[termidx]; match; 
    match = match->nextmatch) {
    if ((endwordnum - exe->term[termidx]->nwords) + 1 == match->wordnum) {
      match->matchtype = PRF_MATCHED;
      match->length = endoffset - match->offset;
    }
  }
  return 1;
}

/* Returns query name corresponding to query ID for a query
   that is part of a search executable. */
char *prf_qname(int qryid, char *xe) {
/*
  Parameters:
      qryid       Query ID for the query whose name is to be
                  returned.
      xe          Search execuable containing the query.

  Returns:
      NULL if qryid is invalid or the search executable is invalid
*/
  prf_s_exe *exe = (prf_s_exe *) xe;
  if (exe && qryid >= 0 && (unsigned) qryid < exe->nqueries)
    return exe->qfiles[qryid];
  else
    return (char *) NULL;
}

/* Returns field names defined for the search executable. */
int prf_getflds(char *xe, int *nfields, char *(*(*fields))) {
/*
  Parameters:
      xe          Search executable from which field names are
                  extracted.
      nfields     Number of returned field names.
      fields      Returned list of field names. (*fields)[i] is
                  the name of field number "i".
  Returns
      0 indicates and error, usually and unrecoverable memory allocation error.
*/
  prf_s_exe *exe = (prf_s_exe *) xe;
  int i;

  if (*fields) free(*fields);
  *fields = (char *(*)) NULL;

  if (!(*nfields = exe->nfields))
    return 1;

  if (!(*fields = (char *(*)) calloc(*nfields, sizeof(char *))))
    return 0;
  for (i=0; i < *nfields; ++i) {
    if (!((*fields)[i] = (char *) calloc(strlen(exe->fields[i])+1, sizeof(char))))
      return 0;
    strcpy((*fields)[i], exe->fields[i]);
  }
  return 1;
}
