/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/***********+********************************************************************
Module Name : prf_expqry					expqry.c

Function : Expands storage for non-term related fields in profile executable.

Author : A. Fregly

Abstract : This routine is used to expand the allocation of logic equation
	pointers, query file names and hit file names.

Calling Sequence : stat = prf_expqry(struct prf_s_exe *exe, int n);

  exe	Profile executable.
  n	Number of entries by which to expand the field allocations.
  stat	returned status.

Notes :

Change log :
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_expqry(struct prf_s_exe *exe, int n)
#else
int prf_expqry(exe, n)
  struct prf_s_exe *exe;
  int n;
#endif
{

  int i,newalloc, *newlogiclen, *(*newlogic);
  char *(*newqfiles);

  newalloc = exe->qryalloc + n;

  newlogiclen = (int *) calloc((size_t) newalloc, sizeof(*newlogiclen));
  if (!newlogiclen) goto ALLOCERR;

  newlogic = (int *(*)) calloc((size_t) newalloc, sizeof(*newlogic));
  if (!newlogic) goto ALLOCERR;

  newqfiles = (char *(*)) calloc((size_t) newalloc, sizeof(*newqfiles));
  if (!newqfiles) goto ALLOCERR;

  for (i=0; i < exe->qryalloc; ++i) {
    *(newlogiclen+i) = *(exe->logiclen+i);
    *(newlogic+i) = *(exe->logic+i);
    *(newqfiles+i) = *(exe->qfiles+i);
  }

  if (exe->qryalloc) {
    free(exe->logiclen);
    free(exe->logic);
    free(exe->qfiles);
  }

  exe->logiclen = newlogiclen;
  exe->logic = newlogic;
  exe->qfiles = newqfiles;

  exe->qryalloc = newalloc;

  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
