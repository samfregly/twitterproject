/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_read                                  read.c

Function : Reads profile executable file.

Author : A. Fregly

Abstract : This routine is used to read a profile executable file.

Calling Sequence: stat = prf_read(char *fname, char *(*exe));

  fname         File name for profile executable file.
  exe           Returned pointer at profile executable read in by this routine.
		If exe is not null, it is assumed to be pointing at an
		executable, which is deallocated by this routine.  A new
		profile is then assigned to exe.

Notes :

Change log :
000  05-MAY-90  AMF     Created
001     24-MAR-93  AMF  Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_read(char *fname, char *(*xe))
#else
int prf_read(fname, xe)
  char *fname, *(*xe);
#endif
{

#define LINELEN FIT_FULLFSPECLEN+3
  struct prf_s_exe *exe;
  FILE *handle;
  int stat,i,rowwidth,tblidx;
  size_t len;
  char linein[LINELEN];
  FIT_REG *startreg, *regptr, *endreg;

  exe = (struct prf_s_exe *) *xe;
  stat = 0;
  handle = fopen(fname, FIT_READ);
  if (!handle) goto OPENERR;

  if (*xe) {
    /* Deallocate the old value of the executable */
    stat = prf_dealloc(xe);
    if (!stat) goto DONE;
  }

/* Allocate the executable and allocate storage for fixed size fields */
  exe = (struct prf_s_exe *) calloc(1,sizeof(*exe));
  if (!exe) goto ALLOCERR;
  *xe = (char *) exe;

  exe->term_xlate = (FIT_UWORD *) calloc((size_t) 256,
    sizeof(*(exe->term_xlate)));
  if (!exe->term_xlate) goto ALLOCERR;

  exe->tx_xlate = (FIT_WORD *) calloc((size_t) 256,sizeof(*(exe->tx_xlate)));
  if (!exe->tx_xlate) goto ALLOCERR;

/* read in header */

  /* get size values */
  fread(&(exe->nterms),sizeof(exe->nterms),(size_t) 1,handle);
  fread(&(exe->termalloc),sizeof(exe->termalloc),(size_t) 1,handle);
  fread(&(exe->nlogicids),sizeof(exe->nlogicids),(size_t) 1,handle);
  fread(&(exe->nqueries),sizeof(exe->nqueries),(size_t) 1,handle);
  fread(&(exe->qryalloc),sizeof(exe->qryalloc),(size_t) 1,handle);
  fread(&(exe->nfields),sizeof(exe->nfields),(size_t) 1,handle);
  fread(&(exe->nrows),sizeof(exe->nrows),(size_t) 1,handle);
  fread(&(exe->ncols),sizeof(exe->ncols),(size_t) 1,handle);
  fread(&(exe->regwidth),sizeof(exe->regwidth),(size_t) 1,handle);
  fread(&(exe->phraseregwidth),sizeof(exe->phraseregwidth),1,handle);
  fread(&(exe->nphraseterms),sizeof(exe->nphraseterms),(size_t) 1,handle);
  fread(&(exe->nproximity),sizeof(exe->nproximity),(size_t) 1,handle);
  fread(&(exe->nlogicids),sizeof(exe->nlogicids),(size_t) 1,handle);

/* Allocate variable length fields within profile executable */
  exe->table.registers = (FIT_REG *(*)) calloc((size_t) exe->regwidth,
    sizeof(*(exe->table.registers)));
  if (!exe->table.registers) goto ALLOCERR;

  /* The term match table */
  for (tblidx = 0; tblidx < exe->regwidth; ++tblidx) {
    exe->table.registers[tblidx] =
      (FIT_REG *) calloc((size_t) ((exe->nrows + 1) * exe->ncols),
      sizeof(*exe->table.registers[tblidx]));
    if (!exe->table.registers[tblidx]) goto ALLOCERR;
  }

  /* EOT flags */
  exe->table.eotflag = (FIT_REG *) calloc((size_t) ((exe->nrows + 1) *
    exe->regwidth), sizeof(*(exe->table.eotflag)));
  if (!exe->table.eotflag) goto ALLOCERR;

  if (exe->phraseregwidth) {
    /* The phrase term match table */
    exe->phrasetbl.registers = 
      (FIT_REG *(*)) calloc((size_t) exe->phraseregwidth,
      sizeof(*(exe->phrasetbl.registers)));
    if (!exe->phrasetbl.registers) goto ALLOCERR;

    for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
      exe->phrasetbl.registers[tblidx] =
	(FIT_REG *) calloc((size_t) ((exe->nrows + 1) * exe->ncols),
      sizeof(*exe->phrasetbl.registers[tblidx]));
    if (!exe->phrasetbl.registers[tblidx]) goto ALLOCERR;

    /* EOT flags */
    exe->phrasetbl.eotflag = 
      (FIT_REG *) calloc((size_t) ((exe->nrows + 1) *
      exe->phraseregwidth), sizeof(*(exe->phrasetbl.eotflag)));
    if (!exe->phrasetbl.eotflag) goto ALLOCERR;
  }

  /* Term descriptors */
  exe->term = (struct prf_s_term *(*)) calloc((size_t) exe->termalloc,
    sizeof(*(exe->term)));
  if (!exe->term) goto ALLOCERR;
  for (i=0; i < exe->nterms; ++i) {
    exe->term[i] =
      (struct prf_s_term *) calloc((size_t) 1,sizeof(*exe->term[i]));
    if (!exe->term[i]) goto ALLOCERR;
  }

  /* Logic equation lengths */
  exe->logiclen = (int *) calloc((size_t) exe->qryalloc,
    sizeof(*(exe->logiclen)));
  if (!exe->logiclen) goto ALLOCERR;

  /* Logic equation pointers */
  exe->logic = (int *(*)) calloc((size_t) exe->qryalloc, 
    sizeof(*(exe->logic)));
  if (!exe->logic) goto ALLOCERR;

  /* Query file names */
  exe->qfiles = (char *(*))
    calloc((size_t) exe->qryalloc,sizeof(*(exe->qfiles)));
  if (!exe->qfiles) goto ALLOCERR;

  /* Field names */
  exe->fields = (char *(*)) NULL;
  if (exe->nfields) {
    exe->fields = (char *(*))
	calloc((size_t) exe->nfields, sizeof(*(exe->fields)));
    if (!exe->fields) goto ALLOCERR;
  }

  /* Masks */
  exe->phrasemask = (FIT_REG *) calloc((size_t) exe->regwidth,
    sizeof(*exe->phrasemask));
  if (!exe->phrasemask) goto  ALLOCERR;
  exe->prox1mask = (FIT_REG *) calloc((size_t) exe->regwidth,
    sizeof(*exe->prox1mask));
  if (!exe->prox1mask) goto  ALLOCERR;
  exe->prox2mask = (FIT_REG *) calloc((size_t) exe->regwidth,
    sizeof(*exe->prox2mask));
  if (!exe->prox2mask) goto  ALLOCERR;
  if (exe->phraseregwidth) {
    exe->phraseprox1mask = 
      (FIT_REG *) calloc((size_t) exe->phraseregwidth,
      sizeof(*exe->phraseprox1mask));
    if (!exe->phraseprox1mask) goto  ALLOCERR;
    exe->phraseprox2mask = 
      (FIT_REG *) calloc((size_t) exe->phraseregwidth,
      sizeof(*exe->phraseprox2mask));
    if (!exe->phraseprox2mask) goto  ALLOCERR;
  }
  else {
    exe->phraseprox1mask = (FIT_REG *) NULL;
    exe->phraseprox2mask = (FIT_REG *) NULL;
  }
  exe->phraseproxmask = (FIT_REG *) calloc((size_t) exe->regwidth,
    sizeof(*exe->phraseproxmask));
  if (!exe->phraseproxmask) goto  ALLOCERR;

  /* Term maps */
  exe->proxterm1map = (int *) calloc((size_t) exe->termalloc,
    sizeof(*exe->proxterm1map));
  if (!exe->proxterm1map) goto  ALLOCERR;
  exe->proxterm2map = (int *) calloc((size_t) exe->termalloc,
    sizeof(*exe->proxterm2map));
  if (!exe->proxterm2map) goto  ALLOCERR;

  /* Term types */
  exe->termtype = (int *) calloc((size_t) exe->termalloc,
    sizeof(*exe->termtype));
  if (!exe->termtype) goto  ALLOCERR;

  if (exe->phraseregwidth) {
    /* Phrase term descriptors */
    exe->phraseterm = 
      (struct prf_s_phraseterm *(*)) calloc((size_t) (exe->phraseregwidth *
      sizeof(*(exe->phrasetbl.registers)) * 8), sizeof(*(exe->phraseterm)));
    if (!exe->phraseterm) goto ALLOCERR;
    for (i=0; i < exe->nphraseterms; ++i) {
	exe->phraseterm[i] = (struct prf_s_phraseterm *)
	  calloc((size_t) 1,sizeof(*exe->phraseterm[i]));
      if (!exe->phraseterm[i]) goto ALLOCERR;
    }
  }

  if (exe->nproximity) {
    /* Proximity relationships */
    exe->proximity = 
      (struct prf_s_proximity *) calloc((size_t) exe->nproximity,
      sizeof(*exe->proximity));
    if (!exe->proximity) goto ALLOCERR;
  }

/* Read in other fields of query */
  /* Read in query file names */
  for (i=0; i < exe->nqueries; ++i) {
    fgets(linein, LINELEN, handle);
    len = strlen(linein);
    if (len) {
      linein[len-1] = 0;
      exe->qfiles[i] = (char *) malloc(len);
      if (!exe->qfiles[i]) goto ALLOCERR;
      strcpy(exe->qfiles[i], linein);
    }
    else
      exe->qfiles[i] = (char *) NULL;
  }

  /* Read in field names */
  for (i=0; i < exe->nfields; ++i) {
    fgets(linein, LINELEN, handle);
    len = strlen(linein);
    if (len) {
      linein[len-1] = 0;
      exe->fields[i] = (char *) malloc(len);
      if (!exe->fields[i]) goto ALLOCERR;
      strcpy(exe->fields[i], linein);
    }
    else
      exe->fields[i] = (char *) NULL;
  }

  /* Read in masks */
  fread(exe->phrasemask,sizeof(*exe->phrasemask),(size_t) exe->regwidth,
    handle);
  fread(exe->prox1mask,sizeof(*exe->prox1mask),(size_t) exe->regwidth,handle);
  fread(exe->prox2mask,sizeof(*exe->prox2mask),(size_t) exe->regwidth,handle);
  if (exe->phraseregwidth) {
    fread(exe->phraseprox1mask,sizeof(*exe->phraseprox1mask),
	(size_t) exe->phraseregwidth,handle);
    fread(exe->phraseprox2mask,sizeof(*exe->phraseprox2mask),
	(size_t) exe->phraseregwidth,handle);
  }
  fread(exe->phraseproxmask,sizeof(*exe->phraseproxmask),
    (size_t) exe->regwidth, handle);
  fread(exe->proxterm1map,sizeof(*exe->proxterm1map),(size_t) exe->nterms, 
    handle);
  fread(exe->proxterm2map,sizeof(*exe->proxterm2map),(size_t) exe->nterms,
    handle);
  fread(exe->termtype,sizeof(*exe->termtype),(size_t) exe->nterms,handle);

  /* Read in term descriptors */
  for (i=0; i < exe->nterms; ++i) {
    fread(&(exe->term[i]->ttype),sizeof((*exe->term)->ttype),(size_t) 1,
      handle);
    fread(&exe->term[i]->nid,sizeof(exe->term[i]->nid),(size_t) 1,handle);
    fread(&exe->term[i]->len,sizeof(exe->term[i]->len),(size_t) 1,handle);
    fread(&exe->term[i]->lson,sizeof(exe->term[i]->lson),(size_t) 1,handle);
    fread(&exe->term[i]->rson,sizeof(exe->term[i]->rson),(size_t) 1,handle);
    fread(&exe->term[i]->nwords,sizeof(exe->term[i]->nwords),(size_t)  1,
      handle);
    fread(&exe->term[i]->nextword,sizeof(exe->term[i]->nextword),(size_t) 1,
      handle);
    fgets(linein, LINELEN, handle);
    len = strlen(linein);
    if (len) {
      linein[len-1] = 0;
      exe->term[i]->val = (char *) malloc(len);
      if (!exe->term[i]->val) goto ALLOCERR;
      strcpy(exe->term[i]->val,linein);
    }
    else
     exe->term[i]->val = (char *) NULL;

    if (exe->term[i]->nid) {
      exe->term[i]->fieldmap = (FIT_REG *)
	calloc((size_t) exe->term[i]->nid, sizeof(*exe->term[i]->fieldmap));
      if (!exe->term[i]->fieldmap) goto ALLOCERR;

      exe->term[i]->logicid = (int *)
	calloc((size_t) exe->term[i]->nid, sizeof(*exe->term[i]->logicid));
      if (!exe->term[i]->logicid) goto ALLOCERR;

      fread(exe->term[i]->fieldmap,sizeof(*exe->term[i]->fieldmap),
	(size_t) exe->term[i]->nid,handle);

      fread(exe->term[i]->logicid,sizeof(*exe->term[i]->logicid),
	(size_t) exe->term[i]->nid,handle);
    }
    else {
      exe->term[i]->fieldmap = (FIT_REG *) NULL;
      exe->term[i]->logicid = (int *) NULL;
    }
  }

  /* Read in phrase term descriptors */
  for (i=0; i < exe->nphraseterms; ++i) {
    fread(&exe->phraseterm[i]->len,sizeof(exe->phraseterm[i]->len),(size_t) 1,
      handle);
    fread(&exe->phraseterm[i]->nextword,
      sizeof(exe->phraseterm[i]->nextword),(size_t) 1,handle);
    fread(&exe->phraseterm[i]->termnum,
      sizeof(exe->phraseterm[i]->termnum),(size_t) 1,handle);
    fread(&exe->phraseterm[i]->termmask,
      sizeof(exe->phraseterm[i]->termmask),(size_t) 1,handle);
    fgets(linein, LINELEN, handle);
    len = strlen(linein);
    if (len) {
      linein[len-1] = 0;
      exe->phraseterm[i]->val = (char *) malloc(len);
      if (!exe->phraseterm[i]->val) goto ALLOCERR;
      strcpy(exe->phraseterm[i]->val,linein);
    }
    else
     exe->phraseterm[i]->val = (char *) NULL;
  }

  /* Read in proximity relations */
  for (i=0; i < exe->nproximity; ++i) {
    fread(&exe->proximity[i].id,sizeof(exe->proximity[i].id),(size_t) 1,
      handle);
    fread(&exe->proximity[i].relation,sizeof(exe->proximity[i].relation),
      (size_t) 1,handle);
    fread(&exe->proximity[i].distance,sizeof(exe->proximity[i].distance),
      (size_t) 1,handle);
  }

  /* Read in logic equations */
  for (i=0; i < exe->nqueries; ++i) {
    fread(&exe->logiclen[i],sizeof(exe->logiclen[i]),(size_t) 1,handle);

    exe->logic[i] = (int *) calloc((size_t) exe->logiclen[i],
      sizeof(*exe->logic[i]));

    if (!exe->logic[i]) goto ALLOCERR;

    fread(exe->logic[i],sizeof(*exe->logic[i]),
      (size_t) exe->logiclen[i],handle);
  }

  fread(exe->term_xlate,sizeof(*(exe->term_xlate)),(size_t) 256,handle);
  fread(exe->tx_xlate,sizeof(*(exe->tx_xlate)),(size_t) 256,handle);

  /* Term match tables */
  rowwidth = exe->ncols;
  for (tblidx = 0; tblidx < exe->regwidth; ++tblidx)
    for (regptr = *(exe->table.registers+tblidx),
	endreg = regptr + exe->nrows * rowwidth; regptr != endreg;) {
      for (i = 1, fread(&len,sizeof(len),(size_t) 1,handle), startreg = regptr,
	  fread(startreg,sizeof(*regptr),(size_t) 1,handle), regptr++;
	  i < len; ++i)
	*(regptr++) = *startreg;
      regptr = startreg + len;
    }

  /* Set eot table offset in match table */
  for (tblidx = 0; tblidx < exe->regwidth; ++tblidx)
    for (i=0, regptr = *(exe->table.registers+tblidx) + exe->ncols - 1;
	i < (exe->nrows+1); regptr += exe->ncols, ++i)
      *regptr = (i * exe->regwidth) + tblidx;

  fread(exe->table.eotflag,sizeof(*(exe->table.eotflag)),
    (size_t) (exe->nrows * exe->regwidth), handle);


  /* Phrase term match tables */
  for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
    for (regptr = *(exe->phrasetbl.registers+tblidx),
	endreg = regptr + exe->nrows * rowwidth; regptr != endreg;) {
      for (i = 1, fread(&len,sizeof(len),(size_t) 1,handle), 
	  startreg = regptr,
	  fread(startreg,sizeof(*regptr),(size_t) 1,handle), regptr++;
	  i < len; ++i)
	*(regptr++) = *startreg;
      regptr = startreg + len;
    }

  /* Set eot table offset in match table */
  for (tblidx = 0; tblidx < exe->phraseregwidth; ++tblidx)
    for (i=0, regptr = *(exe->phrasetbl.registers+tblidx) + exe->ncols - 1;
	i < (exe->nrows+1); regptr += exe->ncols, ++i)
      *regptr = (i * exe->phraseregwidth) + tblidx;

  if (exe->phraseregwidth)
    fread(exe->phrasetbl.eotflag,sizeof(*(exe->phrasetbl.eotflag)),
      (size_t) (exe->nrows * exe->phraseregwidth), handle);

  stat = 1;
  goto DONE;

OPENERR:
  fit_status = FIT_ERR_OPEN;
  stat = 0;
  goto DONE;

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  stat = 0;

DONE:
  if (handle) fclose(handle);
  return(stat);
}
