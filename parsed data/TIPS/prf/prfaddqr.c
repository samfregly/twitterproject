/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_addqry					addqry.c

Function : Adds query to profile executable.

Author : A. Fregly

Abstract : This routine is used to add a compiled query to the profile
  executable.

Calling Sequence :
  stat = prf_addqry(char *qname, char *hname, struct qc_s_cmpqry *cmpqry,
    char *exe);

  qname		Query name.
  hname		Hit file name.
  cmpqry	Compiled query.
  exe		Profile executable.
  stat		Returned status code.


Notes :

Change log :
000	19-MAY-90  AMF	Created.
001     01-JUN-91  AMF  Used unsigned char for indexing into translation
                        tables so that bytes with bit 8 set don't screw up.
002	15-AUG-91  AMF	Set termmap->termidx correctly for following terms in
			proximity relationship. This appears to have no
			functional difference other than being consistent with
			the setting for the leading terms.
003	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tips.h>
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_addqry(char *qname, struct qc_s_cmpqry *cmpqry, char *xe)
#else
int prf_addqry(qname, cmpqry, xe)
  char *qname;
  struct qc_s_cmpqry *cmpqry;
  char *xe;
#endif
{

  struct prf_s_exe *exe;

  struct prf_s_termmap *termmap;
  struct prf_s_proximity *newproximity;

  int i,j,k,n,stat,*fieldmap, cmptermidx, exetermidx, *logic;
  FIT_REG flds;
  unsigned char *delims;

  exe = (struct prf_s_exe *) xe;
  stat = 0;
  termmap = (struct prf_s_termmap*) NULL;
  fieldmap = (int *) NULL;

  /* Map fields in compiled query to those defined for query executable */
  if (cmpqry->nfields) {
    fieldmap = (int *) calloc((size_t) cmpqry->nfields,sizeof(*fieldmap));
    if (!fieldmap) goto ALLOCERR;
    for (i=0; i < cmpqry->nfields; ++i) {
      *(fieldmap+i) = -1;
      for (j=0; j < exe->nfields; ++j)
	if (!strcmp(cmpqry->fields + *(cmpqry->fldoffsets + i),
		   *(exe->fields+j))) {
	  *(fieldmap+i) = j;
	  j = exe->nfields;
	}
    }
  }
  else
    fieldmap = (int *) NULL;

  /* Make copy of logic equation */
  logic = (int *) calloc((size_t) cmpqry->nlogic, sizeof(*logic));
  if (!logic) goto ALLOCERR;
  for (i=0; i < cmpqry->nlogic; ++i)
    *(logic+i) = *(cmpqry->qrylogic+i);


  /* Set term translation table */
  for (i=0; i < 256; ++i)
    exe->term_xlate[i] = 0;

  delims = (unsigned char *) cmpqry->delims;
  for (i=0; i < QC_MAXDELIM; ++i)
    if (delims[i] && delims[i] != ' ' &&
	i != QC_OFF_OPENPAREN &&
	i != QC_OFF_CLOSEPAREN &&
	i != QC_OFF_FIELDDELIM &&
	i != QC_OFF_DELIM_COMMA &&
	i != QC_OFF_QUOTE)
      exe->term_xlate[delims[i]] = i+1;

  /* Create term type map */
  termmap = (struct prf_s_termmap *) calloc((size_t) cmpqry->nterms,
    sizeof(*termmap));
  if (!termmap) goto ALLOCERR;

  if (cmpqry->nproximity) {

    /* Expand proximity relations in exe */
    newproximity = (struct prf_s_proximity *)
      calloc((size_t) (exe->nproximity + cmpqry->nproximity),
      sizeof(*newproximity));
    if (!newproximity) goto ALLOCERR;
    if (exe->proximity) {
      for (i = 0; i < exe->nproximity; ++i)
	newproximity[i] = exe->proximity[i];
      free(exe->proximity);
    }
    exe->proximity = newproximity;

    /* Flag proximity terms in term map */
    for (i=0; i < cmpqry->nproximity; ++i) {
      /* Fill in the proximity descriptor in the executable */
      n = exe->nproximity++;
      exe->proximity[n].id = exe->nlogicids++;
      exe->proximity[n].relation = cmpqry->proximity[i]->relation;
      exe->proximity[n].distance = cmpqry->proximity[i]->distance;

      /* Map terms to proximity lists */
      for (j=0; j < cmpqry->proximity[i]->list0->nelements; ++j) {
	termmap[cmpqry->proximity[i]->list0->element[j]].termtype = QC_PROXIMITY;
	termmap[cmpqry->proximity[i]->list0->element[j]].listnum = n;
	termmap[cmpqry->proximity[i]->list0->element[j]].termidx = i+1;
      }

      for (j=0; j < cmpqry->proximity[i]->list1->nelements; ++j) {
	termmap[cmpqry->proximity[i]->list1->element[j]].termtype = QC_PROXIMITY;
	termmap[cmpqry->proximity[i]->list1->element[j]].listnum = n;
	termmap[cmpqry->proximity[i]->list1->element[j]].termidx = -(i+1);
      }
    }
  }


  /* Patch logic equation with exe term indexes, adding new terms to term
     list */
  for (i=0; i < cmpqry->nterms; ++i) {
    /* Patch field map for term */
    flds = 0;
    for (j=0; j < cmpqry->nfields; ++j)
      if (((long) (1L << j)) & *(cmpqry->fieldmap+i) && *(fieldmap+j) >= 0)
	flds |= (FIT_REG) (1L << *(fieldmap+j));
    if (!flds) flds = ~flds;

    /* Determine term index in profile exe, possibly making entry for term */
    cmptermidx = i;
    stat = prf_addterm(cmpqry->qrytext + *(cmpqry->offsets+i),cmpqry->delims,
	termmap+i, flds, exe, &exetermidx);
    if (!stat) goto DONE;


    if (!termmap[i].termtype)
    /* Patch term number in logic equation */
      for (j=0; j < cmpqry->nlogic; ++j)
	if (*(cmpqry->qrylogic+j) == cmptermidx) {
	  *(logic+j) = exetermidx;
	  j = cmpqry->nlogic;
      }
  }

  /* Patch term number of proximities into logic equation */
  for (i=0, k = exe->nproximity - cmpqry->nproximity; 
      i < cmpqry->nproximity; ++i, ++k)
    for (j = 0; j < cmpqry->nlogic; ++j)
      if (cmpqry->qrylogic[j] == cmpqry->proximity[i]->termnum) {
	logic[j] = exe->proximity[k].id;
	j = cmpqry->nlogic;
      }


  /* Add logic, query name, hit file name */
  if (exe->nqueries == exe->qryalloc) {
    /* Expand pointer lists to accomodate query */
    stat = prf_expqry(exe, FIT_BITS_IN_LONG);
    if (!stat) goto DONE;
  }

  exe->logic[exe->nqueries] = logic;
  exe->logiclen[exe->nqueries] = cmpqry->nlogic;

  if (!(stat = prf_addname(qname, exe->nqueries, exe->qfiles))) goto DONE;
  exe->nqueries += 1;
  goto DONE;

ALLOCERR:
  stat = 0;
  fit_status = FIT_FATAL_MEM;

DONE:
  if (termmap) free(termmap);
  if (fieldmap) free(fieldmap);
  return(stat);
}
