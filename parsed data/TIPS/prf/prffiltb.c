/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_filltbl                                       filltbl.c

Function : Fills in match registers and eot flags for word.

Author : A. Fregly

Abstract :

Calling Sequence : stat = prf_filltbl(unsigned long *registers,
  unsigned long *eotflag, int nrows, int ncols, int regwidth,
  unsigned int *term_xlate, int *tx_xlate, int idx, char *val, int len);

  registers     Match table registers.
  eotflag       End of term flags table.
  nrows         Number of rows in table.
  ncols         Number of columns in registers.
  regwidth      Number of long words which make up width of a column.
  term_xlate    Term translation table.
  tx_xlate      Text translation table.
  idx           Index of word to be inserted.
  val           Word to be inserted.
  len           Length of word to be inserted.

Notes :

Change log :
000     17-NOV-90  AMF  Created.
001     01-JUN-91  AMF  Used unsigned char for indexing into tables so that
			value element with high bit set don't screw up.
002     27-JUL-94  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <prf.h>
#include <prfprvte.h>
#include <tips.h>

#if FIT_ANSI
int prf_filltbl(FIT_REG *registers, FIT_REG *eotflag,
  int nrows, int ncols, int regwidth, FIT_UWORD *term_xlate,
  char delims[QC_MAXDELIM], FIT_WORD *tx_xlate, int idx, char *val, int vlen)
#else
int prf_filltbl(registers, eotflag, nrows, ncols, regwidth, term_xlate,
    delims, tx_xlate, idx, val, vlen)
  FIT_REG *registers, *eotflag;
  int nrows, ncols, regwidth;
  FIT_UWORD *term_xlate;
  char delims[QC_MAXDELIM];
  FIT_WORD *tx_xlate;
  int idx;
  char *val;
  int vlen;
#endif
{

  int i,j,matchchar,offset,charoffset;
  FIT_REG mask, *regptr, *row, *nextrow;
  int c,rangec,prevc,quoted,rangeon;
  unsigned char *v, *dlm;

  v = (unsigned char *) val;
  dlm = (unsigned char *) delims;

  /* Set up enable mask for this term */
  mask = (FIT_REG) 1L << (idx % FIT_BITS_IN_LONG);

  /* Set register offset of term in eotflag table */
  offset = idx / FIT_BITS_IN_LONG;

  /* Parse the term, setting the appropriate bits in the match registers */
  for (i = 0, charoffset = -1, row = registers, nextrow = row + ncols;
      i < vlen && charoffset < ncols-1;
      ++i, ++charoffset, row += ncols, nextrow += ncols) {

    /* Get next character */
    c = *(v+i);

    /* Use term translation table to see if character has special function
       i.e. a wild card or other reserved delimiter */
    matchchar = *(term_xlate+c);

    if (!matchchar || c == dlm[QC_OFF_QUOTE] ||
	c == dlm[QC_OFF_RANGEDELIM]) {

      /* not a wild carded character */
      if (c == dlm[QC_OFF_QUOTE]) ++i;

      if (i < vlen) {
	matchchar = min(ncols-2, *(tx_xlate+*(v+i)));
	if (matchchar > 0) *(row + matchchar) |= mask;
      }
    }

    else if (c == dlm[QC_OFF_SCAWC]) {
      /* single character wild card */
      for (regptr = row; regptr < (nextrow - 1); ++regptr)
	*regptr |= mask;
    }

    else if (c == dlm[QC_OFF_SCNWC]) {
      /* single character numeric wild card */
      for (c = '0'; c <= '9'; ++c) {
	matchchar = *(tx_xlate+c);
	if (matchchar > 0) *(row + matchchar) |= mask;
      }
    }

    else if (c == dlm[QC_OFF_SETWC_OPEN]) {
      /* set selector, all chars between set delimiter characters.  Quoting
	 character may be used for entering reserved characters */

      /* Advance to first character of set */
      ++i;

      /* Parse the characters in the set, adding them to match table */
      for (c = *(v+i), prevc = -1, rangeon = 0; 
	  i < vlen && c != dlm[QC_OFF_SETWC_CLOSE];
	  ++i, c = *(v+i)) {

	quoted = 0;
	if (c == dlm[QC_OFF_QUOTE]) {
	  /* Next char was quoted, set quote flag and point to quoted char */
	  ++i;
	  c = *(v+i);
	  quoted = 1;
	}

	if (i < vlen) {
	  /* Check for range delimiter */
	  if (c == dlm[QC_OFF_RANGEDELIM] && !quoted) {
	    /* Range delimiter found, set flag so that nxt time  through
	       loop, range will be created */
	    rangeon = 1;
	  }
	  else {
	    if (rangeon) {
	      /* Processing a character range, fill in match registers for all
	      characters in range */
	      for (rangec = min(prevc,c); rangec <= max(prevc,c); ++rangec) {
		matchchar = *(tx_xlate + rangec);
		if (matchchar > 0 && matchchar < ncols - 1)
		  *(row+matchchar) |= mask;
	      }
	      /* Turn off range flag */
	      rangeon = 0;
	    }
	    else {
	      /* Non-ranged set character, set bit for char in match
		 registers */
	      matchchar = *(tx_xlate+c);
	      if (matchchar > 0 && matchchar < ncols-1)
		*(row + matchchar) |= mask;

	      /* Remember this character in case it is first character
		 in a range */
	      prevc = c;
	    }
	  }
	}
      }
    }

    else if (c == dlm[QC_OFF_NSWC]) {
      /* Variable length numeric wild card */
      /* set match bit in numeric columns of all remaining rows */
      for (; i < nrows; ++i, row += ncols, nextrow += ncols)
	for (c = '0'; c <= '9'; ++c) {
	  matchchar = *(tx_xlate+c);
	  if (matchchar > 0) *(row + matchchar) |= mask;
	}

      /* Set EOT flag for all remaining rows */
      for (j = max(0,charoffset),
	  regptr = eotflag+(j * regwidth) + offset;
	  j < nrows; ++j, regptr += regwidth)
	*regptr |= mask;

      /* Disable further processing of term */
      i = vlen;
    }

    else if (c == dlm[QC_OFF_ASWC]) {
      /* variable length don't care wild card */
      for (j=charoffset+1; j < nrows; ++j, row += ncols, nextrow += ncols)
	for (regptr = row; regptr < (nextrow - 1); ++regptr)
	  *regptr |= mask;

      /* Set EOT flag for all remaining rows */
      for (j = max(0,charoffset),
	  regptr = eotflag + (j*regwidth) + offset;
	  j < nrows; ++j, regptr += regwidth)
	*regptr |= mask;

      /* Disable further processing of term */
      i = vlen;
    }
  }

  /* set completion bit */
  if (charoffset >= 0)
    *(eotflag + (charoffset * regwidth) + offset) |= mask;

  return(charoffset >= 0);
}
