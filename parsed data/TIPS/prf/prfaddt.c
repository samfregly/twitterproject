/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_addterm					addterm.c

Function : Add a non-range term to the profile executable.

Author : A. Fregly

Abstract : The term is inserted into the term list for the profile if it
  is not already present.  Also if the term is not present in the match
  table, a column within the match table is allocated for the term and
  initialized to match on the term.

Calling Sequence :
  stat = prf_addterm(char *term, char delims[QC_MAXDELIM],
    struct prf_s_termmap *termmap, unsigned long fields, 
    struct prf_s_exe *exe, int *retid);

    term	Value of term to add to query.
    delims	Delimiters, from compile query.
    termmap	Term mapping structure.
    fields	Field bit map for terms.
    exe		Profile executable.
    retid	Returned index assigned to term/field combo.  This value
		is what is patched into the logic equation in place of the
		original term index.

Notes :

Change log :
000	24-MAY-90  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>

#if FIT_ANSI
int prf_addterm(char *term, char delims[QC_MAXDELIM],
    struct prf_s_termmap *termmap, unsigned long fields,
    struct prf_s_exe *exe, int *retid)
#else
int prf_addterm(term, delims, termmap, fields,exe, retid)
  char *term, delims[QC_MAXDELIM];
  struct prf_s_termmap *termmap;
  unsigned long fields;
  struct prf_s_exe *exe;
  int *retid;
#endif
{

  int idx, stat, i, compare=0;

  /* See if term is already in table */
  idx = 0;
  stat = prf_lookup(term,termmap->termtype, exe, &idx, &compare);
  if (!stat) {
    /* Allocate term node */
    if (exe->regwidth * FIT_BITS_IN_LONG <= exe->nterms) {
      /* Current block of preallocated terms is full, expand */
      stat = prf_expterms(exe, 1);
      if (!stat) goto DONE;
    }

    /* Initialize a descriptor node, linking it to term list */
    stat = prf_alloctrm(exe,delims,term,termmap,idx,compare,&idx);
    if (!stat) goto DONE;
  }

  /* See if field map matches any already defined for term, adding it if
     it doesn't */
  for (i=0, stat=0; !stat && i < (*(exe->term+idx))->nid; ++i)
    stat = (fields == *((*(exe->term+idx))->fieldmap+i));
  if (!stat) {
    /* Have to allocate a field node */
    exe->nlogicids += 1;
    stat = prf_addid(*(exe->term+idx),exe->nlogicids-1,fields);
    if (!stat) goto DONE;
    *retid = exe->nlogicids-1;
  }
  else {
    /* Found match for term and field, return assigned ID */
    *retid = *((*(exe->term+idx))->logicid+i-1);
  }
  stat = 1;
DONE:
  return(stat);
}
