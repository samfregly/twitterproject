/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_addname					addname.c

Function : Adds name to list of names.

Author : A. Fregly

Abstract :

Calling Sequence : stat = prf_addname(char *name, int slot, char *(*list));

  name		Name to be added to list.
  slot		Slot in which to put name.
  list		List of names to which name is added


Notes :

Change log :
000	14-AUG-90  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

#if FIT_ANSI
int prf_addname(char *name, int slot, char *(*list))
#else
int prf_addname(name, slot, list)
  char *name;
  int slot;
  char *(*list);
#endif
{

  size_t len;

  len = strlen(name);
  *(list+slot) = (char *) malloc((size_t) (len+1));
  if (*(list+slot) == NULL) return(0);
  strcpy(*(list+slot),name);
  return 1;
}
