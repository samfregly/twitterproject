/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*****************************************************************************
Module Name : prf_search                                       search.c

Function : Matches terms in match table against words in buffer.

Author : A. Fregly

Abstract : This routine is the "portable" version of the TIPS search engine.

Calling Sequence : stat = prf_search(int searchstatus, unsigned int opts,
  char *exe, char *text, unsigned int textlen, int *nmatches,
  int *(*matches), unsigned long *(*qrytermmatches),
  unsigned long *exetermmatches);

  searchstatus  Bit map defining where program is in context of current
                document.
		0 - On, then initialize allocate and initialize internal data
		    structures and allocate storage for matches, then search
		    first buffer of document.
		1 - On, then in final buffer of document.
		2 - On, then do not allocate internal data structures, but
		    initialize them and search initial buffer of document.
		3 - Deallocate internal data structures and matches, and return
		    without searching.
  opts		Options bit mask:
			bit 0 - Return query term match counts
			bit 1 - Return total term match count
  exe		Profile executable.
  text		Text buffer to be searched.
  textlen	Length of text buffer.
  nmatches	Returned number of query matches.
  matches	Returned list of query identifiers for queries which
		matched.  Storage for matches is allocated by this routine,
		so the user should never pre-allocate storage for matches.
		Any preallocated storage will be deallocated prior to the
		search.
  qrytermmatches	Returned, for each query, a count of the number of
		terms which matched.
  exetermmatches	Returned count of the total number of term matches.

Notes :

Change log :
000	26-AUG-90  AMF	Created.
001	08-MAY-91  AMF	Substitute "exe->nproximity" for "exe" in calls to
			prf_search_freeprox.
002	07-FEB-92  AMF	Type casts so it will compile on the SUN.
003	14-DEC-92  AMF	Compile under COHERENT.
004	24-MAR-93  AMF	Compile under GNU C.
005	29-APR-97  AMF	Fix index calc for lookup of matching profile terms
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <prox.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_search(int searchstatus, unsigned int opts, char *xe,
  register unsigned char *text, unsigned int textlen, int *nmatches,
  int *(*matches), unsigned long *(*qrytermmatches), 
  unsigned long *exetermatches)
#else
int prf_search(searchstatus, opts, xe, text, textlen, nmatches, matches,
    qrytermmatches, exetermatches)
  int searchstatus;
  unsigned int opts;
  char *xe;
#if FIT_USEREGISTERS
  register unsigned char *text;
#else
  unsigned char *text;
#endif
  unsigned int textlen;
  int *nmatches, *(*matches);
  unsigned long *(*qrytermmatches), *exetermatches;
#endif
{

#if FIT_USEREGISTERS
  register FIT_REG reg;
  register FIT_WORD *xlate;
  register FIT_WORD c;
  register FIT_REG *row;
  register FIT_REG *curtable;
#else
  static FIT_REG reg;
  static FIT_WORD *xlate;
  static FIT_WORD c;
  static FIT_REG *row;
  static FIT_REG *curtable;
#endif

  static int ncols;

  static FIT_REG *eottable;
  static long startwordnum, curwordnum;
  static int inphrase;
  static unsigned char *begword, *endtext;
  static FIT_REG wordmatched;
  static int *top, *token;
  static unsigned int i,j;

  static FIT_REG *preg;
  static struct prf_s_exe *exe;
  static FIT_REG *(*table), *endmatchreg;
  static int startphrase;
  static FIT_REG notphraseproxmask, notphrasemask;
  static FIT_REG *curfldreg, mask, *startfldreg;
  static int regwidth, *(*logic),*endtoken, *(*endlogic),nrows;
  static int *matched, *logiclen, nfields;

  static int stack[1024];
  static unsigned int tablesize, k;
  static FIT_REG *fieldmap, *temppmask;
  static unsigned char *begtext;
  static struct prf_s_term *term;
  static int listidx;
  static FIT_REG *phrasemask, *(*phrasetable);
  static FIT_REG *peottable,curphrasemask,curfldmask;
  static int termbase, termidx, nterms;
  static struct prf_s_phraseterm *(*phraseterm);
  static FIT_REG phraseproxmask, prox1mask, prox2mask;
  static FIT_REG phraseprox1mask,phraseprox2mask;
  static FIT_REG startprox, notproxmask, notinproximity;
  static unsigned int savefldnum, nextfldnum;
  static FIT_REG savefldmask, nextfldmask;

  static FIT_REG *matchreg = (FIT_REG *) NULL;
  static FIT_REG *fieldreg = (FIT_REG *) NULL;
  static char *idmatches = (char *) NULL;
  static FIT_REG *termmatch = (FIT_REG *) NULL;
  static int phraseregwidth = 0;
  static FIT_REG *pmask = (FIT_REG *) NULL;
  static FIT_REG *altpmask = (FIT_REG *) NULL;
  static FIT_REG *phraseactive = (FIT_REG *) NULL;
  static struct prf_s_proxmatchlist *proxmatchlist =
	(struct prf_s_proxmatchlist *) NULL;
  static int nproximity=0;

  /* Deallocate data structures */
  if (searchstatus & 8) {
    if (matchreg) {
      free(matchreg);
      matchreg = (FIT_REG *) NULL;
    }
    if (fieldreg) {
      free(fieldreg);
      fieldreg = (FIT_REG *) NULL;
    }
    if (idmatches) {
      free(idmatches);
      idmatches = (char *) NULL;
    }
    if (*matches) {
      free(*matches);
      *matches = (int *) NULL;
    }
    if (termmatch) {
      free(termmatch);
      termmatch = (FIT_REG *) NULL;
    }

    if (phraseregwidth) {
      if (pmask) {
	free(pmask);
	pmask = (FIT_REG *) NULL;
      }
      if (altpmask) {
	free(altpmask);
	altpmask = (FIT_REG *) NULL;
      }
      if (phraseactive) {
	free(phraseactive);
	phraseactive = (FIT_REG *) NULL;
      }
      phraseregwidth = 0;
    }

    if (proxmatchlist) {
      prf_search_freeprox(proxmatchlist, exe->nproximity);
      free(proxmatchlist);
      proxmatchlist = (struct prf_s_proxmatchlist *) NULL;
      nproximity = 0;
    }
    return(1);
  }

  if (searchstatus & 1) {
  /* Initialize for search of new document */
    exe = (struct prf_s_exe *) xe;
    nrows = exe->nrows;
    ncols = exe->ncols;
    tablesize = nrows * ncols;
    regwidth = exe->regwidth;
    phraseregwidth = exe->phraseregwidth;
    eottable = exe->table.eotflag;
    peottable = exe->phrasetbl.eotflag;
    nfields = min(max(1,exe->nfields),FIT_BITS_IN_LONG);
    nterms = exe->nterms;
    startwordnum = 0;

    matchreg = (FIT_REG *) calloc((size_t) regwidth, sizeof(*matchreg));
    if (!matchreg) goto ALLOCERR;
    endmatchreg = matchreg + regwidth;

    termmatch = (FIT_REG *) calloc((size_t) exe->nterms,sizeof(*termmatch));
    if (!termmatch) goto ALLOCERR;

    table = exe->table.registers;
    phrasetable = exe->phrasetbl.registers;
    phrasemask = exe->phrasemask;
    phraseterm = exe->phraseterm;

    if (phraseregwidth) {
      pmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	sizeof(*pmask));
      if (!pmask) goto ALLOCERR;
      altpmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	sizeof(*altpmask));
      if (!altpmask) goto ALLOCERR;
      phraseactive =
	(FIT_REG *) calloc((size_t) (phraseregwidth * regwidth),
	sizeof(*phraseactive));
      if (!phraseactive) goto ALLOCERR;
    }

    fieldreg = (FIT_REG *) calloc((size_t) ((nfields+1) * regwidth),
      sizeof(*fieldreg));
    if (!fieldreg) goto ALLOCERR;
    curfldreg = fieldreg;
    savefldnum = 0;
    nextfldnum = 0;
    curfldmask = 1;
    savefldmask = 1;
    nextfldmask = 1;

    idmatches = (char *) calloc((size_t) exe->nlogicids,sizeof(*idmatches));
    if (!idmatches) goto ALLOCERR;

    if (*matches) free(*matches);
    *matches = (int *) calloc((size_t) exe->nqueries, sizeof(*(*matches)));
    if (!*matches) goto ALLOCERR;
    *nmatches = 0;
    inphrase = 0;

    if (nproximity) {
      prf_search_freeprox(proxmatchlist,nproximity);
      free(proxmatchlist);
      proxmatchlist = (struct prf_s_proxmatchlist *) NULL;
      nproximity = 0;
    }

    if (exe->nproximity) {
      proxmatchlist = (struct prf_s_proxmatchlist *)
	calloc((size_t) exe->nproximity,sizeof(*proxmatchlist));
      if (!proxmatchlist) goto ALLOCERR;
      nproximity = exe->nproximity;
      for (i=0; i < nproximity; ++i) {
	proxmatchlist[i].n_1 = 0;
	proxmatchlist[i].first_1 = (struct prf_s_proxmatch *) NULL;
	proxmatchlist[i].last_1 = (struct prf_s_proxmatch *) NULL;
	proxmatchlist[i].n_2 = 0;
	proxmatchlist[i].first_2 = (struct prf_s_proxmatch *) NULL;
	proxmatchlist[i].last_2 = (struct prf_s_proxmatch *) NULL;
      }
    }
  }

  if (searchstatus & 4) {
    memset(void_caste fieldreg, 0, 
      (size_t) (regwidth * nfields * sizeof(*fieldreg)));
    savefldnum = 0;
    nextfldnum = 0;
    savefldmask = 1;
    nextfldmask = 1;
    memset(idmatches, 0, (size_t) (exe->nlogicids * sizeof(*idmatches)));
    if (phraseregwidth)
      memset(void_caste phraseactive, 0,
	(size_t) (sizeof(*phraseactive) * phraseregwidth * regwidth));
    *nmatches = 0;
    memset(void_caste termmatch,0,(size_t) (nterms * sizeof(*termmatch)));
    startwordnum = 0;
    if (nproximity) prf_search_freeprox(proxmatchlist,nproximity);
  }

  begtext = text;
  endtext = text + textlen;

  /* Initialize register variables */
  xlate = exe->tx_xlate;

  /* Main loop of term matching, executes once for each table */
  for (i=0;i < regwidth; ++i) {
    curtable = *(table+i);
    text = begtext;
    curfldreg = fieldreg + (savefldnum * regwidth) + i;
    curphrasemask = exe->phrasemask[i];
    curfldmask = savefldmask;
    curphrasemask = exe->phrasemask[i];
    notphrasemask = ~curphrasemask;
    phraseproxmask = exe->phraseproxmask[i];
    notphraseproxmask = ~phraseproxmask;
    prox1mask = exe->prox1mask[i];
    prox2mask = exe->prox2mask[i];
    notproxmask = ~prox1mask & ~prox2mask;
    curwordnum = startwordnum;

    if (phraseregwidth) {
      for (j = 0, inphrase = 0, preg = phraseactive + (i * phraseregwidth);
	  j < phraseregwidth; ++j) {
	*(pmask+j) = *(preg);
	*(altpmask+j) = 0;
	inphrase |= (*(preg++) != 0);
      }
    }

    /* Match terms in current match table against words in buffer */
    for (; text < endtext; ) {
      if ((c = *(xlate + *(text++))) > 0) {
	curwordnum += 1;

	if (inphrase) {
	  /* Phrase term matching is active, check latest word in text
	     against enabled words in phrase match table */

	  inphrase = 0;
	  begword = --text;

	  /* Run through phrase tables */
	  for (j=0; j < phraseregwidth; ++j) {
	    if (*(pmask+j)) {
	      /* Next table in phrase tables has enabled terms, check them */
	      row = *(phrasetable+j);
	      text = begword;
	      c = *(xlate + *(text++));
	      /* Run through characters in word */
	      for (reg = *(pmask+j); reg && c > 0; row += ncols) {
		reg &= *(row + c);
		c = *(xlate + *(text++));
	      }

	      /* Find any words which matched */
	      wordmatched = (reg & *(peottable + *(row - 1)));
	      if (wordmatched) {
		phraseprox1mask = exe->phraseprox1mask[j];
		phraseprox2mask = exe->phraseprox2mask[j];
		notinproximity = ~phraseprox1mask & ~phraseprox2mask;
		for (termbase = j << FIT_LONG_EXP, k = 0, mask = 1;
		    wordmatched && mask; ++k, mask <<= 1) {
		  if (wordmatched & mask) {
		    termidx = termbase + k;
		    if ((*(phraseterm+termidx))->nextword < 0) {
		      /* Matched on final word of phrase, set match bit in
			 current field collector register */
		      if (mask & notinproximity) {
			*curfldreg |= (*(phraseterm+termidx))->termmask;
		      }
		      else if (mask & phraseprox1mask) {
			termidx = (*(phraseterm+termidx))->termnum;
			listidx = exe->proxterm1map[termidx];
			prf_search_addprox(proxmatchlist+listidx,
			  curwordnum - exe->term[termidx]->nwords+1,
			  curwordnum,1);
		      }
		      else if (mask & phraseprox2mask) {
			termidx = (*(phraseterm+termidx))->termnum;
			listidx = exe->proxterm2map[termidx];
			prf_search_addprox(proxmatchlist+listidx,
			  curwordnum - exe->term[termidx]->nwords+1,
			  curwordnum,0);
		      }
		    }

		    else {
		      /* Enable phrase searching for next word in phrase term */
		      inphrase = 1;
		      termidx = (*(phraseterm+termidx))->nextword;
		      *(altpmask + (termidx >> FIT_LONG_EXP)) |=
			  1L << (termidx % FIT_BITS_IN_LONG);
		    }
		  }
		  wordmatched &= ~mask;
		}
	      }
	      /* Clean out phrase mask for next time it gets used to collect
		 matches */
	      *(pmask+j) = 0;
	    }
	  }
	  /* Swap current prhase enable mask over to the just collected mask */
	  temppmask = altpmask;
	  altpmask = pmask;
	  pmask = temppmask;
	  text = begword + 1;
	  c = *(xlate + *begword);
	}

	/* Do normmal term matching */
	for (row = curtable, reg=~(unsigned long) 0L; reg && c > 0; row +=ncols) {
	  reg &= *(row + c);
	  c = *(xlate + *(text++));
	}
	while (c > 0)
	  c = *(xlate + *(text++));

	if (reg) {
	  if (reg & *(eottable + *(row - 1))) {
	    wordmatched = (reg & *(eottable + *(row - 1)));
	    if (wordmatched & phraseproxmask) {
	      if (wordmatched & curphrasemask) {
		startphrase = (wordmatched & curphrasemask);
		/* For each enabled phrase in startphrase */
		for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
		  startphrase && mask; ++j, mask <<= 1) {
		  if (startphrase & mask) {
		    startphrase &= ~mask;
		    termidx = termbase + j;
		    if ((*(*(exe->term+termidx))->fieldmap) & curfldmask) {
		      inphrase = 1;
		      termidx = (*(exe->term+termidx))->nextword;
		      *(pmask + termidx/FIT_BITS_IN_LONG) |=
		      1L << (termidx % FIT_BITS_IN_LONG);
		    }
		  }
		}
	      }
	      if (wordmatched & prox1mask & notphrasemask) {
		startprox = wordmatched & prox1mask & notphrasemask;
		for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
		    startprox && mask; ++j, mask <<= 1)
		  if (startprox & mask) {
		    startprox &= ~mask;
		    if (*exe->term[termbase+j]->fieldmap & curfldmask) {
		      listidx = exe->proxterm1map[termbase+j];
		      prf_search_addprox(proxmatchlist+listidx,
			  curwordnum,curwordnum,1);
		    }
		  }
	      }
	      if (wordmatched & prox2mask & notphrasemask) {
		startprox = wordmatched & prox2mask & notphrasemask;
		for (termbase = i << FIT_LONG_EXP, j = 0, mask = 1;
		      startprox && mask; ++j, mask <<= 1)
		  if (startprox & mask) {
		    startprox &= ~mask;
		    if (*exe->term[termbase+j]->fieldmap & curfldmask) {
		      listidx = exe->proxterm2map[termbase+j];
		      prf_search_addprox(proxmatchlist+listidx,
			  curwordnum,curwordnum,0);
		    }
		  }
	      }
	    }
	    *curfldreg |= (wordmatched & notphraseproxmask);
	  }
	}
      }

      if (c < -1)  {
	/* Must be a field marker */
	nextfldnum = max(0, min(nfields-1, ((int) *text)));
	nextfldmask = (long) (1L << (int) nextfldnum);
	curfldreg = fieldreg + (nextfldnum * regwidth) + i;
	curfldmask = nextfldmask;
	++text;
      }
    }

    /* Update phrase status matrix */
    if (phraseregwidth)
      for (j = 0, preg = phraseactive + (i * phraseregwidth);
	  j < phraseregwidth; ++j)
	*(preg++) = *(pmask+j);
  }

  startwordnum = curwordnum;
  savefldnum = nextfldnum;
  savefldmask = nextfldmask;

  /* Return if not last buffer */
  if (!(searchstatus & 2)) return(1);

  /* Set field matched masks for each term */
  for (i=0; i < nterms; i += FIT_BITS_IN_LONG)
    for (j=i, mask = 1, startfldreg = fieldreg + i/FIT_BITS_IN_LONG;
	j < nterms && mask; mask <<= 1, ++j)
      for (curfldreg = startfldreg, k = 0; k < nfields;
	  curfldreg += regwidth, ++k)
	if (*curfldreg & mask)
	  termmatch[j] |= 1L << k;

  /* Set ID matched flags for all term ids which matched */
  for (i=0; i < nterms; ++i)
    if (termmatch[i])
      if (!exe->termtype[i]) {
	term = exe->term[i];
	for (j = 0, fieldmap = term->fieldmap; j < term->nid; ++j)
	  if (termmatch[i] & *fieldmap++)
	    idmatches[term->logicid[j]] = 1;
      }

  for (i=0; i < nproximity; ++i)
    prf_search_resolveprox(exe,i,proxmatchlist+i, idmatches);

  /* Evaluate logic equations to determine query matches */
  for (matched = *matches, i = 0, logic = exe->logic,
       endlogic = exe->logic + exe->nqueries,
       logiclen = exe->logiclen; logic < endlogic; ++logic, ++logiclen, ++i) {
    for (top = stack-1, *stack = 0, token = *logic,
      endtoken = token + *logiclen; token < endtoken; ++token)
      if (*token >= 0) {
	*(++top) = *(idmatches + *token);
      }
      else
	switch (*token) {
          case QC_OR : {
            *(top-1) |= *(top--);
            break;
          }
          case QC_AND : {
            *(top-1) &= *(top--);
            break;
          }
          case QC_NOT : {
            *top = !(*top);
            break;
          }
          case QC_COMMA : {
            *(top-1) += *(top--);
            break;
          }
	  case QC_THRESHHOLD : {
	    *top = (*top >= *(++token));
	    break;
	  }
	}

    if (*stack) {
      *(matched++) = i;
      *nmatches += 1;
    }
  }

  if (proxmatchlist) prf_search_freeprox(proxmatchlist,exe->nproximity);
  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
