/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_addphrse					addphrse.c

Function : Add word to phrase tables and initialize phrase word descriptor

Author : A. Fregly

Abstract : This routine is used to add the 2nd through nth words for a phrase
	term to the phrase tables.  A separate call to this routine is
	required for each word to be added to the phrase tables.

Calling Sequence : stat = prf_addphrse(struct prf_s_exe *exe, char *val,
    int vlen, int *idx);

    exe		Profile executable.
    val		Value to be added to tables.
    vlen	Length of val.
    idx		Returned index assigned to the word.

Notes :

Change log :
000	18-NOV-90  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_addphrse(struct prf_s_exe *exe, char delims[QC_MAXDELIM],
  char *val, int vlen, int *idx)
#else
int prf_addphrse(exe, delims, val, vlen, idx)
  struct prf_s_exe *exe;
  char delims[QC_MAXDELIM], *val;
  int vlen, *idx;
#endif
{

  int newwidth,i,j,k,tblidx;
  FIT_REG *(*newregisters), *neweotflags, *regptr, *newphraseproxmask;
  struct prf_s_phraseterm *(*newterms), *termptr;

  if (exe->phraseregwidth * FIT_BITS_IN_LONG <= exe->nphraseterms) {
    /* Current block of preallocated terms is full, expand */
    newwidth = exe->phraseregwidth + 1;

    /* Expand match table registers */
    /* Allocate new match table and phrase mask and proximity mask */
    newregisters = (FIT_REG *(*)) calloc((size_t) newwidth,
      sizeof(*newregisters));
    if (!newregisters) goto ALLOCERR;

    for (i=0; i < newwidth; ++i)
      if (i < exe->phraseregwidth)
	*(newregisters+i) = *(exe->phrasetbl.registers+i);
      else
	*(newregisters+i) = (FIT_REG *) NULL;

    *(newregisters+exe->phraseregwidth) =
	(FIT_REG *) calloc((size_t) ((exe->nrows + 1) * exe->ncols),
	sizeof(*(*newregisters)));
    if (!*(newregisters+exe->phraseregwidth)) goto ALLOCERR;

    /* Free old match table pointers */
    if (exe->phraseregwidth) free(exe->phrasetbl.registers);

    /* Put new tables into profile executable */
    exe->phrasetbl.registers = newregisters;

    /* Set eot table offset in match table */
    for (tblidx = 0; tblidx < newwidth; ++tblidx)
      for (i=0, regptr = *(newregisters+tblidx) + exe->ncols - 1;
	  i < exe->nrows + 1; regptr += exe->ncols, ++i)
	*regptr = (i * newwidth) + tblidx;

    /* Expand match table EOT flags */
      /* Allocate storage for new EOT table */
    neweotflags = 
      (FIT_REG *) calloc((size_t) (newwidth * (exe->nrows + 1)),
      sizeof(*neweotflags));
    if (!neweotflags) goto ALLOCERR;
    if (newwidth > 1) {
      /* Copy old EOT table into new table */
      for (i = 0; i < exe->nrows + 1; ++i)
	for (j = i * exe->phraseregwidth, k = i * newwidth;
	    j < (i+1) * exe->phraseregwidth;)
	  *(neweotflags + k++) = *(exe->phrasetbl.eotflag + j++);

      /* Free up storage used by old table */
      free(exe->phrasetbl.eotflag);
    }

    /* Insert new table into profile executable */
    exe->phrasetbl.eotflag = neweotflags;

    /* Expand phrase proximity term masks */
    newphraseproxmask = 
      (FIT_REG *) calloc((size_t) newwidth, sizeof(*newphraseproxmask));
    if (!newphraseproxmask) goto ALLOCERR;
    if (newwidth > 1) {
      for (i=0; i < newwidth - 1; ++i)
        newphraseproxmask[i] = exe->phraseprox1mask[i];
      free(exe->phraseprox1mask);
    }
    exe->phraseprox1mask = newphraseproxmask;

    newphraseproxmask = 
      (FIT_REG *) calloc((size_t) newwidth, sizeof(*newphraseproxmask));
    if (!newphraseproxmask) goto ALLOCERR;
    if (newwidth > 1) {
      for (i=0; i < newwidth - 1; ++i)
        newphraseproxmask[i] = exe->phraseprox2mask[i];
      free(exe->phraseprox2mask);
    }
    exe->phraseprox2mask = newphraseproxmask;

    /* Expand term list */
    /* Allocate storage for new term list */
    newterms = (struct prf_s_phraseterm *(*)) 
      calloc((size_t) (newwidth * FIT_BITS_IN_LONG), sizeof(*newterms));
    if (!newterms) goto ALLOCERR;

    if (newwidth > 1) {
      /* Copy old term list into new term list */
      for (i=0; i < exe->nphraseterms; ++i)
	*(newterms+i) = *(exe->phraseterm + i);

      /* Free storage used by the old list */
      free(exe->phraseterm);
    }

    /* Insert new term list into profile executable */
    exe->phraseterm = newterms;

    /* Remember the new match table register width (in long words) */
    exe->phraseregwidth = newwidth;
  }

  /* Initialize a descriptor node, linking it to phrase word list */
  /* Initialize node for word */
  *idx = exe->nphraseterms++;
  tblidx = *idx / FIT_BITS_IN_LONG;
  termptr = (struct prf_s_phraseterm *) calloc((size_t) 1,sizeof(*termptr));
  if (!termptr) goto ALLOCERR;
  *(exe->phraseterm + *idx) = termptr;
  termptr->len = vlen;
  termptr->val = (char *) malloc((size_t) (termptr->len+1));
  if (termptr->val == NULL) goto ALLOCERR;
  strncpy(termptr->val,val,(size_t) vlen);
  *(termptr->val+vlen) = 0;
  termptr->nextword = -1;
  termptr->termmask = 0;

  return prf_filltbl(*(exe->phrasetbl.registers+tblidx), 
      exe->phrasetbl.eotflag, exe->nrows, exe->ncols, exe->phraseregwidth,
      exe->term_xlate, delims, exe->tx_xlate, *idx, val, vlen);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
