/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prox.c

Function : Contains routines used in resolving proximity relationships.

Author : A. Fregly

Abstract : This file contains functions used by prf_search in keeping track
	of possible proximity matching terms, and in resolving those possible
	terms.

Calling Sequence : 

Notes : 

Change log :
000	23-APR-91  AMF	Header comment.
001	06-MAY-91	Changed freeprox "exe" parameter to "nproximity".
002	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <prox.h>
#include <fit_stat.h>

#if FIT_ANSI
void prf_matchinfo_addprox(register struct prf_s_proxmatchlist *proxmatchlist,
  long swordnum, long ewordnum, int preceeding, prf_s_match *matchinfo)
#else
void prf_search_addprox(proxmatchlist, swordnum, ewordnum, preceeding)
#if FIT_USEREGISTERS
  register struct prf_s_proxmatchlist *proxmatchlist;
#else
  struct prf_s_proxmatchlist *proxmatchlist;
#endif
  long swordnum, ewordnum;
  int preceeding;
#endif
{
#if FIT_USEREGISTERS
  register struct prf_s_proxmatch *match;
#else
  static struct prf_s_proxmatch *match;
#endif

  match = (struct prf_s_proxmatch *) malloc(sizeof(*match));
  if (!match) return;
  match->swordnum = swordnum;
  match->ewordnum = ewordnum;
  match->match = matchinfo;
  match->next = (struct prf_s_proxmatch *) NULL;

  if (preceeding) {
    if (proxmatchlist->n_1) proxmatchlist->last_1->next = match;
    proxmatchlist->last_1 = match;
    if (!proxmatchlist->n_1) proxmatchlist->first_1 = match;
    proxmatchlist->n_1 += 1;
  }
  else {
    if (proxmatchlist->n_2) proxmatchlist->last_2->next = match;
    proxmatchlist->last_2 = match;
    if (!proxmatchlist->n_2) proxmatchlist->first_2 = match;
    proxmatchlist->n_2 += 1;
  }
  return;
}

#if FIT_ANSI
void prf_matchinfo_resolveprox(prf_s_exe *exe, int idx,
  register prf_s_proxmatchlist *matchlist, prf_s_matchinfo *matchinfo)
#else
void prf_search_resolveprox(exe, idx, matchlist, matchinfo)
   prf_s_exe *exe;
  int idx;
#if FIT_USEREGISTERS
  register prf_s_proxmatchlist *matchlist;
  register prf_s_matchinfo *matchinfo;
#else
  prf_s_proxmatchlist *matchlist;
  prf_s_matchinfo *matchinfo;
#endif
#endif
{

#if FIT_USEREGISTERS
  register long distance;
  register int matched;
  register struct prf_s_proxmatch *preceeding, *follower;
  register int newmatch;
#else
  static long distance;
  static int match;
  static struct prf_s_proxmatch *preceeding, *follower;
  static int newmatch;
#endif

  distance = exe->proximity[idx].distance;
  match = 0;
  if (exe->proximity[idx].relation) {
    for (preceeding = matchlist->first_1; preceeding;
        preceeding = preceeding->next)
      for (follower = matchlist->first_2, newmatch = 0; follower;
          follower = follower->next, newmatch = 0) {
	if (preceeding->swordnum < follower->swordnum) {
	  newmatch |= (follower->swordnum - preceeding->ewordnum <= distance);
	  match |= newmatch;
	}
	if (newmatch) {
	  preceeding->match->matchtype = PRF_MATCHED;
	  follower->match->matchtype = PRF_MATCHED;
        }
      }
  }

  else {
    for (preceeding = matchlist->first_1; preceeding;
      preceeding = preceeding->next)
      for (follower = matchlist->first_2, newmatch = 0; follower;
          follower = follower->next, newmatch = 0) {
	if (preceeding->swordnum < follower->swordnum) {
	  newmatch = (follower->swordnum - preceeding->ewordnum <= distance);
	  match |= newmatch;
	}
	else if (follower->swordnum < preceeding->swordnum) {
	  newmatch = (preceeding->swordnum - follower->ewordnum <= distance);
          match |= newmatch;
	}
	if (newmatch) {
	  preceeding->match->matchtype = PRF_MATCHED;
	  follower->match->matchtype = PRF_MATCHED;
        }
      }
  }

  if (match) {
    matchinfo->idmatch[exe->proximity[idx].id] = ~0L;
    matchinfo->idmap[exe->proximity[idx].id] = -idx - 1;
  }
}