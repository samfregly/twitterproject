/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_findword					findword.c

Function : Finds next search word in string

Author : A. Fregly

Abstract : This routine finds the offset and length of the next word in
  a string.  In parsing the word, it takes into account the translation
  tables which are passed in as parameters.  A word is considered as a
  string of non-whitespace characters, where a whitespace character is
  one whose text translation value is <= 0, and is not a wild card or
  quoting character.

Calling Sequence :  int stat = prf_findword(char *val, int vlen, int *idx,
    unsigned int *term_xlate, int *tx_xlate, int *off, int *wordlen);

    val		String being parsed into words.
    vlen		Length of val.
    idx		Current search index into val.  idx is updated by this
		routine.  idx should be set to 0 prior to the initial call.
    term_xlate	Term character translation table.  Wild cards are any
		characters with a value greater than 255.
    tx_xlate	Text translation table.  Searchable characters have values
		greater than 0, 0 is the "ignore" character, -1 is
		whitespace, and less than -1 is used for field delimitiers.
    off		Returned offset of word which was found.
    wordlen	Returned length of word which was found.
    stat	Returned status, 0 if no word was found, 1 otherwise.

Notes :

Change log :
00	16-NOV-90  AMF	Created.
01	1-JUN-91  AMF	Used unsigned char for table indexes so characters with
			bit 8 set don't screw up.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>

#define isspace(c) (tx_xlate[(unsigned char) (c)] <= 0 && \
    !term_xlate[(unsigned char) (c)])

#define isquotedspace(c) (tx_xlate[(unsigned char) (c)] <= 0)

#if FIT_ANSI
int prf_findword(char *val, int vlen, int *idx, FIT_UWORD term_xlate[256],
  char delims[QC_MAXDELIM], FIT_WORD *tx_xlate, int *off, int *wordlen)
#else
int prf_findword(val, vlen, idx, term_xlate, delims, tx_xlate, off, wordlen)
  char *val;
  int vlen, *idx;
  FIT_UWORD term_xlate[256];
  char delims[QC_MAXDELIM];
  FIT_WORD *tx_xlate;
  int *off, *wordlen;
#endif
{

  int stat,whitespace;
  unsigned char *v, *dlm;

  v = (unsigned char *) val;
  dlm = (unsigned char *) delims;

  /* Skip over any leading white-space */
  stat = 0;
  if (*idx < vlen) {
    v = v + *idx;
    for (whitespace = 1; whitespace && *idx < vlen;) {
      if (dlm[QC_OFF_QUOTE] && *v == dlm[QC_OFF_QUOTE]) {
	++(*idx);
	++v;
	if (*idx < vlen)
	  whitespace = isquotedspace(*v);
      }
      else
	whitespace = isspace(*v);

      if (whitespace && *idx < vlen) {
	++(*idx);
	++v;
      }
    }
  }

  /* Mark boundaries of word */
  if (*idx < vlen) {
    *off = *idx;
    for (*wordlen = 0;!whitespace && *idx < vlen; ++v, ++(*idx)) {
      if (dlm[QC_OFF_QUOTE] && *v == dlm[QC_OFF_QUOTE]) {
	++(*idx);
	++v;
	if (*idx < vlen)
	  whitespace = isquotedspace(*v);
      }
      else
	whitespace = isspace(*v);

      *wordlen += !whitespace;
    }

    stat = 1;
  }

  return(stat);
}
