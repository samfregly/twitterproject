/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*****************************************************************************
Module Name : prf_offsets                                       offsets.c

Function : Finds offsets and lengths of terms in document.

Author : A. Fregly

Abstract : This routine use the term matching algorithm of the TIPS search
  engine to find the offsets and lengths of terms in the document buffer.

Calling Sequence : stat = prf_offsets(int searchstatus, unsigned int opts,
  char *exe, unsigned char *text, unsigned int textlen, int maxoffset,
  int *noffsets, unsigned long *offsets, unsigned char *lengths);

  searchstatus  Bit map defining where program is in context of current
                current document.
		0 - On, then initialize allocate and initialize internal data
		    structures, then search first buffer of document.
		1 - reserved.
		2 - On, then do not allocate internal data structures, but
		    initialize them and search initial buffer of document.
		3 - Deallocate internal data structures and matches, and return
		    without searching.
  opts		Search options, reserved.
  exe		Profile executable.
  text		Text buffer to be searched.
  textlen	Length of text buffer.
  maxoffset	Maximum number of offsets/lengths which may be returned.
  noffsets	Number of offsets/lengths returned
  offsets	Offsets of matching terms.
  lengths	Lengths of matching terms.

Notes :

Change log :
000	24-SEP-90  AMF	Created.
001	07-FEB-92  AMF	Typecasts to remove compiler warnings on SUN.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_MSC || FIT_BSD
#include <string.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#define MAXPHRASEWORDS 32

#if FIT_ANSI
int prf_offsets(int searchstatus, unsigned int opts, char *xe,
  unsigned char *text, unsigned int textlen, int maxoffset, int *noffsets,
  unsigned long *offsets, unsigned *lengths)
#else
int prf_offsets(searchstatus, opts, xe, text, textlen, maxoffset, noffsets,
    offsets, lengths)
  int searchstatus;
  unsigned int opts;
  char *xe;
  unsigned char *text;
  unsigned int textlen;
  int maxoffset, *noffsets;
  unsigned long *offsets;
  unsigned *lengths;
#endif
{

#if FIT_USEREGISTERS
  register FIT_REG reg;
  register FIT_WORD *xlate;
  register FIT_WORD c;
  register FIT_REG *row;
  register FIT_REG *curtable;
#else
  static FIT_REG reg;
  static FIT_WORD *xlate;
  static FIT_WORD c;
  static FIT_REG *row;
  static FIT_REG *curtable;
#endif

  static struct prf_s_exe *exe;
  static unsigned char *begword;
  static FIT_REG *(*table), notphrasemask, *pmask, *altpmask, mask;
  static FIT_REG inphrase, startphrase, wordmatched;
  static FIT_REG *temppmask;
  static int i, j, k, regwidth, ncols;
  static int nfields;
  static unsigned char *endtext, *textbeg;
  static FIT_REG *fieldmap, fieldmask, *eottable, matched;
  static FIT_REG termmask, starttext;
  static int phraseregwidth;
  static FIT_REG *phrasemask, *(*phrasetable), *pptr;
  static FIT_REG *peottable,curphrasemask,*phraseactive;
  static unsigned int termbase, termidx;
  static struct prf_s_phraseterm *(*phraseterm);
  static unsigned long wordoffsets[MAXPHRASEWORDS],vlen,soff,eoff;
  static int wordidx,sidx,ii,jj,termnum,skip,overlap;

  /* Deallocate data structures */
  if (searchstatus & 8) {
    free(fieldmap);
    if (phraseregwidth) {
      free(pmask);
      free(altpmask);
      free(phraseactive);
    }
    return(1);
  }

  if (searchstatus & 1) {
  /* Initialize for search of new document */
    exe = (struct prf_s_exe *) xe;
    ncols = exe->ncols;
    regwidth = exe->regwidth;
    phraseregwidth = exe->phraseregwidth;
    eottable = exe->table.eotflag;
    peottable = exe->phrasetbl.eotflag;
    nfields = exe->nfields;

    table = exe->table.registers;
    phrasetable = exe->phrasetbl.registers;
    phrasemask = exe->phrasemask;
    phraseterm = exe->phraseterm;

    if (phraseregwidth) {
      pmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	sizeof(*pmask));
      if (!pmask) goto ALLOCERR;
      altpmask = (FIT_REG *) calloc((size_t) phraseregwidth,
	sizeof(*altpmask));
      if (!altpmask) goto ALLOCERR;
      phraseactive = 
	(FIT_REG *) calloc((size_t) (phraseregwidth * regwidth),
	sizeof(*phraseactive));
      if (!phraseactive) goto ALLOCERR;
    }

    fieldmap = (FIT_REG *) calloc((size_t) exe->nterms,
      sizeof(*fieldmap));
    for (i = 0; i < exe->nterms; ++i)
      for (j = 0; j < (*(exe->term+i))->nid; ++j)
	*(fieldmap+i) |= *((*(exe->term+i))->fieldmap+j);
  }

  if (searchstatus & (1 | 4)) {
    inphrase = 0;
    *noffsets = 0;
    fieldmask = 1;
    starttext = 0;
    for (i=0; i < MAXPHRASEWORDS; ++i)
      wordoffsets[i] = 0;
    wordidx = 0;
    if (phraseregwidth)
      memset(phraseactive, 0,
	(size_t) (sizeof(*phraseactive) * phraseregwidth * regwidth));
  }

  if (*noffsets >= maxoffset) return(1);

  textbeg = text;
  endtext = text + textlen;
  xlate = exe->tx_xlate;

  for (k=0; k < regwidth; ++k) {
    curtable = *(table+k);
    text = textbeg;
    curphrasemask = *(exe->phrasemask+k);
    notphrasemask = ~curphrasemask;

    if (phraseregwidth) {
      for (j = 0, inphrase = 0, pptr= phraseactive + (k * phraseregwidth);
         j < phraseregwidth; ++j) {
        *(pmask+j) = *(pptr);
        *(altpmask+j) = 0;
        inphrase |= (*(pptr++) != 0);
      }
    }

    for (; text < endtext;) {

      if ((c = *(xlate + *(text++))) > 0) {

	wordidx = (wordidx+1) % MAXPHRASEWORDS;
        wordoffsets[wordidx] = text - textbeg - 1;
	wordoffsets[wordidx] += starttext;

        if (inphrase) {
          /* Phrase term matching is active, check latest word in text
             against enabled words in phrase match table */

	  inphrase = 0;
          begword = --text;

          /* Run through phrase tables */
          for (j=0; j < phraseregwidth; ++j) {
            if (*(pmask+j)) {
              /* Next table in phrase tables has enabled terms, check them */
	      row = *(phrasetable+j);
	      text = begword;
	      c = *(xlate + *(text++));
	      /* Run through characters in word */
	      for (reg = *(pmask+j); reg && c > 0; row += ncols) {
		reg &= *(row + c);
		c = *(xlate + *(text++));
	      }

              /* Find any words which matched */
	      wordmatched = (reg & *(peottable + *(row - 1)));
              if (wordmatched) {
		for (termbase = j << FIT_LONG_EXP, i = 0, mask = 1;
		    wordmatched && i < FIT_BITS_IN_LONG; ++i, mask <<= 1L) {
                  if (wordmatched & mask) {
                    termidx = termbase + i;
                    if ((*(phraseterm+termidx))->nextword < 0) {
                      /* Matched on final word of phrase, add to offset and
			 length arrays */
                      if (*noffsets < maxoffset) {
			/* Determine offset of start of phrase */
                        termnum = (*(phraseterm+termidx))->termnum;
                        sidx = wordidx - (*(exe->term+termnum))->nwords + 1;
                        if (sidx < 0) sidx = MAXPHRASEWORDS + sidx;

                        /* See if it needs to be inserted in front of offsets
			   which have already been put into the arrays */
			for (ii = *noffsets-1; ii > -1 &&
			    *(offsets+ii) > wordoffsets[sidx]; --ii)
			  ;
                        for (jj = *noffsets; jj > ii + 1; --jj) {
                          *(offsets+jj) = *(offsets+jj-1);
                          *(lengths+jj) = *(lengths+jj-1);
                        }

                        *(offsets + jj) = wordoffsets[sidx];
			vlen = starttext + (text - textbeg) - 1;
			vlen -= wordoffsets[sidx];
                        *(lengths + jj) = (unsigned) vlen;

			*noffsets += 1;
                      }
                      else
                        return(1);
                    }

                    else {
                      /* Enable phrase searching for next word in phrase term */
                      inphrase = 1;
                      termidx = (*(phraseterm+termidx))->nextword;
		      *(altpmask + (termidx >> FIT_LONG_EXP)) |=
			  1L << (termidx % FIT_BITS_IN_LONG);
		    }
		  }
		  wordmatched &= ~mask;
		}
	      }
	      /* Clean out phrase mask for next time it gets used to collect
		 matches */
	      *(pmask+j) = 0;
	    }
	  }
	  /* Swap current phrase enable mask over to the just collected mask */
	  temppmask = altpmask;
	  altpmask = pmask;
	  pmask = temppmask;
	  text = begword + 1;
	  c = *(xlate + *begword);
	}

	/* Primary term match loop */
	for (row = curtable, reg=~(unsigned long) 0L; reg && c > 0; row +=ncols) {
	  reg &= *(row + c);
	  c = *(xlate + *(text++));
	}
	while (c > 0)
	  c = *(xlate + *(text++));

	reg &= *(eottable + *(row-1));
	if (reg) {
	  if (reg & *(eottable + *(row - 1))) {
	    wordmatched = (reg & *(eottable + *(row - 1)));
            if (wordmatched) {
              if (wordmatched & curphrasemask) {
                startphrase = (wordmatched & curphrasemask);
                /* For each enabled phrase in startphrase */
		for (termbase = k << FIT_LONG_EXP, j = 0, mask = 1;
		  startphrase && j < FIT_BITS_IN_LONG; ++j, mask <<= 1L) {
                  if (startphrase & mask) {
                    startphrase &= ~mask;
                    termidx = termbase + j;
                    if ((*(*(exe->term+termidx))->fieldmap) & fieldmask) {
                      inphrase = 1;
                      termidx = (*(exe->term+termidx))->nextword;
		      *(pmask + termidx/FIT_BITS_IN_LONG) |=
			1L << (termidx % FIT_BITS_IN_LONG);
                    }
                  }
		}
              }

              if (wordmatched & notphrasemask) {
		for (i = 0, matched = 0, termmask = 1, j = k * FIT_BITS_IN_LONG;
		    !matched && reg && i < FIT_BITS_IN_LONG; ++i,
                    termmask <<= 1L)
	          if (reg & termmask) {
	            matched = (*(fieldmap+j+i) & fieldmask);
	            reg &= ~termmask;
                  }

                if (matched)
                  if (*noffsets < maxoffset) {
                    *(offsets + *noffsets) = wordoffsets[wordidx];
                    *(lengths + (*noffsets)++) =
		      (unsigned) ((starttext + (text - textbeg) -
			wordoffsets[wordidx]) - 1);
		  }
                  else
                    return(1);
              }
            }
	  }
        }
      }

      if (c < -1) {
        /* Must be a field marker */
	fieldmask = (FIT_REG) (1L << min(FIT_BITS_IN_LONG-1, ((int) *text)));
	++text;
      }
    }

    /* Update phrase status matrix */
    if (phraseregwidth)
      for (j = 0, pptr= phraseactive + (k * phraseregwidth);
	  j < phraseregwidth; ++j)
	*(pptr++) = *(pmask+j);
  }

  starttext = starttext + textlen;

  /* Compress any overlapping offsets */
  for (i = 0; i < *noffsets; ++i) {
    soff = *(offsets+i);
    eoff = soff + *(lengths+i);
    for (j=i+1, overlap = j < *noffsets, skip = 0; overlap && j < *noffsets;
	++j) {
      overlap = *(offsets+j) <= eoff;
      if (overlap) {
	skip += 1;
	//eoff = max(eoff,*(offsets+j) + *(lengths+j)-1);
	eoff = *(offsets+j) + *(lengths+j)-1;
      }
    }
    if (skip) {
      for (j = i+1, k = i+skip+1; k < *noffsets; ++j, ++k) {
	*(offsets+j) = *(offsets+k);
	*(lengths+j) = *(lengths+k);
      }
      *noffsets -= skip;
    }

    *(lengths+i) = (unsigned) eoff - soff;
  }
  return(1);

ALLOCERR:
  fit_status = FIT_FATAL_MEM;
  return(0);
}
