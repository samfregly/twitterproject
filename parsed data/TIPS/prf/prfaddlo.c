/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prf_addlogic					addlogic.c

Function : Adds logic equation to list of logic equations.

Author : A. Fregly

Abstract :

Calling Sequence : stat = prf_addlogic(int *logic, int nlogic, int slot,
  int *(*logiclist))

  logic		Logic equation
  nlogic	Number of elements in logic equation
  slot		Slot in which to put logic equation
  logiclist	List of logic equations to which logic equation is added


Notes :

Change log :
000	14-AUG-90  AMF	Created
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <fit_stat.h>

#if FIT_ANSI
int prf_addlogic(int logic[], int nlogic, int slot, int *(*logiclist))
#else
int prf_addlogic(logic, nlogic, slot, logiclist)
  int *logic, nlogic, slot, *(*logiclist);
#endif
{

  int i;

  *(logiclist+slot) = (int *) calloc((size_t) nlogic, sizeof(*logic));
  if (!*(logiclist+slot)) {
    fit_status = FIT_FATAL_MEM;
    return(0);
  }

  for (i=0; i < nlogic; ++i)
    *(*(logiclist+slot)+i) = *(logic+i);

  return(1);
}
