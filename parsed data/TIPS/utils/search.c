/* This example implements a search command which takes as its first
parameter the name of a file containing the source text for a query to 
use in a search, and with following parameters listing the names of 
files to be searched. The results of the search will be written to a 
TIPS compatible result file which takes the same name as the query 
source file, but with an extension of ".hit". The results are appended 
to the result file if it already exists, otherwise a new result file is 
created */ 

/*
Change log:
000     15-DEC-92  AMF  Compile under COHERENT.
*/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <fcntl.h> 
#include <errno.h>
#include <fitsydef.h>
#if FIT_MSC
#include <direct.h>
#endif 
#include <prf.h> 
#include <qc.h> 
#include <drct.h> 
#include <tips.h> 
#define QRYTEXTMAX 4096 
#define BUFFERSIZE 30720 
 
#if FIT_DOS
#define READ "rb"
#define WRITE "wb"
#else
#define READ "r"
#define WRITE "w"
#endif

#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif

static int nbytes, nerrs, searchstat, ineof;
static FIT_WORD tx_xlate[256];
static int textlen, nmatches, *matches, docdescriptor_l, nsearched, remnant;
static int totalmatched;
static long doclen, textsearched, searchrate, secs;
static FILE *in;
static struct drct_s_file *hithandle;
static char srcqry[QRYTEXTMAX+1];
static char defpath[FIT_FULLFSPECLEN+1], hitspec[FIT_FULLFSPECLEN+1];
static char infile[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
static char progname[FIT_FULLFSPECLEN+1];
static char docdescriptor[TIPS_DESCRIPTORMAX+1];
static struct qc_s_cmpqry *cmpqry;
static char *exe, qryspec[FIT_FULLFSPECLEN+1];
static unsigned char *bufptr, *endptr, buffer[BUFFERSIZE+1];
static fit_s_timebuf starttime, endtime, searchtime;
static double millisecs, dtextsearched, dsearchrate;
#if !FIT_UNIX && !FIT_COHERENT
static char *context;
#endif

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{


  /* Initialize dynamically allocated data to NULL */
  in = NULL;
  hithandle = NULL;
  cmpqry = NULL;
  exe = NULL;
  nsearched = 0;
  textsearched = 0;
 
  getcwd(defpath, FIT_FULLFSPECLEN); 
 
  /* Determine name portion of program file specification */ 
  fit_prsfspec(*argv, dummyspec, dummyspec, dummyspec, progname, 
    dummyspec, dummyspec); 
 
  if (--argc < 2 || !strcmp(*(++argv), "?")) { 
    /* Print usage message if not enough arguements or first arg 
	 is a question mark */ 
    printf("Usage: %s query_file filespec {filespec...}\n", 
	progname); 
    goto DONE; 
  } 
 
  /* Attempt to read in the source query */ 
  nbytes = 0; 
  fit_expfspec(*argv, ".qry", defpath, qryspec);

  if ((in = fopen(qryspec, "r")) != NULL) { 
    nbytes = fread(srcqry, 1, QRYTEXTMAX, in); 
    srcqry[nbytes] = 0; 
    fclose(in); 
    if (nbytes == 0) 
	perror("Unable to read query from query text file");
  } 
  else 
    perror("Error opening query file"); 

  if (!nbytes) goto DONE; 
 
  /* Open the result file */ 
  fit_setext(qryspec, ".hit", hitspec); 
  if ((hithandle = drct_open(hitspec, O_RDWR, 0, -1, -1)) == NULL) 
    hithandle = drct_open(hitspec, O_RDWR + O_CREAT, 0, -1, -1);
 
  if (hithandle == NULL) { 
    perror("Unable to open result file"); 
    goto DONE; 
  } 
 
  /* Compile the query */ 
  if ((nerrs = qc_compile(srcqry, 0, NULL, 0, NULL, NULL,  
      &cmpqry))) { 
    printf("Query contained %d errors\n", nerrs); 
    goto DONE; 
  } 
 
  /* Initialize a search executable */ 
  if (!prf_init(24, NULL, 0, NULL,  &exe)) { 
    perror("Error initializing search executable\n"); 
    goto DONE; 
  } 
 
  /* Add the compiled query to the search executable */ 
  if (!prf_addqry("dummyname", cmpqry, exe)) { 
    perror("Error adding query to search executable\n"); 
    goto DONE; 
  } 
 
  /* Get translate table from the search executable */ 
  prf_getxlate(exe, tx_xlate); 
  searchstat = 1 << 0; 
  fit_gettime(&starttime);
 
  /* Search each file specified by the remaining arguements on the 
     command line */ 
  for (--argc, ++argv, nsearched = 0, textsearched = 0, totalmatched = 0; 
      argc;
      --argc, ++argv) { 

#if FIT_COHERENT || FIT_UNIX
    fit_expfspec(*argv, "", defpath, infile); 
#else
    for (context = NULL; *fit_findfile(&context, *argv, NORMAL_FILE, "",
	defpath, infile);) {
#endif
    searchstat |= 4; 
    in = fopen(infile, READ); 
    doclen = 0; 
    if (in != NULL) { 
      for (bufptr = buffer,  
	   nbytes = fread(buffer, 1, BUFFERSIZE, in), 
	   doclen = (long) nbytes; 
	   nbytes; 
	   nbytes = fread(bufptr, 1, endptr-bufptr, in),  
	   doclen += nbytes ) { 
 
	  ineof = feof(in); 
	  endptr = bufptr + nbytes - 1; 
 
	  if (!ineof && nbytes == BUFFERSIZE) 
	    /* Find last delmiter char in buffer */ 
	    while (endptr > bufptr && tx_xlate[*endptr] >= 0) 
	      --endptr; 
	  else 
	    /* Set last buffer in document flag bit */ 
	    searchstat |= 1 << 1; 
 
	  buffer[nbytes] = ' '; 
 
	  /* Set length of text to search */ 
	  textlen = endptr - buffer + 1; 
 
	  /* Search next buffer in document */ 
	  nmatches = 0; 
 
	  prf_search(searchstat, 0, exe, buffer, textlen, &nmatches, 
	    &matches, NULL,NULL); 
 
	  if (textlen < nbytes) { 
	  /* Move unsearched text to beginning of buffer */ 
	    memmove(buffer, endptr+1, nbytes - textlen + 1); 
 
	    /* Set start of buffer pointer beyond moved text */ 
	    bufptr = buffer + nbytes - textlen; 
	  } 
	  else 
	    bufptr = buffer; 
 
	  /* clear search flag bits */ 
	  searchstat = 0; 
      }  /* End of search loop for current document */ 
       
      /* Close the input text file */ 
      fclose(in); 
      in = NULL; 
      textsearched += doclen;
      ++nsearched;
     
      if (nmatches) { 
	/* Add match to result file */ 
	sprintf(docdescriptor, "%s,%ld,%s\n", infile, doclen, 
	    qryspec); 
	docdescriptor_l = strlen(docdescriptor); 
	if (!drct_append(hithandle, docdescriptor,  
	    docdescriptor_l, (long) docdescriptor_l)) { 
	  perror("Error appending to result file"); 
	  goto DONE; 
	} 
	++totalmatched;
      } 
    } 
    else 
	printf("Unable to open file: %s\n", *argv); 
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  } 
  fit_gettime(&endtime);
  fit_timedif(&endtime, &starttime, &searchtime);
  secs =  fit_seconds(&searchtime);
  millisecs = (double) secs * 1000.0;
  remnant = fit_millisecs(&searchtime);
  millisecs += (double) remnant; 
  dtextsearched = (double) textsearched;
  dsearchrate = (dtextsearched * 1000.0) / millisecs;
  searchrate = dsearchrate;
  printf("Searched %ld bytes in %d files with %d matches\n", textsearched, 
  nsearched, totalmatched);
  printf("Search time: %ld.%3.3d seconds, Search rate: %ld bytes/second\n", 
    secs, remnant, searchrate);
 
DONE: 
  if (in != NULL) fclose(in); 
  if (hithandle != NULL) drct_close(hithandle); 
  return errno; 
} 

