/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : afind                                             afind.c

Function : Searches text file with queries in profile executable.

Author : A. Fregly

Abstract : This program is used to search the specified text files with a
	ASAP queries.

Calling Sequence : afind {switches}

  Switches:
    -a                          Files to be searched are TIPS archives.
    -d hex                      This switch is used to specify the 
				character used as a beginning of
				field delimiter. Whenever the field
				delimiter character is seen in the
				input text stream, the following
				character is interpreted as a field
				number.
    -f file {file...}           Files to be searched.
    [+,-]h hitfile              Hit file name. "+" specifies append. A default
				file type of ".hit" is assumed unless explicitly
				given.
    -l objlib {objlib...}       Query object libraries.
    -n field_file               The field file should contain a list of 
				field names in an order corresponding to
				the field numbers used within the input
				stream. The first entry in the field file
				corresponding to field 0, the second to
				field 1,... The field name are taken as the
				first word on each line of the field file,
				the remainder of the line being user 
				definable. If no field files is provided by
				means of the -n qualifier, afind will look
				for a field file which corresponds to the
				archive named with the -a switch, or if the
				-a switch is not used, then afind will look
				for a field file corresponding to the object
				query library. When searchinf for these
				field files, afind looks for a file with the
				same name as the archive or query object 
				library, but with a file name extension of
				".fld".
    -o objqry {objqry...}       Object query name list.
    -p mask                     Set file protection for created hit files. The
				default protection is u+rw, g+r.
    -q srcqry {srcqry...}       Source query file name list. A default file
				type of ".qry" is assumed if the files do
				not have a file type.
    -s{frequency}               Show search statistics.
    -t query_text               Query text.
    -x exeqry                   Profile executable name.
    -z sweep			Sweep hit file name.

Notes :

Change log :
000     27-SEP-90  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
002     27-JUL-94  AMF  Make work under Borland C++, V2.0
003     26-AUF-03  AMF  Set frequency at which stats are updated. Default
                        to 100.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <ctype.h>

#if FIT_DOS
#include <io.h>
#endif

#if FIT_TURBOC
#define max(a,b) (((a) > (b)) ? a : b)
#define min(a,b) (((a) < (b)) ? a : b)
#include <dir.h>
#endif
#if FIT_ZORTECH || FIT_MSC
#include <direct.h>
#endif

#include <fcntl.h>
#if FIT_UNIX || FIT_COHERENT
#if FIT_SUN
#include <sys/types.h>
#include <sys/time.h>
#include <sys/timeb.h>
#else
#include <sys/times.h>
#endif
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <drct.h>
#include <tips.h>

#if !FIT_VMS
#define QRYEXT ".qry"
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".hit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.hit"
#else
#define QRYEXT ".aqry"
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".ahit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.ahit"
#endif

#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif

#if FIT_TURBOC
#define QUERYTEXTMAX 1024
#define BUFFERSIZE 16384
#else
#define QUERYTEXTMAX 4096
#define BUFFERSIZE 30720
#endif

static void printerrors();

static   char exename[FIT_FULLFSPECLEN+1],queryspec[FIT_FULLFSPECLEN+1];
static   char objspec[FIT_FULLFSPECLEN+1],hitspec[FIT_FULLFSPECLEN+1];
static char sweepspec[FIT_FULLFSPECLEN+1];
static   char fieldfile[FIT_FULLFSPECLEN+1];
static   char searchspec[FIT_FULLFSPECLEN+1],infile[FIT_FULLFSPECLEN+1];
static   char outfile[FIT_FULLFSPECLEN+1],querytext[QUERYTEXTMAX+1];
static   char objlibname[FIT_FULLFSPECLEN+1], inputstreamname[FIT_FULLFSPECLEN+1];
static   char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
static   char docdescriptor[TIPS_DESCRIPTORMAX+1];
static   char key[TIPS_DESCRIPTORMAX+1];
static   unsigned char buffer[BUFFERSIZE+1];
static   char doclen_s[14];

static   FILE *in, *out;

#if !FIT_UNIX && !FIT_COHERENT
static   char *context;
#endif

static   struct qc_s_cmpqry *cmpqry;
static   char *xe;
static   struct prf_s_exe *exe;

static   unsigned char *bufptr=NULL, *endbuf=NULL, *endptr=NULL;
static   int *matches=NULL, nmatches, nbytes, textlen, searchstat, i, len, offset;
static   int nerrs,filematches,querymatches,srcpresent,nsearched, showstats, nfiles,statfreq;
static   int ntext, append, archive, readlen, doff, enabfields;
static   long doclen, inlen;
static   char *qtext, defpath[FIT_FULLFSPECLEN+1];
static   char *(*objptr), *(*objlibptr), *(*fileptr), *(*qrytextptr), *(*srcptr),*freqptr;
static   int nobj, nobjlib, nsrc, ineof;
static   unsigned prot;
  
static  fit_s_timebuf startprog,endprog,timedif,searchtime;
#if !FIT_UNIX && !FIT_COHERENT || 1
static  fit_s_timebuf startsearch,endsearch;
#else
static  struct tms tmsbuf;
static   long startsearch, endsearch, searchtime_l;
#endif
static   long srate,trate;
static   double dtotalbytes,dstime,searchmillisecs,dsrate,dtrate;
static   struct drct_s_file *hitfile, *objlib, *sweepfile;

  /* Initialize strings, pointers, counters */
static   long totalbytes, prevfilematches;
static char *(*fields), *(*(fielddata));
static int nfields, fielddelim;


#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{


  totalbytes = 0;
  filematches = 0;
  prevfilematches = 0;
  querymatches = 0;
  showstats = 0;
  statfreq = 100;
  prot = 0;

  exename[0] = 0;
  queryspec[0] = 0;
  objspec[0] = 0;
  hitspec[0] = 0;
  searchspec[0] = 0;
  querytext[0] = 0;
  objlibname[0] = 0;
  sweepspec[0] = 0;
  srcpresent = 0;
  xe = NULL;
  exe = NULL;
  cmpqry = NULL;
  qtext = NULL;
  nobj = 0;
  nsrc = 0;
  ntext = 0;
  nobjlib = 0;
  nfiles = 0;
  append = 0;
  archive = 0;
  nfields = 0;
  fields = NULL;
  fielddata = NULL;
  fieldfile[0] = 0;
  fielddelim = 0xff;

  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname, dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

#if !FIT_VMS
  getcwd(defpath,FIT_FULLFSPECLEN);
#else
  defpath = NULL;
#endif

  /* Pull off switches */
  for (; argc > 0 && (*(*argv) == '-' || *(*argv) == '+'); --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'a' : case 'A' :
	archive = 1;
	break;
      case 'd' : case 'D' :
	--argc;
	sscanf(*(++argv),"%x",&fielddelim);
	break;
      case 'f' : case 'F' :
	for (nfiles = 0, fileptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nfiles, ++argv)
	  ;
	break;
      case 'h' : case 'H' :
	if (!--argc) goto SYNTAX;
	append = ((*argv)[0] == '+');
	strcpy(hitspec,*(++argv));
	break;
      case 'l' : case 'L' :
	for (nobjlib = 0, objlibptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nobjlib, ++argv)
	  ;
	break;
      case 'n' : case 'N' :
	if (!--argc) goto SYNTAX;
	strcpy(fieldfile,*(++argv));
	if (!(enabfields = tips_loadflds(fieldfile, &fields, &fielddata,
		&nfields)))
	  fprintf(stderr,"Error loading field definitions from: %s\n",
		fieldfile);
	break;
      case 'o' : case 'O' :
	for (nobj = 0, objptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nobj, ++argv)
	  ;
	break;
      case 'p' : case 'P' :
	if (!--argc) goto SYNTAX;
	sscanf(*(++argv), "%o", &prot);
	break;
      case 'q': case 'Q' :
	for (nsrc = 0, srcptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nsrc, ++argv)
	  ;
	break;
      case 's' : case 'S' :
	showstats = 1;
        if (argc > 1) {
          freqptr = *(argv+1);
          if (*freqptr != '-') {
            sscanf(freqptr,"%d",&statfreq);
            ++argv;
            --argc;
          }
        }
	break;
      case 't': case 'T' :
	for (ntext = 0, qrytextptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++ntext, ++argv)
	  ;
	break;
      case 'x' : case 'X' :
	if (!--argc) goto SYNTAX;
	strcpy(exename,*(++argv));
	break;
      case 'z' : case 'Z' :
	if (!--argc) goto SYNTAX;
	strcpy(sweepspec, *(++argv));
	break;
      default :
	goto SYNTAX;
    }
  }

  /* Initialize profile executable */
  if (exename[0]) {
    fit_expfspec(exename,EXEEXT,defpath,exename);
    printf("Loading profile executable: %s\n",exename);
    if (!prf_read(exename,&xe)) {
      perror("*** Unable to load profile executable");
      goto DONE;
    }
  }
  else {
    if (!enabfields && archive && nfiles) {
      fit_setext(*fileptr, ".fld", fieldfile);
      enabfields = tips_loadflds(fieldfile,&fields,&fielddata,&nfields);
    }
    if (!enabfields && nobjlib) {
      fit_setext(*objlibptr, ".fld", fieldfile);
      enabfields = tips_loadflds(fieldfile,&fields,&fielddata,&nfields);
    }
    if (!prf_init(TIPS_MAXTERMWORDLEN, NULL, nfields, fields, &xe)) {
      perror("*** Error initializing profile executable");
      goto DONE;
    }
  }
  exe = (struct prf_s_exe *) xe;

  if (nsrc) {
    printf("Loading query source files:\n");
    for (; nsrc; --nsrc, ++srcptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *srcptr, NORMAL_FILE,
	QRYEXT, defpath, infile);) {
#else
      fit_expfspec(*srcptr, QRYEXT, defpath, infile);
#endif
	printf("  %s\n",infile);
	if ((in = fopen(infile, "r")) != NULL) {
	  len = fread(querytext, 1, QUERYTEXTMAX, in);
	  if (len > 0) {
	    *(querytext+len) = 0;
	    nerrs = qc_compile(querytext, 0, NULL, 0, NULL, NULL, &cmpqry);
	    if (nerrs) {
	      printf ("*** Query contains %d errors\n",nerrs);
	      printerrors(querytext,nerrs);
	    }
	    else {
	      fit_setext(infile,OBJEXT,outfile);
	      qc_writeqry(outfile,querytext,cmpqry);
	      if (!prf_addqry(outfile,cmpqry,xe)) {
		perror("*** Error linking query into profile executable");
		goto DONE;
	      }
	    }
	  }
	  fclose(in);
	}
	else
	  printf("*** Error opening query source file");
      }
#if !FIT_UNIX && !FIT_COHERENT
    }
#endif
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (nobj) {
    printf("Loading query object files:\n");
    for (; nobj; --nobj, ++objptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *objptr, NORMAL_FILE,
	OBJEXT, defpath, infile);) {
#else
      fit_expfspec(*objptr, OBJEXT, defpath, infile);
#endif
	printf("  %s\n",infile);
	if (qc_readqry(infile,&qtext,&cmpqry)) {
	  if (!prf_addqry(infile,cmpqry,xe)) {
	    perror("*** Error linking query into profile executable");
	    goto DONE;
	  }
	}
	else
	  perror("*** Error loading query object file");
#if !FIT_UNIX && !FIT_COHERENT
      }
#endif
    }
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (nobjlib) {
    printf("Loading query object libraries:\n");
    for (; nobjlib; --nobjlib, ++objlibptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *objlibptr, NORMAL_FILE,
	  ".qlb", defpath, infile);) {
#else
      fit_expfspec(*objlibptr, ".qlb", defpath, infile);
#endif
	printf("  %s...\n",infile);
	if ((objlib = qc_openlib(infile, O_RDWR, prot)) != NULL) {
	  for (cmpqry = NULL; qc_nextlib(objlib, infile, &cmpqry) >= 0;) {
	    printf("  ...%s\n",infile);
	    if (!prf_addqry(infile, cmpqry, xe)) {
	      perror("*** Error linking query into profile executable");
	      goto DONE;
	    }
	  }
	  drct_close(objlib);
	}
	else
	  perror("*** Error opening query object library");
#if !FIT_UNIX && !FIT_COHERENT
      }
#endif
    }
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (ntext) {
    /* Read in text of command line query */
    for (offset = -1; ntext; --ntext, ++qrytextptr) {
      fit_sadd(querytext, QUERYTEXTMAX, *qrytextptr, &offset);
      if (ntext-1)
	fit_sadd(querytext,QUERYTEXTMAX," ",&offset);
    }
    querytext[min(offset+1,QUERYTEXTMAX)] = 0;
    srcpresent = *querytext;
  }

  if (srcpresent) {
    if (nerrs = qc_compile(querytext, 0, NULL, 0, NULL, NULL, &cmpqry)) {
      printf ("*** Query contains %d errors\n",nerrs);
      printerrors(querytext,nerrs);
    }
    else {
      fit_expfspec("afind",DEFOBJ,defpath,outfile);
      qc_writeqry(outfile,querytext,cmpqry);
      if (!prf_addqry(outfile, cmpqry, xe)) {
	perror("*** Error linking query into profile executable");
	goto DONE;
      }
    }
  }

  if (!exe->nqueries) {
    printf("No queries!  Search canceled\n");
    goto DONE;
  }

  if (cmpqry != NULL) qc_dealloc(&cmpqry);

  endbuf = buffer + BUFFERSIZE;
  searchstat = 1;

  fit_expfspec(hitspec,DEFHIT,defpath,hitspec);
  if (!append)
    drct_delfile(hitspec);
  if ((hitfile = drct_open(hitspec,O_RDWR, prot, -1, -1)) == NULL) {
    if ((hitfile = drct_open(hitspec, O_RDWR + O_CREAT, prot, -1, -1)) ==
	NULL) {
      perror("Error opening hit file");
      goto DONE;
    }
  }

  if (sweepspec[0]) {
    fit_expfspec(sweepspec, DEFHIT, defpath, sweepspec);
    if (!append)
      drct_delfile(sweepspec);
    if ((sweepfile = drct_open(sweepspec,O_RDWR, prot, -1, -1)) == NULL) {
      if ((sweepfile = drct_open(sweepspec, O_RDWR + O_CREAT, prot, -1, -1))
	  == NULL) {
        perror("Error opening sweep file");
        goto DONE;
      }
    }
  }

  if (showstats) {
    printf("Starting search...\n");
    printf("    Bytes       Files  Matched     Hits\n");
    printf("         0          0        0        0");
    fflush(stdout);
#if !FIT_UNIX && !FIT_COHERENT || 1
    fit_zerotime(&searchtime);
#else
    searchtime_l = 0;
#endif
    fit_gettime(&startprog);
    nsearched = 0;
  }
  
  for (; nfiles; --nfiles, ++fileptr) {
#if !FIT_UNIX && !FIT_COHERENT
	  /* printf("\nExpanding file spec to search: %s\n", *fileptr); */
    for (context = NULL; *fit_findfile(&context, *fileptr, NORMAL_FILE,
	"", defpath, infile);) {
#else
    fit_expfspec(*fileptr, "", defpath, infile);
#endif

    searchstat |= 4;
    in = fopen(infile, FIT_READ);
    doclen = 0;

    if (in != NULL && !archive) {
      for (bufptr = buffer, nbytes = fread(buffer, 1, BUFFERSIZE, in),
	  doclen = nbytes;
	  nbytes;
	  nbytes = fread(bufptr, 1, endbuf-bufptr, in), doclen += nbytes ) {

	ineof = feof(in);

	if (showstats) {
	  if (nsearched && !(nsearched % statfreq)) {
	    printf("\r%10ld  %9d",totalbytes,nsearched);
	    if (prevfilematches != filematches) {
	      printf("%9d%9d",filematches,querymatches);
	      prevfilematches = filematches;
	    }
	    fflush(stdout);
	  }
	  nsearched += 1;
#if !FIT_UNIX && !FIT_COHERENT || 1
	  fit_gettime(&startsearch);
#else
	  startsearch = times(&tmsbuf);
#endif
	}

	endptr = bufptr + nbytes - 1;

	if (!ineof && nbytes == BUFFERSIZE)
	  /* Find last delmiter char in buffer */
	  while (endptr > bufptr && *(exe->tx_xlate+*endptr) >= 0)
	    --endptr;
	else
	  /* Set last buffer in document flag bit */
	  searchstat |= 2;

	buffer[nbytes] = ' ';

	/* Set length of text to search */
	textlen = endptr - buffer + 1;

	/* Search next buffer in document */
	nmatches = 0;

	prf_search(searchstat, 0, xe, buffer, textlen, &nmatches, &matches,
		NULL,NULL);

	if (textlen < nbytes) {
	  /* Move unsearched text in buffer to beginning of buffer */
	  memmove(buffer, endptr+1, nbytes - textlen + 1);

	  /* Set start of buffer pointer at loc beyond moved text */
	  bufptr = buffer + nbytes - textlen;
	}
	else
	  bufptr = buffer;

	/* clear flag bits */
	searchstat = 0;

	if (showstats) {
#if !FIT_UNIX && !FIT_COHERENT || 1
	  fit_gettime(&endsearch);
	  fit_timedif(&startsearch, &endsearch, &timedif);
	  fit_addtime(&searchtime, &timedif, &searchtime);
#else
	  endsearch = times(&tmsbuf);
	  searchtime_l += (endsearch - startsearch);
#endif
	}
      }
    
      totalbytes += doclen;
      
      /* Close the input text file */
      fclose(in);

      /* Create hits for  matching queries */
      if (nmatches || sweepspec[0]) {
	if (nmatches) {
	  filematches += 1;
	  querymatches += nmatches;
	}
	tips_makedescr("", -1L, infile, doclen, "", TIPS_DESCRIPTORMAX,
	  docdescriptor, &len);
	len -= 2;
	fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, ",", &len);
	for (i = 0; i < nmatches; ++i) {
	  doff = len;
	  fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX,
	    *(exe->qfiles + *(matches+i)), &doff);
	  fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, "\n", &doff);
	  docdescriptor[doff+1] = 0;
	  if (drct_append(hitfile, docdescriptor, (size_t) doff+1, (size_t) (doff+1))
	      == 0){
	    perror("\n*** Hit file write error");
	    goto DONE;
	  }                          
	}
	if (!nmatches && sweepspec[0]) {
	  doff = len;
	  fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, "\n", &doff);
	  docdescriptor[doff+1] = 0;
	  if (drct_append(sweepfile, docdescriptor, (size_t) doff+1, (size_t) (doff+1))
	      == 0){
	    perror("\n*** Sweep file write error");
	    goto DONE;
	  }                          
	}
      }
    }

    else if (in != NULL) {
      /* Processing an archive */
      strcpy(inputstreamname, infile);

      while (!feof(in)) {
	/* Skip over any leading nulls in the input stream */
	fscanf(in, " %c", docdescriptor);

	if (feof(in)) break;

	/* Push the non-null character back onto the input stream */
	ungetc(*docdescriptor, in);

	*docdescriptor = 0;
	fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,in);
	if (feof(in)) break;

	len = strlen(docdescriptor);
	if (len) docdescriptor[len-1] = 0;

	if (!tips_parsedsc(docdescriptor, infile, &doclen, key)) {
	  fit_logmsg(stderr,"*** Error parsing input descriptor",1);
	  goto DONE;
	}
	
	if (showstats) {
	  if (nsearched && !(nsearched % statfreq)) {
	    printf("\r%10ld  %9d",totalbytes,nsearched);
	    if (prevfilematches != filematches) {
	      printf("%9d%9d",filematches,querymatches);
	      prevfilematches = filematches;
	    }
	    fflush(stdout);
	  }
	  nsearched += 1;
	}

	for (searchstat |= 4, bufptr = buffer, inlen = doclen;
	  !feof(in) && inlen; inlen -= nbytes) {

	  if (endbuf-bufptr < inlen)
	    readlen = endbuf-bufptr;
	  else
	    readlen = (int) inlen;

	  if (!(nbytes = fread(bufptr, 1, readlen, in))) {
	    printf("\n*** Read error on archive: %s\n",inputstreamname);
	    printf("    document: %s/n",docdescriptor);
	    perror("");
	    goto DONE;
	  }

#if !FIT_UNIX && !FIT_COHERENT || 1
	  fit_gettime(&startsearch);
#else
	  startsearch = times(&tmsbuf);
#endif
	  endptr = bufptr + nbytes - 1;

	  if (inlen > nbytes)
	    /* Find last delmiter char in buffer */
	    while (endptr > bufptr && *(exe->tx_xlate+*endptr) > 0)
	      --endptr;
	  else {
	    /* Set last buffer in document flag bit */
	    searchstat |= 2;
	  }

	  buffer[nbytes] = ' ';

	  /* Set length of text to search */
	  textlen = endptr - buffer + 1;

	  /* Search next buffer in document */
	  nmatches = 0;

	  prf_search(searchstat, 0, xe, buffer, textlen, &nmatches, &matches,
		NULL,NULL);

	  if (textlen < nbytes) {
	    /* Move unsearched text in buffer to beginning of buffer */
	    memmove(buffer, endptr+1, nbytes - textlen + 1);

	    /* Set start of buffer pointer at loc beyond moved text */
	    bufptr = buffer + nbytes - textlen;
	  }
	  else
	    bufptr = buffer;

	  /* clear flag bits */
	  searchstat = 0;
	  if (showstats) {
#if !FIT_UNIX && !FIT_COHERENT || 1
	    fit_gettime(&endsearch);
	    fit_timedif(&startsearch, &endsearch, &timedif);
	    fit_addtime(&searchtime, &timedif, &searchtime);
#else
	    endsearch = times(&tmsbuf);
	    searchtime_l += (endsearch - startsearch);
#endif
	  }
	}

	totalbytes += doclen;

	/* Add hits for matching queries */
	if (nmatches || sweepspec[0]) {
	  if (nmatches) {
	    filematches += 1;
	    querymatches += nmatches;
	  }

	  tips_makedescr(inputstreamname, -1L, infile, doclen, "", 
	    TIPS_DESCRIPTORMAX, docdescriptor, &len);
	  len -= 2;
	  fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, ",", &len);

	  for (i = 0; i < nmatches; ++i) {
	    doff = len;
	    fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX,
		*(exe->qfiles + *(matches+i)), &doff);
	    if (*key) {
	      fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, "#", &doff);
	      fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, key, &doff);
	    }
	    fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, "\n", &doff);
	    docdescriptor[doff+1] = 0;
	    if (drct_append(hitfile, docdescriptor, doff+1, (long) (doff+1))
		< 0) {
	      perror("\n*** Hit file write error");
	      goto DONE;
	    }
	  }
	  if (!nmatches && sweepspec[0]) {
	    doff = len;
	    if (*key)
	      fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, key, &doff);
	    fit_sadd(docdescriptor, TIPS_DESCRIPTORMAX, "\n", &doff);
	    docdescriptor[doff+1] = 0;
	    if (drct_append(sweepfile, docdescriptor, (size_t) doff+1, (size_t) (doff+1))
		< 0) {
	      perror("\n*** Sweep file write error");
	      goto DONE;
	    }
	  }
	}
      }
    }
#if !FIT_UNIX && !FIT_COHERENT
    }
#endif
  }

  drct_close(hitfile);
  if (sweepspec[0]) drct_close(sweepfile); 

  if (showstats && totalbytes) {
    fit_gettime(&endprog);

    printf("\r%10ld  %9d%9d%9d\n\n",totalbytes,nsearched,filematches,
      querymatches);

#if FIT_UNIX && 0 || FIT_COHERENT && 0
    if (searchtime_l < 0) searchtime_l = -searchtime_l;
    searchtime.time = searchtime_l / 100;
    searchtime.millitm = (searchtime_l % 100) * 10;
#endif

    fit_timedif(&startprog, &endprog, &timedif);

    printf("Total search time,     %5lu.%03.3u seconds\n",
	fit_seconds(&timedif), fit_millisecs(&timedif));
    printf("Match processing time, %5lu.%03.3u seconds\n",
      fit_seconds(&searchtime), fit_millisecs(&searchtime));
    fit_timedif(&timedif, &searchtime, &timedif);
    printf("I/O overhead time,     %5lu.%03.3u seconds\n",
      fit_seconds(&timedif), fit_millisecs(&timedif));

    fit_timedif(&startprog, &endprog, &timedif);

    printf("Search rates per second\n");

    dtotalbytes = totalbytes;
    dtotalbytes *= 1000.0;

    dstime = (fit_seconds(&timedif) * 1000 + fit_millisecs(&timedif));
    if (!dstime) dstime = 1;
    dsrate = dtotalbytes / dstime;
    srate = dsrate;
    printf("  Search Processing Overall: %7ld\n",srate);

    searchmillisecs = (fit_seconds(&searchtime) * 1000 +
	fit_millisecs(&searchtime));
    if (!searchmillisecs) searchmillisecs = 1;
    dtrate = dtotalbytes / searchmillisecs;
    trate = dtrate;
    printf("  Match processing:          %7ld\n",trate);
  }
  else if (showstats)
    printf("\n*** No text was searched\n");

  goto DONE;

SYNTAX:
  printf("Usage: %s {switches}\n", progname);
  printf("Switches:\n");
  printf("  -a\t\t\tFiles to be searched are TIPS archives.\n");
  printf("  [+,-]h hitfile\tHit file into which to  put results.\n");
  printf("  -f file {file...}\tList of files to search.\n");
  printf("  -l objlib {objlib...}\tList of query object libraries.\n");
  printf("  -n field_file\t\tField definition file.\n");
  printf("  -o objqry {objqry...}\tList of object queries.\n");
  printf("  -p mask\t\tSet created hitfile protection mask.\n");
  printf("  -q srcqry {srcqry...}\tList of source queries.\n");
  printf("  -s\t\t\tShow search statistics.\n");
  printf("  -t querytext\t\tQuery text.\n");
  printf("  -x qryexe\t\tQuery executable.\n");
  printf("  -z sweep_file\t\tSweep hit file for unmatched documents.\n");

DONE:
  return(0);
}

#if FIT_ANSI
void printerrors(char *querytext, int nerrs)
#else
void printerrors(querytext, nerrs)
  char *querytext;
  int nerrs;
#endif
{
  int i,n, errnum;
  char *errmsg;
  for (i=1; i <= nerrs; ++i) {
    errmsg = qc_geterr(i,&errnum, &n);
    qc_puterr(stdout,querytext,errmsg,n);
  }
}
