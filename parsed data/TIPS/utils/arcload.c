/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : arcload                                           arcload.c

Function : Archive loader filter

Author : A. Fregly

Abstract : This function is used to load a TIPS archive. It will read from
	standard input the documents to be loaded, appending them to the
	specified archive and also copying the data written to the archive
	to standard output.  All error and status messages are written to
	standard error.

	The input documents must be preceeded by the standard TIPS document
	header. When inserted into the archive, the header is placed at the
	front of the document, and is prefixed by the document number (in
	ASCII) followed by a "#".

Calling Sequence : arcload {-l} archive_name

	-l      Enables logging of initiation, processing, and termination
		messages to standard error.
	-p mask Set file proteciton mask for archive it it is created. The
		default protection is u+rw, g+r.

Notes: 

Change log :
000     15-JUL-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fitsydef.h>
#include <signal.h>

#include <fcntl.h>

#if FIT_STDLIB
#include <stdlib.h>
#endif

#include <tips.h>
#include <drct.h>

#if FIT_TURBOC
#define BUFFERSIZE 16384
#else
#define BUFFERSIZE 30720
#endif

extern int errno;

/* Declare the shutdown procedure */
void arcload_shutdown();

int logging;
unsigned long totalbytes, nloaded;
char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
struct drct_s_file *archandle;
struct drct_s_info info;
char docdescriptor[TIPS_DESCRIPTORMAX+81];
char infile[FIT_FULLFSPECLEN+1], logmsg[2*FIT_FULLFSPECLEN+1];
char arcname[TIPS_DESCRIPTORMAX+1], docnum_s[TIPS_DESCRIPTORMAX+1];
char inname[TIPS_DESCRIPTORMAX+1];
char key[TIPS_DESCRIPTORMAX+1];
char buffer[BUFFERSIZE+1];

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int stat, len, readlen, nbytes;
  long doclen, inlen, lastrec;
  unsigned prot;

#if FIT_DOS
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  stat = 1;
  totalbytes = 0;
  nloaded = 0;
  lastrec = 0;
  archandle = NULL;
  logging = 0;
  prot = 0;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  /* Pick off command arguements */
  for (; argc > 1; --argc, ++argv) {
    if ((*argv)[0] != '-') goto SYNTAX_ERROR;

    switch ((*argv)[1]) {
      case 'l' : case 'L' :
	logging = 1;
	break;
      case 'p' : case 'P' :
	if (!--argc) goto SYNTAX_ERROR;
	sscanf(*(++argv),"%o", &prot);
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  if (argc > 1) goto SYNTAX_ERROR;
  if (!argc) goto SYNTAX;

  if ((archandle = drct_open(*argv, O_RDWR, prot, -1, -1)) == NULL)
    if ((archandle = drct_open(*argv, O_RDWR + O_CREAT, prot, -1, -1)) == 
	NULL) {
      sprintf(logmsg,"*** Error opening archive: %s", *argv);
      fit_logmsg(stderr, logmsg, 1);
      goto ERREXIT;
    }

  if (logging) {
    sprintf(logmsg, "*** %s initiated ***", progname);
    fit_logmsg(stderr, logmsg, 1);
    fprintf(stderr,"Loading archive: %s\n",*argv);
  }

#if FIT_UNIX || FIT_COHERENT
  signal(SIGPIPE,arcload_shutdown);
  signal(SIGQUIT,arcload_shutdown);
  signal(SIGHUP, SIG_IGN);
#endif
  signal(SIGTERM,arcload_shutdown);
  signal(SIGINT,arcload_shutdown);

  while (1) {

    /* Skip over any leading nulls in the input stream */
    for (*docdescriptor = 0; !*docdescriptor && !feof(stdin);)
      *docdescriptor = getchar();

    if (!*docdescriptor) break;

    /* Push the non-null character back onto the input stream */
    ungetc(*docdescriptor, stdin);

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);
    if (feof(stdin)) goto DONE;

    len = strlen(docdescriptor);
    if (len) docdescriptor[len-1] = 0;

    if (!tips_parsedsc(docdescriptor,infile,&doclen,key)) {
      fit_logmsg(stderr,"*** Error parsing input descriptor\n",1);
      goto DONE;
    }
    tips_nameparse(infile, arcname, docnum_s, inname);

    /* Determine record number which will be assigned to document to be added */
    if (!drct_wlock(archandle, lastrec, 0L)) {
      fit_logmsg(stderr,"*** Error locking direct access file",1);
      goto ERREXIT;
    }

    if (!drct_info(archandle, &info)) {
      fit_logmsg(stderr,
	"*** Error retrieving file info for direct access file\n",1);
      goto ERREXIT;
    }

    lastrec = info.nrecs;
    if (!drct_wlock(archandle, lastrec, 0L)) {
      fit_logmsg(stderr,"*** Error locking direct access file",1);
      goto ERREXIT;
    }

    /* Make document descriptor */
    tips_makedescr("", lastrec, inname, doclen, key, TIPS_DESCRIPTORMAX+1,
	docdescriptor, &len);
    strcpy(buffer, docdescriptor);
    docdescriptor[len-1] = 0;

    if (logging) {
      /* Log document load */
      fit_logmsg(stderr,docdescriptor,1);
    }

    /* Start the archive document with the descriptor */
    if (!drct_append(archandle, buffer, len, doclen + len)) {
      fit_logmsg(stderr,"*** Error adding document header to archive",1);
      goto ERREXIT;
    }

    totalbytes += len;

    /* Also write the buffer to sdtout so filter on backside can see it */
    fwrite(buffer, 1, len, stdout);

    for (inlen = doclen; !feof(stdin) && inlen; inlen -= nbytes) {

      if (BUFFERSIZE < inlen)
	readlen = BUFFERSIZE;
      else
	readlen = inlen;

      if ((nbytes = fread(buffer, 1, readlen, stdin)) != readlen) {
	fit_logmsg(stderr,"*** Input error",1);
	goto ERREXIT;
      }

      if (!drct_append(archandle, buffer, nbytes, doclen) < 0) {
	fit_logmsg(stderr,"*** Error appending data to archive",1);
	goto ERREXIT;
      }

      /* Also write the buffer to sdtout so filter on backside can see it */
      fwrite(buffer, 1, nbytes, stdout);

      totalbytes += nbytes;
    }

    nloaded += 1;
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr,"*** Syntax error\n",1);

SYNTAX:
  fprintf(stderr,"Usage: %s {-l} archive_name\n",progname);
  fprintf(stderr,"  -l\t\tEnable status logging to standard error.\n");
  fprintf(stderr,"  -p mask\tSet created archive protection mask.\n");
  goto DONE;

ERREXIT:
  perror("");

DONE:
  if (archandle != NULL) drct_close(archandle);
  if (logging) {
    fprintf(stderr,"Loaded %ld documents, %ld bytes\n",nloaded,totalbytes);
    sprintf(logmsg,"*** %s terminated ***\n",progname);
    fprintf(stderr,logmsg,progname);
  }
  return stat;
}


void arcload_shutdown() {
  int err;
  err = errno;
  fprintf(stderr,"Loaded %ld documents, %ld bytes\n",nloaded,totalbytes);
  sprintf(logmsg,"*** %s aborted ***",progname);
  fit_logmsg(stderr,logmsg,1);
  perror("");
  if (archandle != NULL) drct_close(archandle);
  exit(err);
}
