/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : getmask						getmask.c

Function: This function will write to standard output a protection mask 
	suitable for use with "chmod".

Author : A. Fregly

Abstract : This program 

Calling Sequence : getmask filespec

  filespec 	Name of file for which protection mask is desired. Note that
		if filespec is not supplied or cannot be accessed, no output
		is written.

Notes: 

Change log : 
000  23-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fitsydef.h>

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *argv;
#endif
{

  unsigned protmask;
  struct stat statbuf;

  if (--argc) 
    if (!stat(*(++argv), &statbuf)) {
      protmask= ((unsigned) statbuf.st_mode) & 0777;
      printf("%o\n", protmask);
    }
  return 0;
}
