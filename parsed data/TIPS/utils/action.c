/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : action                                            action.c

Function : Puts documents into specified directory. 

Author : A. Fregly

Abstract : This function is used to perform an action based on each TIPS 
	document descriptor which it reads from standard input. The action
	may be any executable program or script. Selected portions of the
	descriptor may be selected for use as arguements to the action
	program. By default, the entire descriptor is fed to the command
	as a parameter. If portions of the descriptor are specified, they
	are fed to the command as parameters in the order in which they
	occur in the descriptor. The command is not executed if a descriptor
	does not contain any one of the selected components.

Calling Sequence : action {switches} {cmd} 

  Switches:

	-a      Include archive name and document numbers as parameters.
	-k      Include key field as a parameter.
	-o      Include "original" document name as a parameter.
	-s      Include document size as a parameter.

  Parameter:

	cmd     Command to be executed.

Notes: 

Change log : 
000     13-AUG-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_DOS
#include <process.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tips.h>

/* Declare the shutdown procedure */
#if FIT_ANSI
void action_shutdown(int ss);
#else
void action_shutdown();
#endif

/* Globals */
extern int errno;

char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
char arcname[FIT_FULLFSPECLEN+1], docnum_s[TIPS_DESCRIPTORMAX+1]; 
char docdescriptor[TIPS_DESCRIPTORMAX+1], docnamefld[TIPS_DESCRIPTORMAX+1];
char docname[FIT_FULLFSPECLEN+1], docspec[FIT_FULLFSPECLEN+1];
char dockeyfld[TIPS_DESCRIPTORMAX+1], logmsg[FIT_FULLFSPECLEN*2];


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int use_descr, use_arc, use_orig, use_size, use_key;
  int nargs, stat, pid, tpid, childstat;
  char *(*arglist), *(*argptr), size_s[14], *argval;
  long doclen;

#if FIT_TURBOC
  fit_binmode(stdout);
#endif

  /* Determine invoking command name */
  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  use_arc = 0;
  use_orig = 0;
  use_size = 0;
  use_key = 0;
  nargs = 0;
  stat = 1;

  /* Pick off switches and determine number of argueements for command */
  for (; argc && *(*argv) == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'a' : case 'A' :
	use_arc = 1;
	nargs += 2;
	break;
      case 'k' : case 'K' :
	use_key = 1;
	++nargs;
	break;
      case 'o' : case 'O' :
	use_orig = 1;
	++nargs;
	break;
      case 's' : case 'S' :
	use_size = 1;
	++nargs;
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  if (!argc) goto SYNTAX_ERROR;
  nargs += argc;

  use_descr = !(use_arc || use_orig || use_size || use_key);
  nargs += use_descr;

  /* Add one to nargs to make room for NULL pointer as last entry */
  nargs += 1;

  /* Allocate arguement list for exec call */
  arglist = (char *(*)) calloc(nargs, sizeof(*arglist));
  if (arglist == NULL) {
    perror("*** Error allocating internal data structures");
    goto DONE;
  }

  /* Put command and it's parameters into arguement list */
  for (argptr = arglist; argc; --argc) {
    argval = (char *) malloc(strlen(*argv)+1);
    strcpy(argval, *(argv++));
    *(argptr++) = argval;
  }

  /* Add descriptor based arguements to arguement list */
  if (use_descr)
    *(argptr++) = docdescriptor;

  if (use_arc) {
    *(argptr++) = arcname;
    *(argptr++) = docnum_s;
  }

  if (use_orig)
    *(argptr++) = docspec;

  if (use_size)
    *(argptr++) = size_s;

  if (use_key)
    *(argptr++) = dockeyfld;

  *argptr = NULL;

  signal(SIGTERM, action_shutdown);
  signal(SIGINT,action_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE,action_shutdown);
  signal(SIGQUIT,action_shutdown);
#endif

  /* main loop, process incoming descriptors until EOF */
  while (1) {

    /* Skip over any leading nulls in the input stream */
    for (*docdescriptor = 0; !*docdescriptor && !feof(stdin);)
      *docdescriptor = getchar();

    if (!*docdescriptor) break;

    /* Push the non-null character back onto the input stream */
    ungetc(*docdescriptor, stdin);

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);

    if (feof(stdin)) goto DONE;

    docdescriptor[strlen(docdescriptor)-1] = 0;

    /* Parse the descriptor */
    tips_parsedsc(docdescriptor, docnamefld, &doclen, dockeyfld);

    if (!use_descr)
      /* Parse the name field so that the output name can be determined */ 
      tips_nameparse(docnamefld, arcname, docnum_s, docspec);

    if ((!use_descr || *docdescriptor) &&
	(!use_arc || (*arcname && *docnum_s)) &&
	(!use_orig || *docspec) && (!use_size || doclen) &&
	(!use_key || *dockeyfld)) {

      if (use_size)
	sprintf(size_s,"%ld",doclen);

#if FIT_UNIX || FIT_COHERENT
      if ((pid = fork()) > 0) {
	/* Wait for child to die */
	for (tpid = wait(&childstat);
	    tpid != pid && tpid != -1 || (tpid == -1 && errno == EINTR);
	    tpid = wait(&childstat))
	  ;
      }

      else if (!pid) { 
	execvp(arglist[0], arglist);
	exit(0);
      }
#endif
#if FIT_ZORTECH
      if (spawnvp(0, arglist[0], arglist) == -1)
	exit(0);
#endif
#if FIT_TURBOC
      if (spawnvp(P_WAIT, arglist[0], arglist) == -1)
	exit(0);
#endif
    }
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  stat = 1;
  fprintf(stderr,"*** Invalid command syntax\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} cmd {cmdarg...}\n",progname);
  fprintf(stderr,
    "  -a\tUse archive name and document numbers as parameters.\n");
  fprintf(stderr,"  -k\tUse key field as a parameter.\n");
  fprintf(stderr,"  -o\tUse original document name as a parameter.\n");
  fprintf(stderr,"  -s\tUse document size as a parameter.\n");
  goto DONE;

DONE:
  return stat;
}

#if FIT_ANSI
void action_shutdown(int ss)
#else
void action_shutdown(ss)
int ss;
#endif
{
  int stat;
  stat = errno;
  sprintf(logmsg,"**** %s aborted ****",progname);
  fit_logmsg(stderr,logmsg,1);
  if (stat) perror("");
  exit(stat);
}
