/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : plink                                             plink.c

Function : Links compiled queries into profile executable.

Author : A. Fregly

Abstract : This program is used to link compile query files into a
	query executable. The executable file will default to a file
	type of ".QEX".  The object queries will default to a file type
	of ".QC".  If any errors occur during link of a file, the
	errors will be placed in  a ".ERR" file.

Calling Sequence : plink {-n field_file} exename qname{,qname...}

  Switches:

    -n field_file               The field file should contain a list of 
				field names in an order corresponding to
				the field numbers used within the input
				stream. The first entry in the field file
				corresponding to field 0, the second to
				field 1,... The field name are taken as the
				first word on each line of the field file,
				the remainder of the line being user 
				definable. If no field files is provided by
				means of the -n qualifier, afind will look
				for a field file which corresponds to the
				archive named with the -a switch, or if the
				-a switch is not used, then afind will look
				for a field file corresponding to the object
				query library. When searchinf for these
				field files, afind looks for a file with the
				same name as the archive or query object 
				library, but with a file name extension of
				".fld".

  Parameters:

	exename Name of profile executable.  If it doesn't exist, it will
		be created, otherwise it is updated.

	qname   One or more file names of files containing query object
		files.  The file names may contain wild cards.



Notes :

Change log :
001     15-DEC-92  AMF  Compile under COHERENT.
002     27-JUL-94  AMF  Fix calls to fit_findfile.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_DOS
#include <direct.h>
#endif
#include <qc.h>
#include <qcstat.h>
#include <prf.h>
#include <tips.h>

#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif

#if FIT_TURBOC
#define MAXROWS 16
#else
#define MAXROWS 24
#endif


#if FIT_ANSI
main(int argc, char *argv[])
#else
main(argc,argv)
  int argc;
  char *(*argv);
#endif
{

#if !FIT_UNIX && !FIT_COHERENT
#if FIT_ANSI
  void *state;
#else
  char *state;
#endif
#endif

  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  char exename[FIT_FULLFSPECLEN+1];

  char infile[FIT_FULLFSPECLEN];
  char errfile[FIT_FULLFSPECLEN];

  int stat;
  char *srcptr=NULL;
  char defpath[FIT_FULLFSPECLEN+1];

  struct qc_s_cmpqry *cmpqry=NULL;
  char *exe=NULL;

  char fieldfile[FIT_FULLFSPECLEN+1];
  char *(*fields), *(*(fielddata));
  int nfields, enabfields;

  nfields = 0;
  enabfields = 0;
  fieldfile[0] = 0;
  fields = NULL;
  fielddata = NULL;


  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (--argc < 2 || argc && !strcmp(*argv,"?")) goto SYNTAX;

  if (*(*argv) == '-') {
    if (((*argv)[1] == 'n' || (*argv)[1] == 'N') && --argc) {
      strcpy(fieldfile, *(++argv));
      --argc;
      ++argv;
      if (!(enabfields = tips_loadflds(fieldfile, &fields, &fielddata,
	    &nfields)))
	fprintf(stderr,"Error loading field definitions from: %s\n",
	    fieldfile);
    }
    else {
      fprintf(stderr, "*** Invalid switch: %s\n", *argv);
      goto SYNTAX;
    }
  }

  if (argc < 2) goto SYNTAX;
  strcpy(exename,*(argv++));
  --argc;

#if !FIT_VMS
  getcwd(defpath, FIT_FULLFSPECLEN);
#else
  defpath = (char *) calloc(1,1);
#endif
  fit_setext(exename,".err",errfile);

  fit_expfspec(exename,".prf",defpath,exename);
  exe = NULL;
  if (!prf_read(exename,&exe)) {
    fprintf(stderr,"Creating profile executable: %s\n", exename);
    if (!enabfields) {
      fit_setext(exename, ".fld", fieldfile);
      tips_loadflds(fieldfile, &fields, &fielddata, &nfields);
    }

    if (!prf_init(MAXROWS,NULL,nfields,fields,&exe)) {
      fprintf(stderr,"*** Error initializing profile executable:");
      fprintf(stderr," %s\n",fit_getmsg(fit_lasterr()));
      goto DONE;
    }
  }
  else
    fprintf(stderr,"Modifying profile executable: %s\n",exename);

  cmpqry = NULL;
  infile[0] = 0;

  stat=0;


  for (; argc; --argc) {
#if FIT_UNIX || FIT_COHERENT
    strcpy(infile, *(argv++));
#else
  for (state = NULL; *fit_findfile(&state,*argv, NORMAL_FILE, ".qc",defpath,
    infile);) {
#endif
    fprintf(stderr,"%s\n",infile);

    if (qc_readqry(infile, &srcptr, &cmpqry)) {
      if (!prf_addqry(infile,cmpqry,exe)) {
	fprintf(stderr,"*** Error adding query to executable: %s\n",
	fit_getmsg(fit_lasterr()));
	goto DONE;
      }
      else
	stat += 1;
    }
    else {
      fprintf(stderr,"*** Error reading query object file: %s\n",
      fit_getmsg(fit_lasterr()));
    }
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  }
  goto DONE;

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} exename object_query_list\n",progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -n field_file\t\tField definition file.\n");
  fprintf(stderr,"Parameters:\n");
  fprintf(stderr,"  exename\t\tQuery executable to be created/updated.\n");
  fprintf(stderr,
    "  object_query_list\tObject queries to put into executable.\n");
  return 0; 

DONE:
  if (stat && exe != NULL) {
    stat = prf_write(exename,exe);
    if (!stat)
      fprintf(stderr,"*** Error writing profile executable: %s\n",
	fit_getmsg(fit_lasterr()));
  }
  else
    fprintf(stderr,"*** Unable to find object queries\n");

  return(0);
}
