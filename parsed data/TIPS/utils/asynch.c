/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : asynch						asynch.c

Function : Reads incoming text data from port, creating document files.

Author : A. Fregly

Abstract : 

  This program is used to read data from a communications port. It
  will store the incoming text in output files in the specified output 
  directory with the names of the files fitting the template: 
  "{prefix}{month}{daynum}{sec}{suffix}", where {prefix} is a 1 to 3
  character feed specifier suppied as a parameter to the program, {month} 
  is a one digit hexadecimal number corresponding to the month number for 
  the current year, {daynum} is a two digit hex number specifying the day 
  within the month, {sec} is a 5 digit hex number specifying the number 
  of seconds since midnight, and suffix is a lower case alphabetic character
  used to differentiate between files which were created within the same
  second. This naming convention insures that file names collate 
  alphebetically according to their time of receipt.

  The file currently in progress is has an upper case suffix, and is 
  renamed so that the suffix is in lower case name after it is completed.

Calling Sequence : 

  asynch {switches} 

	-b {bod_marker}	Beginning of document marker. This is specified as
			a hexadecimal number. The default bod marker is a
			0x1.

	-d device	Specifies name of device file for port. This is
			usually a file in "/dev".

	-e {eof_marker}	End of document marker. This is specified as a
			hexadicimal number. The default eod marker is a
			0x4.

	-l		Log initiation, status, termination and message 
			receipt. Normally, the log file is written to 
			standard error, and contains only fatal error 
			messages. If -l is specified, asynch will write 
			a log message for each recieved file in the format of:

		<time stamp>: received: <file name>, size: <size in bytes>

	-m		Use modem control signals for synchronization.

	-o directory	Specifies a directory in which files are to be
			put by asyncman.

	-p prefix	File name prefix.

        -s speed	Set port speed to specified baud rate. A default
			baud rate of 2400 is use.

	-t {minutes}	Optional timeout warning enable. If no messages are
			received in the specified time frame, a timeout
			warning is sent to the console, and put in the log
			file. This option is currently unimplemented.

	-w {directory}	Work directory. The input file currently being recieved
			is put in this directory and moved to the output
			directory once the document is completed. By default,
			the work directory is the directory from which
			the asynch program was initiated.

	-x		Use xon/xoff for synchronization.

	-7		Set port word length to 7 bits.

	-8		Set port word length to eight bits.

	-y {even | odd | none} Set parity to specified type

Notes : 

Change log : 
000	01-MAY-91  AMF	Created.
001	23-MAY-91  AMF  Put in exit handler. Log only errors unless "-l"
			option was specified. Put space between switches
			and their arguements. Put in work directory switch.
			Get rid of alphabetic file name suffix. Changed unique
			file name generation algorithm.
002	14-SEP-91  AMF	Put in comm port settings.
003	14-DEC-92  AMF	Compile under COHERENT.
004 04-AUG-97  AMF	Port to MS C++ 5.0 for Win32.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <fitsydef.h>
#include <time.h>
#if ! FIT_DOS
#include <termio.h>
#endif
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#if FIT_MSC
#include <io.h>
#include <windows.h>
#include <direct.h>
#endif

#if FIT_DOS
#define DIRSEP '\\'
#else
#define DIRSEP '/'
#endif

#define BUFMAX 128
#define BETWEENDOCS 0
#define ARGBUFL 255
#define INDOC 1

/* Globals */
char workspec[FIT_FULLFSPECLEN+1], progname[FIT_FULLFSPECLEN+1];
char logmsg[FIT_FULLFSPECLEN*3+1], argbuf[ARGBUFL+1], curpath[FIT_FULLFSPECLEN+1];
unsigned char inbuf[BUFMAX+1], buf[BUFMAX+1];
#if FIT_MSC
HANDLE dev_in;
#else
int dev_in, dev_out;
#endif
int logging, reset;
#if FIT_MSC
DWORD speed, bod, eod, modem, xon_xoff, parity, noparity, wordlength;
#else
struct termio curparms, newparms;
unsigned int speed, bod, eod, modem, xon_xoff, parity, noparity, wordlength;
#endif
int bufoff, endoff, state;

#if FIT_MSC
DCB commDCB;
COMMTIMEOUTS commtimeouts;
COMSTAT commstat;
DWORD errorcode, commerrorcode;
PVOID *lpMsgBuf;
#endif

FILE *out;

/* Declare functions */ 
void asynch_shutdown(int errnum);
int asynch_read();

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc,argv)
  int argc;
  char *argv[];
#endif
{

  char device[FIT_FULLFSPECLEN+1],outspec[FIT_FULLFSPECLEN+1],prefix[FIT_FULLFSPECLEN+1];
  char dummyspec[FIT_FULLFSPECLEN+1];

  int work_L,out_L,ilen,olen,doclen,secs;
  long timep;
  unsigned long idnum,previdnum;
  struct tm *ts;
#if FIT_MSC
  int setspeed = 0;
  int setxon_xoff = 0;
  int setwordlength = 0;
  int setparity = 0;
  int setnoparity = 0;
  strcpy(device,"COM1:");
#else
  strcpy(device,"tt:");
#endif

  outspec[0] = 0;
  workspec[0] = 0;
  prefix[0] = 0;
  bod = 1;
  eod = 4;
  logging = 0;
  idnum = 0;
  previdnum = 0;
  out = NULL;

  /* Modem setups */
  speed = 0;
  modem = 0;
  xon_xoff = 0;
  wordlength = 0;
  parity = 0; 
  noparity = 0;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  getcwd(curpath, FIT_FULLFSPECLEN);

  /**** Pull off switches *****/
  for (; argc; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch ((*argv)[1]) {
        case 'b' : case 'B' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing bod specifier\n");
	    goto SYNTAX;
	  }
	  sscanf(*(++argv),"%x", &bod);
	  if (!bod) {
	    fprintf(stderr,"*** Invalid bod specifier: %s\n", *argv);
	    goto SYNTAX;
	  }
	  break;
	case 'd' : case 'D' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing device specifer\n");
	    goto SYNTAX;
	  }
	  strcpy(device,*(++argv));
	  break;
        case 'e' : case 'E' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing eod specifier\n");
	    goto SYNTAX;
	  }
	  sscanf(*(++argv),"%x", &eod);
	  if (!eod) {
	    fprintf(stderr,"*** Invalid eod specifier: %s\n", *argv);
	    goto SYNTAX;
	  }
	  break;
	case 'o' : case 'O' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing output directory specifier\n");
	    goto SYNTAX;
	  }
	  strcpy(outspec,*(++argv));
	  break;
	case 'p' : case 'P' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing prefix specifier\n");
	    goto SYNTAX;
	  }
	  strcpy(prefix,*(++argv));
	  break;
	case 'l' : case 'L' :
	  logging = 1;
	  break;
	case 'm' : case 'M' :
	  modem = 1;
	  break;
	case 's' : case 'S' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing speed specifier\n");
	    goto SYNTAX;
	  }
	  sscanf(*(++argv), "%d", &speed);
	  switch (speed) {
#if !FIT_MSC
	    case 50 :
	      speed = B50; 
	      break;
	    case 75 :
	      speed = B75;
	      break;
	    case 110 :
	      speed = B110;
	      break;
	    case 134 :
	      speed = B134;
	      break;
	    case 150 :
	      speed = B150;
	      break;
	    case 200 :
	      speed = B200;
	      break;
	    case 300 :
	      speed = B300;
	      break;
	    case 600 :
	      speed = B600;
	      break;
	    case 1200 :
	      speed = B1200;
	      break;
	    case 1800 :
	      speed = B1800;
	      break;
	    case 2400 :
	      speed = B2400;
	      break;
	    case 4800 :
	      speed = B4800;
	      break;
	    case 9600 :
	      speed = B9600;
	      break;
	    case 19200 :
              speed = B19200;
	      /* speed = EXTA; */
	      break;
	    case 38400 :
              speed = B38400;
	      /* speed = EXTB; */
	      break;
            case 57600 :
              speed = B57600;
              break;
            case 115200 :
              speed = B115200;
              break;
            case 230400 :
              speed = B230400;
              break;
#if FIT_CYGWIN
            case 256000 :
              speed = B256000;
              break; 
#endif
#else
	    case 110 :
	      speed = CBR_110;
	      break;
	    case 300 :
	      speed = CBR_300;
	      break;
	    case 600 :
	      speed = CBR_600;
	      break;
	    case 1200 :
	      speed = CBR_1200;
	      break;
	    case 2400 :
	      speed = CBR_2400;
	      break;
	    case 4800 :
	      speed = CBR_4800;
	      break;
	    case 9600 :
	      speed = CBR_9600;
	      break;
            case 14400 :
              speed = CBR_14400;
              break;
	    case 19200 :
	      speed = CBR_19200;
	      break;
	    case 38400 :
	      speed = CBR_38400;
	      break;
            case 56000 :
              speed = CBR_56000;
              break;
            case 57600 :
              speed = CBR_57600;
              break;
            case 115200 :
              speed = CBR_115200;
              break;
            case CBR_128000 :
              speed = CBR_128000;
              break;
            case CBR_256000 :
              speed = CBR_256000;
              break;
#endif
	    default :
	      fprintf(stderr,"*** Invalid speed setting: %s/n",*argv);
	      goto SYNTAX;
	  }
#if FIT_MSC
	  setspeed = 1;
#endif
	  break;
	case 'w' : case 'W' :
	  if (!--argc) {
	    fprintf(stderr,"*** Missing work directory specifier\n");
	    goto SYNTAX;
	  }
	  strcpy(workspec,*(++argv));
	  break;
	case 'x' : case 'X' :
	  xon_xoff = 1;
#if FIT_MSC
	  setxon_xoff = 1;
#endif
	  break;
	case 'y' : case 'Y' :
	  if (!--argc) goto SYNTAX;
	  strncpy(argbuf, *(++argv), ARGBUFL);
	  argbuf[ARGBUFL] = 0;
	  fit_supcase(argbuf);
	  if (!strcmp(argbuf,"EVEN")) {
#if FIT_MSC
		parity = EVENPARITY;
#else
	    parity = 1;
#endif
	    noparity = 0;
	  }
	  else if (!strcmp(argbuf,"ODD"))
#if FIT_MSC
		parity = ODDPARITY;
#else
	    parity = PARODD+1;
#endif
	  else if (!strcmp(argbuf,"NONE")) {
	    noparity = 1;
#if FIT_MSC
		parity = NOPARITY;
#else
	    parity = 0;
#endif
	  }
	  else {
	    fprintf(stderr,"*** Invalid parity setting: %s\n", *argv);
	    goto SYNTAX;
	  }
#if FIT_MSC
	  setparity = 1;
#endif
	  break;
	case '7' :
#if FIT_MSC
	  wordlength = 7;
	  setwordlength = 1;
#else
	  wordlength = CS7;
#endif
	  break;
	case '8' :
#if FIT_MSC
	  wordlength = 8;
	  setwordlength = 1;
#else
	  wordlength = CS8;
#endif
	  break;
	default :
	  fprintf(stderr,"*** Invalid command option: %s\n",*argv);
	  goto SYNTAX;
      }
    }
    else {
      fprintf(stderr,"*** Invalid command option: %s\n",*argv);
      goto SYNTAX;
    }
  }

  /* Set file name prefix */
  fit_abspath(workspec, curpath, workspec);
  work_L = strlen(workspec);

  if (work_L) {
    if (workspec[work_L-1] != DIRSEP) {
      workspec[work_L-1] = DIRSEP;
      ++work_L;
    }
  }

  fit_abspath(outspec, curpath, outspec);
  out_L = strlen(outspec);
  if (out_L) {
    if (outspec[out_L-1] != DIRSEP) {
      outspec[out_L-1] = DIRSEP;
      ++out_L;
    }
  }

  if (*prefix) {
    strcpy(workspec+work_L,prefix);
    work_L += strlen(prefix);
    strcpy(outspec+out_L,prefix);
    out_L += strlen(prefix);
  }

  /* Open device for input */
#if FIT_MSC
  /* Open handle to comm device */
  if (!(dev_in = CreateFile("COM1:", GENERIC_READ | GENERIC_WRITE, (DWORD) 0,
      (LPSECURITY_ATTRIBUTES) NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
      (HANDLE) NULL))) {
    perror("*** Error opening device for input");
    exit(errno); 
  }

  /* Retrieve current state, will use current values for defaults */
  if (!GetCommState(dev_in, &commDCB)) {
    perror("*** Error retrieving input device state");
    exit(errno);
  }

  /* Set new comm configuration based on switches on command line */
  commDCB.DCBlength = sizeof(DCB);			/* Must be set to this */
  if (setspeed) commDCB.BaudRate = speed;	/* Line speed */
  commDCB.fBinary = TRUE;					/* Always do binary I/O */
  commDCB.fParity = FALSE;					/* Don't crap out on parity errors */
  if (modem) {								
	/* Enable CTS/RTS modem control */
    commDCB.fOutxCtsFlow = TRUE;
	commDCB.fRtsControl = RTS_CONTROL_HANDSHAKE;
  }
  else {
	/* Disable CTS monitoring for output control and keep RTS high just
	   in case modem needs it */
	commDCB.fOutxCtsFlow = FALSE;			
	commDCB.fRtsControl = RTS_CONTROL_ENABLE;
  }
  /* Set other comm control lines so that they will work no matter what */
  commDCB.fOutxDsrFlow = FALSE;				/* Ignore DSR toggle from comm device */
  commDCB.fDtrControl = DTR_CONTROL_ENABLE; /* Keep DTR high in case comm device wants it */
  commDCB.fDsrSensitivity = FALSE;			/* Ignore DSR hign */
  commDCB.fTXContinueOnXoff = TRUE;			/* Keep sending data even sending an XOFF */
  if (setxon_xoff) {
	/* Set up xon/xoff flow control */
    commDCB.fOutX = TRUE;					/* Outgoing xon/xoff control enabled */
    commDCB.fInX = TRUE;				    /* incoming xon/xoff control enabled */
    commDCB.XonChar = (char) (('q' - 'a') + 1);	/* Xon char set to control-q */
    commDCB.XoffChar = (char) (('s' - 'a') + 1);/* Xoff char set to control-s */
	commDCB.fOutxCtsFlow = FALSE;				/* Disable hardware flow control */
	commDCB.fRtsControl = RTS_CONTROL_ENABLE;	/* Disable hardware flow control */
  }

  commDCB.fErrorChar = FALSE;					/* Don't substitute for bad chars */
  commDCB.fNull = FALSE;						/* Don't trash incoming nulls */
  commDCB.fAbortOnError = FALSE;				/* Don't abort on errors */
  commDCB.XonLim = BUFMAX / 8;					/* Set xon transmit to 1/8 application input buffer size */
  commDCB.XoffLim= BUFMAX;						/* Set xoff transmit to size of application input buffer size */
  if (setwordlength) commDCB.ByteSize = (BYTE) wordlength; /*Set word length (byte size) */
  if (setparity) commDCB.Parity = (BYTE) parity;	/* Set parity */

  /* Use new comm settings */
  if (!SetCommState(dev_in, &commDCB)) {
	perror("*** Error setting input device state");
    exit(errno);
  }

  /* Set device driver buffer sizes to 4 times the size of the application buffer */
  if (!SetupComm(dev_in, (DWORD) (BUFMAX * 4), (DWORD) (BUFMAX * 4))) {
    perror("*** Error setting input device buffer sizes");
	exit(errno);
  }

  /* Get current read and write timeout values (no really reused, just here in
     case we want to preserve them at some future time */
  if (!GetCommTimeouts(dev_in, &commtimeouts)) {
	perror("*** Error retrieving input device timeout values");
	exit(errno);
  }

  /* Set read and write timeout values. A read will time out if there is more than
     a .2 second gap between incoming characters. Writes will never time out */
  commtimeouts.ReadIntervalTimeout = (DWORD) 200; /* max of .2 second gap between characters */
  commtimeouts.ReadTotalTimeoutMultiplier = (DWORD) 0; /* no maximum read timeout of all character gaps */
  commtimeouts.ReadTotalTimeoutConstant = (DWORD) 0; /* No total read timeout */
  commtimeouts.WriteTotalTimeoutMultiplier = (DWORD) 0; /* No maximum write timeout for all character gaps */
  commtimeouts.WriteTotalTimeoutConstant = (DWORD) 0; /* No total write timeout */

  /* Use the new timeout values */
  if (!SetCommTimeouts(dev_in, &commtimeouts)) {
	perror("*** Error setting input device timeout values");
	exit(errno);
  }

  /* Clear out the device input and output buffers */
  if (!PurgeComm(dev_in, (DWORD) (PURGE_TXCLEAR | PURGE_RXCLEAR))) {
	perror("*** Error purging device input buffer");
	exit(errno);
  }
#else
  if ((dev_in = open(device, O_RDONLY)) < 0) { 
    perror("*** Error opening device for input");
    exit(errno); 
  }

  if ((dev_out = open(device, O_WRONLY)) < 0) { 
    perror("*** Error opening device for output");
    exit(errno); 
  }

  if (ioctl(dev_in, TCGETA, &curparms) == -1) {
    perror("*** Error retreiving device status info");
    exit(errno);
  }

  newparms = curparms;

  /* Turn off all input interpetation, parity error detection, break
     detection */
  newparms.c_iflag = IGNBRK | IGNPAR; 
  newparms.c_oflag = 0;

  if (xon_xoff)
    newparms.c_iflag |=  IXOFF;

  /* Set control parameters */
  newparms.c_cflag |= CREAD;

  if (wordlength) { 
    /* Word length */
    newparms.c_cflag &= ~(CS6 | CS7 | CS8);
    newparms.c_cflag |= wordlength;
  }
  if (speed) {
    /* Speed */
    newparms.c_cflag &= ~(B50 | B75 | B110 | B134 | B150 | B200 | B300 |
	B600 | B1200 | B1800 | B2400 | B4800 | B9600 | B19200 | B38400 |
	B57600 | B115200 | B230400 
#if FIT_CYGWIN
        | B256000
#endif
    );
    newparms.c_cflag |= speed;
  }
  if (parity)
    newparms.c_cflag |= (PARENB | (parity-1));
  else if (noparity)
    newparms.c_cflag &= ~(PARENB);

  if (modem) {
    newparms.c_cflag &= ~CLOCAL;
#if !FIT_SUN && !FIT_COHERENT && !FIT_LINUX
    if (!xon_xoff)
      newparms.c_cflag |= (CTSFLOW | RTSFLOW);
#endif
  }
  newparms.c_lflag = 0;
  newparms.c_line = 0;

  /* Make reads ask for BUFMAX characters, timing out if intercharacter
     time gap exceeds 5 seconds */
  newparms.c_cc[VMIN] = BUFMAX;
  newparms.c_cc[VTIME] = 50;

  /* Don't really know why POSIX has these two parameters as they seem to
     replicate the c_cflag speed settings, so I leave them commented out
     for now */
/*************
  newparms.c_ispeed = newparms.c_cflag & 017;
  newparms.c_ospeed = newparms.c_ispeed;
*************/

  /* Set up the comm port with the new parameters */
  if (ioctl(dev_in, TCSETA, &newparms) == -1) {
    fit_logmsg(stderr,"*** Error setting device characteristics", 1);
    goto ERREXIT;
  }
#endif

  if (logging) {
    sprintf(logmsg,"**** %s initiated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    fprintf(stderr,"  Input device: %s\n",device);
    fprintf(stderr,"  File name template: %s\n",workspec);
  }

  signal(SIGTERM,asynch_shutdown);
  signal(SIGINT,asynch_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGQUIT,asynch_shutdown);
  signal(SIGPIPE,asynch_shutdown);
#endif

  /* Set buffer offset */
  bufoff = 0;
  endoff = 0;
  state = BETWEENDOCS;
  reset = 0;

  while (1) {

    if (reset) {
    /* I/O error on previous document, reset the port */
#if FIT_MSC
		/* Under MS C++ / Win 32, we will use the ClearCommError function
		   which will hopefully recover from any error on the device. It
		   also give us an error code for use in our log message */
		if (ClearCommError(dev_in, &commerrorcode, &commstat)) {
		  if (commerrorcode & CE_BREAK) 
			  fit_logmsg(stderr, "Break detected device", 1);
		  if (commerrorcode & CE_FRAME) 
			  fit_logmsg(stderr, "Framing error on device", 1);
		  if (commerrorcode & CE_IOE) 
			  fit_logmsg(stderr, "I/O error on device", 1);
		  if (commerrorcode & CE_MODE) 
			  fit_logmsg(stderr, "Unsupported request mode on device", 1);
		  if (commerrorcode & CE_OVERRUN)
			  fit_logmsg(stderr, "Character-buffer overrun on device", 1);
		  if (commerrorcode & CE_RXOVER)
			  fit_logmsg(stderr, "Input buffer overflow for device", 1);
		  if (commerrorcode & CE_RXPARITY)
			  fit_logmsg(stderr, "Parity error on device", 1);
		  if (commerrorcode & CE_TXFULL)
			  fit_logmsg(stderr, "Output buffer overflow on device", 1);
		  if (!(commerrorcode & (CE_BREAK | CE_FRAME | CE_IOE | CE_MODE |
		    CE_MODE | CE_OVERRUN | CE_RXOVER | CE_RXPARITY | CE_TXFULL)))
			  fit_logmsg(stderr, "Unidentified error on device",1);
		}
		else {
			fit_logmsg(stderr, "*** Error resetting device", 1);
			goto ERREXIT;
		}
#else
      /* Under UNIX, there is not much to do after an error other than
		 re-opening the port and resetting it's configuration */
      close(dev_in);
      close(dev_out);
      /* Open device for input */
      if ((dev_in = open(device, O_RDONLY)) < 0) { 
        fit_logmsg(stderr,"*** Error re-opening device for input during reset",
	  1);
	goto ERREXIT;
      }

      if ((dev_out = open(device, O_WRONLY)) < 0) { 
        fit_logmsg(stderr,"*** Error re-opening device for output during reset",
	  1);
	goto ERREXIT;	
      }
      /* Set up the comm port */
      if (ioctl(dev_in, TCSETA, &newparms) == -1) {
	fit_logmsg(stderr,
	  "*** Error setting device characteristics during reset",1);
        goto ERREXIT;
      }
#endif
      reset = 0;
      state = BETWEENDOCS;
      bufoff = 0;
      endoff = 0;
    }

    /* Set name of next file to create */
    time(&timep);
    ts = localtime(&timep);
    sprintf(&workspec[work_L],"%x",ts->tm_mon+1);
    sprintf(&workspec[work_L+1],"%02x",ts->tm_mday);
    secs = ts->tm_hour * 3600 + ts->tm_min * 60 + ts->tm_sec;
    sprintf(&workspec[work_L+3],"%05x",secs);
    workspec[work_L+8] = 0;

    sscanf(&workspec[work_L],"%lx",&idnum);

    if (idnum <= previdnum) {
      idnum = previdnum + 1;
      sprintf(&workspec[work_L],"%08lx",idnum);
    }
    previdnum = idnum;

    /* Open temp file for output */
#if !FIT_DOS
    out = fopen(workspec,"w");
#else
    out = fopen(workspec,"wb");
#endif
    if (out == NULL) {
      sprintf(logmsg,"Error opening output file: %s",workspec);
      fit_logmsg(stderr,logmsg,1);
      goto ERREXIT;
    }

    doclen = 0;

    /* Copy the input file to the output file */
    while (1) {
      ilen = asynch_read();
      if ((reset = (ilen < 0)) || !ilen) break;
      doclen += ilen;
      if (ilen) {
	olen = fwrite(buf,1,ilen,out);
	if (olen != ilen) {
          sprintf(logmsg,"Write error on: %s\n",workspec);
	  fit_logmsg(stderr,logmsg,1);
	  goto ERREXIT;
	}
      }
    }

    if (reset) {
	  /* Error on input comm device, log error message */
#if FIT_MSC
      errorcode = GetLastError();
      FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
          NULL, errorcode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
          &lpMsgBuf, 0, NULL);
	  sprintf(logmsg,"I/O error during processing of: %s - ", workspec);
	  strncat(logmsg, *lpMsgBuf, FIT_FULLFSPECLEN*3);
	  logmsg[FIT_FULLFSPECLEN*3+1] = (char) 0;
	  fit_logmsg(stderr, logmsg, 1);
      // Free the buffer.
      LocalFree( lpMsgBuf );
#else
	  sprintf(logmsg,"I/O error during processing of: %s\n", workspec);
      fit_logmsg(stderr, logmsg, 1);
      perror("");
#endif
    }

	/* Done with current incoming document, close file in
	   work directory and then rename it into the spool directory */
    fclose(out);

    if (doclen) {

      strcpy(outspec+out_L,workspec+work_L);
      fit_frename(workspec,outspec);

      if (logging) {
	sprintf(logmsg,"received: %s, size: %d",outspec,doclen);
	fit_logmsg(stderr,logmsg,1);
      }
    }
    else if (logging) {
      fit_logmsg(stderr,"received zero length document",1);
      fit_fdelete(workspec);
    }
  }

  /* Only get here on a syntax error on the command line, print
     help message */
SYNTAX:
  fprintf(stderr,"Usage: %s {switches}\n",progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -b bod\tSet beginning of document delimiter.\n");
  fprintf(stderr,"  -d device\tSet input device name.\n");
  fprintf(stderr,"  -e eod\tSet end of document delimiter.\n");
  fprintf(stderr,"  -l\t\tEnable document receipt logging.\n");
  fprintf(stderr,"  -m\t\tEnable modem control lines and hardware sync.\n");
  fprintf(stderr,"  -o directory\tSet output directory.\n");
  fprintf(stderr,"  -p prefix\tSet file name prefix.\n");
  fprintf(stderr,"  -s speed\tSet port speed.\n");
  fprintf(stderr,"  -t minutes\t*Set inactive warning timeout.\n");
  fprintf(stderr,"  -w directory\tSet directory for work files.\n");
  fprintf(stderr,"  -x\t\tEnable xon/xoff control flow.\n");
  fprintf(stderr,
	"  -y parity\tSet parity to \"odd\", \"even\", or \"none\".\n");
  fprintf(stderr,"  -7\t\t7 bit word length\n");
  fprintf(stderr,"  -8\t\t8 bit word length\n");
  fprintf(stderr,"Note: Items preceeded by \"*\" are unimplemented.\n");
  return 0;

ERREXIT:
  /* Die with error message on unrecoverable error */
  perror("");
  asynch_shutdown(errno);
}


void asynch_shutdown(int errnum)
{
  /* Shutdown function called when an interrupt is sent to the program. It
     will trie to shutdown gracefully. */
  int stat;

  stat = errno;

  if (out != NULL) 
    fclose(out);
  fit_fdelete(workspec);

  if (logging) {
    sprintf(logmsg,"**** %s terminated ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }

#if !FIT_MSC
  /* Under UNIX we set the comm device back to it's old state. Under
     MSC / Win 32 we can't do this as the driver will hang until the outstanding
	 read completes, which could be forever. */
  if (dev_in >= 0) 
    ioctl(dev_in, TCSETA, &curparms);
#endif
  exit(stat);
}


/* Function to put next bit of document text into buffer, it will return
   a value of 0 on end of file marker found, and a value of -1 on an
   input error */
int asynch_read() {

  int nbytes;
  char *bodptr, *eodptr;

  nbytes = 0;

  while (state == BETWEENDOCS || !nbytes) {
    if (bufoff >= endoff) {
      /* Buffer is empty, do a read */
#if FIT_MSC
	  if (!ReadFile(dev_in, inbuf, (DWORD) BUFMAX, (DWORD *) &nbytes, NULL))
		nbytes = 0;
#else
      nbytes = read(dev_in, inbuf, BUFMAX);
#endif
      /* Should never get a zero byte read */
      if (!nbytes) return -1;

      /* Set up offsets */
      bufoff = 0;
      endoff = nbytes;
      inbuf[endoff] = 0;
    }
    else
      /* Leftover data in buffer, process it */
      nbytes = endoff - bufoff;

    if (nbytes && state == BETWEENDOCS) {  
      /* Look for bod */
      if ((bodptr = strchr(inbuf+bufoff, bod)) != NULL) {
	/* Found bod, set offsets for location of it */ 
	state = INDOC;
	bufoff = bodptr - (char *) inbuf + 1;
        nbytes = endoff - bufoff;
      }
      else
	/* Didn't find BOD, force read on next pass */
	bufoff = endoff;
    }

    if (nbytes && state == INDOC) {
      /* Have some text, check for eod in text */
      if ((eodptr = strchr(inbuf+bufoff, eod)) != NULL) {
	/* found eod, copy any preceeding text to output buffer */
	if ((nbytes = eodptr - (char *) (inbuf + bufoff))) {
	  strncpy(buf, inbuf + bufoff, nbytes);
	  bufoff += nbytes;
	}
	else {
	  /* no text preceeding EOF, skip over it */
	  ++bufoff;
	  if (bod != eod)
	    /* bod is different from eod, set state to BETWEENDOCS */
	    state =BETWEENDOCS;
	  /* Return EOF marker */
	  return 0;
	}
      }
      else {
	/* No eod, copy remaining part of input buffer to output buffer */
	strncpy(buf, inbuf + bufoff, nbytes);
	bufoff += nbytes;
      }
      if (nbytes)
	return nbytes;
    }
  }
}
