/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : makemsgq						makemsgq.c

Function : This program creates a message queue.

Author : A. Fregly

Abstract : This program is used to create a message queue. Normally, message
	queues are created by TIPS daemons, but this utility allows for the
	creation of message queues for testing or system initialization
	purposes.

Calling Sequence : makemsgq {switches} file_spec

  Switches:
	-p	Protection mask for message queue as an octal number. The
		default protection is 0666, which allows all users on the
		system read and write access to the message queue.

  Parameters:
  	file_spec	File spec used for message queue.


Notes: 

Change log : 
000	17-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_UNIX
#include <sys/ipc.h>
#include <sys/msg.h>
#endif
#include <tips.h>

extern int errno;


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  key_t msgkey;
  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int msgqid, prot;

  prot = 0666;

  /* Set program name */
  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname,
        dummyspec, dummyspec);
  --argc;

  if (!argc || (*argv)[0] == '?') goto SYNTAX;

  for (; argc > 1; --argc, ++argv) {
    if ((*argv)[0] == '-' && ((*argv)[1] == 'p' || (*argv)[1] == 'P')) {
      --argc;
      ++argv;
      sscanf(*argv, "%od", &prot);
    }
    else
      goto SYNTAX;
  }

  if (!argc) goto SYNTAX;

  /* Create the message queue */
  msgkey = ftok(*argv, TIPS_FTOKSEED);
  if ((msgqid = msgget(msgkey, IPC_CREAT | IPC_EXCL | prot)) < 0) {
    perror("*** Unable to create message queue");
    exit(errno);
  }
  return 0;

SYNTAX:
  /* Syntax error or no command line switches or parameters or help
     requested. Write usage to standard error */
  fprintf(stderr,"Usage: %s file_spec\n", progname);
  fprintf(stderr,"Parameter:\n");
  fprintf(stderr,"  file_spec\tFile name for message queue\n");
  return 0;
}
