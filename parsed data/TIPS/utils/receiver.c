/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : receiver                                          receiver.c

Function : Polls document input directory, copying qualifying files to stdout.

Author : A. Fregly

Abstract : This program is used as a front end for the further processing
	of files which have been placed in a spool directory by an input
	spooler process (such as asynchman). It will periodically poll the
	directory, copying qualifying files to stdout and moving them
	after they are copied to an archive directory.

Calling Sequence : receiver {-i {input_dir}} {-l} {-o {outputdir}}
	{-p {prefix{,prefix...}}} {-r {seconds}} {-t {tag}}

	-i      Input directory. The default is the current default.

	-l      Write initiation, termination, status, and message processing
		entries to log file. By default, a receiver writes only fatal
		error messages to stderr. If this option is set, a time stamped
		entry is written for each file processed by the receiver.

	-o      Output directory in which processed files are to be placed.

	-p      List of file name prefixes for files to be received. Files
		matching the template {prefix}* are processed. For
		example, a prefix of "-pap,upi" would match on files:

		ap50501a46
		upi5051a03

		if a prefix is not specified, all files placed in the 
		input directory will be matched.

	-r      Rate at which the input directory should be polled. The
		default rate is 60 so that the input directory is polled
		once a minute. If rate is 0, then receiver will make only
		a single pass before exiting.

	-t      Tag to be used in labeling document in output to stdout.
	
Notes : 

Change log : 
000     05-MAY-91  AMF  Created.
001     23-MAY-91  AMF  Catch termination signals and shut down gracefully.
			Put space between flags and their aguements. Match
			on all files with "prefix" instead of just those
			ending with a lower case letter.
002     16-SEP-91  AMF  Set file protections to u+rw, go+r for output files.
003     15-DEC-92  AMF  Compile under COHERENT.
004     22-JAN-04  AMF  Find all files if prefix is not specified
******************************************************************************/
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_MSC
#include <direct.h>
#endif

extern int errno;

#if FIT_UNIX
#if !FIT_COHERENT
#include <unistd.h>
#else
#include <dirent.h>
#endif
#endif

#if FIT_TURBOC
#define BUFMAX 512
#else
#define BUFMAX 30720
#if FIT_COHERENT
#define S_IRGRP 040
#define S_IROTH 04
#endif
#endif
#define MAXPREFIX 50
#if !FIT_DOS
#define READ "r"
#else
#define READ "rb"
#endif

static char searchspec[FIT_FULLFSPECLEN+1], fname[FIT_FULLFSPECLEN+1];
static char inspec[FIT_FULLFSPECLEN+1], outspec[FIT_FULLFSPECLEN+1];
static char dummy[FIT_FULLFSPECLEN+1], curdir[FIT_FULLFSPECLEN+1];
static char ext[FIT_FULLFSPECLEN+1];
static char prefixlist[FIT_FULLFSPECLEN+1], tag[FIT_FULLFSPECLEN+1];
static char logmsg[FIT_FULLFSPECLEN+50],progname[FIT_FULLFSPECLEN+1];
static char *prefix[MAXPREFIX], prefixspec[FIT_FULLFSPECLEN+1], *remaininglist;
static short prefix_L[MAXPREFIX];
static char buf[BUFMAX+1];
static int shutdown;

#if FIT_UNIX || FIT_COHERENT
#define DIRSEP '/'
#define NORMAL_FILE S_IFREG
#else
#define DIRSEP '\\'
#define NORMAL_FILE 0
#endif


void receiver_shutdown(int errnum);

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc,argv)
  int argc;
  char *argv[];
#endif
{

  int i,ss,ilen,olen,doclen,log,rate,nprefixes,in_L,out_L;
  long filelen;
  struct stat statbuf;
  FILE *in;
  char *context;

#if FIT_DOS
  fit_binmode(stdout);
#endif

  searchspec[0] = '.';
  searchspec[1] = DIRSEP;
  searchspec[2] = 0;
  in_L = 2;
  outspec[0] = 0;
  prefixlist[0] = 0;
  tag[0] = 0;
  nprefixes = 0;
  rate = 60;
  log = 0;
  out_L = 0;
  shutdown = 0;
  getcwd(curdir, FIT_FULLFSPECLEN);

  fit_prsfspec(*(argv++),dummy,dummy,dummy,progname,dummy,dummy);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  /**** Pull off switches *****/
  for (; argc; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch ((*argv)[1]) {
	case 'i' : case 'I' :
	  --argc;
	  ++argv;
	  if (argc) {
	    fit_abspath(*argv, curdir, searchspec);
	    in_L = strlen(searchspec);
	  }
	  break;
	case 'o' : case 'O' :
	  --argc;
	  ++argv;
	  if (argc) {
	    fit_abspath(*argv, curdir, outspec);
	    out_L = strlen(outspec);
	  }
	  break;
	case 'p' : case 'P' :
	  --argc;
	  ++argv;
	  if (argc) {
	    strcpy(prefixlist,*argv);
	    for (remaininglist = fit_nextarg(prefixlist,prefixspec); 
		*prefixspec; 
		remaininglist = fit_nextarg(remaininglist,prefixspec)) {
	      if (nprefixes < MAXPREFIX) {
		prefix[nprefixes] = (char *) malloc(strlen(prefixspec)+4);
		if (prefix[nprefixes] == NULL) goto ALLOCERR;
		strcpy(prefix[nprefixes],prefixspec);
		strcat(prefix[nprefixes], "*.*");
		prefix_L[nprefixes] = strlen(prefix[nprefixes]);
		++nprefixes;
	      }
	      if (remaininglist == NULL || !*remaininglist) break;
	    }
	  }
	  break;
	case 'l' : case 'L' :
	  log = 1;
	  break;
	case 'r' : case 'R' :
	  --argc;
	  ++argv;
	  if (argc)
	    sscanf(*argv,"%d",&rate);
	  break;
	case 't' : case 'T' :
	  --argc;
	  ++argv;
	  if (argc)
	    strcpy(tag,*argv+2);
	  break;
	default :
	  fprintf(stderr,"Invalid command option: %s\n",*argv);
	  goto DONE;
      }
    }
    else
      fprintf(stderr,"Invalid command option: %s\n",*argv);
  }

  if (nprefixes == 0) {
    prefix[0]="*.*";
    prefix_L[0]=strlen(prefix[0]);
    nprefixes=1;
  }

  if (log) {
    /* Write initiation log messages */
    sprintf(logmsg,"**** %s initiated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    fprintf(stderr,"  Input directory:  %s\n",searchspec);
    fprintf(stderr,"  Output directory: %s\n",outspec);
    fprintf(stderr,"  Prefixes: ");
    for (i=0; i < nprefixes; ++i) {
      if (i) fputc(',',stderr);
      fputs(prefix[i],stderr);
    }
    fputc('\n',stderr); 
    fprintf(stderr,"  Poll interval: %d seconds\n",rate);
    if (*tag) fprintf(stderr,"  Tag: %s\n",tag);
  }

  signal(SIGTERM,receiver_shutdown);
  signal(SIGINT,receiver_shutdown);
#if FIT_UNIX || FIT_COHERENT  
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE,receiver_shutdown);
  signal(SIGQUIT,receiver_shutdown);
#endif
  
  if (searchspec[in_L-1] != DIRSEP) {
    searchspec[in_L++] = DIRSEP;
    searchspec[in_L] = 0;
  }

  while (!shutdown) {
    for (i=0; i < nprefixes; ++i) {
      searchspec[in_L] = 0;
      strcat(searchspec, prefix[i]);
      for (context = NULL; *fit_findfile((void *(*)) &context, searchspec, NORMAL_FILE,
		"", curdir, inspec);) {
	if ((in = fopen(inspec, READ)) == NULL) {
	  sprintf(logmsg,"Error opening input file: %s", inspec);
	  fit_logmsg(stderr,logmsg,1);
	  goto ERREXIT;
	}

	fit_prsfspec(inspec, dummy, dummy, dummy,outspec+out_L, ext, dummy);
	strcat(outspec, ext);
	strcpy(fname, outspec+out_L);

	doclen = 0;

	/* Write file description to standard output */
	fstat(fileno(in), &statbuf);
	filelen = statbuf.st_size;
	printf("%s,%ld",outspec,filelen);
	if (*tag) printf(",%s",tag);
	putchar('\n');

	/* Copy the input file to the output file */
	while (!feof(in)) {
	  ilen = fread(buf,1,BUFMAX,in);
	  doclen += ilen;
	  if (ilen) {
	    olen = fwrite(buf,1,ilen,stdout);
	    if (olen != ilen) {
	      fit_logmsg(stderr,"Write error on stdout",1);
	      goto ERREXIT;
	    }
	  }
	  else if (!feof(in)) {
	    fit_logmsg(stderr,"Error reading input file\n",1);
	    goto ERREXIT;
	  }
	}

	fclose(in);

	if (log) {
	  sprintf(logmsg,"processed: %s, size: %d",fname, doclen);
	  fit_logmsg(stderr,logmsg,1);
	}
	ss = fit_frename(inspec,outspec);
	if (!ss) {
	  fit_logmsg(stderr,"Error moving file to output directory",1);
	  goto ERREXIT;
	}
	if (!doclen && log)
	  fit_logmsg(stderr,"Processed zero length file",1);
	/* Set protection on file to u+rw, g+r */
#if FIT_UNIX || FIT_COHERENT         
	chmod(outspec, S_IREAD | S_IWRITE | S_IRGRP | S_IROTH);
#endif        
	fflush(stdout);
	fflush(stderr);
      }
    }
    if (!shutdown)
#if !FIT_MSC
      sleep((unsigned) rate);
#else
	  _sleep((unsigned) rate);
#endif
    shutdown = shutdown || !rate;
  }
  goto DONE;

SYNTAX:
  fprintf(stderr,"Usage: %s {switches}\n",progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -i directory\tSet input directory.\n");
  fprintf(stderr,"  -l\t\tEnable logging.\n");
  fprintf(stderr,"  -o directory\tSet output directory.\n");
  fprintf(stderr,"  -p prefixes\tSpecify list of input file prefixes.\n");
  fprintf(stderr,"  -r seconds\tPolling interval, 0 for single pass.\n");
  fprintf(stderr,"  -t tag\tTag to use in document key field\n");
  return 0;

ALLOCERR:
  fit_logmsg(stderr,"Memory allocation error\n",1);

ERREXIT:
  perror("");

DONE:
  if (log) {
    sprintf(logmsg,"**** %s terminated ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  return errno;
}

void receiver_shutdown(int errnum)
{
  shutdown = 1;
}
