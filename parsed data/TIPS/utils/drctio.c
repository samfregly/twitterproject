/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drctio                                            drctio.c

Function : Provides command line access to direct access files.

Author : A. Fregly

Abstract : This function provides a full suite of functions for accessing
	a direct access file including: file creation, reading. writing,
	updating, file deletion, record deletion.


Calling Sequence : drctio {switches} direct_file_spec

  switches:

    -a          Append file specified by the -f switch.
    -c          Create direct access file.
    -d          Delete record(s) specified by the -r switch.
    -f file     Specifies file for the -a, -h, -u, -w, -x, and -z switches.
    -h {{s:}e}  Extract document headers, where the header consists of the
		range of lines specified by s:e. If s is not supplied, it
		defaults to 1. If e is not specified, it also defaults to
		1.
    -i          Display status information for direct access file.
    -o          Add record specified by the  -f switch, reusing space occupied
		by deleted records if possible.
    -p mask     Octal protection mask to be applied to created file. By default,
		a value giving the owner read/write access, group read access,
		and others no access is used.
    -r n{:m}    Specifies record or range of records for -d, -u, -w,
		and -x switches. A trailing specifier of "-1" is used
		to specify through the end of the file.
    -s          Send records specified by the -r switch to the direct access
		file specified by the -f switch.
    -t seconds  Sets read and write timeout to the specified number of
		seconds. If seconds is set to -1, no timeout is used.
    -u          Update the record specified by the -r switch with contents
		of the file specified by the -f switch.
    -v          Verify file structure.
    -w          Write into the direct access file the file specefied by
		the -f switch.
    -x          Extract the record(s) specified by the -r switch, writing
		them to the file specified by the -f switch.
    -z          Zap (delete) direct access file.


Notes:

Change log :
000  03-JUL-91  AMF  Created.
001  09-JUL-97  AMF  Use drct_sizeinfo to produce better information for -i 
					 switch.
******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <fcntl.h>
#include <drct.h>
#include <sys/stat.h>

#define K_APPEND        1
#define K_CREATE        2
#define K_DELREC        3
#define K_INFO          4
#define K_WRITE         5
#define K_EXTRACT       6
#define K_ZAP           7
#define K_VERIFY        8
#define K_UPDATE        9
#define K_SEND          10
#define K_OVERWRITE     11
#define K_BUFLEN        4096

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  char progname[FIT_FULLFSPECLEN+1], fspec[FIT_FULLFSPECLEN+1];
  char dummyspec[FIT_FULLFSPECLEN+1], drctspec[FIT_FULLFSPECLEN+1];
  char dataspec[FIT_FULLFSPECLEN+1];
  char buf[K_BUFLEN+1];
  struct drct_s_file *drcthandle, *sendhandle;
  int operation, stat, timeout, oflags, nbytes, len, n;
  unsigned prot;
  long headerstart, headerend, nlines, curline;
  char *sptr, *lineptr, *endline;
  long startrec, endrec, doclen, outlen, currec, inlen;
  long numutilized, utilizedsize, numallocated, allocatedsize;
  long numfragments, fragmentsize;
  FILE *datafile, *tempfile;
  struct drct_s_info drctinfo;
  struct stat statbuf;


  /* Initialize variables */
  stat = 1;
  operation = 0;
  fspec[0] = 0;
  drctspec[0] = 0;
  startrec = -1;
  endrec = -1;
  timeout = -1;
  datafile = NULL;
  tempfile = NULL;
  drcthandle = NULL;
  sendhandle = NULL;
  headerstart = 0;
  headerend = 0;
  prot = 0;

  strcpy(progname, *(argv++));
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  /* Pick off command arguements */
  for (; argc > 1; --argc, ++argv) {
    if ((*argv)[0] != '-') goto SYNTAX_ERROR;

    switch ((*argv)[1]) {
      case 'a' : case 'A' :
	operation = K_APPEND;
	break;
      case 'c' : case 'C' :
	operation = K_CREATE;
	break;
      case 'd' : case 'D' :
	operation = K_DELREC;
	break;
      case 'f' : case 'F' :
	--argc;
	if (!argc) goto SYNTAX_ERROR;
	strcpy(fspec, *(++argv));
	break;
      case 'h' : case 'H' :
	if (argc > 2 && *(argv+1)[0] != '-') {
	  --argc;
	  ++argv;
	  sptr = strchr(*argv, ':');
	  if (sptr != NULL) *sptr = 0;
	  sscanf(*argv, "%ld", &headerend);
	  if (sptr != NULL) {
	    headerstart = headerend-1;
	    sscanf(&sptr[1], "%ld", &headerend);
	  }
	  else
	    headerstart = 0;
	}
	else {
	  headerstart = 0;
	  headerend = 1;
	}
	operation = K_EXTRACT;
	break;
      case 'i' : case 'I' :
	operation = K_INFO;
	break;
      case 'o' : case 'O' :
	operation = K_OVERWRITE;
	break;
      case 'p' : case 'P' :
	++argv;
	--argc;
	if (!argc || *(argv)[0] == '-') goto SYNTAX_ERROR;
	sscanf(*argv, "%o", &prot);
	break;
      case 'r' : case 'R' :
	++argv;
	--argc;
	if (!argc) goto SYNTAX_ERROR;

	sptr = strchr(*argv, ':');
	if (sptr != NULL) *sptr = 0;
	sscanf(*argv, "%ld", &startrec);
	--startrec;
	if (sptr != NULL) {
	  sscanf(&sptr[1], "%ld", &endrec);
	  if (endrec > 0) --endrec;
	}
	else
	  endrec = startrec;
	break;
      case 's' : case 'S' :
	operation = K_SEND;
	break;
      case 't' : case 'T' :
	--argc;
	++argv;
	if (!argc) goto SYNTAX_ERROR;
	sscanf(*argv, "%d", &timeout);
	break;
      case 'u' : case 'U' :
	operation = K_UPDATE;
	break;
      case 'v' : case 'V' :
	operation = K_VERIFY;
	break;
      case 'w' : case 'W' :
	operation = K_WRITE;
	break;
      case 'x' : case 'X' :
	operation = K_EXTRACT;
	break;
      case 'z' : case 'Z' :
	operation = K_ZAP;
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  if (argc > 1) goto SYNTAX_ERROR;
  if (!argc) goto SYNTAX;

  strcpy(drctspec, *argv);
  strcpy(dataspec, *argv);

  if (!operation) {
    printf("*** Invalid or missing operation switch/n");
    goto SYNTAX;
  }

  if (startrec < 0) {
    if (operation == K_DELREC || operation == K_UPDATE) {
      printf("*** Missing or invalid record specifier\n");
      goto SYNTAX;
    }
    else if (operation == K_EXTRACT || operation == K_SEND)
      startrec = 0;
  }

  if (startrec >= 0 && endrec < startrec && endrec != -1) {
    printf("*** Invalid record range\n");
    goto SYNTAX;
  }

  /* Open direct access file */
  if (operation == K_CREATE)
    oflags = O_RDWR + O_CREAT;
  else if (operation == K_APPEND || operation == K_DELREC ||
      operation == K_UPDATE || operation == K_WRITE || operation == K_INFO ||
      operation == K_OVERWRITE)
    oflags = O_RDWR;
  else 
    oflags = O_RDONLY;

  if ((drcthandle = drct_open(drctspec, oflags, prot, timeout, timeout)) == 
	NULL) {
    perror("*** Error opening direct access file");
    goto DONE;
  }

  if (operation == K_APPEND || operation == K_UPDATE ||
      operation == K_WRITE || operation == K_OVERWRITE) {
    if (*fspec) {
      /* Open input file for read only access */
#if !FIT_DOS
      if ((datafile = fopen(fspec, "r")) == NULL) {
#else
      if ((datafile = fopen(fspec, "rb")) == NULL) {
#endif
	perror("*** Error opening data file");
	goto DONE;
      }
    }
    else
      /* Read from standard input */
      datafile = stdin;


    /* Open temp file */
    if ((tempfile = tmpfile()) == NULL) {
      perror("*** Error opening work file");
      goto DONE;
    }
#if FIT_DOS
    fit_binmode(tempfile);
#endif

    /* Copy input to temp file */
    for (doclen = 0; !feof(datafile); doclen += nbytes) {
      nbytes = fread(buf, 1, K_BUFLEN, datafile);
      if (nbytes)
	if (nbytes != fwrite(buf, 1, nbytes, tempfile)) {
	  perror("*** Error writing work file");
	  goto DONE;
	}
    }

    /* Close the input file if not standard input */
    if (*fspec) fclose(datafile);
    datafile = NULL;

    /* rewind the temp file */
    rewind(tempfile);
  }

  else if (operation == K_EXTRACT) {
    if (*fspec) {
      /* Open output file */
#if !FIT_DOS
      if ((datafile = fopen(fspec, "w")) == NULL) {
#else
      if ((datafile = fopen(fspec, "wb")) == NULL) {
#endif
	perror("*** Error opening data file");
	goto DONE;
      }
    }
    else
      /* Output to standard output */
      datafile = stdout;

  }
  else if (operation == K_SEND) {
    /* Open the destination direct access file */
    if ((sendhandle = drct_open(fspec, O_RDWR, prot, timeout, timeout)) == 
	NULL) {
      perror("*** Error opening send destination file");
      goto DONE;
    }
  }


  if (operation == K_DELREC || operation == K_INFO ||
      operation == K_EXTRACT || operation == K_SEND) {
    if (operation == K_INFO || endrec < 0) {
      if (!drct_info(drcthandle, &drctinfo)) {
	perror("*** Error retrieving direct file status info");
	goto DONE;
      }
      endrec = drctinfo.nrecs - 1;
    }
  }

  /* Perform the requested operation */
  if (operation == K_APPEND || operation == K_WRITE ||
      operation == K_OVERWRITE) {
    /* Append the contents of the datafile to the direct access file */
    for (outlen = 0; outlen < doclen; outlen += nbytes) {
      if (doclen - outlen > K_BUFLEN)
	nbytes = K_BUFLEN;
      else
	nbytes = doclen - outlen;
      if (fread(buf, 1, nbytes, tempfile) != nbytes) {
	perror("*** Read error on temp file");
	goto DONE;
      }
      if (operation == K_APPEND) {
	if (drct_append(drcthandle, buf, nbytes, doclen) < 0) {
	  perror("*** Error appending to direct access file");
	  goto DONE;
	}
      }
      else if (operation == K_WRITE) {
	if (drct_write(drcthandle, buf, nbytes, doclen) < 0) {
	  perror("*** Error writing to direct access file");
	  goto DONE;
	}
      }
      else if (operation == K_OVERWRITE) {
	if (drct_overwrite(drcthandle, buf, nbytes, doclen) < 0) {
	  perror("*** Error appending to direct access file");
	  goto DONE;
	}
      }
    }
  }


  else if (operation == K_DELREC) {
    /* Delete the records in the range of startrec to endrec */
    for (currec = startrec; currec <= endrec; ++currec)
      if (drct_setpos(drcthandle, currec))
	if (!drct_delrec(drcthandle, 1)) {
	  perror("*** Unable to delete record");
	  printf("  #%ld\n", currec);
	}
  }

  else if (operation == K_INFO) {
    if (!drct_sizeinfo(drcthandle, &numutilized, &utilizedsize, &numallocated,
		&allocatedsize, &numfragments, &fragmentsize)) {
	  perror("*** Error retrieving direct file size info");
	  goto DONE;
	}
	if (fstat(drcthandle->data, &statbuf)) {
	  perror("*** Error from fstat on data file");
	  goto DONE;
	}
    /* Print size info about direct access file */
    if (statbuf.st_size == 1)
	  printf("Data file size: %d byte\n", statbuf.st_size);
	else
	  printf("Data file size: %d bytes\n", statbuf.st_size);

    if (allocatedsize == 1L)
	  printf("Allocated: %ld bytes", allocatedsize);
	else
	  printf("Allocated: %ld bytes", allocatedsize);
    if (numallocated == 1L)
	  printf(" in %ld record\n", numallocated);
	else
	  printf(" in %ld records\n", numallocated);

	if (utilizedsize == 1L)
	  printf("Utilizing: %ld byte", utilizedsize);
	else
	  printf("Utilizing: %ld bytes", utilizedsize);
	if (numutilized == 1L)
	  printf(" in %ld record\n", numutilized);
	else
	  printf(" in %ld records\n", numutilized);

	if (fragmentsize == 1L)
	  printf("Fragments: %ld byte", fragmentsize);
	else
	  printf("Fragments: %ld bytes", fragmentsize);
	if (numfragments == 1L)
	  printf(" in %ld record\n", numfragments);
	else
      printf(" in %ld records\n", numfragments);
  }
  else if (operation == K_UPDATE) {
    /* Update record contents */
    drct_setpos(drcthandle, startrec);
    for (outlen = 0; outlen < doclen; outlen += nbytes) {
      if (doclen - outlen > K_BUFLEN)
	nbytes = K_BUFLEN;
      else
	nbytes = doclen - outlen;
      if (fread(buf, 1, nbytes, tempfile) != nbytes) {
	perror("*** Read error on temp file");
	goto DONE;
      }
      if (drct_rewrite(drcthandle, buf, nbytes, doclen) < 0) {
	perror("*** Error updating entry in direct access file");
	goto DONE;
      }
    }
  }

  else if (operation == K_ZAP) {
    stat = drct_delfile(drctspec);
  }

  else if (operation == K_VERIFY) {
    printf("*** This option is not currently supported\n");
  }

  else if (operation == K_EXTRACT && headerend) {
    for (currec = startrec, inlen = 0, curline = 0, 
	nlines = headerend-headerstart; 
	currec <= endrec; ++currec, inlen = 0, curline = 0, 
	nlines = headerend-headerstart) {
      do {
	nbytes = drct_rdrec(drcthandle, currec, buf, K_BUFLEN, &doclen);
	inlen += nbytes;
	if (nbytes) {
	  for (n=0, lineptr = buf; n < nbytes && nlines > 0;) {
	    endline = strchr(lineptr, '\n');
	    if (endline == NULL)
	      endline = lineptr + n - 1; 
	    len = endline - lineptr + 1;
	    if (curline >= headerstart && len > 0) {
	      fwrite(lineptr, 1, len, datafile);
	      if (*endline == '\n')
		--nlines;
	    } 
	    n += 1;
	    curline += (*endline == '\n');
	    lineptr = endline+1;
	  }
	}
	else
	  break;
      }
      while (nlines > 0 && inlen < doclen);

      if (inlen) {
	for (; nbytes && nlines; --nlines)
	  /* Pad header extracted header with blank lines so that a post
	     processor can always find the headers */
	  fputc('\n', datafile);
      }

      drct_cancelio(drcthandle);
      if (drcthandle->locked) {
        drct_unlock(drcthandle);
      }
    }
  }

  else if (operation == K_SEND || operation == K_EXTRACT) {
    for (currec = startrec, inlen = 0; currec <= endrec; ++currec, inlen = 0)
      do {
	nbytes = drct_rdrec(drcthandle, currec, buf, K_BUFLEN, &doclen);
	inlen += nbytes;
	if (nbytes) {
	  if (operation == K_SEND) {
	    if (drct_append(sendhandle, buf, nbytes, doclen) < 0) {
	      perror("*** Error sending data to destination file");
	      goto DONE;
	    }
	  }
	  else
	    fwrite(buf, 1, nbytes, datafile);
	}
	else
	  break;
      }
      while (inlen < doclen);
      if (drcthandle->locked) drct_unlock(drcthandle);
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  printf("*** Syntax error\n");

SYNTAX:
  fit_prsfspec(progname, dummyspec, dummyspec, dummyspec, progname, dummyspec,
    dummyspec);
  printf("Usage: %s {switches} direct_file_name\n", progname);
  printf("  -a\t\tAppend to direct access file the -f specified file.\n");
  printf("  -c\t\tCreate direct access file.\n");
  printf("  -d\t\tDelete -r specified records.\n");
  printf("  -f file\tSpecifies file for a, h, o, s, u, w, x, and z switches.\
\n");
  printf("  -h {{s:}e}\tExtract lines s:e for -r specified records.\n");
  printf("  -i\t\tDisplay status information for direct access file.\n");
  printf("  -o\t\tAdd -f specified file, reusing deleted records.\n");
  printf("  -p mask\tSet created file protection mask.\n");
  printf("  -r n{:m}\tSpecifies record or range of records. A trailing \n");
  printf("\t\tspecifier of \"-1\" is used to specify all trailing records.\n"); 
  printf("  -s\t\tSend -r specified records to -f specified direct access \
file.\n");
  printf("  -t seconds\tSet read and write timeout to seconds.\n");
  printf("  -u\t\tUpdate -r specified record with -f specified file.\n");
  printf("  -w\t\tWrite into direct access file the -f specified file.\n");
  printf("  -x\t\tExtract -r specified records.\n");
  printf("  -z\t\tZap (delete) direct access file.\n");

DONE:
  if (drcthandle != NULL) drct_close(drcthandle);
  if (sendhandle != NULL) drct_close(sendhandle);
  if (tempfile != NULL) fit_tempclose(tempfile);
  if (datafile != NULL) fclose(datafile);
  return stat;
}
