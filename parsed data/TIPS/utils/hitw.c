/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : hitw                                                      hitw.c

Function : Filter which adds hits to hit files.

Author : A. Fregly

Abstract : This function is used to add the hits (TIPS document descriptors)
	to output hit files. hitw will create the hit file(s) if they do not
	already exist. Hit files are assumed to have an extension of ".hit".

Calling Sequence : hitw {switches} {hit_file}

  Switches:

	-f fspec	Read hits from file named by fspec.
	-h		Write hits in hit files corresponding to queries.
        -n		Only use name portion of query in setting hit file name when
			updating hit file corresponding to queries..
	-l		Enable logging.
	-o		Optimize file size of hit files (re-use deleted records)
	-p mask		Specify protection mask for created hit files. The default
				is u+rw, g+r.

Notes:

Change log :
000     23-JUL-91  AMF  Created.
001     ??-???-96  AMF  Added -f switch.
002	17-JUL-97  AMF	Added -n switch.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <fcntl.h>
#include <drct.h>
#include <tips.h>

/* Declare the shutdown procedure */
#if FIT_ANSI
void hitw_shutdown(int ss);
#else
void hitw_shutdown();
#endif

/* Globals */
extern int errno;
char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
char logmsg[2*FIT_FULLFSPECLEN], extension[FIT_FULLFSPECLEN+1];
char curqryname[FIT_FULLFSPECLEN+1], nextqryname[FIT_FULLFSPECLEN+1];
char defhitfile[FIT_FULLFSPECLEN+1], curhitfilename[FIT_FULLFSPECLEN+1];
char docdescriptor[TIPS_DESCRIPTORMAX+1];
int logging;
struct drct_s_file *defhit, *qryhit;
FILE *instream;


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int stat, writeqryhits, optimize, len, nameonly, fileflag;
  unsigned prot;
  long doclen;
  char *cptr, *inspec;

#if FIT_DOS
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  fileflag = 0;
  logging = 0;
  writeqryhits = 0;
  nameonly = 0;
  optimize = 0;
  defhit = NULL;
  qryhit = NULL;
  defhitfile[0] = 0;
  curqryname[0] = 0;
  nextqryname[0] = 0;
  stat = 1;
  prot = 0;
  instream = stdin;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  /* Pick off command arguements */
  for (; argc && *(*argv) == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'f' : case 'F' :
        if (!--argc) goto SYNTAX_ERROR;
        inspec = *argv;
	instream = fopen(*(++argv), FIT_READ);
	if (instream == (FILE *) NULL) {
	  perror("*** Error opening input file");
	  goto ERREXIT;
	}
        fileflag = 1;
	break;
      case 'h' : case 'H' :
	    writeqryhits = 1;
	    break;
      case 'l' : case 'L' :
	    logging = 1;
	    break;
      case 'n' : case 'N' :
	    nameonly = 1;
	    break;
      case 'o' : case 'O' :
	    optimize = 1;
	    break;
      case 'p' : case 'P' :
	    if (!--argc) goto SYNTAX_ERROR;
	    sscanf(*(++argv), "%o", &prot);
	  break;
        default :
	    goto SYNTAX_ERROR;
    }
  }

  if (logging) {
    sprintf(logmsg,"*** %s initiated ***",progname);
    fit_logmsg(stderr, logmsg, 1);
  }


  if (argc) {
    fit_expfspec(*argv, ".hit", "", defhitfile);
    if ((defhit = drct_open(defhitfile, O_RDWR, prot, -1, -1)) == NULL)
      if ((defhit = drct_open(defhitfile, O_RDWR + O_CREAT, prot, -1, -1)) == 
	  NULL) {
	fprintf(stderr,"*** Unable to open hit file %s:",defhitfile);
	goto ERREXIT;
      }
  }

  if (logging) {
    if (fileflag) 
      fprintf(stderr,"  Reading hit file: %s\n", inspec);
    else
      fprintf(stderr,"  Reading input from standard input\n");
    if (*defhitfile)
      fprintf(stderr,"  Default hit file: %s\n",defhitfile);
    else
      fprintf(stderr,"  Writing to default hit file is disabled\n");
    if (optimize)
      fprintf(stderr,"  Hit file optimization is enabled\n");
    else
      fprintf(stderr,"  Hit file optimization is disabled\n");
    if (writeqryhits) {
      fprintf(stderr,"  Matching hit file updating is enabled\n");
      if (nameonly)
        fprintf(stderr,"  Directory name stripping in naming hit files is enabled\n");
      else
        fprintf(stderr,"  Updating hit files in same directories as queries\n");
    }
    else {
      fprintf(stderr,"  Matching hit file updating is disabled\n");
      if (nameonly)
        fprintf(stderr,"  -n switch for directory name stripping in naming hit files is not valid here\n"); 
    }
  }

  if (defhit == NULL && !writeqryhits)
    fprintf(stderr,"*** All hit file updating is disabled\n");

  signal(SIGTERM, hitw_shutdown);
  signal(SIGINT,hitw_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE,hitw_shutdown);
  signal(SIGQUIT,hitw_shutdown);
#endif

  /* Process the hits */
  while (1) {

    /* Skip over any leading nulls in the input stream */
    for (*docdescriptor = 0; !*docdescriptor && !feof(instream);)
      *docdescriptor = getc(instream);

    if (!*docdescriptor) break;

    /* Push the non-null character back onto the input stream */
    ungetc(*docdescriptor, instream);

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,instream);
    if (feof(instream)) goto DONE;

    len = strlen(docdescriptor);
    if (len) docdescriptor[len-1] = 0;

    if (logging) fit_logmsg(stderr,docdescriptor,1);
    docdescriptor[len-1] = '\n';

    fwrite(docdescriptor, 1, len, stdout);
    fflush(stdout);

    if (defhit != NULL) {
      /* Update the hit file specified on the command line */
      if (!optimize) {
	/* Append hit to hit file if optimization is not enabled */
	if (!drct_append(defhit, docdescriptor, len, (long) len)) {
	  fprintf(stderr, "*** Error updating default hit file:");
	  goto ERREXIT;
	}
      }
      else {
	/* Write hit, scavenging deleted records */
	if (!drct_overwrite(defhit, docdescriptor, len, (long) len)) {
	  fprintf(stderr, "*** Error updating default hit file:");
	  goto ERREXIT;
	}
      }
    }

    if (writeqryhits) {
      /* Update the hit file corresponding to the query name found in the
	 hit */
      if (!tips_parsedsc(docdescriptor,dummyspec,&doclen,nextqryname)) {
	fit_logmsg(stderr,"*** Error parsing input descriptor",1);
	if (!logging) fprintf(stderr,"%s",docdescriptor);
	goto DONE;
      }

      /* Strip off trailing parts of key field, leaving only the query name */
      if ((cptr = strchr(nextqryname,'#')) != NULL)
	*cptr = 0;

      if (*nextqryname && strcmp(nextqryname, curqryname)) {
	/* Query has changed, close old hit file and open new */
	strcpy(curqryname, nextqryname);
	if (qryhit != NULL) {
	  /* Close previous hit file */
	  drct_close(qryhit);
	  qryhit = NULL;
	}
	/* Open new hit file */
        if (nameonly) {
          fit_prsfspec(nextqryname, dummyspec, dummyspec, dummyspec, curhitfilename, 
			  dummyspec, dummyspec);
	  strcat(curhitfilename, ".hit");
        }
        else {
	  fit_setext(nextqryname, ".hit", curhitfilename);
        }
	if ((qryhit = drct_open(curhitfilename, O_RDWR, prot, -1, -1)) == NULL)
	  if ((qryhit = drct_open(curhitfilename, O_RDWR + O_CREAT, prot, -1, 
	      -1)) == NULL) {
	    sprintf(logmsg,"*** Unable to open hit file: %s",curhitfilename);
	    fit_logmsg(stderr,logmsg,1);
	    perror("");
		errno = 0;
	  }
      }
      if (qryhit != NULL) {
	if (!optimize) {
	  /* Append hit to hit file if optimization is not enabled */
	  if (!drct_append(qryhit, docdescriptor, len, (long) len)) {
	    fprintf(stderr, "*** Error updating matching hit file: %s\n",
	      curhitfilename);
	    goto ERREXIT;
	  }
	}
	else {
	  /* Write hit, scavenging deleted records */
	  if (!drct_overwrite(qryhit, docdescriptor, len, (long) len)) {
	    fprintf(stderr, "*** Error updating matching hit file: %s\n",
	      curhitfilename);
	    goto ERREXIT;
	  }
	}
      }
    }
    fflush(stderr);
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr,"*** Invalid command syntax\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} {hit_file}\n",progname);
  fprintf(stderr,"  -f file_spec\tRead hits from file instead of standard input\n");
  fprintf(stderr,"  -h\t\tUpdate matching hit files\n");
  fprintf(stderr,"  -l\t\tEnable status logging to standard error\n");
  fprintf(stderr,"  -n\t\tUse only the name portion of queries in setting hit file names\n");
  fprintf(stderr,"  -o\t\tOptimize size of hit files\n");
  fprintf(stderr,"  -p mask\tSet created hitfile protection mask\n");
  goto DONE;

ERREXIT:
  perror("");
  stat = errno;

DONE:
  if (logging) {
    sprintf(logmsg,"*** %s terminated ***", progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (defhit != NULL) drct_close(defhit);
  if (qryhit != NULL) drct_close(qryhit);
  if (instream != stdin) fclose(instream);
  return stat;
}



#if FIT_ANSI
void hitw_shutdown(int ss)
#else
void hitw_shutdown()
#endif
{
  int stat;

  stat = errno;
  if (logging) {
    sprintf(logmsg,"**** %s aborted ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (stat) perror("");
  if (defhit != NULL) drct_close(defhit);
  if (qryhit != NULL) drct_close(qryhit);
  if (instream != stdin) fclose(instream);
  exit(stat);
}
