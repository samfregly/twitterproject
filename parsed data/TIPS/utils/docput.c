/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : docput                                            docput.c

Function : Puts documents into specified directory. 

Author : A. Fregly

Abstract : This function is used to place copies of documents being read from
	standard input into a specified directory. The documents must be 
	prefixed with a TIPS descriptor as a header. This header must contain
	the original document name as the last portion of the name field. The
	document copy which is placed in the output directory will have the
	same name as the original.  The input stream is also copied to standard
	output by default.

Calling Sequence : docput {switches} {path}

  Switch:

	-dd     Delete processed documents from archive.
	-do     Delete processed document originals.
	-h      Enable writing of document headers to standard output.
	-l      Enable logging to standard error.
	-p      Disable placment of documents in output path
	-t      Enable writing of document text to standard output.

  Parameter:

	path    Directory into which documents are to be placed. By default,
		documents are placed in the current directory.

Notes: 

Change log : 
000     13-AUG-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_TURBOC
#include <dos.h>
#endif
#if FIT_DOS
#define WRITE "wb"
#else
#define WRITE "w"
#endif
#include <fcntl.h>
#include <drct.h>
#include <tips.h>

#define BUFFERSIZE 30720

/* Declare the shutdown procedure */
#if FIT_ANSI
void docput_shutdown(int stat);
#else
void docput_shutdown();
#endif

/* Globals */
extern int errno;

char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
char logmsg[2*FIT_FULLFSPECLEN], extension[FIT_FULLFSPECLEN+1];

char arcname[FIT_FULLFSPECLEN+1], prevarcname[FIT_FULLFSPECLEN+1];
char docnum_s[TIPS_DESCRIPTORMAX+1]; 
char docname[FIT_FULLFSPECLEN+1], docspec[FIT_FULLFSPECLEN+1];
char inpath[FIT_FULLFSPECLEN+1], outpath[FIT_FULLFSPECLEN+1];
int outpathlen;

char docdescriptor[TIPS_DESCRIPTORMAX+1], docnamefld[TIPS_DESCRIPTORMAX+1];
char dockeyfld[TIPS_DESCRIPTORMAX+1];

char outdescriptor[TIPS_DESCRIPTORMAX+1];
int outdescriptorlen;

char tempdescriptor[TIPS_DESCRIPTORMAX+1];

int logging, continuous, firstpass, doput;

FILE *out;

struct drct_s_file *arc;
char buffer[BUFFERSIZE+1];

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int deletedocs, deleteorig, writetext, writedescr, stat, nbytes, off, readlen;
  long doclen, docnum, inlen;
  char defpath[FIT_FULLFSPECLEN+1];

#if FIT_DOS
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  logging = 0;
  doput = 1;
  deletedocs = 0;
  deleteorig = 0;
  firstpass = 1;
  writetext = 0;
  writedescr = 0;
  arc = NULL;
  stat = 1;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (--argc && !strcmp(*argv,"?")) goto SYNTAX;

  stat = 0;

  /* Pick off switches */
  for (; argc && *(*argv) == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'd' : case 'D' :
	if ((*argv)[2] == 'd' || (*argv)[2] == 'D')
	  deletedocs = 1;
	else if ((*argv)[2] == 'o' || (*argv)[2] == 'O')
	  deleteorig = 1;
	else {
          fprintf(stderr, "docput, invalid delete option: %s\n", *argv);
	  goto SYNTAX_ERROR;
	}
	break;
      case 'h' : case 'H' :
	writedescr = 1;
	break;
      case 'l' : case 'L' :
	logging = 1;
	break;
      case 'p' : case 'P' :
	doput = 0;
	break;
      case 't' : case 'T' :
	writetext = 1;
	break;
      default :
        fprintf(stderr, "docput, unrecognized switch: %s\n", *argv);
	goto SYNTAX_ERROR;
    }
  }

  if (logging) {
    sprintf(logmsg,"*** %s initiated ***",progname);
    fit_logmsg(stderr, logmsg, 1);
  }

  if (argc > 1) goto SYNTAX_ERROR;

  getcwd(defpath, FIT_FULLFSPECLEN);

  if (argc)
    strncpy(inpath, *argv, FIT_FULLFSPECLEN);
  else
    strcpy(inpath, defpath);

  fit_abspath(inpath, defpath, outpath);
  outpathlen = strlen(outpath);

  if (logging) {
    /* Log initiation and configuration */
    sprintf(logmsg,"**** %s initiated", progname);
    fit_logmsg(stderr, logmsg, 1);
    if (deletedocs)
      fprintf(stderr,"  Archive document deletion enabled\n");
    if (deleteorig)
      fprintf(stderr,"  Document originals will be deleted\n");
    if (!writedescr)
      fprintf(stderr,"  Document descriptor output is disabled\n");
    if (!writetext)
      fprintf(stderr,"  Document text output is disabled\n");
    if (!doput)
      fprintf(stderr,"  Output file creation is disabled\n");
  }

  signal(SIGTERM, docput_shutdown);
  signal(SIGINT,docput_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE,docput_shutdown);
  signal(SIGQUIT,docput_shutdown);
#endif

  while (1) {

    /* Skip over any leading nulls in the input stream */
    for (*docdescriptor = 0; !*docdescriptor && !feof(stdin);)
      *docdescriptor = getchar();

    if (!*docdescriptor) break;

    /* Push the non-null character back onto the input stream */
    ungetc(*docdescriptor, stdin);

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);
    if (feof(stdin)) goto DONE;

    if (logging) {
      strcpy(tempdescriptor, docdescriptor);
      tempdescriptor[strlen(docdescriptor)-1] = 0;
      fit_logmsg(stderr, tempdescriptor, 1);
    }

    /* Parse the descriptor */
    tips_parsedsc(docdescriptor, docnamefld, &doclen, dockeyfld);

    /* Parse the name field so that the output name can be determined */ 
    tips_nameparse(docnamefld, arcname, docnum_s, docspec);

    /* Parse out the name and extension portions of the document name */
    fit_prsfspec(docspec,dummyspec,dummyspec,dummyspec,docname,extension,
      dummyspec);
    if (*docname || !doput) {
      if (doput) {
	/* Set the output file name */
	off = outpathlen-1;
	fit_sadd(outpath, FIT_FULLFSPECLEN, docname, &off);
	if (*extension)
	  fit_sadd(outpath, FIT_FULLFSPECLEN, extension, &off);
	outpath[off+1] = 0;

	if ((out = fopen(outpath, WRITE)) == NULL) {
	  sprintf(logmsg, "*** Error opening output file: %s", outpath);
	  fit_logmsg(stderr, logmsg, 1);
	  goto ERREXIT;
	}

	if (writedescr) {
	  tips_makedescr("",-1L, outpath, doclen, dockeyfld, TIPS_DESCRIPTORMAX,
	    outdescriptor, &outdescriptorlen);
	}
      }
      else if (writedescr) {
	strncpy(outdescriptor, docdescriptor, TIPS_DESCRIPTORMAX);
	outdescriptor[TIPS_DESCRIPTORMAX] = 0;
	outdescriptorlen = strlen(outdescriptor);
      }
      if (writedescr) {
	if (fwrite(outdescriptor, 1, outdescriptorlen, stdout) !=
	    outdescriptorlen) {
	  fit_logmsg(stderr, "*** Write error on standard output", 1);
	  goto ERREXIT;
	}
      }

      for (inlen = doclen; !feof(stdin) && inlen; inlen -= nbytes) {

	if (BUFFERSIZE < inlen)
	  readlen = BUFFERSIZE;
	else
	  readlen = inlen;

	if ((nbytes = fread(buffer, 1, readlen, stdin)) != readlen) {
	  sprintf(logmsg, "*** Input error while processing: %s",docdescriptor);
	  fit_logmsg(stderr, logmsg, 1);
	  goto ERREXIT;
	}

	if (doput) {
	  if (fwrite(buffer, 1, nbytes, out) != nbytes) {
	    sprintf(logmsg, "*** Output error on output file: %s", outpath);
	    fit_logmsg(stderr,logmsg,1);
	    goto ERREXIT;
	  }
	}

	if (writetext) {
	  if (fwrite(buffer, 1, nbytes, stdout) != nbytes) {
	    fit_logmsg(stderr,"*** Output error on standard output",1);
	    goto ERREXIT;
	  }
	}
      }

      if (doput && out != NULL) fclose(out);

      if (deletedocs && docnum_s[0]) {
	/* Delete document from archive */
	if (arc == NULL || strcmp(arcname, prevarcname)) {
	  /* Different archive, close old one (if open) and open new one */
	  if (arc != NULL) drct_close(arc);
	  strcpy(prevarcname, arcname);
	  arc = drct_open(arcname, O_RDWR, 0, -1, -1);
	}

	if (arc != NULL) {
	  sscanf(docnum_s,"%ld",&docnum);
	  if (drct_setpos(arc, docnum)) {
	    if (!drct_delrec(arc, 1)) {
	      sprintf(logmsg, "*** Error deleting archive record: %s",
		docnamefld);
	      fit_logmsg(stderr, logmsg, 1);
	      perror("");
	    }
	  }
	  else {
	    sprintf(logmsg, "*** Error accessing archive record: %s",
		docnamefld);
	    fit_logmsg(stderr, logmsg, 1);
	    perror("");
	  }
	}
	else {
	  sprintf(logmsg, "*** Error opening archive: %s", docnamefld);
	  fit_logmsg(stderr, logmsg, 1);
	  perror("");
	}
      }

      if (deleteorig && *docspec) {
	if (arc != NULL) {
	  /* Close the database, we don't want to keep to many files open */
	  drct_close(arc);
	  arc = NULL;
	}
	if (!fit_fdelete(docspec)) {
	  sprintf(logmsg,"*** Error deleting document: %s", docspec);
	  fit_logmsg(stderr, logmsg, 1);
	  perror("");
	}
      }
    }
    else {
      fit_logmsg(stderr, "*** Descriptor does not contain document name", 1);
      fprintf(stderr, "%s", docdescriptor);
    }
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  stat = 1;
  fprintf(stderr,"*** Invalid command syntax\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} dest_path\n",progname);
  fprintf(stderr,"  -dd\t\tDelete processed documents from archive.\n");
  fprintf(stderr,"  -do\t\tDelete processed files.\n");
  fprintf(stderr,"  -h\t\tEnable output of document descriptors\n");
  fprintf(stderr,"  -l\t\tEnable status logging to standard error.\n");
  fprintf(stderr,"  -p\t\tDisable placement of output files in output \
directory.\n");
  fprintf(stderr,"  -t\t\tEnable output of document text.\n");
  goto DONE;

ERREXIT:
  perror("");
  stat = errno;

DONE:
  if (logging) {
    sprintf(logmsg,"*** %s terminated ***", progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (arc != NULL) drct_close(arc);
  return stat;
}


#if FIT_ANSI
void docput_shutdown(int ss)
#else
void docput_shutdown()
#endif
{
  int stat;
  stat = errno;
  if (logging) {
    sprintf(logmsg,"**** %s aborted ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (arc != NULL) drct_close(arc);
  if (stat) perror("");
  exit(stat);
}
