/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : readmsg						readmsg.c

Function : This program is used to read messages.

Author : A. Fregly

Abstract : This function is used to read messages sent by other TIPS
	processes to message ports. Typically, these message ports are accessed
	transparently by various tips applications, but this utility is
	provided for testing and error recovery purposes. The user of
	this utility must have a thorough understanding of the protocols
	used for communicating with a TIPS daemon.

Calling Sequence : readmsg {switches} file_spec

  Switches:
  	-d word		Word to be returned to originator.
	-m msgtype	Message type for message to be received.
	-r		Send return message if incoming message requires
			a response.
	-t timeout	Seconds to wait for message. A value of 0 is used
			to indicate no wait is to be performed. A value
			of -1 is used to indicate an infinite wait.

  Parameter:
	file_spec	File name for message queue.

Notes: 

Change log : 
000	17-OCT-91  AMF	Created.
001	14-DEC-92  AMF	Removed "-w" wait switch. Work with modified versions
			of tips_sendmsg and tips_rcvmsg.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

extern int errno;


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  key_t msgkey;
  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int waitflag, timeout, replyenabled, msgqid, senderpid;
  long msgtype, inmsgtype, retmsgtype;
  unsigned inlen, datalen;
  char msgbuf[TIPS_MAXMSGDATA], retbuf[TIPS_MAXMSGDATA];

  /* Initialize for default message type, a nowait read of message types
     1 to TIPS_MAXMSGNUM */
  msgtype = -TIPS_MAXMSGNUM;
  timeout = 0;
  datalen = 0;
  replyenabled = 0;

  /* Set program name */
  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname,
	dummyspec, dummyspec);
  --argc;

  if (!argc || (*argv)[0] == '?') goto SYNTAX;

  /* Parse switches */
  for (; argc > 1; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch ((*argv)[1]) {
        case 'd' : case 'D' :
	  --argc;
	  ++argv;
	  datalen = min(TIPS_MAXMSGDATA, strlen(*argv)); 
	  strncpy(retbuf, *argv, datalen);
	  break;
	case 'm' : case 'M' :
	  --argc;
	  ++argv;
	  sscanf(*argv, "%d", &msgtype);
	  break;
	case 'r' : case 'R' :
	  replyenabled = 1;
	  break;
	case 't' : case 'T' :
	  --argc;
	  ++argv;
	  sscanf(*argv, "%d", &timeout);
	  waitflag = 1;
	  break;
	default:
	  goto SYNTAX;
      }
    }
    else
      goto SYNTAX;
  }

  if (argc <= 0) goto SYNTAX;

  /* Attach to the message queue */
  msgkey = ftok(*argv, TIPS_FTOKSEED);
  if ((msgqid = msgget(msgkey, 0)) < 0) {
    perror("*** Unable to attach to message queue");
    exit(errno);
  }

  /* Receive message */
  if (!tips_rcvmsg(msgqid, msgtype, timeout, msgbuf, TIPS_MAXMSGDATA, &inlen,
	&inmsgtype, &senderpid)) {
    perror("*** Error receiving message");
    exit(errno);
  }

  printf("Message Type: %ld, Length: %d, Sender PID: %d\n", inmsgtype, inlen,
    senderpid);
  if (inlen) fwrite(msgbuf, 1, inlen, stdout);
  printf("\n");

  if (replyenabled && senderpid) {
    retmsgtype = senderpid + TIPS_MAXMSGNUM + 1;
    if (!tips_sendmsg(msgqid, retmsgtype, retbuf, datalen, 0, msgbuf, 
	TIPS_MAXMSGDATA, &inlen, &msgtype, &senderpid)) {
      perror("*** Error sending reply"); 
      exit(errno);
    }
  }
  return 0;
 
SYNTAX:
  /* Syntax error or no command line switches or parameters or help
     requested. Write usage to standard error */
  fprintf(stderr,"Usage: %s {switches} file_spec\n", progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -d word\tWord to be returned as data.\n");
  fprintf(stderr,"  -m msgtype\tMessage type.\n");
  fprintf(stderr,"  -r\t\treply enabled.\n");
  fprintf(stderr,"  -t timeout\tSeconds to wait for message.\n");
  fprintf(stderr,"Parameter:\n");
  fprintf(stderr,"  file_spec\tFile name for message queue\n");
  return 0;
}
