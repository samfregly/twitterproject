/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fitdocmd

Function : Used on DOS systems to spawn a command with redirected input/output

Author : A. Fregly

Abstract : This program is meant to be spawned by DOS programs which wish
	to spawn a command with redirected input/output. Since I don't
	currently know a good mechanism for redirecting them, using spawn,
	and then redirecting them back to what they were, this command is
	used as an intermediary. When it redirects input/output for the
	commands to be spawned, it doesn't matter since the command
	is finished anyway.

Calling Sequence : fitdocmd {switches} command {command switches and params}

  Switches:

	-e fspec	Set standard error.

	-i fspec	Set standard input.

	-o fspec	Set standard output.

  Parameters:

	command		Command or program to execute.

	command_args	Arguements to be passed to command.

Notes:

Change log :
000	03-MAY-92  AMF	Created.
******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if !FIT_COHERENT && !FIT_LINUX
#include <process.h>
#endif

extern int errno;

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{
  char dummyspec[FIT_FULLFSPECLEN+1], progname[FIT_FULLFSPECLEN+1];
  char errmessage[FIT_FULLFSPECLEN*3];

  /* Get program name */
  fit_prsfspec(*argv, dummyspec, dummyspec, dummyspec, progname, dummyspec,
    dummyspec);

  /* See if help was requested (no parameters or first parameter is "?" */
  if (!--argc || !strcmp(*(++argv), "?")) goto SYNTAX;

  /* Pull off switches */
  for (; argc && (*argv)[0] == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'e' : case 'E' :
	if (--argc) {
	  if (freopen(*(++argv), "w", stderr) == NULL) {
	    sprintf(errmessage, "%s: *** Unable to open error file, %s",
	     progname, *argv);
	    perror(errmessage);
	    goto DONE;
	  }
	}
	else
	  goto SYNTAX_ERROR;
	break;
      case 'i' : case 'I' :
	if (--argc) {
	  if (freopen(*(++argv), "r", stdin) == NULL) {
	    sprintf(errmessage, "%s: *** Unable to open input file, %s",
	     progname, *argv);
	    perror(errmessage);
	    goto DONE;
	  }
	}
	else
	  goto SYNTAX_ERROR;
	break;
      case 'o' : case 'O' :
	if (--argc) {
	  if (freopen(*(++argv), "w", stdout) == NULL) {
	    sprintf(errmessage, "%s: *** Unable to open output file, %s",
	     progname, *argv);
	    perror(errmessage);
	    goto DONE;
	  }
	}
	else
	  goto SYNTAX_ERROR;
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  /* No command specified? */
  if (!argc) goto SYNTAX_ERROR;

  /* Execute the command and pass to it the arguement list specified on the
     remainder of the command line. */
  execvp(*argv, argv);

  /* Should never get here unless command being executed didn't fly */
  sprintf(errmessage, "%s: *** Unable to execute command, %s", progname,
    *(argv));
  perror(errmessage);
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr, "%s: *** Invalid switch or missing parameter\n", progname);

SYNTAX:
  fprintf(stderr, "Usage: %s {switches} command {command_args}\n", progname);
  fprintf(stderr, "Switches:\n");
  fprintf(stderr, "  -e fspec\tSet standard error.\n");
  fprintf(stderr, "  -i fspec\tSet standard input.\n");
  fprintf(stderr, "  -o fspec\tSet standard output.\n");
  fprintf(stderr, "Parameters:\n");
  fprintf(stderr, "  command\tCommand or program to execute.\n");
  fprintf(stderr, "  command_args\tArguements to be passed to command.\n");

DONE:
  return errno;
}
