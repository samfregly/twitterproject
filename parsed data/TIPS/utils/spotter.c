/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spotter                                           spotter.c

Function : Text data profiler daemon.

Author : A. Fregly

Abstract : This program meant to be used as a daemon for profiling
	of textual data. Documents are fed to it by being piped into
	standard input, and spotter writes to standard output the names of
	processed files and the identities of any profiles which matched
	the file. The input file must be preceeded by a descriptive header
	in SeekWare standard format (as produced by "receiver", "anpa",...).
	The header takes the format of

		identifier,length{,key}<lf>

	Where identifier is an ASCII identifier for the document (file name,
	index,...), length is an ASCII numeric specifying the length of
	the document in bytes, and key is an optional user definable value
	which may be used for a variety of purposes (document type specifier,
	security classification,...).

	The output format of spotter is a stream of line feed separated
	descriptors, which may have the key field prefixed by
	a matching query name, with a "#" character ending the prefix.

Calling Sequence : spotter {switches} objlib_name

  Switches:

	-a name Archive name. Spotter will prefix output document descriptors
		with the supplied name. This option is used if an archive is
		supplied as input via input redirection, and it is desired
		that output hits point at the archive rather than the
		original document. The prefixed name is terminated with a
		'#'.

	-d      This switch is used to specify the character used as a
		beginning of field delimiter. Whenever the field delimiter
		character is seen in the input text stream, the following
		character is interpreted as a field number.

	-f      Field file. The field file should contain a list of field
		names in an order corresponding to the field numbers used
		within the input stream. The first entry in the field file
		corresponding to field 0, the second to field 1,... The field
		name are taken as the first word on each line of the field
		file, the remainder of the line being user definable. If no
		field files if provided by means of the -f qualifier, spotter
		will look for a field file which corresponds to the archive
		named with the -a switch, or if the -a switch is not used,
		then spotter will look for a field file corresponding to the
		object query library. When searchin for these field files,
		spotter looks for a file with the same name as the archive
		or query object library, but with a file name extension of
		".fld".

	-k      Enables stripping of the key field found in input document
		descriptors. Output descriptors will have a key consisting
		only of the matching query name. This switch is used to
		minimize the size of output descriptors in cases where
		there is no need to keep the key value.

	-l      If set, spotter will log initiation, termination, and
		document processing to stderr. Spotter will also maintain
		a statistics file corresponding to the object query library,
		and with an extension of ".act". The first record in this
		file is used to keep track of the number of documents which
		have been searched, the time at which spotter started
		processing, and the time at which the last document
		was searched (in seconds since system 0 time).
		These three long word values are stored in the first three
		long words of the file. Other records in the file correspond
		to queries in the query object library, with each entry
		consisting of three long word values, the first of which is
		the number of matches for the query, the second the start
		time of the query, and the third the index of the query in
		the query executable, or -1 if the query was not loaded into
		the profile executable. A terminating record is also put
		into the activity file, with all value -1L. This file will
		only be updated at time of spotter initiation, when a
		request is received in the spotter command mailbox to update
		the file, and at time of spotter shutdown.

	-n      Specifies that document numbers are to be stripped from
		document descriptors which are output. This switch is used
		if an archive serves as input via i/o redirection, and the
		-a switch is not used. In this case, leaving the document
		number in the output document descriptors will imply that
		the descriptor is for a document in an archive, which will
		make the original file inaccesable via the TIPS hit
		processing utilities.

	-p      Profiling flag. Enables socket that allows spotter to be 
		told  to reload the profiles from the query library.
		This is done when queries are added or deleted from the
		query library by the application which did the add or
		delete. "-p" can optionally be folowed by a port number to
                use for the socket. If the port number is specified, there 
                can be no spaces between "-p" and the port number. The 
                default port number is defined in tips.h with the constant 
                TIPS_SPOTTER_DEFAULT_PORT.

	-s      Specifies that the archive name specified with the -a or
		-i switch is to be substituted for the document name found
		in the incoming document descriptor. The format of the name
		field will be "archive_name{#nnn}" where nnn is the document
		number found in the input descriptor. This switch is used
		to minimize the size of output descriptors in cases where
		there is no need to keep the original document name.

	-t      Enables printing of time statistics at completion of search.

	-z      Specifies that matching query names are not to be prefixed
		to the key field in output descriptors. Normally, the key
		field is modified so that it is prefixed by the matching
		query name, terminated by a "#" if the key field was used
		in the incoming descriptor.

Notes :

Change log :
000     05-MAY-91  AMF  Created.
001     01-OCT-91  AMF  Update activity file on matches.
002     14-DEC-92  AMF  Use new calling sequence for tips_rcvmsg and
			tips_sendmsg.
003     08-SEP-93  AMF  HPUX compatibility.
004     27-JUN-97  AMF	Use named pipes under Windows NT to get messages. First
                        implementation is crude and still has race conditions but
                        for practical purposes should be fine.
005     06-DEC-03  AMF  Use sockets for communication
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>
#if FIT_CYGWIN
#include <io.h>
#include <windows.h>
#endif
#include <errno.h>

#include <time.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>

#if FIT_DOS && 0
#include <direct.h>
#endif

#if FIT_ANSI
long time(long *timbuf);
#else
long time();
#endif

#if FIT_HP
#include <lowlvlio.h>
#endif

#include <tips.h>
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <drct.h>
#include <fit_stat.h>

#define QUERYTEXTMAX 64536

#define BUFFERSIZE 0x100000 
#define PACKEDQRYBUFMAX 128000

#if !FIT_TURBOC
#define READONLY "r"
#define WRITE "w"
#define MAXROW 24
#else
#define READONLY "rb"
#define WRITE "wb"
#define MAXROW 16
#endif

/* Declare the shutdown procedure */
#if FIT_ANSI
void spotter_shutdown(int errnum);
void spotter_loadqueries(char *objlibname, int logging, int profiling, int nfields, 
  char *(*fields), char *(*xe), int *actfile, long *(*act), int *actsize, 
  long *(*qrymap));
#else
void spotter_shutdown();
void spotter_loadqueries();
#endif

/* Globals needed for the shutdown procedure */
#if FIT_UNIX || FIT_COHERENT
extern int errno;
#endif
int fit_status;

static long totalbytes, filematches, querymatches, nsearched;
static int logging, profiling, showstats;
static char progname[FIT_FULLFSPECLEN+1], inputstreamname[FIT_FULLFSPECLEN+1];
static char logmsg[TIPS_DESCRIPTORMAX+81], exename[FIT_FULLFSPECLEN+1];
static char infile[FIT_FULLFSPECLEN+1], docdescriptor[TIPS_DESCRIPTORMAX+1];
static char key[TIPS_DESCRIPTORMAX+1], fieldfile[FIT_FULLFSPECLEN+1];
static char dummyspec[FIT_FULLFSPECLEN+1];
static char objlibname[FIT_FULLFSPECLEN+1];
static long *act, *qrymap;
static int actsize;
static int actfile;
static fit_s_timebuf startsearch, endsearch, searchtime;
static int msgqid;

#if FIT_ANSI
main(int argc, char *(*argv))
#else
main(argc,argv)
  int argc;
  char *(*argv);
#endif
{

  char *xe;
  struct prf_s_exe *exe;
  unsigned char buffer[BUFFERSIZE+1];

  unsigned char *bufptr=NULL, *endbuf=NULL;
  unsigned char *endptr=NULL;
  int *matches=NULL, nmatches, nbytes, searchstat, i, len;
  int fielddelim, readlen, nitems;
  long inlen, doclen;
  unsigned long *querytermmatches, totaltermmatches;
  char *fname;
  int stripdocnums, stripinkeys, stripinname, addqueryprefix;
  int stat, addname, addkey;
  char *objptr;

  char *(*fields), *(*(fielddata)), defpath[FIT_FULLFSPECLEN+1];
  int nfields,enabfields;
  unsigned int searchopts, textlen, retlen;
/******* Disable relevance option for now
  unsigned int relevanceopt;
********/
  long curtime;
  long inmsgtype;
  int inpid;
  int stdin_fd;

  static struct servent s;
  struct servent *servp=&s;
  struct sockaddr_in server, remote;
  int request_sock, new_sock;
  int fd,nfound, maxfd, bytesread,addrlen;
  fd_set rmask, mask;
  static struct timeval timeout = { 0, 000000 }; /* immediate timeout */
  char *port_s=TIPS_SPOTTER_DEFAULT_PORT;

#if FIT_DOS
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  stat = 0;
  logging = 0;
  showstats = 0;
  profiling = 0;
  msgqid = 0;
  stdin_fd = fileno(stdin);
  maxfd = stdin_fd;
  FD_ZERO(&mask);
  FD_SET(stdin_fd,&mask);
  fit_gettime(&startsearch);

  strcpy(progname,*(argv++));
  fit_prsfspec(progname,dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;


#if !FIT_VMS
  getcwd(defpath,FIT_FULLFSPECLEN);
#else
  defpath = NULL;
#endif

  totalbytes = 0;
  filematches = 0;
  querymatches = 0;
  nfields = 0;
/****** Disable relevance option for now
  relevanceopt = 0;
*******/
  enabfields = 0;
  fielddelim = 0xff;
  stripdocnums = 0;
  stripinkeys = 0;
  addqueryprefix = 1;
  stripinname = 0;
  msgqid = -1;

  exename[0] = 0;
  fieldfile[0] = 0;
  inputstreamname[0] = 0;
  fields = NULL;
  fielddata = NULL;
  xe = NULL;
  exe = NULL;
  act = NULL;
  actfile = -1;
  qrymap = NULL;

  /* Pull off switches */
  for (;argc > 1; --argc, ++argv) {

    if (*(*argv) != '-') goto SYNTAX_ERROR;

    switch ((*argv)[1]) {
      case 'a' : case 'A' :
	--argc;
	fit_expfspec(*(++argv), "", defpath, inputstreamname);
	if (!fieldfile[0])
	  fit_setext(inputstreamname, ".fld", fieldfile);
	break;

      case 'd' : case 'D' :
	--argc;
	sscanf(*(++argv),"%x",&fielddelim);
	break;

      case 'f' : case 'F' :
	--argc;
	strcpy(fieldfile,*(++argv));
	enabfields = 1;
	break;

      case 'k' : case 'K' :
	stripinkeys = 1;
	break;

      case 'l' : case 'L' :
	logging = 1;
	break;

      case 'n' : case 'N' :
	stripdocnums = 1;
	break;

      case 'p' : case 'P' :
	profiling = 1;
        if ((*argv)[2]) {
          port_s = (*argv)+2;
        } 
	break;

      case 's' : case 'S' :
	stripinname = 1;
	break;

      case 't' : case 'T' :
	showstats = 1;
	break;

      case 'z' : case 'Z' :
	addqueryprefix = 0;
	break;

      default :
	fprintf(stderr,"Invalid command option %s\n",*argv);
	goto SYNTAX;
    }
  }


  if (!argc) goto SYNTAX_ERROR;
  objptr = *argv;

  fit_expfspec(objptr, ".qlb", defpath, objlibname);
  fit_setext(objlibname, ".qlb", objlibname);
  if (!fieldfile[0])
    fit_setext(objlibname, ".fld", fieldfile);

  if (!(stat = tips_loadflds(fieldfile,&fields,&fielddata,&nfields)) && 
      enabfields) {
    fprintf(stderr,"Error loading field definitions from: %s\n", fieldfile);
    goto ERREXIT;
  }
  else
    enabfields = stat;

  if (logging) {
    sprintf(logmsg,"**** %s initiated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    fprintf(stderr, "  Query library: %s\n", objptr);
    if (*inputstreamname)
      fprintf(stderr,"  Named input stream: %s\n",inputstreamname);
    if (profiling)
      fprintf(stderr,"  Profile update notification processing enabled\n");
    if (*fieldfile)
      fprintf(stderr,"  Field descriptor file: %s\n",fieldfile);
    if (enabfields)
      fprintf(stderr,"  Field delimiter: %x\n",fielddelim);
    if (stripinname)
      fprintf(stderr,"  Input name removal enabled\n");
    if (stripdocnums)
      fprintf(stderr,"  Document number removal enabled\n");
    if (!addqueryprefix)
      fprintf(stderr,"  Query name key prefixing disabled\n");
    if (stripinkeys)
      fprintf(stderr,"  Input key removal enabled\n");
  }

  if (profiling) {
    if ((request_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
      perror("socket");
      exit(1);
    }
    s.s_port = htons((u_short)atoi(port_s));

    memset((void *) &server, 0, sizeof server);
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = servp->s_port;
  
    if (bind(request_sock, (struct sockaddr *)&server, sizeof server) < 0) {
      perror("bind");
      exit(1);
    }
    if (listen(request_sock, SOMAXCONN) < 0) {
      perror("listen");
      exit(1);
    }
  
    FD_SET(request_sock, &mask);
    maxfd = (request_sock > stdin_fd) ? request_sock : stdin_fd;
  }

  /* Load the queries from the query library into a search executable */
  spotter_loadqueries(objlibname, logging, profiling, nfields, fields, &xe, 
      &actfile, &act, &actsize, &qrymap);
  exe = (struct prf_s_exe *) xe;
  if (!profiling && !exe->nqueries) {
    fprintf(stderr,"Search aborted, no queries were loaded\n");
    exit(1);
  }
  nsearched = 0;

  /* Set up signal handlers for graceful shutdown */
  signal(SIGTERM,spotter_shutdown);
  signal(SIGINT,spotter_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE,spotter_shutdown);
  signal(SIGQUIT,spotter_shutdown);
#endif

  endbuf = buffer + BUFFERSIZE;
  searchstat = 1;

/*******  Disable relevance ranking for now
  if (relevanceopt)
    searchopts = PRF_MASK_QRYTERMCOUNTS | PRF_MASK_EXETERMCOUNTS;
  else
*********/
  searchopts = 0;

  fit_gettime(&startsearch);

  while (1) {
    /* See if there is any input available for processing */
    rmask = mask;
    nfound = select(maxfd+1, &rmask, (fd_set *)0, (fd_set *)0, (struct timeval *) NULL);

    if (nfound < 0) {
      if (errno == EINTR) {
        fprintf(stderr,"interrupted system call\n");
        continue;
      }
      /* something is very wrong! */
      perror("select");
      exit(1);
    }

    if (profiling && FD_ISSET(request_sock, &rmask)) {
      /* Message pending on input socket, read it and return response */
      addrlen = sizeof(remote);
      new_sock = accept(request_sock, (struct sockaddr *)&remote, &addrlen);
      if (new_sock < 0) {
        perror("accept");
        exit(1);
      }
      fprintf(stderr,"connection from host %s, port %d, socket %d\n",
        inet_ntoa(remote.sin_addr), ntohs(remote.sin_port), new_sock);
      bytesread = read(new_sock, buffer, BUFFERSIZE - 1);
      if (bytesread <= 0) {
        perror("socket read");
        fprintf(stderr,"End of file on socket: %d\n",new_sock);
        /* fall through */
      }
      strcpy(buffer,"ack\n");
      if (write(new_sock, buffer, strlen(buffer)) != strlen(buffer)) {
        perror("socket echo");
      }
      if (close(new_sock)) perror("socket close");
      
      /* For now, any message causes a reload of the queries. Eventually a number of 
         processing options could be performed based on the message type. */
	 /* Cancel previous search */
      prf_search(8, searchopts, xe, buffer, 0, &nmatches, &matches, 
        &querytermmatches, &totaltermmatches);
      spotter_loadqueries(objlibname, logging, profiling, nfields, fields, &xe, 
        &actfile, &act, &actsize, &qrymap);
      /* Set search status so that search structures get reset in prf_search */
      searchstat=1;
    }

    if (FD_ISSET(stdin_fd, &rmask)) {
      for (nitems = 0; !feof(stdin) && !nitems;) {
        /* Skip over any leading nulls in the input stream */
        nitems = fread(docdescriptor, 1, 1, stdin);
        if (nitems <= 0 || !*docdescriptor) nitems = 0;
      }
  
      if (feof(stdin)) break;
  
      /* Push the non-null character back onto the input stream */
      ungetc(*docdescriptor, stdin);
  
      *docdescriptor = 0;
      fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);
      if (feof(stdin)) goto DONE;
  
      len = strlen(docdescriptor);
      if (len) docdescriptor[len-1] = 0;
  
      if (logging) {
        fit_logmsg(stderr,docdescriptor,1);
        fflush(stderr);
      }
  
      if (!tips_parsedsc(docdescriptor, infile, &doclen, key)) {
        fit_logmsg(stderr,"*** Error parsing input descriptor",1);
        goto DONE;
      }
  
      for (searchstat |= 4, bufptr = buffer, inlen = doclen;
  	!feof(stdin) && inlen; inlen -= nbytes) {
  
        if (endbuf-bufptr < inlen)
          readlen = endbuf-bufptr;
        else
          readlen = (int) inlen;
  
        if (!(nbytes = fread(bufptr, 1, readlen, stdin))) {
          fit_logmsg(stderr,"*** Error, zero length read",1);
          goto ERREXIT;
        }
  
        totalbytes += nbytes;
        endptr = bufptr + nbytes - 1;
  
        if (inlen > nbytes) {
  	/* Find last delmiter char in buffer */
          while (endptr > bufptr && *(exe->tx_xlate+*endptr) > 0) {
            --endptr;
          }
        }
        else {
          /* Set last buffer in document flag bit */
          searchstat |= 2; 
        }
  
        buffer[nbytes] = ' ';
  
        /* Set length of text to search */
        textlen = endptr - buffer + 1;
  
        /* Search next buffer in document */
        nmatches = 0;
  
        prf_search(searchstat, searchopts, xe, buffer, textlen, &nmatches,
  	&matches, &querytermmatches, &totaltermmatches);
  
        if (textlen < nbytes) {
          /* Move unsearched text in buffer to beginning of buffer */
          memmove(buffer, endptr+1, nbytes - textlen + 1);
  
          /* Set start of buffer pointer at loc beyond moved text */
          bufptr = buffer + nbytes - textlen;
        }
        else
          bufptr = buffer;
  
        /* clear flag bits */
        searchstat = 0;
      }
  
      if (profiling) {
        /* Update activity header */
        curtime = (long) time((long *) NULL);
        act[0] += 1;
        act[2] = curtime;
        lseek(actfile, 0L, 0);
        write(actfile, act, 3 * sizeof(*act));
      }
  
      /* Print file names of any matching queries */
      if (nmatches) {
        filematches += 1;
        querymatches += nmatches;
  
        len = 0;
        if (*inputstreamname) {
          strcpy(docdescriptor, inputstreamname);
          len = strlen(inputstreamname);
          if (!stripdocnums || !stripinname) {
            docdescriptor[len++] = '#';
            docdescriptor[len] = 0;
          }
        }
        if (stripdocnums || stripinname) {
          if ((fname = strchr(infile,'#')) != NULL)
            *(fname++) = 0;
          else
            fname = infile + strlen(infile);
          addname = *fname && !stripinname;
          if (!stripdocnums) {
            strcpy(docdescriptor+len, infile);
            len += strlen(docdescriptor+len);
            if (addname) {
              docdescriptor[len++] = '#';
              docdescriptor[len] = 0;
            }
          }
        }
        else {
          fname = infile;
          addname = *fname && !stripinname;
        }
  
        if (addname) {
         strcpy(docdescriptor+len, fname);
         len += strlen(fname);
        }
  
        docdescriptor[len++] = ',';
        docdescriptor[len] = 0;
        sprintf(docdescriptor+len, "%ld\0", doclen);
        len += strlen(docdescriptor+len);
  
        addkey = *key && !stripinkeys;
  
        if (addkey || addqueryprefix) {
          docdescriptor[len++] = ',';
          docdescriptor[len] = 0;
        }
  
        for (i = 0; i < nmatches; ++i) {
          if (addqueryprefix && addkey)
            printf("%s%s#%s\n",docdescriptor, exe->qfiles[matches[i]],
              key);
          else if (addqueryprefix)
            printf("%s%s\n",docdescriptor, exe->qfiles[matches[i]]);
          else if (addkey)
            printf("%s%s\n",docdescriptor, key);
          else
            printf("%s\n",docdescriptor);
          if (logging)
            fprintf(stderr,"...%s\n",exe->qfiles[matches[i]]);
          if (profiling) {
            ++act[qrymap[matches[i]] * 3];
            act[qrymap[matches[i]] * 3 + 1] = curtime;
          }
        }
        if (profiling)
  	write(actfile, &act[3], actsize - (3 * sizeof(*act)));
        fflush(stdout);
      }
      nsearched += 1;
    }
  }
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr,"*** Syntax error\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} object_lib\n",progname);
  fprintf(stderr,"  -a name\tSet input stream name to specified archive.\n");
  fprintf(stderr,"  -d hex\tSet field delimiter to specified hex value.\n");
  fprintf(stderr,"  -f file\tSet field definition file name.\n");
  fprintf(stderr,"  -k\t\tStrip input key field.\n");
  fprintf(stderr,"  -l\t\tEnable logging.\n");
  fprintf(stderr,"  -n\t\tStrip document numbers.\n");
  fprintf(stderr,"  -p\t\tEnable profile activity file and message queue.\n");
  fprintf(stderr,"  -s\t\tSubstitute archive name for file name.\n");
  fprintf(stderr,"  -t\t\tShow time and search statistics\n");
  fprintf(stderr,"  -z\t\tDisable matching query name key prefixing.\n");
  goto DONE;

ERREXIT:
  stat = errno;
  perror("");

DONE:
  spotter_shutdown(stat);
  return stat;
}

#if FIT_ANSI
void spotter_shutdown(int errnum)
#else
void spotter_shutdown(errnum)
  int errnum;
#endif
{
  double dtotalbytes, dsearchtime, dsearchrate;

  if (showstats) {
    fit_gettime(&endsearch);
    fit_timedif(&endsearch, &startsearch, &searchtime);
    sprintf(logmsg,"**** %s terminated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    fprintf(stderr,"  searched: %ld, matched: %ld, query matches: %ld\n",
	nsearched,filematches,querymatches);
    fprintf(stderr,"  Total number of bytes searched %ld\n",totalbytes);
    fprintf(stderr,"  Search Time: %ld.%02.2ld seconds", 
      fit_seconds(&searchtime), fit_millisecs(&searchtime));
    dtotalbytes = totalbytes;
    dsearchtime = fit_seconds(&searchtime) * 1000 + fit_millisecs(&searchtime);
    dsearchtime /= 1000.0;
    dsearchrate = dtotalbytes / dsearchtime; 
    fprintf(stderr,"  Search rate: %ld\n", (long) dsearchrate); 
    if (profiling && act != NULL && actfile != -1) {
      /* Update the activity file */
#if !FIT_TURBOC
      write(actfile, act, actsize);
#else
      _write(actfile, act, actsize);
#endif
    }
  }

  if (profiling) {
    if (actfile != -1) close(actfile);
  }

  exit(errnum);
}


#if FIT_ANSI
void spotter_loadqueries(char *objlibname, int logging, int profiling, int nfields, 
  char *(*fields), char *(*xe), int *actfile, long *(*act), int *actsize, 
  long *(*qrymap))
#else
void spotter_loadqueries(objlibname, logging, profiling, nfields, fields, xe, 
  actfile, act, actsize, qrymap)
  char *objlibname;
  int logging, profiling, nfields;
  char *fields, *(*xe);
  int *actfile;
  long *(*act);
  int *actsize;
  long *(*qrymap);
#endif
{

  struct drct_s_info objinfo;
  long lastqrynum, nextqrynum;
  char qryname[FIT_FULLFSPECLEN+1], actname[FIT_FULLFSPECLEN+1];
  int nbytes, nrecs, i, nobj, qnum;
  long curtime;
  struct qc_s_cmpqry *cmpqry;
  struct drct_s_file *objin;
  char errmessage[2 * FIT_FULLFSPECLEN];

  if (!prf_init(MAXROW,NULL,nfields,fields,xe)) {
    fit_logmsg(stderr,"*** Error initializing profile executable", 1);
    perror("");
    spotter_shutdown(errno);
  }


  /* Open the query object library */
  if ((objin = qc_openlib(objlibname, O_RDWR, -1)) == NULL) {
    if ((objin = qc_openlib(objlibname, O_RDWR + O_CREAT, -1)) == NULL) {
      sprintf(errmessage,"*** Error opening query object library: ", 
	objlibname);
      fit_logmsg(stderr, errmessage, 1);
      perror("");
      spotter_shutdown(errno);
    }
    else if (logging) {
      sprintf(errmessage,"Created query object library: %s", objlibname);
      fit_logmsg(stderr, errmessage, 1);
    }
  }
  else if (logging) {
    sprintf(errmessage, "Opened query object library: %s", objlibname);
    fit_logmsg(stderr, errmessage, 1);
  }

  drct_info(objin, &objinfo);
  lastqrynum = objinfo.nrecs - 1;

  if (profiling) {
    /* Set up query activity file and internal tracking structure */

    /* Allocate internal storage for tracking structure */
    *actsize = ((int) lastqrynum + 3) * 3 * FIT_BYTES_IN_LONG;
    if (*act != NULL) free(*act);
    *act = (long *) calloc(*actsize+FIT_BYTES_IN_LONG, 1);
    if (*act == NULL) {
      fit_logmsg(stderr,"*** Unable to allocate memory for activity list", 1);
      perror("");
      spotter_shutdown(errno);
    }

    /* Attempt to open the file, creating it if it couldn't be opened. */
    fit_setext(objlibname, ".act", actname);

    if (*actfile != -1) close(*actfile);
    *actfile = -1;

    if ((*actfile = open(actname, O_RDWR, -1)) == -1) {
      if ((*actfile = open(actname, O_RDWR + O_CREAT, -1)) == -1) {
	sprintf(errmessage,"*** Error opening query activity file: %s",actname);
	fit_logmsg(stderr, errmessage, 1);
	perror("");
	spotter_shutdown(errno);
      }
      else if (logging) {
	sprintf(errmessage,"Created activity file: %s", actname);
	fit_logmsg(stderr, errmessage, 1);
      }
    }

    /* Read in the old contents of the activity file */
#if !FIT_TURBOC
    nbytes = read(*actfile, *act, *actsize);
#else
    nbytes = _read(*actfile, *act, *actsize);
#endif

    /*  Initialize the structure time values and EOF marks */
    nrecs = nbytes / (FIT_BYTES_IN_LONG * 3) - 1;
    curtime = (long) time(NULL);
    if (nrecs <= 0) {
      (*act)[0] = 0;
      (*act)[1] = curtime;
      ++nrecs;
    }
    else
      (*act)[1] = curtime;
    (*act)[2] = 0;

    for (i = max(1,nrecs + 1); i <= lastqrynum + 2; ++i) {
      /* Mark all new entries with EOF marker */
      (*act)[i * 3] = 0;
      (*act)[i * 3 + 1] = 0;
      (*act)[i * 3 + 2] = -1L;
    }

    for (i = 1; i <= nrecs; ++i)
      /* Mark all of the known queries as "not loaded" */
      (*act)[i * 3 + 2] = -1L;

    /* Update the file */
#if !FIT_TURBOC
    write(*actfile, *act, *actsize);
#else
    _write(*actfile, *act, *actsize);
#endif
  }

  for (nobj = 0, cmpqry = NULL;
      (nextqrynum = qc_nextlib(objin, qryname, &cmpqry)) >= 0 &&
       nextqrynum <= lastqrynum;) {
    if (prf_addqry(qryname, cmpqry, *xe)) {
      if (profiling) {
	(*act)[((int) nextqrynum + 1) * 3] = 0;
	(*act)[((int) nextqrynum + 1) * 3 + 2] = nobj;
      }
      if (logging) {
	sprintf(errmessage,"Loaded query: %s", qryname);
	fit_logmsg(stderr, errmessage, 1);
      }
      ++nobj;
    }
    else {
      sprintf(errmessage, "*** Profile link error on query: %s", qryname);
      fit_logmsg(stderr, errmessage, 1);
      perror("");
      spotter_shutdown(errno);
    }
  }

  fflush(stderr);
  qc_closelib(objin);

  if (profiling) {
    if (*qrymap != NULL) free(*qrymap);
    *qrymap = (long *) calloc(nobj + 1, sizeof(*(*qrymap)));
    if (*qrymap == NULL) {
      fit_logmsg(stderr, "*** Error allocating query mapping structure", 1);
      perror("");
      spotter_shutdown(errno);
    }
    for (i = 1; i <= lastqrynum + 1; ++i) {
      qnum = (*act)[i * 3 + 2];
      if (qnum >= 0)
	(*qrymap)[qnum] = i;
    }
  }

  if (cmpqry != NULL) qc_dealloc(&cmpqry);
}
