/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : nameprse						nameprse.c

Function : Parses name field of TIPS document descriptor into it's component 
	pieces.

Author : A. Fregly

Abstract : This command will parse the supplied TIPS document descriptors and 
	print out desired components of the name field. If multiple components 
	are specified, the components will be separated by pound signs, "#",
	so that the output is a valid name field for a TIPS document descriptor.

Calling Sequence : nameprse {switches} {descriptor} {descriptor...}

  Switches

	-c	Read descriptors from standard input instead of from
		command line.
	-a	Output archive name portion of the name field.
	-n	Output document number portion of the name field.
	-f	Output file name portion document of the descriptor.

  Parameter:

	descriptor	Document descriptor to be parsed.

Notes: 

Change log : 
000	26-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

extern int errno;

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *(*argv);
#endif
{

  int doarchive, donum, dofile, didany, putpound, readinput;
  char progname[FIT_FULLFSPECLEN+1], *optptr, *descriptor;
  char inbuf[TIPS_DESCRIPTORMAX+1];
  char name[TIPS_DESCRIPTORMAX+1], key[TIPS_DESCRIPTORMAX+1];
  char archive[TIPS_DESCRIPTORMAX+1], docnum[TIPS_DESCRIPTORMAX+1];
  char file[TIPS_DESCRIPTORMAX+1];
  long len;

  doarchive=0;
  donum=0;
  dofile=0;
  didany = 0;
  readinput = 0;

  if (!--argc) goto DONE;
  strcpy(progname, *(argv++));

  if (!strcmp(*argv, "?")) goto SYNTAX;

  for (; argc && (*argv)[0] == '-'; --argc, ++argv)
    for (optptr = *argv + 1; *optptr; ++optptr)
      switch (*optptr) {
        case 'a' : case 'A' :
	  doarchive = 1;
	  break;
        case 'n' : case 'N' :
	  donum = 1;
	  break;
	case 'f' : case 'F' :
	  dofile = 1;
	  break;
	case 'c' : case 'C' :
	  readinput = 1;
	  break;
	default:
	  fprintf(stderr, "Invalid switch: %c\n", *optptr);
	  goto SYNTAX;
      }

  if (!doarchive && !donum && !dofile) {
    doarchive = 1;
    donum = 1;
    dofile = 1;
  }
  putpound = ((doarchive + donum + dofile) > 1);

  while (1) {
    if (!readinput) {
      if (!argc--) break;
      descriptor = *(argv++);
    }
    else {
      if (fgets(inbuf, TIPS_DESCRIPTORMAX, stdin) == NULL) break;
      inbuf[TIPS_DESCRIPTORMAX] = 0;
      descriptor = inbuf;
    } 
    len = 0;
    archive[0] = 0;
    docnum[0] = 0;
    file[0] = 0;
    tips_parsedsc(descriptor, name, &len, key);
    if (*name) tips_nameparse(name, archive, docnum, file);
    if (doarchive && *archive || donum && *docnum || dofile && *file) {
      if (doarchive && *archive) fputs(archive, stdout);
      if (putpound) fputc('#', stdout);
      if (donum && *docnum) fputs(docnum, stdout);
      if (putpound) fputc('#', stdout);
      if (dofile && *file) fputs(file, stdout);
      didany = 1;
      if (readinput) {
	fputc('\n', stdout);
	fflush(stdout);
      }
      else if (argc)
        fputc(' ', stdout);
    }
  }
  goto DONE;

SYNTAX:
  fprintf(stderr, "Usage: %s {switches} {descriptor {descriptor...}}\n", 
     progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr, "  -c\t\tContinuous mode, read descriptors from standard \
input\n");
  fprintf(stderr, "  -a\t\tOutput archive name field\n");
  fprintf(stderr, "  -n\t\tOutput document number field\n");
  fprintf(stderr, "  -f\t\tOutput file name field\n");
  fprintf(stderr, "Parameter:\n");
  fprintf(stderr, "  descriptor\tDocument descriptor(s) to be parsed\n");

DONE:
  return !didany;
}
