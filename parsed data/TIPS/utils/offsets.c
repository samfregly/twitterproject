/***************************************************************************

                       Copyright 1997, Andrew M. Fregly

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of Andrew M. Fregly.

    Andrew M. Fregly makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Andrew M. Fregly
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.
*******************************************************************************/

/******************************************************************************
Module Name : offsets                                           offsets.c

Function : Returns offsets of words matching query terms.

Author : A. Fregly

Abstract : offsets searches standard input using the supplied query and prints
	out the character offset and length of each consecutive series of characters
	which match query terms. These offsets and lenght pairs are printed one per 
	output line.

Calling Sequence : offsets {switches} query

  Switches:
    -d hex                      This switch is used to specify the
                                character used as a beginning of
                                field delimiter. Whenever the field
                                delimiter character is seen in the
                                input text stream, the following
                                character is interpreted as a field
                                number.
    -l object_lib		Load queries from specified object library
    -n field_file               The field file should contain a list of
                                field names in an order corresponding to
                                the field numbers used within the input
                                stream. The first entry in the field file
                                corresponding to field 0, the second to
                                field 1,... The field name are taken as the
                                first word on each line of the field file,
                                the remainder of the line being user
                                definable.
    -o objqry {objqry...}       Object query name list.
    -q srcqry {srcqry...}       Source query file name list. A default file
                                type of ".qry" is assumed if the files do
                                not have a file type.
    -s				Show search statistics
    -t query_text               Query text.
    -v				Verbose mode, progress messages are sent to 
				stderr..
    -x query_executable		Load queries contained in specified query
				executable.
 

Notes:

Change log :
000     06-MAY-97  AMF  Created.
001     17-JUL-97  AMF	Added -v switch.
002     09-JUL-98  AMF  Use matchinfo functions. Fixed test for end of document
                        at top of search loop. Fixed files searched counter.
                        Changed default name of created query, object, and hit 
                        files to match program name. Fix index error in 
                        printerrors. Direct query compilation errors to stderr
                        instead of stdout.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <string.h>
#include <ctype.h>

#if FIT_DOS
#include <io.h>
#endif
#if FIT_MSC
#include <direct.h>
#endif

#include <fcntl.h>
#if FIT_UNIX || FIT_COHERENT
#if FIT_SUN
#include <sys/types.h>
#include <sys/time.h>
#include <sys/timeb.h>
#else
#include <sys/times.h>
#endif
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <drct.h>
#include <tips.h>

#if !FIT_VMS
#define QRYEXT ".qry"
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".hit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.hit"
#else
#define QRYEXT ".aqry"
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".ahit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.ahit"
#endif

#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif

#define BUFFERSIZE 32768 

static void printerrors();

static   char exename[FIT_FSPECLEN+1],queryspec[FIT_FSPECLEN+1];
static   char objspec[FIT_FSPECLEN+1],hitspec[FIT_FSPECLEN+1];
static char sweepspec[FIT_FSPECLEN+1];
static   char fieldfile[FIT_FSPECLEN+1];
static   char searchspec[FIT_FSPECLEN+1],infile[FIT_FSPECLEN+1];
static   char outfile[FIT_FSPECLEN+1],querytext[TIPS_QUERYTEXTMAX+1];
static   char objlibname[FIT_FSPECLEN+1], inputstreamname[FIT_FSPECLEN+1];
static   char progname[FIT_FSPECLEN+1], dummyspec[FIT_FSPECLEN+1];
static   char docdescriptor[TIPS_DESCRIPTORMAX+1];
static   char key[TIPS_DESCRIPTORMAX+1];
static   unsigned char buffer[BUFFERSIZE+1];
static   char doclen_s[14];
static char *matchinfo;

static   FILE *in, *out;

#if !FIT_UNIX && !FIT_COHERENT
static   char *context;
#endif

#if FIT_DOS
#define READ "rb"
#else
#define READ "r"
#endif

static   struct qc_s_cmpqry *cmpqry;
static   char *xe;
static   struct prf_s_exe *exe;

static   unsigned char *bufptr=NULL, *endbuf=NULL, *endptr=NULL;
static   nbytes, textlen, searchstat, i, len, offset;
static   int nerrs,srcpresent,nsearched, showstats, nfiles;
static   int ntext, append, archive, readlen, doff, enabfields;
static   long doclen, inlen;
static   char *qtext, defpath[FIT_FULLFSPECLEN+1];
static   char *(*objptr), *(*objlibptr), *(*fileptr), *(*qrytextptr), *(*srcptr);
static   int nobj, nobjlib, nsrc, ineof, verbose;
static   unsigned prot;
  
static  fit_s_timebuf startprog,endprog,timedif,searchtime;
#if !FIT_UNIX && !FIT_COHERENT || 1
static  fit_s_timebuf startsearch,endsearch;
#else
static  struct tms tmsbuf;
static   long startsearch, endsearch, searchtime_l;
#endif
static   long srate,trate;
static   double dtotalbytes,dstime,searchmillisecs,dsrate,dtrate;
static   struct drct_s_file *hitfile, *objlib, *sweepfile;

  /* Initialize strings, pointers, counters */
static   long totalbytes;
static char *(*fields), *(*(fielddata));
static int nfields, fielddelim, noffsets;
static unsigned long *woffsets;
static unsigned *wlengths;

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  totalbytes = 0;
  showstats = 0;
  prot = 0;
  verbose = 0;

  exename[0] = 0;
  queryspec[0] = 0;
  objspec[0] = 0;
  searchspec[0] = 0;
  querytext[0] = 0;
  objlibname[0] = 0;
  sweepspec[0] = 0;
  srcpresent = 0;
  xe = NULL;
  exe = NULL;
  cmpqry = NULL;
  qtext = NULL;
  showstats = 0;
  nobj = 0;
  nsrc = 0;
  ntext = 0;
  nobjlib = 0;
  nfiles = 0;
  append = 0;
  archive = 0;
  nfields = 0;
  fields = NULL;
  fielddata = NULL;
  fieldfile[0] = 0;
  fielddelim = 0xff;
  matchinfo = (char *) NULL;
  wlengths = (unsigned *) NULL;
  woffsets = (unsigned long *) NULL;

  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname, dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

#if !FIT_VMS
  getcwd(defpath, FIT_FSPECLEN);
#else
  defpath = NULL;
#endif

  /* Pull off switches */
  for (; argc > 0 && (*(*argv) == '-' || *(*argv) == '+'); --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'a' : case 'A' :
	archive = 1;
	break;
      case 'd' : case 'D' :
	--argc;
	sscanf(*(++argv),"%x",&fielddelim);
	break;
      case 'f' : case 'F' :
	for (nfiles = 0, fileptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nfiles, ++argv)
	  ;
	break;
      case 'l' : case 'L' :
	for (nobjlib = 0, objlibptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nobjlib, ++argv)
	  ;
	break;
      case 'n' : case 'N' :
	if (!--argc) goto SYNTAX;
	strcpy(fieldfile,*(++argv));
	if (!(enabfields = tips_loadflds(fieldfile, &fields, &fielddata,
		&nfields)))
	  fprintf(stderr,"Error loading field definitions from: %s\n",
		fieldfile);
/*****
	else { 
	  int i;
	  for (i=0;i<nfields;++i) 
	    printf("%s: %s\n", fields[i], fielddata[i]);
	}
*****/
	break;
      case 'o' : case 'O' :
	for (nobj = 0, objptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nobj, ++argv)
	  ;
	break;
      case 'q': case 'Q' :
	for (nsrc = 0, srcptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++nsrc, ++argv)
	  ;
	break;
      case 's' : case 'S' :
	showstats = 1;
	break;
      case 't': case 'T' :
	for (ntext = 0, qrytextptr = argv+1; argc > 1 && *(*(argv+1)) != '-' &&
	    *(*(argv+1)) != '+'; --argc, ++ntext, ++argv)
	  ;
	break;
      case 'v': case 'V' :
	verbose = 1;
        break;
      case 'x' : case 'X' :
	if (!--argc) goto SYNTAX;
	strcpy(exename,*(++argv));
	break;
      default :
	goto SYNTAX;
    }
  }

  /* Initialize profile executable */
  if (exename[0]) {
    fit_expfspec(exename,EXEEXT,defpath,exename);
    fprintf(stderr, "Loading profile executable: %s\n",exename);
    if (!prf_read(exename,&xe)) {
      perror("*** Unable to load profile executable");
      goto DONE;
    }
  }
  else {
    if (!enabfields && archive && nfiles) {
      fit_setext(*fileptr, ".fld", fieldfile);
      enabfields = tips_loadflds(fieldfile,&fields,&fielddata,&nfields);
    }
    if (!enabfields && nobjlib) {
      fit_setext(*objlibptr, ".fld", fieldfile);
      enabfields = tips_loadflds(fieldfile,&fields,&fielddata,&nfields);
    }
    if (!prf_init(TIPS_MAXTERMWORDLEN, NULL, nfields, fields, &xe)) {
      perror("*** Error initializing profile executable");
      goto DONE;
    }
  }
  exe = (struct prf_s_exe *) xe;

  if (nsrc) {
    if (verbose)
      fprintf(stderr, "Loading query source files:\n");
    for (; nsrc; --nsrc, ++srcptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *srcptr, NORMAL_FILE,
	QRYEXT, defpath, infile);) {
#else
      fit_expfspec(*srcptr, QRYEXT, defpath, infile);
#endif
	if (verbose)
	  fprintf(stderr, "  %s\n",infile);
	if ((in = fopen(infile,"r")) != NULL) {
	  len = fread(querytext, 1, TIPS_QUERYTEXTMAX, in);
	  if (len > 0) {
	    *(querytext+len) = 0;
	    nerrs = qc_compile(querytext, nfields, fields, 0, NULL, NULL, 
		&cmpqry);
	    if (nerrs) {
	      fprintf (stderr,"*** Query %s contains %d errors\n",infile,nerrs);
	      printerrors(querytext,nerrs);
	    }
	    else {
	      fit_setext(infile,OBJEXT,outfile);
	      qc_writeqry(outfile,querytext,cmpqry);
	      if (!prf_addqry(outfile,cmpqry,xe)) {
		perror("*** Error linking query into profile executable");
	        fprintf(stderr, "..query: %s\n", infile);
		goto DONE;
	      }
	    }
	  }
	  fclose(in);
	}
	else
	  fprintf(stderr, "*** Error opening query source file");
      }
#if !FIT_UNIX && !FIT_COHERENT
    }
#endif
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (nobj) {
    if (verbose)
      fprintf(stderr, "Loading query object files:\n");
    for (; nobj; --nobj, ++objptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *objptr, NORMAL_FILE,
	OBJEXT, defpath, infile);) {
#else
      fit_expfspec(*objptr, OBJEXT, defpath, infile);
#endif
        if (verbose)
	  fprintf(stderr, "  %s\n",infile);
	if (qc_readqry(infile,&qtext,&cmpqry)) {
	  if (!prf_addqry(infile,cmpqry,xe)) {
	    perror("*** Error linking query into profile executable");
	    fprintf(stderr, "..query: %s\n", infile);
	    goto DONE;
	  }
	}
	else
	  perror("*** Error loading query object file");
	  fprintf(stderr, "..query: %s\n", infile);
#if !FIT_UNIX && !FIT_COHERENT
      }
#endif
    }
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (nobjlib) {
    if (verbose)
      fprintf(stderr, "Loading query object libraries:\n");
    for (; nobjlib; --nobjlib, ++objlibptr) {
#if !FIT_UNIX && !FIT_COHERENT
      for (context = NULL; *fit_findfile(&context, *objlibptr, NORMAL_FILE,
	  ".qlb", defpath, infile);) {
#else
      fit_expfspec(*objlibptr, ".qlb", defpath, infile);
#endif
        if (verbose)
	  fprintf(stderr, "  %s...\n",infile);
	if ((objlib = qc_openlib(infile, O_RDWR, prot)) != NULL) {
	  for (cmpqry = NULL; qc_nextlib(objlib, infile, &cmpqry) >= 0;) {
	    fprintf(stderr, "  ...%s\n",infile);
	    if (!prf_addqry(infile, cmpqry, xe)) {
	      perror("*** Error linking query into profile executable");
	      fprintf(stderr, "..query: %s\n", infile);
	      goto DONE;
	    }
	  }
	  drct_close(objlib);
	}
	else
	  perror("*** Error opening query object library");
	  fprintf(stderr, "..query: %s\n", infile);
#if !FIT_UNIX && !FIT_COHERENT
      }
#endif
    }
    if (cmpqry != NULL) qc_dealloc(&cmpqry);
  }

  if (ntext) {
    /* Read in text of command line query */
    for (offset = -1; ntext; --ntext, ++qrytextptr) {
      fit_sadd(querytext, TIPS_QUERYTEXTMAX, *qrytextptr, &offset);
      if (ntext-1)
	fit_sadd(querytext,TIPS_QUERYTEXTMAX," ",&offset);
    }
    querytext[min(offset+1,TIPS_QUERYTEXTMAX)] = 0;
	srcpresent = *querytext;
  }

  if (srcpresent) {
    if (nerrs = qc_compile(querytext, nfields, fields, 0, NULL, NULL, 
	&cmpqry)) {
      fprintf (stderr, "*** Query contains %d errors\n",nerrs);
      printerrors(querytext,nerrs);
    }
    else {
      fit_expfspec(progname,DEFOBJ,defpath,outfile);
      qc_writeqry(outfile,querytext,cmpqry);
      if (!prf_addqry(outfile, cmpqry, xe)) {
	perror("*** Error linking query into profile executable");
	goto DONE;
      }
    }
  }

  if (!exe->nqueries) {
    fprintf(stderr, "No queries!  Search canceled\n");
    goto DONE;
  }

  if (cmpqry != NULL) qc_dealloc(&cmpqry);

  endbuf = buffer + BUFFERSIZE;
  searchstat = 1;
  fit_zerotime(&searchtime);
  fit_gettime(&startprog);
  nsearched = 0;
  
  for (; nfiles; --nfiles, ++fileptr) {
#if !FIT_UNIX && !FIT_COHERENT
    for (context = NULL; *fit_findfile(&context, *fileptr, NORMAL_FILE,
	"", defpath, infile);) {
#else
    fit_expfspec(*fileptr, "", defpath, infile);
#endif
    searchstat |= 4;
    in = fopen(infile, READ);
    doclen = 0;

    if (in != NULL && !archive) {
      for (bufptr = buffer, nbytes = fread(buffer, 1, BUFFERSIZE, in),
	  doclen = nbytes;
	  nbytes;
	  nbytes = fread(bufptr, 1, endbuf-bufptr, in), doclen += nbytes ) {

	ineof = feof(in);

        if (showstats) {
	  nsearched += (ineof != 0);
#if !FIT_UNIX && !FIT_COHERENT || 1
	  fit_gettime(&startsearch);
#else
	  startsearch = times(&tmsbuf);
#endif
        } 

	endptr = bufptr + nbytes - 1; 
	if (!ineof && nbytes == (endbuf-bufptr))
	  /* Find last delmiter char in buffer */
	  while (endptr > bufptr && *(exe->tx_xlate+*endptr) >= 0)
	    --endptr;
	else
	  /* Set last buffer in document flag bit */
	  searchstat |= 2;

	buffer[nbytes] = ' ';

	/* Set length of text to search */
	textlen = endptr - buffer + 1;

	/* Search next buffer in document */
	noffsets = 0;

        prf_search_matchinfo(searchstat, 0, xe, buffer, textlen,
	  &matchinfo);

	if (textlen < nbytes) {
	  /* Move unsearched text in buffer to beginning of buffer */
	  memmove(buffer, endptr+1, nbytes - textlen + 1);

	  /* Set start of buffer pointer at loc beyond moved text */
	  bufptr = buffer + nbytes - textlen;
	}
	else
	  bufptr = buffer;

	/* clear flag bits */
	searchstat = 0;

	if (showstats) {
#if !FIT_UNIX && !FIT_COHERENT || 1
	  fit_gettime(&endsearch);
	  fit_timedif(&startsearch, &endsearch, &timedif);
	  fit_addtime(&searchtime, &timedif, &searchtime);
#else
	  endsearch = times(&tmsbuf);
	  searchtime_l += (endsearch - startsearch);
#endif
	}
      }
    
      totalbytes += doclen;
      
      /* Close the input text file */
      fclose(in);

      /* Print offsets */
      if (prf_matchinfo_highlightinfo(matchinfo, &noffsets,
	  &woffsets, &wlengths)) {
        for (i=0; i < noffsets; ++i)
          printf("%ld, %d\n",woffsets[i], wlengths[i]);
	free(woffsets);
	woffsets = (unsigned long *) NULL;
	free(wlengths);
	wlengths = (unsigned *) NULL;
      }
    }

    else if (in != NULL) {
      /* Processing an archive */
      strcpy(inputstreamname, infile);

      while (!feof(in)) {
	/* Skip over any leading nulls in the input stream */
	fscanf(in, " %c", docdescriptor);

	if (feof(in)) break;

	/* Push the non-null character back onto the input stream */
	ungetc(*docdescriptor, in);

	*docdescriptor = 0;
	fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,in);
	if (feof(in)) break;

	len = strlen(docdescriptor);
	if (len) docdescriptor[len-1] = 0;

	if (!tips_parsedsc(docdescriptor, infile, &doclen, key)) {
	  fit_logmsg(stderr,"*** Error parsing input descriptor",1);
	  goto DONE;
	}
	
	if (showstats) {
	  nsearched += 1;
	}

	for (searchstat |= 4, bufptr = buffer, inlen = doclen;
	  !feof(in) && inlen; inlen -= nbytes) {

	  if (endbuf-bufptr < inlen)
	    readlen = endbuf-bufptr;
	  else
	    readlen = (int) inlen;

	  if (!(nbytes = fread(bufptr, 1, readlen, in))) {
	    fprintf(stderr, "\n*** Read error on archive: %s\n",inputstreamname);
	    fprintf(stderr, "    document: %s/n",docdescriptor);
	    perror("");
	    goto DONE;
	  }

	  if (showstats) {
#if !FIT_UNIX && !FIT_COHERENT || 1
	    fit_gettime(&startsearch);
#else
	    startsearch = times(&tmsbuf);
#endif
	  }
	  endptr = bufptr + nbytes - 1;

	  if (inlen > nbytes)
	    /* Find last delmiter char in buffer */
	    while (endptr > bufptr && *(exe->tx_xlate+*endptr) > 0)
	      --endptr;
	  else {
	    /* Set last buffer in document flag bit */
	    searchstat |= 2;
	  }

	  buffer[nbytes] = ' ';

	  /* Set length of text to search */
	  textlen = endptr - buffer + 1;

	  /* Search next buffer in document */
	  noffsets = 0;

          prf_search_matchinfo(searchstat, 0, xe, buffer, textlen, 
		&matchinfo);

	  if (textlen < nbytes) {
	    /* Move unsearched text in buffer to beginning of buffer */
	    memmove(buffer, endptr+1, nbytes - textlen + 1);

	    /* Set start of buffer pointer at loc beyond moved text */
	    bufptr = buffer + nbytes - textlen;
	  }
	  else
	    bufptr = buffer;

	  /* clear flag bits */
	  searchstat = 0;
	  if (showstats) {
#if !FIT_UNIX && !FIT_COHERENT || 1
	    fit_gettime(&endsearch);
	    fit_timedif(&startsearch, &endsearch, &timedif);
	    fit_addtime(&searchtime, &timedif, &searchtime);
#else
	    endsearch = times(&tmsbuf);
	    searchtime_l += (endsearch - startsearch);
#endif
	  }
	}

	totalbytes += doclen;
        /* Print offsets */
	if (prf_matchinfo_highlightinfo(matchinfo, &noffsets,
	    &woffsets, &wlengths)) {
          for (i=0; i < noffsets; ++i)
            printf("%ld, %d\n",woffsets[i], wlengths[i]);
	  free(woffsets);
	  woffsets = (unsigned long *) NULL;
	  free(wlengths);
	  wlengths = (unsigned *) NULL;
	}
      }
    }
#if !FIT_UNIX && !FIT_COHERENT
    }
#endif
  }

  if (showstats && totalbytes) {
    fit_gettime(&endprog);
	fprintf(stderr, "     Bytes      Files\n");
	fprintf(stderr, "     -----      -----\n");
    fprintf(stderr, "%10ld  %9d\n\n",totalbytes,nsearched);

#if FIT_UNIX && 0 || FIT_COHERENT && 0
    if (searchtime_l < 0) searchtime_l = -searchtime_l;
    searchtime.time = searchtime_l / 100;
    searchtime.millitm = (searchtime_l % 100) * 10;
#endif

    fit_timedif(&startprog, &endprog, &timedif);

    fprintf(stderr, "Total search time,     %5lu.%03.3u seconds\n",
	fit_seconds(&timedif), fit_millisecs(&timedif));
    fprintf(stderr, "Match processing time, %5lu.%03.3u seconds\n",
      fit_seconds(&searchtime), fit_millisecs(&searchtime));
    fit_timedif(&timedif, &searchtime, &timedif);
    fprintf(stderr, "I/O overhead time,     %5lu.%03.3u seconds\n",
      fit_seconds(&timedif), fit_millisecs(&timedif));

    fit_timedif(&startprog, &endprog, &timedif);

    fprintf(stderr, "Search rates per second\n");

    dtotalbytes = totalbytes;
    dtotalbytes *= 1000.0;

    dstime = (fit_seconds(&timedif) * 1000 + fit_millisecs(&timedif));
    if (!dstime) dstime = 1;
    dsrate = dtotalbytes / dstime;
    srate = dsrate;
    fprintf(stderr, "  Search Processing Overall: %7ld\n",srate);

    searchmillisecs = (fit_seconds(&searchtime) * 1000 +
	fit_millisecs(&searchtime));
    if (!searchmillisecs) searchmillisecs = 1;
    dtrate = dtotalbytes / searchmillisecs;
    trate = dtrate;
    fprintf(stderr, "  Match processing:          %7ld\n",trate);
  }
  else if (showstats)
    fprintf(stderr, "\n*** No text was searched\n");

  goto DONE;

SYNTAX:
  fprintf(stderr, "Usage: %s {switches}\n", progname);
  fprintf(stderr, "Switches:\n");
  fprintf(stderr, "  -a\t\t\tFiles to be searched are TIPS archives.\n");
  fprintf(stderr, "  -f file {file...}\tList of files to search.\n");
  fprintf(stderr, "  -l objlib {objlib...}\tList of query object libraries.\n");
  fprintf(stderr, "  -n field_file\t\tField definition file.\n");
  fprintf(stderr, "  -o objqry {objqry...}\tList of object queries.\n");
  fprintf(stderr, "  -q srcqry {srcqry...}\tList of source queries.\n");
  fprintf(stderr, "  -s\t\t\tShow search statistics.\n");
  fprintf(stderr, "  -t querytext\t\tQuery text.\n");
  fprintf(stderr, "  -x qryexe\t\tQuery executable.\n");

DONE:
  return(0);
}

#if FIT_ANSI
void printerrors(char *querytext, int nerrs)
#else
void printerrors(querytext, nerrs)
  char *querytext;
  int nerrs;
#endif
{
  int i,n, errnum;
  char *errmsg;
  for (i=0; i < nerrs; ++i) {
    errmsg = qc_geterr(i,&errnum, &n);
    qc_puterr(stderr,querytext,errmsg,n);
  }
}
