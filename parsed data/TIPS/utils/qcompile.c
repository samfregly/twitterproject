/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qcompile                                          qcompile.c

Function : Compiles queries

Author : A. Fregly

Abstract : This program is used to compile a query and produce an output
	object query file.  The object query will have a file type
	of ".QC".  If any errors occur during compilation of a file, the
	errors will be placed in  a ".ERR" file.

Calling Sequence : qcompile {-o query_object_lib} fname {fname...}

  Switches:
	-o object_lib   Create/Update specified query object library. Query
			object libraries have a default file type of .qlb.
	-p mask         Set protection mask for created object libraries.

  Parameters:
	fname   One or more file names of files containing the source text
		for a query to be compiled. A default file type of ".qry"
		is assumed.

Notes :

Change log :
001     16-SEP-91  AMF  Added protection.
002     15-DEC-92  AMF  Compile under COHERENT.
003	25-AUG-03  AMF	Temp fix to errno for LINUX
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_TURBOC
#include <dir.h>
#elsif FIT_DOS
#include <direct.h>
#endif
#if FIT_MSC
#include <sys/stat.h>
#endif
#include <fcntl.h>
#include <drct.h>
#include <qc.h>

#define null 0
#define K_TEXTLEN 4096

#if !FIT_LINUX
  extern int errno;
#else
  int errno=1;
#endif

  char infilename[FIT_FULLFSPECLEN+1];
  char outfile[FIT_FULLFSPECLEN+1];
  char errfile[FIT_FULLFSPECLEN+1];
  char objlibname[FIT_FULLFSPECLEN+1];
  char progname[FIT_FULLFSPECLEN+1],dummyspec[FIT_FULLFSPECLEN+1];

#if !FIT_VMS
#define QRYFILETYPE ".qry"
#else
#define QRYFILETYPE ".asapqry"
#endif

#if FIT_KR
int main(argc, argv)
  int argc;
  char *argv[];
#else
int main(int argc, char *argv[])
#endif
{

  int i, nerrs, n, len, errnum;
  char *errmsg, *text;
  char defpath[FIT_FULLFSPECLEN+1];
  unsigned prot;
  int stat = 2;

  FILE *in, *err;
#if !FIT_UNIX && !FIT_COHERENT
  char *context=(char *) NULL;
#if FIT_MSC
#define NORMAL_FILE 0
#else
#define NORMAL_FILE S_IFREG
#endif
#else
#define NORMAL_FILE S_IFREG
#endif

  struct qc_s_cmpqry *cmpqry;
  struct drct_s_file *objlib;

  prot = 0;
  objlib = NULL;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;
#if !FIT_VMS
  getcwd(defpath,FIT_FULLFSPECLEN);
#else
  defpath = NULL;
#endif

  for (; argc && (*argv)[0] == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'o' : case 'O' :
	if (!--argc) goto SYNTAX_ERROR;
	strcpy(objlibname,*(++argv));
	fit_expfspec(objlibname, ".qlb", defpath, objlibname);
	if ((objlib = qc_openlib(objlibname, O_RDWR, prot)) == NULL)
	if ((objlib = qc_openlib(objlibname, O_RDWR + O_CREAT, prot)) == 
	    NULL) {
	  fprintf(stderr,
	    "*** Unable to open or create compiled query library: %s\n",
	    objlibname);
	  goto ERREXIT;
	}
	break;
      case 'p' : case 'P' :
	if (!--argc) goto SYNTAX_ERROR;
	sscanf(*(++argv), "%o", &prot);
	break;
      default :
	break;
    }
  }

  if (!argc) goto SYNTAX_ERROR;

  text = NULL;

  cmpqry = NULL;

  text = (char *) malloc(K_TEXTLEN + 1);

  if (text == NULL) {
    fprintf (stderr,"*** Memory allocation error\n");
    goto ERREXIT;
  }


  for (; argc; --argc, ++argv) {
#if FIT_UNIX || FIT_COHERENT
    fit_expfspec(*argv , QRYFILETYPE, defpath, infilename);
#else
    for (context=NULL; *fit_findfile((void *(*)) &context, *argv, NORMAL_FILE, "",
	  defpath, infilename);) {
#endif
    printf("%s\n",infilename);

    if (objlib == NULL)
      fit_setext(infilename,".qc",outfile);

    fit_setext(infilename,".err",errfile);

    if ((in = fopen(infilename,"r")) != NULL) {
      len = fread(text, 1, K_TEXTLEN - 1, in);
      if (len > 0) {
	*(text+len) = 0;
	nerrs = qc_compile(text, 0, NULL, 0, NULL, NULL, &cmpqry);
	if (nerrs) {
	  printf ("*** Query contains %d errors\n",nerrs);
	  err = fopen(errfile,"w");
	  if (err != NULL)
	    for (i=1; i <= nerrs; ++i) {
	      errmsg = qc_geterr(i,&errnum,&n);
	      qc_puterr(stdout,text,errmsg,n);
	      qc_puterr(err,text,errmsg,n);
	    }
	  else {
	    fprintf(stderr,"*** Error opening error file %s\n",errfile);
	    goto ERREXIT;
	  }
	}
	else {
	  if (objlib == NULL) {
	    if (!qc_writeqry(outfile, text, cmpqry)) {
	      fprintf(stderr,"*** Unable to write object file: %s\n",
		  outfile);
	      goto ERREXIT;
	    }
	  }
	  else {
            if (qc_putlib(objlib, infilename, cmpqry) < 0) {
	      fprintf(stderr,
		"*** Unable to add query to object library, %s\n",infilename);
	      goto ERREXIT;
	    }
          }
	}
      }
      else
	fprintf(stderr,"*** Unable to read file\n",infilename);
    }
    else
      fprintf(stderr,"*** Error opening file: %s\n",infilename);
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr,"*** Syntax error\n");

SYNTAX:
  printf("Usage: %s {switches} query_name_list\n", progname);
  printf("Switches:\n");
  printf("  -o objlib\t\tQuery object library to be updated.\n");
  printf("  -p mask\t\tSet created object library protection.\n");
  printf("Parameters:\n");
  printf("  query_name_list\tQuery file specification(s).\n");
  goto DONE;

ERREXIT:
  perror("");
  stat = errno;

DONE:
  if (objlib != NULL) qc_closelib(objlib);

#if !FIT_VMS
  return stat;
#else
  return (!stat) ? 1 : stat;
#endif
}
