/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : rmvmsgq						rmvmsgq.c

Function : This program removes a no longer needed message queue.

Author : A. Fregly

Abstract : This program is used to clean up after TIPS daemons which have
	been deactivated. It will remove the message queues used to communicate
	with the daemon using the specified queue name.

Calling Sequence : rmvmsgq file_spec

  file_spec	File spec used for message queue.


Notes: 

Change log : 
000	17-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

extern int errno;


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  key_t msgkey;
  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int msgqid;


  /* Set program name */
  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname,
        dummyspec, dummyspec);
  --argc;

  if (!argc || (*argv)[0] == '?') goto SYNTAX;

  /* Attach to the message queue */
  msgkey = ftok(*argv, TIPS_FTOKSEED);
  if ((msgqid = msgget(msgkey, 0)) < 0) {
    perror("*** Unable to attach to message queue");
    exit(errno);
  }

  /* Send the message and possibly wait for a reply */
  if (msgctl(msgqid, IPC_RMID, NULL) == -1) {
    perror("*** unable to remove message queue");
    exit(errno);
  }
  return 0;

SYNTAX:
  /* Syntax error or no command line switches or parameters or help
     requested. Write usage to standard error */
  fprintf(stderr,"Usage: %s file_spec\n", progname);
  fprintf(stderr,"Parameter:\n");
  fprintf(stderr,"  file_spec\tFile name for message queue\n");
  return 0;
}


