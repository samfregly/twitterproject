/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fprep                                             fprep.c

Function : Front end loader filter which prefixes data with tips document
	descriptor.

Author : A. Fregly

Abstract : fprep is the standard tips front end filter used in processing
	user data so that each processed file will have a TIPS descriptor.
	fprep will copies the files to standard output so that they may
	serve as input for other filters. This format is suitable for input
	to many of the tips filters, including spotter and arcload.

Calling Sequence : fprep {switches} {file_name {filename...}}

  Switches:

	-d              Specifies that processed files are to be deleted.
	-l              Enables logging of status messages to standard error.
	-t              Don't write document text to standard output.
        -p {port}       Port number of socket used for communicating with
                        fprep. The default port number is defined in tips.h 
                        with the constant TIPS_FPREP_DEFAULT_PORT.


  Parameters            List of file names to be loaded.

Notes:

Change log :
000     22-JUL-91  AMF  Created.
001     15-DEC-92  AMF  Compile under COHERENT.
002     30-DEC-03  AMF  Use sockets for communication
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <fcntl.h>
#include <sys/stat.h>

#if FIT_DOS
#include <io.h>
#endif
#include <tips.h>

#define BUFFERSIZE 0x100000 
char buffer[BUFFERSIZE+1];

#if FIT_DOS
#define READONLY "rb"
#define NORMAL_FILE 0
#else
#define READONLY "r"
#endif

/* Globals */
extern int errno;
#if FIT_ANSI
void fprep_process(char *infile, int delete, int log, int writetext);
#else
void fprep_process();
#endif

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int log, stat, delete, writetext;
  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  char infile[FIT_FULLFSPECLEN+1];
  char defpath[FIT_FULLFSPECLEN+1];
  char *port_s=(char *) NULL;
#if FIT_DOS
  char *context;
#endif

  stat = 1;
  log = 0;
  delete = 0;
  writetext = 1;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv,"?")) goto SYNTAX;

  getcwd(defpath,FIT_FULLFSPECLEN);

  /* Pick off command arguements */
  for (; argc && *(*argv) == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'd' : case 'D' :
	delete = 1;
	break;
      case 'l' : case 'L' :
	log = 1;
	break;
      case 'p' : case 'P' :
        if (argc - 1 && *(argv[1]) != '-') {
          --argc;
          port_s = *(++argv);
        }
        break;
      case 't' : case 'T' :
	writetext = 0;
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  if (!argc) goto SYNTAX_ERROR;

#if FIT_DOS
  if (!fit_binmode(stdout)) goto ERREXIT;
#endif

  /* Process the file list specified on the command line */
  for (; argc; --argc, ++argv) {
#if FIT_UNIX || FIT_COHERENT  
    fit_expfspec(*argv, "", defpath, infile);
#else
  for (context = NULL; *fit_findfile(&context, *argv, NORMAL_FILE,
	"", defpath, infile);) {
#endif
    fprep_process(infile,delete,log,writetext);
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  fprintf(stderr,"*** Invalid command syntax\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} filespec {,filespec...}\n",progname);
  fprintf(stderr,"  -d\t\tDelete processed files\n");
  fprintf(stderr,"  -l\t\tEnable status logging to standard error\n");
  fprintf(stderr,"  -p {port}\t\tSet message port.\n");
  fprintf(stderr,"  -t\t\tDisable output of document text\n");
  goto DONE;

#if FIT_DOS
ERREXIT:
  perror("");
  stat = errno;
#endif

DONE:
  return stat;
}

#if FIT_ANSI
void fprep_process(char *fname, int delete, int log, int writetext)
#else
void fprep_process(fname, delete, log, writetext)
  char *fname;
  int delete, log, writetext;
#endif
{
  FILE *in;
  struct stat statbuf;
  char descript[TIPS_DESCRIPTORMAX+1];
  long inlen;
  int nbytes, len, infd;

  if (writetext) {
    /* Open input file */
#if !FIT_DOS
    if ((infd = open(fname, O_RDONLY, 0)) < 0) {
      fprintf(stderr,"*** Unable to open file %s:",fname);
      perror("");
      return;
    }
    if ((in = fdopen(infd, READONLY)) == NULL) {
#else
    if ((in = fopen(fname,READONLY)) == NULL) {
#endif
      fprintf(stderr,"*** Unable to open file %s:",fname);
      perror("");
      return;
    }

#if FIT_DOS
    infd = fileno(in);
#endif

  /* Determine size of input file */
    if (fstat(infd, &statbuf)) {
      fprintf(stderr,"*** Unable to retrieve file status for %s:",fname);
      perror("");
      return;
    }
  }
  else if (stat(fname, &statbuf)) {
    fprintf(stderr,"*** Unable to retrieve file status for %s:",fname);
    perror("");
    return;
  }

  /* write document descriptor */
  sprintf(descript,"%s,%ld\n",fname,statbuf.st_size);
  len = strlen(descript);
  fwrite(descript, 1, len, stdout);

  /* Copy the file to standard output */
  for (inlen = statbuf.st_size, nbytes = BUFFERSIZE; writetext && inlen;
      inlen -= nbytes) {
    if (inlen < BUFFERSIZE)
      nbytes = inlen;
    if (fread(buffer, 1, nbytes, in) != nbytes) {
      fprintf(stderr,"*** Found premature end of file in %s:",fname);
      perror("");
      exit(errno);
    }
    if (fwrite(buffer, 1, nbytes, stdout) != nbytes) {
      perror("*** Error writing to standard output");
      exit(errno);
    }
  }

  /* Close the input file */
  if (writetext)
    fclose(in);

  if (delete) {
    /* Delete the file */
    if (!fit_fdelete(fname)) {
      fprintf(stderr,"*** Error deleting %s:",fname);
      perror("");
      exit(errno);
    }
  }

  if (log)
    fwrite(descript, 1, len, stderr);
}
