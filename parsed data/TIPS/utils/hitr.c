/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : hitr                                                      hitr.c

Function : Exports data which is pointed to by entries in a hit file.

Author : A. Fregly

Abstract : hitr is used as a front end for exporting of data based on hits
	in a hit file. The output of hitr may be piped into filters which
	provide the remainder of the export functionality.

	The default action taken by hitr is to make a single pass through
	the hit file, copying the documents corresponding to the hits it
	finds in the hit file to standard output.

Calling Sequence : hitr {switches} hit_file

  Switches:

	-c      Specifies "continuous operation" by hitr. hitr will
		periodically rescan the hit file. The default wait of one
		minute between scans may be overridden witht the -t switch.
		The -c switch implicitly turns on the -dh switch.

	-dd     Delete documents once they have been processed.

	-dh     Delete hit file entries once they have been processed.

	-do     Delete originals once the hit has been processed. The
		document original is located based on the TIPS document
		descriptor found as a header for the document pointed
		at by the hit, thus implicitly implying the existence
		of such headers.

	-l      Enable logging to stderr.

	-o      Specifies that the document original is to be used as the
		source for output rather than the document pointed to by
		the hit.  The document original is located based on the
		TIPS document descriptor found as a header for the document
		pointed at by the hit, thus implicitly implying the existence
		of such headers.

	-t      Disables output of document text. Only headers will be written
		to standard output.

	-w secs Specifies number of seconds to wait between processing passes
		during continous operation (-c switch, default is 60).

  Parameters:

	hit_file Name of TIPS hit file to be processed.

Notes:

Change log :
000     25-JUL-91  AMF  Created.
001     15-DEC-92  AMF  Compile under COHERENT.
002     27-JUL-93  AMF  Make work under Borland C++ 2.0
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#if FIT_DOS
#include <dos.h>
#include <io.h>
#endif
#include <errno.h>
#include <fitsydef.h>
#include <drct.h>
#include <tips.h>

#define BUFFERSIZE 30720

/* Declare the shutdown procedure */
#if FIT_ANSI
void hitr_shutdown(void);
#else
void hitr_shutdown();
#endif

/* Globals */
extern int errno;

char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
char logmsg[2*FIT_FULLFSPECLEN], extension[FIT_FULLFSPECLEN+1];
char hitfilename[FIT_FULLFSPECLEN+1];

char dbname[FIT_FULLFSPECLEN+1], prevdbname[FIT_FULLFSPECLEN+1];
char origdbname[FIT_FULLFSPECLEN+1];
char docnum_s[TIPS_DESCRIPTORMAX+1], origdocnum_s[TIPS_DESCRIPTORMAX+1];

char docname[FIT_FULLFSPECLEN+1], origdocname[FIT_FULLFSPECLEN+1];

char hitdescriptor[TIPS_DESCRIPTORMAX+1],hitnamefld[TIPS_DESCRIPTORMAX+1];
char hitkeyfld[TIPS_DESCRIPTORMAX+1];
long hitlenfld;
int hitdescriptorlen;

char docdescriptor[TIPS_DESCRIPTORMAX+1], docnamefld[TIPS_DESCRIPTORMAX+1];
char dockeyfld[TIPS_DESCRIPTORMAX+1];
long doclenfld;
int docdescriptorlen;

char outdescriptor[TIPS_DESCRIPTORMAX+1];
int outdescriptorlen;

char tempdescriptor[TIPS_DESCRIPTORMAX+1];

int logging, continuous, firstpass;
FILE *in, *orig_in;

struct drct_s_file *hitfile, *db;
char buffer[BUFFERSIZE+1];

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int deletehits, deletedocs, deleteorig, writeorig, writetext;
  int stat, headerlen, tempheaderlen, nbytes, outbytes;
  unsigned timeout;
  long doclen, reclen, docnum, inlen;
  struct stat statbuf;


#if FIT_DOS
  fit_binmode(stdout);
#endif

  logging = 0;
  continuous = 0;
  deletehits = 0;
  deletedocs = 0;
  deleteorig = 0;
  writeorig = 0;
  timeout = 60;
  firstpass = 1;
  writetext = 1;

  hitfile = NULL;
  db = NULL;
  in = NULL;
  stat = 1;

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || argc && !strcmp(*argv, "?")) goto SYNTAX;

  /* Pick off switches */
  for (; argc > 1 && *(*argv) == '-'; --argc, ++argv) {
    switch ((*argv)[1]) {
      case 'c' : case 'C' :
	continuous = 1;
	break;
      case 'd' : case 'D' :
	if ((*argv)[2] == 'h' || (*argv)[2] == 'H')
	  deletehits = 1;
	else if ((*argv[2]) == 'd' || (*argv)[2] == 'D')
	  deletedocs = 1;
	else if ((*argv[2]) == 'o' || (*argv)[2] == 'O')
	  deleteorig = 1;
	else
	  goto SYNTAX_ERROR;
	break;
      case 'l' : case 'L' :
	logging = 1;
	break;
      case 'o' : case 'O' :
	writeorig = 1;
	break;
      case 't' : case 'T' :
	writetext = 0;
	break;
      case 'w' : case 'W' :
	if (!--argc) goto SYNTAX_ERROR;
	sscanf(*++argv, "%u", &timeout);
	if (timeout <= 0) goto SYNTAX_ERROR;
	break;
      default :
	goto SYNTAX_ERROR;
    }
  }

  if (logging) {
    sprintf(logmsg,"*** %s initiated ***",progname);
    fit_logmsg(stderr, logmsg, 1);
  }

  if (!argc || argc > 1) goto SYNTAX_ERROR;

  fit_expfspec(*argv, ".hit", "", hitfilename);

  /* Attempt to open the hit file */
  if ((hitfile = drct_open(hitfilename, O_RDWR, 0, -1, -1)) == NULL) {
    fprintf(stderr,"*** Unable to open hit file %s:", hitfilename);
    goto ERREXIT;
  }

  if (logging) {
    /* Log initiation and configuration */
    fprintf(stderr,"  Hit file: %s\n",hitfilename);
    if (writeorig)
      fprintf(stderr,"  Document originals will be processed via archive references\n");
    if (deletehits)
      fprintf(stderr,"  Hit deletion enabled\n");
    if (deletedocs)
      fprintf(stderr,"  Document deletion enabled\n");
    if (deleteorig)
      fprintf(stderr,"  Document originals will be deleted\n");
    if (!writetext)
      fprintf(stderr,"  Document text output is disabled\n");
    if (continuous) {
      fprintf(stderr, "  Continuous operation enabled\n");
      fprintf(stderr, "  Pause between passes: %u seconds", timeout);
    }
  }

#if !FIT_DOS
  signal(SIGTERM, void_caste hitr_shutdown);
  signal(SIGINT, void_caste hitr_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE, void_caste hitr_shutdown);
  signal(SIGQUIT, void_caste hitr_shutdown);
#else
  signal(SIGTERM, void_caste hitr_shutdown);
  signal(SIGINT, void_caste hitr_shutdown);
#if !FIT_DOS
  signal(SIGHUP, SIG_IGN);
  signal(SIGPIPE, void_caste hitr_shutdown);
#endif
  signal(SIGQUIT, void_caste hitr_shutdown);
#endif
#endif

  /* Process the hits */
  for (firstpass = 1; firstpass || continuous; firstpass = 0) {

    drct_rewind(hitfile);
	/* printf("hitr, rewound hitfile\n"); */

    while ((hitdescriptorlen = drct_rdseq(hitfile, hitdescriptor,
	TIPS_DESCRIPTORMAX, &reclen)) && (continuous || firstpass)) {
		
      if (reclen > TIPS_DESCRIPTORMAX) drct_cancelio(hitfile);
      hitdescriptor[hitdescriptorlen] = 0;
/* printf("hitr, read reacord: %s\n",hitdescriptor); */

      if (logging) {
	strncpy(tempdescriptor, hitdescriptor, hitdescriptorlen);
	tempdescriptor[hitdescriptorlen-1] = 0;
	fit_logmsg(stderr, tempdescriptor, 1);
      }

      /* Parse the descriptor */
      tips_parsedsc(hitdescriptor, hitnamefld, &hitlenfld, hitkeyfld);

      /* See if document is in an archive */
      tips_nameparse(hitnamefld, dbname, docnum_s, docname);

      nbytes = 0;
      headerlen = 0;
      outdescriptorlen = 0;

      if (docnum_s[0]) {
	/* Document is in an archive, load it from the archive */
	if (db == NULL || strcmp(dbname, prevdbname)) {
	  /* Different archive, close old one (if open) and open new one */
	  if (db != NULL) drct_close(db);
	  strcpy(prevdbname, dbname);
	  db = drct_open(dbname, O_RDONLY, 0, -1, -1);
	}

	if (db != NULL) {
	  /* Attempt to load the buffer of the document if the archive is
	     open */
	  sscanf(docnum_s,"%ld",&docnum);
	  nbytes = drct_rdrec(db, docnum, buffer, BUFFERSIZE, &inlen);
	  if (nbytes) {
	    tips_extractdescr(buffer, docdescriptor, &tempheaderlen);
	    tips_parsedsc(docdescriptor, docnamefld, &doclenfld, dockeyfld);
	    tips_nameparse(docnamefld, origdbname, origdocnum_s, origdocname);
	    if (!writeorig) {
	      headerlen = tempheaderlen;
	      doclen = inlen-headerlen;
	      tips_makedescr(dbname, docnum, origdocname, doclen, hitkeyfld, 
		TIPS_DESCRIPTORMAX+1, outdescriptor, &outdescriptorlen);
	    }
	  }
	}
      }

      else if (!writeorig && !deleteorig) {
	if (db != NULL) {
	  /* Close the database, we don't want to keep too many files open */
	  drct_close(db);
	  db = NULL;
	}
	in = fopen(docname,FIT_READ);
	if (in != NULL) {
	  fstat(fileno(in), &statbuf);
	  doclen = statbuf.st_size;
	  nbytes = fread(buffer, 1, BUFFERSIZE, in);
	  if (nbytes)
	    tips_makedescr("", -1L, docname, doclen, hitkeyfld,
	      TIPS_DESCRIPTORMAX+1, outdescriptor, &outdescriptorlen);
	}
      }

      if (nbytes && writeorig) {
	nbytes = 0;
	if (origdocname[0]) {
	  orig_in = fopen(origdocname, FIT_READ);
	  if (orig_in != NULL) {
	    fstat(fileno(orig_in), &statbuf);
	    doclen = statbuf.st_size;
	    nbytes = fread(buffer, 1, BUFFERSIZE, orig_in);
	    if (nbytes)
	      tips_makedescr("", -1L, origdocname, doclen, hitkeyfld,
		  TIPS_DESCRIPTORMAX+1, outdescriptor, &outdescriptorlen);
	  }
	}
      }

      /* Now do output */
      if (nbytes) {
	if (fwrite(outdescriptor, 1, outdescriptorlen, stdout) !=
	    outdescriptorlen)
	  goto OUTPUT_ERROR;

	if (writetext) {
	  outbytes = nbytes - headerlen;
	  if (fwrite(buffer + headerlen, 1, outbytes, stdout) != outbytes)
	      goto OUTPUT_ERROR;

	  inlen = doclen + headerlen;
	  for (inlen -= nbytes; inlen > 0; inlen -= nbytes) {
	    if (!writeorig) {
	      if (docnum_s[0]) {
		if (!(nbytes = drct_rdrec(db, docnum, buffer, BUFFERSIZE,
		    &doclen)))
		  break;
	      }
	      else {
		if (!(nbytes = fread(buffer, 1, BUFFERSIZE, in)))
		  break;
	      }
	    }
	    else {
	      if (!(nbytes = fread(buffer, 1, BUFFERSIZE, orig_in)))
		break;
	    }

	    if (fwrite(buffer, 1, nbytes, stdout) != nbytes)
	      goto OUTPUT_ERROR;
	  }
	}

	/* Flush output buffer */
	fflush(stdout);

	if (in != NULL) {
	  fclose(in);
	  in = NULL;
	}

	if (orig_in != NULL) {
	  fclose(orig_in);
	  orig_in = NULL;
	}

	if (writeorig)
	  drct_cancelio(db);

	if (deletedocs) {
	  if (docnum_s[0]) {
	    if (!drct_delrec(db,1)) {
	      fprintf(stderr,"*** Error deleting record from archive: %s\n",
		  dbname);
	      goto ERREXIT;
	    }
	  }
	  else {
	    if (!fit_fdelete(docname)) {
	      fprintf(stderr,"*** Error deleting input file: %s\n", docname);
	      goto ERREXIT;
	    }
	  }
	}

	if (deleteorig) {
	  if (!fit_fdelete(origdocname)) {
	    sprintf(logmsg,"*** Error deleting document original: %s",
		origdocname);
	    fprintf(stderr,"%s\n",logmsg);
	    perror("");
	  }
	}
      }

      if (deletehits) {
	if (!drct_delrec(hitfile,1))
	  goto HIT_DELERROR;
      }

      if (!nbytes)
	fprintf(stderr,"*** Unable to process document\n");
    }

    if (db != NULL && db->locked) {
      /* Unlock the archive record (if it hasn't been deleted) */
      drct_unlock(db);
    }

    if (hitfile->locked) {
      /* Unlock the record in the hit file (if it hasn't been deleted) */
      drct_unlock(hitfile);
    }

    /* Continuous mode enabled, sleep for the timeout period */
    if (continuous) 
#if !FIT_MSC
		sleep(timeout);
#else
		_sleep(timeout);
#endif
  }

  stat = 0;
  goto DONE;

SYNTAX_ERROR:
  stat = 1;
  fprintf(stderr,"*** Invalid command syntax\n");

SYNTAX:
  fprintf(stderr,"Usage: %s {switches} hit_file\n",progname);
  fprintf(stderr,"  -c\t\tContinous mode enabled.\n");
  fprintf(stderr,"  -dd\t\tDelete processed documents.\n");
  fprintf(stderr,"  -dh\t\tDelete processed hits.\n");
  fprintf(stderr,"  -do\t\tDelete processed originals.\n");
  fprintf(stderr,"  -l\t\tEnable status logging to standard error.\n");
  fprintf(stderr,"  -o\t\tOutput document originals.\n");
  fprintf(stderr,"  -t\t\tDisable output of document text.\n");
  fprintf(stderr,"  -w secs\tWait time between passes during continuous mode");
  fprintf(stderr," operation.\n");
  goto DONE;

OUTPUT_ERROR:
  fprintf(stderr,"*** Output error");
  goto ERREXIT;

HIT_DELERROR:
  fprintf(stderr,"*** Hit delete error:");
  goto ERREXIT;

ERREXIT:
  perror("");
  stat = errno;

DONE:
  if (logging) {
    sprintf(logmsg,"*** %s terminated ***", progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (hitfile != NULL) drct_close(hitfile);
  if (db != NULL) drct_close(db);
  if (in != NULL) fclose(in);
  return stat;
}



#if FIT_ANSI
void hitr_shutdown(void)
#else
void hitr_shutdown()
#endif
{
  int stat;
  stat = errno;
  if (logging) {
    sprintf(logmsg,"**** %s aborted ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (stat) perror("");

  /* Set so that main program will terminate on before processing next hit */
  continuous = 0;
  firstpass = 0;
}
