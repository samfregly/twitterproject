#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>

#if FIT_DOS
#include <io.h>
#endif

#if FIT_TURBOC
#define max(a,b) (((a) > (b)) ? a : b)
#define min(a,b) (((a) < (b)) ? a : b)
#include <dir.h>
#endif
#if FIT_ZORTECH
#include <direct.h>
#endif

#include <fcntl.h>
#if FIT_UNIX || FIT_COHERENT
 #if FIT_SUN
#include <sys/types.h>
#include <sys/time.h>
#include <sys/timeb.h>
#else
#include <sys/times.h>
#endif
#endif
#include <qc.h>
#include <prf.h>
#include <prfprvte.h>
#include <drct.h>
#include <tips.h>

#if !FIT_VMS
#define QRYEXT ".qry"
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".hit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.hit"
#else
#define QRYEXT ".aqry"      
#define OBJEXT ".qc"
#define EXEEXT ".prf"
#define HITEXT ".ahit"
#define DEFOBJ "afind.qc"
#define DEFHIT "afind.ahit"
#endif

#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif

#if FIT_TURBOC
#define QUERYTEXTMAX 1024
#define BUFFERSIZE 16384
#else
#define QUERYTEXTMAX 4096
#define BUFFERSIZE 30720
#endif
#if FIT_UNIX || FIT_COHERENT
#define NORMAL_FILE S_IFREG
#else
#define NORMAL_FILE 0
#endif
static char *defaultlist[] = {"*.*"};

int main(int argc, char *argv[])
{

  void *context;
  char *(*args);
  char curpath[FIT_FULLFSPECLEN + 1];
  char defpath[FIT_FULLFSPECLEN + 1];
  char infile[FIT_FULLFSPECLEN + 1];
  char dev[FIT_FULLFSPECLEN + 1];
  char path[FIT_FULLFSPECLEN + 1];
  char name[FIT_FULLFSPECLEN + 1];
  char ext[FIT_FULLFSPECLEN + 1];
  char dummy[FIT_FULLFSPECLEN + 1];
  long ndir, ntot;

  if (!--argc)
  {
    args = defaultlist;
    argc = 1;
  }
  else
    args = ++argv;

  ndir = 0;
  ntot = 0;

  context = NULL;
  for (context = NULL, curpath[0] = '\0', defpath[0] = '\0';
      argc;
      --argc, ++args, context = NULL)
  {
    for (infile[0] = 0;
	*fit_findfile(&context, *args, NORMAL_FILE, "", defpath, infile);
	infile[0] = 0, ++ndir, ++ntot)
    {
      fit_prsfspec(infile, dummy, dev, path, name, ext, dummy);
      strcat(name, ext);
      strcat(dev, path);
      if (strcmp(dev, curpath))
      {
	if (ndir)
	  printf("%ul files\n\n", ndir + 1L);
	printf("%s\n", dev);
	strcpy(curpath, dev);
	ndir = 0;
      }
      printf("\t%s\n", name);
    }
  }
  printf("%ul files total\n", ntot);
  return 0;
}
