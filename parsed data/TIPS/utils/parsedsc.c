/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fileprse						fileprse.c

Function : Parses TIPS document descriptor into it's major component pieces.

Author : A. Fregly

Abstract : This command will parse the supplied TIPS document descriptors and 
	print out desired components. If multiple components are specified,
	the components will be separated by commas so that the output is
	a valid TIPS descriptor.

Calling Sequence : parsedsc {switches} {descriptor} {descriptor...}

  Switches

	-c	Read descriptors from standard input instead of from
		command line.
	-l	Output document length portion of the descriptor.
	-n	Output document name portion of the descriptor.
	-k	Output key field portion of the descriptor.

  Parameter:

	descriptor	Document descriptor to be parsed.

Notes: 

Change log : 
000	26-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

extern int errno;

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *(*argv);
#endif
{

  int doname, dolen, dokey, didany, putcomma, readinput;
  char progname[FIT_FULLFSPECLEN+1], *optptr, *descriptor;
  char inbuf[TIPS_DESCRIPTORMAX+1];
  char name[TIPS_DESCRIPTORMAX+1], key[TIPS_DESCRIPTORMAX+1];
  long len;

  doname=0;
  dolen=0;
  dokey=0;
  didany = 0;
  readinput = 0;

  if (!--argc) goto DONE;
  strcpy(progname, *(argv++));

  if (!strcmp(*argv, "?")) goto SYNTAX;

  for (; argc && (*argv)[0] == '-'; --argc, ++argv)
    for (optptr = *argv + 1; *optptr; ++optptr)
      switch (*optptr) {
        case 'n' : case 'N' :
	  doname = 1;
	  break;
        case 'l' : case 'L' :
	  dolen = 1;
	  break;
	case 'k' : case 'K' :
	  dokey = 1;
	  break;
	case 'c' : case 'C' :
	  readinput = 1;
	  break;
	default:
	  fprintf(stderr, "Invalid switch: %c\n", *optptr);
	  goto SYNTAX;
      }

  if (!doname && !dolen && !dokey) {
    doname = 1;
    dolen = 1;
    dokey = 1;
  }
  putcomma = ((doname + dolen + dokey) > 1);

  while (1) {
    if (!readinput) {
      if (!argc--) break;
      descriptor = *(argv++);
    }
    else {
      if (fgets(inbuf, TIPS_DESCRIPTORMAX, stdin) == NULL) break;
      inbuf[TIPS_DESCRIPTORMAX] = 0;
      descriptor = inbuf;
    } 
    len = 0;
    tips_parsedsc(descriptor, name, &len, key);
    if (doname && *name || dolen && len || dokey && *key) {
      if (doname && *name) fputs(name, stdout);
      if (putcomma) fputc(',', stdout);
      if (dolen && len) fprintf(stdout, "%ld", len);
      if (putcomma) fputc(',', stdout);
      if (dokey && *key) fputs(key, stdout);
      didany = 1;
      if (readinput) {
	fputc('\n', stdout);
	fflush(stdout);
      }
      else if (argc)
        fputc(' ', stdout);
    }
  }
  goto DONE;

SYNTAX:
  fprintf(stderr, "Usage: %s {switches} {descriptor {descriptor...}}\n", 
     progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr, "  -c\t\tContinuous mode, read descriptors from standard \
input\n");
  fprintf(stderr, "  -k\t\tOutput key field\n");
  fprintf(stderr, "  -l\t\tOutput length field\n");
  fprintf(stderr, "  -n\t\tOutput name field\n");
  fprintf(stderr, "Parameter:\n");
  fprintf(stderr, "  descriptor\tDocument descriptor(s) to be parsed\n");

DONE:
  return !didany;
}
