/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : mbx							mbx.c

Function : This program serves as a relayer of packets sent to a mailbox.

Author : A. Fregly

Abstract : This program serves is used to serve as an asynchronous reader of
	mail for it's parent process. It will read messages from either it's
	standard input or a specified pipe which it creates. These messages
	take the format of <message length><message>. The message length
	is the length of the remainder of the message (minus the <newline>
	following the message length specifier) specified as an ASCII
	character string. It is suggested that message contents also be
	ASCII text so that implementation of message passing between different
	hardware platforms is easily implemented. After reading a message,
	mbx signals it's parent process and copies the message verbatim to 
	standard output.

	Normally, the parent of mbx will either assign it's input to the
	pipe which serves as it's input mailbox, or configure mbx to create
	the input mailbox. Also, the parent usually sets up standard output
	for mbx to be a pipe which the parent will use for reading incoming
	messages. The parent becomes aware of when to read this input pipe
	by catching signals generated by mbx whenever it is about to relay
	a message. To do this, the parent sets up a signal handler to catch
	the signals it has configured mbx to send. The signal handler typically
	sets a global flag, which is later spotted by the parent, which can
	then read the message and reset the signal handler so that the next
	message may be handled.

 	After notifying the parent that a message is available, the  mailbox
	function will sleep until the parent acknowledges receipt of the
	message by sending a "kill" for the same signal back to the mailbox
	process.

	The parent process should also terminate the mailbox process gracefully
	by sending a "kill" signal to the mailbox process at the time the
	parent process is shutting down. Normally, this signal should be
	a SIGTERM or a SIGINT.

Calling Sequence : mbx signal_number {mbx_name}

	signal_number	Signal to be sent to parent when a message comes in.
	mbx_name	Optional name of pipe to be created by mbx.

Notes: 

Change log : 
000	15-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <sys/stat.h>
#include <signal.h>

extern int errno;

char *pipename;
int exitstat;

void mbx_shutdown(int sig);	/* "kill" requesting a shutdown */
void mbx_ack(int sig);		/* Acknowledgement of message receipt by
				   parent */


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, arg)
  int argc;
  char *argc[];
#endif
{

  char progname[FIT_FULLFSPECLEN+1], errmessage[2*FIT_FULLFSPECLEN];
  char dummyspec[FIT_FULLFSPECLEN+1], *msgbuf;
  int msglen, parentpid, sig;
  unsigned outlen;

  /* Get mbx program name for use in error messages */
  fit_prsfspec(*argv, dummyspec, dummyspec, dummyspec, progname, dummyspec,
	dummyspec);

  --argc;
  ++argv;

  parentpid = getpid(); /* Get parent pid so we know who to signal */
  pipename = NULL;	/* To indicate pipe is not being accessed */
  exitstat = 0;		/* Default exit status to "success" */

  if (!argc) goto SYNTAX;

  /* Get from command line the signal number to be sent to parent when
     a message is available */
  sscanf(*(argv++), "%d", &sig);
  if (!sig) goto SYNTAX;

  if (--argc) {
    /* Mailbox specified, create pipe */
    if (mknod(*argv, S_IFIFO, 0)) {
      sprintf(errmessage, "%s, error creating input pipe: %s", progname, *argv);
      fit_logmsg(stderr, errmessage, 1);
      perror("");
    }
    else
      pipename = *argv;

    if (freopen(*argv, "r", stdin) == NULL) {
      /* Opened the specified input file, device, or pipe as standard input */
      sprintf(errmessage, "%s, error opening input: %s", progname, *argv);
      fit_logmsg(stderr, errmessage, 1);
      goto ERRMSG;
    }
  }

  /* Set up signal handler for kill signal */
  signal(SIGTERM, mbx_shutdown);
  signal(SIGINT,mbx_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGPIPE, mbx_shutdown);
  signal(SIGQUIT, mbx_shutdown);
#endif

  /* Main loop, copying standard input to standard output */
  while (1) {
    /* Read length of next message */
    if (scanf(" %d \n", &msglen) == EOF) break;
    if (msglen) {
      /* allocate message buffer */
      if ((msgbuf = (char *) malloc(msglen+10)) == NULL) {
	strcat(errmessage, ", unable to allocate input buffer");
	fit_logmsg(stderr, errmessage, 1);
        goto ERRMSG;
      }
      
      /* Put length into message buffer */
      sprintf(msgbuf, "%d\n", msglen);
      outlen = strlen(msgbuf);

      /* Fill in message portion of message buffer by reading it in */
      if (fread(msgbuf+outlen, 1, msglen, stdin) != msglen) {
	sprintf(errmessage, "%s, error reading input message", progname);
	fit_logmsg(stderr, errmessage, 1);
        goto ERRMSG;
      }

      /* Write the message to standard output */
      outlen += msglen;
      if (fwrite(msgbuf, 1, outlen, stdout) != outlen) {
	sprintf(errmessage, "%s, error writing to ouput", progname);
	fit_logmsg(stderr, errmessage, 1);
	goto ERRMSG;
      }

      /* Flush standard ouput to make sure it is available to parent */
      if (fflush(stdout)) {
	sprintf(errmessage, "%s, error flushing output", progname);
	fit_logmsg(stderr, errmessage, 1);
	goto ERRMSG;
      }

      /* Set up signal handler for return ack signal from parent */
      signal(sig, mbx_ack);

      /* Signal parent to let it know data is available */
      if (kill(parentpid, sig)) {
	sprintf(errmessage, "%s, error signaling parent process", progname);
	fit_logmsg(stderr, errmessage, 1);
	goto ERRMSG;
      }

      /* Wait for ack from parent */
      pause();
    }
  }
  goto DONE;

SYNTAX:
  /* Write command usage message to standard error */
  fprintf(stderr, "Usage: %s signal_number {input_specifier}\n", progname);
  fprintf(stderr,"Parameters:\n");
  fprintf(stderr,"  signal_number\t\tSignal to be raised for parent on\n");
  fprintf(stderr,"\t\t\t\treceipt of a message\n");
  fprintf(stderr,"  input_specifier\t\tOptional input device name, usually\n");
  fprintf(stderr,"\t\t\t\ta named pipe.\n");
  goto DONE;

ERRMSG:
  /* Program aborted by system error, write system error message to standard
     error */
  perror("");

ERREXIT:
  /* On system error exit, set exit status to system error number */
  exitstat = errno;

DONE:
  mbx_shutdown(0);
  return 0;
}


#if FIT_ANSI
void mbx_shutdown(int sig)
#else
void mbx_shutdown(sig)
  int sig;
#endif
{
  /* Exit handler which handle kill signals, usually from parent to indicate
     that it is going down and the mailbox program should terminage 
     gracefully */
  if (pipename != NULL) {
    /* Delete input pipe is mbx program created it */
    fclose(stdin);
    unlink(pipename);
  }
  exit(exitstat);
}


#if FIT_ANSI
void mbx_ack(int sig)
#else
void mbx_ack(sig)
  int sig;
#endif
  /* Handler of message receipt acknowledgements from parent, do nothing */
{
  ;
}
