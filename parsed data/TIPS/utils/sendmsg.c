/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : sendmsg						sendmsg.c

Function : This program is used to send messages to TIPS daemons.

Author : A. Fregly

Abstract : This function is used to send messages to TIPS daemons which
	have a message port. Typically, these message ports are accessed
	transparently by various tips applications, but this utility is
	provided for testing and error recovery purposes. The user of
	this utility must have a thorough understanding of the protocols
	used for communicating with a TIPS daemon.

Calling Sequence : sendmsg {switches} file_spec

  Switches:
  	-d word		Word to be passed as data.
        -m msgtype	Message type.
	-t timeout	Seconds to wait for return message. A timeout of
			zero is set to indicate a return message is not
			expected.

  Parameter:
	file_spec	File name for message queue.

Notes: 

Change log : 
000	17-OCT-91  AMF	Created.
001	14-DEC-92  AMF	Remove "-w" waitflag switch. Fix bugs. Build under
			Coherent.
002	27-JUN-97  AMF	Provide simpler version which uses named pipes under
			NT.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_MSC
#include <io.h>
#include <stddef.h>
#include <string.h>
#include <fcntl.h>
#include <windows.h>
#endif

#if FIT_UNIX || FIT_COHERENT
#include <sys/ipc.h>
#include <sys/msg.h>
extern int errno;
#endif


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  char progname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int timeout, msgqid, receiverpid;
  unsigned datalen, retlen;
  long msgtype;
  #if FIT_MSC
#define NAMEMAX 32
  HANDLE hPipe;
  FILE *pipefile;
  enum PIPES { READ, WRITE }; /* Constants 0 and 1 for READ and WRITE */
  int msglen;
#define BUFSIZE (TIPS_MAXMSGDATA+sizeof(msglen) + sizeof(msgtype))
  char msgbuf[BUFSIZE + 1];
  char pipefilename[FIT_FULLFSPECLEN+1];
  char pipename[NAMEMAX+1];
#else
  key_t msgkey;
#define BUFSIZE (TIPS_MAXMSGDATA+1)
  char msgbuf[BUFSIZE+1], retbuf[BUFSIZE+1];
  long retmsgtype;
#endif

  /* Initialize for default message type, a nowait send of message type 1
     with no data */
  timeout = 0;
  msgtype = 1;
  datalen = 0;
  memset(msgbuf, 0, BUFSIZE+1);

  /* Set program name */
  fit_prsfspec(*(argv++), dummyspec, dummyspec, dummyspec, progname,
	dummyspec, dummyspec);
  --argc;

  if (!argc || (*argv)[0] == '?') goto SYNTAX;

  /* Parse switches */
  for (; argc > 1; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch ((*argv)[1]) {
        case 'd' : case 'D' :
	  --argc;
	  ++argv;
	  datalen = min(TIPS_MAXMSGDATA,strlen(*argv));
#if FIT_MSC
	  strncpy(msgbuf+sizeof(msglen) + sizeof(msgtype), *argv, datalen);
#else
	  strncpy(msgbuf, *argv, datalen);
#endif
	  msgbuf[datalen] = (char) 0;
	  break;
	case 'm' : case 'M' :
	  --argc;
	  ++argv;
#if FIT_MSC
	  sscanf(*argv, "%ld", msgbuf + sizeof(msglen));
#else
	  sscanf(*argv, "%ld", &msgtype);
#endif
	  break;
	case 't' : case 'T' :
	  --argc;
	  ++argv;
	  sscanf(*argv, "%d", &timeout);
	  break;
	default:
	  goto SYNTAX;
      }
    }
    else
      goto SYNTAX;
  }

  if (argc <= 0) goto SYNTAX;

  /* Attach to the message queue */
#if FIT_UNIX || FIT_COHERENT
  msgkey = ftok(*argv, TIPS_FTOKSEED);
  if ((msgqid = msgget(msgkey, 0)) < 0) {
    perror("*** sendmsg: Unable to attach to message queue");
    exit(errno);
  }

  /* Send the message and possibly wait for a reply */
  if (!tips_sendmsg(msgqid, msgtype, msgbuf, datalen, timeout,
	retbuf, TIPS_MAXMSGDATA, &retlen, &retmsgtype, &receiverpid)) {
    perror("*** sendmsg: Error sending message");
    exit(errno);
  }

  if (retlen) {
    /* Return message received, write it to standard output */
    printf("Return message length: %d, Receiver PID: %d\n",
      retlen, receiverpid);
    fwrite(retbuf, 1, retlen, stdout);
    fprintf(stdout,"\n");
  }
#endif
#if FIT_MSC
  strncpy(pipefilename, *argv, FIT_FULLFSPECLEN);
  pipefilename[FIT_FULLFSPECLEN] = 0;
  fit_prsfspec(*argv, dummyspec, dummyspec, dummyspec, pipename, dummyspec, dummyspec);
  pipename[NAMEMAX] = (char) 0;
  strcpy(pipefilename, "\\\\.\\pipe\\");
  strcat(pipefilename, pipename);
  /* printf("pipe file name is %s\n", pipefilename); */  
  if ((hPipe = CreateFile(pipefilename, GENERIC_WRITE, FILE_SHARE_WRITE, NULL,
	    OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM, (HANDLE) NULL)) == INVALID_HANDLE_VALUE) {
    DWORD errstat = GetLastError();
    fprintf(stderr, "Error from CreateFile: %u\n", errstat);
	exit(1);
  }
  msglen = datalen + sizeof(msgtype);
  memcpy(msgbuf, &msglen, sizeof(msglen));
  WriteFile(hPipe, msgbuf, msglen + sizeof(msglen), &retlen, NULL);
  CloseHandle(hPipe);
#endif
  return 0;
 
SYNTAX:
  /* Syntax error or no command line switches or parameters or help
     requested. Write usage to standard error */
  fprintf(stderr,"Usage: %s {switches} file_spec\n", progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -d word\tWord to be passed as data.\n");
  fprintf(stderr,"  -m msgtype\tMessage type.\n");
  fprintf(stderr,"  -t timeout\tSeconds to wait for return message.\n");
  fprintf(stderr,"Parameter:\n");
  fprintf(stderr,"  file_spec\tFile name for message queue\n");
  return 0;
}
