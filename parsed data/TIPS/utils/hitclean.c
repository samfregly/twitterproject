/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : hitclean                                          hitclean.c

Function : This function is used to remove result references to inaccessable
	documents.

Author : A. Fregly

Abstract : This function is used to remove references to no longer accessable
	document from a result file.

Calling Sequence : hitclean searchname {searchname...}

  searchname    Name of query or result file(s) which are to have the results
		cleaned up.

Notes: 

Change log : 
000     21-NOV-91  AMF  Created.
001     15-DEC-92  AMF  Compile under COHERENT.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <fcntl.h>
#if FIT_DOS
#include <io.h>
#endif
#include <tips.h>
#include <drct.h>

char progname[FIT_FULLFSPECLEN+1];

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char argv[];
#endif
{

  char dummyspec[FIT_FULLFSPECLEN+1], hitfilename[FIT_FULLFSPECLEN+1];
  char prevarcname[TIPS_DESCRIPTORMAX+1], arcname[TIPS_DESCRIPTORMAX+1];
  char namefld[TIPS_DESCRIPTORMAX+1], docnum_s[TIPS_DESCRIPTORMAX+1];
  char keyfld[TIPS_DESCRIPTORMAX+1], descriptor[TIPS_DESCRIPTORMAX+1];
  char origname[TIPS_DESCRIPTORMAX+1];
  char errmessage[TIPS_DESCRIPTORMAX+64], curpath[FIT_FULLFSPECLEN+1];
  char *eptr;

  int retstat, delflag, nbytes;
  long reclen, docnum, doclen;
  struct stat statbuf;
  struct drct_s_file *hitfile, *arc;
#if !FIT_UNIX && !FIT_COHERENT
  char *context;
#endif
#if FIT_DOS
#define NORMAL_FILE 0
#else
#define NORMAL_FILE S_IFREG 
#endif

  retstat = 0;
  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (!--argc || !strcmp(*argv,"?")) goto SYNTAX;

  getcwd(curpath, FIT_FULLFSPECLEN);
  prevarcname[0] = 0;
  arc = NULL;

  for (; argc; --argc, ++argv) {
#if FIT_UNIX || FIT_COHERENT  
    fit_setext(*argv, ".hit", hitfilename);
#else  
  for (context = NULL; *fit_findfile(&context, *argv, NORMAL_FILE, ".hit", 
	curpath, hitfilename);) {
#endif
    if ((hitfile = drct_open(hitfilename, O_RDWR, 0, -1, -1)) != NULL) {
      while (nbytes = drct_rdseq(hitfile, descriptor, sizeof(descriptor) - 1, 
	  &reclen)) {
	delflag = 1;
	descriptor[nbytes] = 0;
	eptr = strchr(descriptor, '\n');
	if (eptr != NULL) {
	  *(eptr+1) = 0;
	  tips_parsedsc(descriptor, namefld, &doclen, keyfld);
	  if (namefld[0]) {
	    tips_nameparse(namefld, arcname, docnum_s, origname);
	    if (arcname[0]) {
	      if (strcmp(arcname, prevarcname)) {
		strcpy(prevarcname, arcname);
		if (arc != NULL) drct_close(arc);
		arc = drct_open(arcname, O_RDONLY, 0, -1, -1);
	      }
	      if (arc != NULL && docnum_s[0]) {
		sscanf(docnum_s, "%ld", &docnum);
		delflag = !drct_rdrec(arc, docnum, descriptor, 
		  sizeof(descriptor) - 1, &reclen);
		if (!delflag) drct_cancelio(arc);
	      }
	    }
	    else if (origname[0])
	      delflag = stat(origname, &statbuf);

	    if (delflag) {
	      if (!drct_delrec(hitfile, 1)) {
		sprintf(errmessage, 
		  "%s %ld - Error deleting record %ld from %s",
		  hitfilename, hitfile->currec+1);
		perror(errmessage);
		fprintf(stderr, "  Cleanup of file was abandoned");
		break;
	      }
	    }
	  }
	  else {
	    fprintf(stderr, "%s %ld - Unable to determine document name",
	       hitfilename, hitfile->currec+1);
	  }
	}
	else {
	  fprintf(stderr, "%s %ld: Descriptor was too long", hitfilename,
		hitfile->currec+1);
	}
      }
      drct_close(hitfile);
    }
    else {
      sprintf(errmessage, "Unable to open %s", hitfilename);
      perror(errmessage);
    }
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  }

  if (arc != NULL) drct_close(arc);
  goto DONE;

SYNTAX:
  fprintf(stderr,"Usage: %s search_name {search_name...}\n",progname);
  fprintf(stderr,"Parameter:\n");
  fprintf(stderr,"  search_name\tQuery or result file name\n");

DONE:
  return 0;
}
