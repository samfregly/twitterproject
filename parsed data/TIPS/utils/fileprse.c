/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fileprse						fileprse.c

Function : Parses file specifications into their component pieces.

Author : A. Fregly

Abstract : This command will parse the supplied file specifications and print
	out desired components.

Calling Sequence : fileprse {switches} {filespec {filespec...}}

  Switches

        -c      Read file specifications from standard input instead of from
                the command line.
	-d	Output directory name portion of the file specification
	-n	Output file name portion of the file specification
	-e	Output file name extension portion of the file specification

  Parameter:

	filespec	File specification(s) to be parsed.

Notes: 

Change log : 
000	25-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>

extern int errno;

#if FIT_ANSI
int main(int argc, char *(*argv))
#else
int main(argc, argv)
  int argc;
  char *(*argv);
#endif
{

  int dodir, doname, doext, didany, readinput, len;
  char progname[FIT_FULLFSPECLEN+1], *optptr, dummyspec[FIT_FULLFSPECLEN+1];
  char dir[FIT_FULLFSPECLEN+1], name[FIT_FULLFSPECLEN+1], ext[FIT_FULLFSPECLEN+1];
  char *fspec, inbuf[FIT_FULLFSPECLEN+1];

  dodir=0;
  doname=0;
  doext=0;
  didany = 0;
  readinput = 0;

  if (!--argc) goto DONE;
  strcpy(progname, *(argv++));

  if (!strcmp(*argv, "?")) goto SYNTAX;

  for (; argc && (*argv)[0] == '-'; --argc, ++argv)
    for (optptr = *argv + 1; *optptr; ++optptr)
      switch (*optptr) {
        case 'd' : case 'D' :
	  dodir = 1;
	  break;
        case 'n' : case 'N' :
	  doname = 1;
	  break;
	case 'e' : case 'E' :
	  doext = 1;
	  break;
	case 'c' : case 'C' :
	  readinput = 1;
	  break;
	default:
	  fprintf(stderr, "Invalid switch: %c\n", *optptr);
	  goto SYNTAX;
      }

  if (!dodir && !doname && !doext) {
    dodir = 1;
    doname = 1;
    doext = 1;
  }

  while (1) {
    if (!readinput) {
      if (!argc--) break;
      fspec = *(argv++);
    }
    else {
      if (fgets(inbuf, FIT_FULLFSPECLEN, stdin) == NULL) break;
      inbuf[FIT_FULLFSPECLEN] = 0;
      fspec = inbuf;
      len = strlen(inbuf);
      if (len && inbuf[len-1] == '\n') inbuf[len-1] = 0;
    }

    fit_prsfspec(fspec, dummyspec, dummyspec, dir, name, ext, dummyspec);
    if (dodir && *dir || doname && *name || doext && *ext) {
      if (dodir && *dir) fputs(dir, stdout);
      if (doname && *name) fputs(name, stdout);
      if (doext && *ext) fputs(ext, stdout);
      didany = 1;
      if (readinput) {
        fputc('\n', stdout);
        fflush(stdout);
      }
      else if (argc)
        fputc(' ', stdout);
    }
  }
  goto DONE;

SYNTAX:
  fprintf(stderr, "Usage: %s {switches} {file_spec {file_spec...}}\n", 
    progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr, "  -c\t\tContinuous mode, read file specs from standard \
input\n");
  fprintf(stderr, "  -d\t\tOutput directory name\n");
  fprintf(stderr, "  -n\t\tOutput file name\n");
  fprintf(stderr, "  -e\t\tOutput file name extension\n");
  fprintf(stderr, "Parameter:\n");
  fprintf(stderr, "  file_spec\tFile specification(s) to be parsed\n");

DONE:
  return !didany;
}
