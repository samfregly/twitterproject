#include <stdio.h>
#include <stdlib.h>
#include <fitsydef.h>
#if FIT_ZORTECH
#include <direct.h>
#endif

#if FIT_UNIX || FIT_COHERENT
#define READ "r"
#else
#define READ "rb"
#endif

#define BUFMAX 1024
char buf[BUFMAX+1];

#if FIT_ANSI 
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{
  FILE *in;
  int nbytes;
  long doclen;
#if !FIT_UNIX && !FIT_COHERENT  
  char *curdir, *context, fspec[FIT_FULLFSPECLEN+1];
#define NORMAL_FILE 0
#else
#define NORMAL_FILE S_IFREG 
  char *fspec;
#endif

#if FIT_DOS
  fit_binmode(stdout);
#endif
#if !FIT_UNIX && !FIT_COHERENT  
  curdir = getcwd(NULL, FIT_FULLFSPECLEN+1);
#endif

  if (argc < 2 || *(argv+1)[0] == '?') {
    fprintf(stderr,
	"Usage: %s >output_file anpa_file_name {anpa_file_name...}\n", *argv);
    exit(0);
  }

  for (--argc, ++argv; argc; --argc, ++argv) {
#if !FIT_UNIX && !FIT_COHERENT    
    for (context = NULL;
	*fit_findfile(&context, *argv, NORMAL_FILE, "", curdir, fspec);) {
#else
    fspec = *argv;
#endif
    if ((in = fopen(fspec, READ)) != NULL) {
      fprintf(stderr,"processing: %s, ", fspec);
      nbytes = fwrite("\01", 1, 1, stdout);
      for (doclen = 0; nbytes = fread(buf, 1, BUFMAX, in); doclen += nbytes)
	doclen += (nbytes = fwrite(buf, 1, nbytes, stdout));
      fprintf(stderr,"%ld bytes\n",doclen);
      nbytes = fwrite("\04", 1, 1, stdout);
      fclose(in);
    }
    else {
      fprintf(stderr,"*** Unable to open: %s\n", fspec);
      perror("");
    }
#if !FIT_UNIX && !FIT_COHERENT  
  }
#endif
  }
}
