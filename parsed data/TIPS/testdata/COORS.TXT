COORS Document Retrieval System Security Plan

MEREX Inc.
April 18th, 1994


1	Overview

COORS Brewing is in the process of defining the elements of an enterprise wide electronic document retrieval system. The document retrieval system will be capable of storing a variety of unstructured document types in an electronic database. Retrieval of these documents may be performed using either a GUI interface running under MS Windows, or a character based interface running on terminal emulators and DEC VT series terminals. Both types of interfaces will allow retrieval based on search criteria specified for the meta-data fields describing the documents, and specified for the full text of the actual document. Both interfaces will allow viewing of the meta-data and the full text of the documents. In addition, the GUI interface will allow viewing of the documents in a format corresponding with the printed version of the document.

 The system will be built utilizing core elements already in place at Coors plus some additional components. The core elements of the system consist of existing infrastructure components and  existing document retrieval software components. Infrastructure components include elements such as the existing VAX server systems, the network, and the client work stations. The existing document retrieval components include the BASISPLUS document retrieval system and the user interface which has been built with AMIABLE.

Additional elements which are to be added to the system include BASIS Desktop, a MS Windows based graphical user interface to the BASISPLUS system, and document viewing software such as Adobe Acrobat. Some software components may be custom developed to aid COORS in putting documents into the system.


2	Objectives

2.1	Overview

The objective of this document is to lay out a security model which may be implemented for the COORS electronic document retrieval environment. The goals and constraints for the model will be defined, and the model will be described at both abstract and at implementation levels.


2.2	Environment

The environment on which the document retrieval system must exist is a distributed network environment. The network backbone utilizes DEC's DEC-Net protocol for interlinking the VAX VMS based server systems. PCs connect to the VAX VMS servers using DEC's PathWorks networking software. Many of the PCs are also connected to a Novell network. The PCs connected to the network are capable of running client portion of client/server applications such as the BASIS DESKTOP document retrieval software.

A substantial number of users utilize DEC VT series terminals to connect to the VAX VMS systems. These users connect via serial links to terminal servers on the network. They are limited to running character based applications executing on a server which the user has directly logged on to.

It is anticipated that users across all of COORS major business units and major locations will need to utilize the document retrieval system. The system will allow this access to be accomplished from the workstation which is most convenient to each user, ranging from PCs residing on their desks to terminals placed in common areas.


2.3	Security Mechanism Requirements

2.3.1	The design of the security system should be application independent. This requirement demands that the security model should not rely on specific features of a particular software product to support it's implementation. The security model should be able to be implemented under a variety of infrastructure and application environments. This requirement will allow COORS to change document retrieval system components without having to change the model for the security system.

2.3.2	The security system should provide multidimensional classification of users and data.  Document retrieval system users may be classified along at least two dimensions: their business unit, and their job category. The security model should allow a users access to documents to be determined based on their classification in these two dimensions. The system should also allow arbitrary user classifications to be set up on a document by document basis so that access to a document may be limited to specific users.

2.3.3	The security system may be implemented at the application level. This requirement specifies that the system does not have to protect against unauthorized access to data by users operating outside of the document retrieval application. The environment at COORS allows security to be implemented at the application level due to the tight control which COORS has implemented in providing users access to their network. System users operate in captive accounts which limit them to accessing only those applications which the system administrator has granted them access. This prevents the users from utilizing an application other than the document retrieval system to gain access to documents stored in the system.


3	Database Security System Design

3.1	Security Model

The security model will be based on standard security mechanisms as described in the "Orange Book" guide to computer data security. The "Orange Book" is the standard document which describes the security schemes which must be implemented by systems which store classified data of the United States Government. The book actually describes a number of schemes which may be implemented depending on the desired security level. The security model recommended in this document is based on the model specified in the "Orange Book" which is used for storing documents of multiple security levels on a single system. The document retrieval application will enforce the security model, and should be the only means by which documents in the system may be accessed.

The security scheme works as follows. Each document in the system is classified by means of security tags which are attached to the document at the time it is placed in the system. Each system user is also classified by granting them various security tags. Document security is enforced by the document retrieval application. The application restricts user's to accessing only those documents for which they have been granted a security tag which matches one of the security tags attached to the document.

The users security  tags are configured by a system administrator and stored in the users login profile. The document retrieval application automatically retrieves the tags from the users profile at time of login into the document retrieval application. The application of the security mechanism is automatic within the document retrieval application as it is implemented as an inherent part of the data retrieval mechanism.

The following example illustrates the security scheme.

First, we define some security tags for use in tagging documents and for granting to users.

Tag			Description of Tag
ALL			Tag used to allow access by all users.
BREWING		Brewing operations tag
PACKAGING		Packaging operations tag
EXEC			Limit access to executive
BOARD		Limit access to board member

Next, we define some users and the tags they are granted.

User			Tags
Jim Janitor		ALL
Billy Brewer		ALL, BREWING
Steve Packager		ALL, PACKAGING
Eric Exec		ALL, BREWING, PACKAGING, EXEC
Mr. CEO		ALL, BREWING, PACKAGING, EXEC, BOARD

Next, we define some documents and security tags for those documents, and the list of users who could access them in this security scheme.

Document: 	Company Policies For Vacation Planning
Tags:		ALL
Access:		Jim, Billy, Steve, Eric, CEO

Document:	Recipe Modification For COORS Original
Tags:		BREWING
Access		Billy, Eric, CEO

Document:	Executive Compensation Plan
Tags:		EXEC
Access		Eric, CEO

Document:	COORS Organization Restructuring Proposal
Tags:		BOARD
Access:		CEO

Document:	Product Development Plan for COORS Light
Tags:		BREWING, PACKAGING
Access:		Bill, Steve, Eric, CEO


3.2	BASISPLUS Implementation

The above type of security scheme may be implemented in several ways using inherent capabilities of BASISPLUS. The principal issues involved in choice of the implementation mechanism will be the tradeoff between flexibility and system overhead.

The first mechanism which might be used for implementing the security model utilizes the inherent security tagging capabilities provided as a standard feature of BASISPLUS. BASISPLUS allows tagging of each document in the database with a 20 bit number. This number is stored as a field within the document record. Each bit position within the number represents a security tag with which the document may be labeled. Tags are attached to the document by setting the appropriate bits. Users are granted tags in the same manner. The system administrator assigns a 20 bit number to the user as part of their user profile. Security may be implemented in this scheme in several ways. The mechanism which reflects the chosen security model is to perform a bitwise "AND" between the users 20 bit access number, and the 20 bit access number for a document which the user wishes to view. The users is allowed access to a document if the result of the "AND" operation is non-zero. This occurs when the user has been granted one of the access tags with which the document has been tagged.

The main benefit of the above mechanism is the load overhead it places on the system. The bitwise "AND" operations are executed very quickly and at a low level within the BASISPLUS system, resulting in efficiency of implementation. The mechanism has several drawbacks. Determination of the tags is less straightforward than it might be as logical tags need to be converted to bit offsets, and a number generated to represent the tagging of a user or document once this conversion is completed. This process requires substantial computer literacy, and may be error prone. The second major drawback is the lack of flexibility which occurs due to the limit of 20 possible tags which may be used. While this may be enough tags for COORS needs, it will require more planning and control to create a workable security model with this limitation.

An alternative mechanism for implementing the security model is to utilize the relational qualities of the BASISPLUS system to create user views of the documents which are limited based on relational joins. In this mechanism, a user authorization table is created within which their is one entry for each system user. Each entry has a security tags fields, which serves as an array of the security tags which have been granted to the users. Documents stored in the system are also placed in tables where a security tags field is used to specify the security access tags assigned to the document. Security is enabled in this scheme by forcing the users to use a view to access the data which restricts them to accessing only those documents for which a security tag in their user authorization table matches a tag which has been assigned to a document.

This second security mechanism provides several benefits compared to the first. It may be implemented in an almost identical fashion within a variety of relational database systems. It also provides a more easily understood security structure. The principal drawbacks of this mechanism are the possibility that it may not be compatible with the BASIS Desktop product, and that it may place unacceptable performance penalties on the system.
