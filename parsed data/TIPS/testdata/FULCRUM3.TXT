From afregly@cais.cais.comTue Dec  5 14:43:45 1995
Date: Tue, 28 Nov 1995 10:19:14 -0500 (EST)
From: "Andrew M. Fregly" <afregly@cais.cais.com>
To: Steve Judges <stevej@fultech.com>
Cc: afregly@infodata.com, ahengle@infodata.com, jdunne@infodata.com,
    rtworek@infodata.com
Subject: Repeat send of updated Fulcrum project plan.

This message is meant to serve as an updated project plan for the 
completion of the PDF text reader project. This updated project 
plan reflects a basic change in the approach for the 
implementation of the text reader for the NT and UNIX platforms. 
Acrobat Exchange will not be used as a slave process to perform 
the text extraction function on behalf of the text reader. This 
is the mechanism used in the Windows text reader. The approach 
for NT and UNIX will utilize the Adobe Acrobat Toolkit for 
performing the text extraction function. 
 
The switchover in approach is being performed for several 
reasons. 
 
	It will significantly shorten the development time and lower
	the cost. 
 
	The text reader will be much simpler in design and far less 
	complex in processing. 
 
	The code will be much more portable between the NT and UNIX
	platforms. 
 
	MEREX will attempt to get the source code for the toolkit so 
	that platforms currently not supported by Adobe can be 
	supported (NT Alpha, NT MIPS). 
 
The principal disadvantage of the change in approach is related 
to the fact that the text extraction toolkit does not support 
custom file systems. This means that the text reader will not be 
able to utilize optimized file access methods such as reading of 
memory files under NT. Memory files are utilized in the Acrobat 
Exchange search product. While the text reader will work with the 
Exchange product, it will not be as optimized as it could be. 
Instead of reading the PDF file which has been cached in memory 
by the Exchange search software, the text reader will need to 
pull it's own copy of the PDF file out of the MS Exchange 
repository and cache it in a temp file. 
 
MEREX feels that there is a good chance that the above listed 
disadvantage will only be temporary. MEREX and Fulcrum may 
utilize their influence to get Adobe to include support for 
custom file systems in the greatly enhanced version of the 
toolkit which is currently under development. MEREX may also 
attempt to get access to the the source code so that support for 
custom file systems could be developed by MEREX. 
 
Utilizing the new approach significantly shortens the development 
time by reducing the complexity of the project. The principal 
element which was taking up most of the development time for the 
Windows implementation was the debugging of the interprocess 
communcations between the text reader wrapper and the Acrobat 
Exchange plug-in which did the text extraction. This part of the 
software would need to be completely reworked to support NT, and 
then reworked again to support UNIX. The efort required to do 
this formed a majority of the preliminary time estimate for 
completion of the project which MEREX verbally gave to Fulcrum 
last week. 
 
Under the new approach, there is very little code which needs to 
be written. This is due to the fact that the principal elements 
of the approach are already there. The text reader code which 
interface with SearchServer resides in the current text reader 
wrapper code. The code which performs the text extraction resides 
in the Acrobat Excahnge plug-in. The text reader to be developed 
under the new approach will be created by merging these two 
pieces. The text extraction code will be inserted in the 
locations in the wrapper which currently perform interprocess 
communications to ask the Acrobat Exchange plug-in to do the text 
extraction. Once this code is working, the code will be analyzed 
and changed to make sure it is reentrant and can handle a multi-
threaded environment. The code will also be made compatible with 
the MS Exchange search product. 
 
Schedule: The following schedule is limited in scope to the 
effort required to complete the original goals text reader 
development project utilizing the new approach. It does not 
include the cost of supporting non Intel NT platforms and porting 
costs for the Acrobat PDF Toolkit. 
 
November 27 - 28 
	Determine cause of incompatibilities between SearchServer 
	3.0 Beta and the Windows version of the text reader. 
November 29 
	Finalize synch point insertion algorithm within Windows
	text reader. 
November 30 - December 1 
	Analysis of code integration required between current text 
	reader wrapper and code to be extracted from the Acrobat
	Exchange plug-in 
December 4 - December 22 
	Initial development leading to internal Alpha release for 
	Intel NT 
December 26 - January 5 
	Debug and testing leading to Beta release for Intel NT which 
	is compatible with the MS Exchange for NT product. 
January 6 - 12 
	Port to HP/UX and bug fixes 
January 13 - 19 
	Port to SUN Solaris and bug fixes 
 
Budget: 

As of November 27, the development cost of the text reader to 
date including unbilled time is approximately $65,000. 
 
To complete the text reader development will require 
approximately 7 man weeks of programming effort. This will amount 
to about $28,000 plus about $5000 to cover management and 
technical oversight costs. 
 
MEREX has exhausted the limit of the previous $50,000 T&M 
contract. To cover the cost of the continued development, MEREX 
is asking Fulcrum to cover the costs listed above under a $48,000 
extension of the T&M contract. This amount will cover the 
previously unbilled amount plus the additional development. 
 
Due to the past experience which MEREX has had with the text 
reader code will be utilized in the new approach, we feel that 
the above schedule is conservative. Under optimal development 
circumstances, the project could be completed with as little as 4 
man weeks of additonal programming effort. 
 
MEREX regrets that the cost of this project has exceeded 
expectations. The impact of utilizing Acrobat Exchange as a slave 
process was underestimated. Other complications included 
additional functionality above and beyond the original scope 
including consideration of WEB support needs, future Acrobat 
Exchange search integration, and suport for the MS Exchange 
search product. 
