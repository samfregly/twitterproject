h0370-----
r s BC-FBC--All-BigTen List   11-25 0328
^BC-FBC--All-Big Ten, List,0260<
^All-Big Ten List<
	   CHICAGO (AP) _ The Associated Press 1991 All-Big Ten football
team:
^First Team=
^Offense=
	   Wide Receivers _ Desmond Howard, Michigan; Courtney Hawkins,
Michigan State.
	   Tight End _ Patt Evans, Minnesota.
	   Tackles _ Greg Skrepanek, Michigan; Alan Kline, Ohio State.
	   Guards _ Tim Simpson, Illinois; Matt Elliott, Michigan.
	   Center _ Mike Devlin, Iowa.
	   Quarterback _ Elvis Grbac, Michigan.
	   Running Backs _ Vaughn Dunbar, Indiana; Ricky Powers, Michigan.
	   Kicker _ J.D. Carlson, Michigan.<
^Defense=
	   Linemen-Outside Linebackers _ Mike Poloskey, Illinois; Alonzo
Spellman, Ohio State; LeRoy Smith of Iowa; Jason Simmons, Ohio
State; Mike Evans, Michigan.
	   Linebackers _ Erick Anderson, Michigan; Steve Tovar, Ohio State;
Chuck Bullough, Michigan State.
	   Backs _ Troy Vincent, Wisconsin; Jimmy Young, Purdue; Sean
Lumpkin, Minnesota.
	   Punter _ Eric Bruun, Purdue.
	   ___=
^Second Team=
^Offense=
	   Wide Receivers _ Mark Benson, Northwestern; Elbert Turner,
Illinois.
	   Tight End _ Rod Coleman, Indiana.
	   Tackles _ Randy Schneider, Indiana; Rob Baxley, Iowa.
	   Guards _ Joe Cocozzo, Michigan; Chuck Belin, Wisconsin.
	   Center _ Steve Everitt, Michigan.
	   Quarterback _ Matt Rodgers, Iowa.
	   Running Backs _ Tico Duckett, Michigan State; Carlos Snow, Ohio
State.
	   Kicker _ Scott Bonnell, Indiana.
^Defense=
	   Linemen-Outside Linebackers _ Greg Farrall, Indiana; Ron Geater,
Iowa; Jeff Zgonina, Purdue; Greg Smith, Ohio State.
	   Linebackers _ Dana Howard, Illinois; John Derby of Iowa; Jim
Schwantz, Purdue.
	   Backs _ Marlon Primous, Illinios; Willie Lindsey, Northwestern;
Corwin Brown, Michigan.
	   Punter _ Josh Butland, Michigan State.
	   ___=
^Offensive Player of the Year=
	   Desmond Howard, Michigan.<
^Defensive Player of the Year=
	   Troy Vincent, Wisconsin; Erick Anderson, Michigan, tie.<
^Freshman of the Year=
	   Corey Rogers, Purdue.
	   AP-DS-11-25-91 1814EST<


