	Beginnings

imagination and excitement
gave way to what was you
and feelings began to blossom
together all is new

who could know
what can be
but what has been
I think we shall see

	Traveler

High in the air
Someplace too far away
Oh how I do care
Words cannot say

In a strange place
There is only me
A vision is your face
But only there to see

And I sleep alone
Your voice in my head
I pick up the phone
To hear what you said

So I wing my way home
Anticipating what waits
Not just a poem
To share in our fate

But fate has its turns
To fly away is a sin
Together we must learn
To be as one even then

	Change of Season

Gentle falls the rain
Cleansing all which is exposed
Releasing a winters cold pain
Revealing those questions unposed

And the rain leads the sun
As day shall follow night
Something new has begun
There is joy in the light

As the seeds begin to grow
The smells of spring in the air
And all that begins to show
Leads to what is so fair

A warm breeze from the South
Whispering so I could barely hear
A touch of breath from your mouth
Your presence was so near

Soon to feel the rays of heat
Desire burns within the heart 
Nature's drum never skips a beat
Instinct serves us as our chart

And in the midst of passion
The kindling of a flame
The desire for an action
That we do not name

Summer is on the way
As tides move with the moon
Feeling can push doubt away
It can never be too soon

Anticipating
Soft
Warm
Touches

The one who awaits me
Skin so gentle on skin
Envelopes completely
My soul

Moving
Quickly
Falling
Deeply

Towards a destination long sought
Advancing through the test of time
In love is what I find
Feeling the time is finally mine

Landing
Finally
Thankfully
Home

With feet firmly on ground
The Earth is solid beneath
Standing on rock secure
Is where you are

	Roller Coaster

Memories swirl through the haze
Around an ache which stays
Visions of wonderful yesterday
There is so much I wish to say

Thought I was master of my fate
The effect of love I did not rate
A force that overwhelms all
Has driven me to the wall

What must be your mood
Emotions still need their food
A train moving too quickly on
Wonder if the time has gone

Pressure built upon dreams
But all was not as it seems
Facing issues from the past
Wanting a future which will last

Roller coaster going on a rail
Let us off for a spell
Feel freedom's easy breeze
Doing just what we please

Sun can burn the haze away
Leaving us in a clear day
To look at each other again
Perhaps a new start then
Thank you for
	Being in my life
	Love
	Honesty
	Courage
	Integrity

Remembering
	Phone tag
	Starbucks
	Bilbo Baggins
	The Wharf
	First kiss
	Indian food
	Snuggling
	Fires at the condo
	Loving
	Chincoteague
	"Crazy about you"
	"Love you"
	"Promise"
	"5 more minutes"
	Lobster
	Your birthday
	Rehobeth
	Sleeping in real late
	Phone calls
	Phone off the hook
	Bagels and lox
	Talking about the day
	Wine shared
	Greetings on the couch
	The gym
	Bicycling
	A cook-out
	Coffee
	Chips and salsa
	Green eyes
	Soft skin
	Tiny hands
	Painted toenails
	Your touch
	Your beauty
	Your voice
	Your smell
	Your love 
	You

Wishing You
	Fond memories
	Happiness
	Love
	A family
	Resolution
	Less stress
	Health
	The best

Always
	Will have a piece of my heart
	Will be there if you need me
	Yours

	Lost Things

What happened to the woman I loved
Is she hiding or gone for good
Maybe she was just my fantasy
But I love her still

What happened to the things she did
When she let me know she cared
The wanting to be with me
And the loving given frequently

Where are the things we shared
Together we wanted each others company
So special just being with each other
Enjoying feelings given so freely

Why is it this way
So little is needed
Small things mean so much
Without them we drift away

A man has his needs
And a woman has them too
Time shows how they differ
We decide if it is too much

I want her back
The woman I loved
Without her we are lost
And must search again

Mountains and Sea

In the mountains
	The air is clear
In the mountains
	Life is dear

By the sea
	Waves in a dance
By the sea
	View vast expanse

What to choose
	Mountains have their way
What to choose
	What does the sea say

In the middle
	Lives a man
In the middle
	Dreams he can

Why choose at all
	They are both but a place
Why choose at all
	There is always a space

Live for each day
	Enjoy the things we know
Live for each day
	Watch new things grow

Today is but a moment
	Fleeting seconds frozen in time
Today is but a moment
	Wasting them seems a crime

The future is still to make
	Today is tommorows past
The future is still to make
	Look for those things that last

They live forever
	The mountains and sea
They live forever
	In them our spirit should be	

Another day
or is the last one done
make no mistake
though the mind is numb
to tragedy
and to joy

These are people
so we know
but keep them rolling
on dreams
which may come true
and which often don't

Why are you here
motives once so pure
but the works labor
and the stress
no time for success
but failure has many ways

Who could know the pain
But those who see and feel
The long days and nights
Repetition through all changes
Minutes on guard before instant on 
When speed and skill
Make it worth it all

I
never
thought
this

remember my dreams of years ago
realized that they had faded
that was not in my plans you know
life somehow makes you jaded

Where
went
my
inspiration

there was a time when I planned

	Try

I saw a mountain in the distance
Capped with snow it towered above the rest
Between us there was a jungle
And foothills climbing to the sky

Somehow I got it into my mind to climb
There was only one peak that drew me
On a crystal day it looked so close
I reached for it as though I could touch it

Packing my bags in a place suddenly silent
Things I knew now seemed so different
Voices echoed slowly as words developed meaning
This moment felt as though it was far in the past

I floated from that place on feet which knew
A path that led me away from places familiar
But they had lost something once my heart was gone
Taken over by a longing to look down from on high

Glimpses here and there of friends I knew
Somehow they wavered as though an illusion
But solid in my path stood my mother
The one person who I felt that I really knew

Somehow she had always known my needs
Once an eon ago she had tried some other path
Only to find it had led her in a circle
Experience then worry led to help as we mapped my future

In the jungle there are the beasts and other dangers
They hide in the dark and attack even the wary
Through strength and resolve you fight them off
Take time to heal the wounds of body, mind and heart

Along the path you meet so many other wanderers
Listen to them and perhaps ask a few questions
For some of them have been to where you are headed
And some can help you to find the way

Given a bit of luck you may even find a companion
Someone headed in the same way you are going
You share the path and the loads of baggage each carries
Provide each other a helping hand as is needed

The foothills have cliffs and steep ravines covered by mist
In the jungle you could only see trees and vines
But in these hills occassionally you get a glimpse again
See the peaks towering above the clouds

Each view provides you with yet more detail
These foothills seem hard enough to climb
But the mountain is more rugged and difficult by far
A glance behind shows only the outlines of faded memories

Now is the time when it all seems so hard
Perilous path offers no margin for error
Friends can help but it is so narrow
That most of the time you are on your own

And the path becomes a climb straight up a wall
Which looks to have no place at all to grip
But there is no turning back at this point
For the path behind has crumbled and is gone forever

So you see a small place for hand or foot
Then commit yourself to try that way up
Each time you reach for a spot that is new
You never know if you have reached the end

Time freezes as mind and body work endlessly
Pushing and pulling and reaching and grasping
This cliff seems to go on forever and ever
But ever so slowly and relentlessly you progress

And suddenly it all seems to make sense
You feel at home on this mountain as you surmount it
It wasn't the peak you were were going after
But the knowledge that you could reach it

Undone

Some things I have never done
stuff you would think I did
Simple things we all should do
But to me these things had slid

As common as an unexpected call
to a girl you barely knew
Sure I had thought of this
On occasions which made me blue

A look into an eye which welcomes
returned immediately with a smile
such things which I could try
just seem to be beyond guile

So interest seems to wane
as undone actions fail again
And who is there to blame
but the one who had all to gain



    Wings of Future

Dreams soaring up so high
Floating with wings spread wide
Across an ocean they fly
Until they reach another side

Behind the eagle lies her home
A place she knows too well
Desire for change makes one roam
Searching before dreams can fail

Saw a place on which she was sold
Stayed a while just to see
Had to return before she's old
It was a place to be free

Traveling now so far and fast
Not sure where this flight will end
Yesterday is thousands of miles past
Life just moves on with the wind

Somewhere there are these places
Refuges from the pace she makes
A place to stop between the races
Time to recoup what it takes



Year Gone By

Sun setting on a winter day
Light set so sharply in snow
Blue shadows mark the way
Time leading us in tow

Thinking back a year ago
Across an ocean far away
Met a woman I got to know
Do I really have to say

Just a word or two we said
When something changed inside
Suddenly focused on what I read
Fate would take us for a ride

Paths are seldom very straight
Wander off them for a while
Life constantly changes rate
Kept returning to your smile

Voice that met me in the dark
Stayed there in times so blue
Echoing through places stark
Waiting patiently for a clue

One day sun showed it's face
Spirit warmed by it rays
Found the path to this place
Voice pulling through a phase

Sound was just a sign to heed
Future asked more of me
What was it that I need
It was up to me to see

Took a chance on the past
This is how to make your fate
Life begins to move at last
Better now than to late

Somehow things went so well
Seems like a childs dream
Can two weeks really tell
Are things as they seem

Story is only half written
Patient reader wants to know
Is the hero totally smitten
Will the princess really go

Time picks up it's mighty quill
Dips it in the ink of chance
Does it know how we feel
As it describes our new dance

        Girls

Skipping is a thing she did
        When glad
Bouncing lightly on happy feet
        Seldom sad

Comfort from her parent's arms
        Safely warmed
A place were things were always right
        Security formed

Baked some cookies with her mom
        Just to share
Watched her dad eat them all
        Made her stare

Dolls and toys created a world
        So far away
Friends came by to act a role
        Imagination at play

Boys were creatures sometimes mean
        What are they
Often pretended not to like them
        Things they say

Adults were huge and so complex
        Listened in awe
How could she ever be like that
        People she saw

Some things seemed right and others wrong
        She knew
Until one day she thought things through
        Add emotions too

Looked around and they were gone
        Idols fall
These heros now seemed so plain
        She grew tall

From cute to pretty to unreal
        Still a child
Men and boys no longer acted sane
        They were wild

Why couldn't they see inside her
        Confused and scared
Back off and respect her as a person
        Sometime she glared

Remember always inside this woman
        Thoughts that swirl
Who makes her way through the world
        Lives a little girl       

Things that happen to me
        Can I know the reason why
Why is it so hard to see
        Is happiness just some big lie

This Day Forward

Looking ahead I saw a door
Approached it with some concern
I had never beeen there before
Did not know what I would learn

Tried to turn and look behind
But something kept me looking ahead
A mirror showed it within my mind
Just an image of the life I led

This vision was much like a veil
Through it's fabric I could see
It represented what once was real
Filtering the view of what could be

This shield which changed my sight
Had been there for so long
It changed things like the light
Somehow this seems so wrong

So I seek to remove this curtain
Opening a window within my mind
Changing view of what was certain
Looking ahead to what I might find

And the door ahead is now ajar
Time must move me for a bit
Amazing how I can see so far
When I push myself through it

Arms Around You

warm arms that wrap around
encompass a story half told
a tale that may confound
plot that never grows old

let life pass by so quick
spin and twirl like a top
take mind off issues thick
eventually things do stop

quiet rests on things uneasy
fills time with many a thought
sometimes feel a bit queasy
when faced with what is sought

who knows the other side
guessing only leads to pain
best to let things ride
ignore the need for gain

but the arms are still there
somehow they know the game
provide a comfortable lair
things change or stay the same

deep inside we know the score
count the total in the heart
faith in life builds some more
each day provides a new start

choices do not change the fact
freedom is the hardest way
certain places stay intact
remember what the arms say

Perspectives from Top or Left

Curling tendrils of thought
Drift slowly as mourning mist
Some things cannot be bought
Dreams beaten by a fist

Seeing things through a lens
Sounds muted by am ocean
Reality has it's separate pens
Writes a story without emotion

Blue is a color far to red
Loud is a sound way too dim
What is it that is said
Let us know to sink or swim

Trying so hard to be so right
But effort is not needed here
Things should work without a fight
Maybe the message is not clear

Two tracks find their way
Paths cross as if by chance
Following together for a day
Entwined as one in a dance

What is that but a ridge
Is to another like a wall
We can use it as a bridge
Or be fearful of a fall

No matter what we say or do
The heart has it's own rules
Try and try to start anew
Life laughs at us fools

	The Gamble

Picture held in my hand
Letters written in the sand
Image only is what is seen
Mind acting as the screeen

Time to think of what to say
Thinking of you all the day
Imagination running free
You are all that I see

This dream which feels so real
You must know how I feel
Hearing you in thoughts at night
Emotions provide another sight

Thoughts via electronic mail
Making sure they do not fail
Hard to write it down just right
Words put up such a fight

Face to face we had our stay
Time to see if there is a way
Talking without word or sound
Eyes telling what is found

Touch came at long last
I remember what is past
A gamble when we rolled the dice
Caught a wave that was so nice

Fearful girl protects her heart
Can it stop her at the start
Past can hinder what could be
Pray for strength in her through me

Forward we look at some plan
Logic tells us what it can
But an ace of hearts is the one
The card which brings bright new sun

American Dreamer

American Dreamer
She looks to see
Eyes that turn greener
View blue sky through me

With the strength of steel
Body which holds the soul
Can bend with her will
Does she know the toll

Tall and oh so fair
Looking for a new start
Life lived with a flair
She touched my heart

Talk and food and wine we shared
Plans for what to dare
Don't stop because your scared
Life will decide how we fare

Strange Visitor

Tonight when I looked to the sky
There was a comet which drew my eye
This rare visitor came from afar
It seemed much fairer than a star

Came from a place that is so dark
A realm that is so cold and stark
Felt the relentless grasp of the sun
The time out there was finally done

We saw it first in early spring
Wonder what this visitor will bring
Passing through our niche in space
On it's way to some other place

It has journeyed so far and long
Seen many things right and wrong
Passing judgment is not it's way
Knows that it can never stay

Before the earth was even thought
The comet was a lonely astronaut
Time moves in millennium ticks
Sometimes the clock even sticks

But here on earth spring begins
We plant seeds to atone our sins
Cold gray winter leaves at last
Life seems to move far too fast

Unending cycle of the seasons
We must wonder about the reasons
Why are we in this place and time
Does fate really write this rhyme

In the heavens there is a clue
It is up to us what we do
Comet cannot choose where to go
Our way is waiting for us to know

Weathered Rock

I had a rock
a place to set my feet
where I could jump from
or just take a seat

but then it became cold
and a few small cracks appeared
and the ice on my rock
grew slick as the end neared

the rock weathered to a stone
was slipping slowly from my hand
my hold had become weary
as it dropped into the sand 

I stood anchored in an icy mire
alone in a darkened state 
with feet that could not move
a prisoner of my fate

but as the cold wind blew
a breath of warmth touched my mind
looking up to see the sun
begin to melt the chains that bind

as I slip these bonds that hold me
and begin find a whole new structure
a maze of walls with no end
a mass to strictures I must puncture

someday I may find another rock
that anchors me in space
an end to restless searching
a welcome end to the race

At See

May I

Look into the depth of emotion
See into eyes that tell all
For you are like the ocean
With tides that rise and fall

Wish I

Were like an island in the sea
Would be a solid place to stay
Where shipwrecked sailor could be
Living a carefree life day by day

Hope I

Have the chance to sail away
Plot a course which takes you too
On the water there is time to play
Enjoy at leisure things that are new

Can I

Help you with the things you need
Find safe ports as we roam
Know the meaning of things I read
As the wind whips the waves to foam

For You

Are a beacon that guides at night
Need tending to show the way
Sometimes we can see this light
Brightens with the things we say

We Are

Adrift upon currents of fate
Hoping to find a place to moor
Find the shore before it's late
And finally let the spirits soar

Gone

A moment ago I saw a picture
Reminder of a special place
A moment ago I heard a song
Within it I saw your face

Today I packed some boxes
Then I put them away
Today I found some memories
That will always stay

Yesterday was when you left
Farewell was not for long
Yesterday I said goodbye
Thought that I was strong

Some time ago I had a dream
A home I made with you
Some time ago love was new
So much for us to do

Now my arms are empty
As echoes slowly fade
Now my life carries on
I miss the love we made
