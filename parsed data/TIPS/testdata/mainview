                            Main Menu - View Option

    The View option of the main menu is used for changing the current
    view mode, and also provides an option for swapping the current
    default directory or stream back and forth between "source" and 
    "destination". View modes are described in detail under the help topic
    "View modes". Selection of the View option results in the display of 
    the View option pull-down menu from which the desired view may be
    selected. The options available in the view menu are described below in 
    the order of their occurence in the view menu.

Results:

    The view mode is changed so that result files in the current default
    result directory are displayed and may be manipulated.

Streams:

    The view mode is changed so that streams in the current default stream
    directory are displayed and may be manipulated.

Files:

    The view mode is changed so that files in the current default file 
    directory are displayed and may be manipulated.

Archives:

    The view mode is changed so that archives in the current default archive
    directory are displayed and may be manipulated.

Queries:

    The view mode is changed so that query files in the current default
    search directory are displayed and may be manipulated.

Stream Detail:

    A file selector is displayed which allows selection of a stream from
    the current default stream directory. On selecting a stream, the current
    view mode is changed so that the searches active against the selected
    stream are displayed and may be manipulated. Note that Stream Detail
    view mode may also be entered by opening a stream using the File - Open
    option, or by pressing the [Enter] key to select a stream to be opened
    while in stream view mode.

Swap:

    The Swap option provides a convenient means for jumping back and forth
    between a directory which is being used as the source of files being 
    copied and the destination directory to which they are being copied. 
    Swap also provides the capability for swapping back and forth between 
    source and destination streams when in Stream Detail view mode. For each
    view mode, SPOT keeps track of the directory or stream last used as a copy 
    destination so that it may be used as the default destination for 
    the next copy operation. Selection of the Swap option causes the
    destination directory or stream to become the current default, and makes 
    the items available in the new default available for manipulation. The 
    previous default directory or stream then becomes the default destination. 
    Another invocation of the Swap option will restore the defaults to the 
    state they were in prior to the initial Swap.  Swap provides a convenient 
    short-cut versus using the File - Path option to perform the same function.

