Hi Charlie,

What a nice thing it was to get that message from you. I had to 
reply immediately on reading it, but kept it brief as I was 
having one of my typical busy days. But now it is after hours and 
I can take some time to give you a proper response.

First, I too am suprised that we may get to ski together. That 
would be fantastic. You and your friends were such good company 
and made for great memories of Meribel. I think of coincidence, 
and how it was so easy to talk to you on the bus going up the 
mountain the night we met. You will find that Americans are very 
friendly too, particularly at ski resorts. Of course this seems 
to be true all over the world.

As for who will be on the trip, it may just be me that you know. 
I am trying to get Brian and Nitza to go, but they have several 
obstacles to overcome. I should know in a day or two. Rick is 
definitely a possibility, but is very unpredictable. The hard 
part with him is just getting a hold of him. I have several 
friends who have indicated they want to go. Dave, a buddy of mine 
who is an avid sky-diver is very much wanting to go. Richard, who 
is my friend of many years and boss at work will probably go. I 
am also attempting to get a rather worldly British friend of mine 
to cross the pond once again for yet another week long pub crawl 
with a bit of skiing thrown in. I also suspect that several 
friends of friends may go. Once you start putting a trip 
together, it is not that hard to find people who want to go. I will
try to get a few women to go to balance out the mix.

I also owe you some pictures. I have to track down the negatives 
which I think Brian has. When I get them I will send them.

I also must apologize for not staying in touch. I have the 
standard excuses of being very busy at work and not having enough 
time for all the things I need to do. But, those are not good 
excuses, so I will be better at maintaining contact in the 
future.

I am glad to hear that you have a job which shows promise. You 
seem to be very capable and I know you will do well. I need to 
figure out how we can do some joint business with your company so 
we can find good excuses to subsidize trips like this ski trip.

Once again, I really look forward to seeing you and Jan. Brian 
was very excited to hear the two of you are coming, and I expect 
Nitza will be too. I think this will be enough to motivate him 
and her to overcome the obstacles. I will get back to you soon.

Bye,
		Andy



PS	My address is

		3151K Covewood Ct.
		Falls Church, VA  22042

	My phone number is

		703-280-9209	(home)
		301-816-0500	(work)
