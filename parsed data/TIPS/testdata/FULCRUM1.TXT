From stevej@fulcrum.comTue Dec  5 14:42:17 1995
Date: Thu, 30 Nov 95 00:50:16 EST
From: stevej@fulcrum.com
To: afregly@merex.com
Cc: mikeh@smtplink.fultech.com, kenl@smtplink.fultech.com,
    glens@smtplink.fultech.com, daveh@smtplink.fultech.com
Subject: Discussion and Action Items

     Hi Andy,
     
     As per our call yesterday, here is my list of outstanding discussion 
     topics and action items.  Next Monday to Wednesday are open for me, 
     except from 10 to 11:30.  I'm assuming it would be a PM call to 
     California anyway.
     
     
     Action Items:
     =============
     
     .  Before we do any more development, please write down a 
        a specification of what Alan will be delivering.  This need
        not be a long formal spec, but should provide enough detail that    
        folks like Ken can provide feedback, as well as guage which of      
        his upcoming releases this can be slotted into.  I would 
        suggest that you try to cover the following (some of which you 
        have already provided):
      
        .  interface required for indexing (filter list needed, 
           how interaction with low-level text readers will be done,
           etc.)
     
        .  interface required for searching
     
        .  known limitations in the initial release (e.g. no support for
           highlighting in the Acrobat reader)
     
        .  platform rollout and timeframes
     
        .  any issues related to data compatibility with future releases
     
        .  estimated (incremental) cost
     
     
     .  SJ to check on the internal status of the 3.0 viewer problems.
        We should haul Alan off this until Fulcrum has had a chance to 
        complete its own investigation.  I will follow up with Jennifer
     
     .  AF to talk to Richard about splitting the cost of Alan's training
        time on the DDE environment for Windows.
     
     .  AF to line up a time early next week to talk to Ken Anderson and/or
        Alan Fisher regarding a number of the discussion items listed 
        below.  This will start with a conference call.  We can then decide
        whether or not a visit is required.
     
     
     Outstanding Issues:
     ===================
     
     .  Our field organization will soon need a beta version of the
        PDF indexer. Ideally, we can use the Windows application 
        already created by Alan - this could be distributed to the field.
        We should also be pushing to demonstrate a Windows client to  an
        NT server.
     
     .  What is the future of text extraction in the context of the
        Acrobat toolkit?  We need to know when the I/O redirection
        will be possible, as well as the commmitted platform coverage.
     
     .  If Adobe has no plans for NT on DEC Alpha, how do we get this
        built?  Can Merex get access to the source?
     
     .  Implementation plan for getting a Reader plug-in to do              
        highlighting.  We would love to show this at the January 20th 
        sales kickoff, and are getting it waved in our prospect's faces
        by Verity.
     
     .  What is Adobe's plan/recommended strategy for developing Internet
        applications (like Surfboard) which wish to launch and allow the
        user to navigate by hit within the document.  We discussed a 
        "low rent" solution that leverages the facilities being built into
        Netscape as well as a more sophisticated Fulcrum-specific plug-in.
        The time frames for Netscape implementation would be very useful -
        I don't want to get blind-sided by Verity.
     
     
     Talk to you soon,
     
     Steve

