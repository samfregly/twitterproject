h0371-----
r s AM-TEN--MonicaSeles Bjt   11-25 0718
^AM-TEN--Monica Seles, Bjt,0772<
^By BOB GREENE=
^AP Sports Writer=
	   NEW YORK (AP) _ Monica Seles, who completed a near-perfect year
with a victory in the season-ending Virginia Slims Championship,
hopes 1992 can be even better.
	   At 17, Seles is the youngest player in the professional era to
be honored as the World Champion of Women's Tennis, a designation
she officially won Monday with the release of the year's final
point rankings. She turns 18 next Monday.
	   ``Winning the world champion crown means I met one of my
preseason goals,'' the Yugoslavian left-hander said. ``Before the
year I said I wanted to avoid first- and second-round losses. I was
able to do that, and the consistency brought me to the next level.
	   ``Next year would be wonderful if I reached all the finals and
won all the finals.''
	   She almost did that this year, reaching the final in all 16
events she played in 1991, winning 10, including the Australian
Open, the French Open and the U.S. Open.
	   On Sunday, she won the Virginia Slims Championships for the
second straight year. She skipped Wimbledon because of shin
splints.
	   Seles also became only the second female athelete to surpass $2
million in earnings in a single year, surpassing Martina
Navrtilova's 1984 record of $2,173,556. Counting bonus monies,
Seles earned $2,457,758.
	   Her 20 titles before the age of 18 is second only to the 23 won
by Tracy Austin. But Austin had only one Grand Slam crown before
turning 18, the 1979 U.S. Open, while Seles has won four _ three in
1991 plus the 1990 French Open.
	   In only three full years on the Kraft Tour, Seles has earned
more than $4 million, putting her sixth on the all-time money list.
	   This year, Seles had a 74-6 match record, losing twice to
Navratilova, twice to Steffi Graf and once each to Gabriela
Sabatini and Jennifer Capriati, all of whom are ranked in the top
six in the world. She became only the seventh player in the pro era
to win 10 or more tournaments in a single year.
	   ``She moves great,'' Navratilova, who lost 6-4, 3-6, 7-5, 6-0 to
Seles on Sunday, said of the top-ranked player. ``She doesn't look
like a great athlete, but she gets to the balls.
	   ``She stays so far inside the court that you can't really hit a
short ball to her because she will be on top of it. She is always
wanting to move forward. and she moves back very well as well.
	   ``She doesn't look like she is going to get there, but the next
thing you know the ball is going by you. And she also has very good
anticipation.''
	   Navratilova, who dominated women's tennis for nearly a decade,
said even at her best she would be unable to beat Seles playing at
the top of her game.
	   ``It is very different now,'' Navratilova said. ``But I think I
still would have gotten beat had I played like I did 10 years ago
or eight years ago, when I was dominating, becuse I did not have to
play Monica back then.''
	   How dominating was Seles?
	   Not counting her six losses, she dropped only 11 other sets the
entire year, and only once _ in the semifinals of the Australian
Open in January to seventh-ranked Mary Joe Fernandez _ did she lose
a set at love.
	   ``She puts so much pressure on you,'' Navratilova said. ``She
hits such great shots ... you can't relax for one second. She is
very, very tough mentally.''
	   Seles, who serves left-handed, hits both her backhand and her
forehand with two hands, giving her extra power and extraordinary
control. She doesn't just hit the ball, she jumps all over it,
ripping shots at incredible angles.
	   ``She puts more pressure on you from the baseline than anybody I
have played against,'' Navratilova said. ``Because she hits (with
both hands) on both sides, you never rest.
	   ``With Steffi, as great as she is and the unbelievable forehand
that she has, you get it to the backhand and you are OK for a
little bit. But with Monica, you don't really have an opening.''
	   Fernandez was ousted by Seles in the quarterfinals of the
Virginia Slims Championships.
	   ``You can't afford to make any errors against her,'' Fernandez
said. ``She takes advantage of them right away.
	   ``When you're playing against someone like a machine like
Monica, it does make a difference. You are fighting for every
single point. It gets very frustrating.''
	   AP-DS-11-25-91 1820EST<


