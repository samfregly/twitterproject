p0716-----
u w AM-CongressRdp 3rdLd-Writethru   11-25 1187
^AM-Congress Rdp, 3rd Ld-Writethru,1154<
^Senate Votes for Post-Cold War Soviet Aid<
^Eds: SUBS 8th graf, Congressional tax, with 2 grafs to reflect
committee action on tax extenders bill, other unresolved issues;
INSERTS new graf after 11th graf pvs, The question, with airlift
aid; ADDS 2 grafs on START treaty.<
^By JIM DRINKARD=
^Associated Press Writer=
	   WASHINGTON (AP) _ The Senate voted Monday to spend up to $500
million to help the Soviet Union dismantle its nuclear arsenal
rather than risk letting it fall into the hands of terrorists or
third-world dictators.
	   The 86 to 8 vote, as Congress pushed toward a pre-Thanksgiving
adjournment, came after several Democratic senators declared a
political truce on the issue, promising not to criticize President
Bush if he goes ahead with the aid.
	   The Senate also voted 90 to 4 to approve a treaty setting strict
ceilings on conventional weapons in Europe _ a pact that nearly has
become an anachronism just a year after it was signed. The tally
was well above the two-thirds needed for approval.
	   Despite claims it is outdated, Majority Leader George Mitchell
said the Conventional Forces in Europe Treaty ``remains an
important benchmark and building block for ensuring the future
security of the European continent.''
	   With just one day left before adjournment, lawmakers took these
other actions:
	   _House Democrats were poised to seek a floor vote on a
compromise crime bill that President Bush said was ``simply not
acceptable.'' Republicans, meanwhile, threatened to filibuster the
legislation in the Senate, saying the bill was too soft on crime.
	   _House and Senate negotiators completed work on major sections
of a $151 billion highway measure hailed as capable of putting some
2 million Americans to work. But the tax-writing committees had yet
to approve a four-year extension of a 2.5 cent-a-gallon gasoline
tax, necessary to pay for much of the bill.
	   _Congressional tax committees approved a bill to extend 12
targeted tax breaks scheduled to expire Dec. 31. The extensions are
strongly supported in both parties as helpful in a time of economic
stress.
	   _Banking committees worked on must-pass bills to replenish the
Federal Deposit Insurance fund by $30 billion and to provide $80
billion to close down ailing savings and loans. And negotiators
pressed to improve unemployment benefits even beyond the extension
enacted two weeks ago.
	   On Soviet aid, debate revolved around whether helping dismantle
the former adversary's nuclear weapons was an urgent enough need to
warrant U.S. taxpayers picking up part of the tab.
	   ``The question ... is, whether having won the Cold War, we are
willing to join with our former adversaries to eliminate the
Armageddon arsenals'' it produced, said Sen. Joseph Biden, D-Del.
``To defeat it would be a reckless gamble with history.''
	   Senators also voted 87 to 7 to allow the Pentagon to use an
additional $200 million for emergency airlift of food and medical
supplies to needy areas of the Soviet Union this winter.
	   The Soviet aid provision was attached to a technical bill
allowing implementation of the CFE treaty by authorizing transfer
of some conventional weaponry to other NATO countries.
	   The bill still had to go to the House, which has approved the
CFE-implementation measure, but without the Soviet aid provisions.
	   White House press secretary Marlin Fitzwater said Bush was
``extremely pleased by the Senate's resolution of advice and
consent to ratification of the CFE Treaty.''
	   ``This action could not be more timely,'' said Fitzwater, adding
that the treaty ``is the cornerstone of the new security structure
we have been working to construct in Europe.''
	   The proposal would allow the Bush administration to shift up to
$500 million from elsewhere in the Pentagon budget to provide
technical help to the Soviets to dismantle as many as 15,000
tactical nuclear weapons over several years.
	   The weapons include nuclear mines, artillery shells, bombs and
short-range missile warheads that President Mikhail Gorbachev has
promised to destroy. That promise is similar to a pledge by
President Bush to get rid of tactical nuclear weapons in the U.S.
arsenal.
	   Intelligence reports have painted a picture of deteriorating
order in the Soviet Union as republics assert their independence
and competing political leaders grope for power. Food shortages and
economic upheaval are adding to the volatile mix, experts say.
	   The aid measure was a pared-down version of a $1 billion Soviet
aid package that was withdrawn less than two weeks ago after it ran
into a tide of anti-foreign aid feeling in Congress.
	   Monday's debate highlighted U.S. confusion about how to respond
to a situation it cannot predict; whether American security is best
served by aiding its former adversary or by standing back and
watching it self-destruct.
	   Sen. Malcolm Wallop, R-Wyo., objected to ``asking the U.S.
taxpayers to pay for dismantling the very weapons that were
constructed to destroy them.''
	   But Senate Intelligence Committee Chairman David Boren, D-Okla.,
said without intervention, ``there is a genuine threat of the
proliferation of nuclear weapons, of these weapons falling into the
wrong hands.''
	   The debate came less than a week after Bush announced an
additional $1.4 billion in credits and grants to provide food and
medical aid to the Soviet Union this winter.
	   The CFE treaty, signed by Bush and Gorbachev in Paris a year
ago, would set ceilings on conventional weaponry _ tanks, artillery
and combat aircraft _ deployed by both sides in the region between
the Atlantic and the Ural Mountains.
	   Under the ceilings, the United States and its 15 NATO allies
will have to destroy fewer than 3,000 major weapons, while the
Soviets and the five members of the defunct Warsaw Pact will be
forced to scrap almost 20,000.
	   The Senate added a condition that the United States would seek
parallel agreements with any republic that breaks away from the
Moscow government.
	   And it directed the administration to keep trying to clear up
discrepancies between the number of weapons Moscow says are located
in the treaty area and the higher number estimated by U.S.
intelligence.
	   Senators voting against the Soviet aid provision were all
Republicans: Hank Brown of Colorado, Larry Craig of Idaho, Don
Nickles of Oklahoma, Larry Pressler of North Dakota, John Seymour
of California, Robert Smith of New Hampshire, Steve Symms of Idaho
and Wallop.
	   Not voting were Daniel Akaka, D-Hawaii; Alan Dixon, D-Ill.; Tom
Harkin, D-Iowa; Jesse Helms, R-N.C.; Bob Kerrey, D-Neb., and David
Pryor, D-Ark.
	   Also on Monday, President Bush submitted to the Senate the
treaty he and Gorbachev signed in July to slash long-range nuclear
weapons. The Strategic Arms Reduction Treaty (START) cuts
long-range U.S. and Soviet nuclear weapons by about 35 percent.
	   Bush and Gorbachev subsequently unveiled proposals to make even
deeper cuts. These additional cuts were taken up at the State
Department on Monday by U.S. and Soviet negotiating teams.
	   AP-DS-11-25-91 1824EST<


