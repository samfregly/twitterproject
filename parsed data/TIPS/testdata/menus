				Using Menus

Overview:

  Several types of menus may be presented by this program. A "display" menu
  is a list of items which is presented in the display area of the screen.
  A display menu is typically used to allow selection of items or files on 
  which some function is to be performed. A "command" menu is a menu displayed 
  in the menu bar at the top of the screen. Command menus are used for 
  selecting from the major functions available at that time. A "pull-down" 
  menu is a menu which pops up immediately beneath an entry in another menu 
  when that entry is selected. Pull-down menus are used for selecting specific 
  function(s) to be performed.

Display Menus:

  A display menu is presented in a scrollable portion of the screen. The
  purpose of a display menu is to provide a display of items of interest,
  and to allow the selection of items on which a function is to be performed.
  Depending on the type display, one or more items may be selected as
  targets for a function. The following lists the function keys available for
  use with a display menu: 

	up arrow	Moves item selector up one item.
	down arrow	Moves item selector down one item.
        left arrow	Moves item selector to the left one item.
	right arrow	Moves item selector to the right one item.
	Page-Up		Pages display back a page.
	Page-Down	Pages display forward a page.
	Home		Displays first page of display.
	End		Displays last page of display.
	[Enter]		Selects current item for default function performed
			by the display.
	[Exit]		Exits display.

	The following functions are available in displays which allow
	selection of multiple items.

	[Space-Bar]	Toggles item selection.
	*		Selects all items.
	>		Toggles selection of all items from current item
			to next item which is not in the same selection state
			as the current item.
	<		Toggles selection of all items from current item	
			to closest previous item which is not in the same
			selection state as the current item.

Command Menus:

  A command menu is presented in a horizontal bar just above the display
  area of the screen. While in a command menu, the left and right arrow
  keys may be used to move from item to item, and the [Enter] key may be
  used to select an item. The menu may be exited and display item selection
  resumed by entering an up or down arrow, and the [Exit] key may be used to
  exit the current display. 

Pull-Down Menus:

  A pull-down menu is presented as a list of items in a box immediately below
  a menu-option which has been selected from another menu. While in a
  pull-down menu, the up and down arrow keys may be used to move from item to
  item, and the [Enter] key may be used to select an item. Some pull-down
  menus allow selection of multiple items, which may be selected in the
  same manner as is used for multiple item selection in display menus. A
  pull-down menu may be exited by attempting to move the item selector out 
  of the menu with the up or down arrow.

  The left and right arrow keys may also be used to exit a pull-down menu.
  If the menu was invoked off from a command menu, the previous or next item
  in the command menu becomes the current command menu option, and the 
  pull-down menu for the option is displayed if it exists.

Hot Keys:

  A hot key is a key which may be pressed during command or pull-down menu
  item selection to invoke immediate execution of a specific function. This
  saves the effort of using the cursor keys to advance the item selector to 
  the desired function. If hot keys are available in a menu, the hot key 
  character in each menu item will be highlighted.

Short Cut Keys:

  A short cut key is a hot key from a command menu which may be entered during
  selection from a display menu to immediately invoke the corresponding
  function off the command menu. The experienced user may use short cut keys
  and hot keys as a means of quickly performing system functions without
  having to resort to using the arrow and [Enter] keys.

