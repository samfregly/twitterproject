                            Browse Menu - View Option

    The View option of the main menu is used for changing the current Browse 
    view mode. The SPOT Browser supports three view modes, "Documents", 
    "Names", and "Summary". In "Documents" view mode, the Browse display is 
    used for showing the full text of a single document. In "Names" and 
    "Summary" view modes, the display shows one line for each document 
    available for browsing. In these two modes, multiple document may be 
    selected for manipulation by other Browse functions, allowing the user to 
    quickly review and manipulate multiple documents. 

    Selection of the View option results in the display of the View option 
    pull-down menu from which the user may select the view function to be 
    performed. The options available in the view menu are described below in 
    the order of their occurence in the view menu.

Documents:

    The view mode is changed so that the full text of the current document
    is displayed in the Browse display. 

Names:

    If Browse is being used to view documents which are not "Stream Detail"
    search results, the display is immediately changed to a list of the
    names of available documents. This display acts as a display menu, and
    is used for the selection of documents to be manipulated by other Browse
    functions. The help topic "Using Menus" describes how a display menu may
    be used. 

    If Browse is being used to view documents for "Stream Detail", a pull-down
    menu will appear with the options "Continuous" and "Select". The Continuous 
    option will result in a continuously updated display of the names of the 
    latest documents which have been added to the result file for the stream 
    search. The display will be presented until the user changes the view mode
    or exits Browse. If "Select" is choosen, the display and functioning is
    as described in the preceeding paragraph.
    
Summary:

    The selection of the "Summary" option results in the display of a pull-down
    menu with the options "Line", and "Field". Selection of the "Line" option
    will result in the user being prompted for the line number which is to be 
    displayed from each document. Selection of the "Field" option results in
    a prompt for the field name to be displayed from each document. After 
    selection of what is to be displayed in the Summary view mode, one of two 
    courses may be taken.

    If Browse is being used to view documents which are not "Stream Detail"
    search results, the display is immediately changed to show the summary
    information for each available document. This display acts as a display 
    menu, and is used for the selection of documents to be manipulated by 
    other Browse functions. The help topic "Using Menus" describes how a 
    display menu may be used. 

    If Browse is being used to view documents for "Stream Detail", a pull-down
    menu will appear with the options "Continuous" and "Select". The Continuous 
    option will result in a continuously updated display of the summary
    information for each available document, showing the latest documents 
    which have been added to the result file for the stream search. The 
    display will be presented until the view mode is changed or Browse is
    exited. If "Select" is choosen, the display and functioning is as 
    described in the preceeding paragraph.

Note:

    At any time during presentation of menus after selection of the "Names"
    and "Summary" options, the view function may be terminated by pressing
    the [Exit] or [Quit] keys.

