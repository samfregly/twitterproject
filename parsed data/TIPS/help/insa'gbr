
If this is an initial spot installation, you will need to take the following
steps:

  1. Disable logins on any of the ports which you are now using for spot.
     This is done by entering commands such as:

	disable tty1a
	disable tty2a
	...

  2. Add a line at the bottom of the /etc/inittab file which will automatically
     start up the spot daemons when the system is booted. The line should
     look like this:

	spot:2:wait:/usr/spotsys/script/spotboot 1>>/usr/spotdata/log/spotboot.log 2>&1

  3. Initiate the spot daemons by entering the command:

	/usr/spotsys/script/spotadm startup


  4. Start the initial spot maintenance batch job by entering the command

	echo "sh /usr/spotsys/script/spotat" | at <time> <day>

     where <day> is the day you want it to initially execute and may 
     have a value like "tomorrow" or "Saturday" or "Sunday next month", and
     <time> may have a value like "midnight", or "3am" or "6:30pm". For 
     example, to run the batch job at 12am tomorrow, enter the command:

	echo "sh /usr/spotsys/script/spotat" | at midnight tomorrow

     Once started, the batch job will automatically resubmit itself each time
     it executes.

  5. For each user who wants to use spot, the following command should be
     put into the .profile file in their login directory.

	PATH="$PATH:/usr/spotsys/script:/usr/spotsys/bin"

  6. Each spot user should create a directory for storing their searches and 
     another for storing their private archives. Typically this is done by 
     entering commands such as:

	mkdir $HOME/searches
	mkdir $HOMR/archives

  7. Each spot user should then create a .spotrc file in their home directory
     containing the following variable definitions so that spot will know
     about their private search and archive directories.

	SP_SEARCHES=$HOME/searches
	export SP_SEARCHES
	SP_DARCHIVES=$HOME/archives
	export SP_DARCHIVES"

The spot installation procedure may be run at any time to reconfigure the
system. It must be run by the super-user, and is invoked with the command:

	/usr/spotsys/script/spsetup

These installation notes are in the spot help system and are contained in the 
file:

	/usr/spotsys/help/insaddendum

