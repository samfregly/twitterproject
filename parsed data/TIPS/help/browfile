			    Browse - File Option

    The File option of the Browsers main menu is used for performing document 
    maintenance tasks on the documents currently being Browsed. and also 
    allows a temporary escape to the shell command line. Selection of the File 
    option results in the display of the File option pull-down menu from which 
    the specific File function to be performed may be selected. The options 
    available in the file menu are described below in the order of their 
    occurence in the menu.

Open:

    The open option is used to invoke an application program that allows the
    current document to be viewed in it's native format and modified.  The 
    application program that is invoked to modify the document is determined 
    based on the document type and the application which has been set up for 
    that type of document during SPOT customization. Documents types for which 
    no application has been defined are modified using the default SPOT editor,
    usually vi. The steps required in defining applications for various document
    types is described in the help topic "Customization".

Pick:

    Pick causes the removal of the "File" pull-down menu and the main menu.

Copy:

    Copy is used for making duplicates of the current document, or in "Names" 
    or "Summary" view modes, selected items may be copied in mass. The Copy may
    be to a result file, an archive, or a flat file.
    
    Depending on the current Browse view mode, and the option chosen from the
    first pull-down menu presented during the copy process, one to three 
    pull-down menus will be presented. The first menu is used to specify the 
    type of destination. The options in this menu are "File", "Results", and 
    "Archive", representing the three types of copy destinations currently 
    supported by SPOT. The next menu which may be presented is invoked in 
    Browse view modes "Names" and "Summary" when documents have been selected 
    in the Browse display. This menu has the options "Current" and "Selected", 
    allowing the choice of copying the current document, or all selected 
    documents. The final menu which may be presented is invoked if the "File" 
    option was selected off the first pull-down menu, and contains the options 
    "Replace" and "Append". Selection of the "Replace" option will result in 
    the destination file for the copy being replaced by the copy operation, 
    and selection of the "Append" option will result in the copied documents 
    being appended to the end of the destination file.

    After completion of option selection from the pull-down menus presented
    by the Copy function, a file selector is presented from which the
    destination result file, archive, or file is chosen. A file may be
    created by the copy operation by entering a new file name in the path
    name display of the file selector. A detailed description of the use of 
    a file selector is found in the help topic "File Selectors".

    At any time during the prompting portion of the copy process, the copy
    may be canceled by pressing the [Exit] or the [Quit] key. The copy
    operation may also be canceled during selection off the pull-down
    menus by using an arrow key to move the item selector off the menu.

Delete:

    The Delete option is used to delete the current or selected documents (in
    browse view modes "Names" and "Summary". The delete option is a superset
    of the delete function invoked by the [Delete] function key when viewing
    a document. Delete always provides the option of deleting the document(s),
    versus just the document reference(s). After Delete is selected from the 
    Browse File menu, a menu with the options "Original Document(s)" and 
    "From Archive" is presented. This menu allows multiple item selection, so
    one or both options may be selected with the space bar (see the help topic
    "Using Menus" for a description of mulitple item selection). The 
    "Original Document(s)" is used to specify the deletion of original 
    document files which served as the source for archive documents that are 
    among those selected for deletion. The "From Archive" option is used to 
    specify that documents selected for deletion which reside in an archive 
    are to be deleted from the archive. After selection of what type of
    documents are to be deleted, a requestor is presented asking for 
    confirmation that the deletion should be performed. At any time during the 
    presentation of menu's during this deletion process, the deletion may be 
    canceled with the [Exit] or [Quit] keys. The deletion process will then 
    take place if the operation is confirmed, with all selected documents 
    deleted and when viewing a result file, the document references are deleted     also.
    
Print:
    
    The Print option is used to send a copy of the current or selected items 
    to the system printer. The print option is only valid in "Results", 
    "Files", "Queries", and "Stream Detail" view modes. The printed file will 
    be sent to the default UNIX system printer, or to the printer specified 
    with the environment variable "SP_PRINTER" if SP_PRINTER is defined. 
    Additional environment variables may be set to control the margins and 
    page length of the printed file. The help topic "Customization" contains 
    detailed information about customization of printer output, 
    
Shell:
    
    The Shell option results in temporary suspension of SPOT so that commands
    may entered from the shell command line. The shell is exited and SPOT
    resumed by entering the exit command for the shell, usually "exit" or
    "login" depending on the type of shell being used by the user.
    
Exit Browse:
    
    The Exit Browse option causes the Browser to terminate, resultng in a
    return to the display which was being viewed prior to invocation of the 
    Browser.

