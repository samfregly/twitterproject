		     SPOT - A Real-Time Text Search System

    SPOT is a UNIX based software system which provides real-time full text 
    search and document review capabilities for environments where documents 
    are being continuously updated or added to the system. SPOT has been 
    developed specifically to meet the needs of the professional who must stay 
    on top of events or changing requirements related to their specific 
    interest areas. SPOT is typically used for routing of news wires, 
    intelligence message traffic, OCR input, resumes, RFPs, electronic mail, 
    and other forms of volatile textual data. With SPOT, users will instantly 
    see those items pertaining to their specific interests. To meet changing 
    information requirements, the criteria used to route documents of interest
    may be instantly updated, plus a historical search capability allows 
    research of previously processed documents. SPOT also is an ideal general 
    purpose file management tool for environments which deal primarily in 
    textual data, such as word processing or documentation management 
    departments.

    UNIX was selected as the operating system under which SPOT works in order 
    to provide the broadest possible range of compatible hardware and software. 
    This provides the  flexibility of selection of exactly the amount of 
    processing power necessary to meet user needs, either on current or 
    additional computer systems. The power of the software minimizes the 
    required hardware investment, with PC based systems capable of serving a 
    site with 20 or more users.

    The following lists just a few of the features that make SPOT the leader 
    of the pack in real-time text search systems:

      o	A high performance search engine capable of multitudes of 
	simultaneous searches against multiple incoming document streams.

      o	An intuitive pull-down menu user interface with a variety of 
	features tailored to the real-time text processing environment.

      o	An easily learned query language for specifying the criteria by 
	which documents should be routed. The language provides the power
	of boolean logic, phrase searching, wild cards, numeric ranges, 
	proximity searches, field specifiers, and threshold searches.

      o	Input support software which allows the system to handle almost 
	any type of textual data.

      o	Output support software which provides a flexible output routing 
	system which in addition to presenting documents for review within
        the SPOT user interface, may also be configured to transfer 
	documents in a variety of ways including: copying across a network, 
	local and network mail, faxing, file transfer using telecommunications
	programs and transfer of documents into other applications.


	       For more information about SPOT, please call 
		   Andrew Fregly at (703) 402-0086 (USA)


		      Copyright (c) 2004, Andrew M. Fregly
		SPOT is a copyrighted product of Andrew M. Fregly
			    All Rights Reserved

