    				Browse - Main Menu
    
    The main menu for the SPOT Browser is the means by which the document
    manipulation functions available in the Browser are invoked. These 
    functions are organized into four categories, each of which has a 
    corresponding menu option in the main menu. These four options are: 
    "File", "View", "Search", and "Action". Selection of one of these options 
    off the main menu usually results in display of a pull-down menu containing 
    options for the functions available under the selected main menu option. 
    A specific function may then be selected from the pull-down menu, or the 
    left and right arrow keys may be used to view the other pull-down menus 
    available under the main menu.
    
    The main menu normally is not visible, and must be invoked in one of
    several ways. The [Menu] key specified on the help line at the bottom
    of the screen may be pressed, or a hot key for any of the options on
    the main menu may be pressed. Once invoked, the main menu is displayed
    in the menu bar at the top of the screen, and if a hot key was used to
    invoke it, the pull-down menu for the selected option is displayed. While
    in the main menu, option selection is performed as described in the
    help topic "Using Menus". The menu may be exited prior to selection of
    an option (and the display of the option pull-down menu) by pressing 
    an up or a down arrow. While pull-down menus are being displayed, the
    main menu may be exited by moving off the top or the bottom of the 
    pull-down menu with the up or down arrow keys.
    
    While in the main menu or viewing a document while the main manu is
    not displayed, the Browser may be exited by pressing the [Exit] or the 
    [Quit] key. Note that an exit may also be performed by selecting 
    "Exit Browse" off the "File" pull-down menu.

