				      hitw

Description:

    This function is used to add the results (TIPS document descriptors)
    to output result files. hitw will create the result file(s) if they do not
    already exist. Result files are assumed to have an extension of ".hit".

Syntax: 

	hitw {switches} {hit_file}

Switches:

	-l	Enable logging.

	-h	Write results in result files corresponding to queries.

	-o	Optimize file size of result files (re-use deleted records)

	-p mask	Octal value which specifies protection mask for created result
		files. The default is u+rw, g+r.

Parameter:

	hit_file	Optional spot result file into which all references for
			all documents processed by hitw are placed.

Examples:

    /usr/spotsys/bin/receiver -i usr/spotdata/asynchin -l -o \
    /usr/spotdata/apin -r 15 2>>/usr/spotdata/log/receiver.ap | \
    /usr/spotsys/bin/arcload -l /usr/spotdata/archives/ap \
    2>>/usr/spotdata/log/arcload.ap | \
    /usr/spotsys/bin/spotter -l -p -a /usr/spotdata/archives/ap -s  \
    usr/spotdata/streams/ap.qlb 2>>/usr/spotdata/log/spotter.ap | \
    /usr/spotsys/bin/hitw -l -h 2>>/usr/spotdata/log/hitw.ap >/dev/null &

	This command is similiar to a command which would be used to initiate
	spot daemons to search a stream. The initial input directory for the 
	stream is /usr/spotdata/asynchin, with incoming documents moved to
	/usr/spotdata/apin after being processed. Documents are also placed
	into the spot archive /usr/spotdata/archives/ap.sar. The searches 
	against the stream are drawn from the spot query object library
	/usr/spotdata/streams/ap.qlb. Results from the searches will use the 
	archive name and document number to refer to the documents, and the
	results for each search will be written to the result file corresponding
	to the name and location of the source query for the search. Each
	daemon will append log messages to a log file in directory 
	/usr/spotdata/log.

