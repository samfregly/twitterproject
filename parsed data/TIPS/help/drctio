				drctio


Description:

    This function provides a full suite of functions for accessing a direct 
    access file including: file creation, reading. writing, updating, 
    file deletion, record deletion. Direct access files are used throughout
    the SPOT system, as "result" files, archives, and query object libraries
    are all direct access files. The drctio program provides a standard means
    of performing maintenance and status functions on all of these types
    of files.

Syntax: 

	drctio {switches} direct_file_spec

Switches:

    -a		Append file specified by the -f switch. This is by far the
		most cpu and disk i/o efficient method of updating a direct
		access file, but is the least efficient for disk space 
		utilization. The -a switch is recommended for use with files
		which seldom have records deleted, files which must be updated
		quickly, and for file where the record ordering is important.

    -c		Create direct access file.

    -d		Delete record(s) specified by the -r switch.

    -f file	Specifies file for the -a, -h, -u, -w, -x, and -z switches.

    -h {{s:}e}	Extract document headers, where the header consists of the
		range of lines specified by s:e. If s is not supplied, it
		defaults to 1. If e is not specified, it also defaults to 1.
    -i		Display status information for direct access file.

    -o	        Add record specified by the  -f switch, reusing space occupied
		by deleted records if possible. The -o switch consumes more CPU
		and disk I/O than the -a and -w switches, but is the most 
		efficient at utilizing disk space. The -o switch should be used
		when space is at a premium for a file that has frequent record
		deletions, and for which the ordering of records is unimportant.

    -p mask	Octal protection mask to be applied to created file. By default,
		a value giving the owner read/write access, group read access,
		and others no access is used.

    -r n{:m}	Specifies record or range of records for -d, -u, -w,
		and -x switches. A trailing specifier of "-1" is used
		to specify through the end of the file.

    -s		Send records specified by the -r switch to the direct access
		file specified by the -f switch.

    -t seconds	Sets read and write timeout on record lock requests to the 
		specified number of seconds. If seconds is set to -1, no 
		timeout is used.

    -u		Update the record specified by the -r switch with contents
		of the file specified by the -f switch.

    -v		Verify file structure (unimplemented).

    -w          Write into the direct access file the file specefied by
		the -f switch. A write creates a new record number for the
		entyr, but reuses deleted space if possible. The -w switch
		use larger amounts of CPU and disk i/o than the -a switch,
		but is more efficient in disk space usage. The -w switch is
		recommended for use with files which have frequent record
		deletions, and for which record ordering is important, and
		which are not going to have the data portion of the file
		read by a foreign program which reads the data sequentially.

    -x		Extract the record(s) specified by the -r switch, writing
		them to the file specified by the -f switch.

    -z		Zap (delete) direct access file.

Parameter:

    direct_file_spec

	Direct access file to be manipulated by the drctio command. A direct
	access file actually consists of two files, one with the name specified
	as direct_file_spec, and a companion file with a file type of ".drc".

Example:

    /usr/spotsys/bin/drctio -h /usr/spotdata/stream/ap.qlb

	This command writes to standard output the first line for all object 
	queries in the query object library /usr/spotdata/stream.qlb. Note that
	the  first line of each object query is a SPOT document descriptor of 
	the object query, and tells the name of the source query from which the
	object query was created.

    /usr/spotsys/bin/drctio -h /usr/spotdata/archives/apweek10.sar | \
    /usr/spotsys/bin/hitw /usr/spotdata/archives/apweek10

	This command will create a result file which references the original
	files for all documents in the archive /usr/spot/archives/apweek10.sar.
	Note that the first line of each record in a SPOT archive is a SPOT
	document descriptor which contains the name of the original file from
	which the archive entry was made.

