			Browse Menu - Action Option

    The action option is used to invoke a site specific menu which may 
    be customized for each site and/or user. The purpose of the Action option 
    is to allow site or user specific processing of the current or selected 
    documents which are being Browsed. For instance, a menu option might be 
    defined which mails selected documents to users on a PC network which is 
    linked to the UNIX system on which SPOT is executing. The steps needed in 
    defining the action menu are described under the help topic 
    "Customization". If a custom Action menu has been defined at your site, 
    this help text should be updated to reflect the usage of the Action menu.

    The Action option will be ignored if the Action menu is undefined. If the
    Action menu is defined, the menu will be displayed. After an option
    is picked off the menu, a second menu will be displayed if the current 
    browse view mode is "Names" or "Summary", and documents are selected in
    the browse display. This second menu will contain the options "Current"
    and "Selected". The "Current" option is used to specify that the current
    document in the display is to be used with the Action menu option, and
    the "Selected" option is used to specify that all selected documents are
    to be used with the Action menu option.

    The Action option may be canceled at any time during the presentation
    of pull-down menus by entering the [Exit] key or [Quit] key, or by 
    moving the option selector off a pull-down menu.

