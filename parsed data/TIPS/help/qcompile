				  qcompile


Description:

    This program is used to compile queries, producing object query files and 
    updating spot object query libraries.  Object queries may be use with the 
    spot afind command to perform searches from the shell command prompt. The 
    object query will have a file type of ".qc".  If any errors occur during 
    compilation of a file, the errors will be placed in  a ".err" file with 
    the same name as the query file containing the errors.

Syntax: 

	qcompile {-o query_object_lib} fname {fname...}

Switches:

	-o object_lib	Create/Update specified query object library. Query
			object libraries have a default file type of .qlb.

	-p mask		Octal number specifying the protection mask for 
			created object libraries.

Parameters:

	fname	One or more file names of files containing the source text
		for a query to be compiled. A default file type of ".qry"
		is assumed.

Example:

    qcompile -o /usr/spotdata/streams/ap $HOME/searches/hottopic.qry
    sendmsg -m 1 /usr/spotdata/streams/ap.qlb

	The first command will compile the source query in the file
	$HOME/searches/hottopic.qry, inserting or replacing the object
	query in /usr/spotdata/streams/ap.qlb. The second command notifies
	the spotter searching a stream with the queries in the object 
	library that a query has been inserted or updated, and that it should
	start using it.

