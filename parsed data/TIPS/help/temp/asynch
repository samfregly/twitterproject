				   asynch

Description: 

    The asynch daemon is used to continuously read data from a communications 
    port. It's most common use is to handle a wire service feed or intelligence 
    feed being received from a modem. The asynch daemon will store the incoming 
    text in output files in the specified output directory with the names of 
    the files fitting the template: 

	{prefix}{month}{daynum}{sec}{suffix}

    where {prefix} is a 1 to 3 character feed name suppied as a parameter 
    to the program, {month} is a one digit hexadecimal number corresponding 
    to the month number for the current year, {daynum} is a two digit hex 
    number specifying the day within the month, {sec} is a 5 digit hex number 
    specifying the number of seconds since midnight, and suffix is a lower 
    case alphabetic character used to differentiate between files which were 
    created within the same second. This naming convention insures that file 
    names collate alphebetically according to their time of receipt.

Syntax:

    asynch {switches} 

Switches:

    -b {bod_marker}	Beginning of document marker. This is specified as
			a hexadecimal number. The default bod marker is a
			0x1.

    -d device		Specifies name of device file for the communications
			port to be handled by the asynch command. This is
			usually a file in "/dev".

    -e {eof_marker}	End of document marker. This is specified as a
			hexadicimal number. The default eod marker is a
			0x4.

    -l			Log initiation, status, termination and message 
			receipt. Normally, the log file is written to 
			standard error, and contains only fatal error 
			messages. If -l is specified, asynch will write 
			a log message for each recieved file in the format of:

		<time stamp>, received: <file name>, size: <size in bytes>


    -m			Use modem control signals for synchronization.

    -o directory	Specifies a directory in which files are to be
			put by asyncman.

    -p prefix		File name prefix.

    -s speed		Set port speed to specified baud rate. A default
			baud rate of 2400 is use.

    -w {directory}	Work directory. The input file currently being recieved
			is put in this directory and moved to the output
			directory once the document is completed. By default,
			the work directory is the directory from which
			the asynch program was initiated.

    -x			Use xon/xoff for synchronization.

    -y {even | odd | none}	Set parity to specified type

    -7			Set word length for received data to 7 bits.

    -8			Set word length for received data to eight bits.

Example:

    /usr/spotsys/bin/asynch -l -d /dev/tty2a -o /usr/spotdata/asynchin \
    -w /usr/spotdata/asynchwork -y none -8 -s 1200 -p ap -x \
    2>>/usr/spotdata/log/asynch.ap &

	This command initiates an asynch daemon which will read data from port
	/dev/tty2a, using directory /usr/spotdata/asynchwork as a working 
	dircectory, and placing received files in directory 
	/usr/spotdata/asynchin. The files will all have names starting with the
	prefix "ap". The communications protocol which will be used is 1200 
	baud, 8 bits, no parity, using xon/xoff flow control. Logging will be 
	appended to the file /usr/spotdata/log/asynch.ap, and will include a log
	message for each document received. The "&" at the end of the command
	lets asynch run asynchronously.

