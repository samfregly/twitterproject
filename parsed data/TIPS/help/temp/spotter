				spotter


Description:

    This program meant to be used as a daemon for profiling of textual data. 
    Documents are fed to it by being piped into standard input, and spotter 
    writes to standard output the names of processed files and the identities 
    of any searches which matched the file. The input file must be preceeded 
    by a descriptive header in a standard format (as produced by "receiver", 
    "anpa",...). The header takes the format of

    	identifier,length{,key}<lf>

    Where identifier is an ASCII identifier for the document (file name,
    index,...), length is an ASCII numeric specifying the length of the 
    document in bytes, and key is an optional user definable value which may 
    be used for a variety of purposes (document type specifier, security 
    classification,...).

    The output format of spotter is a stream of line feed separated descriptors,
    which may have the key field prefixed by a matching query name, with a "#" 
    character ending the prefix.

Syntax: 

	spotter {switches} objlib_name

Switches:

	-a name	Archive name. Spotter will prefix output document descriptors
		with the supplied name. This option is used if an archive is
		supplied as input via input redirection, and it is desired
		that output search results point at the archive rather than the
		original document. The prefixed name is terminated with a '#'.

	-d	This switch is used to specify the character used as a
		beginning of field delimiter. Whenever the field delimiter
		character is seen in the input text stream, the following
		character is interpreted as a field number.

	-f	Field file. The field file should contain a list of field
		names in an order corresponding to the field numbers used
		within the input stream. The first entry in the field file
		corresponding to field 0, the second to field 1,... The field
		name are taken as the first word on each line of the field
		file, the remainder of the line being user definable. Fields
		is ignored if the -x option is used, as the fields for the
		executable are defined at the time the executable is linked.

	-k	Enables stripping of the key field found in input document
		descriptors. Output descriptors will have a key consisting
		only of the matching query name. This switch is used to
		minimize the size of output descriptors in cases where
		there is no need to keep the key value.

	-l	If set, spotter will log initiation, termination, and document 
		processing to standard error. Spotter will also maintain a 
		statistics file corresponding to the object query library, with 		an extension of ".act". The first record in this file is used 
		to keep track of the number of documents which have been 
		searched, the time at which spotter started processing, and 
		the time at which the last document was searched (in seconds 
		since system 0 time). These three long word values are stored 
		in the first three long words of the file. Other records in 
		the file correspond to queries in the query object library, 
		with each entry consisting of three long word values, the 
		first of which is the number of matches for the query, the 
		second the start time of the query, and the third the index of 
		the query in the query executable, or -1 if the query was not 
		loaded into the profile executable. A terminating record is 
		also put into the activity file, with all values set to -1L. 

	-n	Specifies that document numbers are to be stripped from
		document descriptors which are output. This switch is used
		if an archive serves as input via i/o redirection, and the
		-a switch is not used. In this case, leaving the document
		number in the output document descriptors will imply that
		the descriptor is for a document in an archive, which will
		make the original file inaccesable via the TIPS hit
		processing utilities.

	-p	Profiling flag. Tells spotter to monitor a message queue 
		whose key is based on the query object library name. The 
		message queue is used to notify the spotter that query has
		been added, replaced, or deleted from the query object
		library. The spotter does not care what the value of the
		message key is for messages sent to the queue, assuming
		all value to be indications that the library has been
		updated. The spot "sendmsg" command may be used from the
		shell command line to send messages to the spotter's 
		message queue as in "/usr/spotsys/bin/sendmsg -m 1
		/usr/spotdata/streams/ap.qlb". On receiving a message, the 
		spotter will reload the searches from the query object library.

	-s	Specifies that the archive name specified with the -a switch 
		is to be substituted for the document name found in the 
		incoming document descriptor. The format of the name field 
		will be "archive_name{#nnn}" where nnn is the document
		number found in the input descriptor. This switch is used
		to minimize the size of output descriptors in cases where
		there is no need to keep the original document name in search
		result files.

	-z	Specifies that matching query names are not to be prefixed
		to the key field in output descriptors. Normally, the key
		field is modified so that it is prefixed by the matching
		query name, terminated by a "#" if the key field was used
		in the incoming descriptor.

Parameter:

	objlibname	The name of a TIPS query object library containing
			the object queries to use in the search. The qcompile
			command, and spot may both be used to create and add
			queries to a query object library. spot does this when
			you create and update "streams". 

Examples:

    /usr/spotsys/bin/receiver -i usr/spotdata/asynchin -l -o \
    /usr/spotdata/apin -r 15 2>>/usr/spotdata/log/receiver.ap | \
    /usr/spotsys/bin/arcload -l /usr/spotdata/archives/ap \
    2>>/usr/spotdata/log/arcload.ap | \
    /usr/spotsys/bin/spotter -l -p -a /usr/spotdata/archives/ap -s  \
    usr/spotdata/streams/ap.qlb 2>>/usr/spotdata/log/spotter.ap | \
    /usr/spotsys/bin/hitw -l -h 2>>/usr/spotdata/log/hitw.ap >/dev/null &

        This command is similiar to a command which would be used to initiate
        spot daemons to search a stream. The initial input directory for the
        stream is /usr/spotdata/asynchin, with incoming documents moved to
        /usr/spotdata/apin after being processed. Documents are also placed
        into the spot archive /usr/spotdata/archives/ap.sar. The searches
        against the stream are drawn from the spot query object library
        /usr/spotdata/streams/ap.qlb. Results from the searches will use the
        archive name and document number to refer to the documents, and the
        results for each search will be written to the result file corresponding
        to the name and location of the source query for the search. Each
        daemon will append log messages to a log file in directory
        /usr/spotdata/log.

    /usr/spotsys/bin/fprep * | \
    /usr/spotsys/bin/spotter $HOME/searches/myquery | \
    /usr/spotsys/bin/hitw $HOME/searches/myresults &

	This command will kick off an asynchronous search of all files in the
	current process default directory using the queries found in the
	spot object query library $HOME/searches/myquery.qlb, and writing
	search results to the file $HOME/searches/myresults.hit.

