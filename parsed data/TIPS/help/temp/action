				    action


Description:

    The action command is used to perform an action based on each document 
    descriptor which it reads from standard input. The action may be any 
    executable program or script. Selected portions of the descriptor may 
    be selected for use as arguements to the action program or script. By 
    default, the entire descriptor is fed to the command as a parameter. If 
    portions of the descriptor are specified, they are fed to the command as 
    parameters in the order in which they occur in the descriptor. The command 
    is not executed if a descriptor does not contain any one of the selected 
    components.
    
    The intended use of the action command is for it to process the piped 
    output of another spot utility command for purposes customized to meet 
    specific user needs. The ability to execute a user specified program or 
    script to process document descriptors opens up a realm of possibilites 
    when combined with the real-time document processing capabilities of the 
    SPOT system. For example, an action command might be set up to handle the 
    output of a hitw command being used to write search results. The script or 
    program invoked by the action command could be set up to spot documents 
    matching a particular search or group of searches, and perform some special 
    function, like operator notification of flash news items, or document 
    conversion and export to a foreign system.

Syntax:

    action {switches} {cmd} 

Switches:

    -a		Include archive name and document numbers as parameters.

    -k		Include key field as a parameter.

    -o		Include "original" document name as a parameter.

    -s		Include document size as a parameter.

Parameter:

    cmd		Command to be executed.

Example:

    action -o -k route 

	Given the input of:

		/usr/inpath/file1,255,/usr/routers/Chicago.qry
		/usr/inpath/file2,4000,/usr/routers/Atlanta.qry
		/usr/inpath/file3,2372,/usr/routers/NewYork.qry

       The following commands would be executed by action:

		route /usr/inpath/file1 /usr/routers/Chicago.qry
		route /usr/inpath/file2 /usr/routers/Atlanta.qry
		route /usr/inpath/file3 /usr/routers/NewYork.qry

