		Viewing and Manipulating Documents with Browse

    Browse provides a means for viewing and manipulating documents which
    have either been selected off the SPOT main screen, have been put into
    a result file by a search, or have been stored in a SPOT archive.
    Any type of document may be viewed. However, Browse does not currently
    support display of non-text documents (such as word processing documents,
    spreadsheets,...) in the format in which they are presented by their 
    native application. This limitation is addressed by the availability of 
    the File - Open option off the main menu, or the File - Open option off 
    the Browse command menu, both of which may be used to invoke the native 
    application for a document. In order for native applications to be invoked 
    by the File - Open options, the application must be defined based on the 
    file type. Directions for doing this are found under the help topic 
    "Customization".
    
    On entering Browse, either the first or the last document available for 
    display is presented in a scrollable display. In all view modes except
    "Stream Detail", the first document is initially displayed. The initial
    display of the last document while in "Stream Detail" view mode is a 
    convenience based on the assumption that users viewing stream detail
    will want to view the latest search results.

    In addition to displaying documents, the Browse display contains other
    information. In the upper right corner of the menu bar at the top of the
    display, a label is displayed. In all view modes except "Files", the 
    label will be the name of the result file or archive being browsed. In
    "Files" view mode, the label is "Browse". The highlighted display footer 
    at the bottom of the browse display is used to display information
    about the document currently in view. The left corner of the footer 
    displays the current document number and total number of documents
    available to be viewed. In the center of the footer is displayed the
    name of the current document. The right corner of the footer is used to
    display the line number within the current document of the line displayed 
    at the top of the Browse display, and the total number of lines contained
    in the document.

    A number of functions are available in Browse to facilitate the viewing
    and manipulation of the available documents. When viewing search results, 
    the words in the document which matched terms in the search are 
    highlighted. The cursor keys and other function keys may be used to move 
    around within the currently displayed document, or to display a different 
    document from the available documents. Browse also provides a command menu 
    which may be accessed by pressing the [Menu] function key or by pressing 
    one of the hot-keys available in the command menu. The options available 
    under the command menu are described under the help topics "Browse - Menu", 
    "Browse - File", "Browse - View", "Browse - Search" and "Browse - Action".
    
    While viewing a document, the following cursor and function keys are
    available.

        up arrow        Scrolls document display down one line.
        down arrow      Scrolls document display up one line.
        Page-Up         Pages document display back one screenfull.
        Page-Down       Pages document display forward one screenfull.
        Home            Displays first screenfull of document.
        End             Displays last screenfull of document.
	[Tab]		When viewing search results, advances to the next
			screen of the document containing a word which matched
			a query term.
	[Help]		Invokes the SPOT help utility.
	right arrow	Displays the next available document.
	left arrow	Displays the closest preceding document.
	[Delete]	When viewing results, files, or queries, removes 
			the current document from the result file. When 
			viewing archives, the [Delete] key is used to remove
			the current document from the archive. Since this
			is a rather drastic action, SPOT will ask for 
			confirmation of the archive document deletion by
			presenting a pop-up menu asking for confirmation.
        [Exit]          Exits Browse.

    An additional method for moving from document to document is to type in 
    a number within the range of available documents as shown in the lower 
    left corner of the display. The number being entered is displayed on the 
    bottom line of the Browse screen as it is entered. The [Enter] key is 
    pressed once the number is entered. If the entered number is preceeded 
    by a "-" or a "+", the document selected will be chosen relative to the 
    current document, otherwise the document chosen will be the one 
    corresponding to the entered number.

    One of the more useful features of SPOT is that result files may be
    browsed at the same time a search is adding results to the result file, 
    and archives may be browsed concurrently with documents being added to 
    them. In the left corner of the Browse display footer line, the current 
    document number and the total number of available document is displayed. 
    As new documents become available, the document total count will be 
    updated. This updating occurs a periodic intervals.  The interval between 
    updates is customizable. The procedure for doing this is described under 
    the help topic "Customization".

