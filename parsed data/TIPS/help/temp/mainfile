			    Main Menu - File Option

    The File option of the main menu is used to perform file maintenance tasks 
    on items in the current view mode, and also allows a temporary escape to 
    the shell command line. Selection of the File option results in the display
    of the File option pull-down menu from which the specific File function to 
    be performed may be selected. The options available in the file menu are 
    described below in the order of their occurence in the menu.

Open:

    The Open option is used to view/modify the current item in the display
    menu of the main screen. The exact action taken depends on the current
    view mode. The actions taken for each view mode are:

    Results:

	The query corresponding to the result file is made available for 
	editing with the query editor.

    Streams:

	The current view mode is changed to "Stream Detail", with information 
	about each individual search active against the stream displayed.

    Files:

	The editor defined for the file type is invoked. By default, this 
	editor will be the default UNIX editor, usually vi. SPOT allows 
	customization so that other applications may be invoked to edit the 
	file based on the file type. The procedure for enabling this feature 
	is described in the help topic "Customization".

    Archives:

	The SPOT browser is invoked, allowing the documents in the archive to
        be viewed and modified.

    Queries:

	The current query is made available for editing with the query editor.

    Stream Detail:

	The query corresponding to the current search name is made available 
	for editing with the query editor. On exiting the editor with the 
	[Exit] key, the query will be updated so that any newly arriving 
	documents will match against the latest version of the query.

New:

    The "New" option is used to create a new item of the type being manipulated
    by the current view mode. SPOT will be prompt for the name to give to the
    item. The item name which is entered should be a valid UNIX file name 
    (should be restricted to alphanumeric characters and "-" and "_"), and 
    should be no more than 9 characters in length. Once the name is entered, 
    SPOT will take the actions necessary for creating the item. For "Queries" 
    and "Stream Detail" view modes, the query editor will be invoked so that 
    a query may entered. In "Stream Detail" view mode, the query will be added 
    to the queries active against the stream, with the query and result files 
    placed in the current default search directory. This is one mechanism used 
    for starting a new query against a stream.

    Note that creating a new stream does not initiate processing of 
    incoming documents. This function simply creates a repository for
    storing queries which may at some time in the future search against
    a stream. The steps required for initiating processing of incoming
    documents by a stream are not described here, as they are beyond
    the scope of this help file. Also, the creation of new streams in
    the initial default stream directory is usually restricted to either 
    privileged users or the superuser, and an attempt by any other user 
    will result in an error message.

Browse:

    Browse is used to invoke the SPOT document browser against the current
    or selected items in the display menu of the main screen. In view modes
    "Results", "Archives", and "Stream Detail", the browser in automatically
    invoked against the current item. In "Queries" view mode, the document
    browser is invoked against the result file of the same name in the
    curent search directory. In "Files" view mode, the browser will 
    automatically be invoked against the current file if no other files
    are currently selected. If files are selected, a pull-down menu will
    appear allowing the choice of browsing either the current or selected
    items. The facility for browsing multiple selected files provides the
    user with the convenience of remaining in the browser while viewing
    a number of files, versus having to jump back and forth between the
    main screen and the browser.

Pick:

    Pick causes the removal of the "File" pull-down menu and the main menu,
    allowing the resumption of item manipulation in the item display menu of
    the main screen.

Copy:

    Copy is used for making duplicates of the current or selected items.
    If no items are currently selected, Copy will assume that a copy is
    to be made of the current item, and prompts for the item name. If
    items are selected in the item display, a pull-down menu is displayed 
    which allows a choice of copying the current or selected items. If 
    "Current" is chosen, SPOT will prompt for the name of the copy destination 
    file.  If "Selected" is chosen, "Copy" will put up a path selector menu 
    or stream selector menu so that the destination directory or stream may be 
    selected. When copying "Selected" items, the copied items will alway have 
    the same name as the originals, and must be copied to a destination that 
    is different from the directory or stream containing the originals so that 
    the originals are not overwritten.

    Depending on the current view mode, other special actions are taken by 
    copy. These are listed below for the view modes for which special actions 
    are taken during a copy.

    Results:

	When copying result files, a pull-down menu is presented with options 
	"Replace" and "Append". The "Replace" option is used to specify that 
	the destination file is to be replaced by the copy operation if it 
	already exists. This is equivalent to deleting the destination file 
	before performing the copy. If "Append" is selected, the entries 
	from the original result file are added to the end of an existing 
	destination file, or a new destination file is created if one does not 
	already exist.

    Streams:

	As noted for stream creation with the "New" option, copying a stream
	does not initiate processing of documents by the stream. This option
	may also be restricted to a privileged group of users or the superuser
	when copying within the initial default stream directory.

    Archives:

	When copying archives, a pull-down menu is presented with options 
	"Replace" and "Append". The "Replace" option is used to specify that
	the destination archive should be replaced by the copy operation if it
	already exists. This is equivalent to deleting the destination archive 
	before performing the copy. If "Append" is selected, documents from 
	the original archive are added to the end of an existing destination 
	archive, or a new destination archive is created if one does not 
	already exist.

Delete:
    
    The Delete option is used to delete the current or selected items. If no 
    items are currently selected, Delete will assume that the current item is 
    to be deleted. If items are selected in the item display, a pull-down menu 
    is displayed which allows a choice of deleting the current or selected 
    items. Depending on the current view mode, another pull-down menu may be 
    presented as described below, then a menu asking for confirmation that the 
    items are to be deleted is presented. At any time during the presentation 
    of menu's during this deletion process, the deletion may be canceled with 
    the [Exit] or [Quit] keys.
    
    As noted above, an additional pull-down menu is presented under some
    view modes. When in "Results", "Queries", or "Stream Detail", a pull-down
    menu is presented with the options "Query File(s)", and "Result File(s)". 
    This pull-down works as a multiple item selection menu, in which items are 
    selected and unselected by pressing the space bar (see help topic "Using 
    Menus" for more information on selecting items). The purpose of this menu 
    is to allow simultaneous deletion of both the query and result files for a 
    search. This saves the trouble of swapping view modes and repeating the 
    delete process to delete both files. Depending on the current view mode, 
    one of the options in this menu may be preselected. In "Queries" mode, 
    "Query File(s)" will be preselected, and in "Results" mode, "Result 
    File(s) will be preselected. 
    
    Note that in "Stream Detail" mode, deletion of the current item normally 
    only cancels the search against the current default stream, and does not 
    delete the query or result files. This is desirable as the same query and 
    result files may be used for searches against multiple streams, and 
    deletion of the query and result files is not appropriate.
    
Print:
    
    The Print option is used to send a copy of the current or selected items 
    to the system printer. The print option is only valid in "Results", 
    "Files", "Queries", and "Stream Detail" view modes. The printed file will 
    be sent to the default UNIX system printer, or to the printer specified 
    with the environment variable "SP_PRINTER" if SP_PRINTER is defined. 
    Additional environment variables may be set to control the margins and 
    page length of the printed file. The help topic "Customization" contains 
    detailed information about customization of printer output, 
    
Path:
    
    The Path option is used to change the directory or stream from which
    items in the current view mode are taken. The path option is most useful
    in "Files" and "Stream Detail" modes. It's use is discouraged for 
    inexperienced computer users while in other view modes due to the
    confusion that may result from it's use.
    
    In all view modes except "Stream Detail", the path option results in
    presentation of a path selector menu which may be used to change the
    default path for the view mode. In "Stream Detail" mode, a file selector
    menu is used to select from the streams available in the current default 
    stream directory. The usage of path and file selector menus is described
    in the help topic "Using Menus".
    
Shell:
    
    The Shell option results in temporary suspension of SPOT so that commands 
    may entered from the shell command line. The shell is exited and SPOT
    resumed by entering the exit command for the shell, usually "exit" or 
    "login" depending on the type of shell being used by the user.
    
Exit:
    
    The Exit option causes SPOT to terminate.

