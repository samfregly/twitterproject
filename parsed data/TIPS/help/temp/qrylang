			     SPOT Query Language

Example Queries:

	baseball, football, basketball, hockey, track \and field

		This query might be used to keep up with activities in the  
		major American spectator sports.

	Oil and price* and rising

		This query might be used to find documents relating to 
		rising oil prices.

	(terror* or bomb*) and %adaf* and ^date ;;8#*

		This query could be used to find documents which discuss 
		terrorist activities involving the Libyan leader which 
		occurred during the 1980s.

	not ^source AP and baseball and baltimore and orioles

		This query might be used to look for articles about the 
		baltimore orioles baseball team which did not come off the 
		AP news wire.

	^author ^byline (kissinger,james baker) and ^text ((Isreal, 
	Jordan,Lebanon,Iraq,Iran,Egypt,Arab,Palestin*,Kuwait) and oil)

		This query attempts to locate documents by Kissinger or 
		James Baker which mention various middle eastern countries 
		and also mention oil.


	any 2(bush and quail, reagon and bush, carter and mondale,
	nixon and ford, mondale and ferraro, nixon and agnew) and
	(election*, candidate*, poll*, president*)

		This query attempts to locate articles containing references
		to at least two U.S. president/vice-president election tickets.

	(gulf, oil) within 5 war

		This query attempts to locate articles about the Persion
		Gulf War of 1991 by searching for documents containing the
		words "gulf" or "oil" within 5 words of the word war.


			Query Language Description

    The SPOT query language provides the mechanism by which users 
    express their queries.  A query is used to specify the words that 
    are to be searched for.  These words, often referred to as query 
    terms, are linked together with boolean logic operators and 
    proximity operators, allowing precise control over the content of 
    matching documents.  The query language supports the following 
    search criteria:

	  o Parenthesizing of expresions so that the desired boolean 
	    evaluation occurs

	  o Variable length wild carding, both alphnumeric and 
	    numeric

	  o Single character wild cards, both alphanumeric and 
	    numeric

	  o Set wild cards, specifying a set of allowable characters 
	    at a particular point in a word

	  o Phrase searching

	  o Field searching

	  o Boolean operators: and, or, and not

	  o Proximity operators: within number_of_words, ahead 
	    number_of_words

	  o Threshhold expressions (n out of m terms)

Terminology:

    A query is composed of a collection of words and phrases which are 
    to be searched for, and interconnecting query logic. The words and 
    phrases of a query are referred to as query terms. The query logic 
    specifies the combination of term occurrences which when present 
    in a document, cause it to be a match for the query. The query 
    logic follows the rules of boolean logic in determining whether or 
    not a query has matched against a document.
    
    Terms may contain one or more words. A multiword term is known as 
    a phrase term. Each word in a term consists of a sequence of non-
    blank characters.
    
    Terms may contain special characters which match on a number of 
    different characters at the corresponding location in a term. 
    These special characters are referred to as wild card characters.
    
Quoting:
    
    Some characters have special meanings to SPOT, and must be quoted 
    if they are to be used within a term. A character is quoted when 
    it is preceded by a backslash, as in "\%". The special characters 
    which must be quoted are as follows:
    
    % * # @ & ! ~ ( ) / ? ; : ^ \ 
    
Case Insensitivity:
    
    Terms are case insensitive, i.e. a term will match on a word 
    regardless of whether or not the characters in the word are in 
    upper or lower case, or some combination of both. For example, the 
    term "AMERICA" would match the word "America".
    
Wild Cards:
    
    SPOT provides a substantial wild card search capability as an aid 
    in creating fuzzy terms which will match on words which are 
    spelled similarly. Terms containing wild cards may be used to 
    reduce the number of terms required in a query while providing the 
    added benefit of increased search accuracy.
    
    A single character alphanumeric wild card (SCAWC) matches on any 
    alphabetic or numeric character which appears at the same relative 
    positions in a word being searched. One or more SCAWCs may be 
    placed in a term at any point. The percent sign, "%", is used to 
    represent the SCAWC. The term "%at" will match on "cat" or "bat" 
    or "rat", but not on "that".
    
    The numeric single character numeric wild card (SCNWC) is very 
    similar to the SCAWC, except it will only match on numeric 
    characters. The number sign, "#", is used to represent the SCNWC. 
    The term "198#" will match on "1980" or "1989", but not on "198a".
    
    The alphanumeric variable length wild card (AVLWC) is used to match on 
    0 or more alphanumeric characters at any point in a word. Only one
    embedded AVLWC or NVLWC (described next) is allowed per term, where
    an embedded AVLWC is one which does not come at the end of a word.
    There is no limit on the number of AVLWC or NVLWC characters which
    may be used at the end of term words to match on any trailing characters.
    The term "IRAN*" will match on "Iran", or "Iranian", but not on "Iraq".
    The phrase "*dafi" would match of "Kadafi", or "Quadafi", or "Qadafi",
    or "dafi". Note that to speed up searches, the ALVLWC wild card described 
    below is recommended for situations in which the wild card is expected to 
    only match on a few characters. 
    
    The numeric variable length wild card (NSWC) is very similar to the AVLWC, 
    except it will match only on numeric characters. The at sign, "@", 
    is used to represent the NSWC. The term "PN1980@" will match on 
    "PN1980", or "PN1980543, but not on "PN18". The term "@91" would match on
    "1991", or "991", or "91". Note that to speed up searches, the NLVLWC wild 
    card described below is recommended for situations in which the wild card
    is expected to only match on a few characters. 
 
    The alphanumeric limited variable length wild card (ALVLWC) is used to 
    match on words which may or may not have a group of characters at 
    a particular point in a word. A series of consecutive ampersand 
    characters, "&", is used to represent the ALVLWC. The number of 
    consecutive ALVLWC characters determines the maximum number of 
    characters which may occur in a matching word. The term "&&play" 
    will match on "play", or "splay", or "replay", but not on 
    "display".
    
    The numeric limited variable length wild card (NLVLWC) is very similar to 
    the ALVLWC, except it will match only on numeric characters. The 
    exclamation point, "!", is used to represent the NVLWC. The term 
    "!!80" will match on "80", or "180", or "1980", but not on 
    "10180".
    
    The alphanumeric fixed length don't care wild card (AFLDC) is very 
    much like the ALVLWC, except that it matches only if a word does 
    not contain any matching characters, or matches for the entire 
    length to the AFLDC. The question mark, "?", is used to represent 
    the AFLDC. The term "??play" will match on "play", or "replay", 
    but not on "splay".
    
    The numeric fixed length don't care wild card (NFLDC) is very 
    similar to the AFLDC, except it will match only on numeric 
    characters. The semicolon, ";" is used to represent the NFLDC. The 
    term ";;80" will match on "80", or "1980", but not on "180".
    
    The set wild card (SETWC) is used to specify a set of matching 
    characters. The SETWC starts with a slash character, "/", and ends 
    at the next non-quoted slash. In between the slashes are listed 
    the set of characters. Within the set wild card, a sequence of 
    characters may be specified by separating the starting and ending 
    characters with a colon, ":". The term "198/0:589/" will match on 
    "1980, or "1985", or "1989", but not on "1986".
    
    SPOT restricts the use of the AVLWC, NVLWC, ALVLWC, NLVLWC, AFLDC, 
    and NFLDC wild cards to one per term.
    
Logic:
    
    Boolean operators are used to specify the logical relationship of 
    the terms in a query. The operators (in order of high precedence 
    to low) are: not, and, and or. Parenthesis may be used to force 
    whatever order of evaluation the user desires. For example, the 
    query:
    
    	not A and B or C
    
    evaluates as though parenthesized as "((not A) and B) or C". To 
    force an evaluation so that the query matches on documents not 
    containing "A", and which contain either "B" or "C"; the query 
    would be formulated as:
    
    	not A and (B or C)
    
Proximity:
    
    Proximity operators allow the specification that terms must be 
    within a specified number of words of each other.  With a 
    bidirectional proximity operator, the order of occurrence of the 
    terms does not matter, whereas with a directional proximity 
    operator, the order does matter.  The proximity operators takes 
    the format of either "within number" for bidirectional proximity, 
    or "ahead number" for directional proximity.  The proximity 
    operator is the highest precedence operator, evaluating before any 
    of the boolean operators.  For example, the query:
    
    missile* within 10 launch* or firing
    
    probably does not perform the intended search, as it evaluates as 
    though parenthesized "(missile* within 10 launch*) or firing".  
    The query should probably be formulated as:
    
    missile* within 10 (launch* or firing)

Threshhold Expressions:

    A threshhold expression is a search criteria which will match on n out of 
    m items in the thresshold expression. A threshhold expression may appear
    at any point in a query at which a term may appear, and the items within
    a threshhold expression may be any valid query terms or expressions. The
    syntax for a threshhold expression is:

	{any n} (item{,item,...})

    where "n" is a number representing the minimum number of items which
    must match in order for the threshhold expression to match. For example,
    the threshhold expression "any 2 (kennedy, johnson, nixon, ford, carter,
    reagon, bush)" would match on any document mentioning at least two of
    the listed American presidents. Note that if the threshold specifier
    "any n" is not present, a default threshhold of 1 is used.

Quoting of Operators:
    
    The words "not", "and", "or", "within", and "ahead" must be 
    preceded by a backslash, "\", if they are to be considered as 
    terms. For Example
    
    War \and Peace
    
    might be used to search for the book "War and Peace".
    
Fields:
    
    A field specifier is used to limit a term or parenthesized 
    expression to specific fields within a document. A field specifier 
    takes the format of ^field_name {^field_name...}. For example, to 
    find documents written by Kissinger, or whose subject is 
    Kissinger, the following query might be used :
    
    ^author ^subject Kissinger
    
    
Search Processing:
    
    During searching, the text being searched is broken into words, 
    which are then matched against the term(s) in the query or queries 
    being matched against the text. A word is started by a searchable 
    character, and ends when a delimiter character is found, or the 
    end of the document is reached. The delimiter characters which end 
    words are typically any non-alphanumeric characters, except in 
    cases where the non-alphanumeric character is a part of a term.

