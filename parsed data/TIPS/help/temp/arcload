				    arcload


Description:

    This filter is used to load a TIPS archive. It will read from standard 
    input the documents to be loaded, appending them to the specified archive 
    and also copying the data written to the archive to standard output.  All 
    error and status messages are written to standard error.
    
    The input documents must each be preceeded by a standard TIPS document 
    header which must at minimum contain a length field. When inserted into 
    the archive, the header is placed at the front of the document, and is 
    prefixed by the document number (in ASCII) followed by a "#".

Syntax:

    arcload {-l} archive_name

	-l	Enables logging of initiation, processing, and termination
		messages to standard error.

	-p mask	Set file proteciton mask for archive if it is created by 
		arcload. "mask" is an octal value.

Example:

     fprep /usr/spotsys/apweek10/ap* | \
	arcload /usr/spotdata/archives/apweek10.sar

	This command loads all the files prefixed with "ap" in the directory
	/usr/spotdata/apweek10 into the archive 
        /usr/spotdata/archives/apweek10.sar.

