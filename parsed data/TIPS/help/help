				Using Help
 
Function Keys:
 
    Due to differences in terminal types which are supported by Help, some
    function keys described below may have different labels or may even
    be invoked by control key combinations. The site specific help topic,
    "Function Keys" may be viewed to determine the function key assignments
    on this system.
 
Selecting a Topic:
 
    On entering help, a menu of available topics is presented. The cursor
    control keys may be used to choose a topic. Once a topic is selected,
    the [Enter] key may be pressed to view the detailed information about the 
    topic. While selecting a topic, the folowing function keys are available:
 
	up arrow	Moves topic selector up one item.
	down arrow	Moves topic selector down one item.
	Page-Up		Pages topic display back a page.
	Page-Down	Pages topic display forward a page.
	Home		Displays first page of topics.
	End		Displays last page of topics.
	[Enter]		Allows user to view detailed information about topic.
	[Exit]		Exits Help.
 
 
Viewing Topic Detail:
 
    When veiwing topic detail, the information about the current topic is 
    presented in a scrollable display. The function keys used for moving within
    the scrollable display are identical to those described for "Selecting a
    Topic". The [Menu] key specified in the help line at the bottom of the
    screen may be pressed to return to the Topic Selection screen.
 
    Other topics may also be selected for viewing without need of returning to 
    the Topic Selection screen. The left and right arrow keys are used to move
    sequentially from topic to topic. A number within the range of available 
    topic numbers as shown in the lower left corner of the display may also be
    entered. When this occurs, the number is displayed on the bottom line
    of the help display as it is entered. The [Enter] key is pressed once
    the number is entered. If the entered number is preceeded by a "-" or a
    "+", the topic selected will be chosen relative to the current topic,
    otherwise the topic chosen will be the one corresponding to the entered
    number.
 
