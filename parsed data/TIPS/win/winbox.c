/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_box						winbox.c

Function : Puts box around border of window.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = win_box(WINDOW *win);

  win		Window to be boxed.

Notes: 

Change log : 
000	13-MAR-9  AMF	Created.
******************************************************************************/

#if FIT_CURSES

#include <curses.h>
#include <memory.h>
#include <fitsydef.h>
#include <win.h>

#if FIT_ANSI
int win_box(WINDOW *win)
#else
int win_box(win)
  WINDOW *win;
#endif
{
#if !FIT_COHERENT
  box(win, 0, 0);
#else
  int winrows, wincols;

  /* Get window size */
  getmaxyx(win, winrows, wincols);
  wmove(win, 0, 0);
  waddch(win, WIN_UPPERLEFT);
  win_fill(win, 0, 1, 1, wincols-2, WIN_HORIZONTAL);
  wmove(win, 0, wincols-1);
  waddch(win, WIN_UPPERRIGHT);
  win_fill(win, 1, 0, winrows-2, 1, WIN_VERTICAL);
  win_fill(win, 1, wincols-1, winrows-2, 1, WIN_VERTICAL);
  wmove(win, winrows-1, 0);
  waddch(win, WIN_LOWERLEFT);
  win_fill(win, winrows-1, 1, 1, wincols-2, WIN_HORIZONTAL);
  wmove(win, winrows-1, wincols-1);
  waddch(win, WIN_LOWERRIGHT);
  return 1;
#endif
}
#endif
