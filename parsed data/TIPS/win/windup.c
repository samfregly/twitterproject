/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_dup						windup.c

Function : Duplicates contents of window, saveing in created save window.

Author : A. Fregly

Abstract : This  function may be used to create a copy of a window. This
	function is usefull for save/restore operations on windows.
	To restore a window, a curses "overwrite" is performed. Note that
	it is up to the caller to delete the returned window copy once
	finished with it by using the curses "delwin" function.

Calling Sequence : int stat = win_dup(WINDOW *srcwin, WINDOW *(*retwin));

  srcwin	Window to be duplicated.
  retwin	Returned copy of srcwin.
  stat		Returned status, 0 if an error occurred.

Notes: 

Change log : 
000	08-SEP-91  AMF	Created.
001	26-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fitsydef.h>
#include <win.h>

//extern int errno;

#if FIT_ANSI
int win_dup(WINDOW *srcwin, WINDOW *(*retwin))
#else
int win_dup(srcwin, retwin)
  WINDOW *srcwin, *(*retwin);
#endif
{
  int x, y, nrows, ncols;
  WINDOW *tempwin= (WINDOW *) NULL;

  if (srcwin) {
    getbegyx(srcwin, y, x);
    getmaxyx(srcwin, nrows, ncols);
    if ((tempwin = newwin(nrows, ncols, y, x))) {
      *retwin = tempwin;
      overwrite(srcwin, tempwin);
    }
    else {
      endwin();
      printf("\nwin_dup, error allocating new window\n");
      exit(errno);
    }
  }
  return (tempwin != (WINDOW *) NULL);
}
