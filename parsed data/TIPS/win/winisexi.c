/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_isexit					winisexi.c

Function : Determines if key is exit key for win_edit.

Author : A. Fregly

Abstract : 

Calling Sequence : exitflag = win_isexit(int key);

  key		Key to be checked to see if it is an exit key. Key will
		be checked against curses function keys and tips standard
		exit keys (enter, ctrlz, ctrld).
  exitflag	non-zero, then key is an exit key for win_edit.

Notes: 

Change log : 
000	08-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <win.h>

#if FIT_ANSI
int win_isexit(int key)
#else
int win_isexit(key)
  int key;
#endif
{
  return (key == '\n' || key == '\r' || key == 9 || key == 4 || key == 26 ||
	key == KEY_UP || key == KEY_DOWN || key == KEY_NPAGE ||
	key == KEY_PPAGE || key == KEY_ENTER || 
#ifdef KEY_HELP
	key == KEY_HELP ||
#endif
#ifdef KEY_BTAB
	key == KEY_BTAB ||
#endif
#ifdef KEY_CANCEL
	key == KEY_CANCEL ||
#endif 
#ifdef KEY_EXIT
	key == KEY_EXIT ||
#endif
#ifdef KEY_MARK
	key == KEY_MARK ||
#endif
#ifdef KEY_FIND
	key == KEY_FIND ||
#endif
#ifdef KEY_MOVE
	key == KEY_MOVE ||
#endif
#ifdef KEY_REPLACE
	key == KEY_REPLACE ||
#endif
#ifdef KEY_SAVE
	key == KEY_SAVE ||
#endif
#ifdef KEY_SCANCEL
	key == KEY_SCANCEL ||
#endif
#ifdef KEY_COPY
	key == KEY_COPY ||
#endif
#ifdef KEY_SCOPY
	key == KEY_SCOPY ||
#endif
#ifdef KEY_SELECT
	key == KEY_SELECT ||
#endif
#ifdef KEY_SFIND
	key == KEY_SFIND ||
#endif
#ifdef KEY_SEXIT
	key == KEY_SEXIT ||
#endif
#ifdef KEY_SHELP 
	key == KEY_SHELP ||
#endif
#ifdef KEY_SMOVE
	key == KEY_SMOVE ||
#endif
#ifdef KEY_SREPLACE
	key == KEY_SREPLACE ||
#endif
#ifdef KEY_SSAVE
	key == KEY_SSAVE ||
#endif
#ifdef KEY_SUNDO
	key == KEY_SUNDO ||
#endif
#ifdef KEY_UNDO
	key == KEY_UNDO ||
#endif
	0);
}
