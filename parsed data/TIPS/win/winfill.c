/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_fill						winfill.c

Function : Fills screen or portion of it with specified fill character.

Author : A. Fregly

Abstract : This function is used to fill a specified portion of a window
	with a specified fill character. Typically, this function is used
	for clearing the screen when it is desired that the current video
	attribute be applied to the cleared area.

Calling Sequence : int stat = win_fill(WINDOW *win, int starty, int startx, 
  int nlines, int ncols, char fill);

  scr		Window to be filled.
  starty	Start row for fill, first is 0.
  startx	Start column for fill, first is 0.
  nlines	Number of lines to be filled, a value of 0 indicates remaining
		lines in display.
  ncols		Number of columns to be filled, a value of 0 indicates
		remaining columns in display.
  fill		Fill character to be used.

Notes: 

Change log : 
000	25-AUG-91  AMF	Created.
001	13-SEP-91  AMF	Put in work-around for curses bug which prevents 
			adding text into the last position of a window.
002	11-SEP-93  AMF	Put in conditional debug.
******************************************************************************/

#include <curses.h>
#include <memory.h>
#include <errno.h>
#include <fitsydef.h>
#include <win.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif

//extern char *sys_errlist[];
//extern int errno;

#if FIT_ANSI
int win_fill(WINDOW *win, int starty, int startx, int nlines, int ncols, 
    int fill)
#else
int win_fill(win, starty, startx, nlines, ncols, fill)
  WINDOW *win;
  int starty, startx, nlines, ncols, fill;
#endif
{
  int erow, currow, winrows, wincols, j;

#if FIT_COHERENT
  if (!fill) fill = ' ';
  fill |= (unsigned) win->_attrs;
#endif

  /* Get window size */
  getmaxyx(win, winrows, wincols);

  /* Make sure valid window area was specified */
  if (starty >= winrows || starty < 0 || startx >= wincols || startx < 0 ||
      nlines < 0 || ncols < 0) {
#if FIT_COHERENT
  printf("\nwin_fill, invalid parameter\n");
  printf("  starty: %d, startx: %d, nlines: %d, ncols: %d, fill: %d\n",
	starty, startx, nlines, ncols, fill);
  printf("  winrows: %d, wincols: %d\n", winrows, wincols);
  exit();
#endif
    return 0;
  }

  /* Set area to be filled. Note that 0 means "rest" */
  if (!nlines || nlines > winrows - starty)
    nlines = winrows - starty;

  if (!ncols || ncols > wincols - startx)
    ncols = wincols - startx;

#if FIT_COHERENT
  if (nlines <= 0 || ncols <= 0) {
    printf("\nwin_fill, invalid fill area specified\n");
    printf("  starty: %d, startx: %d, nlines: %d, ncols: %d, fill: %d\n",
	starty, startx, nlines, ncols, fill);
    printf("  winrows: %d, wincols: %d\n", winrows, wincols);
  }
#endif

  /* Do the fill into the window */
  for (currow = starty, erow = starty + nlines; currow < erow; ++currow) {
    if (wmove(win, currow, startx) == ERR) {
      break;
    }
    for (j=0; j < ncols; ++j)
      if (waddch(win, fill) == ERR) {
	goto DONE;
      }
  }

  /* Return 0 if prematurely terminated due to output error */
DONE:
  if (currow >= erow || (currow == erow -1 && j == ncols-1))
    return 1;
  else {
#if FIT_COHERENT
  printf("\nwin_fill, premature termination\n");
  printf("  starty: %d, startx: %d, nlines: %d, ncols: %d, fill: %d\n",
	starty, startx, nlines, ncols, fill);
  printf("  winrows: %d, wincols: %d, currow: %d, erow: %d, j: %d\n",
	winrows, wincols, currow, erow, j);
  exit();
#endif
    return 0;
  }
}
