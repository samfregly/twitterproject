/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/*******************************************************************************

  Module: win_edit						EDIT.C
  Function: Field edit input routine
  Author: A. Fregly
  Description:
    Allows user to enter/edit data in a fixed length field on the screen.
    Supports both insert and overtype mode text entry.  Editing keys supported
    include:

	Insert/Overtype mode toggle
	Back word
	Delete current character
	End of field
	Forward a word
	Beginning of field
	Delete from cursor to end of field
	Redraw field
	Delete from beginning of field to cursor
	Left one character
	Right one character
	Delete previous character

  The following keys will cause this routine to terminate editing of the field.
	up arrow
	down arrow
	return
	tab
	line feed
	page up
	page down
	F1..F10

  Parameters:
	ss=win_edit(win, buf,bufl,imode,dmode,emode,row,col,off,key)
	win	*WINDOW Window in which to perform edit.
	buf	*char	Buffer containing data to be edited
	bufl	int	Length of data in buffer to be edited
	imode	*int	Insert mode toggle
	dmode	int	Draw field before initiating edit flag
	emode	int	Exit field flag, .true. allow user to type out of
			field or use arrows to exit field.  KEY will contain
			value of last key entered.
	row	int	Row in window
	col	int	Start COL for field in window.
	off	*int	Offset at which editing starts/ends.  1 is start.  If
			user exits by typing or arrowing out of field because
			EMODE is enabled, OFF will have a value of either 0 or
			BUFLEN+1.
	key	*int	Last key entered by user
	ss	*int	Returned status code

  Change Log:
000  11-JAN-88  AMF	Created.
001  30-MAR-93  AMF	Make it work under Coherent.
002  11-SEP-93  AMF	Cosmetic mods. Work under HPUX. Support WIN_DEBUG.
*******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_CURSES
#include <curses.h>
#endif
#include <win.h>

#if FIT_TURBOC || FIT_VMS || FIT_BSD
#include <stdlib.h>
#endif
#if FIT_BSD
#include <string.h>
#endif

#if FIT_ANSI
int win_edit(WINDOW *win, char *inbuf, int bufl, int *imode, int dmode, 
  int emode, int row, int col, int *off, int *key)
#else
int win_edit(win, inbuf, bufl, imode, dmode, emode, row, col, off, key)
  WINDOW *win;
  char *inbuf;
  int bufl, *imode, dmode, emode, row, col, *off, *key;
#endif
{
  int ss, oldoff, e, cpos, ispace, ecol;
  char *buf;
  int prevcurset;

  static int firsttime=1;

  if (firsttime) {
    firsttime = 0;
#if WIN_DEBUG
    fprintf(fit_debug_handle,"winedit, initial entry...\n");
    fprintf(fit_debug_handle,"  KEY_RIGHT = %d\n", KEY_RIGHT);
    fprintf(fit_debug_handle, "  KEY_LEFT = %d\n", KEY_LEFT);
    fflush(fit_debug_handle);
#endif
  }

#if WIN_DEBUG
  fprintf(fit_debug_handle,"winedit entered\n  bufl: %d, off: %d, row: %d, col: %d",
	bufl, *off, row, col);
  fprintf(fit_debug_handle,"  imode: %d, dmode: %d\n", *imode, dmode);
#endif

  /*  Redraw if DMODE set */
  buf = (char*) malloc(bufl+1); 
  *buf=0;
  fit_sassign(buf,0,bufl-1,inbuf,0,max(0,strlen(inbuf) - 1));
  *(buf+bufl) = 0;
#if WIN_DEBUG
  fprintf(fit_debug_handle,"  buf: %s\n", buf);
  fflush(fit_debug_handle);
#endif
  ss = 1;
  if (dmode)
    mvwaddstr(win, row, col, buf);
  ecol = col + bufl - 1;
  *off = (int) min( (int) max(0,*off), bufl);
  cpos = (int) min( (int) max(col+*off, 0), ecol+1);
  wmove(win, row, cpos);
  wrefresh(win);

  prevcurset=curs_set(2);
  while (1) {
    *off = (cpos - col);
    if (cpos > ecol) wmove(win, row, ecol);
    *key = wgetch(win);
#if WIN_DEBUG
    fprintf(fit_debug_handle, " in key: %d, %c\n", *key, *key);
    fflush(fit_debug_handle);
#endif
    if (*key == ERR) goto done;
    if (*key >= ' ' && *key <= '~') {
      if (cpos <= ecol) {
	waddch(win, *key);
	if (cpos == ecol) wmove(win, row, cpos);
	if (*imode && cpos < ecol) {
	  win_lastch(buf, &e);
	  if (e >= *off) {
	    fit_sassign(buf,*off+1,(int) min(e+1,bufl-1), buf,*off,e);
	    waddstr(win, buf + *off + 1);
	    wmove(win, row, cpos+1);
	  }
	}
	*(buf+*off) = (char) *key;
	cpos += 1;
      }
      else {
	if (emode) goto done;
	beep();
      }
    }

    else if (*key == KEY_LEFT
#ifdef KEY_SLEFT
        || *key == KEY_SLEFT
#endif
    ) {
      if (cpos > col) {
	if (cpos <= ecol) {
	  cpos=cpos-1;
	  wmove(win, row, cpos);
	}
	else {
	  wmove(win, row, ecol);
	  cpos=ecol;
	}
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	beep();
    }

    else if (*key == KEY_RIGHT
#ifdef KEY_SRIGHT
        || *key == KEY_SRIGHT
#endif
    ) { 
      if (cpos <= ecol) {
	cpos=cpos+1;
	wmove(win, row, cpos);
      }
      else {
	if (emode) goto done;
	beep();
      }
    }

    else if (*key == KEY_DC
#ifdef KEY_SDC
        || *key == KEY_SDC
#endif
    ) {
      if (cpos <= ecol) {
	win_lastch(buf,&e);
	if (*off < e) 
	  fit_sassign(buf,*off,e,buf,*off+1,e);
	else
	  *(buf+e) = ' ';
	if (e >= *off) {
	  waddstr(win, buf + *off);
	  wmove(win, row, cpos);
	}
      }
      else {
	if (emode) goto done;
	beep();
      }
    }

    else if (*key == '\010' || *key == KEY_BACKSPACE) { 
      if (cpos > col) {
	win_lastch(buf,&e);
	if (cpos <= ecol) {
	  cpos=cpos-1;
	  wmove(win, row, cpos);
	}
	else {
	  wmove(win, row, ecol);
	  cpos=ecol;
	}
	if (*off <= e+1) {
	  if (*off <= e && *off < bufl)
	    fit_sassign(buf,*off-1,e,buf,*off,e);
	  else
	    *(buf+*off-1)=' ';
	  waddstr(win, buf + *off - 1);
	  wmove(win, row, cpos);
	}
      }
      else {
	if (emode) {
	  *off= -1;
	  goto done;
	}
	beep();
      }
    }

    else if (*key == KEY_HOME
#ifdef KEY_SHOME
        || *key == KEY_SHOME
#endif
    ) {
      if (cpos > col) {
	cpos = col;
	wmove(win, row, col);
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else {
	beep();
      }
    }

#ifdef KEY_END
    else if (*key == KEY_END
#ifdef KEY_SEND
      || *key == KEY_SEND
#endif
#else
    else if (*key == ('e' - 'a' + 1)
#endif
    ) {
      *off=bufl;
      ispace=1;
      while (*off > 0 && ispace) {
	if (ispace = (*(buf+(*off)-1) == ' '))
	  *off -= 1;
      }
      cpos = col + *off;
      wmove(win, row, cpos);
    }
#ifdef KEY_NEXT
    else if (*key == KEY_NEXT
#ifdef KEY_SNEXT
        || *key == KEY_SNEXT
#endif
#else
    else if (*key == ('n' - 'a' + 1)
#endif
    ) {
      ispace=1;
      if (*off < bufl)
        ispace= (*(buf+*off) == ' ');
      while (*off < bufl && !ispace ) {
	*off += 1;
	if (*off < bufl) ispace= *(buf+*off) == ' ';
      }
      while (*off < bufl && ispace) {
	*off += 1;
	if (*off < bufl) ispace = *(buf+*off) == ' ';
      }

      if (*off >= bufl) {
	if (emode) goto done;
	*off = bufl - 1;
	if (ecol - cpos == 0) beep();
      }
      cpos = col + *off;
      wmove(win, row, cpos);
    }
#ifdef KEY_PREVIOUS
    else if (*key == KEY_PREVIOUS
#ifdef KEY_SPREVIOUS
        || *key == KEY_SPREVIOUS
#endif
#else
    else if (*key == ('b' - 'a' + 1)
#endif
    ) {
      oldoff = *off;
      if (*off > 0)
	while (*off > 0 && *(buf+max(0,*off-1)) != ' ')
	  *off -= 1;
      if (*off > 0)
	while (*off > 0 && *(buf+max(*off-1,0)) == ' ')
	  *off -= 1;

      if (oldoff != *off) {
	cpos = col + *off;
	wmove(win, row, cpos);
	if (*(buf+*off) != ' ')
	  beep();
      }
      else if (emode) {
	*off= -1;
	goto done;
      }
      else
	beep();
    }

    else if ((*key == KEY_IC
#ifdef KEY_SIC
        || *key  == KEY_SIC
#endif
        ) && !*imode)
      *imode= 1;

    else if ((*key == KEY_EIC || *key == KEY_IC
#ifdef KEY_SIC
        || *key == KEY_SIC
#endif
        ) && *imode)
      *imode = 0;

    else if (*key == KEY_DL
#ifdef KEY_SDL
        || *key == KEY_SDL
#endif
    ) {
      if (*off > 0) {
	win_lastch(buf,&e);
	if (*off < bufl)
	  fit_sassign(buf,0,e,buf,*off,bufl-1);
	else
	  fit_sassign(buf,0,bufl-1," ",0,0);
	mvwaddstr(win, row, col, buf);
	wmove(win, row, col);
	cpos = col;
      }
      else if (emode) {
	*off = -1;
	goto done;
      }
      else {
	beep();
      }
    }
    else if (*key == KEY_EOL
#ifdef KEY_SEOL
        || *key == KEY_SEOL
#endif
    ) {
      if (*off < bufl) {
	win_lastch(buf,&e);
	if (e >= *off) {
	  fit_sassign(buf,*off,e," ",0,0);
	  waddstr(win, buf + *off);
	  wmove(win, row, cpos);
	}
      }
      else {
	if (emode) goto done;
	beep();
      }
    }

#ifdef KEY_REFRESH
    else if (*key == KEY_REFRESH) {
#else
    else if (*key == ('w' - 'a' + 1)) {
#endif
      endwin();
      wrefresh(win);
    }

    else
      break;
#if FIT_COHERENT || FIT_HP || FIT_SUN
    wrefresh(win);
#endif
  }
done:
  mvwaddstr(win, row, col, buf);
  if (cpos >= col) {
    if (cpos <= ecol)
      wmove(win, row, cpos);
    else
      wmove(win, row, ecol);
  }
  else
    wmove(win, row, col);
  wrefresh(win);
  fit_sassign(inbuf,0,bufl,buf,0,bufl);
  free(buf);
  curs_set(prevcurset);
  return (*key != ERR);
}
