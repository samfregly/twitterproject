/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_lastch						winlastch.c 

Function : Finds last non-blank or non-null character in line. 

Author : A. Fregly 

Abstract :  

Calling Sequence : win_lastch(char *buf, int *e);

  buf		Buffer to be scanned for last character.
  e		Returned last non-blank/non-null character in buf. 

Notes: 

Change log : 
000	?-1989  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <win.h>

#if FIT_ANSI
void win_lastch(char *buf,int *e)
#else
void win_lastch(buf,e)
  char *buf;
  int *e;
#endif
{
  char *eptr, *lastchptr;
  for (eptr = buf, lastchptr = buf; *eptr; ++eptr)
    if (*eptr != ' ')
      lastchptr = eptr; 
  *e = lastchptr - buf;
}
