/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win_center					wincente.c

Function : Centers text in specified row of specified window.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = win_center(WINDOW *win, int row, char *str);

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Cosmetic mods and add debug.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <win.h>

#if FIT_ANSI
int win_center(WINDOW *win, int row, char *str)
#else
int win_center(win, row, str)
  WINDOW *win;
  int row;
  char *str;
#endif
{
  int max_y, max_x, stat;
  char *outline=NULL;

  stat = 0;
  if (getmaxyx(win, max_y, max_x) == ERR) {
    goto DONE;
  }
  if (row < 0 || row >= max_y) {
    goto DONE;
  }

  outline = (char *) malloc(max_x+1); 
  if (!outline) {
    goto DONE;
  }
  if (!win_fill(win, row, 0, 0, 0, ' ')) {
    goto DONE;
  }
  if (str) {
    strncpy(outline, str, max_x);
    outline[max_x] = 0;
    if (wmove(win, row, (max_x - strlen(outline)) / 2) == ERR) {
      goto DONE;
    }
    if (waddstr(win, outline) == ERR) {
      goto DONE;
    }
  }
  stat = 1;

DONE:
  if (outline) free(outline);
  return stat;
}
