/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_mapflds					mapflds.c

Function : Maps application fields to fields loaded from field file.

Author : A. Fregly

Abstract : This routine is used to map the field names known to a document
	filter to those defined in a field file so that the filter may use
	the correct field numbers when inserting field delimiters.

Calling Sequence : tips_mapflds(char *appfields[], int nappfields,
	char *loadedfields[], int nloaded, int fieldnums[]);

  appfields	List of field names that the application wants mapped.
  nappfields	Number of entries in appfields.
  loadedfields	List of field names loaded from a field file to which 
		appfields are to be mapped.
  nloaded	Number of entries in loadedfields.
  fieldnums	Returned array of field numbers. The user must allocate at
		least nappfields entry space prior to calling tips_mapfields.

Notes : 

Change log : 
000	19-MAY-91  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_ANSI
void tips_mapflds(char *appfields[], int nappfields,
	char *loadedfields[], int nloaded, int fieldnums[])
#else
void tips_mapflds(appfields, nappfields, loadedfields, nloaded, fieldnums)
  char *appfields[];
  int nappfields;
  char *loadedfields[];
  int nloaded, fieldnums[];
#endif
{

  int i, j;

  for (i=0; i < nappfields; ++i) {
    for (j = 0; j < nloaded; ++j)
      if (!strcmp(appfields[i],loadedfields[j]))
	break;
    if (j < nloaded)
      fieldnums[i] = j;
    else
      fieldnums[i] = -1;
  }
}
