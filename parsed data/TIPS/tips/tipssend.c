/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_sendmsg					tipssend.c

Function : Sends a message and optionally waits for reply.

Author : A. Fregly

Abstract : This function is used to send messages between TIPS programs
	and TIPS daemons. The daemons respond to messages with message
	types <= to TIPS_MAXMSGNUM. If a return message is required, the
	deamon will send it back to the originator. Messages of length
	greater than TIPS_MAXMSGDATA are currently unsupported.

Calling Sequence : int tips_sendmsg(tips_msgqueue msgqid, long msgtype, 
    char *msg, unsigned msglen, int timeout, char *retmsg, unsigned maxretlen,
    unsigned *retlen, long *retmsgtype, int *receiverpid);

  msgqid	Message queue identifier returned by tips_newmsgqueue or
  		tips_attachmsgqueue.
  msgtype	Message type identifier. For messages sent to daemons, this
		should be a value between 1 and TIPS_MAXMSGNUM. Return message
		created by daemons will use a message type equal to the
		originators pid + TIPS_MAXMSGNUM + 1.
  msg		Message buffer to be send.
  msglen	Length of message being sent.
  timeout	non-zero, then tips_sendmsg will wait up to timeout seconds
		for a reply (or indefinitely if timeout is less than 0),
                putting the returned reply into retmsg;
  retmsg	Buffer to hold return message.
  maxretlen	Maximum length of returned message.
  retlen	Length of returned message.
  retmsgtype	Returned message type.
  receiverpid	PID of receiving process if return message was received.

Notes: 

Change log : 
000	16-OCT-91  AMF	Created.
001	19-JUL-92  AMF	DOS support. Use tips_msgqueue and tips_s_msghdr
			structure.
002	12-DEC-92  AMF	Changed parameters. COHERENT support.
003	06-AUG-03  AMF  Temp change to make this a dummy call under Cygwin
******************************************************************************/

#include <stdio.h>
#include <time.h>
#include <fitsydef.h>
#if !FIT_CYGWIN && (FIT_UNIX || FIT_COHERENT)
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#endif
#if FIT_CYGWIN
#include <cygwin/types.h>
#include <cygwin/ipc.h>
#include <cygwin/msg.h>
#endif
#if FIT_MSC || FIT_BSD
#if FIT_MSC
#include <process.h>
#endif
#include <string.h>
#endif
#include <signal.h>
#include <tips.h>

#if FIT_ANSI
int tips_sendmsg(tips_msgqueue msgqid, long msgtype, char *msg,
  unsigned msglen, int timeout, char *retmsg, unsigned maxretlen,
  unsigned *retlen, long *retmsgtype, int *receiverpid)
#else
int tips_sendmsg(msgqid, msgtype, msg, msglen, timeout, retmsg, maxretlen,
    retlen, retmsgtype, receiverpid)
  tips_msgqueue msgqid;
  long msgtype;
  char *msg;
  unsigned msglen;
  int timeout;
  char *retmsg;
  unsigned maxretlen, *retlen;
  long *retmsgtype;
  int *receiverpid;
#endif
{

  int retstat=1;
#if FIT_UNIX || FIT_COHERENT
  struct tips_s_msg outmsg, trashmsg;
  int inmsgtype, senderpid;
  unsigned trashlen;

  if (msglen < 0 || msglen > TIPS_MAXMSGDATA) return 0;
  senderpid = getpid();

  if (timeout) {
    /* Clean out any trash messages which may be left queued as returns
       from prior communications */
    inmsgtype = senderpid + TIPS_MAXMSGNUM + 1;

    while (tips_rcvmsg(msgqid, inmsgtype, 0, (char *) &trashmsg,
	TIPS_MAXMSGDATA, &trashlen, retmsgtype, receiverpid))
      ;
  }

  outmsg.hdr.msgtype = msgtype;
  outmsg.hdr.datalen = msglen;
  outmsg.hdr.pid = senderpid;
  if (msglen) memcpy(outmsg.data, msg, msglen);

  /* Send the outgoing message */
  if (retstat = !msgsnd(msgqid, (struct msgbuf *) &outmsg, msglen + sizeof(outmsg.hdr), 0))
    if (timeout)
      /* Read the return message. */
      retstat = tips_rcvmsg(msgqid, inmsgtype,  timeout, retmsg, maxretlen, 
	retlen, retmsgtype, receiverpid);

#else
  struct tips_s_msg outmsg, trashmsg;
  int inmsgtype, senderpid;
  unsigned trashlen;

  if (msglen < 0 || msglen > TIPS_MAXMSGDATA) return 0;
  senderpid = getpid();

  if (timeout) {
    /* Clean out any trash messages which may be left queued as returns
       from prior communications */
    inmsgtype = senderpid + TIPS_MAXMSGNUM + 1;

    while (tips_rcvmsg(msgqid, inmsgtype, 0, (char *) &trashmsg,
	   TIPS_MAXMSGDATA, &trashlen, retmsgtype, receiverpid))
      ;
  }

  /* Set new highest sequence number */
  if (!drct_wlock(msgqid, 0L, 1L)) return 0;
  if (!drct_rdrec(msgqid, 0L, (char *) &trashmsg, sizeof(trashmsg), &trashlen));
      return 0;
  trashmsg.hdr.seqnum += 1;
  trashmsg.hdr.datalen = 0;
  if (!drct_rewrite(msgqid, (char*) &trashmsg.hdr, (int) sizeof(trashmsg.hdr), 
	  (long) sizeof(trashmsg.hdr))) return 0;
  if (!drct_unlock(msgqid)) return 0;

  outmsg.hdr.msgtype = msgtype;
  outmsg.hdr.datalen = msglen;
  outmsg.hdr.pid = senderpid;
  outmsg.hdr.seqnum = trashmsg.hdr.seqnum;
  if (msglen) memcpy(outmsg.data, msg, msglen);

  /* Send the outgoing message */
  if (retstat = drct_overwrite(msgqid, (char *) &outmsg, (int) (sizeof(trashmsg.hdr) + msglen),
	(int)  (sizeof(trashmsg.hdr) + msglen))) {
    if (timeout)
      /* Read the return message. */
      retstat = tips_rcvmsg(msgqid, inmsgtype,  timeout, retmsg, maxretlen, 
		retlen, retmsgtype, receiverpid);
  }
#endif
  return retstat;
}
