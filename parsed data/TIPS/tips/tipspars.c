/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_parsedsc					parsedsc.c

Function : Parses TIPS standard document descriptor.

Author : A. Fregly

Abstract : This function will parse a TIPS standard document descriptor
	into it's component pieces. The descriptor takes the format of

		identifier,length{,key}<lf>

	Where identifier is an ASCII identifier for the document (file name,
	index,...), length is an ASCII numeric specifying the length of
	the document in bytes, and key is an optional user definable value
	which may be used for a variety of purposes (document type specifier,
	security classification,...).

Calling Sequence : ss = tips_parsedsc(char *descriptor, char *docname,
	long *doclen, char *key);

	descriptor	Character string containing descriptor to be
			parsed.
	docname		Character string buffer in which document name
			is to be placed. Up to TIPS_DESCRIPTORMAX characters
			may be placed in docname, excluding the trailing
			0 byte.
	doclen		Returned document length.
	key		Returned key. Up to TIPS_DESCRIPTORMAX characters
			may be placed in key, excluding the trailing 0 byte.

Notes : 

Change log : 
000  14-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_ANSI
int tips_parsedsc(char *descriptor, char *docname, long *doclen, char *key)
#else
int tips_parsedsc(descriptor, docname, doclen, key)
  char *descriptor, *docname;
  long *doclen;
  char *key;
#endif
{

  char *cptr;
  int len;

  *docname = 0;
  *key = 0;
  *doclen = 0;
  cptr = strchr(descriptor,',');
  if (cptr == NULL) cptr = strchr(descriptor,'\0');

  len = cptr - descriptor;
  if (len) {
    if (len > TIPS_DESCRIPTORMAX) len = TIPS_DESCRIPTORMAX;
    strncpy(docname,descriptor,len);
    docname[len] = 0;
    if (len && docname[len-1] == '\n') docname[len-1] = 0;
  }

  if (*cptr) {
    descriptor = cptr+1;
    sscanf(descriptor,"%ld",doclen);

    cptr = strchr(descriptor,',');
    if (cptr != NULL) {
      descriptor = cptr + 1;
      len = strlen(descriptor);
      if (len > TIPS_DESCRIPTORMAX) len = TIPS_DESCRIPTORMAX;
      if (len) {
        strncpy(key,descriptor,len);
        key[len] = 0;
        if (key[len-1] == '\n') key[len-1] = 0;
      }
    }
  }
  return 1;
}
