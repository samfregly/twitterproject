/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_help						tipshelp.c

Function : This function is used to invoke the help program with help for
	the specfied program.

Author : A. Fregly

Abstract : This program will attempt to locate the help file for the specified
	program, and if it is present, it will spawn the spot help program
	for that help file. By default, the program will attempt to locate
	the help file by looking in $SPOT_ROOT/help for a result file with
	the same name as the program. If this proves unsuccessfull, a search
	is made for the help file in the directory containing the program
	(if that is present in the supplied program name), otherwise a search
	is made in the current directory.

Calling Sequence : int retstat = tips_help(progname);

  progname	Name of program for which help is desired.
  retstat	Returned status, a value of 0 indicates an error occurred.

Notes: 

  if the caller is using curses, they should call endwin() prior to calling
  this function, and use wrefresh to redraw the screen on return from this
  function.

Change log : 
000	12-NOV-91  AMF	Created.
001	26-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if !FIT_DOS
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys\types.h>
#include <sys\stat.h>
#include <process.h>
#if FIT_ZORTECH || FIT_MSC
#include <direct.h>
#endif
#if FIT_TURBOC
#include <dir.h>
#endif
#endif
#include <errno.h>
#include <tips.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_BSD
#include <unistd.h>
#endif

#if FIT_COHERENT || FIT_SUN
char *getcwd();
#endif
//extern int errno;

static int firsttime=1;
static char spotroot[FIT_FULLFSPECLEN+1];

#if FIT_ANSI
int tips_help(char *progname)
#else
int tips_help(progname)
  char *progname;
#endif
{

  char name[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  char helpfile[FIT_FULLFSPECLEN+1], *curdir;
  char *varptr;
  struct stat statbuf;
  int len, pid, exitpid, exitstat;
#if FIT_DOS
#define SEP "\\"
#endif
#if FIT_UNIX
#define SEP "/"
#endif
#if FIT_VMS
#define SEP "]"
#endif

  if (firsttime) {
    /* Get spot root directory */
    firsttime = 0;
    if ((varptr = getenv("SPOT_ROOT"))) {
      fit_abspath(varptr, (curdir = (char *) getcwd(NULL, FIT_FULLFSPECLEN+1)),
        spotroot);
      if (curdir) free(curdir);
#if FIT_VMS
      len = strlen(spotroot);
      if (len) spotroot[len-1] = 0;
      strcat(spotroot,".");
#endif
      strcat(spotroot,"help");
      strcat(spotroot, SEP);
    }
    else
      spotroot[0] = 0;
  }

  if (spotroot[0]) {
    /* Look for help file in help directory */
    fit_prsfspec(progname, dummyspec, dummyspec, dummyspec, name, dummyspec, 
      dummyspec);
    fit_bldfspec(NULL, NULL, spotroot, name, ".hit", NULL, helpfile);
    if (stat(helpfile, &statbuf)) helpfile[0] = 0;
  }
  else
    helpfile[0] = 0;

  if (!helpfile[0]) {
    /* Look for help file in program directory */
    fit_setext(progname, ".hit", helpfile);
    if (stat(helpfile, &statbuf)) helpfile[0] = 0;
  }

  /* Return if help file could not be found */
  if (!helpfile[0]) {
    return 0;
  }

  /* Spawn the help command */
#if FIT_UNIX
  if (!(pid = fork())) {
    execlp("sphelp","sphelp", helpfile, (char *) 0);
    exit(errno);
  }
  else if (pid > 0)
    while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
      ;

  return (exitpid == pid && pid > 0);
#endif

#if FIT_DOS
#if FIT_ZORTECH
  return !spawnlp(0, "sphelp", "sphelp", helpfile, (char *) 0);
#endif
#if FIT_TURBOC
  return !spawnlp(P_WAIT, "sphelp", "sphelp", helpfile, (char *) 0);
#endif
#endif

}
