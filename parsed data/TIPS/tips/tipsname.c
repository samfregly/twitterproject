/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_nameparse					tipsname.c

Function : Parses name field of a TIPS document descriptor.

Author : A. Fregly

Abstract : This function will parse a TIPS document name field, breaking
	it down into the three currently recognized components: archive
	name, archive record number, and original document name. All of
	these fields are optional, though a descriptor must contain at
	minimum an archive name and record number, or an original document
	name in order to be useful. Note however that in TIPS archives, the
	document descriptors are typically contain only the document number
	and original document name fields, as the archive name is implicitly
	known.

	The name field is expected to be formatted in the following manner

		{archive_name#}{document_number}{{#}original_document_name}

	The "#" marks serve as field separators, and do not have to be
	present if there is no following field.

Calling Sequence : int stat = tips_nameparse(char *namefld,
   char arcname[FIT_FULLFSPECLEN+1], docnum_s[FIT_FULLFSPECLEN+1],
   origname[FIT_FULLFSPECLEN+1]);

   namefld	Caller supplied name field to be parsed.
   arcname	Returned archive name, or a zero length string if not present.
   docnum_s	Returned document number as a string, or a zero length string
		if not present.
   origname	Returned original document name, or a zero length string if
		not present.

Notes:
001	Fields that are longer than the maximum allowed length are truncated.

Change log :
000	29-JUL-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_ANSI
int tips_nameparse(char *namefld, char *arcname, char *docnum_s,
  char *origname)
#else
int tips_nameparse(namefld, arcname, docnum_s, origname)
  char *namefld, *arcname, *docnum_s, *origname;
#endif
{
  char *fldptr, *endfldptr;
  int fldlen, isallnumeric;

  *arcname = 0;
  *docnum_s = 0;
  *origname = 0;

  fldptr = namefld;
  endfldptr = strchr(fldptr,'#');
  if (endfldptr == NULL)
    endfldptr = fldptr + strlen(fldptr);
  if ((fldlen = (endfldptr - fldptr)) > FIT_FULLFSPECLEN)
    fldlen = FIT_FULLFSPECLEN;

  isallnumeric = strspn(fldptr,"0123456789") >= (endfldptr - fldptr);

  if (*endfldptr == '#' && !isallnumeric) {
    /* Found archive name field */
    strncpy(arcname, fldptr, fldlen);
    arcname[fldlen] = 0;
    fldptr = endfldptr + 1;
    endfldptr = strchr(fldptr,'#');
    if (endfldptr == NULL)
      endfldptr = fldptr + strlen(fldptr);
    if ((fldlen = (endfldptr - fldptr)) > FIT_FULLFSPECLEN)
      fldlen = FIT_FULLFSPECLEN;
    isallnumeric = strspn(fldptr,"0123456789") >= (endfldptr - fldptr);
  }

  if (!fldlen) return 1;

  if (isallnumeric) {
    /* Found document number field */
    strncpy(docnum_s, fldptr, fldlen);
    docnum_s[fldlen] = 0;
    if (fldlen && docnum_s[fldlen-1] == '\n') docnum_s[fldlen-1] = 0;
    if (!*endfldptr) return 1;
    fldptr = endfldptr + 1;
    if ((fldlen = strlen(fldptr)) > FIT_FULLFSPECLEN)
      fldlen = FIT_FULLFSPECLEN;
  }

  if (!fldlen) return 1;

  strncpy(origname, fldptr, fldlen);
  origname[fldlen] = 0;
  if (fldlen && origname[fldlen-1] == '\n') origname[fldlen-1] = 0;
  return 1;
}
