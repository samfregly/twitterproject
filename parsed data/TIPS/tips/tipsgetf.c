/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_getfunckeys					tipsgetf.c

Function : Gets function key values and names.

Author : A. Fregly

Abstract : This function is used to get the value and names of the
	four configurable function keys used by TIPS, these are

	menu key	Defined by SP_MENUKEY and SP_MENUKEYNAME, default
			value is KEY_F1 and "F1".
	help key	Defined by SP_HELPKEY and SP_HELPKEYNAME, default
			value is KEY_F2 and "F2".
	exit key	Defined by SP_EXITKEY and SP_EXITKEYNAME, default
			value is 26 and "[Ctrl]-Z".
	quit key	Defined by SP_QUITKEY and SP_QUITKEYNAME, default
			value is 4 and "[Ctrl]-D".

Calling Sequence :  void tips_getfunckeys(int *menukey, char *(*menukeyname), 
  int *helpkey, char *(*helpkeyname), int *exitkey, char *(*exitkeyname),
  int *quitkey, char *(*quitkeyname));

  menukey	Returned function key used to invoke command menu line.
  menukeyname	Returned name of menu-key for display on help line.
  helpkey	Returned function key used to invoke context sensitive help.
  helpkeyname	Returned name of helpkey for display on help line.
  exitkey	Returned function key used for "normal" exit from a function.
  exitkeyname	Returned name of exitkey for display on help line.
  quitkey	Returned function key used to specify current action should
		be quit with changes discarded.
  quitkeyname	Returned name of quit key for display on help line.

Notes: 

Change log : 
000	11-NOV-91  AMF	Created.
001	12-DEC-92  AMF	Compile under Coherent.
002	08-SEP-93  AMF  HPUX compatibility.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_CURSES
#include <curses.h>
#if !FIT_COHERENT && !FIT_SUN && !FIT_HP && !FIT_LINUX
#define DEFMENUKEY (KEY_F1)
#define DEFHELPKEY (KEY_F1 + 1)
#else
#define DEFMENUKEY (KEY_F(1))
#define DEFHELPKEY (KEY_F(2))
#endif
static char defmenukeyname[]="F1";
static char defhelpkeyname[]="F2";
#else
#define DEFMENUKEY ('[' && 0x1F)
#define DEFHELPKEY ('?' && 0x1F)
static char defmenukeyname[]="[Esc]";
static char defhelpkeyname[]="[Ctrl]-?";
#endif

#define DEFEXITKEY ('z' - 'a' + 1)
#define DEFQUITKEY ('d' - 'a' + 1)
static char defexitkeyname[]="[Ctrl]-Z";
static char defquitkeyname[]="[Ctrl]-D";

#if FIT_ANSI
void tips_getfunckeys(int *menukey, char *(*menukeyname), int *helpkey,
  char *(*helpkeyname), int *exitkey, char *(*exitkeyname), int *quitkey,
  char *(*quitkeyname))
#else
void tips_getfunckeys(menukey, menukeyname, helpkey, helpkeyname, exitkey,
  exitkeyname, quitkey, quitkeyname)
  int *menukey; 
  char *(*menukeyname);
  int *helpkey;
  char *(*helpkeyname);
  int *exitkey;
  char *(*exitkeyname);
  int *quitkey;
  char *(*quitkeyname);
#endif
{

  char *varptr;

  if ((varptr = getenv("SP_MENUKEY")) != NULL) {
    sscanf(varptr,"%d", menukey);
    *menukeyname = "???";
  }
  else {
    *menukey = DEFMENUKEY;
    *menukeyname = defmenukeyname;
  }

  if ((varptr = getenv("SP_MENUKEYNAME")) != NULL)
    *menukeyname = varptr;

  if ((varptr = getenv("SP_HELPKEY")) != NULL) {
    sscanf(varptr,"%d", helpkey);
    *helpkeyname = "???";
  }
  else {
    *helpkey = DEFHELPKEY;
    *helpkeyname = defhelpkeyname;
  }

  if ((varptr = getenv("SP_HELPKEYNAME")) != NULL)
    *helpkeyname = varptr;

  if ((varptr = getenv("SP_EXITKEY")) != NULL) {
    sscanf(varptr,"%d", exitkey);
    *exitkeyname = "???";
  }
  else {
    *exitkey = DEFEXITKEY;
    *exitkeyname = defexitkeyname;
  }

  if ((varptr = getenv("SP_EXITKEYNAME")) != NULL)
    *exitkeyname = varptr;

  if ((varptr = getenv("SP_QUITKEY")) != NULL) {
    sscanf(varptr,"%d", quitkey);
    *quitkeyname = "???";
  }
  else {
    *quitkey = DEFQUITKEY;
    *quitkeyname = defquitkeyname;
  }

  if ((varptr = getenv("SP_QUITKEYNAME")) != NULL)
    *quitkeyname = varptr;
}
