#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include  <fitsydef.h>
#include  <tips.h>


/* This function is used internally by the tips_sock functions
   to write error message to stderr using perror.
*/
int tips_sockerr(char *msg, char *param) {
  char *errmsg=NULL;
  if (param) {
    if (errmsg= (char *) calloc(1,strlen(msg)+strlen(param)+ 64)) {
      sprintf(errmsg,"socket error, %s %s",msg,param);
    }
  }
  else {
    if (errmsg= (char *) calloc(1,strlen(msg)+64)) {
      sprintf(errmsg,"socket error, %s",msg);
    }
  }
  if (errmsg) {
    perror(errmsg);
    free(errmsg);
    return 1;
  }
  return 0;
}


/***************************************************************
Module: tips_sockServer

Description: 

  Sets up server socket so that clients can begin to 
  connect to the application. Clients can use tips_sockConnet
  to create connections to a server. Once a connection is
  established, "read" and "write" functions can be used for
  two way communication between the client and the server. It
  is the responsibility of the applications, server and client,
  to "close" the socket when communication is complete.

Prototype: 

  int tips_sockServer(char *s_port);

Parameters:

  s_port

    Port number that the server will use. s_port can be a numeric
    port number or a service name. Services are usually listed in
    /etc/services.

Returns:

  This function returns a file descriptor for the socket being used
  for connections or -1 if an error prevents the socket from being
  set up corretly. 

Notes:
  To detect connection attempts without using blocking
  reads, the appication may use either "select", "fcntl", or "ioctl"
  to support asynchronous processing of data from the socket.

Change Log:
000  08/07/03  AMF  Created.
***************************************************************/
 
int tips_sockServer(char *s_port) {
  int request_sock;
  char *errmsg; 
  struct servent *servp;
  static struct servent s;
  struct sockaddr_in server;

  if ((request_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    tips_sockerr("Unable to create","");
    return request_sock;
  }

  if (isdigit(s_port[0])) {
    servp = &s;
    s.s_port = htons((u_short)atoi(s_port));
  } 
  else if ((servp = getservbyname(s_port, "tcp")) == 0) {
    tips_sockerr("Unknown service",s_port);
    close(request_sock);
    return -1;
  }

  memset((void *) &server, 0, sizeof server);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = servp->s_port;

  if (bind(request_sock, (struct sockaddr *)&server, sizeof server) < 0) {
    tips_sockerr("Unable to bind",s_port);
    close(request_sock);
    return -1;
  }
  if (listen(request_sock, SOMAXCONN) < 0) {
    tips_sockerr("listen failed",s_port); 
    close(request_sock);
    return -1; 
  }
  return request_sock;
}


/***************************************************************
Module: tips_sockAccept

Description:  

  This function is meant to be used by servers to accept a
  pending socket connection request from a client. If a
  connection request is pending, the request is accepted and
  and the function reqturns a file descriptor for a socket
  to use in communicating with the client. See description
  of function tips_sockServer for details on responsibilities
  for client and server.
  
Prototype: int tips_sockAccept(int request_sock, int waittime);

Parameters:

  request_sock

    File descriptor for server socket being used to accept
    connection requests.

  waittime

    Milliseconds to wait for a connection attempt to be 
    received. A value of 0 will result in an immediate
    return by this function if no connection attempt is
    currently pending.
 
  
Returns:

  This function returns a file descriptor for the connection
  if one is accepted. If no connection is created, a value
  of -1 is returned. 

Change Log:
000  08/07/03  AMF  Created.
***************************************************************/
int tips_sockAccept(int request_sock, int waittime) {
  int maxfd, nfound, addrlen, fd;
  char ebuf[256];
  fd_set rmask, mask;
  struct sockaddr_in remote;
  static struct timeval timeout; 
  timeout.tv_sec= waittime / 1000;
  timeout.tv_usec = waittime % 1000;
  
  FD_ZERO(&mask);
  FD_SET(request_sock, &mask);
  maxfd = request_sock;
  rmask = mask;
  nfound = select(maxfd+1, &mask, (fd_set *)0, (fd_set *)0, &timeout);
  if (nfound < 0) {
    if (errno == EINTR) {
      tips_sockerr("interrupted system call","");
      fd = -1;
    }
    /* something is very wrong! */
    tips_sockerr("select error","");
    fd = -1; 
  }
  else if (nfound == 0) {
    fd = -1;
  }
  else {
    if (FD_ISSET(request_sock, &rmask)) {
      /* a new connection is available on the connetion socket */
      addrlen = sizeof(remote);
      fd = accept(request_sock, (struct sockaddr *)&remote, (unsigned *) &addrlen);
      if (fd < 0) {
        tips_sockerr("accept error",""); 
        fd = -1;
      }
      else {
        printf("connection from host %s, port %d, socket %d\n",
          inet_ntoa(remote.sin_addr), ntohs(remote.sin_port), fd);
      }
    }
    else {
      sprintf(ebuf,"select indicated request available but none found on: %d",request_sock);
      tips_sockerr(ebuf,"");
      fd = -1;
    }
  }
  return fd;
}


/***************************************************************
Module: tips_sockConnect

Description: 

  tips_sockConnect

Prototype: 

  int tips_sockConnect(char *serverName, char *s_port);

Parameters:

  serverName

    This must be a server name that can be resolved by either a
    DNS server or by looking in the hosts file. The serverName
    can also be an IP address.

  s_port

    Port number that the server is listening for connection on.

Returns:

  This function returns a file descriptor for the socket to use
  in communicating with the server, or -1 if the connection attmep
  failed. 

Notes:
  "read" and "write" can be used for communicating with the server
  once the connection is made. 

Change Log:
000  08/07/03  AMF  Created.
***************************************************************/
 
int tips_sockConnect(char *serverName, char *s_port) {
  struct hostent *hostp;
  struct servent *servp;
  struct sockaddr_in server;
  int sock;
  char *fullServerName=NULL;

  if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    tips_sockerr("unable to create socket","");
    return -1;
  }
  if (isdigit(*s_port)) {
    static struct servent s;
    servp = &s;
    s.s_port = htons((u_short)atoi(s_port));
  } else if ((servp = getservbyname(s_port, "tcp")) == 0) {
    tips_sockerr("unknown service: ",s_port);
    return -1;
  }
  if ((hostp = gethostbyname(serverName)) == 0) {
    tips_sockerr("unknown host:",serverName);
    return -1;
  }

  fullServerName= (char *) calloc(1,strlen(serverName)+strlen(s_port)+2);
  strcpy(fullServerName,serverName);
  strcat(fullServerName,":");
  strcat(fullServerName,s_port); 

  memset((void *) &server, 0, sizeof server);
  server.sin_family = AF_INET;
  memcpy((void *) &server.sin_addr, hostp->h_addr, hostp->h_length);
  server.sin_port = servp->s_port;
  if (connect(sock, (struct sockaddr *)&server, sizeof server) < 0) {
    (void) close(sock);
    tips_sockerr("error attempting to perform connect:",fullServerName);
    sock = -1;
  }
  free(fullServerName);
  return sock;
}


/***************************************************************
Module: tips_sockRead

Description: 

  tips_sockRead is used to read any available data from a socket.
  The socket is read in a non-blocking manner so that the function
  immediately returns if there is no data to be read. A waittime
  can be specified for an amount of time for the function to wait
  on data to become available. 

Prototype: 

  int tips_sockRead(int fd, int waittime, char *buf, int buflen);

Parameters:

  fd

    file descriptor for open socket.

  waittime

    Milliseconds to wait for data to be available for reading. A 
    value of 0 will result in an immediate return by this function 
    if no data is currently available to be read.

  buf

    Buffer into which to write any data read from the socket.

  buflen

    Size of buf. No more than buflen characters will be read.


Returns:

  The number of bytes read. A return value of 0 indicates no data
  was available for reading. A return value of -1 indicates either
  an error on the read or an end of file has been reached.

Change Log:
000  08/07/03  AMF  Created.
***************************************************************/

int tips_sockRead(int fd, int waittime, char *buf, int buflen) {
  int maxfd, nfound, bytesread;
  char ebuf[256];
  fd_set rmask, mask;
  static struct timeval timeout; 

  timeout.tv_sec= waittime / 1000;
  timeout.tv_usec = waittime % 1000;
  
  FD_ZERO(&mask);
  FD_SET(fd, &mask);
  maxfd = fd;
  rmask = mask;

  nfound = select(maxfd+1, &mask, (fd_set *)0, (fd_set *)0, &timeout);
  printf("select completed on fd: %d, nfound: %d\n", fd, nfound);

  if (nfound < 0) {
    if (errno == EINTR) {
      tips_sockerr("read, interrupted system call","");
      bytesread = 0;
    }
    else {
      /* something is very wrong! */
      tips_sockerr("read, select error","");
      bytesread = -1; 
    }
  }
  else if (nfound == 0) {
    bytesread = 0;
    printf("select on socket: %d completed without data\n");
  }
  else if (FD_ISSET(fd, &rmask)) {
    /* process the data */
    bytesread = read(fd, buf, buflen-1);
    if (bytesread < 0) {
      tips_sockerr("socket read error",""); 
      bytesread = -1; 
    }
    else if (bytesread == 0) {
      sprintf(ebuf,"end of file on %d",fd);
      tips_sockerr(ebuf,""); 
      bytesread = -1; 
    }
    else {
      printf("read completed, read %d bytes\n",bytesread);
      buf[bytesread] = '\0';
    }
  }
  else {
    bytesread = 0;
    printf("select completed with nfound != 0 and fd not indicating data avail\n");
  }
  return bytesread;
}
