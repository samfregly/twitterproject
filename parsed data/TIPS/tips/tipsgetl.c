/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_getline					getline.c

Function : Gets next line of input for document being read from input stream.

Author : A. Fregly

Abstract : This routine is used to return the next line of input for a
	fixed length document being read from a continuous input stream.
	This routine is intended for use by FILTERS which need to process
	the next document as described by a TIPS file descriptor. This
	routine may be used to process data from a single input source.
	Simultaneous use for more than one input source will cause corruption
	of internal pointers.

Calling Sequence : char *inline = tips_getline(int *newdoc, FILE *in, 
  long *docleft, int *inlen); 

	inline	Returned pointer at buffer containing next input line.
		inline will be NULL when the end of the document has been
		reached.
	in	File handle for reading document.
	docleft	Number of bytes left to be input for document. The caller
		should set this at the same time newdoc is set. tips_getline
		will maintain the value of docleft.
	len	Returned length of input line. len will be set to 0 when
		the end of the document has been reached.

Notes : 

Change log : 
000	18-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>

#define BUFFERSIZE 4096

#if FIT_ANSI
char *tips_getline(int *newdoc, FILE *in, long *docleft, int *inlen)
#else
char *tips_getline(newdoc, in, docleft, inlen)
  int *newdoc;
  FILE *in;
  long *docleft;
  int *inlen;
#endif
{

  static char inbuffer[BUFFERSIZE+1];
  static int ineob,inptr;
  int movelen,startoffset,remainingbuf,readlen;
  char *lfptr;

  if (*newdoc) {
    /* New document, reset pointers and clear newdoc flag */
    ineob = -1;
    inptr = -1;
    *newdoc = 0;
  }

  /* Remember current pointer into document buffer */
  startoffset = inptr+1;

  while (*docleft || inptr != ineob) {
    if (inptr == ineob) {
      /* Reached the end of the input buffer */
      if (startoffset) {
	/* Buffer had stuff in it, see if all of it was processed */
	movelen = ineob - startoffset + 1;
	if (movelen)
	  /* Move unprocessed text to beginning of buffer */
	  memmove(inbuffer,inbuffer+startoffset,movelen);
	/* Reset pointers at start of unprocessed data and end of buffer */
	ineob = movelen - 1;
	startoffset = 0;
      }

      if (ineob < 0) {
	/* Couldn't find a newline in an entire buffers worth, have to
	   start with a new buffer, reset pointers */
	ineob = -1;
	remainingbuf = BUFFERSIZE;	/* free space is entire buffer */
	startoffset = 0;
      }
      else
	/* Still some text in the buffer, see how much free space */
        remainingbuf = BUFFERSIZE - ineob - 1;

      /* Offset of last processed data is end of previous buffer data */
      inptr = ineob;

      /* See how much to read in, lesser of remaining space in buffer or
	 remaining unread portion of document */
      readlen = (*docleft > remainingbuf) ? remainingbuf : *docleft;
      /* Read in more text */
      readlen = fread(inbuffer+ineob+1, 1, readlen, in);
      if (readlen == 0) goto ERREXIT;

      /* Update pointers */
      *docleft -= readlen;
      ineob += readlen;
      /* Put terminating null byte into buffer */
      inbuffer[ineob+1] = 0;
    }

    /* Find offset of next line feed in buffer */
    lfptr = strchr(inbuffer+inptr+1,'\n');
    if (lfptr != NULL) {
      /* Found a line feed, update pointers and set return values */
      inptr += lfptr - (inbuffer + inptr);
      *inlen = lfptr - (inbuffer + startoffset) + 1;
      /* done */
      return inbuffer + startoffset;
    }
    else {
      /* No line feed in remaining portion of buffer, set pointers so that
	 next pass will read in more data */
      inptr = ineob;
    }
  }

  /* Didn't find an ending line feed, return everthing from staring offset */
  *inlen = ineob - startoffset + 1;
  inptr = inptr + *inlen;
  return (*inlen) ? inbuffer + startoffset : NULL;

ERREXIT:
  *inlen = 0;
  return NULL;
}
