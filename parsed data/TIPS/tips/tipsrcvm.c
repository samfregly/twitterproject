/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_rcvmsg					tipsrcvm.c

Function : Receives messages from message port.

Author : A. Fregly

Abstract : This function is used to receive messages from a message port which
	were sent using the tips_sendmsg function.

Calling Sequence : int retstat = tips_rcvmsg(tips_msgqueue msgqid, 
    int msgtype, int timeout, char *retmsg, unsigned maxmsglen,
    unsigned &retlen, long *retmsgtype, int *senderpid);

  msgqid	Message queue handle returned by tips_newmsgqueue or 
  		tips_attachmsgqueue.
  msgtype	Message type for messages to be retrieved. See documentation
		of msgget for specifics. By convention, message types
		1 to TIPS_MAXMSGNUM are reserved for use by TIPS daemons, and
		other message types are used for return packets during
		synchronous communications, where the message type consists
		of the pid of the originator + TIPS_MAXMSGNUM + 1.
  timeout	Number of seconds to wait for a message. A value of 0
  		indicates nowait, and a value less than 0 indicates an
                infinite wait.
  retmsg	Return buffer into which to place received messages.
  maxmsglen	Maximum length of returned message. Currently, maxmsglen
		is limited to TIPS_MAXMSGDATA.
  retlen	Length of returned message data.
  retmsgtype	Message type actually received.
  senderpid	PID of sender of the messsage.

Notes: 

Change log : 
000	17-OCT-91  AMF	Created.
001	19-JUL-92  AMF	Support DOS. Changed to use tips_msgqueue and
			tips_s_msg structure.
002	14-DEC-92  AMF	Changed parameters.
003	25-MAR-93  AMF	Compile under Coherent/GNU C.
004	06-AUF-03  AMF	Temp fix making this a dummmy call under Cygwin.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <fitsydef.h>
#if !FIT_CYGWIN && (FIT_UNIX || FIT_COHERENT)
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#endif
#if FIT_CYGWIN
#include <cygwin/types.h>
#include <cygwin/ipc.h>
#include <cygwin/msg.h>
#endif

#include <tips.h>
#if FIT_DOS || FIT_BSD
#include <stdlib.h>
#include <string.h>
#include <drct.h>
#endif

#if FIT_UNIX || FIT_COHERENT
#if FIT_ANSI
void tips_rcvmsg_alarm(int sig);
#else
void tips_rcvmsg_alarm();
#endif
#endif

#if FIT_ANSI
int tips_rcvmsg(tips_msgqueue msgqid, long msgtype, int timeout, 
  char *retmsg, unsigned maxmsglen, unsigned *retlen, long *retmsgtype,
  int *senderpid)
#else
int tips_rcvmsg(msgqid, msgtype, timeout, retmsg, maxmsglen, retlen,
    retmsgtype, senderpid)
  tips_msgqueue msgqid;
  long msgtype;
  char *retmsg;
  int timeout;
  unsigned maxmsglen, *retlen;
  long *retmsgtype;
  int *senderpid;
#endif
{

  int retstat=0;
#if FIT_UNIX || FIT_COHERENT

  int msgflg, timeleft, timeused;
  void (*oldfunc)();
  struct tips_s_msg inmsg;
  if (maxmsglen < 0 || maxmsglen > TIPS_MAXMSGDATA) return 0;
  retstat = 0;

  /* Set up wait flag for input */
  msgflg = (timeout) ? 0 : IPC_NOWAIT;

  if (timeout > 0) {
    /* Timeout on the wait, set up alarm for it */
    oldfunc = (void (*)()) signal(SIGALRM, tips_rcvmsg_alarm);
    timeleft = alarm(0);
    alarm((unsigned) timeout);
  }

  /* Attempt to read incoming message */
  retstat = (msgrcv(msgqid, (struct msgbuf *) &inmsg, maxmsglen + sizeof(inmsg.hdr), msgtype, 
	msgflg) > 0);
  if (retstat) {
    *retlen = inmsg.hdr.datalen;
    *retmsgtype = inmsg.hdr.msgtype;
    *senderpid = inmsg.hdr.pid; 
    if (inmsg.hdr.datalen) memcpy(retmsg, inmsg.data, inmsg.hdr.datalen);
  }
  else {
    *retlen = 0;
    *retmsgtype = 0;
    *senderpid = 0;
  }

  if (timeout > 0) {
    /* Reset previous alarm */
    timeused = timeout - alarm(0);
    if (oldfunc) {
      signal(SIGALRM, oldfunc);
      if (timeleft) {
        timeleft -= timeused;
        if (timeleft > 0)
	  /* Previous alarm still had time on it, set alarm */
	  alarm((unsigned) timeleft);
        else
	  /* Previous alarm would have gone off, signal the alarm */
	  kill(SIGALRM, getpid());
      }
    }
  }

  return retstat;
}


#if FIT_ANSI
void tips_rcvmsg_alarm(int sig)
#else
void tips_rcvmsg_alarm(sig)
  int sig;
#endif
{
  ;
}

#else

#if FIT_ZORTECH
#include <time.h>
#endif
#if FIT_TURBOC
#include <dos.h>
#endif

  struct tips_s_msg msg, savemsg;
  unsigned maxmsgtype;
  long msglen, recnum;
  int retstat, waittime = timeout;

  maxmsgtype = (msgtype >= 0) ? msgtype : -msgtype;

  for (waittime = timeout, savemsg.hdr.seqnum = 0; !savemsg.hdr.seqnum &&
    (waittime >= 0 || timeout < 0); waittime -= (timeout >= 0)) {

    /* Search the msgqueue file for the entry with the lowest sequence
       number */

    /* Skip over the first record, which is used to keep track of the
       highest sequence number */
    if (!drct_rdrec(msgqid, 0L, (char *) &msg, sizeof(msg), &msglen))
      return 0;

    while (drct_rdseq(msgqid,(char *) &msg, sizeof(msg), &msglen))
      if ((!savemsg.hdr.seqnum || savemsg.hdr.seqnum > msg.hdr.seqnum) &&
          (!maxmsgtype || maxmsgtype >= msg.hdr.msgtype)) {
        savemsg = msg;
        recnum = drct_curpos(msgqid);
      }

    if (!savemsg.hdr.seqnum && (timeout < 0 || waittime))
#if FIT_TURBOC || FIT_ZORTECH
      Sleep((time_t) 1);
#endif
#if FIT_MSC
	  _sleep(1L);
#endif
  }
  retstat = (savemsg.hdr.seqnum > 0);
  if (retstat) {
    if (!drct_setpos(msgqid, recnum)) return 0;
    if (!drct_delrec(msgqid, 0)) return 0;
    *retlen = savemsg.hdr.datalen;
    *retmsgtype = savemsg.hdr.msgtype;
    *senderpid = savemsg.hdr.pid; 
    if (savemsg.hdr.datalen) memcpy(retmsg, savemsg.data, savemsg.hdr.datalen);
  }
  else {
    *retlen = 0;
    *retmsgtype = 0;
    *senderpid = 0;
  }

  return retstat;
}
#endif
