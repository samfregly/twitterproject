/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_makedescr					tipsmake.c 

Function : Makes a TIPS document descriptor. 

Author : A. Fregly 

Abstract : This function is used to create a TIPS document descriptor. A
	TIPS document descriptor consists of three fields, two of which
	may have subfields. The format of the descriptor is:

	namefield,length{,key}\n

	The namefield is used for locating the document on the system. It
	takes the format {archive}{#}{docnum}{#}{origname}. Note that the "#"
	character serves as a separator, and is not used unless needed
	to separate two subfields.  The archive subfield is used to specify
	the name of the TIPS archive in which the document resides. If the
	archive subfield is present, the docnum field should also be present.
	The docnum field is the document number within the archive. The
	origname field is used to point back to the document original which
	is a file on the file system.

	The length field is specifies the length of the document in bytes.

	The key field is an optional field. It is typically used for document
	routing. The TIPS software formats it as follows when the descriptor
	is being used to record a search hit. 

	{queryname}{#}dockeyfield}

	The queryname subfield is used to specify the file name for the
	matching query. The "#" is used to separate the queryname subfield
	from the remainder of the key field, and is not present if the key
	field contains only the queryname subfield. The dockeyfield is a
	optional user defined text field which may contain any characters 
	except \n and \0. 

Calling Sequence : tips_makedescr(char *archive, long docnum, char *origname,
  long doclen, char *key, int buflen, char buf[buflen], int *retlen);

  archive	Optional archive name.
  docnum	Optional document number. A value of -1 indicates it is not
		to be used.
  origname	Optional original document name.
  doclen	Length of document.
  key		Optional key field.
  buflen	Length of output buffer.
  buf		Returned document descriptor. If the created document
		descriptor would overflow buf, it is truncated to a length of 
		buflen-1, not including the terminating \0 byte.
  retlen	Length of returned descriptor, not including the terminating
		\0.

Notes: 

Change log : 
000	13-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_ANSI
void tips_makedescr(char *archive, long docnum, char *origname,
  long doclen, char *key, int buflen, char *buf, int *retlen)
#else
void tips_makedescr(archive, docnum, origname, doclen, key, buflen, buf,
    retlen) 
  char *archive;
  long docnum;
  char *origname;
  long doclen;
  char *key;
  int buflen;
  char *buf;
  int *retlen;
#endif
{
  int idx=-1;
  char num_s[14];
  if (archive != NULL && *archive) {
    fit_sadd(buf, buflen-1, archive, &idx);
    fit_sadd(buf, buflen-1, "#", &idx);
  }
  if (docnum >= 0) {
    sprintf(num_s, "%ld", docnum);
    fit_sadd(buf, buflen-1, num_s, &idx);
    if (*origname) 
      fit_sadd(buf, buflen-1, "#", &idx);
  }
  if (origname != NULL && *origname)
    fit_sadd(buf, buflen-1, origname, &idx);

  fit_sadd(buf, buflen-1, ",", &idx);
  sprintf(num_s,"%ld",doclen);
  fit_sadd(buf, buflen-1,num_s, &idx);

  if (key != NULL && *key) {
    fit_sadd(buf, buflen-1, ",", &idx); 
    fit_sadd(buf, buflen-1, key, &idx);
  }

  if (buf[idx] != '\n') {
    if (idx > buflen-1)
      --idx;
    buf[++idx] = '\n';
  }
  buf[++idx] = 0;
  *retlen = idx;
}
