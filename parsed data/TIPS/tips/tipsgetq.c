/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_getqry                                       tipsgetq.c

Function : Retrieves object query for specified result key field.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = tips_getqry(char *keyfld, 
  struct qc_s_cmpqry *(*cmpqry));

Notes: 

Change log : 
000     10-SEP-91  AMF  Created.
001     12-DEC-92  AMF  Compile under Coherent.
002     26-MAR-93  AMF  Compile under GNU C.
003     27-JUL-94  AMF  Include stdlib.h
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <qc.h>
#if !FIT_DOS
#include <sys/stat.h>
#else
#include <sys\stat.h>
#endif
#include <fcntl.h>

#if FIT_ANSI
int tips_getqry(char *keyfld, struct qc_s_cmpqry *(*cmpqry))
#else
int tips_getqry(keyfld, cmpqry)
  char *keyfld; 
  struct qc_s_cmpqry *(*cmpqry);
#endif
{
  char *extptr, *querytext, *sepptr;
  char queryname[FIT_FULLFSPECLEN+1];
  size_t len, qrylen;
  FILE *qryin;
  struct stat statbuf;

  *cmpqry = (struct qc_s_cmpqry *) NULL;
  querytext = (char *) NULL;

  sepptr = strchr(keyfld, '#');
  if (sepptr != NULL) 
    len = min(sepptr - keyfld, FIT_FULLFSPECLEN);
  else
    len = min(strlen(keyfld), FIT_FULLFSPECLEN);

  if (!len) return 0;

  strncpy(queryname, keyfld, len);

  queryname[len] = 0;
  extptr = strchr(queryname,'.');

  if (extptr != NULL && (!strcmp(extptr,".qc") || !strcmp(extptr,".QC"))) {
    qc_readqry(queryname, &querytext, cmpqry);
  }
  else {
    if (!stat(queryname, &statbuf) && statbuf.st_size > 0) {
      qrylen = (size_t) statbuf.st_size;
      if (qrylen == (size_t) statbuf.st_size &&
	  (querytext = (char *) malloc(qrylen + 1)) != NULL) {
	if ((qryin = fopen(queryname,"r"))) {
	  if ((len = (unsigned) fread(querytext, 1, qrylen, qryin)) == qrylen) {
	    querytext[qrylen] = 0;
	    qc_compile(querytext, 0, (char *(*)) NULL, 0, (char *(*)) NULL,
		(char *) NULL, cmpqry);
	  }
	  fclose(qryin);
	}
      }
    }
  }

  if (querytext) free(querytext);

  return (*cmpqry != (struct qc_s_cmpqry *) NULL);
}
