/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_loadflds					loadflds.c

Function : Loads field names from a field definition file.

Author : A. Fregly

Abstract : This routine will load the field names from a user field definition
	file. The field names are taken to be the first word of each line of
	the specified file. Implicit in the ordering of the fields in the file
	is the field number, with the first field starting at 0. The field
	number following a beginning of field delimiter is used to mark the
	beginning of a new field within a document.

Calling Sequence : ss = tips_loadflds(char *fieldfile, char *(*(*fields)), 
	char *(*(*fielddata), int *nfields);

	fieldfile	Name of field file to be read.
	fields		Returned field name list. This list is allocated and
			filled in by tips_loadflds. The list will be 
			deallocated by subsequent calls to loadflds, or may
			by  deallocated by freeing each field name within the
			array, (*fields)[i], then freeing the array of 
			character string pointers, *fields. *fields should
			be set to NULL prior to the initial call to loadflds.
        fielddata	Returned field data list. Field data consists of any
			data following the first blank after a field name.
			This list is allocated and filled in by tips_loadflds.
			The list will be deallocated by subsequent calls to 
			loadflds, or may by  deallocated by freeing each field 
			name within the array, (*fielddata)[i], then freeing 
			the array of character string pointers, *fielddata. 
			*fielddata should be set to NULL prior to the initial 
			call to loadflds.
	nfields		If loadflds is being used to deallocate previously
			loaded field names, nfields should point to the count
			of the number of field names. On return, *nfields will
			contain the number of fields loaded into fields.
Notes : 
	The maximum number of fieldnames which may be loaded is defined by
	the constant TIPS_MAXFIELD, and the maximum field name length is 
	defined by the constant TIPS_FLDNAMEMAX. Both constants are defined
	in TIPS.H.

Change log : 
000	14-MAY-91  AMF	Created.
001	26-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <tips.h>

#if FIT_ANSI
int tips_loadflds(char *fieldfile, char *(*(*fields)), char *(*(*fielddata)),
  int *nfields)
#else
int tips_loadflds(fieldfile, fields, fielddata, nfields)
  char *fieldfile, *(*(*fields)), *(*(*fielddata));
  int *nfields;
#endif
{

  FILE *in;
  char tfieldrec[TIPS_FLDRECMAX+1]; 
  char *cptr;
  int len,i,stat,namelen,datalen;

  stat = 1;

  if (*nfields > 0) {
    if (*fields)
      for (i = 0; i < *nfields; ++i)
        if ((*fields)[i]) free((*fields)[i]);
    if (*fielddata)
      for (i = 0; i < *nfields; ++i)
        if ((*fielddata)[i]) free((*fielddata)[i]);
    *nfields = 0;
  }
  if (*fields) {
    free(*fields);
    *fields = (char *(*)) NULL;
  }

  if (*fielddata) {
    free(*fielddata);
    *fielddata = (char *(*)) NULL;
  }

  in = fopen(fieldfile,"r");
  if (!in) goto ERREXIT;

  *fields = (char *(*)) calloc(TIPS_MAXFIELD+1,sizeof(*(*fields)));
  if (!*fields) goto ERREXIT;
  *fielddata =  (char *(*)) calloc(TIPS_MAXFIELD+1,sizeof(*(*fielddata)));
  if (!*fielddata) goto ERREXIT;

  for (i=0; i < TIPS_MAXFIELD + 1; ++i) {
    (*fields)[i] = (char *) NULL;
    (*fielddata)[i] = (char *) NULL;
  }

  while (*nfields < TIPS_MAXFIELD && !feof(in)) {
    if (fgets(tfieldrec,TIPS_FLDRECMAX,in) == NULL) break;
    len = strlen(tfieldrec);
    if (tfieldrec[len-1] == '\n') {
      len -= 1; 
      tfieldrec[len] = 0;
    }
    else
      while (!feof(in) && fgetc(in) != '\n')
	;

    for (cptr = tfieldrec; *cptr > ' '; ++cptr)
      ;

    *cptr = 0;
    namelen = cptr - tfieldrec;

    (*fields)[*nfields] = (char *) calloc(1,namelen+1);
    if (!(*fields)[*nfields]) {
      stat = 0;
      break;
    }
    strcpy((*fields)[*nfields], tfieldrec);

    datalen = max (0,len - namelen - 1);
    (*fielddata)[*nfields] = (char *) calloc(1,datalen+1);
    if (!(*fielddata)[*nfields]) {
      stat = 0;
      break;
    }
    if (datalen)
      strncpy((*fielddata)[*nfields], ++cptr, datalen);

    ++*nfields;
  }

  fclose(in);
  goto DONE;

ERREXIT:
  stat = 0;

DONE:
  return stat;
}
