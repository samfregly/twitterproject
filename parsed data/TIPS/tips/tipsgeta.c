/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_getattributes				tipsgeta.c

Function : Returns screen display attributes for various display functions.

Author : A. Fregly

Abstract : This function is used to set the display attributes for normal and
	menu displays by spot programs. The attributes are set based on whether
	or not the terminal supports color, and the settings of the SP_xxxxx
	environment variables which are used in configuring the display.

Calling Sequence : void tips_getattributes(int *usecolor, int *usepointer,
  int *usecheckmarks, int *usehotkeys, int *uselinedrawing, int *dispnorm, 
  int *disphigh, int *dispsel, int *disphot, int *mennorm, int *menhigh, 
  int *mensel, int *menhot);

  usecolor	If *usecolor is non-zero, its value will be set based on
		the defined value of the environment variable SP_COLOR, or 
		if SP_COLOR is not defined, it will default to 1.
  usepointer	Use pointer in window, defined based on SP_POINTER with a
		default of 0.
  usecheckmarks	Use checkmarks to mark selected items in a multiple select
		menu. Defined based on SP_CHECKMARK with a default of 1.
  usehotkeys	Use hot keys in menus. Defined based on SP_HOTKEYS, with
		a default of 1.
  uselinedrawing Enables line drawing. Boxes will be drawn around pull-down
		menus and other pop-ups. Defined based on SP_LINEDRAWING,
		with a default of 1.
  dispnorm	Display attribute to be used for "normal" text. If color
		is enabled, defined based on SP_DISPNORMAL color pair.
  disphigh	Display attribute for "normal" highlighted text. If color
		is enabled, defined based on SP_DISPHIGH color pair.
  dispsel	Display attribute for "normal" selected text. If color
		is enabled, defined based on SP_DISPSELECTED color pair.
  disphot	Display attribute for "normal" hot keys. If color is
		enabled, defined based on SP_DISPHOT.
  mennorm	Display attribute to be used for "menu" text. If color
		is enabled, defined based on SP_MENNORMAL color pair.
  menhigh	Display attribute for "menu" highlighted text. If color
		is enabled, defined based on SP_MENHIGH color pair.
  mensel	Display attribute for "menu" selected text. If color
		is enabled, defined based on SP_MENSELECTED color pair.
  menhot	Display attribute for "menu" hot keys. If color is
		enabled, defined based on SP_MENHOT.

Notes: 

Change log : 
000	10-NOV-91  AMF	Created by extracting from spot_getenv.
******************************************************************************/

/* #include <stdio.h> */
#include <stdio.h>
#include <fitsydef.h>
#if FIT_CURSES
#include <curses.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#if FIT_MSC
#include <string.h>
#endif

#if FIT_ANSI
void tips_getattributes(int *usecolor, int *usepointer, int *usecheckmarks,
  int *usehotkeys, int *uselinedrawing, int *dispnorm, int *disphigh, 
  int *dispsel, int *disphot, int *mennorm, int *menhigh, int *mensel, 
  int *menhot)
#else
void tips_getattributes(usecolor, usepointer, usecheckmarks, usehotkeys,
  uselinedrawing, dispnorm, disphigh, dispsel, disphot, mennorm, menhigh, 
  mensel, menhot)
  int *usecolor, *usepointer, *usecheckmarks, *usehotkeys, *uselinedrawing;
  int *dispnorm, *disphigh, *dispsel, *disphot, *mennorm, *menhigh, *mensel;
  int *menhot;
#endif
{
  int foreground, background;
  char *varptr;
  unsigned long hilite;

  *usepointer = 0;
  if ((varptr = getenv("SP_POINTER")) != NULL)
    *usepointer = !strcmp(varptr,"1");

  *usecheckmarks = 1;
  if ((varptr = getenv("SP_CHECKMARK")) != NULL)
    *usecheckmarks = !strcmp(varptr,"1");

  if ((varptr = getenv("SP_COLOR")) != NULL && *usecolor)
    *usecolor = !strcmp(varptr,"1");

  *usehotkeys = 1;
  if ((varptr = getenv("SP_HOTKEYS")) != NULL)
    *usehotkeys = !strcmp(varptr,"1");

  *uselinedrawing = 1;
  if ((varptr = getenv("SP_LINEDRAWING")) != NULL)
    *uselinedrawing = !strcmp(varptr,"1");

#if FIT_COLORCURSES
  if (*usecolor) {
    if ((varptr = getenv("SP_DISPNORMAL")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(1, foreground, background);
    }
    else
      init_pair(1, COLOR_GREEN, COLOR_BLACK);

    if ((varptr = getenv("SP_DISPHIGH")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(2, foreground, background);
    }
    else
      init_pair(2, COLOR_BLACK, COLOR_GREEN);

    if ((varptr = getenv("SP_DISPSELECTED")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(3, foreground, background);
    }
    else
      init_pair(3, COLOR_RED, COLOR_BLACK);

    if ((varptr = getenv("SP_DISPHOT")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(4, foreground, background);
    }
    else
      init_pair(4, COLOR_WHITE, COLOR_BLACK);

    if ((varptr = getenv("SP_MENNORMAL")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(5, foreground, background);
    }
    else
      init_pair(5, COLOR_WHITE, COLOR_BLUE);

    if ((varptr = getenv("SP_MENHIGH")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(6, foreground, background);
    }
    else
      init_pair(6, COLOR_BLUE, COLOR_WHITE);

    if ((varptr = getenv("SP_MENSELECTED")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(7, foreground, background);
    }
    else
      init_pair(7, COLOR_BLUE, COLOR_CYAN);

    if ((varptr = getenv("SP_MENHOT")) != NULL) {
      sscanf(varptr,"%d %d", &foreground, &background);
      init_pair(8, foreground, background);
    }
    else
      init_pair(8, COLOR_CYAN, COLOR_BLUE);

    if (!*usepointer) {
      *dispnorm = COLOR_PAIR(1);
      *disphigh = COLOR_PAIR(2);
      *disphot = COLOR_PAIR(4);

      *mennorm = COLOR_PAIR(5);
      *menhigh = COLOR_PAIR(6);
      *menhot = COLOR_PAIR(8);
    }
    else {
      *dispnorm = COLOR_PAIR(1);
      *disphigh = COLOR_PAIR(1);
      *disphot = COLOR_PAIR(4);

      *mennorm = COLOR_PAIR(5);
      *menhigh = COLOR_PAIR(5);
      *menhot = COLOR_PAIR(8);
    }

    if (!*usecheckmarks) {
      *dispsel = COLOR_PAIR(3);
      *mensel = COLOR_PAIR(7);
    }
    else {
      *dispsel = COLOR_PAIR(1);
      *mensel = COLOR_PAIR(5);
    }
  }
  else {
#endif
#if FIT_CURSES
    hilite = A_STANDOUT;
    if (!*usepointer) {
      *dispnorm = 0;
      *disphigh = hilite;
      *disphot = hilite;

      *mennorm = hilite;
      *menhigh = 0;
      *menhot = 0;
    }
    else {
      *dispnorm = 0;
      *disphigh = 0;
      *disphot = hilite;

      *mennorm = hilite;
      *menhigh = hilite;
      *menhot = 0;
    }

    if (!*usecheckmarks) {
      *dispsel = hilite;
      *mensel = 0;
    }
    else {
      *dispsel = 0;
      *mensel = 0;
    }
#endif
    ;
#if FIT_COLORCURSES
  }
#endif
}
