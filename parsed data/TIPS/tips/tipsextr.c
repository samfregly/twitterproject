/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : tips_extractdescr					tipsextr.c

Function : Extracts TIPS document descriptor from front of buffer.

Author : A. Fregly

Abstract : This function is used to find the location of a TIPS document
	descriptor at the front of the supplied buffer. Currently, TIPS
	descriptrs are terminated by a newline character, '\n', so this
	function will delineate the descriptor as consisting of the start
	of the buffer to the first newline (including the newline.

Calling Sequence : char *endptr = tips_extractdescr(char *buffer,
  char docdescriptor[TIPS_DESCRIPTORMAX+1], int *descrlen);

  buffer	Buffer from which descriptor is to extracted.
  docdescriptor	Buffer into which descriptor is to be copied. A zero length
		string indicates the buffer started with a zero length
		string.
  descrlen	Returned number of characters copied in docdescriptor not
		including the ending null byte. descrlen will never have
		a value greater than TIPS_DESCRIPTORMAX.
  endptr	Returned pointer at the newline or null byte which
		terminated the descriptor in the buffer.


Notes:

Change log :
000  29-JUL-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>

#if FIT_ANSI
char *tips_extractdescr(char *buffer, char *docdescriptor,
  int *descrlen)
#else
char *tips_extractdescr(buffer, docdescriptor, descrlen)
  char *buffer, *docdescriptor;
  int *descrlen;
#endif
{
  char *descrend;

  /* Find terminating newline or null byte if no newline */
  descrend = strchr(buffer,'\n');
  if (descrend == NULL) 
    descrend = buffer + strlen(buffer);
  else
    descrend += 1;

  /* Set length of returned descriptor */
  *descrlen = descrend - buffer;

  if (*descrlen > TIPS_DESCRIPTORMAX) *descrlen = TIPS_DESCRIPTORMAX;

  /* Copy the descriptor into the return buffer */
  if (*descrlen) strncpy(docdescriptor, buffer, *descrlen);
  docdescriptor[*descrlen] = 0;

  return descrend;
}
