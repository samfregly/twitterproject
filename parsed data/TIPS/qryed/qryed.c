/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : qryed						qryed.c

Function : Simple query editor.

Author : A. Fregly

Abstract : This program provides a minimalist query editor for use
	in editing spot queries.

Calling Sequence : qryed qryspec

Notes: 

Change log : 
000	09-OCT-91  AMF	Created.
001	26-MAR-93  AMF	Compile under GNU C.
002	09-SEP-93  AMF	HPUX compatibility.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>
#include <curses.h>
#include <win.h>

#define QRYED_LABELWINLEN 20

#if FIT_ANSI
void qryed_message(char *message);
#else
void qryed_message();
#endif
/**********
void qryed_alarm(int sig);
**********/

/************
char *qryed_cmdopts[] = {"File", "Edit", "Help", ""};
char *qryed_fileopts[] = {"Save", "Save As", "New", "Include", "Exit", "");
************/

WINDOW *qryed_cmdwin, *qryed_labelwin, *qryed_dispwin, *qryed_footerwin;
WINDOW /********* *qryed_filewin, ********/ *qryed_helpwin;
struct men_s_menu *qryed_cmdmenu /****** , *qryed_filemenu *******/;
int qryed_usecolor, qryed_usepointer, qryed_usecheckmarks, qryed_usehotkeys;
int qryed_uselinedrawing;
int qryed_dispnorm, qryed_disphigh, qryed_dispsel, qryed_disphot;
int qryed_mennorm, qryed_menhigh, qryed_mensel, qryed_menhot;
int qryed_menukey, qryed_helpkey, qryed_exitkey, qryed_quitkey;
char *qryed_menukeyname, *qryed_helpkeyname, *qryed_exitkeyname;
char *qryed_quitkeyname;
char *qryed_progname;

#if !FIT_CYGWIN
extern int errno;
#else
int errno=0;
#endif

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int retstat, maxline, row, col, bufsize, off, imode, exitkey;
  int c, ecol, foreground, background, r, i, j, len;
  char *editbuf, *(*editline), *outline;
  char fspec[FIT_FSPECLEN+1], message[FIT_FSPECLEN+64], *varptr; 
  FILE *in, *out;

  /* Set program name */
  qryed_progname = *argv;

  if (!--argc) {
    fprintf(stderr, "%s aborted, no file specified on command line",
	*argv);
    exit(0);
  }
  strncpy(fspec, *(++argv), FIT_FSPECLEN);
  fspec[FIT_FSPECLEN] = 0;

  /* initialize curses (screen handling functions) */
  if (!initscr()) {
    retstat = errno;
    perror("Error from initscr");
    exit(retstat);
  }
#if !FIT_SUN && !FIT_COHERENT && !FIT_HP
  qryed_usecolor = (start_color() != ERR);
#else
  qryed_usecolor = 0;
#endif
#if !FIT_COHERENT
  if ((retstat = noecho()) == ERR) {
    endwin();
    perror("Error from noecho()");
    goto ERREXIT;
  }
  if ((retstat = intrflush(stdscr, FALSE)) == ERR) {
    endwin();
    perror("Error from intrflush()");
    goto ERREXIT;
  }
  if ((retstat = keypad(stdscr, TRUE)) == ERR) {
    endwin();
    perror("Error from keypad()");
    goto ERREXIT;
  }
  if ((retstat = raw()) == ERR) {
    endwin();
    perror("Error from raw()");
    goto ERREXIT;
  }
#else
  noecho();
  keypad(stdscr, TRUE);
  raw();
#endif

  /* Set up signal handlers */
  signal(SIGTERM, SIG_IGN);
  signal(SIGINT, SIG_IGN);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGPIPE, SIG_IGN);
  signal(SIGQUIT, SIG_IGN);
#endif

  /* Set display attributes */
  tips_getattributes(&qryed_usecolor, &qryed_usepointer, &qryed_usecheckmarks,
    &qryed_usehotkeys, &qryed_uselinedrawing, &qryed_dispnorm, 
    &qryed_disphigh, &qryed_dispsel, &qryed_disphot, &qryed_mennorm, 
    &qryed_menhigh, &qryed_mensel, &qryed_menhot);

  /* Get global function keys */
  tips_getfunckeys(&qryed_menukey, &qryed_menukeyname, &qryed_helpkey,
    &qryed_helpkeyname, &qryed_exitkey, &qryed_exitkeyname, &qryed_quitkey, 
    &qryed_quitkeyname);

  /* Initialize windows */
  /* Label window */
  qryed_labelwin = newwin(1, QRYED_LABELWINLEN+1, 0, 
    COLS - (QRYED_LABELWINLEN+1));
  scrollok(qryed_labelwin, FALSE);
  wattrset(qryed_labelwin, qryed_mennorm);
  win_center(qryed_labelwin, 0, "Query Edit");
  mvwaddstr(qryed_labelwin, 0, 0, "|");

  /* Command window and menu */
  qryed_cmdwin = newwin(1, COLS-(QRYED_LABELWINLEN+1), 0, 0);
  scrollok(qryed_cmdwin, FALSE);
/***********
  qryed_cmdmenu = men_init(qryed_cmdwin,  MEN_ATTR_HORIZONTAL + 
    MEN_ATTR_HORIZONTALWRAP, qry_edcmdopts, qryed_mennorm,
    qryed_menhigh, qryed_menhigh, qryed_menhigh, ACS_RARROW, ACS_DIAMOND, 1, 0);
************/

  wattrset(qryed_cmdwin, qryed_mennorm);
  win_fill(qryed_cmdwin, 0, 0, 0, 0, ' ');
  wrefresh(qryed_cmdwin);
  wrefresh(qryed_labelwin);

  /* Pull down, File menu */
/*****************
  qryed_filewin = newwin(7, 11, 1, qryed_cmdmenu->scol);
  scrollok(qryed_filewin, FALSE);
  qryed_filemenu = men_init(qryed_filewin, MEN_ATTR_BOXED + MEN_ATTR_HOTKEYS +
	MEN_ATTR_INSTANT, fileopts, qryed_mennorm, qryed_menhigh, qryed_menhigh,
	qryed_menhigh, ACS_RARROW, ACS_DIAMOND, 1, 0);
  men_overlay(qryed_filemenu, qryed_cmdmenu, 0, 0);
******************/

  /* Edit/Display window */
  qryed_dispwin = newwin(LINES-3, COLS, 1, 0);
  keypad(qryed_dispwin, TRUE);
  scrollok(qryed_dispwin, FALSE);
  wattrset(qryed_dispwin, qryed_dispnorm);
  win_fill(qryed_dispwin, 0, 0, 0, 0, ' ');
  wrefresh(qryed_dispwin);

  /* Footer window */
  qryed_footerwin = newwin(1, COLS, LINES - 2, 0);
  scrollok(qryed_footerwin, FALSE);
  wattrset(qryed_footerwin, qryed_mennorm);
  win_fill(qryed_footerwin, 0, 0, 0, 0, ' ');
  win_center(qryed_footerwin, 0, fspec);
  wrefresh(qryed_footerwin);

  /* Help Line */
  qryed_helpwin = newwin(1, COLS, LINES - 1, 0);
  scrollok(qryed_helpwin, FALSE);
  wattrset(qryed_helpwin, qryed_dispnorm);
  win_fill(qryed_helpwin, 0, 0, 0, 0, ' ');
  sprintf(message,"Use %s to exit with save, %s to quit, %s for help",
     qryed_exitkeyname, qryed_quitkeyname, qryed_helpkeyname);
  win_center(qryed_helpwin, 0, message);
  wrefresh(qryed_helpwin);

  /* Initialize the query buffer */
  maxline = LINES - 3;
  bufsize = maxline * COLS;
  editbuf = (char *) malloc(bufsize + 1);
  fit_sblank(editbuf, bufsize);
  editbuf[bufsize] = 0;
  editline = (char *(*)) calloc(maxline, sizeof(*editline));
  for (row=0; row < maxline; ++row)
    editline[row] = editbuf + row * COLS;

  fit_expfspec(fspec, ".qry", (char *) NULL, fspec);

  if ((in = fopen(fspec, "r"))) {
    for (row=0; !feof(in) && row < maxline; ++row) {
      for (col=0; !feof(in) && col < COLS; ++col) {
	editline[row][col] = fgetc(in);
	switch (editline[row][col]) {
	  case '\n' : case '\r' : 
	    editline[row][col] = ' ';
	    col = COLS;
	    while (!feof(in) && (c = fgetc(in)) == '\n' || c == '\r')
	      ;
	    ungetc(c, in);
	    break;
	  default :
	    if (editline[row][col] < ' ' || editline[row][col] > '~')
	      editline[row][col] = ' ';
	}
      }
    }
    if (!feof(in))
      qryed_message("Query is too big, truncated");
    fclose(in);
  }

  /* Draw screen */
  outline = calloc(COLS+1, sizeof(*outline));
  outline[COLS] = 0;
  for (row=0; row < maxline; ++row) {
    strncpy(outline, editline[row], COLS);
    mvwaddstr(qryed_dispwin, row, 0, outline);
  }
  wrefresh(qryed_dispwin);

  /* While not exited */
  off = 0;
  imode = 0;
  col = 0;
  row = 0;
  exitkey = 0;
  while (1) {
    while (1) {
      row = max(0,min(row, maxline-1));
      col = max(0,min(col, COLS-1));
      wmove(qryed_dispwin, row, col);
      wrefresh(qryed_dispwin);
      off = col;
      strncpy(outline, editline[row], COLS);
      outline[COLS] = 0;
      win_edit(qryed_dispwin, outline, COLS, &imode, 0, 0, row, 0, &off,
	&exitkey);
      strncpy(editline[row], outline, COLS);
      col = off; 

      switch (exitkey) {
        case '\n' : case '\r' :
	  if (imode) {
	    if (row == maxline-1 || strspn(editline[maxline-1], " ") < COLS)
	      qryed_message("Cannot Insert line, edit buffer is full");
	    else {
	      for (r=maxline-1; r > row+1; --r) {
		strncpy(editline[r], editline[r-1], COLS);
		strncpy(outline, editline[r], COLS);
		mvwaddstr(qryed_dispwin, r, 0, outline);
	      }
	      fit_sblank(outline, COLS);
	      strncpy(outline, editline[row]+col, COLS - col);
	      strncpy(editline[row+1], outline, COLS);
	      mvwaddstr(qryed_dispwin, row+1, 0, outline);
	      strncpy(outline, editline[row], COLS);
	      fit_sblank(outline+col, COLS-col);
	      strncpy(editline[row], outline, COLS);
	      mvwaddstr(qryed_dispwin, row, 0, outline);
	      col = 0;
	      row = row + 1;
	    }
	  }
	  else {
	    col = 0;
	    row = row + 1;
	  }
	  break;
	case KEY_UP : 
	case KEY_DOWN :
	  if (exitkey == KEY_UP)
	    row = max(0, row - 1);
	  else
	    row = min(row+1, maxline - 1);
	  ecol = COLS;
	  fit_strim(outline, &ecol);
	  strncpy(outline, editline[row], COLS);
	  if (col >= ecol && ecol) {
	    col = COLS;
	    fit_strim(outline, &col);
	  }
	  else {
	    ecol = COLS;
	    fit_strim(outline, &ecol);
	    col = min(ecol, col);
	  }
	  col = min(col, COLS-1);
	  break;
	case KEY_HOME :
#ifdef KEY_SHOME
	case KEY_SHOME :
#endif
	case KEY_PPAGE :
	  row = 0;
	  col = 0;
	  break;
#ifdef KEY_END
	case KEY_END :
#endif
#ifdef KEY_SEND
	case KEY_SEND :
#endif
	case KEY_NPAGE :
	  col = bufsize;
	  fit_strim(editbuf, (unsigned *) &col);
	  editbuf[col] = ' ';
	  row = col / COLS;
	  col = (col % COLS);
	  if (col >= COLS) {
	    row = row + 1;
	    col = 0;
	  }
	  break;
#ifdef KEY_REFRESH
	case KEY_REFRESH :
#endif
        case 'w' - 'a' + 1 :
	  endwin();
	  wrefresh(qryed_dispwin);
	  break;
#ifdef KEY_EXIT 
        case  KEY_EXIT :
#endif
	case 'z' - 'a' + 1 : case 'd' - 'a' + 1 :
	  goto EXITSAVE;
	  break;
#ifdef KEY_CANCEL
	case KEY_CANCEL : case KEY_SCANCEL :
#endif
	case 'c' - 'a' + 1 :
	  goto DONE;
	  break;
	case 'l' - 'a' + 1 :
	  /* Use formfeed as a "join line" command */
	  if (row < maxline - 1) {
	    strncpy(outline, editline[row], COLS);
	    len = COLS;
	    fit_strim(outline, &len);
	    if (len < COLS - 1) {
	      outline[len++] = ' ';
	      for (i=0, j = 0; len < COLS;) {
		for (;j < COLS && editline[row+1][j] == ' '; ++j)
		  ;
		if (len + j - i >= COLS) break;
		if (i != j)
		  strncpy(outline+len, editline[row+1]+i, j - i);
		len += (j-i);
		i = j;
		for (; j < COLS && editline[row+1][j] != ' '; ++j);
		  ;
		if (len + j - i >= COLS) break;
		if (i != j)
		  strncpy(outline+len, editline[row+1]+i, j - i);
		len += (j-i);
		i = j;
	      }
	      strncpy(editline[row], outline, COLS);
	      mvwaddstr(qryed_dispwin, row, 0, outline);
	
	      if (i < COLS && strspn(editline[row+1]+i, " ") < COLS - i) {
		fit_sblank(outline, COLS);
		strncpy(outline, editline[row+1] + i, COLS - i);
		strncpy(editline[row+1], outline, COLS);
		mvwaddstr(qryed_dispwin, row+1, 0, outline);
	      }
	      else {
		for (r=row+1; r < maxline; ++r) {
		  if (r < maxline - 1)
		    strncpy(editline[r], editline[r+1], COLS);
		  else
		    fit_sblank(editline[r], COLS);
		  strncpy(outline, editline[r], COLS);
		  mvwaddstr(qryed_dispwin, r, 0, outline);
		}
	      }
	    }
	  }
	  break;
/*********
	case '\t' : case KEY_NEXT : case KEY_SNEXT :
	  goto CMDMENU;
	  break;
*********/
	default :
	  if (exitkey == qryed_exitkey)
	    goto EXITSAVE;
	  else if (exitkey == qryed_quitkey)
	    goto DONE;
	  else if (exitkey == qryed_helpkey) {
	    endwin();
	    if (!tips_help(qryed_progname)) {
	      wrefresh(qryed_dispwin);
	      qryed_message("Unable to load help file");
	    }
	  }
	  else
	    beep();
      }
    }
  }

EXITSAVE:
  if (!(out = fopen(fspec, "w"))) {
    sprintf(message, "Unable to open output file %s", fspec);
    qryed_message(message);
    tmpnam(fspec);
    fit_setext(fspec, ".qry", fspec);
    if (!(out = fopen(fspec, "w"))) {
      qryed_message("Unable to save in temp file, edit abandoned");
      goto DONE;
    }
    else {
      sprintf(message, "File will be save to: %s", fspec);
      qryed_message(message);
    }
  }

  col = bufsize;
  fit_strim(editbuf, &col);
  if (col) {
    editbuf[col] = ' ';
    maxline = col / COLS + 1;
    for (row = 0; row < maxline; ++row) {
      for (col=COLS-1; col; --col)
        if (editline[row][col] != ' ') break;
      strncpy(outline, editline[row], col+1);
      outline[col+1] = 0;
      fprintf(out, "%s\n", outline);
    }
  }
  fclose(out);
  goto DONE;

ERREXIT:
  perror("Error initializing screen handling, qryed terminated");
  c = getchar();
  exit(errno);

DONE:
  endwin();
  return errno;
}

static WINDOW *qryed_messagewin = (WINDOW *) NULL, *qryed_savemessagewin;

#if FIT_ANSI
void qryed_message(char *message)
#else
void qryed_message(message)
  char *message;
#endif
{
  if (!qryed_messagewin) {
    qryed_messagewin = newwin(2, COLS, 4, 0);
    win_dup(qryed_messagewin, &qryed_savemessagewin);
    keypad(qryed_messagewin, TRUE);
    scrollok(qryed_messagewin, FALSE);
  }

  overwrite(qryed_dispwin, qryed_savemessagewin);
  wattrset(qryed_messagewin, qryed_mennorm);
  win_fill(qryed_messagewin, 0, 0, 0, 0, ' ');
  win_center(qryed_messagewin, 0, message);
  win_center(qryed_messagewin, 1, "Hit any key to continue");
  overwrite(qryed_messagewin, qryed_dispwin);
  wrefresh(qryed_dispwin);
  if (wgetch(qryed_messagewin) == 'w' - 'a' + 1)
    endwin();
  overwrite(qryed_savemessagewin, qryed_dispwin);
  wrefresh(qryed_dispwin);
}
