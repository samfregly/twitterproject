if [ "$SPOT_ROOT" = "" ]; then
  SPOT_ROOT=`pwd`
  export SPOT_ROOT
fi
PATH=$PATH:.:$SPOT_ROOT/scripts:$SPOT_ROOT/bin
export PATH
