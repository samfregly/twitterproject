#!/usr/bin/perl

# name: search.pl
# author: Andrew M. Fregly
# function: Processes a request to enter an archive search.

require "seekitlb.pl";

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

$qryname="a$username";
chop $qryname;
$qrytype="archive";

if ($usertoken eq "") {
  &seekit_notloggedin;
}

else {
  &seekit_query($qryname, $qrytype);
}
&seekit_endpage;
