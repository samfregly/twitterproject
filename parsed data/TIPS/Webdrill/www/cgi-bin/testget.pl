#!/usr/local/bin/perl
require "seekitlb.pl";

$resultstat = &seekit_getHTTP($ARGV[0], "test.html",
  $doctype, $httpstat, $httpmsg);

print "Returned status: $resultstat\n";
print "HTTP status: $httpstat\n";
print "HTTP message: $httpmsg\n";
print "Document type: $doctype\n";

if ($resultstat =~ /Success/i) {
  &seekit_getexternalURLs($doctype, 'test.html', $baseurl, *urls);
  if (!$baseurl || $baseurl eq "") {
    $baseurl = $ARGV[0];
  }
  &seekit_parseURL($baseurl, $basesite, $baseurl, $object);

  print "Base URL: $baseurl\n";
  print "Base Site: $basesite\n";
  print "Object: $object\n";
  print "Embedded URLs..\n";
  foreach $url (keys(%urls)) {
    print "..$url - $urls{$url}\n";
  }
  &seekit_parseURL($baseurl, $basesite, $baseurl, $object);
  print "Base URL: $baseurl, Base Site: $basesite, Object: $object\n";

  # Expand URLS to their abslute references
  &seekit_uniqueURLs($urls, $baseurl, *tempurls);
  foreach $url (keys(%tempurls)) {
    $absurls{$url} = $tempurls{$url};
    $parent{$url} = $urlspec;
  }   

  print "Base URL: $baseurl\n";
  print "Embedded URLs..\n";
  foreach $url (keys(%urls)) {
    print "..$url - $urls{$url}\n";
  }
  &seekit_parseURL($baseurl, $basesite, $baseurl, $object);
  print "Base URL: $baseurl, Base Site: $basesite, Object: $object\n";

  # Expand URLS to their abslute references
  $tempurls = &seekit_uniqueURLs($urls, $baseurl);
  foreach $url (keys(%tempurls)) {
    $absurls{$url} = $tempurls{$url};
    $parent{$url} = $urlspec;
  }

  # Set types for URLS
  &seekit_setURLtypes(*absurls, $basesite);
  print "Absolute URLs..\n";
  foreach $url (keys(%absurls)) {
    print "$url: $absurls{$url}<BR>\n";
  }
}
