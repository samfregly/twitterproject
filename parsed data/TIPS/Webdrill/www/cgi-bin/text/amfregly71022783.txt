� WebDrill Java client  is on it's way
�amfregly
�19970808 02:46:23
�Computer Software
�This is a status report on the evolution of our SeekIt 
WebDrill product. We are currently in the process of
developing a Java client which will provide the following
capabilities.

  Real-time Newsgroup and BBS monitoring
  Graphical traversal of Web Sites based on URL Search
  capability.
  Multiple search agents.
  Topographic mappings of sites based on relevance rankings
  derived from your query sets.

Stay tuned here for further developments. Keep in mind that
registered purchasers of WebDrill are entitled to free 
upgrades for a period of six months after the purchase date.

AF
  
