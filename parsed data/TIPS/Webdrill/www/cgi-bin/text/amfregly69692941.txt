�Suzanne's theories on Edward the cocker spaniels mental capacities
�amfregly
�19970723 17:22:21
�Entertainment
�"That boy is about as sharp as a bowling ball" claims 
Suzanne, blatantly stealing one of the renowned philosopher 
Foghorn Leghorn's most famous observations. I have to take 
issue with this ridiculous statement. First of all, the 
"boy" is not a boy at all, he is and will be a dog for the 
rest of his days. Suzanne looked over my shoulder just now 
and gave him the guy kiss of death by calling him 
sweet. The only thing worse would be if she labeled him a 
"nice guy". And now she continues with her anti-doggie 
crusade by negatively contrasting him to her cat and making 
ever more brutal comments about his dimwitted state. 
Continuing on, Suzanne discourses about THE CAT, sometimes 
referred to by her simply as GOD, or even THE MESSIAH, but 
originally labeled with the moniker Fleetwood, claiming all 
types of extradonary abilities for him. Wisdom, knowledge, 
grace, power and beauty were present in abundance in him 
according to her. Little does Suzanne know that her 
idolizing of this cat puts Edward in a poor light in 
comparison. But he has his defenders, or maybe a defender, 
or maybe just someone who has the honor not to degrade him 
too much, but at least there is someone who is kind of in 
his corner, though it might by quite a small corner and 
a long ways away. Still, I will say that Edward is a prince 
among beasts and worthy of the finest things. Even gold, 
silver and gemstones are not presents worthy of his honor 
and virtue. With this I say that the qualities of the 
heart possessed by Edward make him the superior of just 
about any of the "Human" persuasion that you might ever 
meet. Ooops, I have to go now and take the Prince for his 
morning constitutional. 
