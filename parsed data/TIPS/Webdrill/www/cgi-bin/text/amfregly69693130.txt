� A search tip
�amfregly
�19970723 17:25:30
�Computer Software
�Hey out there. If you want to search for anything submitted 
by a particular person, use the SeekIt field specifier to 
search the author field as in the following query: 

^author (amfregly,lef) and (text, document, adaptive) 
within 20 (search*, retriev*, d/ei/ssimination, filtering) 

There are a few other fields you can search. See the 
"Submit Text" form or look at the WebDrill Help under
Query Language.
AF
