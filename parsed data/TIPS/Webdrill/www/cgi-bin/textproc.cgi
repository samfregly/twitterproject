#!/usr/bin/perl
require "seekitlb.pl";
require "mailer.pl";

# name: textproc.pl
# author: Andrew M. Fregly
# function: Processes the data received from the text submission form of the
# SeekIt Web Demo. The data is written to a file in a user specific directory
# residing in the html document tree. The title and body data are delimited by
# field marks so that field searching can be enabled. The title is field 0, and
# the body is field 1. Once the datafile is written, the active agents are
# searched agains it and the hit files for each matching agent are updated.
# The main menu for the demo system is then output so that the user can perform
# another function.

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

$s = $seekit_dirsep;
$x = $seekit_exe;
$searchdir=$dataroot.$s."searches";
$datadir=$submitroot;

if ($usertoken eq "") {
  &seekit_notloggedin;
}
else {
  if (! -d "$datadir") {
    mkdir($datadir,0644);
  }
  $title=$contents{'Title'};
  $subject=$contents{'Subject'};
  $body=$contents{'Body'};
  $textfilename=$datadir.$s.$username.substr(time,1,8).".txt";

  if (! open(TEXTFILE, ">$textfilename")) {
    print <<"HTML";
<P>An error occurred in storing your text. Please notify 
<EM>SeekIt Inc.</EM> via <A HREF=\"mailto:$seekit_administrator\">e-mail</A> with
a description of the problem.
</P>
HTML
    &seekit_endpage;
  }
  else {
    &seekit_userlog($username,"submitted BBS item $textfilename");
    $titlefldmark=pack("CC",255,0);
    $bodyfldmark=pack("CC",255,1);
    $subjectfldmark=pack("CC",255,2);
    $authorfldmark=pack("CC",255,3);
    $datefldmark=pack("CC",255,4);
    $datestamp = &seekit_timestamp;

    print TEXTFILE "$titlefldmark$title\n";
    print TEXTFILE "$authorfldmark$username\n";
    print TEXTFILE "$datefldmark$datestamp\n";
    print TEXTFILE "$subjectfldmark$subject\n" unless (! $subject);
    print TEXTFILE "$bodyfldmark$body\n";
    close TEXTFILE;

    $hname=$username;
    chop ($hname);
    $hname="_".$hname;
    $cmd = $tipsroot.$s."afind".$x." -f ".$textfilename." -l ".$searchdir.
      $s."webdemo.qlb -h ".$searchdir.$s.$hname.
      ".hit -s 1>>$seekit_nulldev 2>>$seekit_nulldev"; 
    $statusmsg = `$cmd`; 
    if (open (INFILE, $searchdir.$s.$hname.".hit")) {
      if (open(OUT, ">$$.tmp")) {
        print OUT "Title - $title\n";
        print OUT "Author - $username, Date - $datestamp\n";
        print (OUT "Subject - $subject\n") unless (! $subject);
        print OUT "Body - $body\n";
        close(OUT);
        $nhits = 0;
        $nmail = 0;
        while ($hit=<INFILE>) {
          ($docpath, $doclen, $queryname) = split(/,/,$hit);
	  chop($queryname);
	  $recipient = $queryname;
	  $recipient =~ s/.*[\\\/]//;
	  $recipient =~ s/.([^\.]*).*/$1/i;
	  $recipient =~ tr/A-Z/a-z/;
	  chop($recipient);
	  $recipient .= '*';
	  $recipient = `ls users/$recipient`;
	  $recipient =~ s/.*[\\\/]//;
          &seekit_getuserinfo($recipient, $dataroot, *userinfo);
          if ($userinfo{'E-Mail'} && $userinfo{'E-Mail'} ne "") {
	    $status = "";
	    &seekit_highlite($queryname, "$$.tmp", "$username.xxx");
	    if (open(IN, "$username.xxx")) {
	      if (defined(@mail)) { undef(@mail); }
	      for (<IN>) {
	        push(@mail, $_);
 	      }
	      close(IN);
	      unlink("$username.xxx");
              $status = &seekit_smtpmailer($seekit_smtpgateway,
		$userinfo{'E-Mail'},
		"SeekWare WebTips Demo - Search Result", *mail, "","","");
	      if (!($status =~ /Success/i)) {
	        print "Error mailling hit: $status<BR>\n";
	      }
	      else {
	        $nmail += 1;
	      }
            }
	    else {
	      print "<H2>Error highlighting hits for user: $username</H2><P>\n";
	    }
	  }
	  $nhits += 1;
        }
        close INFILE;
        if ($nhits) {
  	  print "Matched: $nhits, Mailed: $nmail<BR>\n";
	  if ($seekit_unix) {
            $cmd = $tipsroot.$s."hitw".$x." -h <".$searchdir.$s.$hname.
	      ".hit 2>".$seekit_nulldev;
	  }
	  else {
            $cmd = $tipsroot.$s."hitw".$x." -h -f ".$searchdir.$s.$hname.
	      ".hit 2>".$seekit_nulldev;
	  }
          $hitstat = `$cmd`;
        } 
        else {
  	  print "Your submission did not match any persistent searches<BR>\n";
        }
	unlink("$$.tmp");
      }
      else {
	print "Error opening work file<BR>\n";
      }
      &seekit_endpage;
    }
    else {
      print "<P>*** Error retrieving search results<BR>\n";
      &seekit_endpage;
    }
  }
}
