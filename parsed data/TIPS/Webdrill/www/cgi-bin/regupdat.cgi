#!/usr/bin/perl
require "seekitlb.pl";

# name: regupdat.pl
# author: Andrew M. Fregly
# function: allows user to update their Seekit Web Demo registration 
# information. The user current registration data is loaded into a form
# and the user is allowed to update it.

# HTTP content type header.
&htmlheader;

# Get contents of registration form.
&gethttpdata;

# Get roots for docs, scripts and data.
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

if ($usertoken eq "") {
  &seekit_notloggedin;
}

else {
  &seekit_getuserinfo($username, $dataroot, *uinfo);
  print <<"HTML";
<H1 ALIGN=CENTER>Update Registration for <EM>$username</EM></H1>
<HR>
<FORM method="Post" action="$cgiroot/regack.$seekit_perlext">
<P><INPUT TYPE="TEXT" NAME="Salutation" SIZE="20" VALUE="$uinfo{'Salutation'}"> Salutation</P>
<P><INPUT TYPE="TEXT" NAME="First_Name" SIZE="20" VALUE="$uinfo{'First_Name'}"> First Name</P>
<P><INPUT TYPE="TEXT" NAME="Middle_Name" SIZE="20" VALUE="$uinfo{'Middle_Name'}"> Middle Name</P>
<P><INPUT TYPE="TEXT" NAME="Last_Name" SIZE="20" VALUE="$uinfo{'Last_Name'}"> Last Name</P>
<P>Address:</P>
<P>
<TEXTAREA ROWS="5" COLS="40" NAME="Address">$uinfo{'Address'}</TEXTAREA></P>
<P><INPUT TYPE="TEXT" NAME="Organization" SIZE="40" VALUE=$uinfo{'Organization'}> Organization</P>
<P><INPUT TYPE="TEXT" NAME="E-Mail" SIZE="40" VALUE=$uinfo{'E-Mail'}> E-Mail</P>
<P><INPUT TYPE="TEXT" NAME="Phone" SIZE="30" VALUE=$uinfo{'Phone'}> Phone</P>
<P><INPUT TYPE="PASSWORD" NAME="Password" SIZE="8"> Update Password</P>
<P><INPUT TYPE="PASSWORD" NAME="Verify_Password" SIZE="8"> Verify Updated Password
<INPUT TYPE="HIDDEN" NAME="Username" VALUE="$username">
<INPUT TYPE="HIDDEN" NAME="Token" VALUE="$usertoken">
<INPUT TYPE="HIDDEN" NAME="Mode" VALUE="update"></P>
<P><Input type="submit" Name="Register" value="Update registration">
<INPUT TYPE="RESET" NAME="Reset" value="Clear Form"></P></FORM>
HTML
}
&seekit_endpage;
