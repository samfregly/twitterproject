#!/usr/bin/perl
require "seekitlb.pl";

# name: results.pl
# author: Andrew M. Fregly
# function: Processes the hit file for the user, dynamically creating an
# html document with links to each matching document.

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

if ($usertoken eq "") {
  &seekit_notloggedin;
}
else {
  $qryname= "p".$username;
  chop($qryname);
  $qrytype="agent";
  &seekit_userlog($username,"viewed $qrytype results for query $qryname");
  &seekit_results($qryname, $qrytype); 
  &seekit_endpage("Search Agent", "$cgiroot/profile.$seekit_perlext?Username=$username&Token=$usertoken");
}
