#!/usr/bin/perl

($sec, $min, $hour, $mday, $mon, $year) = localtime(time);
$sec =~ s/\A(.)\Z/0$1/;
$min =~ s/\A(.)\Z/0$1/;
$hour =~ s/\A(.)\Z/0$1/;
$mday =~ s/\A(.)\Z/0$1/;
$mon =~ s/\A(.)\Z/0$1/;
$year =~ s/\A(.)\Z/0$1/;
$logmsg = "19$year$mon$mday $hour:$min:$sec - $ENV{'REMOTE_ADDR'}, $ENV{'REMOTE_HOST'}, $ENV{'REMOTE_USER'}\n";
open (OUT, ">>logs/homepage.log");
print OUT $logmsg;
close OUT;

($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size) = stat("logo.gif");
print "Content-type: image/jpeg\r\n";
print "Content-length: $size\r\n\r\n";
open(IN, "logo.gif");
binmode(IN);
read(IN, $logo, $size);
close(IN);
print $logo;
