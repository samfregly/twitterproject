#!/usr/bin/perl
require "seekitlb.pl";

# name:	demomenu.pl
# author: Andrew Fregly
# function: Creates main menu for SeekIt WEB demo. Checks user login token
# before allowing this.


&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

if ($usertoken eq "") {
  &seekit_notloggedin;
}
else {
  &demomain($username, $usertoken);
}
