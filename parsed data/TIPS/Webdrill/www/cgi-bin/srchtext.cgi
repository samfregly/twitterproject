#!/usr/bin/perl
require "seekitlb.pl";

# name: srchtext.pl
# author: Andrew M. Fregly
# function: This function dynamically presents a web page which allows the
# user to submit title and body text for profiling by the SeekIt Web Demo
# system.

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

print "<H1><EM>WebTips</EM> Demo - Item Submission Form</H1><HR>\n";

if ($usertoken eq "") {
  &seekit_notloggedin;
}
else {
  print <<"HTML";
<FORM METHOD=POST action=\"$cgiroot/textproc.$seekit_perlext\">
<P>Title:<BR><INPUT TYPE=\"TEXT\" NAME=\"Title\" SIZE=\"60\"></P>
<P>Subject: <select name="Subject" multiple>
<option selected>     (None)     
<option>Automobiles
<option>Business News
<option>Computer Hardware
<option>Computer Software
<option>Computer Networks
<option>Entertainment
<option>Family
<option>Health
<option>Investing
<option>Leisure
<option>Motorcycles
<option>Sports
<option>Travel
</select>
<P>Body:<BR>
<TEXTAREA ROWS=\"10\" COLS=\"60\" NAME=\"Body\"></TEXTAREA>
</P>
<INPUT TYPE=\"HIDDEN\" NAME=\"Username\" VALUE=\"$username\">
<INPUT TYPE=\"HIDDEN\" NAME=\"Token\" VALUE=\"$usertoken\">
<P>Do you need <A HREF=\"/seekware/webdemo/texthelp.htm\">help</A>
<P><P><TABLE width="100%"><TR><TH>
<Input type=\"SUBMIT\" Name=\"Submit\" value=\"Submit Text\">
<INPUT TYPE=\"RESET\" NAME=\"Reset\" value=\"Clear Form\">
</TH></TR></TABLE>
</P></FORM>
HTML
  &seekit_endpage;
}
