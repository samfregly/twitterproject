#!/usr/bin/perl
#
# Author: A. Fregly             June 13, 1997 
#
# Function: Highlight
#
# Function: Finds matches for terms from a TIPS query in the specified input
#       file. The file is printed to standard output with the highlighted terms
#       encompassed in double angle brackets.
#
# Calling Sequence:
#       highlight query_file text_file
#
# Notes:
#       If running from DOS, set the $isDOS variable to 1 so that handling
#       of newline characters will be taken care of correctly.
#

require "seekitlb.pl"

&seekit_highlite($ARGV[0], $ARGV[1], "highlite.out");
print `cat highlite.out`;
unlink "highlite.out";
