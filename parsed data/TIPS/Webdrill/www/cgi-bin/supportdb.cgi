#!/usr/bin/perl
require "seekitlb.pl";
require "mailer.pl";

# name: supportdb.pl
# author: Andrew M. Fregly
# function: Handles receipt of a support request form, storing it in the
# supportdb/text directory and e-mailing a copy of it to myself.

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

$s = $seekit_dirsep;
$x = $seekit_exe;
$searchdir="$dataroot/supportdb/searches";
$datadir="$dataroot/supportdb/text";

if (! -d "$dataroot/supportdb") {
  mkdir("$dataroot/supportdb", 0644);
}
if (! -d "$searchdir") {
  mkdir($searchdir,0644);
}
if (! -d "$datadir") {
  mkdir($datadir,0644);
}
$textfilename=$datadir."/rqst".substr(time,1,8).".txt";

if (! open(TEXTFILE, ">$textfilename")) {
  print <<"HTML";
<P>An error occurred in storing your request. Please notify 
<EM>SeekIt Inc.</EM> via <A HREF=\"mailto:amfregly@erols.com\">e-mail</A> with
a description of the problem.
</P>
</BODY>
</HTML>
HTML
}
else {
  print "<P>Updating Support Database<BR>\n";
  &seekit_loadfields("$searchdir/supportdb.fld", $nfields, *fieldname, *fieldtext);
  $msg = "";
  $i = 0;
  while ($i < $nfields) {
    $fldmark = pack("CC", 255, $i); 
    if ($fieldname[$i] ne "Timestamp") {
      $line = "$fldmark$fieldtext[$i]$contents{$fieldname[$i]}\n";
      $msgline = "$fieldtext[$i]$contents{$fieldname[$i]}\n"; 
      push(@msg, $msgline);
    }
    else {
      $timestamp = &seekit_timestamp;
      $line = "$fldmark$fieldtext[$i]$contents{$fieldname[$i]} $timestamp\n";
      $msgline = "$fieldtext[$i]$contents{$fieldname[$i]} $timestamp\n";
      push(@msg, $msgline);
    }
    print TEXTFILE $line;
    $i += 1;
  }
  close TEXTFILE;
  print "<P>Notifying $seekit_administrator</P>\n";
  if (&seekit_smtpmailer($seekit_smtpgateway, "amfregly@erols.com", 
	"SeekWare Support Request", *msg, "","","") =~ /Success/i) {
    print "<H2>Request was submitted</H2></BODY></HTML>\n";
  }
  else {
    print "<H2>Error mailing request to $seekit_administrator</H2></BODY></HTML>\n";
  }
}
