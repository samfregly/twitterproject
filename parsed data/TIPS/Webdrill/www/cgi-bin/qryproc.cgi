#!/usr/bin/perl

# name: qryproc.pl
# author: Andrew M. Fregly
# function: Processes a query submit request. The query is compiled and checked
# for syntax errors. Valid agent queries are added to the Seekit web demo
# query library and the user is placed back into the main menu of the demo.
  
require "seekitlb.pl";

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

$searchdir = $dataroot.$seekit_dirsep."searches";

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

if ($usertoken eq "") {
  &seekit_notloggedin;
}

else {
  $username = $contents{'Username'};
  $querytext=$contents{'Query'};
  $qryname=$contents{'Queryname'};
  $qrytype=$contents{'Querytype'};
  &seekit_userlog($username, "$qrytype query $qryname $contents{'URL'}");
  $s = $seekit_dirsep;
  $x = $seekit_exe;
  $searchspec = $searchdir.$s.$qryname;

  if (! open(QUERYFILE, ">$searchspec.qry")) {
    print <<"HTML";
<P>An error occurred in updating your saved query. Please notify 
<EM>SeekIt Inc.</EM> via <A HREF=\"mailto:$seekit_administrator\">e-mail</A> with
a description of the problem.
</P>
HTML
    &seekit_endpage;
  }
  else {
    print QUERYFILE $querytext;
    close QUERYFILE;
    unlink "$searchspec.err";
    if ( $qrytype eq 'agent' ) { 
      $cmd = $tipsroot.$s."qcompile".$x." -o ".$searchdir.
	$s."webdemo.qlb ".$searchspec.".qry";
    }
    else {
      $cmd =$tipsroot.$s."qcompile".$x." ".$searchspec.".qry";
    }
    $statusmsg = `$cmd`;
    if (-f "$searchspec.err") {
      print "<P><EM>There were syntax errors in the query.</EM><BR></P>";
      if (open(QRYERRS, "$searchspec.err")) {
	print "<P><EM>There were syntax errors in the query...</EM><BR></P>\n";
	while (<QRYERRS>) {
	  $htmltext=$_;
	  &makehtmltext($htmltext);
	  print "<P></EM>$htmltext</EM></P>\n";
	}
      }
      close QRYERRS;
    }

    elsif ($qrytype eq 'agent') {
      print "<P><EM>Query updated successfully</EM></P>\n";
      &seekit_endpage;
    }

    elsif ($qrytype eq 'archive') {
      $cmd = $tipsroot.$s."afind".$x." -q ".$searchspec.".qry -n ".$searchdir.
	$s."webdemo.fld -h ".$searchspec.".hit -f ".$submitroot.$s.'*.txt'.
	" 2>$seekit_nulldev";
      $statusmsg = `$cmd`;
      &seekit_results($qryname, $qrytype);
      &seekit_endpage("Archive Search", "$cgiroot/search.$seekit_perlext?Username=$username&Token=$usertoken");
    }

    elsif ($qrytype eq 'URL') {
      &seekit_linkmenu('Submit URL', "$cgiroot/srchurl.$seekit_perlext?Username=$username&Token=$usertoken");

      if ($contents{'URL'}) {

	# The form has an option to search the HTML markup elements. If so,
	# we will remember that and not strip out HTML markup and expand
	# HTML quoted characters when the search is performed.
	$domarkup = 0;
	$domarkup = 1 unless ($contents{'DoMarkup'} ne 'on');
	$showall = 0;
	$showall = 1 unless ($contents{'ShowAll'} ne 'on');

	# $fnum will be incremented sequentially for generating file names
	# to use for urls which are pulled and stored locally.
	$fnum = substr(time,1,8);

	# Set the file names for the next url to pull
	$basename = $submitroot.$s.$username.$s.$fnum;
        if (! -d $submitroot.$s.$username) {
          mkdir($submitroot.$s.$username, 0777);
        }
	$urlfilename = $basename.".tmp";
	
	# Determine the absolute URL for the starting URL. The site defaults
	# to the current site if there is not site in the URL.
        if ($contents{'URL'} =~ /\A\w+:\/\//) {
	  $origurl = $contents{'URL'};
        }
        else {
          $origurl = "http://".$contents{'URL'};
        }
	
	$urlspec = &seekit_absURL($origurl, $docroot);
	print  "<H2>Searching URL: $urlspec</H2><P>\n";

	# Copy the contents of the URL to a local file
	$resultstat = &seekit_getHTTP($urlspec, $urlfilename, $doctype, $httpstat,
	   $httpmsg);

	if ($resultstat =~ /Success/i) {
	  # Copy was successfull, search it and add results to output.
	  &dofile($domarkup, $showall, $doctype, $searchspec, $basename, $urlspec); 
	
	  # Pull the urls out of the file.
	  &seekit_getexternalURLs($doctype, $urlfilename, $baseurl, *urls);
	  #foreach $url (keys(%urls)) {
	  #  print "$url: $urls{$url}<BR>\n";
          #}

	  # Clean up the temp file.
	  &seekit_fdelete("$basename.*"); 

	  # Get the base URL for the URL we just searched. Previous call
	  # to seekit_getexternal URLs will have filled in $baseurl if
	  # there was one specified in the page. Otherwise, use the URL
	  # we searched as the source for determining the base.
	  $baseurl = $origurl unless ($baseurl ne "");
	  &seekit_parseURL($baseurl, $basesite, $baseurl, $object);
	  if ($baseurl eq "") {
	    $baseurl = $docroot;
	    &seekit_parseURL($baseurl, $basesite, $baseurl, $object);
	  }

          # Expand URLS to their abslute references
	  &seekit_uniqueURLs(*urls, $baseurl, *tempurls);
	  foreach $url (keys(%tempurls)) {
	    $absurls{$url} = $tempurls{$url};
	    #$parent{$url} = $urlspec;
	  }

	  # Set types for URLS
	  &seekit_setURLtypes(*absurls, $basesite);
	  $Frame = 1;
	  $Remote = 1;
	  $Remote = 0 unless ($contents{'RecurseLinks'} eq 'on');
	  $Local = 1;
	  $Local = 0 unless ($contents{'DrillSite'} eq 'on');
	 
	  foreach $urltype (("Frame", "Remote", "Local")) {
	    if ((($urltype eq "Frame" && $Frame) ||
		 ($urltype eq "Remote" && $Remote) ||
		 ($urltype eq "Local" && $Local)) || 
		  $urltype eq "Frame") {
	      $morenew = 1;
	      $localsfound = 0;
	      $firstone = 0;
	      while ($morenew) { 
	        $morenew = 0;
	        foreach $urlspec (keys(%absurls)) {
		  if ($absurls{$urlspec} eq $urltype) {
		    $absurls{$urlspec} = "Processed";
		    if (!$firstone) {
		      $firstone = 1;
		      print "<HR><H2>Processing $urltype links</H2><P>\n";
		    }

	            # Set name for local file for pulled URL
	            $fnum += 1;
	            $basename = $submitroot.$s.$username.$s.$fnum;

		    # Expand the URL
	            # Get a copy of the contents of the URL.
	            $resultstat = &seekit_getHTTP($urlspec, $basename.".tmp", 
	              $doctype, $httpstat, $httpmsg);

	            if ($resultstat =~ /^Success/i) {
	              # Search the file and add results to output page.
	              &dofile($domarkup, $showall, $doctype, $searchspec, $basename, 
			$urlspec);

		      if (($urltype eq "Frame" && $Local) || $urltype eq "Local") {
		        &seekit_getexternalURLs($doctype, "$basename.tmp", 
				$newbase, *newurls);
		        $newbase = $baseurl unless ($newbase ne "");
		        &seekit_parseURL($newbase, $newsite, $newbase, $newobject);
		        $newbase = $baseurl unless ($newbase ne "");
			&seekit_uniqueURLs(*newurls, $newbase, *newabsurls);

		        foreach $newurlspec (keys(%newabsurls)) {
			  &seekit_parseURL($newurlspec, $newsite, $newbase, 
			    $newobject);
			  if ($newsite eq $basesite) {
			    if (!$absurls{$newurlspec}) {
			      $morenew = 1;
			      $absurls{$newurlspec} = "Local";
			      #$parent{$newurlspec} = $urlspec;
			    }
			  }
		        }
		      }
	            }

	            elsif ($contents{'ShowErrs'}) {
	              print "<P>Retrieval error: $urlspec<BR>..$resultstat<BR>";
	              print "...Doctype: $doctype, HTTP Transfer Status: $httpstat,$httpmsg<BR>\n";
	              print "</P>\n";
	            }

	            # Clean up the temp files.
	            &seekit_fdelete("$basename.*");
	          } 
	        }
	      }
	    }
	  }
	  #print "<HR><H2>Linkages</H2><P>\n";
	  #foreach $url (keys(%parent)) {
	  #  print "$parent{$url} --> $url<BR>";
	  #}
	  &seekit_endpage('Submit URL for Search', "$cgiroot/srchurl.$seekit_perlext?Username=$username&Token=$usertoken");
	}

	else {  
	  print "<P>Retrieval error: $urlspec<BR>..$resultstat<BR>";
	  print "...Doctype: $doctype, HTTP Transfer Status: $httpstat,$httpmsg<BR>\n";
	  print "</P>\n";
	  &seekit_endpage('Submit URL for Search', "$cgiroot/srchurl.$seekit_perlext?Username=$username&Token=$usertoken");
	}
      }

      else {
	print "<P>You must specify a URL</P>\n";
	&seekit_endpage('Submit URL for Search', "$cgiroot/srchurl.$seekit_perlext?Username=$username&Token=$usertoken");
      }
    }

    else {
      print "<P>Invalid query submission form: <EM>bad query type</EM></P>\n";
      &seekit_endpage('Submit URL for Search', "$cgiroot/srchurl.$seekit_perlext?Username=$username&Token=$usertoken");
    }
  }
}

sub dofile {
  # $_[0] true or false. true means html markup is searchable.
  # $_[1] true or false. true means show all documents, even ones which
  #	  don't match.
  # $_[2] is the document type, probably one of "text/html" or "text/plain"
  # $_[3] is the base query name to use in the search. ".qry" will be appended
  #       to it to form the full query name.
  # $_[4] is the base name for the file to search. The file "$basename.tmp"
  #       should exist. the file "$basename.txt" will be created and searched
  #       if the .tmp is html and html markup search is turned off.
  # $_[5] is the url to use for linking to the file. 

  local ($domarkup, $showall, $doctype, $searchspec, $basename, $urlspec,
	$process_spec, $relevance, $title);

  $domarkup = $_[0];
  $showall = $_[1];
  $doctype = $_[2];
  $searchspec = $_[3];
  $basename = $_[4];
  $urlspec = $_[5];

  if (!$domarkup && $doctype =~ /html/i) {
    $doctype = 'text/plain';
    $process_spec = $basename.".txt";
    &seekit_htmltotext("$basename.tmp", ">$process_spec");
  }
  else {
    $process_spec = $basename.".tmp";
  }
  &seekit_searchfile($process_spec, $doctype, $searchspec.".qry", $relevance, 
	$title);
  if ($relevance > 0 || $showall) {
    print "<A HREF=\"$urlspec\">View</A>";
    print "<strong> $relevance </strong>";
    $title = "No title" unless ($title);
    print "$title<BR>\n";
  }
}

