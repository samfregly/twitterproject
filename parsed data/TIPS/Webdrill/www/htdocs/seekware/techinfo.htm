<!DOCTYPE HTML PUBLIC "-//SQ//DTD HTML 2.0 + all extensions//EN" "hmpro3.dtd">
<HTML>
<HEAD>
<TITLE>SeekWare Inc. - Mechanisms for Eletronic Document Distribution</TITLE></HEAD>
<BODY background="/seekware/images/back.jpg" bgcolor="white"
 TEXT="black" LINK="navy" VLINK="maroon" ALINK="red">
<H1 ALIGN="CENTER">Mechanisms for Electronic Document Distribution</H1>
<H2 ALIGN="CENTER">By <EM>Andrew M. Fregly</EM></H2>
<P>
<TABLE width="100%"><TR><TH><IMG SRC="/seekware/images/div.gif"></TH></TR></TABLE>
</P>
<H2>Abstract</H2>
<P>The purpose of this paper is to discuss the basic approaches for electronic
document distribution and the mechanisms by which they are automated. The reader
will be introduced to the basic concepts of posting (pull based systems), and
address based distribution (push based systems). The paper will end with a brief
<A HREF="#ourapproach">description of the unique approach</A> being pioneered
by SeekWare Inc. to solve one of the major outstanding problems in electronic
document distribution. </P>
<H2>Distribution Mechanisms</H2>
<P>Historically there have been two major tasks involved in the distribution of
documents. The first task was delivery of the documents to those who need to see
them. The second was the processing of the documents by those receiving them so
that they could make use of the information contained in them. Over time,
document distribution schemes have become varied, with mechanisms tailored as
appropriate for the needs of both the distributors and recipients. Unfortunately
electronic information processing tools have not evolved as necessary for
optimized delivery of documents. This problem is particularly apparent in
situations where timely distribution of the documents is required so that
appropriate actions may be taken based on their content. </P>
<P>Document distribution mechanisms tend to support two approaches for
selection of recipients, address based distribution (sometimes referred to as &quot;push
technology&quot;), and posting (sometimes referred to as &quot;pull technology&quot;).
In address based distribution, documents are sent to specific destinations which
are predetermined by the sender of the documents. E-mail is an example of
address based distribution. Posting consists of placing information in one or
more publicly accessible locations where interested parties may access it as
desired. Web pages are an example of posting based distribution. </P>
<H2>Use of Address Based Distribution</H2>
<P>Address based distribution tends to be used in two circumstances.
Distribution of information whose knowledge is to be restricted to a known group
of recipients, referred to here as &quot;private information&quot;, is almost
always performed with an address based distribution scheme. E-mail used for
ongoing correspondence between people in an organization is an example of this
circumstance. Address based distribution is also often used for &quot;mass
distribution&quot; of public information to a large population. When mass
distribution schemes are used, the sender of the information typically does not
know which specific recipients may or may not be interested in the information
being distributed. For the sake of efficiency, mass distribution mechanisms
often group many items of unrelated information together for simultaneous
address based distribution to a number of recipients. Bulk promotional E-mail
and news wires are examples of mass distribution schemes for public information.
</P>
<P>While address based distribution is a very efficient means of distributing
private information, it is not always appropriate for distribution of public
information. An address based distribution scheme is cost effective only if the
value of the information to the recipients is greater than the cost of
distribution. This criterion usually restricts the usage of this mechanism to
situations where it is likely that a substantial percentage of recipients can
make use of the received information. </P>
<H2>Use of Posting</H2>
<P>In cases where address based distribution of public information does not
make sense; a &quot;posting&quot; distribution scheme is often used. Posting
provides a low cost means for distributing public information, but has the
drawback of poor exposure of the information to all that might want to see it.
Posting requires repetitive and proactive effort on the part of recipients in
order for the information to reach them in a timely fashion. Posting is also
limited in the scope of whom it can reach to those who are aware of the posting
location and have the capability to access it. Web pages and bulletin boards are
examples of posting distribution schemes. The principal advantages of posting
are lower cost and potentially broader exposure than address based distribution.
The drawbacks of posting should relegate its use to distribution of non-critical
information. Unfortunately, posting is used in many situations where address
based distribution would be preferable due to cost and difficulties involved in
determining who would be interested in a particular piece of information. </P>
<H2>The Problem of Information Overload</H2>
<P>The cost and exposure problems of both address-based distribution and
posting have limited the effectiveness of both mechanisms. The problem is
growing steadily as the volume of publicly distributed information increases.
Recipients are subject to information overload as this volume far exceeds the
recipient's information processing capacities. This overload results in
increasing difficulty for individuals and organizations to keep abreast of the
pertinent publicly distributed information embedded within the mass with which
they are bombarded. Fortunately, the majority of publicly distributed
information is now available in electronic form or may be converted to
electronic form, providing the seeds for a solution to both distribution and
processing problems. </P>
<H2>Current Mechanisms for Automating Distribution</H2>
<P>Distribution of information that is available in electronic form is being
automated by the use of computers. Sophisticated text retrieval systems, such as
those used in WEB search engines, have greatly helped in the distribution of
information by posting based mechanisms. In most cases, E-mail has proven to be
a big advance in address based distribution. What automation has not been very
effective at is the timely and automated delivery of public information to a
large percentage of potentially interested recipients. Bulk e-mail suffers from
the same problems inherent in paper based &quot;junk&quot; mail. Services that
generate up to the minute information about current news provide the typical
user with information overload. They also tend to deal only with the &quot;big
stories&quot; for a lot of different areas of interest. There is a strong need
for the development of systems which can address the problems involved in timely
distribution of public information to the proper audience. A companion need is
for systems that help with the information overload problem by filtering out
information so that recipients do not see items which they are not interested
in. </P>
<H2>A New Approach for Document Distribution</H2>
<P>The biggest hope for the solution to the problems of effective distribution
of public information is the development of systems that combine the benefits of
both posting and addressed based distribution, and which avoid their weaknesses.
Such a system would automatically distribute posted information to the
recipients who would want to see it. This address-based distribution would
select recipients based on information content matching criteria established by
the recipient. The widespread adoption of systems that support this capability
would herald a revolution in information distribution. Information would be
distributed far more efficiently and effectively than it currently is.
Distributors of information would know that it is automatically being
distributed to a large percentage of recipients who would be interested in it.
The problem of information overload for recipients would be lessened, as they
would only receive information that they have selected as pertinent to their
interests. The technological key to the effectiveness of an automated system for
this purpose is in developing processing capabilities that allow information to
be quickly and effectively categorized according to the expressed interests of
potential recipients. </P>
<H2>Text Retrieval as a Document Distribution Tool</H2>
<P>The technology which will be brought to play to facilitate automated content
based distribution will be built on derivatives of the concepts used in
currently available text retrieval systems. These systems utilize a number of
strategies to help user's locate items of interest which have been full-text
indexed by the system. Many people are already familiar with their operation due
to exposure to them via the Internet search engines and CD-ROMs that offer full
text search capabilities of their contents. Typically, these types of systems
allow the user to locate items of interest by searching for selected words or
phrases. Wild cards, Boolean logic (and, or, not) and proximity relationships
(words in the same sentence or paragraph) allow greater control of the selection
process. Advanced systems are able to analyze the user's search criteria and
search results, and automatically search for related words and topics within the
document base. </P>
<P>The current generation of text retrieval systems are designed primarily as a
solution to the information overload problems of posting based distribution
mechanisms. For reasons detailed below, the use of text search systems for
address based distribution is still in its infancy. As information overload
increases, these systems are becoming more prevalent as a means of dealing with
information overload. Currently, most of these systems are optimized for the
purpose of research of textual information that has been accumulated over time
in a posting distribution system. They do not address the problem of timely
automated distribution of the information, relying on the user to be proactive
in searching for the information. The underlying technology employed by these,
the use of full text indexes, makes it difficult for them to be applied to this
problem. This approach requires that information must be indexed prior to
distribution. This indexing process is time consuming and is usually performed
periodically at times when system usage is at a minimum, typically once a day.
Thus most text retrieval systems do not make newly received information
available for distribution until the following day or later. They are also
seldom used for the purpose of timely distribution of information due to the
delays involved in indexing the information prior to distribution.
<A NAME="ourapproach"></A></P>
<H2>The SeekWare Inc. Approach to Document Distribution</H2>
<P>SeekWare Inc. has focused on the problem of timely content based
distribution of information as it's core product area. Our approach is to apply
the search principles of text retrieval systems to this problem in a manner
optimized for real-time or near real-time processing. This is accomplished by
utilizing a very fast sequential text search engine to perform searching of
incoming textual information. The use of sequential search technology removes
the need for the overhead of indexing documents, allowing documents to be
searched and distributed as soon as they are available. This is the technology
that has been utilized within the intelligence networks for many years as the
means for automated routing of their huge volume of message traffic. With the
advent of very low priced and powerful computer systems, this once very
expensive capability is now easily provided for with workstation class
computers. </P>
<P>SeekWare Inc. is excited about being the first to offer inexpensive tools
which can be used to solve the problems of timely content based distribution of
documents. We feel strongly that this technology is the wave of the future for
dealing with the problems of information overload and efficient distribution of
information. For more information about our products, please take a look at our
<A HREF="/seekware/prodinfo.htm">products Web page</A>. </P>
<P><SMALL>Copyright Andrew M. Fregly, 1997</SMALL> </P>
</BODY></HTML>
