#!/usr/bin/perl5
require "seekitlb.pl";

# name: getdoc.pl
# Author: Andrew Fregly
# Function: Is invoked from the results summary page of the Seekit web demo.
# getdoc will retrieve the file specified in the "Doc" parameter of the
# http query. It will used the name for the "Username" specified in the http
# query as the basis for retrieving the TIPS query to use for locating words
# to highlight. The query is run against the document, and the resulting
# word match offsets are used to generate a highlighted HTML rendition of the
# document with the matches highlighted using the <EM> html markup.
 
# Write standard http and html header back to the client.
&htmlheader;

# Get httpdata fields from query portion of the URL.
&gethttpdata;

# Get base roots for file locations
&getroots($docroot, $cgiroot, $dataroot, $textroot, $tipsroot, $textref);

$s = $seekit_dirsep;
$x = $seekit_exe;

# See if valid logon is active for the user
$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);
if ($usertoken eq "") {
  &seekit_notloggedin;
}  

else {
  $qryname=$contents{'Query'};
  print "<P>\n";
  $name = $textroot.$s.$contents{'Doc'};
  print "<P><EM>$contents{'Doc'}</EM></P><P>\n";
  $cmd = $tipsroot.$s."offsets".$x." -f ".$name." -q ".$dataroot.$s.
	"searches".$s.$qryname.".qry"." -n ".$dataroot.$s."searches".$s.
	"webdemo.fld 2>".$seekit_nulldev;
  $offsets=`$cmd`;
  @pairs = split(/\n/, $offsets);
  $npairs = @pairs;

  $idx = 0;
  $i = 0;
  $linenum = 1;
  if ($npairs > 0) {
    ($off, $l) = split(/ /, $pairs[0]);
  }
  if (open (FILE, $name)) {
    while (<FILE>) {
      $linein = $_;
      if ($seekit_dos) {
        $linein =~ s/\n/\r\n/;
      }
      $nextidx = $idx + length($linein);
      if ($i < $npairs && $off < $nextidx) {
        @elements = split(//,$linein);
        $sidx = $off - $idx; 
        $adj = 0;
        while ($i < $npairs && $off < $nextidx) {
          splice(@elements, $sidx, 0, "<STRONG>");
          splice(@elements, $sidx + $l + 1, 0, "</STRONG>");
          $adj += 2;
          $i += 1;
          if ($i < $npairs) {
    	  ($off, $l) = split(/ /, $pairs[$i]);
            $sidx = ($off - $idx) + $adj;
          }
        }
        $linein = join('', @elements);
      }
      if ($linein =~ /^\xFF/) {
	$linein =~ s/\xFF\x00/<BR><EM>Title: <\/EM>/;
        $linein =~ s/\xFF\x01/<BR><EM>Body: <\/EM>/;
        $linein =~ s/\xFF\x02/<BR><EM>Subject: <\/EM>/;
        $linein =~ s/\xFF\x03/<BR><EM>Author: <\/EM>/;
        $linein =~ s/\xFF\x04/<BR><EM>Date: <\/EM>/;
      }
      else {
	$linein =~ s/^/<BR>/;
      }
      $schar="\x80";
      $echar="\xFF";
      $linein =~ tr/[\x80-\xFF]//d;
      $linein =~ tr/[\x00-\x04]//d;
      print "$linein\n";
      $idx = $nextidx;
      $linenum += 1;
    }
    close FILE;
  }
  else {
    print "Unable to open text file: <EM>", $contents{'Doc'}, "</EM>\n";
  }
  &seekit_endpage;
}
