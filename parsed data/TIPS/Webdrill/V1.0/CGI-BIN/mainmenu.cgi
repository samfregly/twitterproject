#!/usr/bin/perl5
require "seekitlb.pl";

# name: mainmenu.pl
# Author: Andrew Fregly

# Start output of the HTML document
&htmlheader;

# Create associative array of form field contents
&gethttpdata;

# Get base URLs and data locations.
&getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

&demomain($username, $usertoken);
