#!/usr/bin/perl5

# Name: seekitlb.pl
# Author: A. Fregly
# Function: seekitlb.pl is a collection of perl subroutines which are used
#   by the seekit web demo.
# Change Log:
# 000	01/97		Under development

$seekit_unix=1;
$seekit_dos=0;
$seekit_netscape=1;
$seekit_smtpgateway="mickey.netscan.com";
$seekit_smtpbye="\r\n.\r\n";  # for NT, <cr><lf><cr><lf>, for unix, .<\n>
$seekit_administrator="amfregly\@seekware.com";
$seekit_perlext="cgi";
$seekit_nosocket=0;
$seekit_hassocketdefs=1;

$seekit_timeout=3600*24;
if ($seekit_unix) {
  $seekit_dirsep="/";
  $seekit_exe="";
  $seekit_nulldev = "/dev/null";
}
else {
  $seekit_dirsep="\\";
  $seekit_exe=".exe";
  $seekit_nulldev = "nul:";
}

sub getroots {
  # $_[0] is returned with the base for html docs. 
  # $_[1] is returned with the base for cgi scripts.
  # $_[2] is returned with the base for data files. 
  # $_[3] is returned with the location for uploaded text files.
  # $_[4] is returned with the location for TIPS executables.
  # $_[5] is returned with the relative root for text files. 

  $_[0] = "http://" . $ENV{'SERVER_NAME'};
#  $_[0] = "http://127.0.0.1";
  $_[1] = $ENV{'SCRIPT_NAME'};
  $_[1] =~ s/[\/\\][^\/\\]+$//;
  $_[1] = $_[0].$_[1];
  if ($seekit_unix) {
    $_[2] = ".";
    $_[3] = "./text";
    $_[4] = ".";
    $_[5] = ".";
  } 
  else {
    $_[2] = ".";
    $_[3] = ".\\text";
    $_[4] = "c:\\utils";
    $_[5] = ".";
  }
}

sub htmlheader {

  # Does the standard HTTP html content type header then puts out the demos
  # standard startup HTML content.
  local ($docroot, $cgiroot, $dataroot);

  &getroots($docroot, $cgiroot, $dataroot);
  $| = 1;
  if ($seekit_unix || $seekit_netscape) {
    print "Content-type: text/html\r\n\r\n";
  }

  print <<"HTML";
<!DOCTYPE HTML PUBLIC \"-//SQ//DTD HTML 2.0\">
<HTML>
<HEAD>
<TITLE>SeekIt WebDrill Demo</TITLE></HEAD>

<BODY BACKGROUND="/seekware/images/demoback.jpg"
BGCOLOR=\"#86AECC\" TEXT=\"#000040\" LINK=\"#0000A0\" VLINK=\"#800040\" ALINK=\"#408080\">
HTML
}

sub gethttpdata {

  # Gets the html fields from the submitting web page. Both GET and POST
  # methods are handled. The results are placed in @contents, an associative
  # array indexed by the field names.

  if ($ENV{'REQUEST_METHOD'} eq 'GET') {
    &getqueryfields;
  }
  elsif ($ENV{'REQUEST_METHOD'} eq 'POST') {
    &getformfields;
  }
  else {
    0;
  }
}

sub getformfields {
  # returns associative array $contents indexed by html form field names and
  # containing values for those fields.

  local ($buffer, @pairs, $pair, $value, $name);

  # Read in the login data
  read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

  # Split the login data into fieldname,fieldvalue pair strings.
  @pairs = split(/&/, $buffer);

  # Split the fieldname, fieldvalue string into it's components and add to
  # contents associative array which is indexed by field name.
  foreach $pair (@pairs) {
    ($name, $value) = split(/=/, $pair);
    $name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    # while we are at it, decode the HTTP coding of spaces and hexed values.
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $contents{$name} = $value;
  }
}

sub getqueryfields {
  # Split the fieldname, fieldvalue string from a "GET" into it's components and add to
  # contents associative array which is indexed by field name.
  local ($widget, $key, $value);
  if ($ENV{'REQUEST_METHOD'} eq 'GET' && $ENV{'QUERY_STRING'} ne '') {
    foreach $widget (split("&", $ENV{'QUERY_STRING'})) {
      if ($widget =~ /(.*)=(.*)/) {
        ($key, $value) = ($1, $2);
        $value =~ s/\+/ /g;
	$value =~ s/%(..)/pack('c',hex($1))/eg;
        $contents{$key} = $value;
      }
    }
  }
}

sub makeusertoken {

  # Creates a session token for the current login. This session token
  # is passed as the "Token" field in web pages within the SeekIt WebDrill Demo.
  # The token is used for timing out the current session so that users cannot
  # remained logged in forever.

  # _[0] is the username for which token is to be generated.
  # _[1] is returned as the token which was made.
  $_[1] = time;
}

sub getusertoken {

  # This function checks the SeekIt WebDrill Demo session token passed to
  # it to determined it the session has timed out. If it has, the
  # session is set to null. If the session has not timed out, the token
  # is updated so that timeout counting is zeroed.

  # _[0] is the username for which token is to be generated.
  # _[1] is the token to be checked. It is updated by this function.

  $now = time;
  if ($seekit_timeout && ($now - $_[1]) > $seekit_timeout) {
    $_[1] = "";
  }
  else {
    $_[1] = $now;
  }
}

sub seekit_getuserinfo {
  # $_[0] is the username of the user for whom info is sought.
  # $_[1] is the data root directory to use in looking for the
  #	  user file.
  # $_[2] *userinfo - returns a hash which is keyed by the field names in the
  # user information file.

  local ($usrname, $droot, *usrinfo, $usrfilename, $linenum, $linein, 
	$prevfield, $fld, $v);

  $usrname = $_[0];
  $droot = $_[1];
  *usrinfo = $_[2];
  $usrfilename = $droot.$seekit_dirsep."users".$seekit_dirsep.$usrname;
  if (open(USERFILE, $usrfilename)) {
    $linenum = 1;
    $prevfield = "";
    for (<USERFILE>) {
      $linein = $_;	
      chop($linein);
      if ($linenum == 1) {
	($usrinfo{'Username'}, $usrinfo{'Password'}) = 
		unpack("A8 A13",$linein); 
      }
      elsif ($linein =~ /:/) {
        ($fld, $v) = split(":", $linein);;
        $usrinfo{$fld} = $v;
	$prevfield = $fld;
      }
      elsif ($prevfield ne "") {
	$usrinfo{$prevfield} .= "\n$linein";
      }
      $linenum += 1;
    }
    close(USERFILE);
  }
  else {
    print "<P>seekit_getuserinfo: Unable to open $usrfilename<BR>..error: $!<BR>\n";
  }
  return 1;
}

sub demomain {

  # This function presents the main menu page for the SeekIt WebDrill Demo.
  # The user is checked to verify that they have logged in.

  # $_[0] is the username for the person currently logged on.
  # $_[1] is the session token.

  local ($docroot, $cgiroot, $dataroot);

  &getroots($docroot, $cgiroot, $dataroot);

  if (!$_[1] || $_[1] eq "") {
     print <<"HTML";
<H1>Your login is no longer valid.</H1>
<P>Please <A HREF=\"/seekware/webdemo/login.htm\"<EM>login</EM></A> again.</P>
</BODY>
</HTML>
HTML
  }
  else {
    print <<"HTML";
<P><TABLE BORDER=1 width="100%">
<TR><TH><FONT SIZE="2"><STRONG>BBS Functions</STRONG></FONT></TH>
<TH><FONT SIZE="2"><A HREF=\"$cgiroot/profile.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Search Agent</A></FONT></TH>
<TH><FONT SIZE="2"><A HREF=\"$cgiroot/results.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Agent Results</A></FONT></TH>
<TH><FONT SIZE="2"><A HREF=\"$cgiroot/search.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Archive Search</A></FONT></TH>
<TH><FONT SIZE="2"><A HREF=\"$cgiroot/srchtext.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Submit Item</A></FONT></TH>
</TR>
<TR><TH ><FONT SIZE="2"><STRONG>Internet Search</STRONG></FONT></TH>
<TH colspan=4><FONT SIZE="2"><A HREF=\"$cgiroot/srchurl.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Submit URL for Search</A></FONT></TH>
</TR>
<TR>
<TH><FONT SIZE="2"><STRONG>Other Functions</EM></STRONG></TH>
<TH colspan="1.33"><FONT SIZE="2"><A HREF=\"$cgiroot/regupdat.$seekit_perlext?Username=$_[0]&Token=$_[1]\" target="main">Update Registration</A></FONT></TH>
<TH colspan="1.33"><FONT SIZE="2"><A HREF=\"/seekware/webdemo/tutorial.htm" target="main">Help</A></FONT></TH>
<TH colspan="1.34"><FONT SIZE="2"><A HREF=\"/seekware/homeframes.htm" target="window">SeekWare Inc. homepage</A></FONT></TH>
</TR>
</TABLE></P>
</BODY>
</HTML>
HTML
  }
}

sub makehtmltext {
  $_[0] =~ s/\&/\&amp;/;
  $_[0] =~ s/\"/\&quot;/;
  $_[0] =~ s/\</\&lt;/;
  $_[0] =~ s/\>/\&gt;/;
}

sub seekit_results {

  # This function displays the search results for a query and allows the
  # user to manipulate those results.
  # $_[0] is the query name
  # $_[1] is the query type (archive or agent)
  # $contents contains the form data from the query submit

  local ($qname, $qtype, $searchdir, $docroot, $cgiroot, 
    $dataroot, $submitroot, $tipsroot, $textroot, $queryname,
    @hitlist, $hit, $doc, @docname, @qname, $fullname, $linein, $docname,
    $queryname, $doclen);

  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  $searchdir=$dataroot.$seekit_dirsep."searches";
  $qname = $_[0];
  $qtype = $_[1];
  $username = $contents{'Username'};
  $usertoken = $contents{'Token'};

  if (! open(SEEKITLBIN, $searchdir.$seekit_dirsep.$qname.".hit")) {
    print "<P>Unable to locate search results</P>\n";
  }
  else {
    print <<"HTML";
      <H2 align="Center">$qtype search results for user: <EM>$username</EM></H2><HR>
      <P><FORM METHOD=POST action=\"$cgiroot/hitproc.$seekit_perlext\">
      <INPUT TYPE=\"HIDDEN\" NAME=\"Username\" VALUE=\"$username\">
      <INPUT TYPE=\"HIDDEN\" NAME=\"Token\" VALUE=\"$usertoken\">
      <INPUT TYPE=\"HIDDEN\" NAME=\"Queryname\" VALUE=\"$qname\">
      <INPUT TYPE=\"HIDDEN\" NAME=\"Querytype\" VALUE=\"$qtype\">
HTML
      print "<TABLE width=\"100%\"><TR><TH>\n";
      print "<Input type=\"submit\" Name=\"Remove\" Value=\"Remove marked\"\>\n";
      print "<Input type=\"submit\" Name=\"Mail\" Value=\"E-mail marked to self\"\>\n";
      print "</TH></TR></TABLE><BR>\n";
      for (<SEEKITLBIN>) {
      @hitlist = split(/^/);
      $i=0;
      for (@hitlist) {
        ($doc, $doclen, $queryname) = split(/,/, $hitlist[$i++]);
        $fullname = $doc;
	if ($seekit_unix) {
          @docname = split(/\//, $doc);
	}
	else {
          @docname = split(/\\/, $doc);
	}
        $doc = $seekit_dirsep.$docname[$#docname];
	if ($seekit_unix) {
          @qname = split(/\//, $queryname);
	}
	else {
          @qname = split(/\\/, $queryname);
	}
        $queryname = $qname[$#qname];
	$queryname =~ s/\#.*//;
        print "<INPUT TYPE=\"checkbox\" NAME=\"$doc\"\>\n";
        if (open (SEEKITLBIN1, $fullname)) {
          $linein = <SEEKITLBIN1>;
          close SEEKITLBIN1;
          chop $linein;
          if ($linein ne '') {
            &makehtmltext($linein);
            $linein =~ tr/\x80-\xFF//d;
            $linein =~ tr/[\x00-\x01]/ /;
            $linein =~ tr/\n/ /;
          }
          else {
            $linein = $docname[$docname-1];
          }
        }
        else {
          $linein = $docname[$docname-1];
        }
        print "<A HREF=\"$cgiroot/getdoc.$seekit_perlext?Doc=$doc\&Query=$qname&Username=$username\&Token=$usertoken\">$linein</A><BR>\n";
      }
    }
    close SEEKITLBIN;
    print "<BR>";
    print "<TABLE width=\"100%\"><TR><TH>\n";
    print "<Input type=\"submit\" Name=\"Remove\" Value=\"Remove marked\"\>\n";
    print "<Input type=\"submit\" Name=\"Mail\" Value=\"E-mail marked to self\"\>\n";
    print "</TH></TR></TABLE>\n";
    print "</FORM>\n";
  }
}

sub seekit_query {

  # $_[0] is the query name
  # $_[1] is the query type (archive or agent)
  # $contents contains the form data from the query submit

  local ($qname, $qtype, $querytext, $searchdir, $docroot, $cgiroot, 
    $dataroot, $submitroot, $tipsroot, $textroot, $htmltext);

  ($qname, $qtype) = @_;
  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);
  $username = $contents{'Username'};
  $searchdir = $dataroot."/searches";

  $querytext="";
  if (open(SEEKITLBIN,"$searchdir/$qname.qry")) {
    while (<SEEKITLBIN>) {
      $querytext .= $_;
    }
    close SEEKITLBIN;
  }
  $htmltext=$querytext;
  &makehtmltext($htmltext);

  print <<"HTML";
<H1><FONT SIZE="+1"><EM>SeekIt WebDrill</EM> &#153 Demo - $qtype search for account <EM>$username</EM></FONT></H1>
<FORM METHOD=POST action=\"$cgiroot/qryproc.$seekit_perlext\">
<P><TABLE>
<TR>
<TD>Query:</TD>
<TD colspan="7" rowspan="3">
<TEXTAREA ROWS=\"2\" COLS=\"80\" NAME=\"Query\">$htmltext</TEXTAREA>
</TD>
</TR>
<TR></TR>
<TR></TR>
HTML
  if ($qtype eq 'URL') {
    print <<"HTML";
<TR>
<TD>URL:</TD>
<TD colspan="7">
<INPUT TYPE="TEXT" NAME="URL" SIZE="75">
</TD>
</TR>
</TABLE>
<TABLE>
<TR><TD>
<INPUT TYPE="checkbox" NAME="RecurseLinks">Search links 
</TD><TD>
<INPUT TYPE="checkbox" NAME="DrillSite">Drill Down Into Site
</TD><TD>
<INPUT TYPE="checkbox" NAME="DoMarkup">Search HTML Markup
</TD></TR>
<TR><TD>
<INPUT TYPE="checkbox" NAME="ShowAll">Show All Documents Searched
</TD><TD>
<INPUT TYPE="checkbox" NAME="ShowErrs">Show Retrieval Errors<BR>
</TD></TR>
</TABLE>
HTML
  }
  else {
    print "</TABLE>\n";
  }
  print <<"HTML";
<P><P><TABLE width="100%"><TR><TH>
<Input type=\"SUBMIT\" Name=\"Submit\" value=\"Submit query\">
<INPUT TYPE=\"RESET\" NAME=\"Reset\" value=\"Restore Original Query\">
<INPUT TYPE=\"HIDDEN\" NAME=\"Username\" VALUE=\"$username\">
<INPUT TYPE=\"HIDDEN\" NAME=\"Token\" VALUE=\"$usertoken\">
<INPUT TYPE=\"HIDDEN\" NAME=\"Queryname\" VALUE=\"$qname\">
<INPUT TYPE=\"HIDDEN\" NAME=\"Querytype\" VALUE=\"$qtype\">
</TH></TR></TABLE></P>
<P>Do you need <A HREF=\"/seekware/webdemo/tutorial.htm#QueryLanguage\">help</A>
on how to form queries?
</P>
</FORM>
HTML
}

sub seekit_loadfields {
  # Loads field definition file into lists.
  # $_[0] name of field file
  # $_[1] returned number of field definitions found in file.
  # $_[2] returned field name list. Field number is index for name.
  # $_[3] returned field markup list. Field number is index for entry..

  local (*names, *markup);
 
  $_[1] = 0;
  *names = $_[2];
  *markup = $_[3];
  if (open(FIELDFILE, $_[0])) {
    for (<FIELDFILE>) {
      chop;
      ($names[$_[1]], $markup[$_[1]]) = split(/ /, $_, 2); 
      $_[1] += 1;
    }
  }
  else {
    print "<P>Error opening support db field file: $_[0], $!<BR>\n";
  }
  return $_[1];
}

sub seekit_linkmenu {

  # This function will draw the standard menu of links which is used
  # to quickly move to major functions within the SeekWare web pages.
  #
  # $_[0]   list of optional entries to display. The list consists of a
  #	    series of option value, URL pairs. 
  local ($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot,
    %options, $i);

  print <<"HTML";
<HR>
<P>
<TABLE width="100%"><TR><TH>
<TABLE BORDER=2>
<TR>
<TH><A HREF=\"/seekware/homeframes.htm\" target="window"><EM>SeekWare Inc.</EM> homepage</A></TH>
HTML

  if ($_[0]) {
    $i=0;
    while ($i <= $#_) {
      print "<TH><A HREF=\"$_[$i+1]\"?Username=$contents{'Username'}&\
Token=$contents{'Token'}\">$_[$i]</A></TH>\n";
      $i += 2;
    }
  }
  print "</TABLE></TH></TR></TABLE>\n";
}

sub seekit_endpage {

  # This function ends a web page in the SeekIt WebDrill demo.
  # This function assumes that a all http and html text up to those needed to
  # wrap up the page have already been sent out.
  # gethttpdata must be called prior to calling this function.
  &seekit_linkmenu(@_);
  print <<"HTML";
</P>
<P></P><P>
<TABLE width="100%"><TR><TH>
<FONT Size="1"><EM>SeekIt WebDrill</EM> &#153 - Copyright &copy 1997, SeekWare Inc., All Rights Reserved
</FONT></TH></TR></TABLE>
</P>
</BODY>
</HTML>
HTML
}
 
sub seekit_notloggedin {

# This function is used to notify the user that they are not logged in. From
# this page, the user may either jump to the login page or return to the
# SeekWare home page.

  local ($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  if ($contents{'Username'} ne '' && $contents{'Token'} ne '') {
    print "<P>Your session has timed out.<BR>\n";
  }
  else {
    print "<P>You are not currently logged in.<BR>\n";
  }
  print <<"HTML";
<P>You must <A HREF=\"/seekware/webdemo/webdemo.htm\" target="window"><EM>login</EM></A> 
  in order to proceed with the SeekIt WebDrill Demo.</P>
HTML
  &seekit_endpage;
} 

sub seekit_getexternalURLs {

  # returns hash of URLS for current specified file. The URL is a key and
  # the URL type is the value, one of "Frame", "Remote", or "Local".
  # $_[0] is the file mime type - text/plain, text/html, application-octetstream
  # $_[1] is the name of the file.
  # $_[2] is returned with the base URL found in the file.
  # $_[3] is returned with the hash.

  local (*markup, $inline, $temp, $ishtml);
  *markup = $_[3];
  $_[2] = "";
  $ishtml = 1;
  $ishtml = 0 unless ($_[0] =~ /text\/html/i);
  if (open(SEEKITLBIN, $_[1])) {
    while (<SEEKITLBIN>) {
      $inline = $_;
      if ($ishtml) {
        while ($inline =~ /</) {
	  if ($inline =~ /(<[^>]*>)/ ) {
	    $temp = $1;
	    if ($_[2] eq "" && $temp =~ /<\s*BASE\s+HREF/i){
	      if ($temp =~ /\s+HREF\s*=\s*\"?([^\"\s>]+)/i ) {
	        $_[2] = $1;
	      }
	    }
	    elsif ($temp =~ /<\s*A\s+/i || $temp =~ /<\s*frame\s+/i ||
	 	   $temp =~ /<\s*area\s+/i || $temp =~ /<\s*img\s+/i) {
	      if ( $temp =~ /\s+HREF\s*=\s*\"?([^\"\s>]+)/i ) {
	        $temp = $1;
	        if (&seekit_isexternallink($temp) && !$markup{$temp}) {
		  $markup{$temp} = "";
	        }
	      }
	      elsif ($temp =~ /<\s*frame\s+.*src\s*=\s*\"?([^\"\s>]+)/i ) {
	        $temp = $1;
	        if (&seekit_isexternallink($temp)) {
		  $markup {$temp} = "Frame";
	        }
	      }
	      elsif ($temp =~ /<\s*img\s+.*usemap\s*=\s*\"?([^\"\s>]+)/i ) {
	        $temp = $1;
	        if (&seekit_isexternallink($temp)) {
		  $markup {$temp} = "";
	        }
	      }
            }
	    $inline =~ s/<[^>]*>//;
	  }
	  else {
	    $inline =~ /(<.*)/;
	    $temp = $1;
	    $inline = <SEEKITLBIN>;
	    while ($inline && !($inline =~ />/ ) ) {
	      $temp .= $inline;
	      $inline = <SEEKITLBIN>;
	    }
	    if ($inline =~ /(.*>)/ ) {
	      $temp .= $1;
	      if ($_[2] eq "" && $temp =~ /<\s*BASE\s*HREF/i) {
	        if ($temp =~ /\s+HREF\s*=\s*\"?([^\"\s>]+)/i ) {
	          $_[2] = $1;
	        }
	      }
	      elsif ($temp =~ /<\s*A\s+/i || $temp =~ /<\s*frame\s+/i ||
	 	     $temp =~ /<\s*area\s+/i || $temp =~ /<\s*img\s+/i) {
	        if ( $temp =~ /\s+HREF\s*=\s*\"?([^\"\s>]+)/i ) {
		  $temp = $1;
	          if (&seekit_isexternallink($temp) && !$markup{$temp}) {
	            $markup{$temp} = "";
		  }
	        }
	        elsif ($temp =~ /<\s*frame\s+.*src\s*=\s*\"?([^\"\s>]+)/i ) {
	          $temp = $1;
	          if (&seekit_isexternallink($temp)) {
	            $markup{$temp} = "Frame";
	          } 
	        }
	        elsif ($temp =~ /<\s*img\s+.*usemap\s*=\s*\"?([^\"\s>]+)/i ) {
	          $temp = $1;
	          if (&seekit_isexternallink($temp)) {
	  	    $markup {$temp} = "";
	          }
	        }
	      }
	      $inline =~ s/.*>//;
	    }
          }
        }
      }
      else {
        # processing non html. Scan for items which look like urls.
        while ($inline =~ s/(http:\/\/[\w\.\-\/\\_]+)//i) {
	  $temp = $1;
	  if (&seekit_isexternallink($temp)) {
	    $markup{$temp} = "";
	  }
        }
      }
    }
    close SEEKITLBIN;
  }
}

sub seekit_isexternallink {
  return ($temp =~ /http:/i || $temp =~ /file:/i || 
     (!($temp =~ /\w+:/) && index($_[0], "\#") != 0));
}

sub seekit_parseURL {
  # returns base portion of specified URL
  # $_[0] - URL from which base is to be extracted.
  # $_[1] - returned site for URL.
  # $_[2] - returned base URL. Same as the function returns as it's value.
  # $_[3] - object, object at end of URL.
  local ($spec);

  $spec = $_[0];
  $spec =~ s/(^\w+:\/*[^\\\/]*)//;
  $_[1] = $1;
  $spec =~ s/([^\\\/]*)$//;
  $_[2] = $_[1].$spec;
  $_[3] = $1;
  return $_[2];
}

sub seekit_absURL {
  # returns expanded URL. Pointers to internal HTML anchors are also removed
  # $_[0] URL to expand.
  # $_[1] base URL as returned by a call to seekit_baseurl.
  local ($baseurl, $basesite, $object, $location, $s);
  
  $baseurl = &seekit_parseURL($_[1], $basesite, $baseurl, $object);
  $baseurl .= "/" unless ($baseurl =~ /\/\Z/); 
  $location = $_[0];
  $location =~ s/\#.*//;
  #$s = $seekit_dirsep unless ($_[1] =~ /[\/\\]\Z/);
  if ($location ne "" &&  !($location =~ /\w+:/)) {
    if ($location =~ /\A[\\\/]/ ) {
      $location = $basesite.$location;
    }
    else {
      $location = $baseurl.$location;
    }
  }
  return $location;
}

sub seekit_uniqueURLs {
  # Returns reference to a hash of unique elements of supplied URL list.
  # The hash is keyed by the URLs with values set to 0. This is so that
  # the values can be used for a reference count during processing of the
  # URLs.
  # $_[0] *urllist, hash of URLs to process.
  # $_[1] Base URL as returned by a call to seekit_baseurl.
  # $_[2] *absurl, hash of absolute urls.

  local (*urllist, $origurl, *absurl);

  *urllist = $_[0];
  *absurl = $_[2];
  foreach $origurl (keys(%urllist)) {
    $absurl{&seekit_absURL($origurl, $_[1])} = "";
  }
}

sub seekit_setURLtypes {
  # Updates values for a hash table which has URLs for keys. If the value
  # for an entry in the table is "", the entry is updated to reflect whether
  # the URL is Local or Remote relative to a specified base URL.
  #
  # $_[0]  Reference to the hash containing the URLs whose types are to be set.
  # $_[1]  The base URL which is to be used in resolving whether the URLs
  #	   are "Local" or "Remote"
  local (*urlhash, $url, $site, $base, $site, $newbase, $newsite, $obj);
  
  *urlhash = $_[0];
  $base = $_[1];
  &seekit_parseURL($base, $site, $base, $obj);

  foreach $url (keys(%urlhash)) {
    if (! $urlhash{$url} || $urlhash{$url} eq "") {
      &seekit_parseURL($url, $newsite, $newbase, $obj);
      if ($newsite eq "" || $site eq $newsite) {
        $urlhash{$url} = "Local"; 
      }
      else {
        $urlhash{$url} = "Remote"; 
      }
    }
  }
}

sub seekit_getHTTP {
  #_[0] URL to be retrieved
  #_[1] File to be written with the contents of the URL
  #_[2] Returned http document type
  #_[3] Returned http status code
  #_[4] Returned http status message

  if (!$seekit_nosocket) {
    use Socket;
  }

  local ($remote, $doc, $port, $proto, $line, $errmsg,
	$addr2, $thataddrs, $msg, $AF_INET, $SOCK_STREAM, $PF_INET);

  if ($seekit_nosocket) {
    $_[3] = "999";
    $_[4] = "Perl support for sockets is inoperable on server";
    return $_[4];
  }
  $doc = $_[0];
  $doc =~ s/\w+:\/\/([^\/]+)//;
  $doc = "/" unless ($doc ne "");
  $remote = $1;
  $errmsg = "";
  $_[2] = "";
  $_[3] = "";
  $_[4] = "";

  $port = 80;
  if ($seekit_hassocketdefs) {
    $AF_INET = AF_INET;
    $SOCK_STREAM = SOCK_STREAM;
    $PF_INET = PF_INET;
  }
  else {
    $AF_INET = 2;
    $SOCK_STREAM = 1;
    $PF_INET = 2;
  }
  $sockaddr = 'S n a4 x8';
  $thataddrs = gethostbyname($remote);
  $addr2 = pack($sockaddr, $AF_INET, $port, $thataddrs);
  $proto   = getprotobyname('tcp');

  if (! socket(SEEKITLBSOCK, $PF_INET, $SOCK_STREAM, $proto)) {
   $errmsg = "Error creating socket: $!";
  }
  else {
    if (! connect(SEEKITLBSOCK, $addr2)) {
      $errmsg = "Error connecting to socket: $!";
    }
  }

  if ($errmsg eq "") {
    select(SEEKITLBSOCK); $| = 1; select (STDOUT);
    print SEEKITLBSOCK "GET $doc HTTP/1.0\r\n";
    print SEEKITLBSOCK "Accept: text/plain, text/html, application/octet-stream\r\n";
    print SEEKITLBSOCK "\r\n";

    $line = <SEEKITLBSOCK>;
    $msg = $line;
    if ($line =~ s/http\/(\d\.\d) (\d\d\d)//i) {
      $httpver=$1;
      $httpstat = $2; 
      $_[3] = $httpstat;
      $_[4] = $line;
      if (($httpstat =~ /2\d\d/)) {
        for ($line = <SEEKITLBSOCK>; $line ne "\r\n" && $line ne ""; $line = <SEEKITLBSOCK>) {
          if ($line =~ /Content-Type:\s*(.*)\r\n/i) {
	    $line = $1;
            $_[2] = $line;
	    if (!($line =~ /^text\/html/i) && !($line =~ /^text\/plain/i) &&
	        !($line =~ /^application\/octet-stream/i)) {
	      $errmsg = "Unsupported content type: $line";
	    }
          }
        }
      }
      else {
	$errmsg = "Http error response received\n  Http version: $httpver\n  status: $httpstat";
      }
    }
    else {
      $errmsg = "Unable to decode Http response, url: $_[0], messge: $msg";
    }

    if ($errmsg eq "") {
      if (open (SEEKITLBOUT, ">$_[1]")) {
        for (<SEEKITLBSOCK>) {
          print SEEKITLBOUT $_;
        }
        close SEEKITLBOUT;
      }
      else {
	$errmsg = "Error opening output file: $_[1]";
      }
    }
  }

  shutdown (SEEKITLBSOCK, 2);
  if ($errmsg eq "") {
    $errmsg = "Successfully retrieved $_[0]";
  }
  $errmsg;
}

sub seekit_searchfile {
  # $_[0] is the name of the file to search
  # $_[1] is the file type, right now one of "text/plain" or "text/html"
  # $_[2] is the name of the query to use in the search. 
  # $_[3] is returned with the relevance rating for the document. A positive
  #	relevance ranking indicates a match on the query.
  # $_[4] is returned with the title for the document.
  # returns the relevance ranking for the document.

  local ($title, $linein, $endhead, $doctype, $cmd, $searchspec,
    $statusmsg, $docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, 
    $textroot, $offset, @offsetlist, $noffsets, $nwords, $relevance,
    $hitspec, $textfilename, $x);

  $textfilename = $_[0];
  $doctype = $_[1];
  $searchspec = $_[2];
  $x = $seekit_exe;


  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  $title = "";

  if (open(SEEKITLBIN,$textfilename)) {
    if ($doctype =~ /text\/html/i) {
      for ($linein=<SEEKITLBIN>, $endhead=0; 
	  $linein ne "" && $title eq "" && !$endhead; 
	  $linein=<SEEKITLBIN>) {
        $endhead = ($linein =~/<\/HEAD>/i);
        if ($linein =~ s/<TITLE>([^<]+)//i) {
	  $title = $1;
	  if (!$endhead && $linein eq "") {
	    $linein=<SEEKITLBIN>;
	  }
	  while (!$endhead && ($linein =~ s/\A([^<]+)//)) {
	    $title .= " $1";
	    $endhead = ($linein =~ /<\/HEAD>/i);
	    if ($linein eq "" && !$endhead) {
	      $linein=<SEEKITLBIN>;
	    }
	  } 
        }
      } 
    }
    else {
      while ((length($title) < 70) && ($linein=<SEEKITLBIN>)) {
        chop($linein);
        $title .= $linein." ";
      }
      if (length($title) > 70) {
	$title = substr($title,0,70)
      }
    }
    close SEEKITLBIN;
  }
  $_[4] = $title;

  $hitspec = $dataroot.$seekit_dirsep."searches".$seekit_dirsep."tmp".$$;
  $cmd = $tipsroot.$s."afind".$x." -q ".$searchspec." -h ". 
    $hitspec.".hit -f ".$textfilename." 2>$seekit_nulldev";
  $statusmsg = `$cmd`;

  if (open(SEEKITLBIN,$hitspec.".hit")) {
    $hit = <SEEKITLBIN>;
    close SEEKITLBIN;
    unlink $hitspec.".hit";
    unlink $hitspec.".drc";
    if ($hit ne "") {
      $cmd = $tipsroot.$s."offsets".$x." -q ".$searchspec." -f ".
	$textfilename." 2>$seekit_nulldev";
      $offsets = `$cmd`;
      @offsetlist = split(/\n/, $offsets);
      $noffsets = @offsetlist;
      $cmd = "wc -w ".$textfilename;
      $nwords = `$cmd`;
      $nwords =~ s/^[^\d]*(\d+).*/$1/;
      $nwords = 1 unless ($nwords);
      $relevance = ($noffsets * 100) / $nwords;
      $relevance =~ s/(\d*\.\d\d).*/$1/;
    }
    else {
      $relevance = 0;
    }
  }
  else {
    $relevance = -1;
  }
  $_[3] = $relevance;
} 

%seekit_htmlmarkup = (
  '&#34;',	"\"", '&quot;',	"\"",
  '&#38;',	"&", '&amp;',	"\&",
  '&#60;',	"<", '&lt;',	"\<",
  '&#62;',	">", '&gt;',	"\>",
  '&#160;',	"\xa0", '&nbsp;',	"\0160",	 
  '&#161;',	"\xA1", '&iexcl;',	"\xA1",
  '&#162;',	"\xA2", '&cent;',	"\xA2",
  '&#163;',	"\xA3", '&pound;',	"\xA3",
  '&#164;',	"\xA4", '&curren;',	"\xA4",
  '&#165;',	"\xA5", '&yen;',	"\xA5",
  '&#166;',	"\xA6", '&brvbar;',	"\xA6", '&brkbar;',	"\xA6",
  '&#167;',	"\xA7", '&sect;',	"\xA7",
  '&#168;',	"\xA8", '&uml;',	"\xA8", '&die;',	"\xA8",
  '&#169;',	"\xA9", '&copy;' ,	"\xA9",  
  '&#170;',	"\xAA", '&ordf;',	"\xAA",
  '&#171;',	"\xAB", '&laquo;',	"\xAB",
  '&#172;',	"\xAC", '&not;',	"\xAC",
  '&#173;',	"\xAD", '&shy;',	"\xAD",
  '&#174;',	"\xAE", '&reg;',	"\xAE",
  '&#175;',	"\xAF", '&macr;',	"\xAF", '&hibar;',	"\xAF",
  '&#176;',	"\xb0", '&deg;',	"\xb0",
  '&#177;',	"\xb1", '&plusmn;',	"\xb1",
  '&#178;',	"\xb2", '&sup2;',	"\xb2",
  '&#179;',	"\xb3", '&sup3;',	"\xb3",
  '&#180;',	"\xb4", '&acute;',	"\xb4",
  '&#181;',	"\xb5", '&micro;',	"\xb5",
  '&#182;',	"\xb6", '&para;',	"\xb6",
  '&#183;',	"\xb7", '&middot;',	"\xb7",
  '&#184;',	"\xb8", '&cedil;',	"\xb8",
  '&#185;',	"\xb9", '&sup1;',	"\xb9",
  '&#186;',	"\xba", '&ordm;',	"\xba",
  '&#187;',	"\xbb", '&raquo;',	"\xbb",
  '&#188;',	"\xbc", '&frac14;',	"\xbc",
  '&#189;',	"\xbd", '&frac12;',	"\xbd",
  '&#190;',	"\xbe", '&frac34;',	"\xbe",
  '&#191;',	"\xbf", '&iquest;',	"\xbf",
  '&#192;',	"\xc0", '&Agrave;',	"\xc0",
  '&#193;',	"\xc1", '&Aacute;',	"\xc1", 
  '&#194;',	"\xc2", '&Acirc;',	"\xc2",  
  '&#195;',	"\xc3", '&Atilde;',	"\xc3", 
  '&#196;',	"\xc4", '&Auml;',	"\xc4",   
  '&#197;',	"\xc5", '&Aring;',	"\xc5",  
  '&#198;',	"\xc6", '&AElig;'  ,	"\xc6",
  '&#199;',	"\xc7", '&Ccedil;' ,	"\xc7",
  '&#200;',	"\xc8", '&Egrave;' ,	"\xc8",
  '&#201;',	"\xc9", '&Eacute;' ,	"\xc9",
  '&#202;',	"\xca", '&Ecirc;'  ,	"\xca",
  '&#203;',	"\xcb", '&Euml;'   ,	"\xcb",
  '&#204;',	"\xcc",  '&Igrave;' ,	"\xcc",
  '&#205;',	"\xcd",  '&Iacute;' ,	"\xcd",
  '&#206;',	"\xce",  '&Icirc;'  ,	"\xce",
  '&#207;',	"\xcf",  '&Iuml;'   ,	"\xcf",
  '&#208;',	"\xd0",  '&ETH;'    ,	"\xd0",  '&Dstrok;',	"\xd0",
  '&#209;',	"\xd1",  '&Ntilde;' ,	"\xd1",
  '&#210;',	"\xd2",  '&Ograve;' ,	"\xd2",
  '&#211;',	"\xd3",  '&Oacute;' ,	"\xd3",
  '&#212;',	"\xd4",  '&Ocirc;'  ,	"\xd4",
  '&#213;',	"\xd5",  '&Otilde;' ,	"\xd5",
  '&#214;',	"\xd6",  '&Ouml;'   ,	"\xd6",
  '&#215;',	"\xd7",  '&times;',	"\xd7",
  '&#216;',	"\xd8",  '&Oslash;' ,	"\xd8",
  '&#217;',	"\xd9",  '&Ugrave;' ,	"\xd9",
  '&#218;',	"\xda",  '&Uacute;' ,	"\xda",
  '&#219;',	"\xdb",  '&Ucirc;'  ,	"\xdb",
  '&#220;',	"\xdc",  '&Uuml;'   ,	"\xdc",
  '&#221;',	"\xdd",  '&Yacute;' ,	"\xdd",
  '&#222;',	"\xde",  '&THORN;'  ,	"\xde",
  '&#223;',	"\xdf",  '&szlig;'  ,	"\xdf",
  '&#224;',	"\xe0",  '&agrave;' ,	"\xe0",
  '&#225;',	"\xe1",  '&aacute;' ,	"\xe1",
  '&#226;',	"\xe2",  '&acirc;'  ,	"\xe2",
  '&#227;',	"\xe3",  '&atilde;' ,	"\xe3",
  '&#228;',	"\xe4",  '&auml;'   ,	"\xe4",
  '&#229;',	"\xe5",  '&aring;'  ,	"\xe5",
  '&#230;',	"\xe6",  '&aelig;'  ,	"\xe6",
  '&#231;',	"\xe7",  '&ccedil;' ,	"\xe7",
  '&#232;',	"\xe8",  '&egrave;' ,	"\xe8",
  '&#233;',	"\xe9",  '&eacute;' ,	"\xe9",
  '&#234;',	"\xea",  '&ecirc;'  ,	"\xea",
  '&#235;',	"\xeb",  '&euml;'   ,	"\xeb",
  '&#236;',	"\xec",  '&igrave;' ,	"\xec",
  '&#237;',	"\xed",  '&iacute;' ,	"\xed",
  '&#238;',	"\xee",  '&icirc;'  ,	"\xee",
  '&#239;',	"\xef",  '&iuml;'   ,	"\xef",
  '&#240;',	"\xf0",  '&eth;'    ,	"\xf0",
  '&#241;',	"\xf1",  '&ntilde;' ,	"\xf1",
  '&#242;',	"\xf2",  '&ograve;' ,	"\xf2",
  '&#243;',	"\xf3",  '&oacute;' ,	"\xf3",
  '&#244;',	"\xf4",  '&ocirc;'  ,	"\xf4",
  '&#245;',	"\xf5",  '&otilde;' ,	"\xf5",
  '&#246;',	"\xf6",  '&ouml;'   ,	"\xf6",
  '&#247;',	"\xf7",  '&divide;',	"\xf7",
  '&#248;',	"\xf8",  '&oslash;' ,	"\xf8",
  '&#249;',	"\xf9",  '&ugrave;' ,	"\xf9",
  '&#250;',	"\xfa",  '&uacute;' ,	"\xfa",
  '&#251;',	"\xfb",  '&ucirc;'  ,	"\xfb",
  '&#252;',	"\xfc",  '&uuml;'   ,	"\xfc",
  '&#253;',	"\xfd",  '&yacute;' ,	"\xfd",
  '&#254;',	"\xfe",  '&thorn;'  ,	"\xfe",
  '&#255;',	"\xff",  '&yuml;'   ,	"\xff",
);

sub seekit_htmltotext {
  # Removes HTML markup from file and replaces HTML quoted characters in
  # text with the character they represent.
  # $_[0] input file, user is responsible for any input redirection as in <, |.
  # $_[1] output file, user is responsible for outspecifer of >, >>, or |.
  local ($linein);
  if (open(SEEKITLBIN, "$_[0]")) {
    if (open(SEEKITLBOUT, "$_[1]")) {
      while ($linein = <SEEKITLBIN>) {
        while ($linein =~ /</) {
          while ($linein =~ s/<[^>]*>//) {
            ;
          } 
          if ($linein =~ s/<.*//) {
            print SEEKITLBOUT &seekit_expandmarkup($linein);
            while (($linein = <SEEKITLBIN>) && !($linein =~ />/)) {
              ;
            }
            $linein =~ s/[^>]*>//;
          }
        }
        print SEEKITLBOUT &seekit_expandmarkup($linein);
      } 
      close SEEKITLBOUT;
    }
    close SEEKITLBIN;
  }
}

sub seekit_expandmarkup {
  local ($markup, $replacement, $orig, $fixed);
  $orig = $_[0];
  $fixed = $_[0];
  while ($orig =~ s/(&[\#\w]\w+;)//) { 
    $markup = $1;
    $replacement = $seekit_htmlmarkup{$markup};
    substr($fixed, index($fixed, $markup), length($markup)) = $replacement; 
  }
  $fixed;
}

sub seekit_fdelete {
  if ($seekit_unix) {
    `rm $_[0] 2>$seekit_nulldev`;
  }
  else {
    `del $_[0] 2>$seekit_nulldev`;
  }
}

sub seekit_highlite {
# Function: Finds matches for terms from a TIPS query in the specified input
#       file. The file is printed to standard output with the highlighted terms
#       encompassed in double angle brackets.
#
# Calling Sequence:
#       &seekit_highlite($query_file, $text_file, $outfile)
#

  local ($offsets, @pairs, $npairs, $idx, $i, $linenum, $linein, @elements,
     $nextidx, $sidx, $adj, $off, $l, $docroot, $cgiroot, $dataroot,
     $submitroot, $tipsroot, $textroot, $cmd);

  &getroots($docroot, $cgiroot, $dataroot, $submitroot, $tipsroot, $textroot);

  $cmd = $tipsroot.$seekit_dirsep."offsets".$seekit_exe." -q $_[0] -f $_[1] 2>$seekit_nulldev";
  $offsets=`$cmd`;
  @pairs = split(/\n/, $offsets);
  $npairs = @pairs;

  $idx = 0;
  $i = 0;
  $linenum = 1;
  if ($npairs > 0) {
    ($off, $l) = split(/ /, $pairs[0]);
  }
  if (open (SEEKITINFILE, $_[1]) && open(SEEKITOUTFILE, ">$_[2]")) {
    while (<SEEKITINFILE>) {
      $linein = $_;
      if ($seekit_dos) { $linein =~ s/\n/\r\n/; }
      $nextidx = $idx + length($linein);
      if ($i < $npairs && $off < $nextidx) {
        @elements = split(//,$linein);
        $sidx = $off - $idx; 
        $adj = 0;
        while ($i < $npairs && $off < $nextidx) {
          splice(@elements, $sidx, 0, "**");
          splice(@elements, $sidx + $l + 1, 0, "**");
          $adj += 2;
          $i += 1;
          if ($i < $npairs) {
          ($off, $l) = split(/ /, $pairs[$i]);
            $sidx = ($off - $idx) + $adj;
          }
        }
        $linein = join('', @elements);
      }
      print SEEKITOUTFILE $linein;
      $idx = $nextidx;
      $linenum += 1;
    }
    close SEEKITOUTFILE;
  }
  if (SEEKITINFILE) {close SEEKITINFILE};
}

sub seekit_timestamp {
  local ($sec, $min, $hour, $mday, $mon, $year);
  ($sec, $min, $hour, $mday, $mon, $year) = localtime(time);
  $sec =~ s/\A(.)\Z/0$1/;
  $min =~ s/\A(.)\Z/0$1/;
  $hour =~ s/\A(.)\Z/0$1/;
  $mday =~ s/\A(.)\Z/0$1/;
  $mon += 1;
  $mon =~ s/\A(.)\Z/0$1/;
  $year =~ s/\A(.)\Z/0$1/;
  return  "19$year$mon$mday $hour:$min:$sec";
}

sub seekit_userlog {
  # $_[0] Username to log to.
  # $_[1] Message to be logged.

  local ($logfile);

  $logfile = "logs".$seekit_dirsep."webdrill.log";
  open (SEEKITUSERLOG, ">>$logfile");
  print SEEKITUSERLOG &seekit_timestamp." - $_[0], $_[1]\n";
  close (SEEKITUSERLOG);
}

1;
