#!/usr/bin/perl5

# name: srchurl.pl
# author: Andrew M. Fregly
# function: Processes a request to enter an URL search.

require "seekitlb.pl";

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

$qryname="u$username";
chop $qryname;
$qrytype="URL";

if ($usertoken eq "") {
  &seekit_notloggedin;
}

else {
  &seekit_query($qryname, $qrytype);
}
&seekit_endpage;
