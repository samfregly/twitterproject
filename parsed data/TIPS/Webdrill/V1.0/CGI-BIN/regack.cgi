#!/usr/bin/perl5
require "seekitlb.pl";
require "mailer.pl";

# name: regack.pl
# author: Andrew M. Fregly
# function: processes a Seekit Web Demo registration form. The data in the
# form is written to a user registration file. The user is then presented
# with a page which let's them jump to the login form or go back to the
# SeekIt Inc. home page.

# HTTP content type header.
&htmlheader;

# Get contents of registration form.
&gethttpdata;

# Get roots for docs, scripts and data.
&getroots($docroot, $cgiroot, $dataroot);

$error = 0;
# Do some error checking. First see if required fields are present.
if (!$contents{'Last_Name'} || !$contents{'E-Mail'} || 
    !$contents{'Username'}  || (!$contents{'Password'} && 
     $contents{'Mode'} ne 'update')) {
  $error = 1;
  print <<"HTML";
<P>
You have left a required field blank. Please make sure you have filled in
the <EM>Last Name</EM>, <EM>E-Mail</EM>, 
<EM>Username</EM>,and <EM>Password</EM> fields. 
</P>
HTML
}

# See if username is already in use.
elsif ($contents{'Mode'} ne 'update') {
  if (open(USERFILE, "$dataroot/users/$contents{'Username'}")) {
    $error = 1;
    close USERFILE;
    print <<"HTML";
<P>
The <EM>Username</EM> \'<STRONG>$contents{Username}</STRONG>\' is already in 
use. Please pick another.
</P>
HTML
  }
}
else {
  &seekit_getuserinfo($contents{'Username'}, $dataroot, *userinfo);
}

$pw = $contents{'Password'};
# Make sure the <EM>Password</EM> is at least 4 characters in length
if (!$error && $pw) {
  if (length($pw) < 4) {
    $error = 1;
    print <<"HTML";
<P>
The <EM>Password</EM> you picked is not long enough. Please enter a new
one which is at least 4 characters in length.
</P>
HTML
  }

# Make sure validation password matches first password
  elsif ($contents{'Password'} ne $contents{'Verify_Password'}) {
    $error = 1;
    print <<"HTML";
<P>
Your <EM>Password</EM> did not match the <EM>Verify Password
</EM> you entered. Please enter the same value for both.
HTML
  }
  else {
    #   Encrypt the password.
    srand($$);
    $pwseed = pack("CC", int(rand(94)) + 33,int(rand(94) + 33) + 1);
    $pw = crypt($pw, $pwseed);
  }
}
elsif (!$error) {
  $pw = $userinfo{'Password'};
}

if (!$error) {
  if ($contents{'Mode'} ne 'update' && !($contents{'Username'} =~ /^[\w-]+$/)) {
    print <<"HTML";
<P>
The <EM>Username</EM> \'<STRONG>$contents{'Username'}</STRONG>\' is invalid. 
Please pick a name consisting of nothing but letters, numbers, dashes, and
underbars.
</P>
HTML
  }
  # Try to open user file.
  elsif (!open(USEROUT,  ">$dataroot/users/$contents{'Username'}")) {
    print <<"HTML";
<P>
Error creating registration entry for the <EM>Username</EM>  
\'<STRONG>$contents{'Username'}</STRONG>\'. Please notify SeekWare Inc.<BR>
User file: $dataroot/users/$contents{'Username'}
</P>
HTML
  }

  # Okay, everything seems Okay. Write the registration data out to the user file.
  else {

    # Write out fixed length record containing the username and the encrypted password.
    # Follow this with the remainder of the registration data.
    $userrec = pack("A8 A13", $contents{'Username'}, $pw);

    print USEROUT <<"ENDREC";
$userrec
Salutation:$contents{'Salutation'}
First_Name:$contents{'First_Name'}
Middle_Name:$contents{'Middle_Name'}
Last_Name:$contents{'Last_Name'}
Organization:$contents{'Organization'}
E-Mail:$contents{'E-Mail'}
Phone:$contents{'Phone'}
Address:$contents{'Address'}
ENDREC
    close USEROUT;

# e-mail a copy of the registration to the administrator
    push(@notification, "User - $contents{'Username'}");
    push(@notification, "Salutation - $contents{'Salutation'}");
    push(@notification, "First_Name - contents{'First_Name'}");
    push(@notification, "Middle_Name - contents{'Middle_Name'}");
    push(@notification, "Last_Name - contents{'Last_Name'}");
    push(@notification, "Organization - contents{'Organization'}");
    push(@notification, "E-Mail - contents{'E-Mail'}");
    push(@notification, "Phone - contents{'Phone'}");
    push(@notification, "Address - contents{'Address'}");
    &seekit_smtpmailer($seekit_smtpgateway, $seekit_administrator,
	"Processed WebDrill Registration", *notification, "", "", "");

# Acknowledge to the client that the registration was received.
    if ($contents{'Mode'} ne 'update') {
      print <<"HTML";
<H1>$contents{'First_Name'} $contents{'Middle_Name'} $contents{'Last_Name'}</H1>
<HR>
<P>
Your <EM>SeekIt</EM> Web Demo account for user <STRONG>\'$contents{'Username'}\'</STRONG>
has been created and will be active for the next 30 days.
</P>
<P>
To get started simply <A HREF=\"/seekware/webdemo/webdemo.htm\" target="window">log in</A>.
</P>
HTML
    }
    else {
      print <<"HTML";
<H1>$contents{'First_Name'} $contents{'Middle_Name'} $contents{'Last_Name'}</H1>
<HR>
<P>
Your <EM>SeekIt</EM> Web Demo registration information 
has been updated.</P>
HTML
    }
  }
}

&seekit_endpage;
