#!/usr/bin/perl5

require "seekitlb.pl";

&htmlheader;

print "<H1>Recent logins</H1><P>\n";
$logins=`tail -20 logs/homepage.log`;
@lines=split(/\n/, $logins);
foreach $line (@lines) {
  print "$line<BR>\n";
}
$maxcount=5;
print "<HR><H1>Multiple Accesses, $maxcount or more</H1><P>\n";
$accesses=`cat logs/homepage.log | cut -d\'-\' -f2 | cut -d, -f1 | sort -u`;
@lines=split(/\n/, $accesses);
$total=0;
$nlines=0;
foreach $line (@lines) {
  chop($line);
  $count=`grep $line logs/homepage.log | wc -l`;
  chop($count);
  $total += $count;
  $nlines += 1;
  if ($count > $maxcount) {
    print "$line - $count<BR>\n";
  }
}
print "<BR>$total total visits by $nlines visitors<BR>\n";
print "<HR><H1>WebDrill Activity</H1><P>\n";
$activities=`tail -20 logs/webdrill.log`;
@lines=split(/\n/, $activities);
foreach $line (@lines) {
  print "$line<BR>\n";
}
print "</BODY></HTML>\n";
