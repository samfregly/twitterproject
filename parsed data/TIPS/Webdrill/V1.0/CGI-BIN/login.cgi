#!/usr/bin/perl5
require "seekitlb.pl";

# name: login.pl
# Author: Andrew Fregly
# Function: Is invoked by the WebTips demo login html doc. It creates a
# checks the username and password for validity. If valid, it generates a
# session token which is used by the other demo script along with the username
# to allow the user to perform system functions. If the user logs in, the
# main menu doc for the demo is generated and returned to the client.

# Create associative array of form field contents
&gethttpdata;

# Get base URLs and data locations.
&getroots($docroot, $cgiroot, $dataroot);

&htmlheader;

# see if user file is present
$username = $contents{'Username'};
$password = $contents{'Password'};
$s = $seekit_dirsep;
$userfilename=$dataroot.$s."users".$s.$username;
if (! $username || !(-r $userfilename)) { 
  &badlogin;
}   

# Try to open user file
elsif (!open(USERFILE, $userfilename)) {
  &badlogin;
}

else {
  # See if password matches
  read(USERFILE, $buffer, 21);
  ($username, $pw) = unpack("A8 A13", $buffer);
  if (crypt($password,$pw) ne $pw)
  {
    &badlogin;
  }


  else {
    &makeusertoken($username, $usertoken);
    if ($usertoken eq "") {
      &file_error;
    }
    else {
      # Put up welcome screen
      print <<"HTML";
<H1 align="Center">Welcome to the <EM>SeekIt WebDrill</EM> &#153 Demo</H1>
<P>
<TABLE width="100%">
<TR><TH>
<TABLE border="2" width="200">
<TR>
<TH><A HREF="$cgiroot/mainmenu.$seekit_perlext?Username=$username&
Token=$usertoken" target="menu">Main Menu</A></TH>
</TR>
<TR>
<TH><A HREF="/seekware/homeframes.htm" target="window"><EM>SeekWare Inc.</EM> homepage</A></TH>
</TR>
</TABLE>
</TH></TR>
</TABLE>
</P>
<P></P><P>
<TABLE width="100%"><TR><TH>
<FONT Size="1"><EM>SeekIt WebDrill</EM> &#153 - Copyright &copy 1997, SeekWare Inc., All Rights Reserved
</FONT></TH></TR></TABLE>
</P>
</BODY>
</HTML>
HTML
      &seekit_userlog($username, "logged in");
    }
  }
}

sub badlogin {
  print <<"HTML";
<H1>WebTips Demo Login for <EM>$username</EM></H1>
<P>
You have either entered an invalid <EM>Username</EM> or <EM>Password</EM>
or your account has expired.
</P>
HTML
  &endloginerr;
}

sub file_error {
    &htmlheader;
    print <<"HTML";
<H1>WebTips Demo Login for <EM>$username</EM></H1>
<P>
There was a file error in updating your user records. You will not be able to
login at this time.
</P>
HTML
  &endloginerr;
}

sub endloginerr {
  print <<"HTML";
<P>Try to <A HREF=\"/seekware/webdemo/login.htm\">
<EM>login</EM></A> again.<HR>
<P>
HTML
  &seekit_endpage;
}
