#!/usr/bin/perl5

# name: profile.pl
# author: Andrew M. Fregly
# function: Processes a request to enter/update a agent search.

require "seekitlb.pl";

&htmlheader;
&gethttpdata;
&getroots($docroot, $cgiroot, $dataroot);

$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);

$qryname="p$username";
chop($qryname);
$qrytype="agent";

if ($usertoken eq "") {
  &seekit_notloggedin;
}

else {
  &seekit_query($qryname, $qrytype);
  &seekit_endpage("Agent Results","$cgiroot/results.$seekit_perlext?Username=$username&Token=$usertoken");
}
