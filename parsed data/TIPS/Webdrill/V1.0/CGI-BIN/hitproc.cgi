#!/usr/bin/perl5
require "seekitlb.pl";
require "mailer.pl";

# name: hitproc.pl
# Author: Andrew Fregly
# Function: Is invoked from the results summary page of the Seekit web demo.
# remove will delete from the users hit file each hit which was marked on the
# results summary page.
 
# Write standard http and html header back to the client.
&htmlheader;

# Get httpdata fields from query portion of the URL.
&gethttpdata;

#foreach $akey (sort(keys(%contents))) {
#  print "$akey: $contents{$akey}<BR>\n";
#}
#print "</P><P>\n";
# Get base roots for file locations
&getroots($docroot, $cgiroot, $dataroot, $textroot, $tipsroot, $textref);

# See if valid logon is active for the user
$username=$contents{'Username'};
$usertoken=$contents{'Token'};
&getusertoken($username, $usertoken);
$qryname = $contents{'Queryname'};
$outname = $qryname;
chop $outname;
$outname = "_".$outname;
$qrytype = $contents{'Querytype'};
$s = $seekit_dirsep;
$x = $seekit_exe;
$searchdir = $dataroot.$s."searches";

if ($usertoken eq "") {
  &seekit_notloggedin;
}  
elsif ($contents{'Remove'}) {
  $n = 0;
  # $cmd = $tipsroot.$s."hitr".$x." -t ".$searchdir.$s.$qryname.".hit 2>$seekit_nulldev";
  if ($seekit_unix) {
    $cmd = "cat $searchdir$s$qryname.hit";
  }
  else {
    $cmd = "type $searchdir$s$qryname.hit";
  }
  @hitlist = `$cmd`;
  $outname = $searchdir.$s.$outname.".tmp";
  if ( open (OUTFILE, ">$outname")) {
    foreach $hit (@hitlist) {
      ($doc, $doclen, $queryname) = split(/,/,$hit);
      $doc =~ s/.*([\/\\])/$1/;
      if ($contents{$doc} ne 'on') {
        print OUTFILE $hit;
      }
      else {
        $n += 1;
      }
    }
    close OUTFILE;
  }
  else {
    print "*** Error opening work file: $outname</P><P>\n";
  }
  if ($n) {
    unlink $searchdir.$s.$qryname.".hit";
    unlink $searchdir.$s.$qryname.".drc";
    $cmd = $tipsroot.$s."hitw".$x." -h -f ".$outname." 2>$seekit_nulldev";
    $kept = `$cmd`;
    if ($n > 1) {
      print "Removed $n hits<HR>\n";
    }
    else {
      print "Removed hit<HR>\n";
    }
  }
  else {
    print "No hits were removed<BR>\n";
  }
  unlink $outname;
}
elsif ($contents{'Mail'}) {
  $n = 0;
  $cmd = "cat ".$searchdir.$s.$qryname.".hit";
  $hits = `$cmd`;
  @hitlist=split(/\n/, $hits);
  &seekit_getuserinfo($username, $dataroot, *userinfo);
  if (!$userinfo{'E-Mail'} || $userinfo{'E-Mail'} eq "") {
    print "You have not specified an e-mail address for yourself<BR>\n";
  }
  else {
    $count = 0;
    foreach $hit (@hitlist) {
      ($docpath, $doclen, $queryname) = split(/,/,$hit);
      if ($seekit_unix) {
        @docname = split(/\//, $docpath);
      }
      else {
        @docname = split(/\\/, $docpath);
      }
      $doc = $s.$docname[$docname-1];
      if ($contents{$doc} eq 'on') {
	$count += 1;
	&seekit_highlite($queryname, $docpath, "$username.tmp");
        if (open(IN, "$username.tmp")) {
  	  $linenum = 1;
	  $title = "";
	  undef(@body);
	  for (<IN>) {
	    $linein = $_;
            if ( $linenum == 1) {
              $linein =~ s/^/\Title - /;
            }
            elsif ($linenum == 2) {
              $linein =~ s/^/Body - /;
            }
            $linein =~ s/\xFF\x02/Subject -  /;
            $linein =~ s/\xFF\x03/\Author -  /;
            $linein =~ s/\xFF\x04/Date - /;
            $linein =~ tr/[\x80-\xFF]//d;
            $linein =~ tr/[\x00-\x04]//d;
	    push (@body, $linein);	
	    if ($linenum == 1) {
	      $title = $linein;
	    }
	    $linenum += 1;
          }
          close (IN);
	  $status = "";
          $status = &seekit_smtpmailer($seekit_smtpgateway, $userinfo{'E-Mail'},
	    "SeekWare WebTips Demo - Search Result", *body, "", "", "");
	  if ($status =~ /Success/i) {
	    print "Mailed \"$title\"<BR>\n"; 
	  }
	  else {
	    print "Mail error: $status<BR>..\"$title\"<BR>\n";
	  }
	}
	else {
	  print "<H2>Error processing document: $doc</H2><P>\n"; 
        }
      }
    }
    if (! $count) {
      print "Please use checkbox to select items to mail<BR>\n";
    }
  }
  print "<HR>\n";
}
else {
  print "Function is not yet supported<BR>\n";
}
&seekit_results($qryname, $qrytype);
&seekit_endpage;
