#!/usr/bin/perl5

sub seekit_smtpmailer {
  # $_[0] is the address of the SMTP server to send the mail through.
  # $_[1] is a string containing a list of users to whom the mail should be sent.
  # $_[2] is the subject for the message.
  # $_[3] is a list of lines containing the data for the mail message.
  # $_[4] is the "Reply to:" address.
  # $_[5] is the name of this server.
  # $_[6] is the "From" field value. If not filled in, it will default to "SMTPMAIL".

  local ($port, $them, $AF_INET, $SOCK_STREAM, $sockaddr, $name, $aliases,
	$proto, $port, $type, $len, $thisaddr, $thataddr, $them, $this, $that,
        @ADDRESSEE, $SUBJECT, *DATA, $REPLYTO, $SERVERNAME, $line, $errmsg, $FROM,
	$oldterminator);

  if ($seekit_nosocket) {
    return "Perl support for sockets is inoperable on server";
  }
  else {
    use Socket;
  }

  $them = $_[0];
  $them = $seekit_smtpgateway if ($them eq "");
  @ADDRESSEE=split(/[,;:]/, $_[1]);
  $SUBJECT = $_[2];
  $SUBJECT = "SeekWare Inc. WebTips Notification" if ($SUBJECT eq "");
  *DATA=$_[3];
  push(@DATA,"*** No Data ***") unless ($DATA[0]);
  $REPLYTO=$_[4];
  $REPLYTO=$seekit_administrator if ($REPLYTO eq "");
  $SERVERNAME=$_[5];
  $SERVERNAME='LOCALHOST' if ($SERVERNAME eq "");
  $FROM=$_[6];
  $FROM=$seekit_administrator if (!$FROM || $FROM eq "");

  $port = 25;
  if ($seekit_hassocketdefs) {
    $AF_INET = AF_INET;
    $PF_INET = PF_INET;
    $SOCK_STREAM = SOCK_STREAM;
    $proto = getprotobyname('tcp');
  }
  else {
    $AF_INET = 2;
    $PF_INET = 2;
    $SOCK_STREAM = 1;
    $proto=6;
  }
  $SIG{'INT'} = 'seekit_dokill';
  $errmsg = "";

  sub seekit_dokill {
      kill 9,$child if $child;
  }

  $sockaddr = 'S n a4 x8';

  ($name,$aliases,$port) = getservbyname($port,'tcp')
      unless $port =~ /^\d+$/;;
  ($name,$aliases,$type,$len,$thataddr) = gethostbyname($them);

  $that = pack($sockaddr, $AF_INET, $port, $thataddr);
  if (!socket(SOCK, $AF_INET, $SOCK_STREAM, $proto)) { 
    die $!;
  }

  if (!connect(SOCK, $that)) {
    $errmsg = $!;
  }
  else {
    select(SOCK); $| = 1; select(STDOUT);
    $reply=<SOCK>;
    print SOCK "HELO ${SERVERNAME}\n";
    $reply=<SOCK>;
    print SOCK "MAIL FROM:<$FROM>\n";
    $reply=<SOCK>;
    print SOCK "RCPT TO:<@ADDRESSEE[0]>\n";
    $reply=<SOCK>;
    if ($#ADDRESSEE > 0) { 
      foreach (1..$#ADDRESSEE) {
        print SOCK "RCPT TO: @ADDRESSEE[$_]\n";
        $reply=<SOCK>;
      }
    }
    print SOCK "DATA \n";
    $reply=<SOCK>;
    print SOCK "To: $ADDRESSEE[0]\n";
    if ($#ADDRESSEE > 0) { 
      foreach (1..$#ADDRESSEE) { 
        print SOCK "Cc: $ADDRESSEE[$_]\n"; 
      }
    }
    print SOCK "Subject: $SUBJECT\n";
    print SOCK "Reply-To: $REPLYTO\n";
    # Display each field and responses
    foreach $line (@DATA) {
     $line =~ s/^\./_./;
     print SOCK "$line";
    }
    print SOCK $seekit_smtpbye;
    $reply=<SOCK>;
    # Quit
    print SOCK "QUIT";
    # Read in the response and print it.
    $oldterminator = $/;
    undef $/;  $_=<DATA>;  print;
    shutdown(SOCK,2);
    $/=$oldterminator;
    $errmsg="Success";
  }
  return $errmsg;
}
  1;
