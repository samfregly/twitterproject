/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_swap						spotswap.c

Function : Swaps two file names.

Author : A. Fregly

Abstract : This function is used to exchange the contents of two string
	buffers which contain file names, or any other type of string
	data up to FIT_FULLFSPECLEN bytes in length.

Calling Sequence : spot_swap(char *str1, char *str2);

Notes: 

Change log : 
000	02-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_swap(char *str1, char *str2)
#else
void spot_swap(str1, str2)
  char *str1, *str2;
#endif
{
  char tempstr[FIT_FULLFSPECLEN+1];

  strncpy(tempstr, str1, FIT_FULLFSPECLEN);
  tempstr[FIT_FULLFSPECLEN] = 0;
  strncpy(str1, str2, FIT_FULLFSPECLEN);
  str1[FIT_FULLFSPECLEN] = 0;
  strcpy(str2, tempstr);
}

