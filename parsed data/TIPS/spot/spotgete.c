/*****************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.
*******************************************************************************/

/*****************************************************************************
 Module Name : spot_getenv					spotgete.c

Function : Sets up initial value for spot environmental variables.

Author : A. Fregly

Abstract : This function sets up spot environmental variables based on
	the user specified values. These values are determined by querying
	the process environment to determine the values of corresponding
	process environment variables. Normally, these values are set up
	by invocation of the $HOME/.spotrc file, which defines the 
	environmental variables.

Calling Sequence : int stat = spot_getenv(struct spot_s_env *env);

  env		Returned environment structure.
  stat		Returned status, 0 indicates a processing error.

Notes: 

Change log : 
000	03-SEP-91  AMF	Created.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
int spot_getenv(struct spot_s_env *env)
#else
int spot_getenv(env)
  struct spot_s_env *env;
#endif
{
  int count, i, j, n, enablemask, opts, cmdlen;
  char nextaction[3*FIT_FULLFSPECLEN], *endcmd;
  char cmd[FIT_FULLFSPECLEN+1], *menuopt;
  char *varptr;

  /* Initial view mode */
  env->view = SPOT_VIEWRESULTS;
  if ((varptr = getenv("SP_VIEWMODE")) != NULL) {
    if (!strcmp(varptr,"FILES"))
      env->view = SPOT_VIEWFILES;
    else if (!strcmp(varptr,"STREAMS"))
      env->view = SPOT_VIEWSTREAMS;
    else if (!strcmp(varptr,"RESULTS"))
      env->view = SPOT_VIEWRESULTS;
    else if (!strcmp(varptr,"ARCHIVES"))
      env->view = SPOT_VIEWARCHIVES;
    else if (!strcmp(varptr,"QUERIES"))
      env->view = SPOT_VIEWQUERIES;
    else if (!strcmp(varptr,"STREAMDETAIL"))
      env->view = SPOT_VIEWSTREAMDETAIL;
  }

  tips_getattributes(&env->usecolor, &env->usepointer, &env->usecheckmarks,
    &env->usehotkeys, &env->uselinedrawing, &env->dispnorm, &env->disphigh, 
    &env->dispsel, &env->disphot, &env->mennorm, &env->menhigh, &env->mensel, 
    &env->menhot);

  if (env->uselinedrawing) {
    env->popupnorm = env->dispnorm;
    env->popuphigh = env->disphigh;
    env->popupsel = env->dispsel;
    env->popuphot = env->disphot;
  }
  else {
    env->popupnorm = env->mennorm;
    env->popuphigh = env->menhigh;
    env->popupsel = env->mensel;
    env->popuphot = env->menhot;
  }

  if ((varptr = getenv("SP_FILES")) != NULL)
    strncpy(env->filedir,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(env->filedir,".");
  env->filedir[FIT_FULLFSPECLEN] = 0;
  fit_abspath(env->filedir, spot_curpath, env->filedir);

  if ((varptr = getenv("SP_FILEFILTER")) != NULL)
    strncpy(spot_filefilter,varptr,FIT_FULLFSPECLEN);
  else
    spot_filefilter[0] = 0;
  spot_filefilter[FIT_FULLFSPECLEN] = 0;

  if ((varptr = getenv("SP_DFILES")) != NULL)
    strncpy(spot_filedest,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(spot_filedest, env->filedir);
  spot_filedest[FIT_FULLFSPECLEN] = 0;
  fit_abspath(spot_filedest, spot_curpath, spot_filedest);

  if ((varptr = getenv("SP_SEARCHES")) != NULL)
    strncpy(env->searchdir,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(env->searchdir,".");
  env->searchdir[FIT_FULLFSPECLEN] = 0;
  fit_abspath(env->searchdir, spot_curpath, env->searchdir);

  if ((varptr = getenv("SP_DSEARCHES")) != NULL)
    strncpy(spot_searchdest,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(spot_searchdest, env->searchdir);
  spot_searchdest[FIT_FULLFSPECLEN] = 0;
  fit_abspath(spot_searchdest, spot_curpath, spot_searchdest);

  if ((varptr = getenv("SP_ARCHIVES")) != NULL)
    strncpy(env->arcdir,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(env->arcdir,".");
  env->arcdir[FIT_FULLFSPECLEN] = 0;
  fit_abspath(env->arcdir, spot_curpath, env->arcdir);

  if ((varptr = getenv("SP_DARCHIVES")) != NULL)
    strncpy(spot_arcdest,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(spot_arcdest, env->arcdir);
  spot_arcdest[FIT_FULLFSPECLEN] = 0;
  fit_abspath(spot_arcdest, spot_curpath, spot_arcdest);

  if ((varptr = getenv("SP_STREAMS")) != NULL)
    strncpy(env->streamdir,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(env->streamdir,".");
  env->streamdir[FIT_FULLFSPECLEN] = 0;
  fit_abspath(env->streamdir, spot_curpath, env->streamdir);

  if ((varptr = getenv("SP_DSTREAMS")) != NULL)
    strncpy(spot_streamdest,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(spot_streamdest, env->streamdir);
  spot_streamdest[FIT_FULLFSPECLEN] = 0;
  fit_abspath(spot_streamdest, spot_curpath, spot_streamdest);

  if ((varptr = getenv("SP_STREAMNAME")) != NULL)
    fit_expfspec(varptr, ".act", env->streamdir, env->streamname);
  else {
    env->streamname[0] = 0;
    if (env->view == SPOT_VIEWSTREAMDETAIL) env->view = SPOT_VIEWSTREAMS;
  }
  env->streamname[FIT_FULLFSPECLEN] = 0;
  strcpy(spot_streamdestname, env->streamname);

  if ((varptr = getenv("SHELL")) != NULL)
    strncpy(spot_shell, varptr, FIT_FULLFSPECLEN);
  else
    strcpy(spot_shell, "sh");
  spot_shell[FIT_FULLFSPECLEN] = 0;

  if ((varptr = getenv("SP_FIELDFILE")) != NULL)
    strncpy(spot_defaultfields, varptr, FIT_FULLFSPECLEN);
  else
    spot_defaultfields[0] = 0;
  spot_defaultfields[FIT_FULLFSPECLEN] = 0;

  if ((varptr = getenv("SP_FIELDDELIM")) != NULL)
    sscanf(varptr,"%d", &spot_defaultdelim);
  else
    spot_defaultdelim = 0xFF;

  if ((varptr = getenv("SP_MAILER")) != NULL)
    strncpy(spot_mailer, varptr, FIT_FULLFSPECLEN);
  else
    strcpy(spot_mailer, "spotmail");
  spot_mailer[FIT_FULLFSPECLEN] = 0;

  /* Get editors defined by environment variables SP_EDITOR1, SP_EDITOR2,... 
     SP_EDITORn. */
  spot_envseq("SP_EDITOR", spot_defaulteditor, &spot_editor, &spot_editortype,
    &spot_neditor);
  if (!*spot_defaulteditor) strcpy(spot_defaulteditor, "vi");

  /* Get print commands defined by SP_PRINTCMD1, SP_PRINTCMD2, ... */
  spot_envseq("SP_PRINTCMD", spot_defaultprintcmd, &spot_printcmd,
    &spot_printtype, &spot_nprintcmd);
  if (!*spot_defaultprintcmd) strcpy(spot_defaultprintcmd, "spotprt");

  if ((varptr = getenv("SP_TIMER")) != NULL)
    sscanf(varptr,"%d", &env->updateinterval);
  else
    env->updateinterval = 30;

  if ((varptr = getenv("SP_PRINTER")) != NULL)
    strncpy(env->printer,varptr,FIT_FULLFSPECLEN);
  else
    strcpy(env->printer,"spot.prt");
  env->printer[FIT_FULLFSPECLEN] = 0;

  if ((varptr = getenv("SP_PAGE")) != NULL)
    sscanf(varptr,"%d", &env->pagelen);
  else
    env->pagelen = 58;

  if ((varptr = getenv("SP_WRAP")) != NULL)
    sscanf(varptr,"%d", &env->wrap);
  else
    env->wrap = 80;

  if ((varptr = getenv("SP_LEFT")) != NULL)
    sscanf(varptr,"%d", &env->leftmargin);
  else
    env->leftmargin = 0;

  if ((varptr = getenv("SP_TOP")) != NULL)
    sscanf(varptr,"%d", &env->topmargin);
  else
    env->topmargin = 3;

  if ((varptr = getenv("SP_SEPARATOR")) != NULL)
    sscanf(varptr,"%d", &env->separator);
  else
    env->separator = CTRLD;

  tips_getfunckeys(&spot_menukey, &spot_menukeyname, &spot_helpkey,
	&spot_helpkeyname, &spot_exitkey, &spot_exitkeyname, &spot_quitkey, 
	&spot_quitkeyname);

  /* Initialize action lists for main menu */

  /* Initialize all of the lists */
  for (i=0; i <= SPOT_MAXVIEW; ++i) {
    spot_actions[i].nopts = 0;
    spot_actions[i].menuwidth = 0;
    spot_actions[i].opts = NULL;
    spot_actions[i].cmds = NULL;
    spot_actions[i].menuitems = NULL;
    spot_actions[i].win = NULL;
    spot_actions[i].menu = NULL;
  }

  /* Count how many actions are defined */
  for (count = 0;; ++count) {
    sprintf(nextaction,"SP_ACTION%d", count+1);
    if ((varptr = getenv(nextaction)) == NULL) break;
  }

  if (count) {
    /* Allocate the dynamic portions of the action descriptors */
    for (i=0; i <= SPOT_MAXVIEW; ++i) {
      spot_actions[i].opts = 
	(int *) calloc(count, sizeof(*(spot_actions[i].opts)));
      spot_actions[i].cmds = 
	(char *(*)) calloc(count, sizeof(*(spot_actions[i].cmds)));
      spot_actions[i].menuitems = 
	(char *(*)) calloc(count+2, sizeof(*(spot_actions[i].menuitems)));
    }

    /* Load in the action descriptors */
    for (i=0; i < count; ++i) {
      sprintf(nextaction,"SP_ACTION%d", i+1);
      if ((varptr = getenv(nextaction)) == NULL) break;
      enablemask = 0;
      opts = 0;
      if (*varptr == '-') {
	/* Pull off switches */
	for (++varptr; *varptr && *varptr != ' '; ++varptr) {
	  switch (tolower(*varptr)) {
	    case 'a' :
	      enablemask |= 1 << SPOT_VIEWARCHIVES;
	      break;
	    case 'b' :
	      enablemask |= 1 << SPOT_MAXVIEW;
	      break;
	    case 'd' :
	      enablemask |= 1 << SPOT_VIEWSTREAMDETAIL;
	      break;
	    case 'f' :
	      enablemask |= 1 << SPOT_VIEWFILES;
	      break;
	    case 'l' :
	      opts |= ACTION_MASK_PARAMLIST;
	      break;
	    case 'o' :
	      opts |= ACTION_MASK_ONEPARAM;
	      break;
	    case 'p' :
	      opts |= ACTION_MASK_PIPEPARAMS;
	      break;
	    case 'q' :
	      enablemask |= 1 << SPOT_VIEWQUERIES;
	      break;
	    case 'r' : 
	      enablemask |= 1 << SPOT_VIEWRESULTS;
	      break;
	    case 's' :
	      enablemask |= 1 << SPOT_VIEWSTREAMS;
	      break;
	    case 'x' :
	      opts |= ACTION_MASK_NOPARAMS;
	      break;
	    default :
	      ;
	  }
	}
      }
      if (!enablemask) enablemask = ~(1 << SPOT_MAXVIEW);

      while (*varptr == ' ')
	++varptr;

      if (*varptr) {
        endcmd = strchr(varptr,' ');
        if (endcmd == NULL) endcmd = strchr(varptr, 0); 
	cmdlen = endcmd - varptr;
        strncpy(cmd, varptr, cmdlen);
        cmd[cmdlen] = 0;
	varptr = endcmd;
	while (*varptr == ' ')
	  ++varptr;
	if (*varptr)
	  menuopt = varptr;
	else
	  menuopt = cmd;
	for (j=0; j <= SPOT_MAXVIEW; ++j) {
	  if (enablemask & (1 << j)) {
	    n = spot_actions[j].nopts;
	    spot_actions[j].opts[n] = opts;
	    spot_actions[j].cmds[n] = (char *) calloc(cmdlen+1, 
		sizeof(*(spot_actions[j].cmds[n])));
	    strcpy(spot_actions[j].cmds[n], cmd);
	    spot_actions[j].menuitems[n] = (char *) calloc(strlen(menuopt) + 1,
		sizeof(*(spot_actions[j].menuitems[n])));
	    strcpy(spot_actions[j].menuitems[n], menuopt);
	    spot_actions[j].menuwidth = min(COLS-6, 
		max(spot_actions[j].menuwidth, strlen(menuopt)));
	    spot_actions[j].nopts += 1;
	  }
	}
      }
    }

    /* Complete menu options lists for any menu's which were defined */
    for (i=0; i < SPOT_MAXVIEW+1; ++i) {
      if (spot_actions[i].nopts) {
	spot_actions[i].menuitems[spot_actions[i].nopts] = 
	  (char *) calloc(1,1);
	spot_actions[i].menuitems[spot_actions[i].nopts+1] = NULL;
      }
    }
  }

  env->menu = NULL;
  env->prevview = SPOT_VIEWFILES;
  env->summaryitem[0] = 0;

  return 1;
}
