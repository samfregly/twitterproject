/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_doaction					spotdoac.c

Function : Performs action command against items in supplied list.

Author : A. Fregly

Abstract : This function is used to spawn an "action" script or program.
	Depending on the options selected in the action descriptor: the
	action process may be executed once with no parameters; 
	the action process will be executed once with the  items supplied on
	the command line; the action process will be executed once with the
	items supplied one per input line, or the action process will be
	executed once for each item, with the item supplied on the command
	line.

	Before initiating the action, spot_doaction will put a "Working..."
	message on the help line, then call endwin() to end windows processing.
	On completion of the action processing, the curses is restared, the
	help line is restored, and the screen restored. If the action process
	requires time for a user to look at the screen before having it
	restored, it is the responsibility of the action process to provide
	some means of delaying it's termination, either by prompting the
	user to continue, or by a timeout or some other creative means.

Calling Sequence : int stat = spot_doaction(struct spot_s_env *env, 
  struct spot_s_actiondescr *action, int menuopt, char *(*itemlist));

  env		Spot environment structure.
  action	Action descriptor.
  menuopt	Specifies which action function is to be executed.
  itemlist	List of items to be processed. itemlist is terminated by
		either a zero length entry, or a NULL entry.

Notes: 

Change log : 
000	25-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;

#if FIT_ANSI
int spot_doaction(struct spot_s_env *env, struct spot_s_actiondescr *action, 
  int menuopt, char *(*itemlist))
#else
int spot_doaction(env, action, menuopt, itemlist)
  struct spot_s_env *env; 
  struct spot_s_actiondescr *action;
  int menuopt;
  char *(*itemlist);
#endif
{

  char *tempfile, tempinput[FIT_FULLFSPECLEN+1];
  FILE *tempfile_out, *tempinput_out;
  int pid, exitpid, exitstat;

  /* Initialize dynamic stuff */
  tempfile = NULL;
  tempfile_out = NULL;
  tempinput_out = NULL;
  tempinput[0] = 0;

  /* Open work files */

  /* tempfile will be a script which performs the actions */
  tempfile = tmpnam(NULL);
  if (tempfile == NULL) {
    spot_errmessage("Error setting work file name");
    goto DONE;
  }
  if ((tempfile_out = fopen(tempfile, "w")) == NULL) {
    spot_errmessage("Error opening work file");
    goto DONE;
  }

  if (action->opts[menuopt] & ACTION_MASK_PIPEPARAMS) {
    fit_setext(tempfile, ".tmp", tempinput);
    /* tempinput will serves as input for the action script when "piped"
       input is enabled */
    if ((tempinput_out = fopen(tempinput, "w")) == NULL) {
      spot_errmessage("Error opening work file");
      goto DONE;
    }
  }

  if (action->opts[menuopt]  & ACTION_MASK_NOPARAMS)
    /* Script will contain single line invoking the action command */
    fprintf(tempfile_out, "%s\n", action->cmds[menuopt]);

  else if (action->opts[menuopt] & ACTION_MASK_PARAMLIST) {
    /* Script will contain single line invoking the action command, with
       the items as arguments to the command */
    fprintf(tempfile_out, "%s ", action->cmds[menuopt]);
    for (; *itemlist != NULL && (*itemlist)[0]; ++itemlist)
      fprintf(tempfile_out,"%s ", *itemlist);
    fprintf(tempfile_out, "\n");
  }

  else if (action->opts[menuopt] & ACTION_MASK_PIPEPARAMS) {
    /* Script will contain single line invoking the action command, which
       will have as standard input a file containing the items, one per
       line. */
    fprintf(tempfile_out, "%s <%s\n", action->cmds[menuopt], tempinput);
    for (; *itemlist != NULL && (*itemlist)[0]; ++itemlist)
      fprintf(tempinput_out,"%s\n", *itemlist);
  }

  else
    for (; *itemlist != NULL && (*itemlist)[0]; ++itemlist)
      fprintf(tempfile_out,"%s %s\n", action->cmds[menuopt], *itemlist);

  fclose(tempfile_out);
  tempfile_out = NULL;
  chmod(tempfile, S_IEXEC | S_IREAD | S_IWRITE);

  if (tempinput_out != NULL) fclose(tempinput_out);
  tempinput_out = NULL;

  endwin();

  if (!(pid = fork())) {
    /* Execute the script */
    execlp(tempfile, tempfile, (char *) 0);

    /* This should never get executed */
    exit(errno);
  }
  else if (pid == -1)
    /* Error from fork */
    spot_errmessage("Error forking action process");
  else
    while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
      ;
#if FIT_COHERENT
  spot_scrinit();
#endif

DONE:

  if (tempfile_out != NULL) fclose(tempfile_out);
  if (tempfile != NULL) unlink(tempfile);

  if (tempinput_out != NULL) fclose(tempinput_out);
  if (*tempinput) unlink(tempinput);
}
