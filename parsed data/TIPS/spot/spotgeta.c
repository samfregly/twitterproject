/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_getapplication				spotgeta.c

Function : Returns application/editor to use with specified file.

Author : A. Fregly

Abstract : This function will determine the native application which is
	used with the supplied file. Currently, native applications are
	determined based on the file extension. Applications are defined
	for various file extensions by means of environment variables
	SP_EDITOR1, SP_EDITOR2,..., with each such environment variable
	defining a one such linkage. The format of the environment variables
	should be as follows:

		SP_EDITOR="editor .ext"

	where editor is the command needed to invoke the native application,
	and .ext is the file extension for files which should be edited with
	the native application. If no editor is defined for the file type,
	the default editor defined by the environment variable SP_EDITOR
	is returned, and if SP_EDITOR was not defined, "vi" is returned.

Calling Sequence : char *app = spot_getapplication(char *fspec);

	fspec	File specification for which application needs to be
		determined.
	app	Returned native application name.

Notes:
	Eventually, we will need to get sophisticated enough to do an
	analysis of the file to determine what the default application
	should be if no SP_EDITORn is defined for the file type. For
	example, it might be nice to spot executables, and return the
	name of the file itself as the editor.

Change log : 
000	07-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
char *spot_getapplication(char *fspec)
#else
char *spot_getapplication(fspec)
  char *fspec;
#endif
{
  char extension[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int i;

  fit_prsfspec(fspec, dummyspec, dummyspec, dummyspec, dummyspec, extension,
	dummyspec);
  for (i=0; i < spot_neditor; ++i)
    if (!strcmp(spot_editortype[i], extension))
      return spot_editor[i];
  return spot_defaulteditor;
}
