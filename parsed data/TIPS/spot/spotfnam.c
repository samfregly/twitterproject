/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fname					spotfnam.c

Function : Pulls out file name portion of file display menu entry

Author : A. Fregly

Abstract : 

Calling Sequence : spot_fname(char *optval, char *fname);

  optval	File display menu option from which file name is to be
		extracted.
  fname		Returned file name portion of optval.

Notes: 

Change log : 
000	06-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_fname(char *optval, char *fname)
#else
void spot_fname(optval, fname)
  char *optval, *fname;
#endif
{
  char *spaceptr;
  int len;

  spaceptr = strchr(optval,' ');
  if (spaceptr != NULL)
    len = min(spaceptr - optval, FIT_FULLFSPECLEN);
  else
    len = FIT_FULLFSPECLEN;

  if (len)
    strncpy(fname, optval, len);

  fname[len] = 0;
}
