/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_scrsave					spotscrs.c

Function : Saves current spot display.

Author : A. Fregly

Abstract : This function will take a snapshot of the spot display windows.

Calling Sequence : spot_scrsave(WINDOW *(*cmdwin), WINDOW *(*labelwin), 
  WINDOW *(*dispwin), WINDOW *(*footerwin), WINDOW *(*promptwin));

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_scrsave(WINDOW *(*cmdwin), WINDOW *(*labelwin), 
  WINDOW *(*dispwin), WINDOW *(*footerwin), WINDOW *(*promptwin))
#else
void spot_scrsave(cmdwin, labelwin, dispwin, footerwin, promptwin)
  WINDOW *(*cmdwin), *(*labelwin), *(*dispwin), *(*footerwin), *(*promptwin);
#endif
{
  if (cmdwin != NULL) win_dup(spot_cmdwin, cmdwin);
  if (labelwin != NULL) win_dup(spot_labelwin, labelwin);
  if (dispwin != NULL) win_dup(spot_dispwin, dispwin);
  if (footerwin != NULL) win_dup(spot_footerwin, footerwin);
  if (promptwin != NULL) win_dup(spot_promptwin, promptwin);
}
