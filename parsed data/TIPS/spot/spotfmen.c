/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fmenu					spotfile.c

Function : Creates a file selector menu.

Author : A. Fregly

Abstract : This function is used to create a file selector menu.

Calling Sequence : struct men_s_menu *filemenu = spot_fmenu(
  struct spot_s_env *env, int viewtype, WINDOW *win, int attrflag, 
  char *filelist[], summaryitem);

  env		Spot environment structure.
  viewtype	View type for which winodw is being built.
  win		Window in which to put menu.
  attrflag	0 - use normal screen attribues from env, else use menu
		attributes.
  filelist	List of files to put in menu, terminated by a "" file name.
  summaryitem	Field or line specifier for vietypes of SPOT_VIEWLINE or
		SPOT_VIEWFIELD.
  filemenu	Returned menu structure.

Notes: 

Change log : 
000	05-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#define DISPVALMAXLEN 512

#if FIT_ANSI
struct men_s_menu *spot_fmenu(struct spot_s_env *env, int viewtype,
  WINDOW *win, int attrflag, char *filelist[], char *summaryitem)
#else
struct men_s_menu *spot_fmenu(env, viewtype, win, attrflag, filelist,
    summaryitem)
  struct spot_s_env *env;
  int viewtype;
  WINDOW *win;
  int attrflag;
  char *filelist[];
  char *summaryitem;
#endif
{

  int menutype, maxoptlen, datalen, selecthigh,len;
  struct men_s_menu *filemenu;
  char *curdefault, *curext, *(*fname);
  char dispval[DISPVALMAXLEN], expfspec[FIT_FULLFSPECLEN+1];

  if (viewtype == SPOT_VIEWFILES || viewtype == SPOT_VIEWQUERIES) {
    for (maxoptlen=0, fname=filelist; *fname != NULL && (*fname)[0]; ++fname) {
      len=strlen(*fname);
      maxoptlen=max(maxoptlen,len);
    }
    maxoptlen=min(maxoptlen,COLS);
  }
  else
    maxoptlen = 0;

  if (filelist == NULL && viewtype < SPOT_VIEWDETAIL ||	
      env->doc.handle == NULL && viewtype >= SPOT_VIEWDETAIL) return NULL;

  menutype = MEN_ATTR_HORIZONTALWRAP + MEN_ATTR_MULTSEL + 
    MEN_ATTR_HORIZONTAL + MEN_ATTR_LEFTJUST + MEN_ATTR_VIRTUAL; 
  if (viewtype <= SPOT_VIEWDETAIL) menutype += MEN_ATTR_HIGHFIRST;
  if (env->usepointer) menutype += MEN_ATTR_POINTER;
  if (env->usecheckmarks) menutype += MEN_ATTR_CHECK;

  /* Create the menu */
  if (attrflag) {
    selecthigh = (viewtype < SPOT_VIEWDETAIL) ? env->mensel : env->mennorm;
    filemenu = men_init(win, menutype, NULL, filelist, env->mennorm, 
	env->menhigh, selecthigh, env->menhot, ACS_RARROW, ACS_DIAMOND, 1, 
	maxoptlen);
  }
  else {
    selecthigh = (viewtype < SPOT_VIEWDETAIL) ? env->dispsel : env->dispnorm;
    filemenu = men_init(win, menutype, NULL, filelist, env->dispnorm, 
	env->disphigh, selecthigh, env->disphot, ACS_RARROW, ACS_DIAMOND, 1, 
	maxoptlen);
  }

  if (filemenu == NULL) return filemenu;

  if (viewtype < SPOT_VIEWDETAIL) {
    curdefault = spot_curdefault(env, viewtype);
    curext = spot_curext(viewtype);
    for (fname = filelist; *fname != NULL && (*fname)[0]; ++fname) {
      fit_expfspec(*fname, curext, curdefault, expfspec);
      datalen = strlen(expfspec) + 1;
      spot_fmtdisp(viewtype, expfspec, dispval);
      if (!men_addopt(filemenu, dispval, datalen, expfspec)) {
	spot_errmessage("Error building menu");
	return filemenu;
      }
    }
  }
  else {
    if (env->doc.handle != NULL) drct_rewind(env->doc.handle);
    env->doc.docnum = -1;
    if (!spot_sumupdate(env, &env->doc, filemenu, viewtype, summaryitem))
      spot_errmessage("Error building menu");
  }
  return filemenu;
}
