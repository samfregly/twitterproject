/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_destination					spotdest.c

Function : Returns pointer at destination directory for current view mode.

Author : A. Fregly

Abstract : 

Calling Sequence : char *destptr = spot_destination(struct spot_s_env *env);

  env		Spot environment structure.
  destptr	Returned pointer at destination name buffer in environment
		structure.

Notes: 

Change log : 
000	24-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
char *spot_destination(struct spot_s_env *env)
#else
char *spot_destination(env)
  struct spot_s_env *env;
#endif
{

  switch (env->view) {
    case SPOT_VIEWQUERIES :
    case SPOT_VIEWRESULTS :
      return spot_searchdest;
    case SPOT_VIEWARCHIVES :
      return spot_arcdest;
    case SPOT_VIEWSTREAMS :
      return spot_streamdest;
    case SPOT_VIEWSTREAMDETAIL :
      return spot_searchdest;
    default :
      return spot_filedest;
  }
}
