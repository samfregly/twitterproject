/******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fselect					spotfsel.c

Function : Allows user to choose files by means of a file selector gadget.

Author : A. Fregly

Abstract : This function allows the user to pick one or more files from
	a menu of files matching the caller supplied criteria. While selecting
	files, the user is able to change directories, select the current or
	"selected files", or quit. These options are provided by a menu which
	is at the top of the selector.

Calling Sequence : spot_fselect(struct spot_s_env *env, int multsel, 
  int newflag, char *path, char *extension, char *(*(*retlist)), char *retpath);

  env		Spot environment structure.
  multsel	Allow multiple files to be selected flag.
  newflag	Specifies that new file is being selected. User is initially
		put into field editing display.
  path		Input path in which to initially start file selection.
  extension	File extension of files which are available for selection.
		A NULL or empty string indicates all files in path are
		available.
  retlist	Returned list of selected files. This list is terminated
		by an entry which is a 0 length string. fit_delfilelist
		may be used to delete the file list once it is no longer
		needed. A value of NULL is returned if the user canceled
		out of the file selector. The files names in retlist will
		not contain a path specification, and will not have a file
		extension if "extension" was provided.
  retpath	Returned path in which files in retlist are found.

Notes: 

Change log : 
000	18-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Fix call to fit_filelist. Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

static int firsttime=1;
static char *cmdopts[] = {"Current","Selected","Path","Quit", ""};

static WINDOW *outerwin, *saveouterwin, *cmdwin, *namewin, *dispwin;
static WINDOW *promptwin;
static struct men_s_menu *cmdmenu, *dispmenu;


#if FIT_ANSI
void spot_fselect(struct spot_s_env *env, int multsel, int newflag, char *path, 
  char *extension, char *(*(*retlist)), char *retpath)
#else
void spot_fselect(env, multsel, newflag, path, extension, retlist, retpath)
  struct spot_s_env *env;
  int multsel, newflag;
  char *path, *extension, *(*(*retlist)), *retpath;
#endif
{

  char *(*filelist);
  char fullpath[FIT_FULLFSPECLEN+1], fspec[FIT_FULLFSPECLEN+1];
  char retext[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  char errmessage[FIT_FULLFSPECLEN + 64 + 1];
  int menutype, key, optselected, i, count, nfiles, mincolwidth;
  int timedout, off, draw, imode;
  unsigned len;
  long curfileopt, retopt;
  int normattr, highattr, selattr, hotattr, mennormattr;
  struct stat statbuf;

  *retlist = NULL;
  strcpy(retpath, path);

  if (firsttime) {
    /* Create windows and command menu */
    firsttime = 0;
    if ((outerwin = newwin(9, COLS, 5, 0)) == NULL) return;
    if ((cmdwin = newwin(1, COLS-2, 6, 1)) == NULL) return;
    if ((dispwin = newwin(5, COLS-2, 7, 1)) == NULL) return;
    if ((namewin = newwin(1, COLS-2, 12, 1)) == NULL) return;
    win_dup(spot_promptwin, &promptwin);
    keypad(namewin, TRUE);
    scrollok(namewin, FALSE);
    menutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_HORIZONTALWRAP;
    if (env->usepointer) menutype += MEN_ATTR_POINTER;
    if (env->usehotkeys) menutype += MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT;
    if ((cmdmenu = men_init(cmdwin, menutype, NULL, cmdopts, env->mennorm,
	env->menhigh, env->mensel, env->menhot, ACS_RARROW, ACS_DIAMOND,
	1, 0)) == NULL) return;
    win_dup(outerwin, &saveouterwin);
  }

  if (outerwin == NULL || cmdwin == NULL || namewin == NULL || dispwin == NULL 
	|| cmdmenu == NULL || saveouterwin == NULL) {
    spot_message("Unable to create file selector display");
    return;
  }

  if (multsel) {
    normattr = env->dispnorm;
    highattr = env->disphigh;
    selattr = env->dispsel;
    hotattr = env->disphot;
    mennormattr = env->mennorm;
    cmdmenu->normal = env->mennorm;
    cmdmenu->curhighlight = env->menhigh;
    cmdmenu->selhighlight = env->mensel;
    cmdmenu->hothighlight = env->menhot;
  }
  else {
    normattr = env->mennorm;
    highattr = env->menhigh;
    selattr = env->mensel;
    hotattr = env->menhot;
    mennormattr = env->dispnorm;
    cmdmenu->normal = env->dispnorm;
    cmdmenu->curhighlight = env->disphigh;
    cmdmenu->selhighlight = env->dispsel;
    cmdmenu->hothighlight = env->disphot;
  }

  /* Save the area of the screen being overwritten */
  overwrite(spot_dispwin, saveouterwin);
  overwrite(spot_promptwin, promptwin);

  if (!multsel || env->uselinedrawing) {
    wattrset(outerwin, normattr);
    win_fill(outerwin, 0, 0, 0, 0, ' ');
  }
  else {
    wattrset(outerwin, mennormattr);
    win_fill(outerwin, 1, 0, 0, 0, ' ');
  }

  if (env->uselinedrawing)
    win_box(outerwin);
    
  wattrset(outerwin, mennormattr);
  win_fill(outerwin, 1, 1, 1, COLS-2, ' ');
  win_fill(outerwin, 7, 1, 1, COLS-2, ' ');

  /* Initialize the selector windows */
  wattrset(cmdwin, mennormattr);
  win_fill(cmdwin, 0, 0, 0, 0, ' ');
  overwrite(cmdwin, spot_dispwin);
  overwrite(cmdwin, outerwin);

  wattrset(namewin, mennormattr);
  win_fill(namewin, 0, 0, 0, 0, ' ');
  wmove(namewin, 0, 0);
  waddstr(namewin,"Path: ");
  overwrite(namewin, outerwin);

  wattrset(dispwin, normattr);
  win_fill(dispwin, 0, 0, 0, 0, ' ');
  overwrite(dispwin, outerwin);

  touchwin(outerwin);
  wrefresh(outerwin);
  overwrite(outerwin, spot_dispwin);

  /* Set up the menu type for the display window */
  menutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_HORIZONTALWRAP + 
	MEN_ATTR_VERTICALWRAP + MEN_ATTR_LEFTJUST; 
  if (env->usepointer) menutype += MEN_ATTR_POINTER;
  if (env->usecheckmarks) menutype += MEN_ATTR_CHECK;
  if (multsel) menutype += MEN_ATTR_MULTSEL;

  dispmenu = NULL;
  cmdmenu->curopt = 0;

  /* OUTER loop */
  while (1) {

    /* Display current path in selector path window */
    wattrset(namewin, mennormattr);
    win_fill(namewin, 0, 6, 0, 0, ' ');
    wmove(namewin, 0, 6);
    if ((len = strlen(retpath)) > 1 && retpath[len-1] == '/')
      retpath[len-1] = 0;
    waddstr(namewin, retpath);
    if (len && !retpath[len-1])
      retpath[len-1] = '/';
    overwrite(namewin, spot_dispwin);
    wrefresh(namewin);

    /* Force the display window to be updated. */
    wrefresh(dispwin);
    wrefresh(spot_dispwin);

    /* Build a new file display for the current path */
    if ((filelist = fit_filelist(retpath, extension, S_IFREG, 
	spot_curpath, &nfiles, fullpath)) != NULL) {
      mincolwidth = (nfiles < 4) * 14;
      dispmenu = men_init(dispwin, menutype, NULL, filelist, normattr,
	highattr, selattr, hotattr, ACS_RARROW, ACS_DIAMOND,
	1, mincolwidth);
      if (dispmenu != NULL) {
        men_draw(dispmenu);
        men_put(dispmenu, spot_dispwin);
      }
    }

    /*  Allow user to toggle back and forth between file selection and
	selecotr command menu, and file name window until they exit selector 
	or decide to change the path for the file display */
    /* MIDDLE LOOP */
    while (1) {
      optselected = 0;
      if (dispmenu != NULL && !newflag) {
	/* Files are in display, allow user to select from file display */
        /* Clear the command menu so that they know they are in file select
	   mode */
        wattrset(cmdwin, cmdmenu->normal);
        win_fill(cmdwin, 0, 0, 0, 0, ' ');
        wrefresh(cmdwin);
	if (multsel)
	  spot_puthelpline(spot_multselectorhelp);
	else
	  spot_puthelpline(spot_selectorhelp);
	newflag = 0;

	/* Allow them to do their selecting */
	while (!optselected) {
          key = men_select(dispmenu, 0, &curfileopt, &optselected, &timedout);
	  if (!optselected) {
	    if (key == '\t' || key == KEY_NEXT || key == KEY_SNEXT ||
		key == spot_menukey)
	      break;
	    else if (key == spot_exitkey || key == spot_quitkey || 
		key == KEY_EXIT || key == KEY_SEXIT || key == KEY_CANCEL || 
		key == KEY_SCANCEL || key == ('z' - 'a' + 1) ||
		key == ('d' - 'a' + 1)) {
	      men_reset(dispmenu);
	      goto OUT_OUTER;
	    }
	    else if (key == spot_helpkey)
	      spot_help();
	  } 
	}
      }
      else
	key = '\t';

      if (key == '\t' || key == KEY_NEXT || key == KEY_SNEXT) {
	newflag = 0;
	imode = 0;
	strcpy(fspec, retpath);
	off = min(strlen(fspec), COLS - 7 - 2 - 1);
	draw = 1;
	spot_puthelpline(spot_fieldedithelp);

	while (1) {
	  win_edit(namewin, fspec, COLS - 7 - 2, &imode, draw, 0, 0, 6, &off, 
		&key);

	  draw = 0;
	  if (key == spot_menukey) key = '\t';

	  switch (key) {
	    case '\n' : case '\r' : case KEY_ENTER :
	      len = 0;
	      fit_strim(fspec, &len);
	      if (fspec[max(0,len-1)] == '/') {
		fspec[len-1] = 0;
		if (stat(fspec, &statbuf) || !(statbuf.st_mode & S_IFDIR)) {
		  sprintf(errmessage,"Unable to access directory: %s", fspec);
		  spot_errmessage(errmessage); 
		}
		else {
		  strcpy(retpath, fspec);
		  retpath[len-1] = '/';
		  retpath[len] = 0;
		  goto OUT_MIDDLE;
		}
	      }
	      else if (!stat(fspec, &statbuf) && (statbuf.st_mode & S_IFDIR)) {
		strcpy(retpath, fspec);
		retpath[len] = '/';
		retpath[len+1] = 0;
		goto OUT_MIDDLE;
	      }
	      else {
	        fit_expfspec(fspec, extension, retpath, fullpath);
	        fit_prsfspec(fullpath, dummyspec, dummyspec, fullpath, fspec,
		  retext, dummyspec);
	        len = strlen(fullpath);
	        if (len > 1) fullpath[len-1] = 0;
	        if (stat(fullpath, &statbuf) || !(statbuf.st_mode & S_IFDIR)) {
	          sprintf(errmessage,"Unable to access dirctory: %s", fullpath);
	          spot_message(errmessage);
	        }
	        else if (*fspec || strcmp(extension, retext)) {
	          if (len > 1) fullpath[len-1] = '/';
	          strcpy(retpath, fullpath);
                  if ((*retlist = (char *(*)) calloc(2, sizeof(*(*retlist)))) ==
		      NULL)
	            goto RETLIST_ALLOC_ERROR;
	          strcat(fspec, retext);
	          if (((*retlist)[0] = (char *) calloc(strlen(fspec)+1, 1)) == 
		      NULL)
		    goto RETLIST_ALLOC_ERROR;
	          strcpy((*retlist)[0], fspec);
	          goto DONE;
	        }
	      }
	      break;
	    
	    case KEY_CANCEL : case KEY_EXIT : case KEY_SCANCEL : 
	    case KEY_SEXIT : 
#if FIT_LINUX
	    case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
	      goto DONE;
	      break;

	    case KEY_HELP : case KEY_SHELP :
	      break; 

	    case KEY_REFRESH : case CTRLW :
	      endwin();
	      wrefresh(namewin);

	    case KEY_UP : case KEY_PREVIOUS : case KEY_PPAGE :
	    case KEY_SPREVIOUS : case 'n' - 'a' + 1 :
	    case KEY_NEXT : case KEY_SNEXT : case '\t' : case KEY_DOWN :
	    case KEY_NPAGE : case 'p' - 'a' + 1 :
    	      win_fill(namewin, 0, 6, 0, 0, ' ');
	      if ((len = strlen(retpath)) && retpath[len-1] == '/')
		retpath[len-1] = 0;
    	      mvwaddstr(namewin, 0, 6, retpath);
	      if (len && !retpath[len-1])
		retpath[len-1] = '/';
    	      wrefresh(namewin);
	      overwrite(namewin, outerwin);
	    
	      if (key == KEY_NEXT || key == KEY_SNEXT || key ==  '\t' || 
		  key == KEY_DOWN || KEY_NPAGE || key == 'n' - 'a' + 1)
	        goto OUT_ENTERLOOP; 
	      else
	        goto OUT_MIDDLE;
	      break;

	    default :
	      if (key == spot_exitkey || key == spot_quitkey)
		goto DONE;
	      else if (key == spot_helpkey)
		spot_help();
	  }
	}
      }

OUT_ENTERLOOP:

      /* Draw the command menu so that user knows they are in command mode */
      if (!optselected || dispmenu == NULL || 
	  optselected && men_nselected(dispmenu)) {
	if (dispmenu != NULL)
	  if (!men_nselected(dispmenu))
	    cmdmenu->curopt = 0;
	  else
	    cmdmenu->curopt = 1;
	else
	  cmdmenu->curopt = 3;
        men_draw(cmdmenu);
        wrefresh(cmdwin);
        optselected = 0;

        /* Keep user in command menu until they choose some function which exits
	   it */
	for (key=0, optselected = 0; 
	    key != '\t' && key != KEY_NEXT && key != KEY_SNEXT && 
	    key != KEY_UP && key != KEY_DOWN && key != KEY_NPAGE && 
	    key != KEY_PPAGE && key != KEY_PREVIOUS && key != KEY_SPREVIOUS &&
	    key != 'n' - 'a' + 1 && key != 'p' - 'a' + 1 && !optselected;) {
	  /* Get user menu options */
	  spot_puthelpline(spot_selectorcmdhelp);
          key = men_select(cmdmenu, 0, &retopt, &optselected, &timedout);
	  if (!optselected) {
	    retopt = -1;
	    if (key == spot_exitkey || key == spot_quitkey || 
		key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
		key == ('d' - 'a' + 1)) {
	      men_reset(dispmenu);
	      goto OUT_OUTER;
	    }
	    else if (key == spot_helpkey) {
	      spot_help();
	      men_draw(cmdmenu);
	      touchwin(cmdmenu->optwin);
	      wrefresh(cmdmenu->optwin);
	    }
	  }
	}

  	wattrset(cmdwin, mennormattr);
	win_fill(cmdwin, 0, 0, 0, 0, ' ');
	touchwin(cmdwin);
	wrefresh(cmdwin);
	overwrite(cmdwin, outerwin);
	overwrite(outerwin, spot_dispwin);

	newflag = (key == KEY_UP || key == KEY_PPAGE || key == KEY_PREVIOUS ||
	    key == KEY_SPREVIOUS || key == 'p' - 'a' + 1);

	/* Make sure spot display window is current so that other popups
	   can restore the screen correctly */
        overwrite(cmdwin, spot_dispwin);
	if (dispmenu != NULL && dispmenu->optwin != dispwin) {
	  overwrite(dispmenu->optwin, dispwin);
	  overwrite(dispwin, spot_dispwin);
	}
      }
      else
	retopt = 0;

      switch (retopt) {
  	case 0 :
	  if (dispmenu != NULL) {
	    men_reset(dispmenu);
	    dispmenu->options[curfileopt].descr.stat = 1;
	    goto OUT_OUTER;
	  }
	  else
	    spot_message("No files in current path");
	  break;

	case 1 : 
	  /* Selected */
	  if (dispmenu != NULL)
	    for (i=0, count = 0; i < dispmenu->nopts; ++i)
	      count += (dispmenu->options[i].descr.stat != 0);

	  if (dispmenu == NULL || !count)
	    spot_message("No files are currently selected");
          else
	    goto OUT_OUTER;
	  break;

	case 2 :
	  /* Path */
	  spot_setpath(env, retpath, retpath);
	  goto OUT_MIDDLE;

	case 3 :
	  /* Quit */
	  if (dispmenu != NULL)
	    men_reset(dispmenu);
	  goto OUT_OUTER;

	default :
	  goto OUT_MIDDLE;
      }
    }
OUT_MIDDLE:
    if (filelist != NULL)
      fit_delfilelist(&filelist);

    if (dispmenu != NULL) {
      wattrset(dispmenu->optwin, normattr);
      win_fill(dispmenu->optwin, 0, 0, 0, 0, ' ');
      if (dispmenu->optwin != dispmenu->win)
        overwrite(dispmenu->optwin, dispmenu->win);
      overwrite(dispmenu->win, spot_dispwin);
      wrefresh(dispmenu->optwin);
      men_dealloc(&dispmenu);
    }
  }
OUT_OUTER:

  if (dispmenu != NULL) {
    for (i=0, count = 0; i < dispmenu->nopts; ++i)
      count += (dispmenu->options[i].descr.stat != 0);
    if (count) {
      if ((*retlist = 
	  (char *(*)) calloc(count+2, sizeof(*(*retlist)))) == NULL)
	goto RETLIST_ALLOC_ERROR;
      else {
        for (i=0, count=0; i < dispmenu->nopts; ++i) {
	  if (dispmenu->options[i].descr.stat) {
	    if (((*retlist)[count] = 
		(char *) calloc(1, strlen(dispmenu->options[i].val) + 1)) ==
		NULL)
	      goto RETLIST_ALLOC_ERROR;
	    else
	      strcpy((*retlist)[count++], dispmenu->options[i].val);
	  }
	}
	if (((*retlist)[count++] = (char *) calloc(1,1)) == NULL)
	  goto RETLIST_ALLOC_ERROR;
	(*retlist)[count] = NULL;
      }  
    }
  }
  goto DONE;

RETLIST_ALLOC_ERROR:
  spot_errmessage("Error allocating file list structure");
  if (*retlist != NULL) fit_delfilelist(retlist);

DONE:
  if (filelist != NULL) fit_delfilelist(&filelist);
  if (dispmenu != NULL) men_dealloc(&dispmenu);

  /* Restore screen */
  overwrite(saveouterwin, spot_dispwin);
  overwrite(promptwin, spot_promptwin);
  wrefresh(spot_dispwin);
  wrefresh(spot_promptwin);
}
