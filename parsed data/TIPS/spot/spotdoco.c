/*******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_docopy					spotdoco.c

Function : Determines destination for copy and performs document copy.

Author : A. Fregly

Abstract :

Calling Sequence : int stat = spot_docopy(struct spot_s_env *env, int desttype, 
	int useselected);

  env		Spot environment structure.
  desttype	Destination type, FILEDEST, RESULTS, ARCHIVES, DIRECTORY,
		or STREAM.
  useselected	nonzero, then selected items are copied, otherwise only the
		current item is copied.

Notes:

Change log :
000	20-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;

#if FIT_ANSI
int spot_docopy(struct spot_s_env *env, int desttype, int useselected)
#else
int spot_docopy(env, desttype, useselected)
  struct spot_s_env *env;
  int desttype; 
  int useselected;
#endif
{

  struct spot_s_env destenv;
  char *ext, *destpath, retpath[FIT_FULLFSPECLEN+1], *(*filelist);
  char destspec[FIT_FULLFSPECLEN+1], message[FIT_FULLFSPECLEN + 64];
  char streamdir[FIT_NAMELEN+1],streamname[FIT_NAMELEN+1],dummy[FIT_NAMELEN+1];
  char srcspec[TIPS_DESCRIPTORMAX+1], qrylib[FIT_FULLFSPECLEN+1]; 
  char qryspec[FIT_FULLFSPECLEN+1];
  char srclib[FIT_FULLFSPECLEN+1], tempout[FIT_FULLFSPECLEN+1];
  char temphits[FIT_FULLFSPECLEN+1], sumfspec[TIPS_DESCRIPTORMAX+1];
  int newflag, ftype, pid, exitpid, exitstat, count, len;
  long i, startopt, endopt;
  struct men_s_option *opt;
  FILE *out, *actfile, *tempouthandle;
  char *tempfile;
  WINDOW *promptwin;
  struct drct_s_file *temphandle;

  newflag = 0;
  destenv = *env;
  tempfile = NULL;
  temphandle = NULL;
  out = NULL;
  promptwin = NULL;
  temphits[0] = 0;

  switch (desttype) {
    case FILEDEST : case DIRECTORY :
      if (env->view == SPOT_VIEWQUERIES || env->view >= SPOT_VIEWDETAIL &&
	  env->prevview == SPOT_VIEWQUERIES) {
	destenv.view = SPOT_VIEWQUERIES;
	destpath = spot_searchdest;
      }
      else {
        destenv.view = SPOT_VIEWFILES;
        destpath = spot_filedest;
      }
      newflag = 1;
      break;
    case RESULTS :
      destenv.view = SPOT_VIEWRESULTS;
      destpath = spot_searchdest;
      break;
    case ARCHIVES :
      destenv.view = SPOT_VIEWARCHIVES;
      destpath = spot_arcdest;
      break;
    case STREAM :
      destenv.view = SPOT_VIEWSTREAMS;
      destpath = spot_streamdest;
      break;
    default :
      destpath = spot_filedest;
  }

  ext = spot_curext(destenv.view);
  filelist = NULL;

  switch (desttype) {
    case FILEDEST :  
      while (1) {
        if (!spot_fprompt(env, 0, "Enter copy destination file name", NULL,
	    ext, destpath, destspec, destpath, &ftype)) 
	  goto DONE;
        else if (ftype & S_IFDIR) {
	  strcpy(message, "Destination is a directory: ");
	  strcat(message, destspec);
	  spot_message(message);
	}
	else if (ftype) {
	  if (spot_ynprompt(env, "File exists, append selected items?")) 
	    break;
	}
        else
	  break;
      }
      break;

    case DIRECTORY :
      if (!spot_setpath(&destenv, destpath, destspec)) goto DONE;
      if (!strcmp(destspec, spot_curdefault(env, env->view))) {
	spot_message(
	  "Destination directory must be different from source directory");
	goto DONE;
      }
      break;

    default :
      spot_fselect(env, 0, newflag, destpath, ext, &filelist, retpath);
      if (filelist == NULL) goto DONE;
      strcpy(destpath, retpath);
      fit_expfspec(filelist[0], ext, destpath, destspec);
      fit_delfilelist(&filelist);

      if (desttype == STREAM) {
	fit_setext(destspec, ".act", qrylib); 
	if ((actfile = fopen(qrylib, "a")) != NULL)
	  fclose(actfile);
	fit_setext(destspec, ".qlb", qrylib);
      }
  }

  if (useselected) {
    startopt = 0;
    endopt = env->menu->nopts - 1;
  }
  else {
    startopt = env->menu->curopt;
    endopt = env->menu->curopt;
  }

  tempfile = tmpnam(NULL);
  if (tempfile == NULL) {
    spot_errmessage("Error setting work file name");
    goto DONE;
  }
  if ((out = fopen(tempfile, "w")) == NULL) {
    spot_errmessage("Error opening work file");
    goto DONE;
  } 

  if (env->view >= SPOT_VIEWDETAIL) {
    /* Create temp hit file for use with summary items */
    fit_setext(tempfile, ".hit", temphits);
    if ((temphandle = drct_open(temphits, O_RDWR + O_CREAT, 0, -1, -1)) !=
	NULL) {
      for (i=startopt; i <= endopt; ++i) {
        opt = men_opt(env->menu, i);
        if (opt->descr.stat || !useselected) {
	  strcpy(sumfspec, spot_fspec(opt, env->view));
	  len = strlen(sumfspec);
	  sumfspec[len++] = '\n';
	  sumfspec[len] = 0;
	  if (!drct_append(temphandle, sumfspec, len, (long) len)) {
	    spot_errmessage("Error adding data to work file");
	    goto DONE;
	  }
        }
      }
      drct_close(temphandle);
      temphandle = NULL;
    }
    else {
      spot_errmessage("Error creating work file");
      goto DONE;
    }
    startopt = 0;
    endopt = 0;
    useselected = 0;
  }

/******
  fprintf(out, "set -fvx\n");
******/
  if (desttype == STREAM) {
    fprintf(out, "if [ ! -f \"%s\" ]; then\n", qrylib);
    fprintf(out, "  drctio -c %s\n", qrylib);
    fprintf(out, "fi\n");
  }
  else if (desttype == RESULTS || desttype == ARCHIVES) {
    fprintf(out, "if [ ! -f \"%s\" ]; then\n", destspec);
    fprintf(out, "drctio -c %s\n", destspec);
    fprintf(out, "fi\n");
  }

  win_dup(spot_promptwin, &promptwin);
  win_center(spot_promptwin, 0, "Working...");
  wrefresh(spot_promptwin);
  srcspec[TIPS_DESCRIPTORMAX] = 0;

  for (i = startopt; i <= endopt; ++i) { 
    opt = men_opt(env->menu, i);
    strncpy(srcspec, spot_fspec(opt, env->view), TIPS_DESCRIPTORMAX);
    if (env->view == SPOT_VIEWSTREAMDETAIL)
      fit_setext(srcspec, ".hit", srcspec);

    if (opt->descr.stat || !useselected) {
      switch (desttype) {
        case FILEDEST :
	  switch (env->view) {
	    case SPOT_VIEWFILES : case SPOT_VIEWQUERIES :
	      while (i <= endopt) {
	        fprintf(out, "cat ");
	        for (count = 0; count < 50 && i <= endopt; ++i) {
	          opt = men_opt(env->menu, i);
	          if ((opt->descr.stat || !useselected) &&
	              strcmp(spot_fspec(opt, env->view), destspec)) {
	            fprintf(out, "%s ", spot_fspec(opt, env->view));
		    count += 1;
		  }
	        }
	        fprintf(out, ">>%s\n", destspec);
	      }
	      break;
	    case SPOT_VIEWRESULTS : case SPOT_VIEWSTREAMDETAIL :
	      if (strcmp(srcspec, destspec))
	        fprintf(out, "hitr %s >>%s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWARCHIVES :
	      if (strcmp(srcspec, destspec))
	        fprintf(out, "drctio -x %s >>%s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	      fprintf(out, "hitr %s >>%s\n", temphits, destspec);
	      break;
	    default :
	      ;
	  }
	  break;

        case RESULTS :
	  switch (env->view) {
	    case SPOT_VIEWFILES : case SPOT_VIEWQUERIES :
	      fit_setext(tempfile, ".tmp", tempout);
	      if ((tempouthandle = fopen(tempout, "w")) != NULL) {
	        for (i = startopt; i <= endopt; ++i) {
		  opt = men_opt(env->menu, i);
		  if (opt->descr.stat || !useselected)
	            fprintf(tempouthandle, "%s\n", spot_fspec(opt, env->view));
	        }
	        fclose(tempouthandle);
	        fprintf(out, "hitw <%s %s\n", tempout, destspec);
	        fprintf(out, "rm %s\n", tempout);
	      }
	      else
	        spot_errmessage("Error creating work file");
	      break;
	    case SPOT_VIEWRESULTS : case SPOT_VIEWSTREAMDETAIL :
	      if (strcmp(destspec, srcspec))
	        fprintf(out, "drctio -s -f %s %s\n", destspec, srcspec);
	      break;
	    case SPOT_VIEWARCHIVES :
	      fprintf(out, "drctio -h %s | cut -f1 -d\\# | sed \"s=^=%s#=\" | \
		hitw %s\n", srcspec, srcspec, destspec);
	      break;
	    case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	      fprintf(out, "drctio -s -f %s %s\n", destspec, temphits);
	      break;
	    default :
	      ;
	  }
	  break;

        case ARCHIVES :
	  switch (env->view) {
	    case SPOT_VIEWFILES : case SPOT_VIEWQUERIES :
	      while (i <= endopt) {
	        fprintf(out, "fprep ");
		for (count = 0; count < 50 && i <= endopt; ++i) {
		  opt = men_opt(env->menu, i);
		  if (opt->descr.stat || !useselected) {
	            fprintf(out, "%s ", spot_fspec(opt, env->view));
		    count += 1;
		  }
		}
	        fprintf(out, "| arcload %s\n", destspec);
	      }
	      break;
	    case SPOT_VIEWRESULTS : case SPOT_VIEWSTREAMDETAIL :
	      fprintf(out, "hitr %s | arcload %s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWARCHIVES :
	      if (strcmp(srcspec, destspec))
	        fprintf(out, "drctio -x %s | arcload %s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	      fprintf(out, "hitr %s | arcload %s\n", temphits, destspec);
	      break;
	    default :
	      ;
	  }
	  break;

        case DIRECTORY :
	  switch (env->view) {
	    case SPOT_VIEWFILES : case SPOT_VIEWQUERIES :
	      while (i <= endopt) {
	        fprintf(out, "cp ");
		for (count = 0; count < 50 && i <= endopt; ++i) {
		  opt = men_opt(env->menu, i);
		  if (opt->descr.stat || !useselected) {
	            fprintf(out, "%s ", spot_fspec(opt, env->view));
		    count += 1;
		  }
		}
	        fprintf(out, "%s\n", destspec);
	      }
	      break;
	    case SPOT_VIEWRESULTS : case SPOT_VIEWSTREAMDETAIL :
	      fprintf(out, "hitr %s | docput %s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWARCHIVES :
	      fprintf(out, "drctio -x  %s | docput %s\n", srcspec, destspec);
	      break;
	    case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	      fprintf(out, "hitr %s | docput %s\n", temphits, destspec);
	      break;
	    default :
	      ;
	  }
	  break;

        case STREAM :
	  switch (env->view) {
            case SPOT_VIEWQUERIES : case SPOT_VIEWRESULTS :
            case SPOT_VIEWSTREAMDETAIL :
              fit_prsfspec(qrylib,dummy,dummy,streamdir,streamname,dummy,dummy);
              strcat(streamdir,streamname);
              fprintf(out,"if [ ! -d \"%s\" ]; then\n",streamdir);
              fprintf(out,"  mkdir %s\n", streamdir);
              fprintf(out,"fi\n");
              for (i=startopt; i <= endopt; ++i) {
                 opt = men_opt(env->menu, i);
                 if (opt->descr.stat || !useselected) {
                   fit_setext(spot_fspec(opt, env->view), ".qry", qryspec);
                   fprintf(out,"cp %s %s\n",qryspec,streamdir);
                   fit_prsfspec(qryspec,dummy,dummy,dummy,qryspec,dummy,dummy); 
	           fprintf(out, "drctio -c %s/%s.hit\n", streamdir,qryspec);
                 }
              }
              fprintf(out, "qcompile -o %s ", qrylib);
              for (count=0,i=startopt; i <= endopt; ++i) {
                opt = men_opt(env->menu, i);
		if (opt->descr.stat || !useselected) {
                  fit_prsfspec(spot_fspec(opt,env->view),dummy,dummy,dummy,qryspec,dummy,dummy);
		  fprintf(out, "%s/%s.qry ",streamdir,qryspec);
		  count += 1;
                }
              }
              fprintf(out,"\n");
              break;

	    case SPOT_VIEWSTREAMS :
	      fit_setext(srcspec, ".qlb", srclib);
	      if (strcmp(srclib, qrylib)) {
	        fprintf(out, "drctio -h %s | sed s\"@^@qcompile -o %s @\" |\
		  sh\n", srclib, qrylib);
	      }
	      else
	        spot_message(
		  "Destination stream must be different from source directory");
	      break;
	    default :
	      break;
	  }
	  break;

        default :
	  ;
      }
    }
  }
  fclose(out);
  chmod(tempfile, S_IEXEC | S_IREAD | S_IWRITE);
  out = NULL;
  if (!(pid = fork())) {
    /* In child process */
    /* Reopen standard in, out, and error to where we want them to be */
    freopen("/dev/null", "r", stdin);
    freopen("/dev/null", "w", stdout);
    freopen("/dev/null", "w", stderr);

    /* Execute the script */
    execlp(tempfile, tempfile, (char *) 0);

    /* This should never get executed */
    exit(errno);
  }
  else if (pid == -1)
    /* Error from fork */
    spot_errmessage("Error forking copy process");
  else {
    while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
      ;
#if FIT_COHERENT
    spot_scrinit();
#endif
    if (desttype == STREAM) {
      spot_pupdate(destspec);
    }
  }

DONE:
  if (out != NULL) fclose(out);
  if (tempfile != NULL) unlink(tempfile);
  if (temphandle != NULL) drct_close(temphandle);
  if (temphits[0]) drct_delfile(temphits);
  if (promptwin != NULL) {
    overwrite(promptwin, spot_promptwin);
    wrefresh(spot_promptwin);
    delwin(promptwin);
  }
  return 1;
}

