/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_sumupdate					spotblds

Function : Updates summary display menu with newly available documents.

Author : A. Fregly

Abstract : 

Calling Sequence : int retstat = spot_sumupdate(struct spot_s_env *env,
  struct spot_s_doc *doc, struct men_s_menu *menu, int summarytype, 
  char *summaryitem);

  env		Spot environment structure.
  doc		Document descriptor structure for archive or result file for
		which summary is to be updated.
  menu		Virtual menu containing summary display.
  summarytype	Type of summary to be built, SPOT_VIEWDETAIL - document names, 
		SPOT_VIEWLINE - line n extracted from documents, 
		SPOT_VIEWFIELD - Data extracted from specified field.
  summaryitem	Specifies line number or field name for summarytype of
		SPOT_VIEWLINE or SPOT_VIEWFIELD.

Notes: 

Change log : 
000	16-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
int spot_sumupdate(struct spot_s_env *env, struct spot_s_doc *doc, 
  struct men_s_menu *menu, int summarytype, char *summaryitem)
#else
int spot_sumupdate(env, doc, menu, summarytype, summaryitem)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  struct men_s_menu *menu;
  int summarytype;
  char *summaryitem;
#endif
{

  int retstat, retlen, doopen;
  long prevdocnum, startline, doclen, docl, ndocs, modtime;
  char *inbuf;
  FILE *in;
  struct drct_s_file *arc;
  char namefld[TIPS_DESCRIPTORMAX+1], key[TIPS_DESCRIPTORMAX+1];

  if (env->doc.handle == NULL) return 0;

  if ((inbuf = (char *) calloc(max(COLS+1, menu->maxoptlen+1),1)) == NULL) {
    spot_errmessage("Error allocating buffer");
    return 0;
  }

  if (summarytype == SPOT_VIEWLINE)
    sscanf(summaryitem, " %ld", &startline);

  in = NULL;
  arc = NULL;
  ndocs = menu->nopts;
  doopen = summarytype != SPOT_VIEWDETAIL;

  for (prevdocnum = doc->docnum, 
      retstat = spot_finddoc(env, doc, 1, 1, 0, summarytype == SPOT_VIEWFIELD,
      doopen, &in, &arc, &doclen, &modtime);
      retstat && doc->docnum > prevdocnum;
      retstat = spot_finddoc(env, doc, 1, 1, 0, summarytype == SPOT_VIEWFIELD,
      doopen, &in, &arc, &doclen, &modtime)) {

    tips_parsedsc(doc->docname, namefld, &docl, key);
    strcpy(doc->docname, namefld);

    prevdocnum = doc->docnum;

    if (summarytype == SPOT_VIEWDETAIL) {
      spot_fmtdisp(summarytype, doc->docname, inbuf);
      retlen = strlen(inbuf);
    }
    else if (summarytype == SPOT_VIEWLINE) {
      /* Find specified line in document and add to menu */
      spot_loadlines(doc, doclen, inbuf, menu->maxoptlen, startline, in, arc,
	&retlen);
    }
    else if (summarytype == SPOT_VIEWFIELD) {
      /* Find specified field in document and add to menu */
      spot_getfield(doc, doclen, inbuf, menu->maxoptlen, summaryitem, in, arc,
	&retlen);
    }
    else
      retlen = 0;

    if (in != NULL) {
      fclose(in);
      in = NULL;
    }
    if (arc != NULL)
      drct_unlock(arc);

    if (!spot_addtosummary(menu, inbuf, retlen, doc->docnum, doc->docname)) {
      spot_errmessage("Error appending to display list");
      break;
    }
  }
  free(inbuf);
  if (ndocs != menu->nopts)
    men_draw(menu);
  return 1;
}
