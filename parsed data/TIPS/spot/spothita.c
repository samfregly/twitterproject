/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_hitappend					spothita.c

Function : Appends menu item to result file.

Author : A. Fregly

Abstract : 

Calling Sequence : int retstat = spot_hitappend(struct men_s_menu *menu,
  long idx, int viewtype, struct drct_s_file *handle);

  menu		Menu from which item to append is being taken.
  idx		Index of item in menu.
  viewtype	Viewtype for menu. If view type is not for a summary view,
		the data field for menu entries is assumed to be an expanded
		file name. If the view type is for a summary view, the first
		long word of the data field is the record number of the
		archive or result file for the menu item, and the following
		portion is an expanded file name.
  handle	Handle for result file to which item is to be added.

Notes: 

Change log : 
000	21-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_hitappend(struct men_s_menu *menu, long idx, int viewtype, 
  struct drct_s_file *handle)
#else
int spot_hitappend(menu, idx, viewtype, handle)
  struct men_s_menu *menu;
  long idx;
  int viewtype;
  struct drct_s_file *handle;
#endif
{

  struct men_s_option *opt;
  char docname[TIPS_DESCRIPTORMAX+1];
  int reclen;

  if ((opt = men_opt(menu, idx)) != NULL && opt->descr.datalen) {
    if (viewtype >= SPOT_VIEWDETAIL)
      strcpy(docname, (char *) opt->data + FIT_BYTES_IN_LONG);
    else
      strcpy(docname, (char *) opt->data);

    reclen = strlen(docname);
    docname[reclen++] = '\n';
    docname[reclen] = 0;
    return drct_append(handle, docname, reclen, (long) reclen);
  }
  else
    return 0;
}
