/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brfooter					spotbrfo.c

Function : Displays/Updates Browse footer.

Author : A. Fregly

Abstract : This function is used to draw selected portions of the browse
	footer.

Calling Sequence :  spot_brfooter(char *docname, long docnum, long maxdoc, 
    int topline, int nlines);

  docname	Document name. A null value indicates it is not to be updated.
  docnum	Document number, or 0 if it is not to be updated.
  maxdoc	Maximum document number, or 0 it is not to be updated.
  topline	Line number, or 0 if it is not to be updated.
  nlines	Number of lines, or 0 if it is not to be updated.

Notes: 
  if docnum or maxdoc <= 0, neither is updated in the display.
  if topline or nlines <= 0, neither is updated in the display.

Change log : 
000	10-SEP-91  AMF	Creaeted.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_brfooter(char *docname, long docnum, long maxdoc, int topline, 
  int nlines)
#else
void spot_brfooter(docname, docnum, maxdoc, topline, nlines)
  char *docname;
  long docnum, maxdoc;
  int topline, nlines;
#endif
{
  char outline[257];

  if (docnum > 0 && maxdoc > 0) {
    sprintf(outline,"%ld of %ld                    ", docnum, maxdoc);
    outline[20] = 0;
    mvwaddstr(spot_footerwin, 0, 0, outline);
  }

  if (docname != NULL && *docname) {
    char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
    char dbname[TIPS_DESCRIPTORMAX+1], docnum_s[TIPS_DESCRIPTORMAX+1];
    char filespec[TIPS_DESCRIPTORMAX+1];
    long lenfld;

    tips_parsedsc(docname, namefld, &lenfld, keyfld);
    if (namefld[0]) {
      tips_nameparse(namefld, dbname, docnum_s, filespec);
      if (dbname[0]) {
	strcpy(namefld, dbname);
	strcat(namefld, " ");
	strcat(namefld, docnum_s);
      }
      else {
	if (docnum_s[0]) {
	  strcpy(namefld, docnum_s);
	  strcat(namefld, " ");
	  strcat(namefld, filespec);
	}
	else {
	  strcpy(namefld, filespec);
	}
      }
    }
    else
      strcpy(namefld, docname);

    fit_sassign(outline, 0, COLS-40-1, namefld, 0, strlen(docname)-1);
    outline[COLS-40] = 0;
    fit_scenter(outline, COLS - 40);
    mvwaddstr(spot_footerwin, 0, 20, outline); 
  }

  if (topline > 0 && nlines > 0) {
    sprintf(outline,"%d/%d                    ", topline, nlines);
    outline[20] = 0;
    fit_srjust(outline, 20);
    mvwaddstr(spot_footerwin, 0, COLS-20, outline);
  }
  wrefresh(spot_footerwin);
}
