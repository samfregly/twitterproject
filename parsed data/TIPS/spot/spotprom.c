/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_prompt					spotprom.c

Function : Prompts user on bottom line of the screen.

Author : A. Fregly

Abstract : This function will save the contents of the help line at the bottom
	of the screen, and after displaying the supplied prompt string, allow
	the user to enter and edit a response on the remainder of the help
	line. 

Calling Sequence : int retstat = spot_prompt(char *promptstr, char *promptresp,
	int promptresp_l);

  promptstr	Prompt to be displayed starting at the leftmost column of the
	 	bottom line of the screen.
  promptresp	Returned response. promptresp will have a length of 0 if the
		user canceled out of the prompt, or did not enter a response.
  promptresp_l	Length of promptresp buffer.
  retstat	returned value of 0 indicates user canceled out of the prompt.

Notes: 

Change log : 
000	18-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_prompt(char *promptstr, char *promptresp, int promptresp_l)
#else
int spot_prompt(promptstr, promptresp, promptresp_l)
  char *promptstr, *promptresp;
  int promptresp_l;
#endif
{

  int promptlen, maxresplen, imode, dmode, exitkey, off;
  int width, lcol;
  unsigned resplen;
  WINDOW *promptwin, *savepromptwin, *helpwinsave;

  promptlen = strlen(promptstr);
  width = min(COLS, 2 + promptlen + promptresp_l);
  lcol = (COLS - width) / 2;
  
  if ((promptwin = newwin(3, width, LINES/2 - 2, lcol)) == NULL) return 0;
  keypad(promptwin, TRUE);
  scrollok(promptwin, FALSE);
  wattrset(promptwin, spot_env.mennorm);
  win_fill(promptwin, 0, 0, 0, 0, ' ');
  if (spot_env.uselinedrawing)
    win_box(promptwin);
  win_dup(promptwin, &savepromptwin);
  overwrite(spot_dispwin, savepromptwin);

  win_dup(spot_promptwin, &helpwinsave);
  overwrite(spot_promptwin, helpwinsave);
  spot_puthelpline(spot_edithelp); 

  maxresplen = min(width - promptlen - 2, promptresp_l);

  resplen = strlen(promptresp);
  resplen = min(min(resplen, maxresplen), promptresp_l);

  if (resplen < maxresplen)
    fit_sblank(promptresp + resplen, maxresplen - resplen);
  promptresp[maxresplen] = 0;

  if (!spot_env.uselinedrawing)
    wattrset(promptwin, spot_env.dispnorm);

  win_fill(promptwin, 1, 1, 1, width - 2, ' ');
  mvwaddstr(promptwin, 1, 1, promptstr);
  touchwin(promptwin);
  wrefresh(promptwin);

  imode = 0;
  off = resplen;
  dmode = 1;
  for (exitkey = spot_helpkey; exitkey == spot_helpkey;) {
    if (!win_edit(promptwin, promptresp, maxresplen, &imode, dmode, 0, 1, 
	promptlen + 1, &off, &exitkey) ||
        (exitkey != '\n' && exitkey != '\r' && exitkey != KEY_ENTER && 
	 exitkey != spot_helpkey)) {
      promptresp[0] = 0;
      break;
    }
    else if (exitkey == spot_helpkey) {
      overwrite(promptwin, spot_dispwin);
      spot_help();
    }
    dmode = 0;
  }

  overwrite(helpwinsave, spot_promptwin);
  wrefresh(spot_promptwin);
  delwin(helpwinsave);
  overwrite(savepromptwin, spot_dispwin);
  wrefresh(spot_dispwin);
  delwin(savepromptwin);
  delwin(promptwin);

  resplen = maxresplen; 
  fit_strim(promptresp, &resplen);

  return (resplen && (exitkey == '\n' || exitkey == '\r' || 
	exitkey == KEY_ENTER));
}
