/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_docparse					spotdocp.c

Function : Parse document temp file, building line offsets and matching term
	descriptors.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_docparse(struct spot_s_doc *doc);

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Make friendly to strict compilers.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
int spot_docparse(struct spot_s_doc *doc)
#else
int spot_docparse(doc)
  struct spot_s_doc *doc;
#endif
{

  int highlight, i, offstat, mv, lidx, maxhigh, nbytes;
  struct qc_s_cmpqry *cmpqry;
  long lenfld, docoffset; 
  char *lfptr, *docptr, *ffptr;
  char archive[FIT_FULLFSPECLEN+1];
  char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char docnum_s[FIT_FULLFSPECLEN+1], file[FIT_FULLFSPECLEN+1];

  if (!doc) {
    return 0;
  }

  /* Initialize counts and current startline in doc buffer */
  doc->startline = -1L;
  doc->topline = 0;
  doc->nhigh = 0;
  highlight = 0;

  /* Deallocate leftover buffers */
  if (doc->lineoffsets != NULL) {
    free(doc->lineoffsets);
    doc->lineoffsets = NULL;
  }
  if (doc->highoffsets != NULL) {
    free(doc->highoffsets);
    doc->highoffsets = NULL;
  }
  if (doc->highlengths != NULL) {
    free(doc->highlengths);
    doc->highlengths = NULL;
  }

    /* Allocate line offsets array */
    doc->lineoffsets = (long *) calloc((unsigned int) doc->nlines+3,
      sizeof(*doc->lineoffsets));
    if (doc->lineoffsets == NULL) return 0;

  if (doc->nlines) {
    /* Create query exe and/or set highlighting flag so that results with
       query field can have matching terms in document highlighted */
    tips_parsedsc(doc->docname, namefld, &lenfld, keyfld);
    if (*namefld)
      tips_nameparse(namefld, archive, docnum_s, file);
    else
      *archive = 0;

    if (*keyfld && strcmp(keyfld, doc->keyfld)) {
      if (tips_getqry(keyfld, &cmpqry)) {
        if (prf_init(TIPS_MAXTERMWORDLEN, NULL, doc->nfields, doc->fieldnames, 
	    &doc->qryexe)) {
          if (highlight = prf_addqry(keyfld, cmpqry, doc->qryexe)) {
	    strncpy(doc->keyfld, keyfld, TIPS_DESCRIPTORMAX);
	    doc->keyfld[TIPS_DESCRIPTORMAX] = 0;
	  }
        }
        qc_dealloc(&cmpqry);
      }
    }
    else
      highlight = (*keyfld && doc->qryexe != NULL);
  }
  else
    highlight = 0;

  if (highlight) {
    if ((doc->nlines * 10) + doc->wrapcol > SPOT_MAXHIGH)
      maxhigh = SPOT_MAXHIGH;
    else
      maxhigh = (doc->nlines * 10) + doc->wrapcol;

    /* Allocate word match highlighting descriptors */
    doc->highoffsets = (unsigned long *) calloc(maxhigh, 
      sizeof(*doc->highoffsets));
    doc->highlengths = 
      (unsigned *) calloc(maxhigh, sizeof(*doc->highlengths));
    highlight = (doc->highoffsets != NULL && doc->highlengths != NULL);
  }

  /* Rewind temporary document file */
  rewind(doc->temp);

  /* Do highlighting and create line descriptors */
  for (docoffset = 0, lidx = 0, offstat = 1, doc->lineoffsets[0] = 0; 
      docoffset < doc->doclen;) {
    doc->startline = lidx;

    /* See how much to read on next read */
    if (doc->doclen - docoffset > doc->buflen)
      nbytes = doc->buflen;
    else
      nbytes = (int) (doc->doclen - docoffset); 

    /* Get next document buffer */
    if (fread(doc->docbuf, 1, nbytes, doc->temp) != nbytes)
      return 0;

    if (doc->doclen - docoffset > nbytes) {
      /* If not on last buffer, mv back to end of last complete line in
         buffer */
      for (i=nbytes - 1; i > 0; --i)
	if (doc->docbuf[i] == '\n') break;
      if (i) {
        mv = i - nbytes + 1;
        nbytes = i + 1;
        if (mv)
          fseek(doc->temp, (long) mv, SEEK_CUR);
      }
    }

    /* Put terminating null byte at end of buffer */
    doc->docbuf[nbytes] = 0;

    if (highlight) {
      /* Add offsets for current document buffer */
      prf_offsets(offstat, 0, doc->qryexe, doc->docbuf, nbytes, maxhigh, 
        &doc->nhigh, doc->highoffsets, doc->highlengths);
      offstat = 0;
    }

    /* Add lineoffsets for current document bufer */
    for (docptr = (char *) doc->docbuf; docptr != NULL; docptr = lfptr) {
      lfptr = strchr(docptr,'\n');
      if (lfptr != NULL) {
	*lfptr = 0;
	ffptr = strchr(docptr,'\f');
	*lfptr = '\n';
	if (ffptr != NULL) lfptr = ffptr;
      }
      else
	lfptr = strchr(docptr, '\f');

      if (lfptr != NULL)
        doc->lineoffsets[++lidx] = (++lfptr - (char *) doc->docbuf) + docoffset;
    }
    /* Set document offset */
    docoffset = ftell(doc->temp);
  }

  doc->nlines = lidx;
  doc->lineoffsets[++lidx] = docoffset;
  doc->lineoffsets[++lidx] = docoffset;

  if (highlight) {
    /* Tell highlighter to deallocate internal data structures */
    prf_offsets(8, 0, doc->qryexe, doc->docbuf, nbytes, maxhigh, &doc->nhigh, 
	doc->highoffsets, doc->highlengths);
  }

  return 1;
}

