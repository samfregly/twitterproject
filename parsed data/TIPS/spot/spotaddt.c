/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_addtosummary					spotaddt.c

Function : Adds an item to the end of a browse summary display menu.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_addtosummary(struct men_s_menu *menu, 
  char *option, int optlen, long docnum, char *docname);

  menu		Virtual menu to which item is to be added.
  option	Value to be displayed in the menu.
  optlen	Length of option.
  docnum	Index of document in source archive or result file.
  docname	Document name descriptor. This is the name field from a TIPS
		document descriptor, and is used for accessing the document 
		being summarized.
  stat		Returned status, 0 indicates an error.

Notes: 

Change log : 
000	17-MAR-92  AMF	Created.
001	02-NOV-96  AMF	Fixed call to memcpy so that docnum is passed as a pointer.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
int spot_addtosummary(struct men_s_menu *menu, char *option, int optlen,
  long docnum, char *docname)
#else
int spot_addtosummary(menu, option, optlen, docnum, docname)
  struct men_s_menu *menu;
  char *option;
  int optlen;
  long docnum;
  char *docname;
#endif
{

  int retstat, datalen, i;
  char *data;

  if (menu == NULL) return 0;

  /* Allocate buffer for stuffing data into */
  /* Determine size */
  datalen = sizeof(docnum) + strlen(docname) + 1;

  /* Allocate */
  if ((data = (char *) calloc(datalen, 1)) == NULL) return 0;

  /* Fill the data buffer */
  memcpy(data, &docnum, sizeof(docnum));
  strcpy(data + sizeof(docnum), docname);

  /* Clean out the option buffer */
  for (i=0; i < optlen; ++i) {
    if (option[i] < ' ' || option[i] > '~') {
      if (option[i] == spot_defaultdelim && i < optlen - 1)
        option[i++] = ' ';
      option[i] = ' ';
    }
  }
  option[optlen] = 0;

  retstat = men_addopt(menu, option, datalen, data);
  free(data);

  return retstat;
}
