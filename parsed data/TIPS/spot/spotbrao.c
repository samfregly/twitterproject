/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_braoption					spotbrao.c

Function : Processes Action menu options off from browse menu.

Author : A. Fregly

Abstract : This function processes the various action options off the browse
        menu. It will display any pulldowns and dialogs required for completing
        initiation of the function, then do the function.

Calling Sequence : void spot_braoption(struct spot_s_env *env,
        struct men_s_menu *menu, int option);

  env           Spot environment structure.
  menu          Menu displaying action options.
  option        Selected off action menu.

Notes:

Change log :
000     25-FEB-92  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_braoption(struct spot_s_env *env, struct spot_s_doc *doc, 
  struct men_s_menu *menu, int option)
#else
void spot_braoption(env, doc, menu, option)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  struct men_s_menu *menu;
  int option;
#endif
{

  char *(*filelist);

  filelist = NULL;

  if ((filelist = (char *(*)) calloc(3, sizeof(*filelist))) != NULL) {
    if ((filelist[0] = (char *) calloc(strlen(doc->docname) + 1, 
	sizeof(*(filelist[0])))) != NULL)
      strcpy(filelist[0], doc->docname);
    filelist[1] = (char *) calloc(1,1); 
    filelist[2] = NULL;
  }
  
  if (filelist != NULL && filelist[0] != NULL && *filelist[0])
    spot_doaction(env, &spot_actions[SPOT_MAXVIEW], option, filelist);

  if (filelist != NULL) fit_delfilelist(&filelist);
  wrefresh(spot_dispwin);
}
