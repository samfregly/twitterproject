/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot						spot.c

Function : Main routine for spot program

Author : A. Fregly

Abstract : spot is the main routine for the "spot" system user interface.
	It sets up the system environment, processes key entries made
	in the main command menu, and also performs periodic updates of
	those displays which need it.

Calling Sequence : spot.

Notes: 

Change log : 
000	03-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <string.h>
#include <fitsydef.h>
#include <errno.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

struct spot_s_env spot_env;

int spot_dispwidth, spot_dispheight, spot_hardscroll;

WINDOW *spot_labelwin, *spot_cmdwin, *spot_dispwin, *spot_footerwin;
WINDOW *spot_promptwin, *spot_selectwin, *spot_copywin;
WINDOW *spot_filewin, *spot_viewwin, *spot_querywin, *spot_searchwin;
WINDOW *spot_detailwin, *spot_addwin, *spot_strcopywin, *spot_sumsrcwin;
WINDOW *spot_sumdelwin;

WINDOW *spot_brfilewin, *spot_brviewwin, *spot_brdeloptwin;
WINDOW *spot_brcopywin, *spot_brviewtypewin, *spot_brsummaryoptwin;

WINDOW *spot_errwin, *spot_msgwin, *spot_srcwin;
WINDOW *spot_errwinsave, *spot_msgwinsave, *spot_promptwinsave;

struct men_s_menu *spot_dispmenu, *spot_cmdmenu, *spot_selectmenu;
struct men_s_menu *spot_filemenu, *spot_viewmenu, *spot_querymenu;
struct men_s_menu *spot_searchmenu, *spot_strcopymenu;
struct men_s_menu *spot_detailmenu, *spot_addmenu, *spot_copymenu;
struct men_s_menu *spot_sumsrcmenu, *spot_sumdelmenu;

/* Declare action descriptors */
struct spot_s_actiondescr spot_actions[SPOT_MAXVIEW+1];

struct men_s_menu *spot_brfilemenu, *spot_brviewmenu;
struct men_s_menu *spot_brcopymenu, *spot_brviewtypemenu;
struct men_s_menu *spot_brsummaryoptmenu, *spot_brdeloptmenu;

struct men_s_menu *spot_srcmenu;

char spot_curpath[FIT_FULLFSPECLEN+1];
char spot_filedest[FIT_FULLFSPECLEN+1], spot_arcdest[FIT_FULLFSPECLEN+1];
char spot_searchdest[FIT_FULLFSPECLEN+1],spot_streamdest[FIT_FULLFSPECLEN+1];
char spot_streamdestname[FIT_FULLFSPECLEN+1];
char spot_filefilter[FIT_FULLFSPECLEN+1];

int spot_menukey, spot_helpkey, spot_exitkey, spot_quitkey;
char *spot_menukeyname, *spot_helpkeyname, *spot_exitkeyname, *spot_quitkeyname;

char spot_disphelp[128], spot_cmdhelp[128], spot_edithelp[128];
char spot_selectorhelp[128], spot_multselectorhelp[128], spot_brdisphelp[128];
char spot_fieldedithelp[128], spot_selectorcmdhelp[128], spot_multmenuhelp[128];

int spot_neditor;
char *(*spot_editor), *(*spot_editortype);
char spot_defaulteditor[FIT_FULLFSPECLEN+1];
char spot_mailer[FIT_FULLFSPECLEN+1];

/* Print command and file types to which they apply */
int spot_nprintcmd;
char *(*spot_printcmd), *(*spot_printtype);
char spot_defaultprintcmd[FIT_FULLFSPECLEN+1];

char *spot_progname;
char spot_shell[FIT_FULLFSPECLEN+1];
char spot_defaultfields[FIT_FULLFSPECLEN+1];
int spot_defaultdelim;

FILE *fit_debug_handle;

#if MEN_DEBUG
WINDOW *men_win_debug;
#endif

#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int key, menudown, optselected, exitflag, switchkey, i;
  long retopt;
  int /***** curview, ****/ stat, alarmset, timout;
  int subopt, immediate;
  char *introline, *varptr, *nlptr;
  FILE *intro;

#if FIT_DEBUG
  fit_debug_handle = fopen("spot.dbg", "w");
#endif

  /* Set program name */
  spot_progname = *argv;

  /* initialize curses (screen handling functions) */
  if (!initscr()) {
    stat = errno;
    perror("Error from initscr");
    exit(stat);
  }
//#if FIT_SCO 
  spot_env.usecolor = (start_color() != ERR);
//#else
//  spot_env.usecolor = 0;
//#endif
#if FIT_SCO || FIT_SUN || FIT_HP || FIT_CYGWIN || FIT_LINUX
  if ((stat = noecho()) == ERR) goto ERREXIT;
  if ((stat = intrflush(stdscr, FALSE)) == ERR) goto ERREXIT;
  if ((stat = keypad(stdscr, TRUE)) == ERR) goto ERREXIT;
  if ((stat = raw()) == ERR) goto ERREXIT;
#if !FIT_HP
  stat = (curs_set(0) != ERR);
#endif
#else
  noecho();
  keypad(stdscr, TRUE);
  raw();
#endif

#if MEN_DEBUG
  men_win_debug = newwin(1, COLS, LINES-1, 0);
  scrollok(men_win_debug, FALSE);
#endif

  /* Initialize display menu to NULL */
  spot_dispmenu = NULL;

  /* Get current default directory */
  getcwd(spot_curpath, FIT_FULLFSPECLEN);

  /* Read in environment */
  if (!spot_getenv(&spot_env)) goto ERREXIT;
  spot_env.prevsel = NULL;

  /* Setup help lines */
  sprintf(spot_disphelp, 
    "[Enter] - select, %s - menu, %s - help, [Space] - toggle", 
    spot_menukeyname, spot_helpkeyname);
  sprintf(spot_cmdhelp, "[Enter] - select, %s - help, %s - cancel", 
    spot_helpkeyname, spot_exitkeyname);
  sprintf(spot_edithelp, "[Enter] - continue, %s - help, %s - cancel", 
    spot_helpkeyname, spot_exitkeyname);
  sprintf(spot_selectorhelp, 
    "[Enter] - select, %s - menu, %s - help, %s - cancel, [Tab] - advance",
    spot_menukeyname, spot_helpkeyname, spot_exitkeyname);
  sprintf(spot_multselectorhelp, 
    "[Enter] - select, %s - help, [Tab] - advance,  [Space] - toggle",
    spot_helpkeyname);
  sprintf(spot_brdisphelp, "%s - menu, %s - help, [Tab] - next match",
    spot_menukeyname, spot_helpkeyname);
  sprintf(spot_fieldedithelp, 
    "[Enter] - continue, %s - help, [Tab] - advance, %s - cancel",
    spot_helpkeyname, spot_exitkeyname);
  sprintf(spot_selectorcmdhelp,  
    "[Enter] - select, %s - help, %s - cancel, [Tab] - advance",
    spot_helpkeyname, spot_exitkeyname);
  sprintf(spot_multmenuhelp, 
    "[Enter] - continue, %s - help, %s - cancel, [Space] - toggle", 
	spot_helpkeyname, spot_exitkeyname);

  /* Create display windows and menus, and set their attributes */
  if (!spot_makedisplay(&spot_env)) goto ERREXIT;

  /* Put up copyright notice */
  if ((varptr = getenv("SP_INTRO")) != NULL) {
    introline = (char *) malloc(COLS + 1);
    if ((intro = fopen(varptr, "r")) != NULL) {
      for (i=0; i < LINES - 3; ++i) {
        if (fgets(introline, COLS, intro) == NULL) break;
	if ((nlptr = strchr(introline, '\n')) != NULL) *nlptr = 0;
	win_center(spot_dispwin, i, introline);
      }
      fclose(intro);
    }
    else
      varptr = NULL;
    free(introline);
  }
  if (varptr == NULL) {
    win_center(spot_dispwin, LINES/2 -7, "Welcome to SPOT");
    win_center(spot_dispwin, LINES/2 - 6, "Version 1.2, Release 1");
    win_center(spot_dispwin, LINES/2 - 4, "Leader of the pack for Real-Time\
 Text Retrieval");
    win_center(spot_dispwin, LINES/2 - 1, 
      "Contact Andrew Fregly for sales and support");
    win_center(spot_dispwin, LINES/2, "703-402-0086");
    win_center(spot_dispwin, LINES - 5, "Copyright (c) 2004, Andrew M. Fregly, \
United States of America");
    win_center(spot_dispwin, LINES - 4, "All rights reserved");
  }
  wrefresh(spot_dispwin);
  signal(SIGALRM, spot_timeralarm);
  alarm(10);
  key = wgetch(spot_cmdwin);
  alarm(0);

  /* Initialize initial view */
  spot_newdoc(&spot_env.doc);
  spot_viewinit(&spot_env, spot_env.view, -1L, "");

  wrefresh(spot_labelwin);
  wrefresh(spot_dispwin);
  wrefresh(spot_footerwin);

  exitflag = 0;
/*****  curview = spot_env.view; *****/

  /* Set up signal handlers */
  signal(SIGTERM, SIG_IGN);
  signal(SIGINT, SIG_IGN);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGPIPE, SIG_IGN);
  signal(SIGQUIT, SIG_IGN);
#endif


  while (1) {
    wattrset(spot_cmdwin, spot_env.mennorm);
    win_fill(spot_cmdwin, 0, 0, 0, 0, ' ');
    wrefresh(spot_cmdwin);

    menudown = 0;

    if (spot_env.menu != NULL) {
      if (spot_env.updateinterval &&
	 (spot_env.view == SPOT_VIEWRESULTS  ||
	  spot_env.view == SPOT_VIEWARCHIVES ||
	  spot_env.view == SPOT_VIEWSTREAMDETAIL ||
	  spot_env.view == SPOT_VIEWSTREAMS ||
	  spot_env.view == SPOT_VIEWDETAIL ||
	  spot_env.view == SPOT_VIEWLINE ||
	  spot_env.view == SPOT_VIEWFIELD))
	timout = spot_env.updateinterval;
      else
	timout = 0;

      spot_puthelpline(spot_disphelp);

      for (optselected = 0, key = 0; spot_env.menu != NULL && !optselected && 
	  key != spot_menukey;) {
        key = men_select(spot_env.menu, timout, &retopt, 
	  &optselected, &alarmset);
	if (alarmset) {
          spot_refresh(&spot_env, spot_env.menu);
	}
	else if (key == spot_menukey) {
          spot_cmdmenu->curopt = 0;
	  switchkey = key;
	}
	else if (!optselected) {
	  menudown = 1;
	  switchkey = key;
	  key = spot_menukey;
	  switch (switchkey) {
	    case 'f' : case  'F' :
	      spot_cmdmenu->curopt = 0;
	      break;
	    case 'v' : case 'V' :
	      spot_cmdmenu->curopt = 1;
	      break;
	    case 's' : case 'S' :
	      spot_cmdmenu->curopt = 2;
	      break;
	    case 'a' : case 'A' :
	      if (spot_env.view < SPOT_VIEWDETAIL ||
		  spot_env.view >= SPOT_VIEWDETAIL &&
		  (spot_env.prevview == SPOT_VIEWQUERIES &&
		  spot_actions[SPOT_VIEWQUERIES].menu != NULL ||
		  spot_env.prevview != SPOT_VIEWQUERIES &&
		  spot_actions[SPOT_VIEWFILES].menu != NULL))
	        spot_cmdmenu->curopt = 3;
	      else
		key = switchkey;
	      break;
	    case KEY_EXIT : case KEY_SEXIT :
#if FIT_LINUX
 		case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
	      goto DONE;
	      break;
	    default :
	      if (switchkey == spot_exitkey || switchkey == spot_quitkey)
	        if (spot_env.view < SPOT_VIEWDETAIL)
		  goto DONE;
		else {
		  spot_viewinit(&spot_env, spot_env.prevview, 1, 
		    spot_env.summaryitem);
  		  wrefresh(spot_dispwin);
  		  wrefresh(spot_footerwin);
		}
	      else if (switchkey == spot_helpkey)
		spot_help();
	      key = switchkey;
	      menudown = 0;
	  }
	}
      }
    }
    else {
      key = spot_menukey;
      switchkey = key;
      optselected = 0;
    }

    spot_puthelpline(spot_cmdhelp);

    if (optselected) {
      menudown = 0;
      immediate = 1;
      switch (spot_env.view) {

	case SPOT_VIEWFILES : case SPOT_VIEWQUERIES : case SPOT_VIEWSTREAMS :
          spot_cmdmenu->curopt = 0;	/* File menu */
	  subopt = 0;			/* Open option */
	  break;

	case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	  spot_cmdmenu->curopt = 0;
	  if (spot_env.prevview == SPOT_VIEWSTREAMDETAIL ||
	      spot_env.prevview == SPOT_VIEWRESULTS ||
	      spot_env.prevview == SPOT_VIEWARCHIVES)
	    subopt = 2;
	  else
	    subopt = 0;
	  break;

	default :
	  spot_cmdmenu->curopt = 0;  /* Browse */
	  subopt = 2;		     /* Current */
      }
      key = spot_maincmd(&spot_env, spot_cmdmenu->curopt, optselected, 
	  immediate, &subopt, &menudown, &exitflag);
      if (exitflag) {
	if (spot_env.view < SPOT_VIEWDETAIL)
	  goto DONE;
	else {
	  spot_viewinit(&spot_env, spot_env.prevview, 1, spot_env.summaryitem);
  	  wrefresh(spot_dispwin);
  	  wrefresh(spot_footerwin);
	  break;
	}
      }
    }
    else if (key == spot_menukey || spot_env.menu == NULL) {
      immediate = 0;
      subopt = 0;
      optselected = 1;
      men_draw(spot_cmdmenu);
      men_put(spot_cmdmenu, NULL);
      if (key == switchkey) {
        spot_puthelpline(spot_cmdhelp);
	for (optselected = 0; !optselected;) {
          key = men_select(spot_cmdmenu, timout, &retopt, &optselected, 
		&alarmset);
	  if (alarmset && spot_env.menu != NULL)
            spot_refresh(&spot_env, spot_env.menu);
          else if (key == spot_quitkey || key == spot_exitkey || 
	      key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
	      key == ('d' - 'a' + 1))
	    goto DONE;
	  else if (key == spot_helpkey)
	    spot_help();
	  else if (key == KEY_DOWN || key == KEY_UP || key == '\t' || 
	    key == KEY_NEXT || key == KEY_SNEXT)
	    break;
	}
      }
      while (optselected || menudown) {
	menudown = 1;
	optselected = 0;
        key = spot_maincmd(&spot_env, spot_cmdmenu->curopt, optselected, 
	    immediate, &subopt, &menudown, &exitflag);
        if (exitflag) {
	  if (spot_env.view < SPOT_VIEWDETAIL)
	    goto DONE;
	  else {
	    spot_viewinit(&spot_env, spot_env.prevview, 1, 
		spot_env.summaryitem);
  	    wrefresh(spot_dispwin);
  	    wrefresh(spot_footerwin);
	    break;
	  }
	}
	if (menudown) {
	  if (key == 0 && menudown) {
	    key = wgetch(spot_cmdmenu->optwin);
            if (key == spot_quitkey ||
	        key == spot_exitkey || key == spot_quitkey ||
		key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
		key == ('d' - 'a' + 1))
	      goto DONE;
	    else if (key == CTRLW) {
	      endwin();
	      wrefresh(spot_cmdmenu->optwin);
	    }
	    else if (key == KEY_UP || key == KEY_DOWN ||
	        key == spot_exitkey || key == spot_quitkey ||
		key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
		key == ('d' - 'a' + 1))
	      break;
	    else if (key == spot_helpkey)
	      spot_help();
	  }
	  men_dokey(spot_cmdmenu, key, &optselected);
	}
      }
    }
  }

DONE:
  endwin();
  return 0;

ERREXIT:
  endwin();
  perror("Exiting due to system error");
  return errno;
}
