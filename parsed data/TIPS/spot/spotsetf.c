/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_setfieldfile					spotsetf.c

Function : Sets name of field file to use for search.

Author : A. Fregly

Abstract : This function is used to set the name of the field file to be
	used during a search. If the search is being executed against files
	or search result files, the default field file is used. Otherwise,
	in searches against archives and result files for stream searches,
	the default field file is returned only if there is not a field
	file defined for the archive or query object library for the stream.

Calling Sequence : int stat = spot_setfieldfile(char *defaultflds, 
	char *archive, char *stream, char *fieldfile);

  defaultflds	Default value to be returned if no field file corresponding to
		archive or stream is located.
  archive	Name of archive to use in looking for field file. If a field
		file whose name is the same as archive, but with an extension
		of ".fld" exists, a zero length field file name is returned.
		Otherwise, a check will be made to see if a field file
		corresponding to stream exists. 
  stream	Name of stream to use in looking for field file if no field
		file is found for "archive". If stream is used, the field
		file is determined by looking for a file corresponding to
		stream, but with an extension of ".fld". If the stream field
		file exists, a zero length field file name is returned.
  fieldfile	Returned field file name. "fieldfile" should be a buffer of
		at least FIT_FULLFSPECLEN+1 bytes in length. A zero length field
		file name is returned if a field file matching "archive" or
		"stream" exists. This indicates that the default field 
		file resolution used by spotter should find the appropriate
		field file automatically.
  stat		Returned status. A non-zero value is returned if a field
		file corresponding to archive or stream is located, otherwise
		a value of 0 is returned, indicating a non zero length
		value in fieldfile may be used.

Notes: 

Change log : 
000
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#define READ_ACCESS 4

#if FIT_ANSI
int spot_setfieldfile(char *defaultflds, char *archive, char *stream, 
  char *fieldfile)
#else
int spot_setfieldfile(defaultflds, archive, stream, fieldfile)
  char *defaultflds, *archive, *stream, *fieldfile;
#endif
{

  char testfile[FIT_FULLFSPECLEN+1];
  int retstat;

  fieldfile[0] = 0;
  retstat = 1;

  if (archive != NULL && *archive) {
    /* See if archive field file exists and user has read access to the archive 
       field file */
    fit_setext(archive, ".fld", testfile);
    if (!access(testfile, READ_ACCESS)) goto SUCCESS;
  }
  if (stream != NULL && *stream) {
    /* See if stream field file exists and user has read access to the stream 
       field file */
    fit_setext(stream, ".fld", testfile);
    if (!access(testfile, READ_ACCESS)) goto DONE;
  }

  retstat = 0;
  if (defaultflds != NULL && *defaultflds) {
    strcpy(testfile, defaultflds);
    if (*testfile && !access(testfile, READ_ACCESS)) goto SUCCESS;
  }

  goto DONE;

SUCCESS:
  strcpy(fieldfile, testfile);

DONE:
  return retstat;
}
