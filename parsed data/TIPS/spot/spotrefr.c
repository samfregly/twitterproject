/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_refresh					spotrefr.c

Function : Updates realtime displays on main screen.

Author : A. Fregly

Abstract : This function is used to refresh the display area of the main
	screen when viewing results, archives, or streams. All entries on
	the currently visible portion of the display are updated to reflect
	the current status of the entity.

Calling Sequence : void spot_refresh(struct spot_s_env *env, 
	struct men_s_menu *disp);

  env		Spot environment structure.
  disp		Menu containing the display to be updated.

Notes: 

Change log : 
000	03-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#define VALMAXLEN 512

#if FIT_ANSI
void spot_refresh(struct spot_s_env *env, struct men_s_menu *disp)
#else
void spot_refresh(env, disp)
  struct spot_s_env *env;
  struct men_s_menu *disp;
#endif
{
  long i;
  struct men_s_option *opt;
  char newval[VALMAXLEN];

  if (env->view >= SPOT_VIEWDETAIL)
    if (env->prevview == SPOT_VIEWSTREAMDETAIL || 
	env->prevview == SPOT_VIEWRESULTS ||
	env->prevview == SPOT_VIEWARCHIVES)
      spot_sumupdate(env, &env->doc, env->menu, env->view, env->summaryitem);
    else
      return;

  if (disp != NULL && (env->view == SPOT_VIEWRESULTS ||
      env->view == SPOT_VIEWARCHIVES || env->view == SPOT_VIEWSTREAMS ||
      env->view == SPOT_VIEWSTREAMDETAIL || env->view == SPOT_VIEWDETAIL)) {
    for (i=disp->firstopt; i < disp->firstopt + disp->nrows && 
	i < disp->nopts; ++i) {
      opt = men_opt(disp, i);
      spot_fmtdisp(env->view, spot_fspec(opt, env->view), newval);
      if (strcmp(newval, opt->val)) {
	strncpy(opt->val, newval, disp->maxoptlen);
	men_update(disp, i);
      }
    }
  }

  if (env->view != SPOT_VIEWFILES && env->view != SPOT_VIEWQUERIES) {
    men_draw(disp);
    men_refresh(disp);
  }
}
