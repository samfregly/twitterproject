/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_copyfile					spotcopy.c

Function : Copies files.

Author : A. Fregly

Abstract : 

Calling Sequence : int retstat = spot_copyfile(struct spot_s_env env, 
	char *srcspec, char *destspec, int querycopy, int replace);

  env		Spot environment structure. Used for determining current view 
		mode and default directories.
  srcspec	Name of file to copy.
  destspec	Name of file, dirctory, or query library to which srcspec
		should be copied.
  querycopy	Flag used when view mode is STREAMDETAIL to indicate the the
		.qry and .hit files should be copied.
  replace	Flag used when copying direct access files (results, archives,
		query libraries). false -> append records from source files
		to destination files (if they exist), otherwise create new
		destination files.

Notes: 

Change log : 
000	24-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
long time(long *timebuf);
#else
long time();
#endif

#if FIT_ANSI
int spot_copyfile(struct spot_s_env *env, char *srcspec, char *destspec,
  int querycopy, int replace)
#else
int spot_copyfile(env, srcspec, destspec, querycopy, replace)
  struct spot_s_env *env;
  char *srcspec, *destspec;
  int querycopy, replace;
#endif
{
#if FIT_UNIX
#define BUFMAX 4096 
#else
#define BUFMAX TIPS_DESCRIPTORMAX
#endif

  char src[FIT_FULLFSPECLEN+1], dest[FIT_FULLFSPECLEN+1], *textptr;
  char docdescriptor[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char origname[TIPS_DESCRIPTORMAX+1], docname[TIPS_DESCRIPTORMAX+1];
  char arcname[TIPS_DESCRIPTORMAX+1], docnum_s[TIPS_DESCRIPTORMAX+1];
  char qryname[FIT_FULLFSPECLEN+1];
  char *(*filelist);
  int nbytes, retstat, docdescriptor_l;
  FILE *in, *out;
  char buf[BUFMAX+1];
  struct drct_s_file *drctin, *drctout;
  char errmessage[FIT_FULLFSPECLEN+64];
  long outdocnum, inlen, doclen, reclen, outdoclen, doclenfld, recnum;
  int desthandle;
  long act[3];
  struct qc_s_cmpqry *cmpqry;
  struct stat statbuf;


  fit_expfspec(srcspec, spot_curext(env->view), 
    spot_curdefault(env, env->view), src);
  fit_expfspec(destspec, spot_curext(env->view), src, dest);

  retstat = 1;

  if (env->view == SPOT_VIEWQUERIES || env->view == SPOT_VIEWFILES ||
      (env->view == SPOT_VIEWSTREAMDETAIL && querycopy)) {
    /* Copy of normal file, just do a straight copy */
    if ((in = fopen(src,"r")) == NULL) {
      sprintf(errmessage,"Unable to copy source: %s", src);
      spot_errmessage(errmessage);
      return 0;
    }
    if ((out = fopen(dest,"w")) == NULL) {
      fclose(in);
      sprintf(errmessage, "Unable to create copy destination file: %s", dest);
      spot_errmessage(errmessage);
      return 0;
    }
    while ((nbytes = fread(buf, 1, BUFMAX, in)) > 0)
      if (fwrite(buf, 1, nbytes, out) != nbytes) {
	sprintf(errmessage, "Error writing to output file: %s", dest);
        spot_errmessage(errmessage);
	break;
      }
    fclose(in);
    fclose(out);
  }

  if (env->view == SPOT_VIEWRESULTS || env->view == SPOT_VIEWSTREAMS ||
      env->view == SPOT_VIEWARCHIVES || (env->view == SPOT_VIEWSTREAMDETAIL &&
      querycopy)) {
    /* Copy of a direct access file, only copy existing records */

    /* If view mode is stream, set extension to ".qlb" on files so that
       the query library for the stream is copied */
    if (env->view == SPOT_VIEWSTREAMS) {
      if ((desthandle = creat(dest, 0)) >= 0) {
        act[0] = 0;
        time(&act[1]);
        act[2] = 0;
        write(desthandle, act, 3 * sizeof(act[0]));
	close(dest);
      }
      fit_setext(src,".qlb",src);
      fit_setext(dest,".qlb",dest);
    }
    else if (env->view == SPOT_VIEWSTREAMDETAIL) {
      fit_setext(src, ".hit", src);
      fit_setext(dest, ".hit", dest);
    }

    /* Open direct access source file */
    if ((drctin = drct_open(src, O_RDONLY, 0, -1, -1)) == NULL) {
      sprintf(errmessage,"Unable to read source: %s", src);
      spot_errmessage(errmessage);
      return 0;
    }

    /* Open direct access destination file */
    if (!stat(dest, &statbuf) && replace) {
      if  (!drct_delfile(dest)) {
	sprintf(errmessage, "Unable to replace destination file: %s", dest);
	spot_errmessage(errmessage);
	return 0;
      }
    }
    
    drctout = drct_open(dest, O_RDWR, 0, -1, -1);
    if (drctout == NULL)
      drctout = drct_open(dest, O_CREAT + O_RDWR, 0, -1, -1);

    if (drctout == NULL) {
      drct_close(drctin);
      sprintf(errmessage, "Unable to open destination file: %s", dest);
      spot_errmessage(errmessage);
      return 0;
    }

    /* Do the  copy */
    for (outdocnum = 0, nbytes = drct_rdseq(drctin, buf, BUFMAX, &doclen), 
	inlen = doclen; doclen > 0 && nbytes > 0;
	nbytes = drct_rdseq(drctin, buf, BUFMAX, &doclen), inlen = doclen) {
      buf[nbytes] = 0;

      if (env->view == SPOT_VIEWARCHIVES) {
	/* Copying an archive, have to remove document descriptor from
	   original and create new one appropriate for destination */
	tips_extractdescr(buf, docdescriptor, &docdescriptor_l);
	textptr = buf + docdescriptor_l;
	nbytes -= docdescriptor_l;
	outdoclen = doclen - docdescriptor_l;
	tips_parsedsc(docdescriptor, docname, &doclenfld, keyfld);
	tips_nameparse(docname, arcname, docnum_s, origname);
	tips_makedescr(NULL, outdocnum++, origname, outdoclen,
	    keyfld, TIPS_DESCRIPTORMAX, docdescriptor, &docdescriptor_l);
	if (!drct_append(drctout, docdescriptor, docdescriptor_l, 
	    (long) (outdoclen + docdescriptor_l))) {
	  sprintf(errmessage,"Unable to append to output archive: %s",
		dest);
          spot_errmessage(errmessage);
	  retstat = 0;
	  break;
	}
      }
      else {
	textptr = buf;
	outdoclen = doclen;
      }

      for (reclen = outdoclen; reclen > 0; textptr = buf) {
	if (nbytes > 0) {
          if (!drct_append(drctout, textptr, nbytes, outdoclen)) {
	    sprintf(errmessage,"Unable to append to output archive: %s",
		dest);
            spot_errmessage(errmessage);
	    retstat = 0;
	    break;
	  }
	  reclen -= nbytes;
	}
        if (reclen > 0) {
	  if (!(nbytes = drct_rdseq(drctin, buf, BUFMAX, &doclen))) {
	    sprintf(errmessage, "Error reading source file: %s", src);
	    spot_errmessage(errmessage);
	    retstat = 0; 
	    break;
	  }
	} 
      }
      
      if (!retstat) break;
    }
    drct_close(drctin);
    drct_close(drctout);

    if (retstat && env->view == SPOT_VIEWSTREAMDETAIL) {
      /* Need to submit copied query against the current stream */ 
      retstat = 0;
      fit_setext(dest, ".qry", dest);
      if ((filelist = (char *(*)) calloc(2, sizeof(*filelist))) != NULL) {
	if ((filelist[0] = (char *) calloc(strlen(dest)+1, 1)) != NULL) {
	  strcpy(filelist[0], dest);
	  filelist[1] = NULL;
	  retstat = spot_submit(filelist, env->searchdir, env->streamname, 1, 
	      0);
	}
	else
	  spot_errmessage("Error allocating internal data structure");
	fit_delfilelist(&filelist);
      }
      else
	spot_errmessage("Error allocating internal data structure");
    }
    return retstat;
  }

  if (env->view == SPOT_VIEWSTREAMDETAIL && !querycopy) {
    /* Copying a query in one query librar to another */
    retstat = 0;
    fit_setext(env->streamname, ".qlb", src);
    fit_setext(dest, ".qlb",dest);
    cmpqry = NULL;
   
    if ((drctin = qc_openlib(src, O_RDONLY, 0)) == NULL) {
      spot_errmessage("Error opening source query library");
    }
    else {
      if ((drctout = qc_openlib(dest, O_RDWR, 0)) == NULL) {
	spot_errmessage("Error opening destination query library");
      }
      else {
	if ((recnum = qc_findlib(drctin, srcspec)) < 0) {
	  spot_message("Error locating source query in query library");
	}
	else {
	  if (!qc_getlib(drctin, recnum, qryname, &cmpqry)) {
	    spot_errmessage("Error reading source query from query library");
	  }
	  else {
	    if (qc_putlib(drctout, qryname, cmpqry) < 0) {
	      spot_errmessage("Error inserting query in destination library");
	    }
	    else {
	      retstat = 1;
	    }
	    qc_dealloc(&cmpqry);
	  }
	}
	qc_closelib(drctout);
      }
    }
    qc_closelib(drctin);
  }

  return retstat; 
}
