/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_rebuilddisp					spotrebu.c

Function : Rebuilds file display.

Author : A. Fregly

Abstract : This function is used to rebuild the spot file display window
  so that the display is as close as possible to the currrent display, just
  updated to reflect new or deleted files.

Calling Sequence : void spot_rebuilddisp(struct spot_s_env *env); 

Notes: 

Change log : 
000	06-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
void spot_rebuilddisp(struct spot_s_env *env)
#else
void spot_rebuilddisp(env)
  struct spot_s_env *env;
#endif
{

  int len, cmpstat;
  char *(*filelist), curfilename[TIPS_DESCRIPTORMAX+1];
  char nextfile[TIPS_DESCRIPTORMAX+1];
  long i, j, nselected, topopt, curopt;
  struct men_s_option *opt;

  filelist = NULL;
  nselected = 0;
  curopt = -1;

  /* Make a copy of the file names in the current display which are
     selected */
  if (env->menu != NULL && env->menu->nopts && (env->view < SPOT_VIEWDETAIL ||
        env->view >= SPOT_VIEWDETAIL && (env->prevview == SPOT_VIEWFILES ||
	env->prevview == SPOT_VIEWQUERIES))) {
    if ((filelist = (char *(*)) calloc(env->menu->nopts, sizeof(*filelist))) ==
      NULL) goto ALLOCERR;
    for (i=0, nselected = 0; i < env->menu->nopts; ++i) {
      if ((opt = men_opt(env->menu, i)) != NULL && opt->descr.stat) {
	len = opt->descr.datalen - (env->view >= SPOT_VIEWDETAIL) *
	    FIT_BYTES_IN_LONG;
 	if ((filelist[nselected] = (char *) malloc(len+1)) == NULL)
	  goto ALLOCERR;
	strcpy(filelist[nselected++], spot_fspec(opt, env->view));
      }
    }
  }
  else if (env->menu != NULL)
    curopt = env->menu->curopt;

  if (env->menu != NULL) {
    topopt = env->menu->firstopt;
    opt = men_opt(env->menu, env->menu->curopt);
    strcpy(curfilename, spot_fspec(opt, env->view));
    fit_sextend(curfilename, TIPS_DESCRIPTORMAX);
  }
  else {
    topopt = 0;
    curfilename[0] = 0;
  }

  /* Create new file menu for the display */
  spot_viewinit(env, env->view, -1L, env->summaryitem);

  if (env->menu != NULL && env->menu->nopts && (env->view < SPOT_VIEWDETAIL ||
        env->view >= SPOT_VIEWDETAIL && (env->prevview == SPOT_VIEWFILES ||
	env->prevview == SPOT_VIEWQUERIES))) {
    /* Reset the status of any files which were selected in the previous
       display */
    for (i = 0, j = 0; i < env->menu->nopts && j < nselected; ++i) {
      for (; j < nselected; ++j) {
	opt = men_opt(env->menu, i);
	if ((cmpstat = strcmp(filelist[j], spot_fspec(opt, env->view))) >= 0) {
	  if (!cmpstat) {
	    opt->descr.stat = 1;
	    men_update(env->menu, i);
	  }
	  break;
	}
      }
    }
  }

  if (*curfilename && curopt < 0 && env->menu != NULL) {
    /* Find the current option in the display */
    for (i=0, env->menu->curopt = -1; i < env->menu->nopts; ++i) {
      opt = men_opt(env->menu, i);
      strcpy(nextfile, spot_fspec(opt, env->view));
      fit_sextend(nextfile, TIPS_DESCRIPTORMAX);
      cmpstat = strcmp(nextfile, curfilename);
      if (cmpstat >= 0) {
	env->menu->curopt = i;
        break;
      }
    }
    if (env->menu->curopt != i) env->menu->curopt = i - 1;
  }
  else if (curopt >= 0 && env->menu != NULL)
    env->menu->curopt = min(curopt, env->menu->nopts-1);

  if (env->menu != NULL) {
    /* Calculate what the top and current options should be in the
       rebuilt display, preserving their values as close as possible
       to previous values while ensuring that the current option is
       in the viewable portion of the display. */
    if (topopt > env->menu->nopts) topopt = env->menu->nopts -
	((env->menu->nrows - 1) * env->menu->ncols);
    topopt -= (topopt % env->menu->ncols);
    if (env->menu->curopt < 0) env->menu->curopt = topopt;
    if (env->menu->curopt < topopt) topopt = env->menu->curopt;
    if (env->menu->curopt >= 
	topopt + env->menu->nrows * env->menu->ncols) 
      topopt = env->menu->curopt - ((env->menu->nrows - 1) *
	env->menu->ncols);
    env->menu->firstopt = topopt - (topopt % env->menu->ncols);

    /* Redraw the display (internally only for now) */
    men_draw(env->menu); 
  }
  goto DONE;

ALLOCERR:
  spot_errmessage("Memory allocation error during screen rebuild");

DONE:
  for (i=0; i < nselected; ++i)
    free(filelist[i]);
  if (filelist != NULL) free(filelist);
  return;
}
