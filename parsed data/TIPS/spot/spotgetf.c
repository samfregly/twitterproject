/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_getfield					spotload.c

Function : Loads buffer with contents of document starting at specified field.

Author : A. Fregly

Abstract : 

Calling Sequence : void spot_getfield(struct spot_s_doc *doc, long doclen, 
  char *buf, int buflen, char *field, FILE *in, struct drct_s_file *arc,
  int *retlen);

  doc		Spot document descriptor structure.
  doclen	Amount of document left to be read.
  buf		Buffer in which to put loaded data.
  buflen	Length of buffer which may be used for storing data.
  field		Starting field to be loaded.
  in		If not NULL, Input file handle for file containing data to be
		loaded.
  arc		If "in" is NULL, archive file handle for document containing 
		data to be loaded.

Notes: 

Change log : 
000	17-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXFLDLEN 16

#if FIT_ANSI
void spot_getfield(struct spot_s_doc *doc, long doclen, char *buf, int buflen,
  char *field, FILE *in, struct drct_s_file *arc, int *retlen)
#else
void spot_getfield(doc, doclen, buf, buflen, field, in, arc, retlen)
  struct spot_s_doc *doc;
  long doclen;
  char *buf;
  int buflen;
  char *field;
  FILE *in;
  struct drct_s_file *arc;
  int *retlen;
#endif
{

  long reclen;
  int bufoff, nbytes, i, fieldidx, foundfield, atfield, avail;
  char infield[MAXFLDLEN+1], fielditem[MAXFLDLEN+1];

  *retlen = 0;
  buf[0] = 0;

  if (arc == NULL && in == NULL) return;

  /* See if field is defined */
  fieldidx = -1;
  strncpy(infield, field, MAXFLDLEN);
  infield[MAXFLDLEN] = 0;
  fit_supcase(infield);
  fielditem[MAXFLDLEN] = 0;

  for (i=0; i < doc->nfields; ++i) {
    strncpy(fielditem, doc->fieldnames[i], MAXFLDLEN);
    fit_supcase(fielditem);
    if (!strcmp(infield, fielditem)) {
      fieldidx = i;
      break;
    }
  }
  if (fieldidx < 0) return;

  if (!doc->buflen || doc->docbuf == NULL) goto DONE;

  /* Read document until it is all gone or we have found our line */ 
  for (atfield = 0, foundfield = 0; doclen > 0;) {
    if (in != NULL) 
      nbytes = fread(doc->docbuf, 1, doc->buflen, in);
    else if (arc != NULL)
      nbytes = drct_rdrec(arc, arc->currec, (char *) doc->docbuf, doc->buflen,
	&reclen);

    if (!nbytes) break; /* No more text, quit */
    doclen -= nbytes;

    for (bufoff = 0; bufoff < nbytes;) {

      for (; !foundfield && !atfield && bufoff < nbytes;)
        atfield =  ((unsigned char) doc->docbuf[bufoff++] == spot_defaultdelim);

      if (atfield && bufoff < nbytes) {
        foundfield = ((unsigned char) doc->docbuf[bufoff] == fieldidx);
        atfield = 0;
	++bufoff;
      }
      else
	bufoff += (!foundfield);

      if (bufoff < nbytes && foundfield) {
	avail = nbytes - bufoff;
        if (avail > (buflen - *retlen)) avail =  buflen - *retlen;
	memcpy(buf + *retlen, doc->docbuf + bufoff, avail);
	*retlen += avail;
	if (*retlen >= buflen) goto DONE;
	bufoff += avail;
      } 
    }
  }

DONE:
  /* Free up the temporary buffer */
  if (in == NULL && arc != NULL) drct_cancelio(arc);
}
