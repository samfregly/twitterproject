/******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_ynprompt					spotympr.c

Function : Gives user a yes/no prompt to answer.

Author : A. Fregly

Abstract : 

Calling Sequence : spot_ynprompt(char *promptstr);

  promptstr	Prompt string to put as first line of requestor.

Notes: 

Change log : 
000	23-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

static int firsttime=1;
static char *cmdopts[] = {"Yes", "No", ""};

static WINDOW *outerwin, *saveouterwin, *cmdwin, *namewin;
static struct men_s_menu *cmdmenu;


#if FIT_ANSI
int spot_ynprompt(struct spot_s_env *env, char *promptstr)
#else
int spot_ynprompt(env, promptstr)
  struct spot_s_env *env;
  char *promptstr;
#endif
{

  int optselected, menutype, promptlen, timedout;
  long retopt;
  char promptbuf[81];

  if (firsttime) {
    /* Create windows and command menu */
    firsttime = 0;
    if ((outerwin = newwin(4, COLS, 5, 0)) == NULL) return;
    if ((namewin = newwin(1, COLS-2, 6, 1)) == NULL) return;
    if ((cmdwin = newwin(1, 9, 7, ((COLS-2)-10)/2)) == NULL) return;
    menutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_HORIZONTALWRAP +
	MEN_ATTR_VERTICALWRAP;
    if (env->usepointer) menutype += MEN_ATTR_POINTER;
    if (env->usehotkeys) menutype += MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT;
    if (env->uselinedrawing) {
      if ((cmdmenu = men_init(cmdwin, menutype, NULL, cmdopts, env->mennorm,
	env->menhigh, env->mensel, env->menhot, ACS_RARROW, ACS_DIAMOND,
	1, 0)) == NULL) return;
    }
    else {
      if ((cmdmenu = men_init(cmdwin, menutype, NULL, cmdopts, env->dispnorm,
	env->disphigh, env->dispsel, env->disphot, ACS_RARROW, ACS_DIAMOND,
	1, 0)) == NULL) return;
    }
    win_dup(outerwin, &saveouterwin);
    if (env->uselinedrawing) 
      win_box(outerwin);
    else {
      wattrset(outerwin, env->mennorm);
      win_fill(outerwin, 0, 0, 0, 0, ' ');
    }
  }

  if (outerwin == NULL || cmdwin == NULL || namewin == NULL || cmdmenu == NULL 
	|| saveouterwin == NULL) {
    spot_message("Unable to create prompt display");
    return 0;
  }

  /* Save the area of the screen being overwritten */
  overwrite(spot_dispwin, saveouterwin);

  promptbuf[80] = 0;
  strncpy(promptbuf, promptstr, 80);
  promptlen = strlen(promptbuf);
  if (promptlen > COLS-2) promptlen = COLS-2;
  promptbuf[promptlen] = 0;

#if FIT_SUN
  if (!env->uselinedrawing) {
    wattrset(outerwin, env->mennorm);
    win_fill(outerwin, 0, 0, 0, 0, ' ');
    touchwin(outerwin);
    wrefresh(outerwin);
  }
#endif
  
  if (env->uselinedrawing) {
    wattrset(outerwin, env->mennorm);
    wattrset(namewin, env->mennorm);
  }
  else {
    wattrset(outerwin, env->dispnorm);
    wattrset(namewin, env->dispnorm);
  }

  touchwin(outerwin);
  wrefresh(outerwin);

  win_fill(outerwin, 1, 1, 2, COLS-2, ' ');
  touchwin(outerwin);
  wrefresh(outerwin);

  win_fill(namewin, 0, 0, 0, 0, ' ');
  mvwaddstr(namewin, 0, (COLS-promptlen-2) / 2, promptbuf);
  touchwin(namewin);
  wrefresh(namewin);
  cmdmenu->curopt = 1;
  men_draw(cmdmenu);
  men_select(cmdmenu, 0, &retopt, &optselected, &timedout);

  overwrite(saveouterwin, spot_dispwin);
  wrefresh(spot_dispwin);

  return(((int) !retopt) && optselected);
}
