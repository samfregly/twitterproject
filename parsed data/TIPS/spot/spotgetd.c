/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_getdoc					spotgetd.c

Function : Initializes document temp file and pointers into it.

Author : A. Fregly

Abstract : This routine readies a document for access by the spot
  browse of print functions. The document is copied to an output file, and
  may optionally be cleaned up (removal of control characters), spacing is 
  inserted for a left margin, text is wrapped, and an array of offsets of the 
  start of each line is set up.

Calling Sequence : int stat = spot_getdoc(struct spot_s_env *env,
  struct spot_s_doc *doc, long docid, int format, int relative);

  env		Spot environment structure.
  doc		Spot document descriptor structure.
  docid		Document specified.
  format	Flag, non-zero then clean up the document.
  relative	Flag, non-zero then docid is relative to current document.
  stat		Returned status, 0 if docid a document could not be retrieved.

Notes :

Change log :
000	08-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>
#include <fcntl.h>


#if FIT_ANSI
int spot_getdoc(struct spot_s_env *env, struct spot_s_doc *doc, long docid,
  int format, int relative)
#else
int spot_getdoc(env, doc, docid, format, relative)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  long docid;
  int format, relative;
#endif
{

  static FILE *in;
  struct drct_s_file *arc;
  char *outline;
  long reclen, doclen, modtime;
  int stat, bufoff, nbytes, outlen, more;

  outline = NULL;

  if (!(stat = spot_finddoc(env, doc, docid, relative, 1, 0, 1, &in, &arc, 
      &doclen, &modtime))) {
    goto DONE;
  }

  /* Rewind the work file */
  rewind(doc->temp);

  if (doc->docbuf == NULL || doc->buflen <= 0) {
    goto DONE;
  }

  if (!format) {
    /* No formatting, just copy the input document to the output file */
    for (nbytes = 1; doclen && nbytes; doclen -= nbytes) {
      nbytes = (doc->buflen > doclen) ? doclen : doc->buflen;

      /* More text to read from document, load it into document buffer */
      if (arc != NULL)
        nbytes = drct_rdrec(arc, arc->currec, (char *) doc->docbuf, nbytes, 
	  &reclen);
      else if (in != NULL) 
        nbytes = (unsigned int) fread(doc->docbuf, 1, nbytes, in);

      if (nbytes && fwrite(doc->docbuf, 1, nbytes, doc->temp) != nbytes)
	nbytes = 0;
    }
    stat = (doclen == 0);
    if (!stat) {
      goto DONE;
    }
  }

  /* Allocate output line buffer */
  outline = (char *) calloc(doc->wrapcol+2, 1);
  if (outline == NULL) {
    goto DONE;
  }

  /* Copy input document to work file, doing formatting and control character
     stripping */
  for (nbytes = 0, bufoff = 0, more = 1;;) {
    if (bufoff < nbytes) {
      /* Move unprocessed text from last pass to front of buffer */
      memmove(doc->docbuf, doc->docbuf + bufoff, nbytes - bufoff);
      bufoff = nbytes - bufoff;
    }
    else
      bufoff = 0;

    if (more) {
      /* More text to read from document, load it into document buffer */
      if (arc != NULL)
        nbytes = drct_rdrec(arc, arc->currec, (char *) doc->docbuf + bufoff, 
	  doc->buflen - bufoff, &reclen);
      else if (in != NULL) 
        nbytes = (unsigned int) fread(doc->docbuf, 1, doc->buflen - bufoff, 
 	  in);
      doclen -= nbytes;
    }
    else
      nbytes = 0;

    /* Determine amount of text to be processed */
    nbytes += bufoff;
    if (!nbytes) break; /* No more text, quit */

    more = (doclen > 0);
    if (!more) {
      /* On last buffer, insert trailing linefeed if needed so that last line
	 will get inserted into temp file */
      if (doc->docbuf[nbytes-1] != '\n')
        doc->docbuf[nbytes++] = '\n';
    }
    doc->docbuf[nbytes] = 0;

    for (bufoff = 0; bufoff < nbytes;) {
      /* Parse input one line at a time, adding to output */
      if (!spot_getline((char *) doc->docbuf, &bufoff, nbytes, doc->margin, 
        doc->wrapcol, spot_defaultdelim, outline, &outlen)) break;
      if (outlen) {
        fwrite(outline, 1, outlen, doc->temp);
        ++doc->nlines;
      }
    }
  }

  /* Free up the temporary buffer */
  free(outline);
  outline = NULL;
  stat = 1;

DONE:
  if (in != NULL) fclose(in);
  if (outline != NULL) free(outline);
  if (arc != NULL) drct_unlock(arc);
  doc->doclen = ftell(doc->temp);
  return stat;
}
