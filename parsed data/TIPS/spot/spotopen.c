/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_open						spotope.c

Function : Handles file open/create options off main file menu.

Author : A. Fregly

Abstract : 

Calling Sequence : int spot_open(struct spot_s_env *env, int optidx, 
  int newflag);

Notes: 

Change log : 
000  20-FEB-92  AMF	Created.
001  11-SEP-93  AMF	Fixed call to fstat. Make friendly to strict compilers.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_open(struct spot_s_env *env, int optidx, int newflag)
#else
int spot_open(env, optidx, newflag)
  struct spot_s_env *env;
  int optidx, newflag;
#endif
{

  char *(*filelist), retpath[FIT_FULLFSPECLEN+1], srcspec[FIT_FULLFSPECLEN+1];
  int updated, ftype, nbytes;
  long doclen;
  struct drct_s_file *drcthandle;
  int inhandle, view;
  struct men_s_option *opt;
  struct stat statbuf;
  FILE *in;
  struct drct_s_info arcinfo;

  filelist = NULL;
  ftype = 0;

  if (env->view >= SPOT_VIEWDETAIL)
    view = (env->prevview != SPOT_VIEWQUERIES) ? SPOT_VIEWFILES : 
	SPOT_VIEWQUERIES;
  else
    view = env->view;

  if (newflag) {
    if (!spot_fprompt(env, 1, "Enter name of file to create", NULL,
	spot_curext(view), spot_curdefault(env, view), srcspec, 
	retpath, &ftype))
      goto DONE;
  }
  else if (env->menu == NULL) {
    spot_message("No files are currently available for opening");
    goto DONE;
  }
  else {
    opt = men_opt(env->menu, env->menu->curopt);
    /* Opening existing file, use current option on file menu */
    strcpy(srcspec, spot_fspec(opt, env->view));
  }

  if ((view == SPOT_VIEWSTREAMDETAIL || view == SPOT_VIEWRESULTS) &&
       !newflag) {
    /* Opens always edits the query for searches */
    fit_setext(srcspec, ".qry", srcspec);
    fit_expfspec(srcspec, ".qry", env->searchdir, srcspec);
  }
  else
    fit_expfspec(srcspec, spot_curext(view), spot_curdefault(env, view), 
      srcspec);

  if (view == SPOT_VIEWFILES || view == SPOT_VIEWQUERIES ||
      view == SPOT_VIEWSTREAMDETAIL || (view == SPOT_VIEWRESULTS && 
      !newflag)) {

    /* Create/Edit file or query */
    spot_edit(srcspec, spot_getapplication(srcspec), &updated);

    if (view == SPOT_VIEWSTREAMDETAIL && updated) {
      /* Submit new query against stream */
      if ((filelist = (char *(*)) calloc(2, sizeof(*filelist))) == NULL)
  	spot_errmessage("Error allocating internal data structure");
      else {
  	if ((filelist[0] = (char *) calloc(FIT_FULLFSPECLEN+1, 1)) == NULL)
  	  spot_errmessage("Error allocating internal data structure");
  	else {
  	  strncpy(filelist[0], srcspec, FIT_FULLFSPECLEN);
  	  filelist[1] = NULL;
  	  spot_submit(filelist, env->searchdir, env->streamname, 1, 0);
        }
      }
    }
    else if (updated && env->view >= SPOT_VIEWDETAIL && newflag) {
      if (env->prevview == SPOT_VIEWARCHIVES) {
	/* Load the file into the archive */
	if ((in = fopen(srcspec, "r")) != NULL) {
	  drct_wlock(env->doc.handle, 0L, 0L);
	  drct_info(env->doc.handle, &arcinfo);
	  if (!fstat(fileno(in), &statbuf)) {
	    sprintf((char *) env->doc.docbuf,"%ld#%s,%ld\n", arcinfo.nrecs, 
		srcspec, statbuf.st_size);
	    nbytes = strlen(env->doc.docbuf);
	    doclen = nbytes + statbuf.st_size;
	    do {
	      if (!drct_append(env->doc.handle, (char *) env->doc.docbuf, 
		  nbytes, doclen)) {
		spot_errmessage("Error appending file to archive");
	        break;
	      }
	      doclen -= nbytes;
	      nbytes = (doclen > env->doc.buflen) ? env->doc.buflen : doclen;
	      if (doclen) nbytes = fread(env->doc.docbuf, 1, nbytes, in);
	    } while (nbytes && doclen);
	    if (doclen) drct_cancelio(env->doc.handle);
	  }
	  else
	    spot_errmessage("Unable to load document into archive, status\
 inaccessable");
	  fclose(in);
	  drct_unlock(env->doc.handle);
	}
	else
	  spot_errmessage("Unable to load document into archive, file could\
 not be opened");
      }
      else {
	/* Append document to result file being used for the summary display */
	if (!stat(srcspec, &statbuf)) {
	  sprintf((char *) env->doc.docbuf, "%s,%ld\n", srcspec, 
	    statbuf.st_size);
	  doclen = strlen((char *) env->doc.docbuf);
	  if (!drct_append(env->doc.handle, (char *) env->doc.docbuf, 
		(int) doclen, doclen))
	    spot_errmessage("Error inserting document reference into result\
 file");
	}
	else
	  spot_errmessage("Unable to insert document into result file, status\
 inaccessable"); 
      }
    }
  }
  else if (view == SPOT_VIEWARCHIVES || view == SPOT_VIEWRESULTS ||
	   view == SPOT_VIEWSTREAMS || view >= SPOT_VIEWDETAIL) {
    if (newflag) {
      /* Creating a stream, archive, or result file */
      if (view == SPOT_VIEWSTREAMS) {
	fit_setext(srcspec, ".act", srcspec);
  	if ((inhandle = creat(srcspec, 0664)) < 0)
  	  spot_errmessage("Error creating new file");
  	else
  	  close(inhandle);
  	fit_setext(srcspec, ".qlb", srcspec);
      }
      /* Create the object library (for a stream), archive or result file */
      if ((drcthandle = drct_open(srcspec, O_RDWR + O_CREAT, 0664,-1,-1)) == 
	  NULL)
  	spot_errmessage("Error creating file");
      else
  	drct_close(drcthandle);
    }
    else if (view == SPOT_VIEWARCHIVES) {
      /* Opening an archive, browse it */
      spot_docinit(env, &env->doc, 1);
      spot_browse(env, &env->doc, 0L);
      spot_docdeall(&env->doc);
      men_draw(spot_cmdmenu);
      wrefresh(spot_cmdwin);
      spot_rebuilddisp(env);
      wrefresh(spot_dispwin);
      wrefresh(spot_footerwin);
    }
    else {
      /* Has to be a stream open, allow user to use the stream status display,
	 archive would have been browsed above, and open of existing result
	 file was taken care of by editing the query */
      env->view = SPOT_VIEWSTREAMDETAIL;
      strcpy(env->streamname, srcspec);
      spot_viewinit(env, env->view, -1L, "");
      wrefresh(spot_dispwin);
      wrefresh(spot_footerwin);
    }
  }

DONE:
  if (filelist != NULL) fit_delfilelist(&filelist);
}
