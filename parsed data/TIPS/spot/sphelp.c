/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : sphelp							sphelp.c

Function : Help program for use with spot user interface.

Author : A. Fregly

Abstract : This function provides help for the spot user interface.
  The help is function is basically a modified version of the spot browser,
  with the difference being that there is no command menu. In place of the
  command menu, a summary list of help documents is presented to the user
  to allow them to choose a topic to be displayed. The summary display
  consists of the first line of text from each document in the result
  file.

Calling Sequence : sphelp result_file {docnum}

  result_file	TIPS result file containing document references of documents
		which make up elements of the help file.
  docnum	Optional document number of initial document to display. If
		docnum is not specified, the user is allowed to choose the
		initial document from the help summary display.

Notes: 

Change log : 
000	11-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sphelp.h>

struct spot_s_env spot_env;

int spot_dispwidth, spot_dispheight, spot_hardscroll;

WINDOW *spot_labelwin, *spot_cmdwin, *spot_dispwin, *spot_footerwin;
WINDOW *spot_promptwin, *spot_selectwin;
WINDOW *spot_filewin, *spot_viewwin, *spot_querywin, *spot_searchwin;
WINDOW *spot_actionwin, *spot_detailwin, *spot_addwin;

WINDOW *spot_brfilewin, *spot_brviewwin, *spot_bractionwin;
WINDOW *spot_brcopywin, *spot_brviewtypewin, *spot_brsummaryoptwin;
WINDOW *spot_brdeloptwin;

WINDOW *spot_errwin, *spot_msgwin, *spot_srcwin;
WINDOW *spot_errwinsave, *spot_msgwinsave, *spot_promptwinsave;

struct men_s_menu *spot_dispmenu, *spot_cmdmenu, *spot_selectmenu;
struct men_s_menu *spot_filemenu, *spot_viewmenu, *spot_querymenu;
struct men_s_menu *spot_searchmenu, *spot_actionmenu;
struct men_s_menu *spot_detailmenu, *spot_addmenu;

struct men_s_menu *spot_brfilemenu, *spot_brviewmenu, *spot_bractionmenu;
struct men_s_menu *spot_brcopymenu, *spot_brviewtypemenu;
struct men_s_menu *spot_brsummaryoptmenu, *spot_brdeloptmenu;

struct men_s_menu *spot_srcmenu;

struct spot_s_actiondescr spot_actions[SPOT_MAXVIEW+1];

char spot_curpath[FIT_FULLFSPECLEN+1];
char spot_filedest[FIT_FULLFSPECLEN+1], spot_arcdest[FIT_FULLFSPECLEN+1];
char spot_searchdest[FIT_FULLFSPECLEN+1],spot_streamdest[FIT_FULLFSPECLEN+1];
char spot_streamdestname[FIT_FULLFSPECLEN+1];
char spot_filefilter[FIT_FULLFSPECLEN+1];

int spot_menukey, spot_helpkey, spot_exitkey, spot_quitkey;
char *spot_menukeyname, *spot_helpkeyname, *spot_exitkeyname, *spot_quitkeyname;

char spot_disphelp[128], spot_cmdhelp[128], spot_edithelp[128];
char spot_selectorhelp[128], spot_multselectorhelp[128], spot_brdisphelp[128];
char spot_fieldedithelp[128], spot_selectorcmdhelp[128], spot_multmenuhelp[128];

int spot_neditor;
char *(*spot_editor), *(*spot_editortype);
char spot_defaulteditor[FIT_FULLFSPECLEN+1];
char spot_mailer[FIT_FULLFSPECLEN+1];

/* Print command and file types to which they apply */
int spot_nprintcmd;
char *(*spot_printcmd), *(*spot_printtype);
char spot_defaultprintcmd[FIT_FULLFSPECLEN+1];

char *spot_progname;
char spot_shell[FIT_FULLFSPECLEN+1];
char spot_defaultfields[FIT_FULLFSPECLEN+1];
int spot_defaultdelim;

extern long *sphelp_helpidx;
long *sphelp_helpidx;

#if SPHELP_DEBUG
FILE *fit_debug_handle;
#endif


#if FIT_ANSI
int main(int argc, char *argv[])
#else
int main(argc, argv)
  int argc;
  char *argv[];
#endif
{

  int key, optselected, exitflag;
  long retopt;
  char hspec[FIT_FULLFSPECLEN+1], errmessage[FIT_FULLFSPECLEN+64];
  int retstat, alarmset;
  struct drct_s_file *help;
  long opt;

#if SPHELP_DEBUG
  fit_debug_handle = fopen("sphelp.dbg", "w");
  fprintf(fit_debug_handle, "sphelp, initializing\n");
#endif

  /* Set program name */
  spot_progname = *argv;

  /* Get current default directory */
  getcwd(spot_curpath, FIT_FULLFSPECLEN);

  if (!--argc) {
    fprintf(stderr,
      "No help file was specified on command line, hit [Enter] to continue\n");
    key = getchar();
    return 0;
  }
  strncpy(hspec, *(++argv), FIT_FULLFSPECLEN);
  hspec[FIT_FULLFSPECLEN] = 0;

  fit_expfspec(hspec, ".hit", spot_curpath, hspec);

  if ((help = drct_open(hspec, O_RDONLY, 0, -1L, -1L)) == NULL) {
    sprintf(errmessage, "Unable to open helpfile: %s\n", hspec);
    perror(errmessage);
    fprintf(stderr, "Hit [Enter] to continue\n");
    key = getchar();
    return 0;
  }

  /* initialize curses (screen handling functions) */
  if (initscr() == NULL) {
    retstat = errno;
    perror("Error from initscr");
    exit(retstat);
  }

#if FIT_TERMINFO
  spot_env.usecolor = (start_color() != ERR);
#else
  spot_env.usecolor = 0;
#endif
  if ((retstat = noecho()) == ERR) goto ERREXIT;
#if !FIT_COHERENT
  if ((retstat = intrflush(stdscr, FALSE)) == ERR) goto ERREXIT;
#endif
  if ((retstat = keypad(stdscr, TRUE)) == ERR) goto ERREXIT;
  if ((retstat = raw()) == ERR) goto ERREXIT;
#if !FIT_HP && !FIT_COHERENT
  retstat = (curs_set(0) != ERR);
#endif

  /* Initialize display menu to NULL */
  spot_dispmenu = NULL;

  /* Read in environment */
  if (!spot_getenv(&spot_env)) goto ERREXIT;

  /* Setup help lines */
  sprintf(spot_disphelp, "Press %s for menu, %s to exit", spot_menukeyname,
	spot_exitkeyname);
  sprintf(spot_cmdhelp, "Press [Enter] to select, %s to exit", 
	spot_exitkeyname);

  /* Create display windows and menus, and set their attributes */
  if (!sphelp_makedisplay(&spot_env, help)) goto ERREXIT;

  if (spot_dispmenu == NULL) {
    spot_message("Help file is empty");
    goto DONE;
  }

  exitflag = 0;

  /* Set up signal handlers */
  signal(SIGTERM, SIG_IGN);
  signal(SIGINT, SIG_IGN);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGPIPE, SIG_IGN);
  signal(SIGQUIT, SIG_IGN);
#endif

  while (1) {
    spot_puthelpline(spot_cmdhelp);
    optselected = 0;
    alarmset = 0;
    key = 0;
    flushinp();
    for (; !optselected;) {
      key = men_select(spot_dispmenu, 0, &retopt, &optselected, &alarmset);
      if (key == spot_quitkey || key == spot_exitkey || key == KEY_EXIT ||
	  key == KEY_SEXIT || key == KEY_CANCEL || key == KEY_SCANCEL ||
	  key == ('z' - 'a' + 1) || key == ('d' - 'a' + 1))
	goto DONE;
    }
    opt = sphelp_helpidx[spot_dispmenu->curopt];
    for (key = 0; key != spot_menukey;) {
      key = sphelp_browse(&spot_env, help, opt, &opt);
      if (key == spot_menukey) break;
      if (key == spot_quitkey || key == spot_exitkey || key == KEY_EXIT ||
          key == KEY_SEXIT || key == KEY_CANCEL || key == KEY_SCANCEL ||
	  key == ('z' - 'a' + 1) || key == ('d' - 'a' + 1))
        goto DONE;
    }
    men_draw(spot_dispmenu);
    men_put(spot_dispmenu, spot_dispwin);
    wrefresh(spot_dispwin);
  }

DONE:
  endwin();
  return 0;

ERREXIT:
  endwin();
  perror("Exiting due to system error, hit [Enter] to continue");
  key = getchar();
  return errno;
}
