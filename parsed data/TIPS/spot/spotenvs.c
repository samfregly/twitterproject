/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_envseq					spotenvs.c

Function : Retrieves values for sequence of environment variables.

Author : A. Fregly

Abstract : This function is used to retrieve the value for a sequence of
	environment variables. A sequence consists of a number of variables
	which have a common prefix, and are terminated by a sequence number.
	The sequence numbers must be consecutive and start at 1. For example,
	the variables SP_EDITOR1, SP_EDITOR2, ... are defined to spot as a
	sequence which defines editory according to file type. The values
	defined by the variables consist of two elements, the first being the
	name of a program or script, and the second being a file extension
	which is used in determining which files the program or script should
	be used to process. For example, the following definition:

	  SP_EDITOR1="/usr/spotsys/bin/qryed .qry"
	  export SP_EDITOR1

	specifies the the spot query editor is to be used for editing files
	which have a name ending with ".qry".

Calling Sequence : spot_envseq(char *root, char *defval, char *(*(*applist)),
	char *(*(*ftypes)), int *n);

  root		Prefix for the variables which all in the sequence will share.
  defval	Returned value for environment variable which has no sequence
		number, i.e. is equal to root.
  applist	Returned list of applications defined as the first element of
		the value defined by each variable in the sequence.
  ftypes	Returned list of file types defined as the second element of
		the value defined by each variable in the sequence.
  n		Returned number of values in applist/ftypes.

Notes: 

Change log : 
000	04-APR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
void spot_envseq(char *root, char *defval, char *(*(*applist)),
  char *(*(*ftypes)), int *n)
#else
void spot_envseq(root, defval, applist, ftypes, n)
  char *root, *defval, *(*(*applist)), *(*(*ftypes));
  int *n;
#endif
{

  char *varptr;
  int count;
  char varname[128];
  char app[FIT_FULLFSPECLEN+1], ftype[FIT_FULLFSPECLEN+1];

  defval[0] = 0;
  if ((varptr = getenv(root)) != NULL)
    strncpy(defval,varptr,FIT_FULLFSPECLEN);
  defval[FIT_FULLFSPECLEN] = 0;

  for (count = 0;; count += 1) {
    sprintf(varname, "%s%d", root, count+1);
    if ((varptr = getenv(varname)) == NULL) break;
  }

  /* Build array of editor names and corresponding file types */
  if (count) {
    *applist = (char *(*)) calloc(count, sizeof(*(*applist)));
    *ftypes = (char *(*)) calloc(count, sizeof(*(*ftypes)));
  }
  else {
    *applist = NULL;
    *ftypes = NULL;
  }

  if (*applist != NULL && *ftypes != NULL) {
    for (*n=0; *n < count; *n += 1) {
      sprintf(varname, "%s%d", root, *n+1);
      varptr = getenv(varname);
      sscanf(varptr, "%s %s", app, ftype);
      (*applist)[*n] = (char *) calloc(strlen(app)+1, 1);
      if ((*applist)[*n] == NULL) break;
      strcpy((*applist)[*n], app);
      (*ftypes)[*n] = (char *) calloc(strlen(ftype)+1, 1);
      if ((*ftypes)[*n] == NULL) break;
      strcpy((*ftypes)[*n], ftype);
    }
    if (*n < count) *n = 0;
  }
  else
    *n = 0;
}
