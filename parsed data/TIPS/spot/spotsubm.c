/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_submit					spotsubm.c

Function : Submits query for profiling against a stream.

Author : A. Fregly

Abstract : This function is used to submit one or more queries for profiling
	against a stream. The queries are compiled and placed in the query
	library for the stream, and then a "reload" message is sent to the
	spotter for the stream to tell it to reload the queries from the
	library.  If any  of the queries have compilation errors, the errors
	are displayed to the user, and the query is not placed into the query
	library.

Calling Sequence : int nsubmitted = spot_submit(char *filelist[], 
    char *defpath, char *libname, int reload, int replace);

  filelist	List of file names for queries to use in the submit.
  defpath	Default path in which to look for queries.
  libname	Query library to be updated.
  reload	Non-zero, then an attempt will be made to notify the spotter
		for the library that a reload should be done.
  replace	Specifies that hit files should be replaced.
  nsubmitted	Returned number of queries successfully submitted.

Notes: 

Change log : 
000	20-OCT-91  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

static char CONTMSG[] = "Hit <Enter> to continue...\n";


#if FIT_ANSI
int spot_submit(char *filelist[], char *defpath, char *qlibname, int reload, 
  int replace)
#else
int spot_submit(filelist, defpath, qlibname, reload, replace)
  char *filelist[], *defpath, *qlibname;
  int reload, replace;
#endif
{

  int count, i, j, nerrs, errnum, n, nbytes, c;
  char fspec[FIT_FULLFSPECLEN+1], errmessage[FIT_FULLFSPECLEN*2], *querytext;
  char qrylibname[FIT_FULLFSPECLEN+1], *errmsg;
  struct stat statbuf;
  struct qc_s_cmpqry *cmpqry;
  FILE *in;
  struct drct_s_file *qrylib, *hitfile;

  qrylib = NULL;
  querytext = NULL;
  in = NULL;
  nbytes = 0;
  cmpqry = NULL;
  count = 0;

  /* Quit if no entries in file list */
  if (filelist == NULL) {
    spot_message("No selected entries");
    return 0;
  }

  fit_setext(qlibname, ".qlb", qrylibname);
  if ((qrylib = qc_openlib(qrylibname, O_RDWR, 0)) == NULL) {
    if ((qrylib = qc_openlib(qrylibname, O_RDWR + O_CREAT, 0)) == NULL) {
      sprintf(errmessage, "Erro opening query library for stream: %s",
	qlibname);
      spot_errmessage(errmessage);
      return 0;
    }
  }

  for (i=0, count = 0; filelist[i] != NULL; ++i) {
    if (*filelist[i]) { 
      querytext = NULL;
      in = NULL;
      nbytes = 0;
      cmpqry = NULL;

      fit_expfspec(filelist[i], ".hit", defpath, fspec);
      fit_setext(fspec, ".hit", fspec);

      if (replace && !stat(fspec, &statbuf)) {
	if (!drct_delfile(fspec)) {
	  sprintf(errmessage, "Error deleting previous result file: %s", fspec);
	  spot_errmessage(errmessage);
	  goto DONE;
	}
      }
      if (stat(fspec, &statbuf)) {
        /* attempt to create hit file */
        if ((hitfile = drct_open(fspec, O_RDWR + O_CREAT, 0, -1 , -1)) == NULL){
	  sprintf(errmessage, "Unable to initialize result file: %s", fspec);
	  spot_errmessage(errmessage);
	  goto DONE;
	}
	else
	  drct_close(hitfile);
      }

      fit_setext(fspec, ".qry", fspec);
      if (stat(fspec, &statbuf) || !statbuf.st_size) {
	sprintf(errmessage, "Unable to locate query: %s", fspec);
	spot_errmessage(errmessage);
      }

      /* query was created/updated, see if it will compile */
      else if ((querytext = (char *) malloc((size_t) statbuf.st_size + 1)) ==
	  NULL) {
        spot_errmessage("Error allocating query text buffer"); 
	goto DONE;
      }

      else if ((in = fopen(fspec, "r")) == NULL) {
	sprintf(errmessage,"Unable to open query file: %s", fspec);
	spot_errmessage(errmessage);
        goto DONE;
      }

      else {
        nbytes = (int) statbuf.st_size;
        if (fread(querytext, 1, nbytes, in) != nbytes) {
	  sprintf(errmessage, "Unable to read query: %s", fspec);
          spot_errmessage(errmessage);
	  nbytes = 0;
        }
      }

      if (nbytes) {
	querytext[nbytes]  = 0;
        if (nerrs = qc_compile(querytext, 0, NULL, 0, NULL, NULL, &cmpqry)) {
	  sprintf(errmessage, "Query contained %d errors: %s", nerrs, fspec);
	  spot_message(errmessage);
	  endwin();
          for (j=0; j < nerrs; ++j) {
            errmsg = qc_geterr(j,&errnum,&n);
	    if (errmsg == NULL) errmsg = "";
            qc_puterr(stdout, querytext, errmsg, n);
	    if (!((j+1) % ((LINES-2) / 2))) {
	      printf(CONTMSG);
              c = getchar();
	    }
	  }
	  if (((j) % ((LINES-2) / 2))) {
	    printf(CONTMSG);
            c = getchar();
	  }
	  wrefresh(spot_cmdwin);
	}
	else {
          if (qc_putlib(qrylib, fspec, cmpqry) < 0) {
	    sprintf(errmessage, "Error inserting query into query library: %s",
		fspec);
	    spot_errmessage(errmessage);
	    goto DONE;
	  }
	  else
	    count += 1;
	}
      }
      /* Free temp storage used for query compilation */
      if (querytext != NULL) free(querytext);
      querytext = NULL;
      if (cmpqry != NULL) qc_dealloc(&cmpqry);
      if (in != NULL) fclose(in);
      in = NULL;
    }
  }

  if (qrylib != NULL) {
    qc_closelib(qrylib);
    qrylib = NULL;
  }

  if (count && reload)
    spot_pupdate(qrylibname);
  else if (reload)
    spot_message("Query update terminated, no valid queries");

DONE:
  if (qrylib != NULL) drct_close(qrylib);
  if (querytext != NULL) free(querytext);
  if (in != NULL) fclose(in);
  if (cmpqry != NULL) qc_dealloc(&cmpqry);

  return count;
}
