/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brviewinit					spotbrvi.c

Function : Initializes browse screen for specified document.

Author : A. Fregly

Abstract : This function is used to put up the initial document display
	for a document within the spot browse function. Documents to be
	displayed are referenced either by a result file entry, or a
	spot archive reference.

Calling Sequence : int stat = spot_brviewinit(struct spot_s_env *env, 
  struct spot_s_doc *doc, long docid, int relative);

  env		Spot environment structure.
  doc		Document info structure.
  docid		Specifies which document to load. If docid is absolute
		and less than 0, and the view mode is for stream detail,
		the last document in the result file is accessed.
  relative	Flag, non-zero indicates docid is relative to current
		document.
  stat		Returned status, 0 indicates an error.

Notes: 

Change log : 
00	08-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_brviewinit(struct spot_s_env *env, struct spot_s_doc *doc, long docid,
  int relative)
#else
int spot_brviewinit(env, doc, docid, relative)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  long docid;
  int relative;
#endif
{

  int stat;
  unsigned long udocid;

  /* Clear the display window, and the footer window */
  wattrset(spot_dispwin, env->dispnorm);
  win_fill(spot_dispwin, 0, 0, 0, 0, ' ');
  wattrset(spot_footerwin, env->mennorm);
  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
#if SPOT_DEBUG
  dummy_write("brview1.dbg", "blanked display");
#endif

  /* Load the document into the temp file */
  if (!relative && docid < 0) {
    if (env->view != SPOT_VIEWSTREAMDETAIL)
      docid = 0;
    else {
      udocid = (unsigned long) (1L << (FIT_BITS_IN_LONG - 1)) - 1;
      docid = udocid;
    }
  }

#if SPOT_DEBUG
  dummy_write("brview2.dbg", "calling spot_getdoc");
#endif

  if (!(stat = spot_getdoc(env, doc, docid, 1L, relative))) {
    return stat;
  }
#if SPOT_DEBUG
  dummy_write("brview3.dbg", "completed call to spot_getdoc");
#endif

/*************
  if (strcmp(doc->editor, spot_defaulteditor)) {
    sprintf(errmessage, "Foreign document, use File/Open to invoke: %s", 
      doc->editor);
    signal(SIGALRM, spot_timeralarm);
    alarm(5);
    spot_message(errmessage);
    alarm(0);
  }
************/

  /* Set up line and offset descriptors */
  if (!(stat = spot_docparse(doc))) {
    return stat;
  }
#if SPOT_DEBUG
  dummy_write("brview4.dbg", "completed spot_docparse");
#endif

  /* Redraw the display with the start of the new document */
  doc->topline = -1;
  spot_brdraw(env, doc, 0);
#if SPOT_DEBUG
  dummy_write("brview5.dbg", "completed spot_brdraw");
#endif
  return 1;
}
