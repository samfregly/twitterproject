/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fldprompt					spotfldp.c

Function : Prompts user to enter field for use in extracted field summary
	display.

Author : A. Fregly

Abstract : This function allows the user to either enter a field name directly,
	or select if from a list of available fields.

Calling Sequence : int retstat = spot_fldprompt(struct spot_s_env *env,
  struct men_s_menu *parent, char retfield[17]);

Notes: 

Change log : 
000	23-MAR-92  AMF	Created.
001	11-SEP-93  AMF	Make friendly to strict compilers.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#define MAXFLDLEN 16
#define MAXROWS 8

static int firsttime = 1;
static WINDOW *outerwin, *menuwin;
static char lastfield[MAXFLDLEN+1];

#if FIT_ANSI
int spot_fldprompt(struct spot_s_env *env, struct men_s_menu *parent,
  char *retfield)
#else
int spot_fldprompt(env, parent, retfield)
  struct spot_s_env *env;
  struct men_s_menu *parent;
  char *retfield;
#endif
{

  WINDOW *saveouterwin, *promptwin;
  int menutype, nfields, r, c, imode, dmode, retstat, off, key, optnum;
  unsigned len;
  struct men_s_menu *fieldmenu;
  char newfieldfile[FIT_FULLFSPECLEN+1];

  saveouterwin = NULL;
  promptwin = NULL;
  nfields = 0;
  fieldmenu = NULL;
  imode = 0;
  dmode = 1;
  retstat = 0;

  if (firsttime) {
    /* Create windows */
    r = (LINES - (MAXROWS + 4)) / 2;
    c = (COLS - (MAXFLDLEN + 2)) / 2;
    if ((outerwin = newwin(MAXROWS + 4, MAXFLDLEN + 2, r, c)) == NULL)
      return 0;
    if ((menuwin = newwin(MAXROWS, MAXFLDLEN, r+3, c+1)) == NULL) return 0;
    keypad(outerwin, TRUE);
    keypad(menuwin, TRUE);
    scrollok(outerwin, FALSE);
    scrollok(menuwin, FALSE);

    wattrset(outerwin, env->mennorm);
    if (env->uselinedrawing) {
      win_fill(outerwin, 0, 0, 0, 0, ' ');
      win_box(outerwin);
      mvwaddch(outerwin, 2, 0, ACS_LTEE);
      win_fill(outerwin, 2, 1, 1, MAXFLDLEN, ACS_HLINE);
      mvwaddch(outerwin, 2, MAXFLDLEN + 1, ACS_RTEE); 
    }
    else {
      win_fill(outerwin, 0, 0, 0, 0, ' ');
    }
    lastfield[0] = 0;
    firsttime = 0;
  }

  if (!env->uselinedrawing)
    wattrset(outerwin, env->dispnorm);

  win_fill(outerwin, 1, 1, 1, MAXFLDLEN, ' ');
  mvwaddstr(outerwin, 1, 1, lastfield);
  win_fill(outerwin, 3, 1, MAXROWS, MAXFLDLEN, ' ');
  overwrite(outerwin, menuwin);

  win_dup(outerwin, &saveouterwin);
  overwrite(spot_dispwin, saveouterwin);

  menutype = 0;
  if (env->usepointer) menutype += MEN_ATTR_POINTER;

  /* Get field list */
  if (spot_fieldload(env, env->doc.prevfieldfile, 
      env->view != SPOT_VIEWARCHIVES &&  env->view < SPOT_VIEWDETAIL || 
      env->view >= SPOT_VIEWDETAIL && env->prevview != SPOT_VIEWARCHIVES,
      env->view, (env->view == SPOT_VIEWARCHIVES && env->menu->nopts) ? 
      men_opt(env->menu, env->menu->curopt)->data : env->doc.prevdbname,
      &env->doc.fieldnames, &env->doc.fielddata, &env->doc.nfields,
      newfieldfile) && env->doc.nfields) {
    if (env->uselinedrawing)
      fieldmenu = men_init(menuwin, menutype, "", env->doc.fieldnames,
	env->mennorm, env->menhigh, env->mensel, env->menhot,
	ACS_RARROW, ACS_DIAMOND, 1, 0);
    else
      fieldmenu = men_init(menuwin, menutype, "", env->doc.fieldnames,
	env->dispnorm, env->disphigh, env->dispsel, env->disphot,
	ACS_RARROW, ACS_DIAMOND, 1, 0);
    strcpy(env->doc.prevfieldfile, newfieldfile);
  }

  if (fieldmenu != NULL) {
    men_draw(fieldmenu);
    men_put(fieldmenu, spot_dispwin);
  }

  win_dup(spot_promptwin, &promptwin);
  overwrite(spot_promptwin, promptwin);

  touchwin(outerwin);
  overwrite(outerwin, spot_dispwin);

  while (1) {
    len = strlen(lastfield);
    off = len;
    dmode = 1;
    spot_puthelpline(spot_fieldedithelp);
    if (fieldmenu != NULL) overwrite(fieldmenu->optwin, outerwin);

    while (1) {
      win_edit(outerwin, lastfield, MAXFLDLEN, &imode, dmode, 0, 1, 1,
	&off, &key);
      dmode = 0;
      len = 0;
      fit_strim(lastfield, &len);

      switch (key) {
        case '\n' : case '\r' : case KEY_ENTER :
	  if (*lastfield) {
	    strcpy(retfield, lastfield);
	    retstat = 1;
	    goto DONE;
          }
	  else 
	    spot_message("You must enter a field name or use the exit key to \
cancel");
	  break;

        case KEY_CANCEL : case KEY_EXIT : case KEY_SCANCEL :
        case KEY_SEXIT :
#if FIT_LINUX 
	case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
	  goto DONE;

        case KEY_HELP : case KEY_SHELP :
	  overwrite(outerwin, spot_dispwin);
          spot_help();
	  break;

        case KEY_REFRESH : case CTRLW :
          endwin();
          wrefresh(outerwin);
	  break;

        case KEY_UP : case KEY_PREVIOUS : case KEY_PPAGE :
        case KEY_SPREVIOUS : case 'n' - 'a' + 1 :
        case KEY_NEXT : case KEY_SNEXT : case '\t' : case KEY_DOWN :
        case KEY_NPAGE : case 'p' - 'a' + 1 :
	  goto OUT_ENTERLOOP;
	  break;

        default :
          if (key == spot_exitkey || key == spot_quitkey)
            goto DONE;
          else if (key == spot_helpkey)
	    overwrite(outerwin, spot_dispwin);
            spot_help();
      }
    }
OUT_ENTERLOOP:

    if (fieldmenu != NULL) {
      spot_puthelpline(spot_selectorcmdhelp);
      overwrite(outerwin, spot_dispwin);
      while (!spot_getpulldown(fieldmenu, &optnum, &key)) {

        switch (key) {
          case KEY_CANCEL : case KEY_EXIT : case KEY_SCANCEL :
          case KEY_SEXIT :
#if FIT_LINUX
	  case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
	    goto DONE;

          case KEY_UP : case KEY_PREVIOUS : case KEY_PPAGE :
          case KEY_SPREVIOUS : case 'n' - 'a' + 1 :
          case KEY_NEXT : case KEY_SNEXT : case '\t' : case KEY_DOWN :
          case KEY_NPAGE : case 'p' - 'a' + 1 :
	    goto OUT_MENULOOP;
	    break;

          default :
            if (key == spot_exitkey || key == spot_quitkey)
              goto DONE;
        }
      }

      strncpy(lastfield, fieldmenu->options[optnum].val, MAXFLDLEN);
      lastfield[MAXFLDLEN] = 0;
      strcpy(retfield, lastfield);
      retstat = 1;
      goto DONE;

OUT_MENULOOP:
      ;
    }
  }

DONE:
  if (promptwin != NULL) {
    overwrite(promptwin, spot_promptwin);
    delwin(promptwin);
  }

  if (fieldmenu != NULL)
    men_dealloc(&fieldmenu);

  if (saveouterwin != NULL) {
    overwrite(saveouterwin, spot_dispwin);
    delwin(saveouterwin);
  }

  return retstat;
}
