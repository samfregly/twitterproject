/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brdraw					spotbrdr.c

Function : Draws spot browse document display and footer.

Author : A. Fregly

Abstract : This function is used to draw the document display area and
  footer for the spot browse function.

Calling Sequence : spot_brdraw(struct spot_s_env *env, struct spot_s_doc *doc);

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
001	11-SEP-93  AMF  Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#define LINELEN 512

#if FIT_ANSI
void spot_brdraw(struct spot_s_env *env, struct spot_s_doc *doc, int line)
#else
void spot_brdraw(env, doc, line)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  int line;
#endif
{
  int row, len, idx, previdx, sidx, eidx, oidx, bidx, highlen;
  int srow, erow, lastidx, rowdif;
  int nlines, highlight, noffsets;
  unsigned char *docbuf;
  long *lineoffset;
  unsigned long *offsets;
  unsigned *lengths;
  unsigned char outline[LINELEN+1], outbuf[LINELEN+1];
  unsigned int loff, curoff;
  unsigned long soff, eoff, startoff;
  int forced;

  /* Make sure line index is valid */
  if (doc->nlines < 1) {
    return;
  }

  if (line < 0)
    line = 0;
  else if (line >= doc->nlines)
    line = doc->nlines - 1;

  forced = 0;

  /* Make sure line is viewable */
  if (doc->topline < 0) {
    /* Forced redraw */
    win_fill(spot_dispwin, 0, 0, 0, 0, ' ' + env->dispnorm);
    doc->topline = line;
    srow = 0;
    erow = env->menu->nrows - 1;
    forced = 1;
  }

  else if (line < doc->topline) {
    /* line precedes the top displayed line */
    rowdif = doc->topline - line;
#if !FIT_SUN
    if (rowdif >= env->menu->nrows) {
#else
    if (rowdif > 1) {
#endif
      /* Line is more than a screen away, redraw with line at top */
      doc->topline = line;
      srow = 0;
      erow = env->menu->nrows-1;
      forced = 1;
    }
    else {
      /* Line is less than a screen away, scroll to the line */
      doc->topline = line;
      srow = 0;
      erow = rowdif-1;
      for (; rowdif; --rowdif) {
        wmove(spot_dispwin, 0, 0);
	winsertln(spot_dispwin);
	win_fill(spot_dispwin, 0, 0, 1, 0, ' ' + env->dispnorm);
	wrefresh(spot_dispwin);
      }
    }
  }

  else if (line >= doc->topline + env->menu->nrows) {
    /* Line is past the last displayed line */
    lastidx = doc->topline + env->menu->nrows - 1;
    rowdif = line - lastidx;
#if !FIT_SUN
    if (rowdif >= env->menu->nrows) {
#else
    if (rowdif > 1) {
#endif
      /* Line is more than a screen away, redraw with line at bottom */
      doc->topline = line - env->menu->nrows + 1;
      srow = 0;
      erow = env->menu->nrows-1;
      forced = 1;
    }
    else {
      /* Line is less than a screen away, scroll to it */
      doc->topline += rowdif;
      srow = env->menu->nrows - rowdif;
      erow = env->menu->nrows - 1;
      for (; rowdif; --rowdif) {
        wmove(spot_dispwin, 0, 0);
	wdeleteln(spot_dispwin);
        wattrset(spot_dispwin, env->dispnorm);
	win_fill(spot_dispwin, env->menu->nrows-1, 0, 1, 0, ' ');
	wrefresh(spot_dispwin);
      }
    }
  }
  else {
    return;
  }

  lineoffset = doc->lineoffsets;
  offsets = doc->highoffsets;
  lengths = doc->highlengths;
  line = doc->topline + srow;
  startoff = lineoffset[line];
  docbuf = (unsigned char *) spot_docline(doc, line);
  nlines = doc->nlines;
  highlight = doc->nhigh;
  noffsets = doc->nhigh;

  outline[COLS] = 0;
  if (forced) idlok(spot_dispwin, FALSE);
  wattrset(spot_dispwin, env->dispnorm);
 
  if (!highlight || !noffsets) {
    for (row = srow; row <= erow; ++row, ++line) {
      win_fill(spot_dispwin, row, 0, 1, 0, ' ');
      wmove(spot_dispwin, row, 0);
      if (line < nlines) {
	len = min(COLS, lineoffset[line+1] - lineoffset[line] - 1);
	if (len > 0 && docbuf[lineoffset[line]-startoff] != '\f') {
	  fit_sassign((char *) outline, 0, len-1, (char *) docbuf,
	    (int) (lineoffset[line]-startoff), 
	    (int) (lineoffset[line+1] - 2 - startoff));
	  outline[len] = 0;
	  waddstr(spot_dispwin, (char *) outline);
        }
      }
      wrefresh(spot_dispwin);
    }
  }

  else {
    /* Find offset preceding top line */
    sidx = 0;
    eidx = noffsets - 1;
    idx = 0;
    previdx = noffsets;
    loff = *(lineoffset+line);
    while (previdx != idx) {
      previdx = idx;
      idx = (sidx + eidx) >> 1;
      if (*(offsets+idx) > loff)
	eidx = max(sidx, idx-1);
      else if (*(offsets+idx) < loff)
	sidx = min(eidx, idx+1);
    }

    if (idx && *(offsets+idx) > loff)
      --idx;

    /* Determine if in the middle of highlighting a wrapped phrase */
    soff = *(offsets+idx);
    eoff = *(offsets+idx) + lengths[idx] - 1;
    if (soff < loff && eoff >= loff)
      soff = loff;
    else if (eoff < loff && idx < (noffsets-1)) {
      soff = *(offsets + ++idx);
      eoff = *(offsets + idx) + lengths[idx] - 1;
    }

    /* Draw the display, doing any term high lighting which is needed. */
    for (row = srow; row <= erow; ++row, ++line) {

      win_fill(spot_dispwin, row, 0, 1, 0, ' ');
      wmove(spot_dispwin, row, 0);

      if (line < nlines && docbuf[lineoffset[line]-startoff] != '\f') {
	len = max(0, min(LINELEN, lineoffset[line+1] - lineoffset[line]));
	if (len > 0) {
	  fit_sassign((char *) outline, 0, len-1, (char *) docbuf,
	    (int) (lineoffset[line]-startoff), 
	    (int) (lineoffset[line+1] - 1 - startoff));
	  len -= 1;	
	}
	outline[len] = 0;

	if (lineoffset[line+1] <= soff || lineoffset[line] > eoff || len < 1)
	  /* no words to highlight on current line, just display the line */
	  waddstr(spot_dispwin, (char *) outline);

	else {
	  /* Highlight while drawing line */
	  for (oidx = 0, bidx = 0, curoff = lineoffset[line];
	      oidx < len; ++oidx, ++curoff)
	    if (curoff == soff) {
	      /* At the start of a word to highlight */
              if (bidx) {
		/* Draw portion of line which is unhighlighted */
		outbuf[bidx] = 0;
		
		waddstr(spot_dispwin, (char *) outbuf);
		bidx = 0;
              }

	      /* Get portion of line to highlight */
	      highlen = (int) min(len-oidx,eoff-curoff+1);
	      strncpy((char *) outbuf,(char *) (outline+oidx), highlen);
	      outbuf[highlen] = 0;

	      /* Draw the word to highlight in reverse video */
	      wattrset(spot_dispwin, env->disphigh);
	      waddstr(spot_dispwin, (char *) outbuf);
	      wattrset(spot_dispwin, env->dispnorm);

	      /* Update indices to account for text written to screen */
	      oidx += highlen-1;
	      curoff += highlen-1;

	      /* Set up offsets of next word to highlight */
	      if (soff + highlen - 1 < eoff) {
		/* Phrase continues on next line, set soff to point at it */
		soff = lineoffset[line+1];
	      }

	      else if (idx < noffsets-1) {
		soff = *(offsets + ++idx);
		eoff = soff + lengths[idx] - 1;
	      }
	    }
	    else
	      /* Move unhighlighted character to outbuf */
	      outbuf[bidx++] = outline[oidx];


	  /* Draw any leftover characters */
	  if (bidx) {
	    outbuf[bidx] = 0;
	    waddstr(spot_dispwin, (char *) outbuf);
	  }
	}
      }
      wrefresh(spot_dispwin);
    }
  }
  if (forced) idlok(spot_dispwin, TRUE);
}
