/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_scrinit					spotscri.c

Function : Initializes/resets screen handling options.

Author : A. Fregly

Abstract : 

Calling Sequence : spot_scrinit();

Notes: 

Change log : 
000  30-APR-94  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

int spot_scrinit()
{
  int retstat;
#if FIT_SCO || FIT_HP || FIT_SUN
  if ((retstat = noecho()) == ERR) goto ERREXIT;
  if ((retstat = intrflush(stdscr, FALSE)) == ERR) goto ERREXIT;
  if ((retstat = keypad(stdscr, TRUE)) == ERR) goto ERREXIT;
  if ((retstat = raw()) == ERR) goto ERREXIT;
#if !FIT_HP
  retstat = (cur_set(0) != ERR);
#endif
#else
  noecho();
  keypad(stdscr, TRUE);
  raw();
  retstat = !ERR;
#endif
ERREXIT:
  return retstat;
}
