/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_finddoc					spotfind.c

Function : Finds a document for the browser and summary display builder.

Author : A. Fregly

Abstract : This routine finds a document for the spot browse, summary, or print 
  functions.

Calling Sequence : int spot_finddoc(struct spot_s_env *env, 
  struct spot_s_doc *doc, long docid, int relative, int dosearch, 
  int loadfields, int doopen, FILE *(*in), struct drct_s_file *(*arc), 
  long *doclen, long *modtime)

  env		Spot environment structure.
  doc		Spot document descriptor structure.
  docid		Document specified.
  relative	Flag, non-zero then docid is relative to current document.
  dosearch	Flag, allows search for document in direction other than
		relative direction.
  loadfields	Flag, non-zero enables loading of field defs for document.
  doopen	Flag, non-zero opens document and readies it for input.
  in		Returned file handle to use for document retrieval if document
		is in a file.
  arc		Returned archive handle to use for document retrieval if "in"
		is NULL.
  doclen	Returned length of document.
  modtime	Returned modification date for document which is a file.
  stat		Returned status, 0 if a document could not be retrieved.

Notes :

Change log :
000	08-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>
#include <fcntl.h>


#if FIT_ANSI
int spot_finddoc(struct spot_s_env *env, struct spot_s_doc *doc, long docid, 
  int relative, int dosearch, int loadfields, int doopen, FILE *(*in), 
  struct drct_s_file *(*arc), long *doclen, long *modtime)
#else
int spot_finddoc(env, doc, docid, relative, dosearch, loadfields, doopen, in,
  arc, doclen, modtime)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  long docid;
  int relative, loadfields, dosearch, doopen;
  FILE *(*in);
  struct drct_s_file *(*arc);
  long *doclen, *modtime;
#endif
{

  struct stat statbuf;
  char dbname[TIPS_DESCRIPTORMAX+1], *docnum_s;
  char docname[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char descriptor[TIPS_DESCRIPTORMAX+1], newfieldfile[FIT_FULLFSPECLEN+1];
  char docnum_str[14];
  long reclen, docnum, currec, docinc;
  int retstat, nbytes, descriptlen, i, npasses, isfile;
  int descriptorlen, indescriptorlen;
  struct drct_s_file *archandle;

  *in = NULL;
  archandle = NULL;

  retstat = 1;
  *doclen = 0;
  npasses = (dosearch) ? 2 : 1;
  isfile = 0;

  /* Clean up doc structure for new document */
  doc->nlines = 0;
  doc->docname[0] = 0;
  doc->origdocname[0] = 0;
  strcpy(doc->editor, spot_defaulteditor);

  fstat(doc->handle->idx, &statbuf);
  doc->ndocs = statbuf.st_size / sizeof(doc->handle->drct);
  if (!relative)
    currec = docid;
  else
    currec = doc->docnum + docid;

  if (currec < 0) currec = 0;

  if (currec >= doc->ndocs) {
    currec = doc->ndocs - 1;
    if (!dosearch) {
      retstat = 0;
      goto DONE;
    }
  }

  if (currec < 0) {
    currec = 0;
    retstat = 0;
    goto DONE;
  }

  docinc = (!relative || docid >= 0) ? 1L : -1L;
  docid = currec;

  if (doc->filetype != SPOT_VIEWARCHIVES) {
    /* Retrieve the hit for the document */
    for (i=0, descriptlen = 0; i < npasses && !descriptlen;) {
      while (i < npasses) {
        if (descriptlen = drct_rdrec(doc->handle, currec, doc->docname,
          TIPS_DESCRIPTORMAX, &reclen)) break;

	currec += docinc;

        if (currec < 0) {
	  currec = docid + 1;
          docinc = -docinc;
	  i += 1;
	}
	else if (currec >= doc->ndocs) {
	  currec = docid - 1;
          docinc = -docinc;
	  i += 1;
	}
      }
    }

    /* Unlock the file */
    drct_unlock(doc->handle);
    doc->docnum = currec;
    if (!descriptlen) {
      retstat = 0;
      goto DONE;
    }

    if (doc->docname[descriptlen-1] == '\n')
      doc->docname[descriptlen-1] = 0;

    /* Parse the descriptor */
    tips_parsedsc(doc->docname, docname, doclen, keyfld);

    /* Load the document into the  document buffer */
    docnum_s = strchr(docname,'#');

    if (docnum_s != NULL) {
      /* Document is in an archive, load it from the archive */
      strncpy(dbname, docname, (int)  (docnum_s - docname));
      dbname[docnum_s - docname] = 0;
      ++docnum_s;

      if (doc->db == NULL || strcmp(dbname, doc->prevdbname)) {
        /* Different archive, close old one (if open) and open new one */
        if (doc->db != NULL) drct_close(doc->db);
        strcpy(doc->prevdbname, dbname);
        doc->db = drct_open(dbname, O_RDONLY, 0, -1, -1);
      }
      if (doc->db != NULL) {
        /* Attempt to load the document if the archive is open */
        sscanf(docnum_s,"%ld",&docnum);
      }
      archandle = doc->db;
    }

    else {
      /* Document is not in archive, read it from file system */
      if (doc->db != NULL) {
        drct_close(doc->db);
        doc->db = NULL;
      }

      /* Set editor name */
      strcpy(doc->editor, spot_getapplication(docname));

      /* Open document file */
      if (doopen)
        *in = fopen(docname,"r");
      strcpy(doc->origdocname, docname);
      isfile = 1;
    }
  }
  else {
    /* Processing an archive directly */
    archandle = doc->handle;
    docnum = currec;
    strcpy(doc->prevdbname, archandle->dataspec);
  }

  if (archandle == NULL && !isfile || *in == NULL && doopen && isfile) {
    goto DONE;
  }

  /* Determine document size */
  if (archandle != NULL) {
    *doclen = 0;
    docname[TIPS_DESCRIPTORMAX] = 0;
    if (archandle == doc->handle) { 
      for (i = 0, nbytes = 0; i < 2 && !nbytes; ++i) {
        while (1) {
          if (nbytes = drct_rdrec(archandle, docnum, (char *) docname, 
	      TIPS_DESCRIPTORMAX, doclen))
	    break;

	  docnum += docinc;

	  if (docnum < 0) {
	    docnum = docid + 1;
	    docinc = -docinc;
	  }
	  else if (docnum >= doc->ndocs) {
	    docnum = docid - 1;
	    docinc = -docinc;
	  }

	  if (docnum < 0 || docnum >= doc->ndocs) break;
        }
      }
      currec = docnum;
      if (nbytes) {
	tips_extractdescr(docname, descriptor, &descriptorlen);
	indescriptorlen = descriptorlen;
	if (descriptorlen && descriptor[descriptorlen-1] == '\n') {
	  descriptor[descriptorlen-1] = 0;
	  descriptorlen -= 1;
	}
	if (descriptorlen) {
          tips_parsedsc(descriptor, docname, doclen, keyfld);
	  tips_nameparse(docname, dbname, docnum_str, doc->origdocname);
	  sprintf(doc->docname, "%s#%ld#%s,%ld,%s", doc->handle->dataspec, 
		docnum, doc->origdocname, *doclen, keyfld);
	  if (doc->origdocname[0])
	    strcpy(doc->editor, spot_getapplication(doc->origdocname));
	}
	else
	  sprintf(doc->docname, "%s#%ld,%ld", doc->handle->dataspec, 
		docnum, *doclen);
	descriptorlen = indescriptorlen;
      }
    }
    else {
      nbytes = drct_rdrec(archandle, docnum, (char *) docname, 
	TIPS_DESCRIPTORMAX, doclen);
      if (nbytes) {
        tips_extractdescr(docname, descriptor, &descriptorlen);
        if (descriptorlen) {
          tips_parsedsc(descriptor, docname, doclen, keyfld);
	  tips_nameparse(docname, dbname, docnum_str, doc->origdocname);
	  if (doc->origdocname[0])
	    strcpy(doc->editor, spot_getapplication(doc->origdocname));
        }
      }
      else
        descriptorlen = 0;
    }
    drct_cancelio(archandle);
    if (descriptorlen && doopen) {
      drct_rdrec(archandle, docnum, descriptor, descriptorlen, doclen);
      *doclen -= descriptorlen;
    }
  }
  else {
    statbuf.st_size = 0;
    statbuf.st_mtime = 0;
    if (doopen && *in != NULL) {
      if (!fstat(fileno(*in), &statbuf)) {
        *doclen = statbuf.st_size;
        *modtime = statbuf.st_mtime;
      }
    }
    else if (!stat(docname, &statbuf)) {
      *doclen = statbuf.st_size;
      *modtime = statbuf.st_mtime;
    }
  }

  if (loadfields)
    if (spot_fieldload(env, doc->prevfieldfile, isfile, env->view,
	doc->prevdbname, &doc->fieldnames, &doc->fielddata, &doc->nfields,
	newfieldfile))
      strcpy(doc->prevfieldfile, newfieldfile);

DONE:
  doc->docnum = max(0, min(doc->ndocs - 1, currec));
  *arc = archandle;
  return retstat;
}
