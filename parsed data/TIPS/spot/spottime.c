/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_timeralarm					spottime.c

Function : Handles timer alarm signals.

Author : A. Fregly

Abstract : 

Calling Sequence : This function is invoked asynchronously by expiration
	of a timer alarm. This is set up when the function name is supplied
	as the second parameter to a call to the UNIX "signal" function.

Notes: 

Change log : 
000  03-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <fitsydef.h>

#if FIT_ANSI
void spot_timeralarm(int sig)
#else
void spot_timeralarm(sig)
  int sig;
#endif
{
  /* This function does nothing, just keeps the process from being killed
     by the alarm signal */
  ;
}
