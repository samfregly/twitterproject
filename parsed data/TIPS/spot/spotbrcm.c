/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brcmd					spotmain.c

Function : Handles command processing for browse command menu.

Author : A. Fregly

Abstract : This function handles command processing for all options off 
	from spot's browse command menu. Control will remain in this function or
	it's subfunctions until an invoked option is completed, or the
	user exits a pulldown which is being processed by this function.

Calling Sequence : int key = spot_brcmd(struct spot_s_env *env, 
  struct spot_s_doc *doc, int option, int optselected, int *menudown, 
  int *exitflag);

  env		spot environment structure.
  doc		Document access structure.
  option	current command menu option.
  optselected	non-zero then user wants current option executed.
  menudown	keeps track of whether or not menu's are currently
		pulled down.
  exitflag	Returned as non-zero if user entered program exit key or
		selected Exit off the File menu.
  stat		Returned key entered by user if menus down and key is a
		main menu movement key, zero otherwise. 

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
001	03-MAR-92  AMF	Bail out if pulldown for option is NULL.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;

#if FIT_ANSI
int spot_brcmd(struct spot_s_env *env, struct spot_s_doc *doc, int option,
  int optselected, int *menudown, int *exitflag)
#else
int spot_brcmd(env, doc, option, optselected, menudown, exitflag)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  int option, optselected, *menudown;
  int *exitflag;
#endif
{
  struct men_s_menu *menu;
  int key, menuoptsel, pid, exitstat, exitpid, ss;
  int retopt, copyopt, append, delopt;
  int copyoptsel, updated, addoptsel, deloptsel;
  int descriptorlen;
  int nbytes, topline;
  long doclen, reclen, docnum;
  char filespec[FIT_FULLFSPECLEN+1], *filespecptr;
  struct stat statbuf;
  struct spot_s_env tempenv;
  char promptstr[FIT_FULLFSPECLEN+64];
  char destspec[FIT_FULLFSPECLEN+1], docdescriptor[TIPS_DESCRIPTORMAX+1];
  char mailfilename[FIT_FULLFSPECLEN+1], distlist[257], subject[257];
  char dbname[FIT_FULLFSPECLEN+1], docnum_s[FIT_FULLFSPECLEN+1]; 
  char docname[FIT_FULLFSPECLEN+1], docnamefld[TIPS_DESCRIPTORMAX+1];
  char keyfld[TIPS_DESCRIPTORMAX+1]; char defext[5], *defdir, *(*filelist);
  struct spot_s_doc tempdoc;
  struct drct_s_file *drctout;
  struct drct_s_info drctinfo;
  FILE *mailfile;

  *exitflag = 0;

  while (1) {
    menu = NULL;
    switch (option) {
      case 0 :
        menu = spot_brfilemenu;
        break;
      case 1 :
	*menudown = 0;
        break;
      case 2 :
	if (optselected) {
	  tmpnam(filespec);
	  fit_setext(filespec, ".qry", filespec);
	  filespecptr = filespec;
	  tempenv = *env;
	  tempenv.prevsel = NULL;
	  spot_search(&tempenv, doc, 0, 0, 0, &filespecptr, 1);
	  spot_brviewinit(env, doc, 0L, 1);
	  unlink(filespec);
	  fit_setext(filespec, ".hit", filespec);
	  drct_delfile(filespec);
          wrefresh(spot_dispwin);
          win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
          spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, 
		doc->nlines);
	  *menudown = 0;
	}
        break;
      case 3 :
        menu = spot_actions[SPOT_MAXVIEW].menu;
        break;
      default :
        ;
    }
    if (menu != NULL || optselected) break;

    key = wgetch(spot_cmdmenu->win);
    *exitflag = (key == spot_quitkey);
    if (*exitflag || key == KEY_UP || key == KEY_DOWN || key == spot_exitkey ||
	key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
	key == ('d' - 'a' + 1)) {
      *menudown = 0;
      return key;
    }
    else if (key == spot_helpkey)
      spot_help();
    else {
      men_dokey(spot_cmdmenu, key, &optselected);
      option = spot_cmdmenu->curopt;
    }
  }

  if (menu != NULL) {
    men_reset(menu);
    menuoptsel = spot_getpulldown(menu, &retopt, &key);
    *menudown = (key == KEY_LEFT || key == KEY_RIGHT || key == KEY_SLEFT ||
	key == KEY_SRIGHT);
    if (!menuoptsel || *menudown) {
      men_remove(menu, spot_dispwin);
      return key;
    }
  }
  else
    goto DONE;

  switch (option) {
    case 0 :
      /* File menu option was selected */
      switch (menu->curopt) {
	case 0 :
	  /* Allow user to edit document using editor defined for
	     document type */
	  men_remove(menu, spot_dispwin);
	  menu = NULL;
	  spot_edit(doc->docname, doc->editor, &updated); 
	  if (updated) {
	    spot_brviewinit(env, doc, 0L, 1);
            wrefresh(spot_dispwin);
            win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
            spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, 
		doc->nlines);
	  }
	  break;
	case 1 :
	  /* Pick */
	  break;
	case 2 :
	  /* Copy */
	  /* Place destination menu */
          men_overlay(spot_brcopymenu, menu, menu->curopt, 1);
          men_reset(spot_brcopymenu);

	  /* Place cursor according to current view mode */
	  if (env->view == SPOT_VIEWARCHIVES)
	    spot_brcopymenu->curopt = 2;
	  else if (env->view == SPOT_VIEWSTREAMDETAIL || 
		   env->view == SPOT_VIEWRESULTS)
	    spot_brcopymenu->curopt = 1;
	  else
	    spot_brcopymenu->curopt = 0;
	  men_draw(spot_brcopymenu);

	  /* Allow user to select from the copy destination menu */
	  copyoptsel = spot_getpulldown(spot_brcopymenu, &copyopt, &key);
	  if (!copyoptsel) {
	    men_remove(spot_brcopymenu, spot_dispwin);
	    break;
	  }

	  /* Put up menu so that selection of append or overwrite may be
	     chosen */
	  if (copyopt == 0) {
            men_overlay(spot_addmenu, spot_brcopymenu, spot_brcopymenu->curopt, 
	      1);
            men_reset(spot_addmenu);
	    spot_addmenu->curopt = 1;
	    men_draw(spot_addmenu);
	    addoptsel = spot_getpulldown(spot_addmenu, &append, &key);
	  }
	  else {
	    append = 1;
	    addoptsel = 1;
	  }

	  /* Set file extension and default directory for destination */
	  if (copyopt == 0) {
	    defext[0] = 0;
	    defdir = spot_filedest;
	  }
	  else if (copyopt == 1) {
	    strcpy(defext, ".hit");
	    defdir = spot_searchdest;
	  }
	  else {
	    strcpy(defext, ".sar");
	    defdir = spot_arcdest;
	  }

	  /* Allow user to select destination using a file selector */
	  destspec[0] = 0;
	  spot_fselect(env, 0, (int) !append, defdir, defext, &filelist, 
		defdir);
	  if (filelist != NULL) {
	    fit_expfspec(filelist[0], defext, defdir, destspec); 
	    fit_delfilelist(&filelist);
	  }

	  /* Remove the menus used during the prompting process */
	  if (copyopt == 0) men_remove(spot_addmenu, spot_dispwin);
	  men_remove(spot_brcopymenu, spot_dispwin);

	  /* Don't continue if user canceled out of the prompting process */
	  if (!addoptsel || !destspec[0]) break;

	  if (!append && !stat(destspec, &statbuf)) {
	    /* Give a warning if output file will be replaced by the copy */
	    sprintf(promptstr, "%s will be overwritten, continue?", destspec);
	    if (!spot_ynprompt(env, promptstr)) break;

	    /* Delete the old version */
	    ss = !unlink(destspec);

	    if (!ss) {
	      sprintf(promptstr,"Unable to delete: %s", destspec);
	      spot_errmessage(promptstr);
	      break;
	    }
	  }

	  /* Set up a document descriptor structure so that spot_getdoc may
	     be used to retrieve the document */
	  tempdoc = *doc;		/* Copy the one use for the display */
	  /* Clear out stuff we don't want clobbered in the original */
          tempdoc.docbuf = (unsigned char *) calloc(SPOT_DOCBUFMAX+1, 1);
	  tempdoc.buflen = SPOT_DOCBUFMAX;
	  tempdoc.db = NULL;
	  tempdoc.temp = NULL;
	  drctout = NULL;

	  /* Open destination file */
	  if (copyopt == 0) {
	    /* A little trick when doing a copy to a file, make the temp
	       file for the document descriptor structure the destination
	       file for the copy, opened for append access so that we don't
	       clobber any previous contents if doing an append. */
	    ss = ((tempdoc.temp = fopen(destspec, "a+")) != NULL);
	  }
	  else {
	    /* Copying to a result file or an archive, try to open the direct
	       access file for read/write access, and if that fails, try
	       if with create enabled. */
	    
	    if (!(ss = 
		((drctout = drct_open(destspec, O_RDWR, 0, -1, -1)) != NULL))) {
	      ss = ((drctout = 
		  drct_open(destspec, O_RDWR + O_CREAT, 0, -1, -1)) != NULL);
	    }
	    if (ss && copyopt == 2) {
	      if (!(ss = ((tempdoc.temp = tmpfile()) != NULL))) {
		spot_errmessage("Unable to create copy work file");
		drct_close(drctout);
		drctout = NULL;
		break;
	      }
	    }
	  }
	  if (!ss) {
	    sprintf(promptstr, "Unable to open: %s", destspec);
	    spot_errmessage(promptstr);
	    break;
	  }

	  if (copyopt == 1) {
	    /* Copying a document reference to a result file */
	    strncpy(docdescriptor, doc->docname, TIPS_DESCRIPTORMAX-1);
	    docdescriptor[TIPS_DESCRIPTORMAX-1] = 0;
	    descriptorlen = strlen(docdescriptor);
	    docdescriptor[descriptorlen] = '\n'; 
	    docdescriptor[++descriptorlen] = 0;
	    ss = drct_append(drctout, docdescriptor, descriptorlen,
		(long) descriptorlen);
	  }
	  else {
	    /* Copying to a file or an archive, retrieve the document into
	       the temp file, which as noted above, is the final destination
	       for a copy to a file */
	    ss = spot_getdoc(env, &tempdoc, 0L, 0, 1);
	    if (ss && copyopt == 2) {
	      /* Copying to an archive, determine record number it will be
		 place in */
	      if (drct_rlock(drctout, 0L, 0L) && drct_info(drctout, &drctinfo)){
		/* Create a document descriptor for the destination document */
		sprintf(docdescriptor, "%ld#%s,%ld\n", drctinfo.nrecs,
			doc->origdocname, tempdoc.doclen);
		/* Determine length of destination document. */
		doclen = tempdoc.doclen + strlen(docdescriptor);
		/* Put the descriptor as the first part of the new document */
		ss = drct_append(drctout, docdescriptor,
			(int) (doclen - tempdoc.doclen), doclen);
	        if (ss) {
		  /* Copy the contents of the temp file into the remainder
		     of the document */
	          rewind(tempdoc.temp);
		  reclen = doclen;
	          for (nbytes=1, doclen = tempdoc.doclen; nbytes && doclen;
		      doclen -= nbytes) {
		    nbytes = 
		      (tempdoc.buflen > doclen) ? doclen : tempdoc.buflen;
		    if (nbytes = fread(tempdoc.docbuf, 1, nbytes, 
			tempdoc.temp)) {
		      if (!drct_append(drctout, (char *) tempdoc.docbuf, nbytes,
			  reclen))
			nbytes = 0;
		    }
		    else
		      nbytes = 0;
		  }
		  ss = (doclen == 0);
		}
	      }
	    }
	  }

	  if (!ss) {
	    sprintf(promptstr, "Error in copy to: %s", destspec);
	    spot_errmessage(promptstr);
	  }
	  if (drctout != NULL) drct_close(drctout);
	  if (tempdoc.temp != NULL) fclose(tempdoc.temp); 
	  if (tempdoc.docbuf != NULL) free(tempdoc.docbuf);
	  if (tempdoc.db != NULL) drct_close(tempdoc.db);
	  break;

        case 3 :
	  /* Mail */
	  /* Prompt for user name list */
	  distlist[0] = 0;
	  subject[0] = 0;
	  spot_prompt("To>", distlist, sizeof(distlist)-1);
	  if (!distlist[0]) break;
	  if (!spot_prompt("Subject>", subject, sizeof(subject)-1)) break;
	  if (!subject[0]) strncpy(subject, doc->docname, sizeof(subject)-1);
	  if (!subject[0]) strcpy(subject, "Mail from spot user");
	  subject[sizeof(subject)-1] = 0;
	  mailfilename[0] = 0;
	  tmpnam(mailfilename);
	  if ((mailfile = fopen(mailfilename, "w")) == NULL) {
	    spot_errmessage("Unable to open work file");
	    break;
	  }

	  rewind(doc->temp);
	  for (doclen = doc->doclen, nbytes = 1; nbytes && doclen;
	      doclen -= nbytes) {
	    nbytes = (doc->buflen > doclen) ? doclen : doc->buflen;
	    if (nbytes = fread(doc->docbuf, 1, nbytes, doc->temp)) {
	      if (fwrite(doc->docbuf, 1, nbytes, mailfile) != nbytes) {
		spot_errmessage("Error writing to work file");
		nbytes = 0;
	      }
	    }
	    else {
	      spot_errmessage("Error reading document work file");
	      nbytes = 0;
	    }
	  }
	  fclose(mailfile);
	  if (!doclen) {
	    endwin();

	    if (!(pid = fork())) {
              /* Execute the script */
              execlp(spot_mailer, spot_mailer, mailfilename, subject, 
		  distlist, (char *) 0);
	      exit(errno);
	    }
	    else if (pid == -1)
              /* Error from fork */
              spot_errmessage("Error forking search process");
            else
              while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
                ;
#if FIT_COHERENT
	      spot_scrinit();
#endif
	  }
	  unlink(mailfilename);
	  topline = doc->topline;
	  spot_brviewinit(env, doc, 0L, 1);
	  wrefresh(spot_dispwin);
          spot_brdraw(env, doc, topline);
          win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
          spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, doc->topline, 
		doc->nlines);
	  wrefresh(spot_footerwin);
	  break;

	case 4 :
	  /* Delete */
	  /* Allow user to choose what to delete in addition to the document
	     reference, archive document and/or the "original" */
          men_overlay(spot_brdeloptmenu, menu, menu->curopt, 1);
          men_reset(spot_brdeloptmenu);
	  spot_puthelpline(spot_multmenuhelp);
	  deloptsel = spot_getpulldown(spot_brdeloptmenu, &delopt, &key);
	  men_remove(spot_brdeloptmenu, spot_dispwin);
	  if (!deloptsel) break;

	  if (men_nselected(spot_brdeloptmenu) && 
	      !spot_ynprompt(env, "Document(s) will be deleted, continue")) 
	    break;

	  if (spot_brdeloptmenu->options[0].descr.stat) {
	    /* Attempt to delete document original */
	    tips_nameparse(doc->origdocname, dbname, docnum_s, docname);
	    if (docname[0]) {
	      if (unlink(docname)) {
		sprintf(promptstr, "Unable to delete document original: %s", 
			docname);
		spot_errmessage(promptstr);
	      }
	    }
	    else
	      spot_message("Unable to determine original document name");
	  }

	  if (spot_brdeloptmenu->options[1].descr.stat) {
	    tips_parsedsc(doc->docname, docnamefld, &doclen, keyfld);
	    tips_nameparse(docnamefld, dbname, docnum_s, docname);
	    if (dbname[0] && docnum_s[0]) {
	      if ((drctout = drct_open(dbname, O_RDWR, 0, -1, -1)) != NULL) {
		sscanf(docnum_s, "%ld", &docnum);
		if (drct_setpos(drctout, docnum)) {
		  if (!drct_delrec(drctout, 1)) {
		    sprintf(promptstr,
			"Error deleting archived document: %s #%ld",
			dbname, docnum+1);
		    spot_errmessage(promptstr);
		  }
		}
		else {
		  sscanf(docnum_s, "%ld", &docnum);
		  sprintf(promptstr,
		    "Unable to access archived document: %s #%ld",
		    dbname, docnum+1);
		  spot_errmessage(promptstr);
		}
		drct_close(drctout);
	      }
	      else {
		sprintf(promptstr,"Unable to open archive: %s", dbname);
		spot_errmessage(promptstr);
	      }
	    }
	    else
	      spot_message(
		"Unable to determine archive reference for document");
	  }

	  if (env->view != SPOT_VIEWARCHIVES)
	    if (drct_setpos(doc->handle, doc->handle->currec))
	      if (!drct_delrec(doc->handle, 1))
		spot_errmessage("Error removing result file entry");

	  spot_brviewinit(env, doc, 0L, 1);
	  wrefresh(spot_dispwin);
          win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
          spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 
	    doc->topline, doc->nlines);
	  wrefresh(spot_footerwin);
	  menu = NULL;
	  break;

	case 5 :
	  /* Print */
	  tips_parsedsc(doc->docname, docnamefld, &doclen, keyfld);
	  tips_nameparse(docnamefld, dbname, docnum_s, docname);
	  if (dbname[0]) {
	    sscanf(docnum_s, "%ld", &docnum);
	    sprintf(docname, "%s #%ld", dbname, docnum+1);
	  }
	  signal(SIGALRM, spot_timeralarm);
	  if (spot_print(env, doc->docname)) {
	    sprintf(promptstr, "%s sent to printer: %s", docname,
		env->printer); 
	    spot_message(promptstr);
	  }
	  else {
	    sprintf(promptstr, "Error sending %s to printer: %s", docname,
		env->printer);
	    spot_message(promptstr);
	  }
	  break;

	case 6 :
	  /* Shell */
          spot_message( "Use shell exit command to return to SPOT, usually either \"exit\" or \"login\"");
          endwin();
          if (!(pid = fork())) {
            execlp(spot_shell, spot_shell, (char *) 0);
            exit(errno);
          }
          else if (pid > 0)
            while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
              ;
          else
            exitstat = errno;
#if FIT_COHERENT
	  spot_scrinit();
#endif
          wrefresh(spot_dispwin);
          break;

        case 7 :
	  *exitflag = 1;
	  break;

	default :
	  ;
	  break;
        }
      break;

    case 1 :
      *menudown = 0;
      break;

    case 3 :
      /* Action */
      spot_braoption(env, doc, menu, menu->curopt);
      break;

    default :
      ;
  }

DONE:
  if (menu != NULL) men_remove(menu, spot_dispwin);
  return 0;
} 
