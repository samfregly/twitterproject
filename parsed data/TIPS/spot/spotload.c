/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_loadlines					spotload.c

Function : Loads buffer with lines of document starting at specified line.

Author : A. Fregly

Abstract : 

Calling Sequence : void spot_loadlines(struct spot_s_doc *doc, long doclen, 
  char *buf, int buflen, long startline, FILE *in, struct drct_s_file *arc,
  int *retlen);

  doc		Spot document descriptor structure.
  doclen	Amount of document left to be read.
  buf		Buffer in which to put loaded data.
  buflen	Length of buffer which may be used for storing data.
  startline	Starting line number to be loaded. Line numbers start at
		line 1.
  in		If not NULL, Input file handle for file containing data to be
		loaded.
  arc		If "in" is NULL, archive file handle for document containing 
		data to be loaded.

Notes: 

Change log : 
000	17-MAR-92  AMF	Created.
001	11-SEP-93  AMF	Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>
#include <fcntl.h>

#if FIT_ANSI
void spot_loadlines(struct spot_s_doc *doc, long doclen, char *buf, int buflen,
  long startline, FILE *in, struct drct_s_file *arc, int *retlen)
#else
void spot_loadlines(doc, doclen, buf, buflen, startline, in, arc, retlen)
  struct spot_s_doc *doc;
  long doclen;
  char *buf;
  int buflen;
  long startline;
  FILE *in;
  struct drct_s_file *arc;
  int *retlen;
#endif
{

  char *outline;
  long reclen;
  int bufoff, nbytes, outlen, more;
  long curline;

  *retlen = 0;
  buf[0] = 0;
  buf[buflen] = 0;
  if ((outline = (char *) calloc(doc->wrapcol + 2, 1)) == NULL) return;

  curline = doc->nlines + 1;

  if (!doc->buflen || doc->docbuf == NULL) goto DONE;

  /* Read document until it is all gone or we have found our line */ 
  for (nbytes = 0, bufoff = 0, more = 1;;) {
    if (bufoff < nbytes) {
      /* Move unprocessed text from last pass to front of buffer */
      memmove(doc->docbuf, doc->docbuf + bufoff, nbytes - bufoff);
      bufoff = nbytes - bufoff;
    }
    else
      bufoff = 0;

    if (more) {
      /* More text to read from document, load it into document buffer */
      if (in != NULL) 
        nbytes = (unsigned int) fread(doc->docbuf, 1, doc->buflen - bufoff, 
 	  in);
      else if (arc != NULL)
        nbytes = drct_rdrec(arc, arc->currec, (char *) doc->docbuf + bufoff, 
	  doc->buflen - bufoff, &reclen);
      doclen -= nbytes;
    }
    else
      nbytes = 0;

    /* Determine amount of text to be processed */
    nbytes += bufoff;
    if (!nbytes) break; /* No more text, quit */

    more = (doclen > 0);
    if (!more) {
      /* On last buffer, insert trailing linefeed if needed */ 
      if (doc->docbuf[nbytes-1] != '\n')
        doc->docbuf[nbytes++] = '\n';
    }

    for (bufoff = 0; bufoff < nbytes; curline += (outlen != 0)) {
      /* Parse input one line at a time, adding to output */
      if (!spot_getline((char *) doc->docbuf, &bufoff, nbytes, doc->margin, 
        doc->wrapcol, spot_defaultdelim, outline, &outlen)) break;
      if (curline >= startline && outlen) {
	strncpy(buf + *retlen, outline, min(buflen - *retlen, outlen));
	*retlen = min(buflen, *retlen + outlen);
	if (*retlen >= buflen) goto DONE;
      }
    }
  }

DONE:
  if (in == NULL && arc != NULL) drct_cancelio(arc);

  /* Free up the temporary buffer */
  if (outline != NULL) free(outline);
}
