/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_docinit					spotdoci.c

Function : Initializes document access structure.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_docinit(struct spot_s_env *env,
	struct spot_s_doc *doc, int useselected);

  env		SPOT environment structure. The documents to be accessed are
		drawn from the data fields of the env->menu.
  doc		Document structure to be initialized.
  useselected	0, then current document in env->menu is used, otherwise
		selected options are used.

Notes: 

Change log : 
000	03-19-92   AMF	Created.
001	11-SEP-93  AMF	Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_docinit(struct spot_s_env *env, struct spot_s_doc *doc,
  int useselected)
#else
int spot_docinit(env, doc, useselected)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  int useselected;
#endif
{

  struct men_s_option *opt;
  char resultname[FIT_FULLFSPECLEN+1];
  long i;
  int retstat;

#if SPOT_DEBUG
  dummy_write("docinit1.dbg", "entered");
#endif

  retstat = 0;

  if (doc->handle != NULL)
    drct_close(doc->handle);
  if (doc->db != NULL && doc->db != doc->handle)
    drct_close(doc->db);
  if (doc->temp != NULL)
    fclose(doc->temp);
  if (doc->qryexe != NULL)
    prf_dealloc(&doc->qryexe);
  if (doc->lineoffsets != NULL)
    free(doc->lineoffsets);
  if (doc->highoffsets != NULL)
    free(doc->highoffsets);
  if (doc->highlengths != NULL)
    free(doc->highlengths);
#if SPOT_DEBUG
  dummy_write("docinit2.dbg", "finished cleanup of doc struct");
#endif

  doc->handle = NULL;
  doc->db = NULL;
  doc->margin = 0;
  doc->wrapcol = COLS;
  doc->ndocs = 0;
  doc->docnum = 0;
  doc->doclen = 0;
  doc->buflen = SPOT_DOCBUFMAX;
  doc->nlines = 0;
  doc->nhigh = 0;
  doc->startline = 0;
  doc->topline = 0;
  doc->docname[0] = 0;
  doc->prevdbname[0] = 0;
  doc->keyfld[0] = 0;
  if (!doc->docbuf)
    doc->docbuf = (unsigned char *) calloc(SPOT_DOCBUFMAX+1, 1);
  doc->temp = NULL;
  doc->qryexe = NULL;
  doc->lineoffsets = NULL;
  doc->highoffsets = NULL;
  doc->highlengths = NULL;
  doc->prevfieldfile[0] = 0;
#if SPOT_DEBUG
  dummy_write("docinit3.dbg", "finished doc structure init");
#endif

  if (doc->nfields)
    /* Use the the reload function to deallocate the dynamic data */
    tips_loadflds("", &doc->fieldnames, &doc->fielddata, &doc->nfields);
  else {
    if (doc->fieldnames != NULL)
      free(doc->fieldnames);
    if (doc->fielddata != NULL)
      free(doc->fielddata);
  }
  doc->fieldnames = NULL;
  doc->fielddata = NULL;
  doc->nfields = 0;
#if SPOT_DEBUG
  dummy_write("docinit4.dbg", "finished fields dealloc");
#endif

  if (env->view == SPOT_VIEWFILES ||  env->view == SPOT_VIEWQUERIES ||
      env->view >= SPOT_VIEWDETAIL) {
    /* Create temporary hit file for file/query view modes */
    if ((doc->handle = drct_tmpopen()) == NULL) {
      spot_errmessage("Unable to create work file");
      goto DONE;
    }

    if (env->menu != NULL) {
      for (i=0; i < env->menu->nopts; ++i) {
        opt = men_opt(env->menu, i);
        if (!useselected || opt->descr.stat)
	  spot_hitappend(env->menu, i, env->view, doc->handle); 
      }
    }
    drct_rewind(doc->handle);
    if (env->view < SPOT_VIEWDETAIL)
      strcpy(doc->source, spot_curdefault(env, env->view));
    else if (doc->source != env->doc.source)
      strcpy(doc->source, env->doc.source);
#if SPOT_DEBUG
  dummy_write("docinit5", "finished creation of temp hit file");
#endif
  }

  else if (env->menu != NULL) {
    opt = men_opt(env->menu, env->menu->curopt);
    if (opt != NULL && opt->descr.datalen) {
      strcpy(resultname, spot_fspec(opt, env->view));
      if (env->view != SPOT_VIEWARCHIVES)
	fit_setext(resultname, ".hit", resultname);
      if ((doc->handle = drct_open(resultname, O_RDWR, 0, -1, -1)) == NULL)
        doc->handle = drct_open(resultname, O_RDONLY, 0, -1, -1);
      if (doc->handle == NULL) {
        spot_errmessage("Unable to open file");
        goto DONE;
      }
      if (env->view == SPOT_VIEWARCHIVES)
	strcpy(doc->prevdbname, resultname);
      strcpy(doc->source, resultname);
    }
#if SPOT_DEBUG
  dummy_write("docinit6.dbg", "opened hit file");
#endif
  }

  doc->filetype = (env->view != SPOT_VIEWARCHIVES) ? SPOT_VIEWRESULTS :
	SPOT_VIEWARCHIVES;

  retstat = 1;

DONE:
#if SPOT_DEBUG
  dummy_write("docinit7.dbg", "completed successfully");
#endif
  return retstat;
}
