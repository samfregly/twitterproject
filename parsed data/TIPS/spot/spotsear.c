/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_search					spotsear.c

Function : Handles "search" option off spot main menu.

Author : A. Fregly

Abstract : 

Calling Sequence : int retstat = spot_search(struct spot_s_env *env, 
    struct spot_s_doc *doc, int option, int prompt, int append, 
    char *(*searchspec), int nsearchspec);

  env		Spot environment structure.
  option	Option selected off search menu, 0 - new query, 1 - old queries.
  prompt	Determines if prompting should occur.
  append	If prompt is 0, the append is used to determine if results
		from the search should be appended to result files, or the
		result files should be replaced.
  searchspec	if prompt is 0, list of queries to use for the search.
  nsearchspec	Number of items in searchspec list.
  retstat	Returned status, non-zero if a view change is recommended.

Notes: 

Change log : 
000	17-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <curses.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

static char CONTMSG[] = "Hit <Enter> to continue...\n";
static char defaultqry[FIT_FULLFSPECLEN+1] = "default";


#if FIT_ANSI
int spot_search(struct spot_s_env *env, struct spot_s_doc *doc, int option, 
    int prompt, int appendw, char *(*searchspec), int nsearchspec)
#else
int spot_search(env, doc, option, prompt, appendw, searchspec, nsearchspec)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  int option, prompt, appendw;
  char *(*searchspec);
  int nsearchspec;
#endif
{

  int srcoptsel, pid, key, len;
  long i, count;
  int nerrs, retstat;
  int nbytes, timedout, replace, addoptsel, updated, nhistorical, ftype;
  int searchall;
  long reclen, searchselected, append;
  int searchselectedw;
  char qryspec[FIT_FULLFSPECLEN+1];
  char qryname[FIT_FULLFSPECLEN+1], qryext[FIT_FULLFSPECLEN+1];
  char defaultpath[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  char hitspec[FIT_FULLFSPECLEN+1], filespec[FIT_FULLFSPECLEN+1];
  char tempname1[FIT_FULLFSPECLEN+1], tempname2[FIT_FULLFSPECLEN+1];
  char libname[FIT_FULLFSPECLEN+1], temphitfilename[FIT_FULLFSPECLEN+1];
  char errmessage[FIT_FULLFSPECLEN*2], hitdescriptor[TIPS_DESCRIPTORMAX+1];
  char retpath[FIT_FULLFSPECLEN+1], fieldfile[FIT_FULLFSPECLEN+1];
  char *defaultfile_path, *defaultfile_ext;
  struct men_s_menu *tempmenu;

  char *(*filelist);
  FILE *script;
  struct drct_s_file *hitfile, *hitin;
  struct spot_s_env tempenv;
  struct men_s_option *opt;

  filelist = NULL;
  hitfile = NULL;
  hitin = NULL;
  script = NULL;
  nhistorical = 0;
  retstat = 0;
  append = appendw;

  /* Quit if no entries for current view mode */ 
  if (!env->menu->nopts) {
    spot_message("No files in current view mode");
    return 0; 
  }

  if (!prompt) {
    searchselected = 0;
    /* The criteria for searching all documents is based on the fact that
       if prompting for what to search is not done, the search is being
       originated in one of the following situations:

	1. From a summary display from the main screen with no items selected.
	   by default, all items should be searched.
	2. A "refined" search while in browse. Again all items should be
	   searched. The check for whether or not the search is being submitted
	   from browse is to compare the address of the environment structure
	   with the global one (spot_env). Browse uses a temp one, and the
	   main screen always uses spot_env.
    */
    searchall = (!prompt && env->view >= SPOT_VIEWDETAIL) ||
      (env != &spot_env && (env->view == SPOT_VIEWFILES ||
       env->view == SPOT_VIEWQUERIES));
    strncpy(defaultpath, env->searchdir, FIT_FULLFSPECLEN);
  }
  else {
    /* See if any files are currently selected */
    for (count=0, i=0; i < env->menu->nopts; ++i)
      count += (men_opt(env->menu, i)->descr.stat != 0);

    if (count) {
      /* Use current/selected menu if there are selected files */

      if (env->view < SPOT_VIEWDETAIL) {
        tempmenu = spot_srcmenu;
        tempmenu->curopt = 1;
      }
      else {
        tempmenu = spot_sumsrcmenu;
        tempmenu->curopt = 0;
      }

      /* Move the menu to appropriate location */
      men_overlay(tempmenu, spot_searchmenu, spot_searchmenu->curopt, 1);

      /* Reset the menu and draw it */
      men_draw(tempmenu);
      men_put(tempmenu, spot_dispwin);

      /* Allow user to select from menu */
      srcoptsel = spot_getpulldown(tempmenu, &searchselectedw, &key);
      searchselected = (env->view < SPOT_VIEWDETAIL) ? searchselectedw :
	!searchselectedw;
      searchall = (!searchselected && env->view > SPOT_VIEWDETAIL);

      if (option && srcoptsel && env->view != SPOT_VIEWSTREAMS) {
        men_overlay(spot_addmenu, tempmenu, tempmenu->curopt, 1);
        men_put(spot_addmenu, spot_dispwin);
        for (key = spot_helpkey; key == spot_helpkey;) {
          key = men_select(spot_addmenu, 0, &append, &addoptsel, &timedout);
          if (key == spot_helpkey) spot_help();
        }
        men_remove(spot_addmenu, spot_dispwin);
      }
      else {
        addoptsel = 1;
        append = 1;
      }
      /* Remove the menu and restore the display */
      men_remove(tempmenu, spot_dispwin);
      men_put(spot_searchmenu, spot_dispwin);

      /* User canceled out of menu, return */
      if (!srcoptsel || !addoptsel) return 0;
    }
    else {
      /* No selected files, use current (or all if in summary) */
      if (option && env->view != SPOT_VIEWSTREAMS) {
        men_overlay(spot_addmenu, spot_searchmenu, spot_searchmenu->curopt, 1);
        men_put(spot_addmenu, spot_dispwin);
        for (key = spot_helpkey; key == spot_helpkey;) {
          key = men_select(spot_addmenu, 0, &append, &addoptsel, &timedout);
          if (key == spot_helpkey) spot_help();
        }
        men_remove(spot_addmenu, spot_dispwin);
        men_put(spot_searchmenu, spot_dispwin); 
        if (!addoptsel) return 0;
      }
      else {
        append = 1;
      }

      /* Remove the menu and restore the display */
      searchselected = 0;
      searchall = (env->view >= SPOT_VIEWDETAIL);
    }
  }

  replace = !append;
    
  if (!option) {
    nerrs = 0;
    /* User selected new option, prompt for file name */
    if (prompt) {
      if (!spot_fprompt(env, 0, "Enter search name", 
	  NULL, ".qry", env->searchdir, qryspec, retpath, &ftype)) {
	return 0;
      }
    }
    else {
      fit_expfspec(searchspec[0], ".qry", env->searchdir, qryspec);
    }

    /* Parse the query spec */
    fit_prsfspec(qryspec, dummyspec, dummyspec, defaultpath, qryname, qryext, 
	dummyspec);

    spot_edit(qryspec, spot_getapplication(qryspec), &updated);
    if (!updated) {
      spot_message("Edit canceled");
      goto TEXTQUIT;
    }

    if ((filelist = (char *(*)) calloc(2, sizeof(*filelist))) ==
        NULL)
      spot_errmessage("Error allocating internal data structure");
    else {
      if ((filelist[0] =
          (char *) calloc(strlen(qryspec) + 1, 1)) == NULL) {
        spot_errmessage(
          "Error allocating internal data structure");
	fit_delfilelist(&filelist);
      }
      else {
        strcpy(filelist[0], qryspec);
        filelist[1] = NULL;
      }
    }
    goto TEXTDONE;

TEXTQUIT:
    nerrs = 1;

TEXTDONE:
    if (nerrs) return 0;
  }

  else {
    if (prompt) {
      /* Queries option selected off text/query menu, use file selector */
      spot_fselect(env, 1, 0, env->searchdir, ".qry", &filelist, defaultpath);
    }
    else {
      if ((filelist = (char *(*)) calloc(nsearchspec+2, sizeof(*filelist))) != 
          NULL) {
        for (i=0; i < nsearchspec; ++i) {
          if ((filelist[i] = 
	      (char *) calloc(strlen(searchspec[i]) + 1, 1)) == NULL) {
	    fit_delfilelist(&filelist);
	    break;
	  }
        }
        if (filelist != NULL && (filelist[i] = 
	    (char *) calloc(1,sizeof(*filelist[i]))) == NULL)
	  fit_delfilelist(&filelist);
	else {
	  filelist[i++][0] = 0;
	  filelist[i] = NULL;
	}
      }

      if (filelist == NULL)
        spot_errmessage("Error allocating internal data structure");
    }
  }

  script = NULL;
  hitfile = NULL;
  hitin = NULL;
  if (env->view != SPOT_VIEWSTREAMS) {
    strcpy(tempname1,tmpnam(NULL));
    strcpy(libname,tempname1);
    strcat(libname,"_queries");
    strcat(libname,".qlb");
  }

  if (filelist != NULL) {
    if (env->view != SPOT_VIEWSTREAMS && 
        (nhistorical = spot_submit(filelist, env->searchdir, libname, 0, 
         replace))) {
      /* Ok to do the search. Create script to do it */
      /* get temp file names */
      strcpy(tempname2,tmpnam(NULL));

      /* Get default directory and file extension for current view mode */
      defaultfile_path = spot_curdefault(env, env->view);
      defaultfile_ext = spot_curext(env->view);

      /* open temp_script */ 
      if ((script = fopen(tempname1, "w")) == NULL) {
        spot_errmessage("Error creating work file");
        goto DONE;
      }

      if (env->view != SPOT_VIEWARCHIVES) {
	/* Create a hit file containing all names of all files to be
	   searched */
        strcpy(temphitfilename,tempname2);
        strcat(temphitfilename,".hit");
	fit_expfspec(temphitfilename, ".hit", spot_curpath, hitspec);
	if ((hitfile = drct_open(hitspec, O_RDWR+O_CREAT, 0, -1, -1)) == NULL) {
	  spot_errmessage("Error creating work hit file");
	  goto DONE;
	}
        if (env->view == SPOT_VIEWFILES || env->view == SPOT_VIEWQUERIES ||
	    env->view >= SPOT_VIEWDETAIL) {
	  if (searchselected || searchall) {
	    /* Put all selected files into hit file */
	    for (i=0; i < env->menu->nopts; ++i) {
	      opt = men_opt(env->menu, i);
	      if (opt->descr.stat || searchall) {
		strcpy(filespec, spot_fspec(opt, env->view));
		len = strlen(filespec);
		filespec[len++] = '\n';
		filespec[len] = 0;
	        if (!drct_append(hitfile, filespec, len, (long) len)) {
		  spot_errmessage("Error appending to work hit file");
		  goto DONE;
	        }
	      }
	    }
	  }
	  else {
	    /* Put only "current" file into hit file */
	    opt = men_opt(env->menu, env->menu->curopt);
	    strcpy(filespec, spot_fspec(opt, env->view));
	    len = strlen(filespec);
	    filespec[len++] = '\n';
	    filespec[len] = 0;
	    if (!drct_append(hitfile, filespec, len, (long) len)) {
	      spot_errmessage("Error appending to work hit file");
	      goto DONE;
	    }
	  }
        }
        else {
	  /* Copy contents of hit files into temp file */
	  if (searchselected || searchall) {
	    /* Put all selected files into hit file */
	    for (i=0; i < env->menu->nopts; ++i) {
	      opt = men_opt(env->menu, i);
	      if (opt->descr.stat || searchall) {
		strcpy(filespec, spot_fspec(opt, env->view));
		if ((hitin = drct_open(filespec, O_RDONLY, 0, -1, -1)) != 
		    NULL) {
		  while (nbytes = 
		      drct_rdseq(hitin, hitdescriptor, TIPS_DESCRIPTORMAX,
		      &reclen)) {
		    len = min(nbytes, TIPS_DESCRIPTORMAX);
	            if (!drct_append(hitfile, hitdescriptor, len, (long) len)) {
		      spot_errmessage("Error appending to work hit file");
		      goto DONE;
		    }
		    if (nbytes != reclen) drct_cancelio(hitin);
		  }
		  drct_close(hitin);
		  hitin = NULL;
	        }
	        else {
		  sprintf(errmessage,"Error accessing hit file: %s", filespec);
		  spot_errmessage(errmessage);
		}
	      }
	    }
	  }
	  else {
	    /* Put only "current" file into hit file */
	    opt = men_opt(env->menu, env->menu->curopt);
	    strcpy(filespec, spot_fspec(opt, env->view));
	    if ((hitin = drct_open(filespec, O_RDONLY, 0, -1, -1)) != NULL) {
	      while (nbytes = drct_rdseq(hitin, hitdescriptor, 
		  TIPS_DESCRIPTORMAX, &reclen)) {
		len = min(nbytes, TIPS_DESCRIPTORMAX);
	        if (!drct_append(hitfile, hitdescriptor, len, (long) len)) {
		  spot_errmessage("Error appending to work hit file");
		  goto DONE;
		}
		if (nbytes != reclen) drct_cancelio(hitin);
	      }
	      drct_close(hitin);
	      hitin = NULL;
	    }
	    else {
	      sprintf(errmessage,"Error accessing hit file: %s", filespec);
	      spot_errmessage(errmessage);
	      goto DONE;
	    }
	  }
	}
	drct_close(hitfile);
	hitfile = NULL;

	/* Add command to do the search */
	if (env->view == SPOT_VIEWSTREAMDETAIL)
	  spot_setfieldfile(spot_defaultfields, NULL, env->streamname, 
		fieldfile);
	else
	  spot_setfieldfile(spot_defaultfields, NULL, NULL, fieldfile);

	if (fieldfile[0]) {
	  if (spot_defaultdelim >= 0)
	    fprintf(script, "hitr %s | spotter -d %x -f %s %s | hitw -h\n", 
		hitspec, spot_defaultdelim, fieldfile, libname);
	  else
	    fprintf(script, "hitr %s | spotter -f %s %s | hitw -h\n", 
		hitspec, fieldfile, libname);
	}
	else
	  fprintf(script, "hitr %s | spotter %s | hitw -h\n", hitspec,
		libname);

	/* Add command to delete the temp hit file */
	fprintf(script, "rm %s*\n", tempname2);
      }

      else {
	/* Create command to have archives profiled */
	if (searchselected || searchall) {
	  for (i=0; i < env->menu->nopts; ++i) {
	    opt = men_opt(env->menu, i);
	    if (opt->descr.stat || searchall) {
	      strcpy(filespec, spot_fspec(opt, env->view));
	      if (spot_defaultdelim >= 0)
	        spot_setfieldfile(spot_defaultfields, filespec, NULL, 
		  fieldfile);

	      if (fieldfile[0])
	        if (spot_defaultdelim >= 0)
	          fprintf(script,
		    "spotter <%s -a %s -d %x -f %s %s | hitw -h\n", filespec,
		     filespec, spot_defaultdelim, fieldfile, libname);
	        else
	          fprintf(script, "spotter <%s -a %s -f %s %s | hitw -h\n",
		    filespec, filespec, fieldfile, libname);
	      else
	        fprintf(script, "spotter <%s -a %s %s | hitw -h\n", filespec,
		  filespec, libname);
	    }
	  }
	}
	else {
	  opt = men_opt(env->menu, env->menu->curopt);
	  strcpy(filespec, spot_fspec(opt, env->view));
	  spot_setfieldfile(spot_defaultfields, filespec, NULL, fieldfile);
	  if (fieldfile[0])
	    if (spot_defaultdelim >= 0)
	      fprintf(script, "spotter <%s -a %s -d %x -f %s %s | hitw -h\n",
		filespec, filespec, spot_defaultdelim, fieldfile, libname);
	    else
	      fprintf(script, "spotter <%s -a %s -f %s %s | hitw -h\n",
		filespec, filespec, fieldfile, libname);
	  else
	    fprintf(script, "spotter <%s -a %s %s | hitw -h\n", filespec,
	      filespec, libname);
	}
      }

      /* Put in command to delete the files names tempname1 */
      fprintf(script, "rm %s*\n", tempname1);

      /* Close the script */
      fclose(script);
      script = NULL;

      /* Spawn a shell to execute the created script */
      if (!(pid = fork())) {
        /* In child process */
        /* Reopen standard in, out, and error to where we want them to be */ 
        freopen(tempname1, "r", stdin);
        freopen("/dev/null", "w", stdout);
        freopen("/dev/null", "w", stderr);

        /* Execute the script */
        execlp("sh", "sh", (char *) 0);

        /* This should never get executed */
        exit(errno);
      }

      else if (pid == -1)
        /* Error from fork */
        spot_errmessage("Error forking search process");
      else
	retstat = 1;
#if FIT_COHERENT
      spot_scrinit();
#endif
    }

    else if (env->view == SPOT_VIEWSTREAMS) {
      /* Put in commands to do stream updating */
      if (searchselected || searchall) {
	for (i=0; i < env->menu->nopts; ++i) {
	  opt = men_opt(env->menu, i);
	  if (opt->descr.stat || searchall) {
	    fit_setext(spot_fspec(opt, env->view), ".qlb", filespec);
	    spot_submit(filelist, defaultpath, filespec, 1, replace);
	  }
	}
      }
      else {
	opt = men_opt(env->menu, env->menu->curopt);
	fit_setext(spot_fspec(opt, env->view), ".qlb", filespec);
	retstat = spot_submit(filelist, defaultpath, filespec, 1, replace);
      }
    }
  }

  if (nhistorical == 1) {
    retstat = 0;
    /* On historical searches with only one query, automatically invoke
       browse */
    fit_expfspec(filelist[0], ".hit", defaultpath, filespec);
    fit_setext(filespec, ".hit", filespec);
    tempenv = *env;
    tempenv.view = SPOT_VIEWRESULTS;
    tempenv.prevsel = NULL;
    spot_newdoc(&tempenv.doc);
    tempenv.menu = spot_fmenu(&tempenv, tempenv.view, spot_dispwin, 0, 
	filelist, "");
    tempenv.doc.handle = drct_open(filespec, O_RDWR, 0, -1, -1);
    tempenv.doc.filetype = SPOT_VIEWRESULTS;
    spot_browse(&tempenv, &tempenv.doc, 0L);
    spot_docdeall(&tempenv.doc);
    men_dealloc(&tempenv.menu);
  }

DONE:
  /* Clean up, remove temporary files and data structures */
  if (filelist != NULL) fit_delfilelist(&filelist);
  if (hitfile != NULL) {
    drct_close(hitfile);
    drct_delfile(hitspec);
  }
  if (hitin != NULL) drct_close(hitin);

  if (script != NULL) {
    fclose(script);
    unlink(tempname1);
  }
  return retstat;
}
