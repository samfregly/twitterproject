/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_scrrestore.c					spotscrr.c

Function : Restores saved spot display.

Author : A. Fregly

Abstract : This function is used to restore a spot display.

Calling Sequence : spot_scrrestore(unsigned selmask, WINDOW *cmdwin, 
  WINDOW *labelwin, WINDOW *dispwin, WINDOW *footerwin, WINDOW *promptwin);

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_scrrestore(unsigned selmask, WINDOW *cmdwin, WINDOW *labelwin, 
  WINDOW *dispwin, WINDOW *footerwin, WINDOW *promptwin)
#else
void spot_scrrestore(selmask, cmdwin, labelwin, dispwin, footerwin, promptwin)
  unsigned selmask;
  WINDOW *cmdwin, *labelwin, *dispwin, *footerwin, *promptwin;
#endif
{

  if (selmask & 0x1) {
    if (cmdwin != NULL) {
      overwrite(cmdwin, spot_cmdwin);
      delwin(cmdwin);
    }
    else {
      wattrset(spot_cmdwin, spot_cmdmenu->normal);
      win_fill(spot_cmdwin, 0, 0, 0, 0, ' ');
    }
    touchwin(spot_cmdwin);
    wrefresh(spot_cmdwin);
  }

  if (selmask & 0x2) {
    if (labelwin != NULL) {
      overwrite(labelwin, spot_labelwin);
      delwin(labelwin);
    }
    else {
      wattrset(spot_labelwin, spot_cmdmenu->normal);
      win_fill(spot_cmdwin, 0, 0, 0, 0, ' ');
    }
    touchwin(spot_labelwin);
    wrefresh(spot_labelwin);
  }

  if (selmask & 0x4) {
    if (dispwin != NULL) {
      overwrite(dispwin, spot_dispwin);
      delwin(dispwin);
    }
    else if (spot_env.menu != NULL) {
      wattrset(spot_dispwin, spot_dispmenu->normal);
      win_fill(spot_dispwin, 0, 0, 0, 0, ' ');
    }
    touchwin(spot_dispwin);
    wrefresh(spot_dispwin);
  }

  if (selmask & 0x8) {
    if (footerwin != NULL) {
      overwrite(footerwin, spot_footerwin);
      delwin(footerwin);
    }
    else {
      wattrset(spot_footerwin, spot_cmdmenu->normal);
      win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
    }
    touchwin(spot_footerwin);
    wrefresh(spot_footerwin);
  }

  if (selmask & 0x10) {
    if (promptwin != NULL) {
      overwrite(promptwin, spot_promptwin);
      delwin(promptwin);
    }
    else if (spot_dispmenu != NULL) {
      wattrset(spot_promptwin, spot_dispmenu->normal);
      win_fill(spot_promptwin, 0, 0, 0, 0, ' ');
    }
    touchwin(spot_promptwin);
    wrefresh(spot_promptwin);
  }
}
