/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_edit						spotedit.c

Function : Invokes appropriate editor to allow user to edit specified document.

Author : A. Fregly

Abstract : This function will invoke the specified editor/application for the 
	document so that the user may edit it.

Calling Sequence : spot_edit(char *docname, char *editor, int *updated);

  docname	Document name of document to be edited. Docname may be either
		a file specification or a TIPS document descriptor. If docname
		specifies a document in an archive, the document is extracted
		to a temporary file, and the editor is invoked on the temporary
		file, and the document is then placed back into the archive
		if it was updated.
  editor	Editor/Application to be invoked against docname.
  updated	non-zero value is returned if the document is modified.

Notes: stub for now.

Change log : 
000	04-OCT-91  AMF	Created.
001	30-APR-94  AMF  Re-init screen after edit under Coherent
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
long time(long *rettime);
#else
long time();
#endif

//extern int errno;

#if FIT_ANSI
void spot_edit(char *docname, char *editor, int *updated)
#else
void spot_edit(docname, editor, updated)
  char *docname, *editor;
  int *updated;
#endif
{

#define BUFLEN 2048

  char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char errmessage[TIPS_DESCRIPTORMAX+1+64],dbname[TIPS_DESCRIPTORMAX+1];
  char docnum_s[TIPS_DESCRIPTORMAX+1], filespec[TIPS_DESCRIPTORMAX+1];
  char buf[BUFLEN], *editfile;
  char descriptor[TIPS_DESCRIPTORMAX+1], arcdbname[TIPS_DESCRIPTORMAX+1];
  char tempspec[FIT_FULLFSPECLEN+1];
  long doclen, docnum, curtime, recleft, arcdoclen;
  int nbytes, pid, exitpid, exitstat, descriptorlen;
  struct drct_s_file *arc;
  FILE *tmp;
  struct stat statbuf;

  *updated = 0;
  arc = NULL;
  tmp = NULL;
  tempspec[0] = 0;

  /* Parse document name */
  tips_parsedsc(docname, namefld, &doclen, keyfld);
  if (!docname[0]) {
    sprintf(errmessage,"Unable to decode document descriptor: %s",
	docname);
    goto ERREXIT;
  }

  tips_nameparse(namefld, dbname, docnum_s, filespec);
  if (!dbname[0] && !filespec[0]) {
    sprintf(errmessage,"Unable to determine document name from: %s",
	namefld);
    goto ERREXIT;
  }
  
  if (dbname[0]) {
    if ((arc = drct_open(dbname, O_RDWR, 0, -1, -1)) == NULL) {
      sprintf(errmessage,"Unable to open archive: %s", dbname);
      goto ERREXIT;
    }
    sscanf(docnum_s, "%ld", &docnum);
    if (!(nbytes = drct_rdrec(arc, docnum, buf, TIPS_DESCRIPTORMAX, &doclen))) {
      sprintf(errmessage,"Error loading document from archive: %s", dbname);
      goto ERREXIT;
    }

    buf[nbytes] = 0;
    tips_extractdescr(buf, descriptor, &descriptorlen);
    if (descriptorlen) {
      tips_parsedsc(descriptor, docname, &arcdoclen, keyfld);
      if (docname[0])
        tips_nameparse(docname, arcdbname, docnum_s, filespec);
      else
	filespec[0] = 0;
      if (descriptorlen < nbytes)
        memmove(buf, buf + descriptorlen, nbytes - descriptorlen); 
    }
    else {
      filespec[0] = 0;
      keyfld[0] = 0;
      descriptorlen = 0;
    }

    editfile = tmpnam(tempspec);
    if ((tmp = fopen(editfile,"w")) == NULL) {
      strcpy(errmessage,"Unable to open work file");
      goto ERREXIT;
    }

    for (recleft = doclen - descriptorlen, nbytes -= descriptorlen; recleft;) {
      if (nbytes > 0) {
        if (fwrite(buf, 1, nbytes, tmp) != nbytes) {
	  strcpy(errmessage,"Error adding text to work file");
	  goto ERREXIT;
        }
        recleft -= nbytes;
      }

      if (recleft) {
        if (!(nbytes = drct_rdrec(arc, docnum, buf, min(BUFLEN, (int) recleft),
	    &doclen))) {
          sprintf(errmessage,"Error loading document from archive: %s", dbname);
	  goto ERREXIT;
	}
      }
    }
    drct_unlock(arc);
    fclose(tmp);
    tmp = NULL;
  }
  else
    editfile = filespec; 

  curtime = time(NULL);
  endwin();
  if (!(pid = fork())) {
    execlp(editor, editor, editfile, (char *) 0);
    exit(errno);
  }
  else if (pid > 0) {
    while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
      ;
#if FIT_COHERENT
    spot_scrinit();
#endif
  }
  else {
    wrefresh(spot_dispwin);
    spot_errmessage("Error from fork");
  }

  *updated = (!stat(editfile, &statbuf) && statbuf.st_mtime > curtime &&
	statbuf.st_size); 

  if (*updated && arc != NULL) {
    if ((tmp = fopen(editfile, "r")) == NULL) {
      sprintf(errmessage, "Error opening edited file");
      spot_errmessage(errmessage);
    }
    else {
      tips_makedescr(arcdbname, docnum, filespec, statbuf.st_size, keyfld,
	TIPS_DESCRIPTORMAX, buf, &nbytes);
      doclen = statbuf.st_size + nbytes;
      recleft = doclen;
      do {
	if (!drct_rewrite(arc, buf, nbytes, doclen)) {
	  sprintf(errmessage, "Error rewriting document into archive %s", 
	    dbname);
	  spot_errmessage(errmessage);
	  break;
	}
	if (recleft -= nbytes) {
	  if (recleft < BUFLEN)
	    nbytes = recleft;
	  else
	    nbytes = BUFLEN;
	  if (!(nbytes = fread(buf, 1, nbytes, tmp))) {
	    sprintf(errmessage, "Error reading edited file");
	    spot_errmessage(errmessage);
	    break;
	  }
	}
      } while (recleft);
    }
  }
  goto DONE;

ERREXIT:
  spot_message(errmessage);

DONE:
  if (arc != NULL) drct_close(arc);
  if (tmp != NULL) fclose(tmp);
  if (tempspec[0]) unlink(tempspec);
}

