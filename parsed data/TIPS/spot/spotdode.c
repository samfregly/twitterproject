/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_dodelete					spotdode.c

Function : This function deletes the items in the supplied list. 
	it.

Author : A. Fregly

Abstract : This function is called during processing of the main menu 
  File/Delete option to perform the deletion of the specified items.

Calling Sequence : int spot_dodelete(struct spot_s_env *env, 
  int useselected, int delqrys, int delhits);

  env		Spot environment structure.
  delqrys	Specifies that query files for searches being deleted should
		be deleted.
  delhits	Specifies that result files for searches being deleted should
		be deleted.

Notes: 

Change log : 
000  20-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>
#include <drct.h>

#if FIT_ANSI
int spot_dodelete(struct spot_s_env *env, int useselected, int delqrys, 
  int delhits)
#else
int spot_dodelete(env, useselected, delqrys, delhits)
  struct spot_s_env *env;
  int useselected, delqrys, delhits;
#endif
{

  char *src, streamname[FIT_FULLFSPECLEN+1], srcspec[FIT_FULLFSPECLEN+1];
  char errmessage[FIT_FULLFSPECLEN+64];
  struct drct_s_file *qlib;
  int delstat, deldocs;
  long entryidx, i, startopt, endopt, docnum, lenfld;
  struct stat statbuf;
  struct drct_s_file *dbhandle;
  char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char dbname[FIT_FULLFSPECLEN+1], docnum_s[FIT_FULLFSPECLEN+1], fspec[FIT_FULLFSPECLEN+1];
  char prevdbname[FIT_FULLFSPECLEN+1];
  struct men_s_menu *menu;
  struct men_s_option *opt;

  delstat = 0;
  qlib = NULL;
  deldocs = delqrys;
  prevdbname[0] = 0;
  dbhandle = NULL;

  if (env->view == SPOT_VIEWSTREAMDETAIL) {
    /* Open stream library */
    fit_setext(env->streamname, ".qlb", streamname);
    if ((qlib = qc_openlib(streamname, O_RDWR, 0)) == NULL) {
      strcpy(errmessage,"Unable to open query library for stream: ");
      strcat(errmessage, streamname);
      spot_errmessage(errmessage);
      goto DONE;
    }
  }

  if (useselected) {
    startopt = 0;
    endopt = env->menu->nopts - 1;
  }
  else {
    startopt = env->menu->curopt;
    endopt = startopt;
  }

  for (i=startopt; i <= endopt; ++i) {
    opt = men_opt(env->menu, i);
    if (opt->descr.stat || !useselected) {
      src = spot_fspec(opt, env->view);
      if (env->view == SPOT_VIEWSTREAMDETAIL) {
        fit_setext(src, ".qry", srcspec);
        if ((entryidx = qc_findlib(qlib, srcspec)) >= 0) {
	  if (!qc_dellib(qlib, entryidx)) {
	    strcpy(errmessage,"Error deleting object query: ");
	    strcat(errmessage, srcspec);
	    spot_errmessage(errmessage);
	  }
	  else
	    delstat = 1;
        }
      }
      else
        strcpy(srcspec, src);

      if (env->view != SPOT_VIEWSTREAMDETAIL && env->view != SPOT_VIEWQUERIES &&
	  env->view != SPOT_VIEWRESULTS && env->view < SPOT_VIEWDETAIL) {

        if (env->view == SPOT_VIEWSTREAMS) {
          /* Stop profiler for stream */
	  fit_setext(srcspec, ".act", srcspec);
          spot_pstop(srcspec);
        }

        if (env->view == SPOT_VIEWFILES || env->view == SPOT_VIEWSTREAMS) {
          if ((stat(srcspec, &statbuf) || unlink(srcspec)) &&
	      env->view != SPOT_VIEWSTREAMS) {
	    strcpy(errmessage, "Error deleting: ");
	    strcat(errmessage, srcspec);
            spot_errmessage(errmessage);
	  }
        }

        if (env->view == SPOT_VIEWSTREAMS)
	  fit_setext(srcspec, ".qlb", srcspec);

        if (env->view == SPOT_VIEWARCHIVES || env->view == SPOT_VIEWSTREAMS) {
          if (stat(srcspec, &statbuf) || !drct_delfile(srcspec)) {
	    strcpy(errmessage, "Error deleting: ");
	    strcat(errmessage, srcspec);
            spot_errmessage(errmessage);
          }
        }
      }
      else if (env->view < SPOT_VIEWDETAIL) {
        if (delqrys) {
          fit_setext(src, ".qry", srcspec);
          if (stat(srcspec, &statbuf) || unlink(srcspec)) {
	    strcpy(errmessage, "Error deleting query file: ");
	    strcat(errmessage, srcspec);
            spot_errmessage(errmessage);
          }
        }

        if (delhits) {
          fit_setext(src, ".hit", srcspec);
          if (stat(srcspec, &statbuf) || !drct_delfile(srcspec)) {
	    strcpy(errmessage, "Error deleting result file: ");
	    strcat(errmessage, srcspec);
            spot_errmessage(errmessage);
          }
        }
      }
      else {
	if (!drct_setpos(env->doc.handle, i)) {
	  strcpy(errmessage, "Internal error accessing document reference: #");
	  sprintf(docnum_s,"%ld", i);
	  strcat(errmessage, docnum_s);
	  spot_errmessage(errmessage);
	}
	else {
	  if (!drct_delrec(env->doc.handle, 1)) {
	    strcpy(errmessage, "Error deleting document reference: #");
	    sprintf(docnum_s,"%ld", i+1);
	    strcat(errmessage, docnum_s);
	    spot_errmessage(errmessage);
	  }
	  else if (delqrys && env->prevview != SPOT_VIEWARCHIVES) {
	    tips_parsedsc(src, namefld, &lenfld, keyfld);
	    tips_nameparse(namefld, dbname, docnum_s, fspec);
	    if (!*dbname) {
	      if (unlink(fspec)) {
	        strcpy(errmessage, "Error deleting file: ");
	        strcat(errmessage, fspec);
	        spot_errmessage(errmessage);
	      }
	      else {
	        if (strcmp(dbname, prevdbname)) {
	          if (dbhandle != NULL) drct_close(dbhandle);
	          dbhandle = drct_open(dbname, O_RDWR, 0, -1, -1);
	          strcpy(prevdbname, dbname);
	          if (dbhandle == NULL) {
		    strcpy(errmessage, "Error accessing archive: ");
		    strcat(errmessage, dbname);
		    spot_errmessage(errmessage);
	          }
	        }
	        if (dbhandle != NULL) {
	          sscanf(docnum_s," %ld", &docnum);
	          if (!drct_setpos(dbhandle, docnum)) {
		    strcpy(errmessage, 
			"Error accessing archive document: ");
		    strcat(errmessage, dbname);
		    strcat(errmessage," #");
		    sprintf(docnum_s,"%ld", docnum+1);
		    strcat(errmessage, docnum_s);
		  }
		  else if (!drct_delrec(dbhandle, 1)) {
		    strcpy(errmessage, 
			"Error deleting archive document: ");
		    strcat(errmessage, dbname);
		    strcat(errmessage," #");
		    sprintf(docnum_s,"%ld", docnum+1);
		  }
	        }
	      }
	    }
	  }
	}
      }
    }
  }

DONE:
  if (env->view == SPOT_VIEWSTREAMDETAIL && delstat)
    spot_pupdate(streamname);

  if (qlib != NULL) qc_closelib(qlib);
  if (dbhandle != NULL) drct_close(dbhandle);

  if (env->view >= SPOT_VIEWDETAIL && 
      (env->prevview == SPOT_VIEWFILES || env->prevview == SPOT_VIEWQUERIES) ||
      env->view < SPOT_VIEWDETAIL)
    spot_rebuilddisp(env);
  else {
    menu = spot_fmenu(env, env->view, spot_dispwin, 0, NULL, 
	env->summaryitem);
    if (env->menu != NULL) men_dealloc(&env->menu); 
    env->menu = menu;
    if (env->menu != NULL)
      men_draw(env->menu);
    else {
      wattrset(spot_dispwin, env->dispnorm);
      win_fill(spot_dispwin, 0, 0, 0, 0, ' ');
    }  
  }
}
