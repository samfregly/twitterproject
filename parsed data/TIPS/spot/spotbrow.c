/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_browse					spotbrow.c

Function : Document browser for the spot program.

Author : A. Fregly

Abstract : This is the main routine for the spot document browser. Spot allows
	browsing of documents in all view modes except streams. For browses
	invoked during "file" or "queries" mode, a temporary result file 
	containing all selected documents is created, then a "hit" browse
	is performed on the temporary result file. In database mode, and
	archive mode, the "current" document for the view mode is the
	one which is browsed.

Calling Sequence : spot_browse(struct spot_s_env *env, struct spot_s_doc *doc,
  long curopt); 

  env		Spot environment structure.
  doc		Spot document structure containing handle for archive or
		result file to be browsed.
  curopt	Current item to be initially displayed.

Notes: 

Change log : 
000	07-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>
#include <drct.h>

static int firsttime=1;

#if FIT_ANSI
void spot_browse(struct spot_s_env *env, struct spot_s_doc *doc, long curopt)
#else
void spot_browse(env, doc, curopt)
  struct spot_s_env *env;
  struct spot_s_doc *doc;
  long curopt;
#endif
{

  int menudown, optselected, key, exitflag;
  int relative, idx, sidx, eidx, previdx, topline, inviewmode, timedout;
  long retopt;
  int switchkey, waittime;
  char errmessage[FIT_FULLFSPECLEN+64];
  char fname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  long mv, loff;
  struct stat statbuf;
  struct men_s_option *opt;

  WINDOW *cmdwin = NULL;
  WINDOW *labelwin = NULL;
  WINDOW *dispwin = NULL;
  WINDOW *footerwin = NULL;
  WINDOW *promptwin = NULL;

  inviewmode = env->view;
  if (env->view == SPOT_VIEWQUERIES) env->view = SPOT_VIEWRESULTS;

  /* Save snapshot of current screen */
#if !FIT_SUN
  spot_scrsave(NULL, &labelwin, NULL, &footerwin, &promptwin);
#else
  spot_scrsave(NULL, &labelwin, &dispwin, &footerwin, &promptwin);
#endif

  if (firsttime) {
    /* First time called, create windows and menus */
    if (!spot_brmakedisp(env)) {
      spot_errmessage("Error creating browse menus");
      goto DONE;
    }
    firsttime = 0;
#if SPOT_DEBUG
  dummy_write("browse1.dbg", "first time init completed");
#endif
  }

  /* Put up label */
  if (env->view == SPOT_VIEWFILES) {
    spot_label("Browse");
    wrefresh(spot_labelwin);
#if SPOT_DEBUG
  dummy_write("browse1a.dbg", "Put \"Browse\" into label window");
#endif
  }
  else {
    if (env->view >= SPOT_VIEWDETAIL)
      fit_prsfspec(doc->source, dummyspec, dummyspec, dummyspec, fname,
	dummyspec, dummyspec);
    else {
      opt = men_opt(env->menu, env->menu->curopt);
      fit_prsfspec(spot_fspec(opt, env->view), dummyspec, dummyspec, dummyspec,
	fname, dummyspec, dummyspec);
    }
#if SPOT_DEBUG
  dummy_write("browse1b.dbg", "parsed file name");
#endif
    win_fill(spot_labelwin, 0, 1, 0, 0, ' ');
    mvwaddstr(spot_labelwin, 0, max(0,(SPOT_LABELWINLEN - strlen(fname))/2), 
      fname);
    wrefresh(spot_labelwin);
#if SPOT_DEBUG
  dummy_write("browse1c.dbg", "Updated label with file name");
#endif
  }
#if SPOT_DEBUG
  dummy_write("browse2.dbg", "label window filled in");
#endif

  /* Initialize doc structure */
  if (doc->temp == NULL) {
#if SPOT_DEBUG
    dummy_write("browse2a.dbg", "calling tmpfile");
#endif
    doc->temp = tmpfile();
#if SPOT_DEBUG
    dummy_write("browse2b.dbg", "return from tmpfile");
#endif
  }
  if (doc->temp == NULL) {
    spot_errmessage("Unable to open document work file");
    goto DONE;
  }

  /* Draw initial display of first document */

#if SPOT_DEBUG
  dummy_write("browse2c.dbg", "calling strcpy");
#endif

  strcpy(errmessage, 
    "No documents currently available, wait or press any key to exit Browse");
  waittime = 3;
  relative = 0;
#if SPOT_DEBUG
  dummy_write("browse3.dbg", "starting wait for results loop");
#endif
  while (!spot_brviewinit(env, doc, curopt, relative)) {
#if SPOT_DEBUG
  dummy_write("browse4.dbg", "brviewinit failed, continuing wait");
#endif
    curopt = -1;
    relative = 1;
    wrefresh(spot_dispwin);
    wrefresh(spot_footerwin);
    signal(SIGALRM, spot_timeralarm);
    alarm(waittime);
    if (waittime >= 5) {
      key = spot_message(errmessage);
      waittime = 10;
    }
    else {
      key = wgetch(spot_dispwin);
      waittime = waittime + 1;
    }
    alarm(0);
    if (key != ERR) 
      goto DONE;
  }

  wrefresh(spot_dispwin);
  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
  spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, doc->nlines);

  /* Allow user to manipulate the display */
  menudown = 0;
  optselected = 0;
  spot_cmdmenu->curopt = 0;
  exitflag = 0;
#if SPOT_DEBUG
  dummy_write("browse5.dbg", "starting main loop");
#endif

  while (1) {
    wattrset(spot_cmdwin, env->mennorm);
    win_fill(spot_cmdwin, 0, 0, 0, 0, ' ');
    wrefresh(spot_cmdwin);
    spot_puthelpline(spot_brdisphelp);

    menudown = 0;
    optselected = 0;

#if SPOT_DEBUG
  dummy_write("browse6.dbg", "waiting for event");
#endif
    for (key = spot_menukey+1; key != spot_menukey;) {
      if (env->updateinterval) {
	signal(SIGALRM, spot_timeralarm);
	alarm(env->updateinterval);
      }

      key = wgetch(spot_cmdmenu->optwin);

      if (env->updateinterval) 
	timedout = !alarm(0);
      else
	timedout = 0;

      if (key == ERR) {
	if (timedout) {
	  fstat(doc->handle->idx, &statbuf);
  	  doc->ndocs = statbuf.st_size / sizeof(doc->handle->drct);
	  spot_brfooter(NULL, doc->docnum+1, doc->ndocs, 0, 0);
	}
      }

      else if (key == KEY_DOWN || key == KEY_UP || key == KEY_HOME ||
          key == KEY_END || key == KEY_BEG || key == KEY_SBEG ||
          key == KEY_SEND || key == KEY_SHOME || key == KEY_NPAGE ||
          key == KEY_PPAGE || key == '\t' || key == KEY_FIND || 
	  key == KEY_SFIND || key == 'b' - 'a' + 1 || key == 'e' - 'a' + 1 ||
	  key == 'n' - 'a' + 1 || key == 'p' - 'a' + 1) {

        switch (key) {
          case KEY_DOWN :
	    spot_brdraw(env, doc, doc->topline + spot_dispheight);
	    break;
	  case KEY_UP :
	    spot_brdraw(env, doc, doc->topline-1);
	    break;
	  case KEY_NPAGE : case 'n' - 'a' + 1 :
	    spot_brdraw(env, doc, 
 	      doc->topline + spot_dispheight * 2 - 1);	
	    break;
	  case KEY_PPAGE : case 'p' - 'a' + 1 :
	    spot_brdraw(env, doc, doc->topline - spot_dispheight);
	    break;
	  case '\t' : case KEY_FIND : case KEY_SFIND :
	    if (doc->nlines && doc->nhigh) {
	      sidx = 0;
	      eidx = doc->nhigh - 1;
	      idx = 0;
	      previdx = doc->nhigh;
	      loff = doc->lineoffsets[min(doc->nlines-1,
	      doc->topline + spot_dispheight)];

	      while (previdx != idx) {
		previdx = idx;
		idx = (sidx + eidx) >> 1;
		if (doc->highoffsets[idx] > loff)
		  eidx = max(sidx, idx-1);
		else if (doc->highoffsets[idx] < loff)
		  sidx = min(eidx, idx+1);
	      }

	      if (doc->highoffsets[idx] < loff && idx < doc->nhigh-1)
		idx += 1;

	      if (doc->highoffsets[idx] >= loff) {
		for (topline = max(0,doc->topline);
		    topline < (doc->nlines - spot_dispheight + 1) &&
		    doc->lineoffsets[topline] < doc->highoffsets[idx];)
		  ++topline;
		if (doc->lineoffsets[topline] >= doc->highoffsets[idx])
		  topline = max(0,topline - spot_dispheight / 2);

		if (topline < doc->topline)
		  spot_brdraw(env, doc, topline);
		else
		  spot_brdraw(env, doc, topline + spot_dispheight - 1);
	      }
	    }
	    break;

	  case KEY_HOME : case KEY_SHOME : case KEY_BEG : case KEY_SBEG :
	  case 'b' - 'a' + 1 :
	    spot_brdraw(env, doc, 0);
	    break;

	  case KEY_END : case KEY_SEND : case 'e' - 'a' + 1 :
	    spot_brdraw(env, doc, doc->nlines - 1);
	    break;

	  default :
	      ;
      	}
        spot_brfooter("", 0L, 0L, doc->topline+1, doc->nlines);
      }

      else if (key == spot_quitkey  || key == spot_exitkey || key == KEY_EXIT ||
	  key == KEY_SEXIT || key == ('z' - 'a' + 1) || key == ('d' - 'a' + 1)) {
        goto DONE;
      }

      else if (key == KEY_LEFT || key == KEY_SLEFT) {
	spot_brviewinit(env, doc, -1, 1);
	wrefresh(spot_dispwin);
	win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
	spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, doc->nlines);
      }

      else if (key == KEY_RIGHT || key == KEY_SRIGHT) {
	spot_brviewinit(env, doc, 1, 1);
	wrefresh(spot_dispwin);
	win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
	spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, doc->nlines);
      }

      else if (key == '+' || key == '-' || (key >= '0' && key <= '9')) {
        if (spot_brvalue(key, &mv, &relative)) {
	  spot_brviewinit(env, doc, mv, relative);
	  wrefresh(spot_dispwin);
	  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
	  spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, 
		doc->nlines);
        }
      }

      else if (key == spot_menukey) {
        spot_cmdmenu->curopt = 0;
        switchkey = key;
      }

      else if (key == KEY_DC || key == KEY_SDC || key == 0177) {
	if (doc->docnum < doc->ndocs) {
	  if (env->view == SPOT_VIEWARCHIVES && 
	      !spot_ynprompt(env, "Delete document from archive?"))
	    ;
	  else if (!drct_delrec(doc->handle, 1)) {
	    strcpy(errmessage, "Error removing document from ");
	    if (env->view == SPOT_VIEWARCHIVES)
	      strcat(errmessage, "archive");
	    else
	      strcat(errmessage, "result file");
	    spot_errmessage(errmessage);
	  }
	  else {
	    spot_brviewinit(env, doc, 1, 1);
	    wrefresh(spot_dispwin);
	    win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
	    spot_brfooter(doc->docname, doc->docnum+1, doc->ndocs, 1, 
		doc->nlines);
	  }
	}
      }
      else if (key == CTRLW) {
	endwin();
	wrefresh(spot_cmdmenu->optwin);
      }

      else if (key == spot_helpkey) {
	spot_help();
      }

      else {
        menudown = 1;
        switchkey = key;
	optselected = 1;
        key = spot_menukey;
        switch (switchkey) {
          case 'f' : case  'F' :
            spot_cmdmenu->curopt = 0;
            break;
          case 'v' : case 'V' :
            spot_cmdmenu->curopt = 1;
            break;
          case 's' : case 'S' :
            spot_cmdmenu->curopt = 2;
            break;
          case 'a' : case 'A' :
            if (spot_actions[SPOT_MAXVIEW].menu != NULL) {
              spot_cmdmenu->curopt = 3;
            }
            else
              key = switchkey;
            break;
          default :
            key = switchkey;
            menudown = 0;
	    optselected = 0;
        }
      }
    }

    men_draw(spot_cmdmenu);
    men_put(spot_cmdmenu, NULL);
    spot_puthelpline(spot_cmdhelp);

    if (!menudown) {
      for (optselected = 0; !optselected;) {
        key = men_select(spot_cmdmenu, env->updateinterval, &retopt, 
	  &optselected, &timedout);
        if (timedout) {
	  fstat(doc->handle->idx, &statbuf);
  	  doc->ndocs = statbuf.st_size / sizeof(doc->handle->drct);
	  spot_brfooter(NULL, doc->docnum + 1, doc->ndocs, 0, 0);
        }
        else if (key == spot_quitkey)
          goto DONE;
	else if (key == KEY_UP || key == KEY_DOWN || key == spot_exitkey || 
	    key == KEY_EXIT || key == KEY_SEXIT || key == ('z' - 'a' + 1) ||
	    key == ('d' - 'a' + 1))
	  break;
	else if (key == spot_helpkey)
	  spot_help();
      }
    }

    if (optselected) {
      for (menudown = 1; menudown;) {
        key = spot_brcmd(env, doc, spot_cmdmenu->curopt, optselected, 
	  &menudown, &exitflag);
        if (exitflag) goto DONE;
        if (menudown) {
          men_dokey(spot_cmdmenu, key, &optselected);
	  spot_puthelpline(spot_cmdhelp);
	}
      }
    }
  }

DONE:
#if !FIT_SUN
  spot_scrrestore(0x1A, NULL, labelwin, NULL , footerwin, promptwin);
#else
  spot_scrrestore(0x1E, NULL, labelwin, dispwin, footerwin, promptwin);
#endif
  env->view = inviewmode;
}
