/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_newdoc					spotnewd.c

Function : Initializes a new document structure.

Author : A. Fregly

Abstract : This function is used to initialize a totally uninitialized document
	structure.

Calling Sequence : int retstat = spot_newdoc(struct spot_s_doc *doc);

  doc		SPOT document strucrure to be initialized.
  retstat	Returned status.

Notes: 

Change log : 
000	21-MAR-92  AMF	Created.
001	11-SEP-93  AMF	Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_newdoc(struct spot_s_doc *doc)
#else
int spot_newdoc(doc)
  struct spot_s_doc *doc;
#endif
{
  doc->source[0] = 0;
  doc->handle = NULL;
  doc->db = NULL;
  doc->filetype = -1;
  doc->margin = 0; 
  doc->wrapcol = COLS;
  doc->ndocs = 0;
  doc->docnum = -1;
  doc->docname[0] = 0;
  doc->origdocname[0] = 0;
  doc->editor[0] = 0;
  doc->keyfld[0] = 0;
  doc->prevdbname[0] = 0;
  doc->temp = NULL;
  doc->doclen = 0;
  if ((doc->docbuf = (unsigned char *) malloc(SPOT_DOCBUFMAX+1)) != NULL)
    doc->buflen = SPOT_DOCBUFMAX;
  else
    doc->buflen = 0;
  doc->nlines = 0;
  doc->nhigh = 0;
  doc->startline = 0;
  doc->topline = 0;
  doc->qryexe = NULL;
  doc->lineoffsets = NULL;
  doc->highoffsets = NULL;
  doc->highlengths = NULL;
  doc->prevfieldfile[0] = 0;
  doc->fieldnames = NULL;
  doc->fielddata = NULL;
  doc->nfields = 0;

  return (doc->buflen != 0);
}
