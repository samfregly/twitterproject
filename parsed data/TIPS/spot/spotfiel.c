/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fieldload					spotfiel.c

Function : Attempts to load appropriate fields defs for current document.

Author : A. Fregly

Abstract : This function is used by spot_finddoc to load field defintions
	appropriate for the document it has just found.

Calling Sequence : int retstat = spot_fieldload(struct spot_s_env *env, 
	char *prevfieldfile, int isfile, int viewtype, char *arcname, 
	char *(*(*fieldnames)), char *(*(*fielddata)), int *nfields,
	char *newfieldfile);

  env		Spot environment structure.
  prevfieldfile	Field file use for currently loaded fields.
  isfile	Specifies that file for which fields are desired is a file.
  viewtype	Current view mode.
  arcname	Archive from which document for which fields are desired comes.
		arcname is ignored if "isfile" is set.
  fieldnames	On input, is either a pointer at a null field name list, or
		points at currently loaded field names. On return, it will
		contain the field names appropriate for the document.
  fielddata	On input, is either a pointer at a null field data list, or
		points at currently loaded field data. On return, it will
		contain the field data appropriate for the document. 
  nfields	Number of field loaded.
  newfieldfile	Name of field file from which fields where loaded.	
  retstat	A non-zero value is returned if new fields where loaded.

Notes: 

Change log : 
000	17-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>

#if FIT_ANSI
int spot_fieldload(struct spot_s_env *env, char *prevfieldfile, int isfile, 
  int viewtype, char *arcname, char *(*(*fieldnames)), char *(*(*fielddata)),
  int *nfields, char *newfieldfile)
#else
int spot_fieldload(env, prevfieldfile, isfile, viewtype, arcname, fieldnames,
    fielddata, nfields, newfieldfile)
  struct spot_s_env *env;
  char *prevfieldfile;
  int isfile, viewtype, *nfields;
  char *arcname, *(*(*fieldnames)), *(*(*fielddata)), *newfieldfile;
#endif
{
  char testfile[FIT_FULLFSPECLEN+1];
  struct stat statbuf;

  *newfieldfile = 0;

  if (isfile) {
    /* Document is in a file, use either field defs for stream or fallback
       to default field definitions */
    if (viewtype == SPOT_VIEWSTREAMS || viewtype == SPOT_VIEWSTREAMDETAIL) {
      /* See if field definitions for stream are defined */
      fit_setext(env->streamname, ".fld", testfile);
      if (!strcmp(testfile, prevfieldfile))
	/* Field file didn't change */
	strcpy(newfieldfile, testfile);
      else if (!stat(testfile, &statbuf))
	/* Field file will be set to field file for stream */
	strcpy(newfieldfile, testfile);
    }
  }

  else {
    /* Document is in an archive, get field defs for archive, with fallbacks
       to stream field defs, then default field definitions */
    if (*arcname) {
      fit_setext(arcname, ".fld", testfile);
      if (!strcmp(testfile, prevfieldfile))
	/* Field file has not changed */
	strcpy(newfieldfile, testfile);
      else if (!stat(testfile, &statbuf))
	/* Use field file for archive */
	strcpy(newfieldfile, testfile);
    }

    if (!*newfieldfile && (env->view == SPOT_VIEWSTREAMS || 
        env->view == SPOT_VIEWSTREAMDETAIL)) {
      fit_setext(env->streamname, ".fld", testfile);
      if (!strcmp(testfile, prevfieldfile))
	/* Field file has not changed */
	strcpy(newfieldfile, testfile);
      if (!stat(testfile, &statbuf))
	/* Use  field file for stream */
        strcpy(newfieldfile, testfile);
    }
  }

  if (!*newfieldfile && *spot_defaultfields && strcmp(spot_defaultfields, 
        prevfieldfile))
      /* No field file defined above, use default field definitions */
      strcpy(newfieldfile, spot_defaultfields);

  if (*newfieldfile && strcmp(newfieldfile, prevfieldfile))
    /* Found field file and it is different than current, load it */
    return tips_loadflds(newfieldfile, fieldnames, fielddata, nfields);
  else
    return(*fieldnames != NULL);
}
