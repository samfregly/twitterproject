/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_aoption					spotaopt.c

Function : Processes Action menu options off from main menu.

Author : A. Fregly

Abstract : This function processes the various action options off the main
        menu. It will display any pulldowns and dialogs required for completing
        initiation of the function, then do the function.

Calling Sequence : void spot_aoption(struct spot_s_env *env,
        struct men_s_menu *menu, int option);

  env           Spot environment structure.
  menu          Menu displaying action options.
  option        Selected off action menu.

Notes:

Change log :
000     20-FEB-92  AMF  Created by extracting from spot_maincmd.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_aoption(struct spot_s_env *env, struct men_s_menu *menu, int option)
#else
void spot_aoption(env, menu, option)
  struct spot_s_env *env;
  struct men_s_menu *menu;
  int option;
#endif
{

  int srcopt, srcoptsel, key, view;
  char *(*filelist);
  WINDOW *promptwin;

  srcoptsel = 0;
  srcopt = CURRENT;
  filelist = NULL;
  promptwin = NULL;

  if (env->view < SPOT_VIEWDETAIL)
    view = env->view;
  else if (env->prevview != SPOT_VIEWQUERIES)
    view = SPOT_VIEWFILES;
  else
    view = SPOT_VIEWQUERIES;

  if (menu != NULL) {
   if (env->menu == NULL || env->menu->nopts < 1)
     spot_message("No files are available for browsing");
   else if (men_nselected(env->menu) && 
	!(spot_actions[view].opts[option] & ACTION_MASK_NOPARAMS) &&
	!(spot_actions[view].opts[option] & ACTION_MASK_ONEPARAM)) {
     men_overlay(spot_srcmenu, menu, menu->curopt, 1);
     spot_srcmenu->curopt = SELECTED;
     srcoptsel = spot_getpulldown(spot_srcmenu, &srcopt, &key);
     men_remove(spot_srcmenu, spot_dispwin);
   }
   else
     srcoptsel = 1;
  }

  men_remove(menu, spot_dispwin);

  if (srcoptsel && 
      (filelist = spot_worklist(env, srcopt, env->menu->curopt)) != NULL) {
    /* Put "Working..." message in help window */
    win_dup(spot_promptwin, &promptwin);
    if (promptwin != NULL) {
      win_center(spot_promptwin, 0, "Working...");
      wrefresh(spot_promptwin);
    }

    spot_doaction(env, &spot_actions[view], option, filelist);
  }

  if (filelist != NULL) fit_delfilelist(&filelist);

  if (promptwin != NULL) {
    overwrite(promptwin, spot_promptwin);
    delwin(promptwin);
    wrefresh(spot_promptwin);
  }

  spot_rebuilddisp(env);
  wrefresh(spot_dispwin);
}
