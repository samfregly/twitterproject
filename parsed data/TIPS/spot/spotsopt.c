/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_soption					spotsopt.c

Function : Processes options off search menu of main menu.

Author : A. Fregly

Abstract : This function processes the various search options off the main
        menu. It will display any pulldowns and dialogs required for completing
        initiation of the function, then do the function.

Calling Sequence : void spot_soption(struct spot_s_env *env,
        struct men_s_menu *menu, int option);

  env           Spot environment structure.
  menu          Menu displaying search options.
  option        Selected off of menu.

Notes:

Change log :
000     20-FEB-92  AMF  Created by extracting from spot_maincmd.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_soption(struct spot_s_env *env, struct men_s_menu *menu, int option)
#else
void spot_soption(env, menu, option)
  struct spot_s_env *env;
  struct men_s_menu *menu;
  int option;
#endif
{

  int init, rebuild;
  struct men_s_option *opt;

  init = 0;
  rebuild = 0;

  /* Make sure that menu will be restored when display is restored */
  if (env->menu != NULL && env->menu->nopts && 
      spot_search(env, &env->doc, option, 1, 0, NULL, 0)) {
    if (env->view == SPOT_VIEWSTREAMS) {
      signal(SIGALRM, spot_timeralarm);
      alarm(5);
      spot_message("Changing main view to \"Stream Detail\"");
      alarm(0);
      opt = men_opt(env->menu, env->menu->curopt);
      fit_setext(spot_fspec(opt, SPOT_VIEWSTREAMS), ".qlb",  env->streamname);
      env->view = SPOT_VIEWSTREAMDETAIL;
      if (menu != NULL) men_remove(menu, spot_dispwin);
      init = 1;
    }
    else if (env->view != SPOT_VIEWRESULTS) {
      signal(SIGALRM, spot_timeralarm);
      alarm(5);
      spot_message("Changing main view to \"Results\"");
      alarm(0);
      env->view = SPOT_VIEWRESULTS;
      init = 1;
    }
    else
      rebuild = 1;
  }
  else
    rebuild = 1;

  if (menu != NULL) 
    men_remove(menu, spot_dispwin);

  if (init || rebuild) {
    if (init)
      spot_viewinit(env, env->view, -1L, "");
    else
      spot_rebuilddisp(env);
    wrefresh(spot_dispwin);
  }
  wrefresh(spot_footerwin);
}
