/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brvalue					spotprva.c

Function : Allow user to enter relative/absolute document specifier.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_brvalue(int key, long *mv, int *relative);

  key		First character entered by user in typing in value. 
  mv		Returned document specified by user.
  relative	Returned flag, non-zero then move is relative to current
		document.
  stat		0 -> user canceled out.

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

static int imode = 1;

#if FIT_ANSI
int spot_brvalue(int key, long *mv, int *relative)
#else
int spot_brvalue(key, mv, relative)
  int key;
  long *mv;
  int *relative;
#endif
{

  char editline[257], *relptr;
  int editl, stat;

  editl = 12;
  editline[0] = key;
  editline[1] = 0;
  stat = (spot_prompt("Document>", editline, editl) && editline[0]);

  if (stat) {
    relptr = strchr(editline, '+');
    if (relptr == NULL) relptr = strchr(editline, '-');
    *relative = (relptr != NULL);
    if (*relative && strpbrk(relptr,"0123456789") == NULL) {
      relptr[1] = '1';
      relptr[2] = 0;
    }
    else if (!*relative)
      relptr = editline;
    sscanf(relptr," %ld", mv);
    *mv -= !*relative;
  }
  return stat;
}
