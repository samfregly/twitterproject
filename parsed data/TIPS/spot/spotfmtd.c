/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fmtdisip					spotfmtd.c

Function :  Formats output line for main display

Author : A. Fregly

Abstract : 

Calling Sequence : spot_fmtdisp(int viewtype, char *expfspec, char *dispval);

  viewtype	Display type for which output is being formatted.
  expfspec	Expanded file name for item to be formatted.
  dispval	Returned display value.

Notes: 

Change log : 
000	20-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_fmtdisp(int viewtype, char *expfspec, char *dispval)
#else
void spot_fmtdisp(viewtype, expfspec, dispval)
  int viewtype;
  char *expfspec, *dispval;
#endif
{

  char fspec[FIT_FULLFSPECLEN+1];
  long filelength, nsearched, ndocs;
  char tempstr[32];
  struct stat statbuf;
  FILE *in;
  struct drct_s_idx drctentry;
  int len; 
  char tempfspec[FIT_FULLFSPECLEN+1];
  char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  long lenfld, docnum;
  char dbname[FIT_FULLFSPECLEN * 2 +1], docnum_s[FIT_FULLFSPECLEN+1];
  char *fname, dispfspec[FIT_FULLFSPECLEN+1], extension[FIT_FULLFSPECLEN+1];
  char dummyspec[FIT_FULLFSPECLEN+1];

  tips_parsedsc(expfspec, namefld, &lenfld, keyfld);
  tips_nameparse(namefld, dbname, docnum_s, fspec);

  if (*dbname) {
    strcat(dbname, " #");
    sscanf(docnum_s, " %ld", &docnum);
    sprintf(docnum_s, "%ld", docnum + 1);
    strcat(dbname, docnum_s);
    fname = dbname;
  }
  else {
    if (viewtype < SPOT_VIEWDETAIL && viewtype != SPOT_VIEWSTREAMDETAIL) {
      fit_prsfspec(fspec, dummyspec, dummyspec, dummyspec, dispfspec,
	extension, dummyspec);
      if (viewtype == SPOT_VIEWFILES)
	strcat(dispfspec, extension);
    }
    else if (viewtype == SPOT_VIEWSTREAMDETAIL)
      fit_setext(fspec, "", dispfspec);
    else
      strcpy(dispfspec, fspec);
    fname = dispfspec;
  }
      
  len = strlen(fname);
  if (len > COLS) {
    strncpy(dispval,fname,COLS-1);
    dispval[COLS-1] = 0;
    len = COLS;
  }
  else {
    strcpy(dispval, fname);
    len = strlen(dispval);
    fit_sblank(dispval+len, COLS - len);
  }

  if (!*dbname && (viewtype == SPOT_VIEWARCHIVES || 
      viewtype == SPOT_VIEWSTREAMS || viewtype == SPOT_VIEWSTREAMDETAIL ||
      viewtype == SPOT_VIEWRESULTS || viewtype == SPOT_VIEWDETAIL)) {
    if (viewtype == SPOT_VIEWSTREAMS)
      fit_setext(fspec, ".qlb", tempfspec);
    else if (viewtype == SPOT_VIEWSTREAMDETAIL)
      fit_setext(fspec, ".hit", tempfspec);
    else
      strcpy(tempfspec, fspec);

    if (!stat(tempfspec, &statbuf)) {
      if (viewtype == SPOT_VIEWRESULTS || viewtype == SPOT_VIEWSTREAMS)
        fit_sassign(dispval, COLS - 55, COLS - 32, ctime(&statbuf.st_mtime), 
		0, 23);
      else if (viewtype == SPOT_VIEWARCHIVES)
        fit_sassign(dispval, COLS - 60, COLS - 37, ctime(&statbuf.st_mtime), 
		0, 23);
      else if (viewtype == SPOT_VIEWSTREAMDETAIL)
	fit_sassign(dispval, COLS - 44, COLS - 21, ctime(&statbuf.st_mtime), 
		0, 23);
      else if (viewtype == SPOT_VIEWDETAIL)
	fit_sassign(dispval, COLS - 39, COLS - 16, ctime(&statbuf.st_mtime), 
		0, 23);

      filelength = statbuf.st_size;

      if (viewtype == SPOT_VIEWARCHIVES || viewtype == SPOT_VIEWRESULTS ||
	  viewtype == SPOT_VIEWSTREAMDETAIL) {
	fit_setext(fspec, ".drc", tempfspec);
        if (!stat(tempfspec, &statbuf))
	  ndocs = (long) (statbuf.st_size / sizeof(drctentry));
	else
	  ndocs = 0;
        if (viewtype != SPOT_VIEWARCHIVES) {
	  if (ndocs != 1)
	    sprintf(tempstr,"%7ld documents", ndocs);
	  else
	    sprintf(tempstr,"%7ld document", ndocs);
          fit_sassign(dispval, COLS-19, COLS-3, tempstr, 0, strlen(tempstr)-1);
      	}
        else {
	  if (ndocs != 1)
	    sprintf(tempstr,"%7ld docs", ndocs);
	  else
	    sprintf(tempstr,"%7ld doc", ndocs);
          fit_sassign(dispval, COLS - 32, COLS - 21, tempstr, 0, 
	    strlen(tempstr)-1);
	  sprintf(tempstr,"%5ld.%02.2ld mb", (long) filelength / 1000000,
	    (long) ((filelength / 10000) % 100));
	  fit_sassign(dispval, COLS - 13, COLS-3, tempstr, 0, 
	    strlen(tempstr)-1);
        }
      }
      else if (viewtype == SPOT_VIEWSTREAMS) {
	fit_setext(fspec, ".act", tempfspec);
        if ((in = fopen(tempfspec, "r")) != NULL) {
	  if (fread(&nsearched, 1, sizeof(nsearched), in) == 
	      sizeof(nsearched)) {
	    sprintf(tempstr,"searched: %10ld", nsearched);
	    fit_sassign(dispval, COLS - 22, COLS-3, tempstr, 0, 
	      strlen(tempstr)-1);
	  }
        }
	fclose(in);
      }
      else {
	sprintf(tempstr, "%10ld", filelength);
	fit_sassign(dispval, COLS - 12, COLS - 3, tempstr, 0, 
	  strlen(tempstr) - 1);
      }
    }
  }
}
