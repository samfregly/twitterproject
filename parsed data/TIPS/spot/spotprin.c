/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_print					spotprin.c

Function : Formats document and copies it to spot default printer.

Author : A. Fregly

Abstract : This funcition will copy the specified document to the current
	default spot printer, formatting it according the print default
	print formatting characteristics.

Calling Sequence : int retstat = spot_print(struct spot_s_env *env,
	char *docdescriptor);

  env		Spot environment structure.
  docdescriptor	Document descriptor.
  retstat	0, then an error occurred.

Notes: 

Change log : 
000	21-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_print(struct spot_s_env *env, char *docdescriptor)
#else
int spot_print(env, docdescriptor)
  struct spot_s_env *env;
  char *docdescriptor;
#endif
{

  FILE *printer;
  char tempresultname[FIT_FULLFSPECLEN+1];
  struct spot_s_doc doc;
  int descriptorlen, lineonpage, retstat, linelen;
  long line;

  printer = NULL;
  tempresultname[0] = 0;
  retstat = 0;

  /* Initialize document descriptor structure */
  doc.temp = NULL;
  doc.handle = NULL;
  doc.db = NULL;
  doc.margin = env->leftmargin;
  doc.wrapcol = env->wrap;
  doc.ndocs = 0;
  doc.docnum = 0;
  doc.doclen = 0;
  doc.buflen = 0;
  doc.nlines = 0;
  doc.nhigh = 0;
  doc.startline = 0;
  doc.topline = 0;
  doc.docname[0] = 0;
  doc.prevdbname[0] = 0;
  doc.keyfld[0] = 0;
  doc.docbuf = NULL;
  doc.temp = NULL;
  doc.qryexe = NULL;
  doc.lineoffsets = NULL;
  doc.highoffsets = NULL;
  doc.highlengths = NULL;

  if (!(descriptorlen = strlen(docdescriptor))) goto DONE;

  /* See if the printer can be opened */
  if ((printer = fopen(env->printer, "a")) == NULL) goto DONE;

  /* Create a temp result file and put document descriptor in it */
  tmpnam(tempresultname);
  strcat(tempresultname, ".hit");

  if ((doc.handle = drct_open(tempresultname, O_RDWR + O_CREAT, 0, -1, -1)) ==
      NULL) goto DONE;

  if (!drct_append(doc.handle, docdescriptor, descriptorlen,
	(long) descriptorlen)) goto DONE; 

  if ((doc.temp = tmpfile()) == NULL) goto DONE;

  /* Format the document */
  doc.filetype = SPOT_VIEWRESULTS;

  if (!spot_getdoc(env, &doc, 0L, 1, 0)) goto DONE;
  if (!spot_docparse(&doc)) goto DONE;

  /* Copy the temp file to the printer file */
  lineonpage = 0;
  if (env->pagelen < 1) env->pagelen = 1;
  if (env->topmargin >= env->pagelen) env->topmargin = env->pagelen - 1;

  for (line = 0; line < doc.nlines;) {
    for (lineonpage = 0; lineonpage < env->topmargin; ++lineonpage)
      fprintf(printer, "\n");
    for (; lineonpage < env->pagelen && line < doc.nlines; ++line) {
      linelen = doc.lineoffsets[line+1] - doc.lineoffsets[line];
      if (linelen) {
	if (fseek(doc.temp, doc.lineoffsets[line], 0)) goto DONE;
	if (fread(doc.docbuf, 1, linelen, doc.temp) != linelen) goto DONE;
	doc.docbuf[linelen] = 0;
	if (fwrite(doc.docbuf, 1, linelen, printer) != linelen) goto DONE;
	if (linelen == 1 && doc.docbuf[0] == '\f')
	  lineonpage = env->pagelen + 1;
      }
    }
    if (lineonpage == doc.nlines)
      fputc('\f', printer);
  }
  retstat = 1;

DONE:

  /* Clean Up */
  if (printer != NULL) fclose(printer);
  if (doc.handle != NULL) drct_close(doc.handle);
  if (tempresultname[0]) drct_delfile(tempresultname);
  if (doc.db != NULL) drct_close(doc.db);
  if (doc.docbuf != NULL) free(doc.docbuf);
  if (doc.qryexe != NULL) prf_dealloc(&doc.qryexe);
  if (doc.lineoffsets != NULL) free(doc.lineoffsets);
  if (doc.highoffsets != NULL) free(doc.highoffsets);
  if (doc.highlengths != NULL) free(doc.highlengths);
  if (doc.temp != NULL) fclose(doc.temp);

  return retstat;
}
