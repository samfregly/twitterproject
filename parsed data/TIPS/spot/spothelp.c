/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_help						spothelp.c

Function : This function will invoke the spot help utility to display spot
	program help.

Author : A. Fregly

Abstract : This function is a shell around the tips_help function. It provides
	the calls necessary for curses to work right, and also will display
	an error message if the call to the help function fails.

Calling Sequence : spot_help();

Notes: 
  The help result file spot.hit must be available in either the spot help
  directory $SPOT_ROOT/help, or in the directory containing the spot program
  executable.

Change log : 
000	13-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

void spot_help()
{
  endwin();
  if (!tips_help(spot_progname)) {
    wrefresh(spot_dispwin);
    spot_errmessage("Error invoking help. Check definition of SPOT_ROOT and help file availability");
  }
  else
    wrefresh(spot_dispwin);
  return;
}
