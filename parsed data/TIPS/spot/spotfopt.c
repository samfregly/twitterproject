/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_foption					spotfopt.c

Function : Processes File options off main menu.

Author : A. Fregly

Abstract : This function processes the various file options off the main
	menu. It will display any pulldowns and dialogs required for completing
	initiation of the function, then do the function.

Calling Sequence : void spot_foption(struct spot_s_env *env, 
	struct men_s_menu *menu, int option, int *exitflag);

  env           Spot environment structure.
  menu          Menu displaying file options.
  option        Selected off from file menu.
  exitflag	Returned flag indicating if user selected Exit option.

Notes: 

Change log : 
000	20-FEB-92  AMF	Created by extracting from spot_maincmd.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_foption(struct spot_s_env *env, struct men_s_menu *menu, int option, 
  int *exitflag)
#else
void spot_foption(env, menu, option, exitflag)
  struct spot_s_env *env;
  struct men_s_menu *menu;
  int option, *exitflag;
#endif
{

  struct men_s_menu *parent, *pulldown;
  int key, srcopt, srcoptsel, pid, exitpid, exitstat, copyopt, copyoptsel;
  int delqrys, delhits, detailoptsel, retopt, redraw;
  char *curpath, retpath[FIT_FULLFSPECLEN+1], promptstr[80], *(*filelist);
  long curopt;
  struct spot_s_doc doc;

  redraw = 0;
  filelist = NULL;

  switch (option) {
    case OPEN : case NEW :
      men_remove(menu, spot_dispwin);
      menu = NULL;
      if (option == NEW) {
        spot_open(env, -1L, option);
      }
      else {
        spot_open(env, env->menu->curopt, option);
      }
      spot_rebuilddisp(env);
      redraw = 1;
      break;

    case BROWSE :
      if (env->menu == NULL || env->menu->nopts < 1) {
        spot_message("No files are available for browsing");
        break;
      }
      else if (env->view == SPOT_VIEWSTREAMS) {
        spot_message("Streams cannot be browsed");
        break;
      }
      else if (env->view == SPOT_VIEWFILES || env->view >= SPOT_VIEWDETAIL) {
#if SPOT_DEBUG
  dummy_write("fopt1.dbg", "file or detail browse initiated");
#endif
	if (men_nselected(env->menu)) {
          men_overlay(spot_srcmenu, menu, menu->curopt, 1);
          spot_srcmenu->curopt = 1;
	  men_draw(spot_srcmenu);
          srcoptsel = spot_getpulldown(spot_srcmenu, &srcopt, &key);
          men_remove(spot_srcmenu, spot_dispwin);
          if (!srcoptsel) break;
	}
	else
	  srcopt = 0;

	spot_newdoc(&doc);
	spot_docinit(env, &doc, (long) (srcopt != 0));

	curopt = (srcopt) ? 0 : env->menu->curopt;
      }
      else if (env->view != SPOT_VIEWQUERIES) {
	spot_newdoc(&doc);
	spot_docinit(env, &doc, 1L);
	curopt = (env->view == SPOT_VIEWSTREAMDETAIL) ? env->doc.ndocs - 1 : 0;
      }
      else if (env->view == SPOT_VIEWQUERIES) {
	env->view = SPOT_VIEWRESULTS;
	spot_newdoc(&doc);
	spot_docinit(env, &doc, 1L);
	env->view = SPOT_VIEWQUERIES;
	curopt = 0;
      }
 
      /* Browse */
      men_remove(menu, spot_dispwin);
      menu = NULL;
#if SPOT_DEBUG
  dummy_write("fopt2.dbg", "spot_fopt, calling spot_browse");
#endif
      spot_browse(env, &doc, curopt);

      if (env->view < SPOT_VIEWDETAIL) spot_docdeall(&doc);
      spot_rebuilddisp(env);
      redraw = 1;
      goto DONE;
      break;

    case PICK :
      goto DONE;
      break;

    case COPY : case DELETE : case PRINT :
      /* copy, delete, print */

      pulldown = NULL;
      parent = NULL;

      if (men_nselected(env->menu)) {
        /* Put up menu for selection for "current" or "selected" files */
        men_overlay(spot_srcmenu, menu, menu->curopt, 1);
        men_reset(spot_srcmenu);
        spot_srcmenu->curopt=SELECTED;
        parent = spot_srcmenu;
        srcoptsel = spot_getpulldown(spot_srcmenu, &srcopt, &key);
      }
      else {
        parent = menu;
        srcoptsel = 1;
        srcopt = CURRENT;
      }

      if (srcoptsel && option == COPY) {
        /* Doing a copy, determine destination */
	if (env->view != SPOT_VIEWSTREAMS) {
  	  /* Set pulldown to use */
  	  if (env->view != SPOT_VIEWSTREAMDETAIL && 
	      env->view != SPOT_VIEWQUERIES && env->view != SPOT_VIEWRESULTS)
  	    /* Pulldown with options "File", "Results", "Archives", 
	       "Directory" */
  	    pulldown = spot_copymenu;
  	  else
  	    /* Pulldown with options "File", "Results", "Archive", and
  	     "Stream" */
  	    pulldown = spot_strcopymenu;

  	  /* Place the pulldown */
  	  men_overlay(pulldown, parent, parent->curopt, 1);
  	  men_reset(pulldown);

  	  /* Determine initial option */
	  if (srcopt == SELECTED) {
	    if (env->view != SPOT_VIEWSTREAMS && 
		env->view != SPOT_VIEWSTREAMDETAIL)
	      pulldown->curopt = DIRECTORY;
	    else
	      pulldown->curopt = STREAM;
	  }
	  else {
  	    switch (env->view) {
  	      case SPOT_VIEWFILES : case SPOT_VIEWQUERIES :
  	        pulldown->curopt = FILEDEST;
  	        break;
  	      case SPOT_VIEWRESULTS :
  	        pulldown->curopt = RESULTS;
  	        break;
  	      case SPOT_VIEWARCHIVES :
  	        pulldown->curopt = ARCHIVES;
  	        break;
  	      case SPOT_VIEWSTREAMDETAIL :
  	        pulldown->curopt = STREAM;
  	      default :
  	        pulldown->curopt = FILEDEST;
  	    }
          }

  	  /* Let the user pick the copy destination */
  	  copyoptsel = spot_getpulldown(pulldown, &copyopt, &key);
	}
	else {
  	  copyoptsel = 1;
          copyopt = STREAM;
	}
      }

      if (!srcoptsel) {
        if (pulldown != NULL) 
  	  men_remove(pulldown, spot_dispwin);
        if (parent != NULL && parent != menu) 
  	  men_remove(parent, spot_dispwin);
        break;
      }

      switch (option) {
        case COPY :
  	  if (copyoptsel) {
  	    spot_docopy(env, copyopt, srcopt == SELECTED);
	    men_remove(pulldown, spot_dispwin);
            if (parent != NULL && parent != menu) 
  	      men_remove(parent, spot_dispwin);
	    men_remove(menu, spot_dispwin);
	    menu = NULL;
  	    spot_rebuilddisp(env);
	    redraw = 1;
  	  }
	  else {
	    men_remove(pulldown, spot_dispwin);
            if (parent != NULL && parent != menu) 
  	      men_remove(parent, spot_dispwin);
	    men_remove(menu, spot_dispwin);
	  }
  	  break;

        case DELETE : case PRINT :
  	  if (option == DELETE) {
            if (env->view == SPOT_VIEWSTREAMDETAIL ||
  	        env->view == SPOT_VIEWQUERIES ||
  	        env->view == SPOT_VIEWRESULTS) {
  	      men_overlay(spot_detailmenu, parent, parent->curopt, 1);
  	      men_reset(spot_detailmenu);
  	      spot_detailmenu->options[0].descr.stat =
  		(env->view == SPOT_VIEWQUERIES);
  	      spot_detailmenu->options[1].descr.stat =
  		(env->view == SPOT_VIEWRESULTS);
  	      if (env->view != SPOT_VIEWQUERIES)
  	        spot_detailmenu->curopt = 1;
  	      men_draw(spot_detailmenu);

  	      spot_puthelpline(spot_multmenuhelp);
  	      detailoptsel = spot_getpulldown(spot_detailmenu, &retopt,
  		&key);
  	      spot_puthelpline(spot_cmdhelp);
  	      men_remove(spot_detailmenu, spot_dispwin);
  	      if (!detailoptsel) {
		if (parent != NULL && parent != menu)
		  men_remove(parent, spot_dispwin);
		break;
	      }
  	      delqrys = spot_detailmenu->options[0].descr.stat;
  	      delhits = spot_detailmenu->options[1].descr.stat;
            }

	    else if (env->view >= SPOT_VIEWDETAIL && 
		env->prevview != SPOT_VIEWARCHIVES) {
  	      men_overlay(spot_sumdelmenu, parent, parent->curopt, 1);
  	      men_reset(spot_sumdelmenu);
  	      men_draw(spot_sumdelmenu);

  	      detailoptsel = spot_getpulldown(spot_sumdelmenu, &retopt,
  		&key);
  	      men_remove(spot_sumdelmenu, spot_dispwin);
  	      if (!detailoptsel) {
		if (parent != NULL && parent != menu)
		  men_remove(parent, spot_dispwin);
		break;
	      }
  	      delqrys = retopt;
            }
	    else if (env->view >= SPOT_VIEWDETAIL &&
		env->prevview == SPOT_VIEWARCHIVES) {
	      delqrys = 1;
	      delhits = 0;
	    }
            else {
  	      delqrys = 0;
  	      delhits = 0;
            }
	    if (env->view < SPOT_VIEWDETAIL || 
		env->prevview != SPOT_VIEWARCHIVES)
  	      strcpy(promptstr, "Delete ");
	    else
	      strcpy(promptstr, "Delete from archive ");
  	  }
  	  else
  	    strcpy(promptstr, "Print ");

  	  if (srcopt)
  	    strcat(promptstr, "selected items?");
  	  else
  	    strcat(promptstr, "current item?");
  	
  	  if (!spot_ynprompt(env, promptstr)) {
	    if (parent != menu) men_remove(parent, spot_dispwin);
  	    break;
  	  }

	  if (parent != menu) men_remove(parent, spot_dispwin);
  	  men_remove(menu, spot_dispwin);
	  menu = NULL;

  	  /* Print is unsupported for now */
  	  if (option == PRINT)
	    spot_doprint(env, srcopt);
	  else {
  	    spot_dodelete(env, srcopt, delqrys, delhits);
  	    /**** spot_rebuilddisp(env) ****/;
	    redraw = 1;
	  }
  	  break;

        default :
  	  ;
      }
      break;

    case PATH :
      /* Set path */
      if (env->view != SPOT_VIEWSTREAMDETAIL && env->view < SPOT_VIEWDETAIL) {
        curpath = spot_curdefault(env, env->view);
        if (spot_setpath(env, curpath, retpath) && 
  	    strcmp(curpath, retpath)) {
          switch (env->view) {
	    case SPOT_VIEWRESULTS :
	      strcpy(env->searchdir, retpath);
	      break;
  	    case SPOT_VIEWFILES :
	      strcpy(env->filedir, retpath);
  	      break;
  	    case SPOT_VIEWARCHIVES :
  	      strcpy(env->arcdir, retpath);
  	      break;
  	    case SPOT_VIEWQUERIES :
  	      strcpy(env->searchdir, retpath);
  	      break;
  	    case SPOT_VIEWSTREAMS :
  	      strcpy(env->streamdir, retpath);
  	      break;
  	    default :
  	      ;
          }
        }
        men_remove(menu, spot_dispwin);
	menu = NULL;
        spot_viewinit(env, env->view, -1, env->summaryitem);
	redraw = 1;
        wrefresh(spot_footerwin);
      }
      else if (env->view < SPOT_VIEWDETAIL) {
        spot_fselect(env, 0, 0, env->streamdir, ".qlb", &filelist, 
	  env->streamdir);
        if (filelist != NULL) {
  	  fit_expfspec(filelist[0], ".act", env->streamdir, env->streamname);
	  men_remove(menu, spot_dispwin);
	  menu = NULL;
          spot_viewinit(env, env->view, -1, env->summaryitem);
	  redraw = 1;
        }
      }
      else
	spot_message("Summary path cannot be changed");
      goto DONE;
      break;

    case SHELL :
      /* Shell */
      spot_message( "Use shell exit command to return to SPOT, usually either \"exit\" or \"login\"");
      endwin();
      if (!(pid = fork())) {
        execlp(spot_shell, spot_shell, (char *) 0);
        exit(errno);
      }
      else if (pid > 0)
        while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
  	;
      else
        exitstat = errno;
#if FIT_COHERENT
      spot_scrinit();
#endif
      spot_rebuilddisp(env);
      redraw = 1;
      break;
        
    case EXIT :
      /* Exit */
      *exitflag = 1;
      goto DONE; 
      break;

    default :
      ;
  }

DONE:
  if (menu != NULL) men_remove(menu, spot_dispwin);
  if (redraw) {
    wrefresh(spot_cmdwin);
    wrefresh(spot_dispwin);
    wrefresh(spot_labelwin);
    wrefresh(spot_footerwin);
    wrefresh(spot_promptwin); 
  }
  if (filelist != NULL) fit_delfilelist(&filelist);
}
