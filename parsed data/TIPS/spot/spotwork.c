/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_worklist					spotwork.c

Function : This function creates a list of files from selected entries in the
	main display.

Author : A. Fregly

Abstract :  

Calling Sequence : char *(*spot_worklist(struct spot_s_env *env, 
  int useselected, int curopt));

  env		Spot environment structure.
  useselected	Specifies that selected files should be put into list rather
		than "current" file.
  curopt	Pointer at "current" file.

Notes: 

Change log : 
000	20-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

#if FIT_ANSI
char *(*spot_worklist(struct spot_s_env *env, int useselected, int curopt))
#else
char *(*spot_worklist(env, useselected, curopt))
  struct spot_s_env *env;
  int useselected, curopt;
#endif
{

  char *(*filelist);
  int len, n;
  long i, j; 
  struct men_s_option *opt;

  filelist = NULL;

  if (useselected) {
    for (i=0, n=0; i < env->menu->nopts; ++i)
      n += (men_opt(env->menu, i)->descr.stat != 0);
    if (n)
      filelist = (char *(*)) calloc(n+2, sizeof(*filelist));
    if (filelist == NULL) goto DONE;
    for (i=0, j=0; i < env->menu->nopts; ++i) {
      opt = men_opt(env->menu, i);
      if (opt->descr.stat) {
	len = strlen(spot_fspec(opt, env->view));
	if ((filelist[j] = (char *) calloc(1, len+1)) == NULL) goto ERREXIT;
	strcpy(filelist[j++], spot_fspec(opt, env->view));
      }
    }
  }
  else if (curopt < env->menu->nopts) {
    filelist = calloc(3, sizeof(*filelist));
    if (filelist == NULL) goto ERREXIT; 
    opt = men_opt(env->menu, (long) curopt);
    len = strlen(spot_fspec(opt, env->view));
    if ((filelist[0] = (char *) calloc(1, len+1)) == NULL) goto ERREXIT;
    strcpy(filelist[0], spot_fspec(opt, env->view));
    filelist[1] = (char *) calloc(1,1);
    if (filelist[1] == NULL) goto ERREXIT;
    filelist[1][0] = 0;
    filelist[2] = NULL;
  }
  goto DONE;

ERREXIT:
  spot_errmessage("Error allocating internal data structure");
  fit_delfilelist(&filelist);

DONE:
  return filelist;
}
