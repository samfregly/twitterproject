/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_dispupdate					spotdisp.c

Function : Updates file status area in buffer for main display.

Author : A. Fregly

Abstract : This function is used to update the status portion of a single
	entry in the display buffer for the file display in the SPOT main
	screen. This function is used only during viewing of results, archives,
	and streams. In these modes, the display show a single line per file,
	with the first element in a line being the file name sans extension,
	and the remainder of the line containing status information about the
	file. In all three modes, the first status entry is the time of last
	update. In result and archive view modes, the next entry is the number
	of documents in the file, while in stream mode, the next entry is the
	number of documents processed. If the view mode is for archives, the
	line contains an ending entry which is the size of the archive.

Calling Sequence : int retstat = spot_dispupdate(struct spot_s_env *env,
	char *dispitem, int dispidx);

  env		Spot environment structure.
  dispitem	Display item buffer to be updated.
  dispidx	Index of display item in display menu.

Notes: 

Change log : 
000	02-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_dispupdate(struct spot_s_env *env, char *dispitem, int dispidx)
#else
int spot_dispupdate(env, dispitem, dispidx)
  struct spot_s_env *env;
  char *dispitem;
  int dispidx;
#endif
{

  char fspec[FIT_FULLFSPECLEN+1];
  char fname[15], *spaceptr;
  long filelength, nsearched, ndocs;
  char tempstr[32];
  struct stat statbuf;
  FILE *in;
  struct drct_s_idx drctentry;
  int len, offset;
  char dispspec[FIT_FULLFSPECLEN+1];

  if (env->view != SPOT_VIEWSTREAMDETAIL) {
    strncpy(fname, dispitem, 14);
    fname[14] = 0;
    if ((spaceptr = strchr(fname, ' ')) != NULL) *spaceptr = 0;
    strcpy(fspec, spot_fspec(men_opt(env->menu, dispidx), env->view));
    fit_sblank(dispitem+14, COLS - 14);
  }
  else {
    strcpy(fspec, men_opt(env->menu, dispidx)->val);
    fit_setext(fspec, "", dispspec);
    len = strlen(dispspec);
    offset = max(0, len - 34);
    len -= offset;
    strcpy(dispitem, dispspec + offset);
    fit_sblank(dispitem + len, COLS - len);
  }

  if (!stat(fspec, &statbuf)) {
    if (env->view != SPOT_VIEWSTREAMDETAIL && env->view != SPOT_VIEWARCHIVES)
      fit_sassign(dispitem, 25, 48, ctime(&statbuf.st_mtime), 0, 23);
    else if (env->view == SPOT_VIEWSTREAMDETAIL)
      fit_sassign(dispitem, 35, 58, ctime(&statbuf.st_mtime), 0, 23);
    else
      fit_sassign(dispitem, 20, 43, ctime(&statbuf.st_mtime), 0, 23);
    filelength = statbuf.st_size;

    if (env->view != SPOT_VIEWSTREAMS) {
      if (env->view != SPOT_VIEWSTREAMDETAIL)
        fit_expfspec(fname, ".drc", spot_curdefault(env, env->view), fspec);
      else
	fit_setext(fspec, ".drc", fspec);

      if (!stat(fspec, &statbuf))
	ndocs = (long) (statbuf.st_size / sizeof(drctentry));
      else
	ndocs = 0;

      if (env->view != SPOT_VIEWARCHIVES) {
	if (ndocs != 1)
	  sprintf(tempstr,"%7ld documents", ndocs);
	else
	  sprintf(tempstr,"%7ld document", ndocs);
        fit_sassign(dispitem, 60, COLS-1, tempstr, 0, strlen(tempstr)-1);
      }
      else {
	if (ndocs != 1)
	  sprintf(tempstr,"%7ld docs", ndocs);
	else
	  sprintf(tempstr,"%7ld doc", ndocs);
        fit_sassign(dispitem, 48, 59, tempstr, 0, strlen(tempstr)-1);
	sprintf(tempstr,"%5ld.%02.2ld mb", (long) filelength / 1000000,
	  (long) ((filelength / 10000) % 100));
	fit_sassign(dispitem, 65, COLS-1, tempstr, 0, 
	  strlen(tempstr)-1);
      }
    }
    else {
      if ((in = fopen(fspec, "r")) != NULL) {
	if (fread(&nsearched, 1, sizeof(nsearched), in) == 
	    sizeof(nsearched)) {
	  sprintf(tempstr,"searched: %10ld", nsearched);
	  fit_sassign(dispitem, 57, COLS-1, tempstr, 0,
	    strlen(tempstr)-1);
        }
	fclose(in);
      }
    }
  }
  return 1;
}

