/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_results					spotresu.c

Function : Updates search results display.

Author : A. Fregly

Abstract : 

Calling Sequence :  spot_results(struct spot_s_env *env, 
  struct men_s_menu *disp);

  env		Spot environment structure.
  disp		Display menu in which search results are to be updated.

Notes: 

Change log : 
000	05-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_results(struct spot_s_env *env, struct men_s_menu *disp)
#else
void spot_results(env, disp)
  struct spot_s_env *env;
  struct men_s_menu *disp;
#endif
{
  /* Stub for now */
  mvwaddstr(spot_promptwin, 0, 0, "Updating Result Display...");
  wrefresh(spot_promptwin);
  sleep(1);
  win_fill(spot_promptwin, 0, 0, 0, 0, ' ');
  wrefresh(spot_promptwin);
}
