/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_docdeall					spotdocd.c

Function : Deallocates the contents of a document structure.

Author : A. Fregly

Abstract : This function is used to deallocate the contents of a document
	structure.

Calling Sequence : int retstat = spot_docdeall(struct spot_s_doc *doc);

  doc		SPOT document strucrure to have contents deallocated.
  retstat	Returned status.

Notes: 

Change log : 
000	21-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_docdeall(struct spot_s_doc *doc)
#else
int spot_docdeall(doc)
  struct spot_s_doc *doc;
#endif
{
  if (doc->db != doc->handle && doc->db != NULL) drct_close(doc->db);
  if (doc->handle != NULL) drct_close(doc->handle);
  if (doc->temp != NULL) fclose(doc->temp);
  if (doc->docbuf != NULL) free(doc->docbuf);
  if (doc->qryexe != NULL) prf_dealloc(&doc->qryexe);
  if (doc->lineoffsets != NULL) free(doc->lineoffsets);
  if (doc->highoffsets != NULL) free(doc->highoffsets); 
  if (doc->highlengths != NULL) free(doc->highlengths);
  if (doc->fieldnames != NULL)
    tips_loadflds("", &doc->fieldnames, &doc->fielddata, &doc->nfields);
  return spot_newdoc(doc);
}
