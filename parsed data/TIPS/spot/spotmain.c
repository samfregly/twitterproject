/*******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_maincmd					spotmain.c

Function : Handles command processing for main command menu.

Author : A. Fregly

Abstract : This function handles command processing for all options off 
	from spot's main menu. Control will remain in this function or
	it's subfunctions until an invoked option is completed, or the
	user exits a pulldown which is being processed by this function.

Calling Sequence : int key = spot_maincmd(struct spot_s_env *env, int option,
  int optselected, int immediate, int *subopt, int *menudown, int *exitflag);

  env		spot environment structure.
  option	current command menu option.
  optselected	non-zero then user wants current option executed.
  menudown	keeps track of whether or not menu's are currently
		pulled down.
  immediate	non-zero value is used to force execution of an option. 
		The "subopt" option off the menu specified by "option" is
		executed without display of a menu.  
  *subopt	Menu option to be executed when immediate is set. On return,
		is the value of the current suboption off a pull-down.
  exitflag	Returned as non-zero if user enter program exit key or
		select exit off the File menu.
  stat		Returned key entered by user if menus down and key is a
		main menu movement key, zero otherwise. 

Notes: 

Change log : 
000	05-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#define OPEN 0
#define NEW 1


#if FIT_ANSI
int spot_maincmd(struct spot_s_env *env, int option, int optselected, 
  int immediate, int *subopt, int *menudown, int *exitflag)
#else
int spot_maincmd(env, option, optselected, immediate, subopt, menudown, 
    exitflag)
  struct spot_s_env *env;
  int option, optselected, immediate, *subopt, *menudown, *exitflag;
#endif
{

  struct men_s_menu *menu; 
  int key, menuoptsel; 

  if (!optselected && !*menudown) {
    *subopt = 0;
    return 0;
  }

  menu = NULL;

  /* Set menu to be displayed */
  switch (option) {
    case 0 :
      menu = spot_filemenu;
      break;
    case 1 :
      menu = spot_viewmenu;
      break;
    case 2 :
      menu = spot_searchmenu;
      break;
    case 3 :
      if (env->view < SPOT_VIEWDETAIL)
        menu = spot_actions[env->view].menu;
      else if (env->prevview != SPOT_VIEWQUERIES)
	menu = spot_actions[SPOT_VIEWFILES].menu;
      else
	menu = spot_actions[SPOT_VIEWQUERIES].menu;
      break;
    default :
      ;
  }

  if (menu == NULL) {
    key = 0;
    *subopt = 0;
    goto DONE;
  }

  /* Set initial option in menu */
  if (!immediate) {
    /* Reset menu */
    men_reset(menu);
  }
  else {
    /* Caller set option to be invoked */
    menu->curopt = *subopt;
    men_draw(menu);
    men_put(menu, spot_dispwin);
  }

  if (menu != NULL && !*exitflag) {
    if (!immediate) {
      /* Caller did not force menu option, allow user to choose */
      menuoptsel = spot_getpulldown(menu, subopt, &key);
      *menudown = (key == KEY_LEFT || key == KEY_RIGHT || key == KEY_SLEFT || 
	key == KEY_SRIGHT);
    }
    else {
      /* Caller forced option, setup so option will get executed */
      *menudown = 0;
      menuoptsel = 1;
    }

    if (*menudown || !immediate && !menuoptsel) {
      /* User bailed out of the menu */
      men_remove(menu, spot_dispwin);
      /* if (key != spot_quitkey) key = 0; */
      *subopt = 0;
      goto DONE;
    }

    key = 0;

    /* Perform the appropriate function based on the user selected option */
    switch (option) {
      case 0 :
	/* File menu option */
	spot_foption(env, menu, *subopt, exitflag);
	break;

      case 1 :
	/* View menu option */
	spot_voption(env, menu, *subopt);
	break;

      case 2 : 
        /* Search menu option */
	spot_soption(env, menu, *subopt);
	break;

     case 3 :
	/* Action menu option */
	spot_aoption(env, menu, *subopt);
	break;

      default :
        goto DONE;
    }
  }

DONE:
  return key;
} 
