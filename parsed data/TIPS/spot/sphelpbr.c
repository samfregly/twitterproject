/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : sphelp_browse					sphelpbr.c

Function : Document browser for the spot help program.

Author : A. Fregly

Abstract : This is the main routine for the spot help browser. During document
	browsing, the user can use the paging and cursor movement keys to
	move around within the current document, and to move from document
	to document in the help file. The may also enter absolute and relative
	document numbers on the prompt line to directly jump to a document.

Calling Sequence : int exitkey = sphelp_browse(struct spot_s_env *env, 
    struct drct_s_file *help, long startdoc, long *retdoc); 

  env		Spot environment structure.
  help		Direct access file handle open to SPOT result file containing
		references to the documents which make up the available
		help.
  startdoc	Document number of document to display first.
  retdoc	Returned document number at the time the user exited browse.

Notes: 

Change log : 
000	12-NOV-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <spot.h>
#include <sphelp.h>


#if FIT_ANSI
int sphelp_browse(struct spot_s_env *env, struct drct_s_file *help,
  long startdocnum, long *retdocnum)
#else
int sphelp_browse(env, help, startdocnum, retdocnum)
  struct spot_s_env *env;
  struct drct_s_file *help;
  long startdocnum, *retdocnum;
#endif
{

  int key, relative;
  struct spot_s_doc doc;
  long mv;

  WINDOW *labelwin = NULL;
  WINDOW *footerwin = NULL;
  WINDOW *promptwin = NULL;
#if SPHELP_DEBUG
  fprintf(fit_debug_handle, "sphelp_browse, entered\n");
#endif

  doc.handle = help;
  doc.db = NULL;
  doc.margin = 0;
  doc.wrapcol = COLS;
  doc.ndocs = 0;
  doc.docnum = 0;
  doc.doclen = 0;
  doc.buflen = 0;
  doc.nlines = 0;
  doc.nhigh = 0;
  doc.startline = 0;
  doc.topline = 0;
  doc.docname[0] = 0;
  doc.prevdbname[0] = 0;
  doc.keyfld[0] = 0;
  doc.docbuf = NULL;
  doc.temp = NULL;
  doc.qryexe = NULL;
  doc.lineoffsets = NULL;
  doc.highoffsets = NULL;
  doc.highlengths = NULL;
  doc.docbuf = (unsigned char *) calloc(SPOT_DOCBUFMAX+1, 1);
  doc.buflen = SPOT_DOCBUFMAX;
  env->menu = spot_dispmenu;

  /* Save snapshot of current screen */
  spot_scrsave(NULL, &labelwin, NULL, &footerwin, &promptwin);

  /* Initialize doc structure */
  doc.handle = help;
  doc.filetype = SPOT_VIEWRESULTS;
  doc.temp = tmpfile();
  if (doc.temp == NULL) {
    spot_errmessage("Unable to open document work file");
    goto DONE;
  }

  /* Draw initial display of first document */
  env->view = SPOT_VIEWRESULTS;
  if (!spot_brviewinit(env, &doc, startdocnum, 0)) {
#if SPHELP_DEBUG
    fprintf(fit_debug_handle, "sphelp_browse, error from initial call to spot_brviewinit\n");
#endif
    ;
  }
  wrefresh(spot_dispwin);
  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
  spot_brfooter(doc.docname, doc.docnum+1, doc.ndocs, 1, doc.nlines);

  /* Put help line */
  spot_puthelpline(spot_disphelp);

  /* Allow user to manipulate the display */
  for (key = wgetch(spot_dispwin);
      key != spot_menukey && key != spot_quitkey  && key != spot_exitkey && 
      key != KEY_EXIT && key != KEY_SEXIT && key != ('z' - 'a' + 1) &&
      key != ('d' - 'a' + 1);
      key = wgetch(spot_dispwin)) {

    if (key == KEY_DOWN || key == KEY_UP || key == KEY_HOME ||
        key == KEY_END || key == KEY_BEG || key == KEY_SBEG ||
        key == KEY_SEND || key == KEY_SHOME || key == KEY_NPAGE ||
        key == KEY_PPAGE || key == '\t' || key == KEY_FIND || 
	key == KEY_SFIND || key == 'b' - 'a' + 1 || key == 'e' - 'a' + 1 ||
	key == 'n' - 'a' + 1 || key == 'p' - 'a' + 1) {

      switch (key) {
        case KEY_DOWN :
	  spot_brdraw(env, &doc, doc.topline + spot_dispmenu->nrows);
	  break;
	case KEY_UP :
	  spot_brdraw(env, &doc, doc.topline-1);
	  break;
	case KEY_NPAGE : case 'n' - 'a' + 1 :
	  spot_brdraw(env, &doc, 
 	    doc.topline + spot_dispmenu->nrows * 2 - 1);	
	  break;
	case KEY_PPAGE : case 'p' - 'a' + 1 :
	  spot_brdraw(env, &doc, doc.topline - spot_dispmenu->nrows);
	  break;

	case KEY_HOME : case KEY_SHOME : case KEY_BEG : case KEY_SBEG :
	case 'b' - 'a' - 1 :
	  spot_brdraw(env, &doc, 0);
	  break;

	case KEY_END : case KEY_SEND : case 'e' - 'a' + 1 :
	  spot_brdraw(env, &doc, doc.nlines - 1);
	  break;

	default :
	  ;
      }
      spot_brfooter("", 0L, 0L, doc.topline+1, doc.nlines);
    }

    else if (key == KEY_LEFT || key == KEY_SLEFT) {
      spot_brviewinit(env, &doc, -1, 1);
      wrefresh(spot_dispwin);
      win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
      spot_brfooter(doc.docname, doc.docnum+1, doc.ndocs, 1, doc.nlines);
    }

    else if (key == KEY_RIGHT || key == KEY_SRIGHT) {
      spot_brviewinit(env, &doc, 1, 1);
      wrefresh(spot_dispwin);
      win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
      spot_brfooter(doc.docname, doc.docnum+1, doc.ndocs, 1, doc.nlines);
    }

    else if (key == '+' || key == '-' || (key >= '0' && key <= '9')) {
      if (spot_brvalue(key, &mv, &relative)) {
	spot_brviewinit(env, &doc, mv, relative);
	wrefresh(spot_dispwin);
	win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
	spot_brfooter(doc.docname, doc.docnum+1, doc.ndocs, 1, doc.nlines);
      }
    }

    else if (key == CTRLW) {
      endwin();
      wrefresh(spot_cmdwin);
    }
  }

DONE:
  /* Clean up files and buffers allocated for browse */
  if (doc.db != NULL)
    drct_close(doc.db);

  if (doc.docbuf != NULL)
    free(doc.docbuf);

  if (doc.temp != NULL)
    fclose(doc.temp);

  if (doc.lineoffsets != NULL)
    free(doc.lineoffsets);

  if (doc.qryexe != NULL)
    prf_dealloc(&doc.qryexe);

  if (doc.highoffsets != NULL)
    free(doc.highoffsets);

  if (doc.highlengths != NULL)
    free(doc.highlengths);

  spot_scrrestore(0x1A, NULL, labelwin, NULL, footerwin, promptwin);
  *retdocnum = doc.docnum;
  return key;
}
