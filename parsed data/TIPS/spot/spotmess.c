/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_message					spoterrm.c

Function : Displays messages to user and prompts to continue.

Author : A. Fregly

Abstract : 

Calling Sequence : int key = spot_message(char *usermessage);

  usermessage	Message to appear in first of message display.
  key		Key pressed by user to exit message prompt.

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;
//extern char *sys_errlist[];

#if FIT_ANSI
int spot_message(char *usermessage)
#else
int spot_message(usermessage)
  char *usermessage;
#endif
{
  int key;

  overwrite(spot_dispwin, spot_msgwinsave);
  wattrset(spot_msgwin, spot_env.mennorm);
  win_fill(spot_msgwin, 0, 0, 0, 0, ' ');
  win_center(spot_msgwin, 0, usermessage);
  win_center(spot_msgwin, 1, "Hit any key to continue");
  overwrite(spot_msgwin, spot_dispwin);
  touchwin(spot_msgwin);
  wrefresh(spot_msgwin);
  for (key = spot_helpkey; key == spot_helpkey;) {
    if ((key = wgetch(spot_msgwin)) == CTRLW)
      endwin();
    else if (key == spot_helpkey)
      spot_help();
  }
  overwrite(spot_msgwinsave, spot_dispwin);
  wrefresh(spot_dispwin);
  return key;
}
