/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_setpath					spotsetp.c

Function : Provides user with path selector gadget.

Author : A. Fregly.

Abstract : This function provides a path selector gadget which allows a user
	to toggle between directly entering a new path, or to wander around 
	the directory tree, returning the last path selected by the user, or
	the start path if the user quits out of the gadget.

Calling Sequence : int retstat = spot_setpath(struct spot_s_env *env, 
    char *inpath, char *retpath);

  inpath	Initial path.
  retpath	Returned selected path.
  retstat	Returned status, a value of 0 indicates an error occurred or
		the user quit out of the path selector.

Notes: 

Change log : 
000	19-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <spot.h>

static int firsttime = 1;
static WINDOW *namewin, *savenamewin, *dispwin, *promptwin;
static struct men_s_menu *dispmenu;


#if FIT_ANSI
int spot_setpath(struct spot_s_env *env, char *inpath, char *retpath)
#else
int spot_setpath(env, inpath, retpath)
  struct spot_s_env *env;
  char *inpath, *retpath;
#endif
{

  char curpath[FIT_FULLFSPECLEN+1], fullpath[FIT_FULLFSPECLEN+1];
  char disppath[FIT_FULLFSPECLEN+1], errmessage[FIT_FULLFSPECLEN+64];
  char *(*dirlist);
  int menutype, key, optselected, ndirs, mincolwidth;
  unsigned len;
  int retstat, timedout, off, draw, imode;
  long retopt;
  struct stat statbuf;

  retstat = 1;
  fit_abspath(inpath, spot_curpath, curpath);
  if (inpath != retpath)
    strcpy(retpath, inpath);
  dirlist = NULL;
  dispmenu = NULL;

  if (firsttime) {
    /* Create windows and command menu */
    firsttime = 0;
    if ((namewin = newwin(8, COLS, 14, 0)) == NULL) return 0;
    if ((dispwin = newwin(5, COLS-2, 16, 1)) == NULL) return 0;
    win_dup(namewin, &savenamewin);
    win_dup(spot_promptwin, &promptwin);
    keypad(namewin, TRUE);
  }

  if (namewin == NULL || dispwin == NULL || savenamewin == NULL || 
      savenamewin == NULL) {
    spot_message("Unable to create path selector display");
    return 0;
  }

  /* Save the area of the screen being overwritten */
  overwrite(spot_dispwin, savenamewin);
  overwrite(spot_promptwin, promptwin);

  wattrset(namewin, env->mennorm);
  if (env->uselinedrawing)
    win_box(namewin);
  else
    win_fill(namewin, 1, 0, 0, 0, ' ');
  touchwin(namewin);
  wrefresh(namewin);

  if (env->uselinedrawing)
    wattrset(namewin, env->dispnorm);
  else
    wattrset(namewin, env->mennorm);

  win_fill(namewin, 1, 1, 1, COLS-2, ' ');
  wmove(namewin, 1, 1);
  waddstr(namewin,"Path: ");
  if (env->uselinedrawing)
    wattrset(namewin, env->mennorm);
  else
    wattrset(namewin, env->dispnorm);
  win_fill(namewin, 2, 1, 5, COLS-2, ' ');
  overwrite(namewin, dispwin);
  overwrite(namewin, spot_dispwin);
  wrefresh(namewin);

  /* Set up the menu type for the display window */
  menutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_VERTICALWRAP + 
    MEN_ATTR_HORIZONTALWRAP + MEN_ATTR_LEFTJUST;
  if (env->usepointer) menutype += MEN_ATTR_POINTER;

  dispmenu = NULL;

  while (1) {

    imode = 0;
    strcpy(fullpath, curpath);
    len = strlen(fullpath);
    if (len > 1) fullpath[len-1] = 0;
    off = min(strlen(fullpath), COLS - 7 - 2 - 1);
    draw = 1;
    if (env->uselinedrawing)
      wattrset(namewin, env->dispnorm);
    else
      wattrset(namewin, env->mennorm);

    spot_puthelpline(spot_fieldedithelp);

    while (1) {
      win_edit(namewin, fullpath, COLS - 7 - 2, &imode, draw, 0, 1, 7, &off,
        &key);
      draw = 0;
      switch (key) {
        case '\n' : case '\r' : case KEY_ENTER : case '\t' : case KEY_NEXT :
	case KEY_SNEXT :
          len = 0;
	  draw = 1;
          fit_strim(fullpath, &len);
	  fit_abspath(fullpath, curpath, fullpath);
          len = strlen(fullpath);
          if (len > 1) fullpath[len-1] = 0;
          if (stat(fullpath, &statbuf) || !(statbuf.st_mode & S_IFDIR)) {
            sprintf(errmessage,"Unable to access dirctory: %s", fullpath);
            spot_message(errmessage);
	    if (key == '\t' || key == KEY_NEXT || key == KEY_SNEXT)
	      goto OUT_ENTERLOOP;
          }
          else if (key == '\n' || key == '\r' || key == KEY_ENTER) {
            if (len > 1) fullpath[len-1] = '/';
            strcpy(retpath, fullpath);
            goto DONE;
          }
	  else {
	    if (len > 1) fullpath[len-1] = '/';
	    strcpy(curpath, fullpath);
	    goto OUT_ENTERLOOP;
	  }
          break;

        case KEY_CANCEL : case KEY_EXIT : case KEY_SCANCEL : case KEY_SEXIT :
#if FIT_LINUX
	  case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
	  retstat = 0;
          goto DONE;
          break;

        case KEY_HELP : case KEY_SHELP :
          break;

        case KEY_REFRESH : case CTRLW :
          endwin();
          wrefresh(namewin);

        default :
	  if (key == spot_exitkey || key == spot_quitkey) {
	    retstat = 0;
	    goto DONE;
	  }
	  else if (key == spot_helpkey)
	    spot_help();
      }
    }

OUT_ENTERLOOP:
    spot_puthelpline(spot_selectorhelp);
    for (optselected = 0; !optselected;) {
      /* Display current path in selector path window */
      if (env->uselinedrawing)
        wattrset(namewin, env->dispnorm);
      else
        wattrset(namewin, env->mennorm);
      win_fill(namewin, 1, 7, 1, COLS-(6+2), ' ');
      strcpy(disppath, curpath);
      len = strlen(disppath);
      if (len > 1) disppath[len-1] = 0;
      mvwaddstr(namewin, 1, 7, disppath);
/******
      wattrset(dispwin, env->dispnorm);
      overwrite(dispwin, namewin);
*******/
      overwrite(namewin, spot_dispwin);
      wrefresh(namewin);

      /* Build a new file display for the current path */
      if ((dirlist = fit_dirlist(curpath, &ndirs)) != NULL) {
        mincolwidth = (ndirs < 4) * 14;
	if (env->uselinedrawing)
          dispmenu = men_init(dispwin, menutype, NULL, dirlist, env->mennorm,
            env->menhigh, env->mensel, env->menhot, ACS_RARROW, ACS_DIAMOND,
            1, mincolwidth);
	else
          dispmenu = men_init(dispwin, menutype, NULL, dirlist, env->dispnorm,
            env->disphigh, env->dispsel, env->disphot, ACS_RARROW, ACS_DIAMOND,
            1, mincolwidth);
        if (dispmenu != NULL) {
          men_draw(dispmenu);
          men_put(dispmenu, spot_dispwin);
        }
        else {
	  spot_errmessage("Error creating path display");
	  retstat = 0;
	  goto DONE;
        }
      }
      else {
        spot_errmessage("Error reading directory");
	retstat = 0;
        goto DONE;
      }

      while (!optselected) {
        key = men_select(dispmenu, 0, &retopt, &optselected, &timedout);
        if (optselected) {
	  if (!strcmp(dispmenu->options[retopt].val,".")) {
            /* Selected current directory, consider this as an exit request */
            strcpy(retpath, curpath);
            goto DONE;
	  }
	  else {
	    optselected = 0;
	    break;
	  }
        }
        else {
          if (key == spot_exitkey || key == spot_quitkey || key == KEY_EXIT || 
	      key == KEY_SEXIT || key == KEY_CANCEL || key == KEY_SCANCEL ||
	      key == ('z' - 'a' + 1) || key == ('d' - 'a' + 1)) {
	    retstat = 0;
            goto DONE;
	  }
	  else if (key == '\t' || key == KEY_NEXT || key == KEY_SNEXT) {
	    optselected = 1;
	    break; 
	  }
	  else if (key == spot_helpkey)
	    spot_help();
        }
      }
/******
      if (!optselected) {
*******/
        /* Blank out the display window */
/******
        win_fill(namewin, 2, 1, 5, COLS-2, ' ');
******/

      /* Expand the path selected by the user */
      fit_abspath(dispmenu->options[dispmenu->curopt].val, curpath, fullpath);
      strcpy(curpath, fullpath);

      /* Free previous directory list and file list */
      fit_delfilelist(&dirlist);
      men_dealloc(&dispmenu);
    }
  }

DONE:
  /* Free memory allocated for the display window */
  if (dirlist != NULL) fit_delfilelist(&dirlist);
  if (dispmenu != NULL) men_dealloc(&dispmenu); 

  /* Restore screen */
  overwrite(savenamewin, spot_dispwin);
  overwrite(promptwin, spot_promptwin);
  wrefresh(spot_dispwin);
  wrefresh(spot_promptwin);
  return retstat;
}
