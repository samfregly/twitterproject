/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_getline					spotgetl.c

Function : Prepares next output line during spot document retrieval process.

Author : A. Fregly

Abstract : This function is used to create filtered lines during spot document
	retrieval. A filtered line uses the supplied margins and wrap settings
	and has all control characters and field delimiters stripped, and 
	expands tabs to the appropriate number of spaces to reach the next 
	tab stop.

Calling Sequence : int stat = spot_getline(char *docbuf, int *bufoff, 
  int nbytes, int margin, int wrapcol, int fielddelim, char outline[], 
  int *outlen);

  docbuf	Buffer containing input text being processed.
  bufoff	Offset at which to scan for next line. Bufoff is updated to
		point at location for next scan on return.
  nbytes	Number of bytes in docbuf.
  margin	Left margin.
  wrapcol	Column at which to do wrap.
  fielddelim	Field delimiter character, or -1 if no field delimiters.
  outline	Output buffer, should be at least wrapcol+2 in length.
  outlen	Returned length of data put into outline, including terminating
		line feed.

Notes: 
001	Field delimiter removal should be removed, but all other software
	will need to be modified to handle embedded field delims correctly.

Change log : 
000	09-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
int spot_getline(char *docbuf, int *bufoff, int nbytes, int margin, int wrapcol,
  int fielddelim, char outline[], int *outlen)
#else
int spot_getline(docbuf, bufoff, nbytes, margin, wrapcol, fielddelim, outline,
  outlen)
  char *docbuf;
  int *bufoff, nbytes, margin, wrapcol, fielddelim;
  char outline[];
  int *outlen;
#endif
{

  unsigned char *eptr, *cptr, *spaceptr;
  int sidx, begoff;

  cptr = (unsigned char *) docbuf + *bufoff;
  eptr = (unsigned char *) docbuf + nbytes;
  begoff = *bufoff;

  *outlen = 0;

  if (cptr >= eptr || margin >= wrapcol || margin < 0) return 0;

  if (margin) {
    for (; *outlen < margin; ++*outlen)
      outline[*outlen] = ' ';
  }

  for (spaceptr = NULL, sidx = -1; cptr < eptr && *outlen < wrapcol; 
      ++cptr) {

    if (*cptr > ' ' && *cptr <= '~')
      outline[(*outlen)++] = *cptr; 

    else if (*cptr == '\n' || *cptr == '\f') {
      ++cptr;
      outline[(*outlen)++] = '\n';
      break;
    }

    else if (fielddelim >= 0 && *cptr == fielddelim) {
      cptr += 1;
      spaceptr = cptr;
      sidx = *outlen;
      outline[(*outlen)++] = ' '; 
    }

    else if (*cptr == '\t') {
      for (outline[(*outlen)++] = ' ';
	(*outlen - margin) % 8 && *outlen < wrapcol; ++*outlen) 
	outline[*outlen] = ' ';
      sidx = *outlen - 1;
      spaceptr = cptr;
    }

    else if (*cptr == ' ') {
      sidx = *outlen;
      outline[(*outlen)++] = *cptr;
      spaceptr = cptr;
    }

    else if (*outlen > 0 && outline[*outlen-1] != ' ') {
      sidx = *outlen;
      outline[(*outlen)++] = ' '; 
      spaceptr = cptr;
    }
  }

  if (*outlen >= wrapcol && outline[*outlen-1] != '\n' && 
      cptr < eptr && *cptr != '\n' && *cptr != '\f' &&
      (*cptr != '\r' || cptr >= eptr - 1 || *(cptr+1) != '\n' ||
      *(cptr+1) != '\f')) { 

    /* See if break is in the middle of a word */
    if (spaceptr != NULL && outline[*outlen-1] != ' ' && cptr < eptr && 
        *(cptr - 1) > ' ' && *(cptr -1) < '~') {

      /* Found the break, reset the input pointer */ 
      if (sidx >= 0) {
        *outlen = sidx + 1;
	cptr = spaceptr + 1;
      }
    }
    else {
      /* Break was at the end of a word, skip over blanks until we find
         the next word */
      while (cptr < eptr && *cptr == ' ')
	++cptr;
    }
    outline[(*outlen)++] = '\n';
  }

  *bufoff = cptr - (unsigned char *) docbuf;
  outline[*outlen] = 0;
  return (*outlen || *bufoff != begoff);
}
