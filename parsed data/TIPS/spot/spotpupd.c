/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_pupdate					spotpupd.c

Function : Updates profiling of specified stream so that latest queries in
  stream library for stream are used.

Author : A. Fregly

Abstract : 

Calling Sequence : spot_pupdate(char *streamname);

  streamname	Stream name for which profiling is to be updated.

Notes: 

Change log : 
000	14-OCT-91  AMF	Created.
001	11-SEP-93  AMF	Fixed call top tips_sendmsg.
******************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <ctype.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

extern  int errno;

#if FIT_ANSI
void spot_pupdate(char *streamname)
#else
void spot_pupdate(streamname)
  char *streamname;
#endif
{
  struct hostent *hostp;
  struct servent *servp;
  struct sockaddr_in server;
  static struct servent s;
  int sock;
  char buf[16];
  int bytesread;
  char sname[FIT_NAMELEN+1],tempbuf[FIT_NAMELEN+16], dummy[FIT_FULLFSPECLEN+1];
  char *servername, *serverport,serverport_b[10];

  fit_prsfspec(streamname,dummy,dummy,dummy,sname,dummy,dummy);
  strcpy(tempbuf,sname);
  strcat(tempbuf,"_server");
  servername = getenv(tempbuf);
  if (!servername) {
    servername="localhost";
  }
  //printf("Server name: %s\n",servername);
  strcpy(tempbuf,sname);
  strcat(tempbuf,"_port");
  serverport = getenv(tempbuf);
  if (!serverport) {
    serverport=TIPS_SPOTTER_DEFAULT_PORT;
  }
  //printf("Server port: %s\n",serverport);

  if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    return;
  }
  if (isdigit(*serverport)) {
    servp = &s;
    s.s_port = htons((u_short)atoi(serverport));
  } else if ((servp = getservbyname(serverport, "tcp")) == 0) {
    return;
  }
  if ((hostp = gethostbyname(servername)) == 0) {
    return;
  }

  memset((void *) &server, 0, sizeof server);
  server.sin_family = AF_INET;
  memcpy((void *) &server.sin_addr, hostp->h_addr, hostp->h_length);
  server.sin_port = servp->s_port;

  if (connect(sock, (struct sockaddr *)&server, sizeof server) < 0) {
  //printf("Error connecting to socket\n");
    (void) close(sock);
    return;
  }
  strcpy(buf,"reload");
  if (write(sock, buf, strlen(buf)) < 0) {
  //printf("Error writing to socket\n");
    (void) close(sock);
    return;
  }
  bytesread = read(sock, buf, sizeof buf);
  //printf("Read response from spotter, bytesread: %d, buf: %s\n",bytesread,buf);
  (void) close(sock);
  return;
}

