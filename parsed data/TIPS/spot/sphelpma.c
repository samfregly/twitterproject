/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : sphelp_makedisplay				sphelpma.c

Function : Makes sphelp windows and menus.

Author : A. Fregly

Abstract : This function will create all windows used by sphelp, and
	also initialize all menus.

Calling Sequence : int stat = sphelp_makedisplay(struct spot_s_env *env,
  struct drct_s_file *help);

  env	Spot environment data.
  help	Handle for result file containing document references for help
	entries.
  stat	Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	04-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>
#include <sphelp.h>

extern long *sphelp_helpidx;


#if FIT_ANSI
int sphelp_makedisplay(struct spot_s_env *env, struct drct_s_file *help)
#else
int sphelp_makedisplay(env, help)
  struct spot_s_env *env;
  struct drct_s_file *help;
#endif
{

#define BUFLEN (TIPS_DESCRIPTORMAX * 2)

  int menutype, i, count, nbytes, docdescriptor_l;
  char *inputline, *(*dispmenu), *endheader, buf[BUFLEN+1];
  char docdescriptor[TIPS_DESCRIPTORMAX+1], docname[TIPS_DESCRIPTORMAX+1];
  char keyfld[TIPS_DESCRIPTORMAX+1], llabel[SPOT_LABELWINLEN+1];
  long doclen;
  FILE *tmp;

  spot_dispmenu = NULL;

  /* Make windows */
  /* Label window */
  spot_labelwin = newwin(1, SPOT_LABELWINLEN+1, 0, COLS - (SPOT_LABELWINLEN+1));
  wattrset(spot_labelwin, env->mennorm);
  scrollok(spot_labelwin, FALSE);
  mvwaddstr(spot_labelwin, 0, 0, "|");
  strcpy(llabel,"Help");
  fit_scenter(llabel, SPOT_LABELWINLEN);
  mvwaddstr(spot_labelwin, 0, 1, llabel);
  wrefresh(spot_labelwin);

  /* Command window */
  spot_cmdwin = newwin(1, COLS-(SPOT_LABELWINLEN+1), 0, 0);
  scrollok(spot_cmdwin, FALSE);
  wattrset(spot_cmdwin, env->mennorm);
  win_fill(spot_cmdwin, 0, 0, 0, 0, ' ');
  wrefresh(spot_cmdwin);

  /* Display window */
  spot_dispwin = newwin(LINES-3, COLS, 1, 0);
  spot_dispheight = LINES - 3;
  spot_dispwidth = COLS;
#if !FIT_COHERENT
  spot_hardscroll = has_il();
#else
  spot_hardscroll = 1;
#endif
  if (spot_hardscroll)
    idlok(spot_dispwin, TRUE);
  scrollok(spot_dispwin, FALSE);
  wattrset(spot_dispwin, env->dispnorm);
  win_fill(spot_dispwin, 0, 0, 0, 0, ' ');
  wrefresh(spot_dispwin);

  /* Footer window */
  spot_footerwin = newwin(1, COLS, LINES-2, 0);
  wattrset(spot_footerwin, env->mennorm);
  scrollok(spot_footerwin, FALSE);
  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');
  wrefresh(spot_footerwin);

  /* Prompt window */
  spot_promptwin = newwin(1, COLS-1, LINES-1, 0);
  wattrset(spot_promptwin, env->dispnorm);
  win_fill(spot_promptwin, 0, 0, 0, 0, ' ');
  scrollok(spot_promptwin, FALSE);
  keypad(spot_promptwin, TRUE);
  wrefresh(spot_promptwin);

  /* Message window and save window for it */
  spot_msgwin = newwin(2, COLS, 2, 0);
  win_dup(spot_msgwin, &spot_msgwinsave);
  scrollok(spot_msgwin, FALSE);
  keypad(spot_msgwin, TRUE);
  wattrset(spot_msgwin, env->disphigh);
  win_fill(spot_msgwin, 0, 0, 0, 0, ' ');

  /* Errow window and save window for it */
  spot_errwin = newwin(3, COLS, 2, 0);
  win_dup(spot_errwin, &spot_errwinsave);
  scrollok(spot_errwin, FALSE);
  keypad(spot_errwin, TRUE);
  wattrset(spot_errwin, env->disphigh);
  win_fill(spot_errwin, 0, 0, 0, 0, ' ');

  /* Make menu */
  if ((tmp = tmpfile()) == NULL)
    return 0;

  drct_rewind(help);
  for (count = 0; nbytes = drct_rdseq(help, buf, BUFLEN, &doclen);) {
    if (nbytes < doclen) drct_cancelio(help);
    buf[nbytes] = 0;
    tips_extractdescr(buf, docdescriptor, &docdescriptor_l);
    tips_parsedsc(docdescriptor, docname, &doclen, keyfld);
    if (*keyfld) {
      if (strspn(keyfld, " ") != strlen(keyfld)) {
	fprintf(tmp, "%ld %s\n", help->currec, keyfld);
	++count;
      }
    }
  }

  if (count) {
    rewind(tmp);
    inputline = (char *) calloc(COLS+1, sizeof(*inputline));
    dispmenu = (char *(*)) calloc(count + 2, sizeof(*dispmenu));
    sphelp_helpidx = (long *) calloc(count + 2, sizeof(*sphelp_helpidx));
    if (dispmenu != NULL && inputline != NULL && sphelp_helpidx != NULL) {
      for (i=0; i < count; ++i) {
	fscanf(tmp, "%ld ", &sphelp_helpidx[i]);
        fgets(inputline, COLS, tmp);
	if ((endheader = strchr(inputline, '\n')) != NULL)
	  *endheader = 0;
	dispmenu[i] = (char *) calloc(strlen(inputline) + 1, 1);
        if (dispmenu[i] != NULL)
	  strcpy(dispmenu[i], inputline);
      }
      dispmenu[i] = "";

      menutype = 0;
      if (env->usepointer)
        menutype += MEN_ATTR_POINTER;
      else
        menutype += MEN_ATTR_HIGHFIRST;

      spot_dispmenu = men_init(spot_dispwin, menutype, NULL, dispmenu,
          env->dispnorm, env->disphigh, env->dispsel, env->disphot,
          ACS_RARROW, ACS_DIAMOND, 1, 0);
    }
    if (inputline != NULL) free(inputline);
  }
  fclose(tmp);

  return 1;
}
