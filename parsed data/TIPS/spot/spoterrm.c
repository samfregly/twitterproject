/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_errmessage					spoterrm.c

Function : Displays system error messages to user and prompts to continue.

Author : A. Fregly

Abstract : 

Calling Sequence : spot_errmessage(char *usermessage);

  usermessage	Message to appear in first line of error message display.
		Second line will contain system error message for most
		recent system error.

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;
//extern char *sys_errlist[];

#if FIT_ANSI
void spot_errmessage(char *usermessage)
#else
void spot_errmessage(usermessage)
  char *usermessage;
#endif
{
  int key;

  overwrite(spot_dispwin, spot_errwinsave);
  wattrset(spot_errwin, spot_env.mennorm);
  win_fill(spot_errwin, 0, 0, 0, 0, ' ');
  win_center(spot_errwin, 0, usermessage);
  win_center(spot_errwin, 1, (char *) sys_errlist[errno]);
  win_center(spot_errwin, 2, "Hit any key to continue");
  overwrite(spot_errwin, spot_dispwin);
  touchwin(spot_errwin);
  wrefresh(spot_errwin);
/*****
  wrefresh(spot_dispwin);
******/
  for (key = spot_helpkey; key == spot_helpkey;) {
    if ((key = wgetch(spot_errwin)) == CTRLW)
      endwin();
    else if (key == spot_helpkey)
      spot_help();
  }
  overwrite(spot_errwinsave, spot_dispwin);
  wrefresh(spot_dispwin);
}
