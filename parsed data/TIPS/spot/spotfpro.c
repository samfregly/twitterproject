/*******************************************************************************

                       Copyright 1997, SeekWare Inc.
    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_fprompt					spotfpro.c

Function : Prompts use to enter name of a file to be created.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_fprompt(struct spot_s_env *env, int opts,
	char *prompt, char *defname, char *defext, char *defpath, 
	char *retfspec, char *retpath, int *ftype);

  env		Spot environment structure.
  opts		Options bit mask. Bits enable following options:
			0 - File must not already exist.
			1 - Do not do any checking on entered file name. The
			    returned file name will not be expanded, the
			    default extension will be ignored, and the
			    ftype value will not be set.
  prompt	Prompt.
  defname	Default value to be initially place in user editable field.
  defext	Default file name extension.
  defpath	Default path.
  retfspec	Returned file name. If checking on entered file name is enabled,
		retfspec will be a fully expanded file name, otherwise it will
		be the value entered by the user.
  retpath	Returned path.
  ftype		Non-zero return value indicates files exists. The file type
		bits defined in <sys/stat.h> may  be used to determine the
		file type.
  stat		Returned status. A value of 0 indicates the user canceled out
		of the requestor.

Notes: 

Change log : 
000	22-FEB-92  AMF	Created.
001	11-SEP-93  AMF	Make friendly to strict compilers.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fitsydef.h>
#include <spot.h>

static int firsttime=1;
static WINDOW *outerwin, *saveouterwin, *promptwin;
static int normattr, rvsattr;

#if FIT_ANSI
int spot_fprompt(struct spot_s_env *env, int opts, char *promptstr, 
  char *defname, char *defext, char *defpath, char *retfspec, char *retpath,
  int *ftype)
#else
int spot_fprompt(env, opts, promptstr, defname, defext, defpath, retfspec, 
    retpath, ftype)
  struct spot_s_env *env;
  int opts;
  char *promptstr, *defname, *defext, *defpath, *retfspec, *retpath;
  int *ftype;
#endif
{

  char curpath[FIT_FULLFSPECLEN+1], fspec[FIT_FULLFSPECLEN+1];
  char message[FIT_FULLFSPECLEN+64];
  int imode, draw, off, retstat, key, exists;
  unsigned len;
  struct stat statbuf;

  *ftype = 0;
  retstat = 0;

  if (firsttime) {
    /* Create windows and command menu */
    firsttime = 0;

    if (env->uselinedrawing) {
      normattr = env->mennorm;
      rvsattr = env->mennorm;
    }
    else {
      normattr = env->dispnorm;
      rvsattr = env->mennorm;
    }

    if ((outerwin = newwin(5, COLS, 5, 0)) == NULL) return retstat;
    keypad(outerwin, TRUE);
    win_dup(outerwin, &saveouterwin);
    win_dup(spot_promptwin, &promptwin);

    wattrset(outerwin, rvsattr);
    win_fill(outerwin, 0, 0, 0, 0, ' ');
    if (env->uselinedrawing) 
      win_box(outerwin);
    wattrset(outerwin, env->mennorm);
  }

  /* Save the area of the screen being overwritten */
  overwrite(spot_dispwin, saveouterwin);
  overwrite(spot_promptwin, promptwin);

  wattrset(outerwin, normattr);
  win_fill(outerwin, 1, 1, 3, COLS - 2, ' ');
  touchwin(outerwin);
  wrefresh(outerwin);
  if (promptstr != NULL) 
    mvwaddstr(outerwin, 1, 1 + ((COLS - 2) - strlen(promptstr)) / 2, promptstr);
  mvwaddstr(outerwin, 2, 2, "Name:");
  mvwaddstr(outerwin, 3, 2, "Path:");

  if (defpath != NULL)
    strcpy(curpath, defpath);
  else
    getcwd(curpath, FIT_FULLFSPECLEN);

  len = strlen(curpath);
  if (len > 1 && curpath[len-1] == '/') curpath[len-1] = 0;
  mvwaddstr(outerwin, 3, 8, curpath);
  wrefresh(outerwin);

  if (defname != NULL)
    strcpy(retfspec, defname);
  else
    retfspec[0] = 0;

  imode = 0;
  draw = 1;
  off = 0;
  spot_puthelpline(spot_fieldedithelp);
  overwrite(outerwin, spot_dispwin);
  wrefresh(spot_dispwin);

  while (1) {
    win_edit(outerwin, retfspec,  COLS - 7 - 2, &imode, draw, 0, 2, 8, &off, 
	&key);
    draw = 0;

    switch (key) {
      case '\n' : case '\r' : case KEY_ENTER :
        len = 0;
        fit_strim(retfspec, &len);
	if (!(opts && (1 << 1))) {
	  if (!len) goto DONE; 
	  fit_expfspec(retfspec, defext, curpath, fspec);
	  exists = !stat(fspec, &statbuf);
	  if (!exists || !(opts && 1 << 0)) {
	    strcpy(retfspec, fspec);
	    strcpy(retpath, curpath);
	    *ftype = statbuf.st_mode * exists;
	    retstat = 1;
	    goto DONE;
	  }
	  else {
	    strcpy(message, "File exists: ");
	    strcat(message, fspec);
  	    overwrite(outerwin, spot_dispwin);
	    spot_message(message);
	  }
	}
	else {
	  retstat = 1;
	  strcpy(retpath, curpath);
	  goto DONE;
	}
	break;

      case '\t': case KEY_NEXT : case KEY_SNEXT : case KEY_DOWN :
      case KEY_NPAGE :
  	overwrite(outerwin, spot_dispwin);
        spot_setpath(env, curpath, curpath);
        len = strlen(curpath);
	if (len > 1 && curpath[len-1] == '/') curpath[len-1] = 0;
	win_fill(outerwin, 3, 8, 1, COLS - 2 - 8, ' ');
  	mvwaddstr(outerwin, 3, 8, curpath);
	touchwin(outerwin);
	wrefresh(outerwin);
	break;

      case KEY_CANCEL : case KEY_EXIT : case KEY_SCANCEL :
      case KEY_SEXIT :
#if FIT_LINUX
      case ('z' - 'a' + 1) : case ('d' - 'a' + 1) :
#endif
        goto DONE;
        break;

      case KEY_HELP : case KEY_SHELP :
  	overwrite(outerwin, spot_dispwin);
        spot_help();
        break;

      case KEY_REFRESH : case CTRLW :
        endwin();
        wrefresh(outerwin);

      default :
        if (key == spot_exitkey || key == spot_quitkey)
          goto DONE;
        else if (key == spot_helpkey) {
  	  overwrite(outerwin, spot_dispwin);
          spot_help();
	}
    }
  }

DONE:
  /* Restore screen */
  overwrite(saveouterwin, spot_dispwin);
  overwrite(promptwin, spot_promptwin);
  wrefresh(spot_dispwin);
  wrefresh(spot_promptwin);
 
  return retstat;
}
