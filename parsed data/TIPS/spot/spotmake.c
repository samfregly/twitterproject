/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_makedisplay					spotmake.c

Function : Makes spot windows and menus.

Author : A. Fregly

Abstract : This function will create all windows used by spot, and
	also initialize all menus except the display menu.

Calling Sequence : int stat = spot_makedisplay(struct spot_s_env *env);

  env	Spot environment data.
  stat	Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	04-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

/* Define Menus available from the main screen */

/* The main command menu */
static char *cmdopts[] = {"File", "View", "Search", "Action", ""};

/* The File menu */
static char *fileopts[] = {"Open","New","Browse","Pick","Copy","Delete",
  "Print","Path", "Shell", "Exit",""};

/* The View menu */
static char *viewopts[] = {"Results","Streams","Files","Archives", "Queries", 
  "Stream Detail", "Summary", "Monitor", "Swap", ""};

/* Summary pulldown for view menu */
static char *summaryopts[] = {"Names", "Line #", "Field", ""};

/* The Search menu */
static char *searchopts[] = {"New Query","Old Queries",""};

/* Command source selection menu */
static char *srcopts[] = {"Current", "Selected", ""};

/* Summary source seleciton menu */
static char *sumsrcopts[] = {"Selected", "All", ""};

/* Stream detail delete prompt for disposition of queries and result files */
static char *detailopts[] = {"Query File(s)", "Result File(s)", ""};

/* Append/Replace options for copies and searches */
static char *addopts[] = {"Replace", "Append", ""};

/* Copy destination menu */
static char *copyopts[] = {"File", "Results", "Archive", "Directory", ""};

/* Copy destination menu when a stream is an allowable destination */
static char *strcopyopts[] = {"File", "Results", "Archive", "Directory", 
  "Stream", ""};

/* Summary delete selection menu */
static char *sumdelopts[] = {"References only", "Documents and References", ""};

/* Maximum length of an actions menu option */
#define MAXACTIONLEN 24

#if FIT_ANSI
int spot_makedisplay(struct spot_s_env *env)
#else
int spot_makedisplay(env)
  struct spot_s_env *env;
#endif
{

  int cmdmenutype, pulldownmenutype, selectmenutype;
  int maxoptlen, optlen, i, j;

  /* Set menu operation attributes */
  cmdmenutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_HORIZONTALWRAP;
  pulldownmenutype = (env->uselinedrawing) ? MEN_ATTR_BOXED : 0; 
  if (env->usepointer) {
    cmdmenutype += MEN_ATTR_POINTER;
    pulldownmenutype += MEN_ATTR_POINTER;
  }
  selectmenutype = pulldownmenutype + MEN_ATTR_MULTSEL + 
    (env->usecheckmarks != 0) * MEN_ATTR_CHECK;
  if (env->usehotkeys) {
    cmdmenutype += (MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT);
    pulldownmenutype += (MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT);
  }

  /* Make windows */
  /* Label window */
  spot_labelwin = newwin(1, SPOT_LABELWINLEN+1, 0, COLS - (SPOT_LABELWINLEN+1));
  wattrset(spot_labelwin, env->mennorm);
  scrollok(spot_labelwin, FALSE);
  mvwaddstr(spot_labelwin, 0, 0, "|");
  spot_label("Main");
  wrefresh(spot_labelwin);

  /* Command window and menu */
  spot_cmdwin = newwin(1, COLS-(SPOT_LABELWINLEN+1), 0, 0);
  scrollok(spot_cmdwin, FALSE);
  spot_cmdmenu = men_init(spot_cmdwin, cmdmenutype, NULL, cmdopts, 
    env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW, 
    ACS_DIAMOND, 1, 0);
  wattrset(spot_cmdwin, env->mennorm);
  if (!win_fill(spot_cmdwin, 0, 0, 0, 0, ' ')) {
    ;
  }
  wrefresh(spot_cmdwin);

  /* Pull down, File menu */
  if (!(spot_filewin = newwin(10 + 2 * env->uselinedrawing, 8 + env->usepointer + 
    2 * env->uselinedrawing, 1, spot_cmdmenu->scol))) {
    ;
  }
  scrollok(spot_filewin, FALSE);
  spot_filemenu = men_init(spot_filewin, pulldownmenutype, NULL, fileopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);
  men_overlay(spot_filemenu, spot_cmdmenu, 0, 0);

  /* Pull down, View menu */
  spot_viewwin = newwin(9 + 2 * env->uselinedrawing, 15 + env->usepointer +
    2 * env->uselinedrawing, 1, 
    spot_cmdmenu->scol + spot_cmdmenu->colwidth);
  scrollok(spot_viewwin, FALSE);
  spot_viewmenu = men_init(spot_viewwin, pulldownmenutype, NULL, viewopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);
  men_overlay(spot_viewmenu, spot_cmdmenu, 1, 0);

  /* Pull down, summary options window */
  spot_brsummaryoptwin = newwin(4 + (env->uselinedrawing != 0), 8 +
    2 * env->uselinedrawing + env->usepointer, 0, 0);
  scrollok(spot_brsummaryoptwin, FALSE);
  spot_brsummaryoptmenu = men_init(spot_brsummaryoptwin, pulldownmenutype,
      "Content", summaryopts, env->mennorm, env->menhigh, env->mensel,
      env->menhot, ACS_RARROW, ACS_DIAMOND, 1, 0);
  men_overlay(spot_brsummaryoptmenu, spot_viewmenu, 6, 1);

  /* Pull down, Search menu */
  spot_searchwin = newwin(2 + 2 * env->uselinedrawing, 13 + env->usepointer +
    2 * env->uselinedrawing, 1, 
    spot_cmdmenu->scol + spot_cmdmenu->colwidth * 3);
  scrollok(spot_searchwin, FALSE);
  spot_searchmenu = men_init(spot_searchwin, pulldownmenutype, NULL, searchopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);
  men_overlay(spot_searchmenu, spot_cmdmenu, 2, 0);

  /* Pull down, Actions menu */
  for (i=0; i <= SPOT_MAXVIEW; ++i) {
    for (j=0, maxoptlen = 0; j < spot_actions[i].nopts; ++j)
      if ((optlen = strlen(spot_actions[i].menuitems[j])) > MAXACTIONLEN) {
	spot_actions[i].menuitems[j][MAXACTIONLEN] = 0;
	maxoptlen = MAXACTIONLEN;
      }
      else
	maxoptlen = max(maxoptlen, optlen);
    if (maxoptlen) {
      spot_actions[i].win = newwin(min(LINES - 3 - 2 * env->uselinedrawing,
	spot_actions[i].nopts + 2 * env->uselinedrawing), 
	maxoptlen + 2 + env->usepointer + 2 * env->uselinedrawing, 1, 
	spot_cmdmenu->scol + spot_cmdmenu->colwidth * 5);
      scrollok(spot_actions[i].win, FALSE);
      spot_actions[i].menu = men_init(spot_actions[i].win, pulldownmenutype, 
	NULL, spot_actions[i].menuitems, env->mennorm, env->menhigh, 
	env->mensel, env->menhot, ACS_RARROW, ACS_DIAMOND, 1, 0);
      men_overlay(spot_actions[i].menu, spot_cmdmenu, 3, 0);
    }
  }

  /* Pull down, copy destination menu */
  spot_copywin = newwin(5 + (env->uselinedrawing != 0), 13 + env->usepointer +
    2 * env->uselinedrawing, 0, 0);
  scrollok(spot_copywin, FALSE);
  spot_copymenu = men_init(spot_copywin, pulldownmenutype, "Destination",
	copyopts, env->mennorm, env->menhigh, env->mensel, env->menhot, 
	ACS_RARROW, ACS_DIAMOND, 1, 0);
  men_overlay(spot_copymenu, spot_filemenu, 4, 1);

  /* Pull down, copy destination menu when stream is a possbile destination */
  spot_strcopywin = newwin(6 + (env->uselinedrawing != 0), 
    13 + env->usepointer + 2 * env->uselinedrawing, 0, 0);
  scrollok(spot_strcopywin, FALSE);
  spot_strcopymenu = men_init(spot_strcopywin, pulldownmenutype, "Destination",
	strcopyopts, env->mennorm, env->menhigh, env->mensel, env->menhot, 
	ACS_RARROW, ACS_DIAMOND, 1, 0);
  men_overlay(spot_strcopymenu, spot_filemenu, 4, 1);

  /* Source selection menu */
  spot_srcwin = newwin(3 + (env->uselinedrawing != 0), 10 + env->usepointer +
    2 * env->uselinedrawing, 1, 1);
  scrollok(spot_srcwin, FALSE);
  spot_srcmenu = men_init(spot_srcwin, pulldownmenutype, "Source", srcopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);

  /* Source selection menu for summary displays */
  spot_sumsrcwin = newwin(3 + (env->uselinedrawing != 0), 10 + env->usepointer +
    2 * env->uselinedrawing, 1, 1);
  scrollok(spot_sumsrcwin, FALSE);
  spot_sumsrcmenu = men_init(spot_srcwin, pulldownmenutype, "Source",
	sumsrcopts, env->mennorm, env->menhigh, env->mensel, env->menhot,
	ACS_RARROW, ACS_DIAMOND, 1, 0);
  men_overlay(spot_sumsrcmenu, spot_viewmenu, 6, 1);

  /* Pull down, summary delete target window */
  spot_sumdelwin = newwin(3 + (env->uselinedrawing != 0),
    26 + env->usepointer + 2 * env->uselinedrawing, 0, 0);
  scrollok(spot_sumdelwin, FALSE);
  spot_sumdelmenu = men_init(spot_sumdelwin, pulldownmenutype,
      "Items to Delete", sumdelopts, env->mennorm, env->menhigh,
      env->mensel, env->menhot, ACS_RARROW, ACS_DIAMOND, 1, 0);

  /* Detail delete options window */
  spot_detailwin = newwin(3 + (env->uselinedrawing != 0), 17 + env->usepointer +
    2 * env->uselinedrawing, 1, 1);
  scrollok(spot_detailwin, FALSE);
  spot_detailmenu = men_init(spot_detailwin, selectmenutype, "Items To Delete",
	detailopts, env->popupnorm, env->popuphigh, env->popupsel, 
	env->popuphot, ACS_RARROW, ACS_DIAMOND, 1, 0);

  spot_addwin = newwin(3 + (env->uselinedrawing != 0), 9 + env->usepointer +
    2 * env->uselinedrawing, 1, 1);
  scrollok(spot_addwin, FALSE);
  spot_addmenu = men_init(spot_addwin, pulldownmenutype, "Method", addopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);

  spot_dispwin = newwin(LINES-3, COLS, 1, 0);
  leaveok(spot_dispwin, TRUE);
  spot_dispheight = LINES - 3;
  spot_dispwidth = COLS;
#if FIT_SCO || FIT_SUN
  spot_hardscroll = has_il();
#else
  spot_hardscroll = 1;
#endif
  if (spot_hardscroll)
    idlok(spot_dispwin, TRUE);
  scrollok(spot_dispwin, FALSE);
  wattrset(spot_dispwin, env->dispnorm);
  if (!win_fill(spot_dispwin, 0, 0, 0, 0, ' ')){
    ;
  }
  wrefresh(spot_dispwin);

  spot_footerwin = newwin(1, COLS, LINES-2, 0);
  wattrset(spot_footerwin, env->mennorm);
  scrollok(spot_footerwin, FALSE);
  if (!win_fill(spot_footerwin, 0, 0, 0, 0, ' ')){
    ;
  }
  wrefresh(spot_footerwin);

  spot_promptwin = newwin(1, COLS-1, LINES-1, 0);
  wattrset(spot_promptwin, env->dispnorm);
  if (win_fill(spot_promptwin, 0, 0, 0, 0, ' ')){
    ;
  }
  scrollok(spot_promptwin, FALSE);
  keypad(spot_promptwin, TRUE);
  wrefresh(spot_promptwin);
  win_dup(spot_promptwin, &spot_promptwinsave);

  /* Message window and save window for it */
  spot_msgwin = newwin(2, COLS, 2, 0);
  win_dup(spot_msgwin, &spot_msgwinsave);
  scrollok(spot_msgwin, FALSE);
  keypad(spot_msgwin, TRUE);
  wattrset(spot_msgwin, env->disphigh);
  if (!win_fill(spot_msgwin, 0, 0, 0, 0, ' ')){
    ;
  }

  /* Errow window and save window for it */
  spot_errwin = newwin(3, COLS, 2, 0);
  win_dup(spot_errwin, &spot_errwinsave);
  scrollok(spot_errwin, FALSE);
  keypad(spot_errwin, TRUE);
  wattrset(spot_errwin, env->disphigh);
  if (!win_fill(spot_errwin, 0, 0, 0, 0, ' ')){
    ;
  }

  return 1;
}
