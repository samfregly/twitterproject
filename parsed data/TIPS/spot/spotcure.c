/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_curext					spotcure.c

Function : Returns file extension used by specified view mode.

Author : A. Fregly

Abstract : 

Calling Sequence : char *ext = spot_curext(int viewmode);

  viewmode	View mode for which extension is desired.
  ext		Returned pointer at current extension.

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
char *spot_curext(int viewmode)
#else
char *spot_curext(viewmode)
  int viewmode;
#endif
{
  switch (viewmode) {
    case SPOT_VIEWFILES :
      return ""; 
      break;
    case SPOT_VIEWRESULTS : 
      return ".hit";
      break;
    case SPOT_VIEWQUERIES :
      return ".qry"; 
      break;
    case SPOT_VIEWARCHIVES :
      return ".sar";
      break;
    case SPOT_VIEWSTREAMS :    
      return ".act";
      break;
    case SPOT_VIEWSTREAMDETAIL :
      return ".qry";
      break;
    default :
      ;
  }
  return "";
}
