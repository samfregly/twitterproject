/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_docline					spotdocl.c

Function : Returns pointer at buffer containing document text at specified line.

Author : A. Fregly

Abstract : This function return a character pointer at a document buffer
	which contains text starting at the specified line, and continuing
	for at least a screen worth of lines (unles of there are not that
	many lines in the document).

Calling Sequence : char *lineptr = spot_docline(struct spot_s_doc *doc, 
  int line);

  doc		Document descriptor structor.
  lineptr	Returned pointer.

Notes: 

Change log : 
000	10-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Make strict compilers happy.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
char *spot_docline(struct spot_s_doc *doc, int line)
#else
char *spot_docline(doc, line)
  struct spot_s_doc *doc;
  int line;
#endif
{

  long lineoffset, endoffset, loadoffset;
  int offset, loadline, endline, loadflag;

  if (line < 0 || line >= doc->nlines) return NULL;

  lineoffset = doc->lineoffsets[line];
  loadflag = 0;
  offset = 0;

  if (line < doc->startline)
    loadflag = 1;
  else if  (line > doc->startline) {
    /* See if line and a following screens worth are in the document buffer */
    endline = min(doc->nlines, line + LINES);
    endoffset = doc->lineoffsets[endline];
    loadflag =  (endoffset - doc->lineoffsets[doc->startline] > doc->buflen); 
  }

  if (loadflag) {
    loadline = line - (doc->buflen/doc->wrapcol) / 2;
    if (loadline < 0) loadline = 0;
    loadoffset = doc->lineoffsets[loadline];
    if (fseek(doc->temp, loadoffset, 0)) return NULL;
    if (!fread(doc->docbuf, 1, doc->buflen, doc->temp)) return NULL;
    doc->startline = loadline;
    offset = lineoffset - loadoffset;
  }
  else
    offset = doc->lineoffsets[line] - doc->lineoffsets[doc->startline];

  return (char *) (doc->docbuf + offset);
}
