/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_label					spotlabe.c

Function : Fills in screen label.

Author : A. Fregly

Abstract : This function is used to fill in the screen label. The supplied
	string is added as a suffix to "Spot - ", and the result is displayed
	centered in the the screen label window.

Calling Sequence : spot_label(char *label);

  label		Label to be put into label window.

Notes: 

Change log : 
000	04-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_label(char *label)
#else
void spot_label(label)
  char *label;
#endif
{
  char labelbuf[SPOT_LABELWINLEN + 1];
  int off;

  fit_sblank(labelbuf, SPOT_LABELWINLEN);
  off = -1;
  fit_sadd(labelbuf, SPOT_LABELWINLEN, "Spot - ", &off);
  fit_sadd(labelbuf, SPOT_LABELWINLEN, label, &off);
  fit_scenter(labelbuf, SPOT_LABELWINLEN);
  mvwaddstr(spot_labelwin, 0, 1, labelbuf);
}
