/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_curdefault					spotcurd.c

Function : Returns default directory used by specified view mode.

Author : A. Fregly

Abstract : 

Calling Sequence : char *path = spot_curdefault(struct spot_s_env *env,
	int viewtype);

  env		Spot environment structure.
  viewtype	View type for which default is to be returned.
  ext		Returned pointer at current default.

Notes: 

Change log : 
000	19-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
char *spot_curdefault(struct spot_s_env *env, int viewtype)
#else
char *spot_curdefault(env, viewtype)
  struct spot_s_env *env;
  int viewtype;
#endif
{
  switch (viewtype) {
    case SPOT_VIEWFILES :
      return env->filedir; 
    case SPOT_VIEWRESULTS : 
    case SPOT_VIEWQUERIES :
      return env->searchdir; 
    case SPOT_VIEWARCHIVES :
      return env->arcdir;
    case SPOT_VIEWSTREAMS :    
      return env->streamdir;
    case SPOT_VIEWSTREAMDETAIL :
      return env->searchdir;
    default :
      ;
  }
  return spot_curpath;
}
