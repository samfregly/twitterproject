/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_brmakedisp					spotbrma.c

Function : Creates windows and menu's specific to spot browse function.

Author : A. Fregly

Abstract : 

Calling Sequence : int stat = spot_brmakedisp(struct spot_s_env *env);

  env		Spot environment structure.
  stat		Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	09-SEP-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>

/* Define Menus available from the main screen */

/* The File menu */
static char *fileopts[] = {"Open", "Pick", "Copy", "Mail",  "Delete", 
  "Print", "Shell", "Exit Browse",""};

/* Copy destination menu */
static char *copyopts[] = {"File", "Results", "Archive", ""};

/* Delete target menu */
static char *delopts[] = {"Original Document(s)", "From Archive", ""};


#if FIT_ANSI
int spot_brmakedisp(struct spot_s_env *env)
#else
int spot_brmakedisp(env)
  struct spot_s_env *env;
#endif
{

  int cmdmenutype, pulldownmenutype, displaymenutype;

  /* Set menu operation attributes */
  cmdmenutype = MEN_ATTR_HORIZONTAL + MEN_ATTR_HORIZONTALWRAP;
  pulldownmenutype = (env->uselinedrawing) ? MEN_ATTR_BOXED : 0;
  displaymenutype = (env->uselinedrawing) ? MEN_ATTR_BOXED : 0;
  displaymenutype += MEN_ATTR_MULTSEL;
  if (env->usepointer) {
    cmdmenutype += MEN_ATTR_POINTER;
    pulldownmenutype += MEN_ATTR_POINTER;
    displaymenutype += MEN_ATTR_POINTER;
  }
  if (env->usehotkeys) {
    cmdmenutype += (MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT);
    pulldownmenutype += (MEN_ATTR_HOTKEYS + MEN_ATTR_INSTANT);
  }

  if (env->usecheckmarks)
    displaymenutype += MEN_ATTR_CHECK;

  /* Pull down, File menu */
  spot_brfilewin = newwin(8 + 2 * env->uselinedrawing, 13 + env->usepointer +
	2 * env->uselinedrawing, 0, 0);
  scrollok(spot_brfilewin, FALSE);
  spot_brfilemenu = men_init(spot_brfilewin, pulldownmenutype, NULL, fileopts,
	env->mennorm, env->menhigh, env->mensel, env->menhot, ACS_RARROW,
	ACS_DIAMOND, 1, 0);
  men_overlay(spot_brfilemenu, spot_cmdmenu, 0, 0);

  /* Pull down, delete target window */
  spot_brdeloptwin = newwin(3 + (env->uselinedrawing != 0), 
    22 + env->usepointer + env->usecheckmarks + 2 * env->uselinedrawing, 0, 0);
  scrollok(spot_brdeloptwin, FALSE);
  spot_brdeloptmenu = men_init(spot_brdeloptwin, displaymenutype, 
      "Items to Delete", delopts, env->popupnorm, env->popuphigh, 
      env->popupsel, env->popuphot, ACS_RARROW, ACS_DIAMOND, 1, 0);

  /* Pull down, copy destination menu */
  spot_brcopywin = newwin(4 + (env->uselinedrawing != 0), 
    13 + env->usepointer + 2 * env->uselinedrawing, 0, 0);
  scrollok(spot_brcopywin, FALSE);
  spot_brcopymenu = men_init(spot_brcopywin, pulldownmenutype, "Destination",
	copyopts, env->mennorm, env->menhigh, env->mensel, env->menhot, 
	ACS_RARROW, ACS_DIAMOND, 1, 0);

  return 1;
}
