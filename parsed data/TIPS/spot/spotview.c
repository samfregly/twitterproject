/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_viewinit					spotview.c

Function : Initializes result display window and fills it based on current
	view mode.

Author : A. Fregly

Abstract : This function is used to fill define menu's for the result
  display window, and fill them up with their initial display.

Calling Sequence : spot_viewinit(struct spot_s_env *env, int viewtype,
	long useall, char *summaryitem); 

  env		Spot environment.
  viewtype	View type to be initialized.
  useall	For summary view types, indicate what is to be summarized. A
		value of 0 indicates that selected items are to be summarized,
		while a non-zero value indicates all items are to be used.
  summaryitem	For summary view types, the field or line number to be used. 

Notes: 

Change log : 
000	04-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Fix calls to fit_wildlist and fit_filelist. Make strict
			compilers happy.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <spot.h>
#include <sys/stat.h>

#if FIT_ANSI
int spot_viewinit(struct spot_s_env *env, int viewtype, long useall,
  char *summaryitem)
#else
int spot_viewinit(env, viewtype, useall, summaryitem)
  struct spot_s_env *env;
  int viewtype;
  long useall;
  char *summaryitem;
#endif
{

  char *(*filelist) = NULL;
  char *dirname, *ftype, *viewname;
  char fullpath[FIT_FULLFSPECLEN+1], streamqlib[FIT_FULLFSPECLEN+1];
  char qryspec[FIT_FULLFSPECLEN+1];
  char *(*fspec), *newfspec;
  int count, col, fullpath_l, len, cmpstat;
  unsigned *filelist_L=NULL;
  long i, j, nsel;
  struct drct_s_file *qlib;
  struct drct_s_info qlibinfo;
  struct men_s_menu *menu = NULL;
  struct men_s_option *opt;
  struct spot_s_doc doc;

  dirname = spot_curdefault(env, viewtype);
  ftype = spot_curext(viewtype);
#if FIT_DEBUG
  fprintf(fit_debug_handle,"spot_viewinit: dirname: %s, ftype: %s\n",dirname,ftype);
  fflush(fit_debug_handle);
#endif

  /* Set directory name */
  switch (viewtype) {
    case SPOT_VIEWFILES :
      viewname = "Files: ";
      break;
    case SPOT_VIEWSTREAMS :
      viewname = "Streams: ";
      break;
    case SPOT_VIEWARCHIVES :
      viewname = "Archives: ";
      break;
    case SPOT_VIEWRESULTS :
      viewname = "Results: ";
      break;
    case SPOT_VIEWQUERIES :
      viewname = "Queries: ";
      break;
    case SPOT_VIEWSTREAMDETAIL :
      viewname = "Stream Detail: ";
      break;
    case SPOT_VIEWDETAIL :
      viewname = "Document Info: ";
      break;
    case SPOT_VIEWLINE :
      viewname = "Extracted Lines: ";
      break;
    case SPOT_VIEWFIELD :
      viewname = "Extracted Field: ";
      break;
    default :
      viewname = "";
  }
#if FIT_DEBUG
  fprintf(fit_debug_handle,"view name: %s\n",viewname);
#endif

  /* Create file list for files in view path for current view mode */
  if (viewtype == SPOT_VIEWFILES && *spot_filefilter) {
    if ((filelist = fit_wildlist(dirname, spot_filefilter, S_IFREG, 
	spot_curpath, &count, fullpath)) == NULL)
      count = 0;
  }
  else if (viewtype != SPOT_VIEWSTREAMDETAIL && viewtype < SPOT_VIEWDETAIL) {
    if ((filelist = fit_filelist(dirname, ftype, S_IFREG, spot_curpath, &count,
        fullpath)) == NULL)
      count = 0;
  }
  else if (viewtype == SPOT_VIEWDETAIL || viewtype == SPOT_VIEWLINE ||
      viewtype == SPOT_VIEWFIELD) {
    if (env->menu == NULL) return 0; 
    filelist = NULL;
    count = 0;
    if (env->view < SPOT_VIEWDETAIL) {
      if (env->view == SPOT_VIEWARCHIVES || env->view == SPOT_VIEWRESULTS ||
	  env->view == SPOT_VIEWSTREAMDETAIL) {
	opt = men_opt(env->menu, env->menu->curopt);
	strcpy(fullpath, spot_fspec(opt, env->view));
      }
      else
	strcpy(fullpath, spot_curdefault(env, env->view));

      /* Initialize document structure for use with summary menu */
      spot_docinit(env, &env->doc, !useall);
    }
    else {
      spot_newdoc(&doc);
      spot_docinit(env, &doc, !useall);
      strcpy(fullpath, env->doc.source);
      spot_docdeall(&env->doc);
      env->doc = doc;
    }
  }
  else if (viewtype == SPOT_VIEWSTREAMDETAIL) {
    count = 0;
    strcpy(fullpath, env->streamname);
    fit_expfspec(env->streamname, ".qlb", env->streamdir, streamqlib);
    fit_setext(streamqlib, ".qlb", streamqlib);
    if ((qlib = qc_openlib(streamqlib, O_RDONLY, 0)) != NULL) {
      if (drct_info(qlib, &qlibinfo) && qlibinfo.nrecs) {
	filelist = (char *(*)) calloc((int) qlibinfo.nrecs+2, sizeof(*filelist));
	filelist_L = (unsigned *) calloc((int) qlibinfo.nrecs+1, 
	  sizeof(*filelist_L));
        if (filelist && filelist_L) {
	  for (count=0, i=0, drct_rewind(qlib); i < qlibinfo.nrecs; ++i) {
	    if (qc_getqrylib(qlib, qryspec) >= 0) {
	      if ((filelist[count] = 
		  (char *) malloc(strlen(qryspec)+5))) {
		fit_setext(qryspec, ".hit", filelist[count]);
		filelist_L[count] = strlen(filelist[count]);
                ++count;
	      }
	      else
		break;
	    }
	    else
	      break;
	  }

          if (count) {
            fit_ssort(filelist, filelist_L, count);
          }
          else {
            fit_delfilelist(&filelist);
	    free(filelist_L);
	  }
        }
      }
      qc_closelib(qlib);
    }
  }

  /* Clear the display window */
  wattrset(spot_dispwin, env->dispnorm);
  win_fill(spot_dispwin, 0, 0, 0, 0, ' ');
  touchwin(spot_dispwin);

  /* Clear the footer window */
  wattrset(spot_footerwin, env->mennorm);
  win_fill(spot_footerwin, 0, 0, 0, 0, ' ');

  /* Fill in footer with "view_name: path" */
  fullpath_l = strlen(fullpath);
  col = (COLS - (strlen(viewname) + fullpath_l)) / 2;
  if (col < 0) col = 0;
  mvwaddstr(spot_footerwin, 0, col, viewname);

  if (*fullpath) {
    if (viewtype >= SPOT_VIEWDETAIL) {
      fit_setext(fullpath, "", fullpath);
    }
    fullpath_l = strlen(fullpath);
    if (fullpath_l && fullpath[fullpath_l - 1] == '/') 
      fullpath[--fullpath_l] = 0;
    if (*fullpath)
      waddstr(spot_footerwin, fullpath);
  }

  if (viewtype >= SPOT_VIEWDETAIL && env->view < SPOT_VIEWDETAIL) {
    env->prevview = env->view;
    env->previtem[0] = 0;
    if (env->prevsel != NULL) fit_delfilelist(&env->prevsel);
    if (env->menu != NULL) {
      opt = men_opt(env->menu, env->menu->curopt);
      if (opt != NULL) strcpy(env->previtem, spot_fspec(opt, env->view));
      if (nsel = men_nselected(env->menu)) {
	if ((env->prevsel = (char *(*)) calloc((int) (nsel+1), 
	    sizeof(*env->prevsel))) != NULL) {
	  for (i=0, j = 0; i < env->menu->nopts; ++i) {
	    if ((opt = men_opt(env->menu, i)) != NULL && opt->descr.stat) {
	      newfspec = spot_fspec(opt, env->view);
	      len = strlen(newfspec);
	      if ((env->prevsel[j] = (char *) calloc(len+1, 1)) != NULL)
		strcpy(env->prevsel[j++], newfspec);
	    }
	  }
	  env->prevsel[j] = NULL;
	}
      }	
    }
  }
  menu = spot_fmenu(env, viewtype, spot_dispwin, 0, filelist, summaryitem); 
  if (viewtype < SPOT_VIEWDETAIL && env->view >= SPOT_VIEWDETAIL &&
      viewtype == env->prevview && menu != NULL) {
    if (env->prevsel != NULL) {
      for (i = 0, fspec = env->prevsel; i < menu->nopts && *fspec != NULL;
	 ++i) {
        opt = men_opt(menu, i);
        newfspec = spot_fspec(opt, viewtype);
        for (; *fspec != NULL; ++fspec) {
          if ((cmpstat = strcmp(*fspec, newfspec)) >= 0) {
            if (!cmpstat) {
              opt->descr.stat = 1;
              men_update(menu, i);
            }
            break;
          }
        }
      }
    }
    if (env->previtem[0]) {
      /* Find the current option in the display */
      for (i=0, menu->curopt = 0; i < menu->nopts; ++i) {
        if ((opt = men_opt(menu, i)) != NULL)
          cmpstat = strcmp(spot_fspec(opt, viewtype), env->previtem);
        if (cmpstat >= 0) {
          menu->curopt = i;
          men_draw(menu);
	  men_putopt(menu, i + ((menu->nrows-1) * menu->ncols) / 2);
	  men_putopt(menu, i - ((menu->nrows-1) * menu->ncols) / 2);
          break;
        }
      }
    }
  }
  else if (menu != NULL)
    men_draw(menu);

  if (env->menu != NULL) men_dealloc(&env->menu);
  env->menu = menu;
  if (viewtype > SPOT_VIEWDETAIL && env->view < SPOT_VIEWDETAIL)
    strcpy(env->summaryitem, summaryitem);
  env->view = viewtype;
  return env->menu != NULL; 
}
