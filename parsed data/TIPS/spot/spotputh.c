/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_puthelpline					spotputh.c

Function : Puts help line text centered in prompt line.

Author : A. Fregly

Abstract : This function is used to center a help line in the prompt line.

Calling Sequence : void spot_puthelpline(char *text);

  text	Text to be displayed in help line.

Notes: 

Change log : 
000	29-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <spot.h>

#if FIT_ANSI
void spot_puthelpline(char *text)
#else
void spot_puthelpline(text)
  char *text;
#endif
{
  win_center(spot_promptwin, 0, text);
  wrefresh(spot_promptwin);
}
