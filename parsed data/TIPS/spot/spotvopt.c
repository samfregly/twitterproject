/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_voption					spotvopt.c

Function : Handles View menu option of main view menu.

Author : A. Fregly

Abstract :  This function processes the various view options off the main
        menu. It will display any pulldowns and dialogs required for completing
        initiation of the function, then do the function.

Calling Sequence : spot_voption(struct spot_s_env *env, 
	struct men_s_menu *menu, int option);

  env		Spot environment structure.
  menu		Menu displaying view options.
  option	Selected off off from view menu.

Notes: 

Change log : 
000	20-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <fitsydef.h>
#include <spot.h>

#define MAXVIEWOPT 9
static int viewmap[MAXVIEWOPT] = {
  SPOT_VIEWRESULTS, SPOT_VIEWSTREAMS, SPOT_VIEWFILES, SPOT_VIEWARCHIVES,
  SPOT_VIEWQUERIES, SPOT_VIEWSTREAMDETAIL, SPOT_VIEWDETAIL, SPOT_VIEWDETAIL+1,
  -1};

#if FIT_ANSI
void spot_voption(struct spot_s_env *env, struct men_s_menu *menu, int option)
#else
void spot_voption(env, menu, option)
  struct spot_s_env *env;
  struct men_s_menu *menu;
  int option;
#endif
{

  char *(*streamlist), streampath[FIT_FULLFSPECLEN+1];
  int newview, oldview, ftype, key, summaryopt, srcopt, optselected;
  char summaryitem[17];
  struct men_s_menu *tempmenu;

  oldview = env->view;
  newview = viewmap[option];
  tempmenu = NULL;

  if (newview == SPOT_VIEWSTREAMDETAIL) {
    /* Stream detail */
    spot_fselect(env, 0, 0, env->streamdir, ".act", &streamlist, streampath);
    if (streamlist != NULL) {
     strcpy(env->streamdir, streampath);
     env->streamdir[FIT_FULLFSPECLEN] = 0;
	fit_expfspec(streamlist[0], ".act", streampath, env->streamname);
	env->view = SPOT_VIEWSTREAMDETAIL;
	fit_delfilelist(&streamlist);
    }
  }
  else if (newview == -1) { 
    /* Swap current directory for destination directory */
    switch (env->view) {
      case SPOT_VIEWRESULTS : case SPOT_VIEWQUERIES :
	spot_swap(env->searchdir, spot_searchdest);
	newview = env->view;
	break;
      case SPOT_VIEWSTREAMDETAIL :
	spot_swap(env->streamname, spot_streamdestname);
	newview = env->view;
	break;
      case SPOT_VIEWSTREAMS :
	spot_swap(env->streamdir, spot_streamdest);
	newview = env->view;
	 break;
      case SPOT_VIEWFILES :
	spot_swap(env->filedir, spot_filedest);
	newview = env->view;
	break;
      case SPOT_VIEWARCHIVES :
	spot_swap(env->arcdir, spot_arcdest);
	newview = env->view;
	break;
      case SPOT_VIEWDETAIL : case SPOT_VIEWLINE : case SPOT_VIEWFIELD :
	spot_message("Summaries cannot be swapped");
	break;
      default :
 	;
    }
  }
  else if (newview == SPOT_VIEWFILES) {
    if (!spot_fprompt(env, 1 << 1, 
	"Enter file name filter, or blank for all files", spot_filefilter,
	"", env->filedir, spot_filefilter, env->filedir, &ftype))
      env->view = oldview;
  }
  else if (newview == SPOT_VIEWDETAIL) {
    if (env->view == SPOT_VIEWSTREAMS) {
      spot_message("Streams cannot be summarized");
      men_remove(menu, spot_dispwin);
      return;
    }
    else if (env->menu == NULL || env->menu->nopts == 0) {
      spot_message("There are no items to summarize");
      men_remove(menu, spot_dispwin);
      return;
    }
    men_reset(spot_brsummaryoptmenu);
    if (env->view == SPOT_VIEWFILES || env->view == SPOT_VIEWQUERIES ||
	env->view >= SPOT_VIEWDETAIL) {
      if (men_nselected(env->menu)) {
	men_reset(spot_sumsrcmenu);
	men_overlay(spot_sumsrcmenu, menu, menu->curopt, 1);
	tempmenu = spot_sumsrcmenu;
	if (!spot_getpulldown(spot_sumsrcmenu, &srcopt, &key))
	  newview = -1;
      }
      else {
	tempmenu = menu;
        srcopt = 1;
      }
    }

    else {
      tempmenu = menu;
      srcopt = 1;
    }

    if (newview == -1) goto DONE;

    men_overlay(spot_brsummaryoptmenu, tempmenu, tempmenu->curopt, 1);

    if (spot_getpulldown(spot_brsummaryoptmenu, &summaryopt, &key)) {
      newview += summaryopt;
      if (newview == SPOT_VIEWLINE) {
	strcpy(summaryitem, "1");
	if (!spot_prompt("Line: ", summaryitem, 12))
	  newview = -1;
      }
      else if (newview == SPOT_VIEWFIELD) {
	if (!spot_fldprompt(env, spot_brsummaryoptmenu, summaryitem))
	  newview = -1;
      }
    }
    else
      newview = -1;
    men_remove(spot_brsummaryoptmenu, spot_dispwin);
  }
  else if (newview == SPOT_VIEWDETAIL + 1) {
    if (env->menu == NULL || env->view < SPOT_VIEWDETAIL ||
	env->view >= SPOT_VIEWDETAIL &&
	env->prevview != SPOT_VIEWARCHIVES &&
	env->prevview != SPOT_VIEWRESULTS &&
	env->prevview != SPOT_VIEWSTREAMDETAIL) {
      spot_message("Monitor is disabled for current view mode");
      men_remove(menu, spot_dispwin);
      return;
    }

    men_remove(menu, spot_dispwin);
    spot_puthelpline("Press any key to exit Monitor mode");

    for (key = ERR; key == ERR;) {
      spot_refresh(env, env->menu);
      men_dokey(env->menu, KEY_END, &optselected);
      wrefresh(env->menu->optwin);
      signal(SIGALRM, spot_timeralarm);
      alarm(env->updateinterval);
      key = wgetch(env->menu->optwin);
      alarm(0);
    }
    spot_puthelpline(spot_disphelp);
    return;
  }

DONE:
  if (tempmenu != NULL && tempmenu != menu) men_remove(tempmenu, spot_dispwin);
  if (menu != NULL) men_remove(menu, spot_dispwin);
  if (newview < 0) return;
  spot_viewinit(env, newview, srcopt, summaryitem);
  wrefresh(spot_dispwin);
  wrefresh(spot_footerwin);
}
