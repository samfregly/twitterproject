/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_pstop					spotpsto.c

Function : Stops profiling of specified stream.

Author : A. Fregly

Abstract : 

Calling Sequence : spot_pstop(char *streamname);

  streamname	Stream name for which profiling is to be stopped.

Notes: 

Change log : 
000	14-OCT-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fitsydef.h>
#include <spot.h>

//extern  int errno;

#if FIT_ANSI
void spot_pstop(char *streamname)
#else
void spot_pstop(streamname)
  char *streamname;
#endif
{
  char fname[FIT_FULLFSPECLEN+1], dummyspec[FIT_FULLFSPECLEN+1];
  int pid, exitpid, exitstat;

  fit_prsfspec(streamname, dummyspec, dummyspec, dummyspec, fname, dummyspec,
    dummyspec); 

  if (!(pid=fork())) {
    freopen("/dev/null", "w", stdout);
    freopen("/dev/null", "w", stderr);
    execlp("spotadmin", "spotadmin", "shutdown", "stream", fname, (char *) 0);
    exit(errno);
  }
  else if (pid > 0) {
    while ((exitpid=wait(&exitstat)) >= 0 && exitpid != pid)
      ;
  }
  else
    spot_errmessage("Error forking profile shutdown command");
#if FIT_COHERENT
  spot_scrinit();
#endif
}
