/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_doprint					spotdopr.c

Function : Performs print commands against items in supplied list.

Author : A. Fregly

Abstract : This function is used to spawn an "print" script or program for
	either the current item in the main display or selected items from
	the main display. Before initiating printing, spot_doprint will put 
	a "Working..." message on the help line, then call endwin() to end 
	windows processing.  On completion of the action processing, the 
	curses is restarted, the help line is restored, and the screen 
	restored. If the print process requires time for a user to look at 
	the screen before having it restored, it is the responsibility of the 
	action process to provide some means of delaying it's termination, 
	either by prompting the user to continue, or by a timeout or some 
	other creative means.

Calling Sequence : int stat = spot_doprint(struct spot_s_env *env,
  int useselected); 

  env		Spot environment structure.
  useselected	Nonzero value specifies that selected files are to be
		printed.

Notes: 

Change log : 
000	05-APR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fitsydef.h>
#include <spot.h>

//extern int errno;

#if FIT_ANSI
int spot_doprint(struct spot_s_env *env, int useselected)
#else
int spot_doprint(env, useselected)
  struct spot_s_env *env; 
  int useselected;
#endif
{

  char *src, *printcmd;
  char namefld[TIPS_DESCRIPTORMAX+1], keyfld[TIPS_DESCRIPTORMAX+1];
  char dbname[FIT_FULLFSPECLEN+1], docnum_s[FIT_FULLFSPECLEN+1], fspec[FIT_FULLFSPECLEN+1];
  char dummyspec[FIT_FULLFSPECLEN+1], ext[FIT_FULLFSPECLEN+1];
  struct men_s_option *opt;
  long i, startopt, endopt, lenfld;
  int pid, exitpid, exitstat, j;
  WINDOW *promptwin;

  win_dup(spot_promptwin, &promptwin);
  win_center(spot_promptwin, 0, "Working...");
  wrefresh(spot_promptwin);
  endwin();

  if (useselected) {
    startopt = 0;
    endopt = env->menu->nopts - 1;
  }
  else {
    startopt = env->menu->curopt;
    endopt = startopt;
  }

  for (i=startopt; i <= endopt; ++i) {
    opt = men_opt(env->menu, i);
    if (opt->descr.stat || !useselected) {
      src = spot_fspec(opt, env->view);
      tips_parsedsc(src, namefld, &lenfld, keyfld);
      tips_nameparse(namefld, dbname, docnum_s, fspec);
      printcmd = NULL;
      if (!*dbname) {
        fit_prsfspec(fspec, dummyspec, dummyspec, dummyspec,
	  dummyspec, ext, dummyspec);
	for (j=0; j < spot_nprintcmd; ++j) {
	  if (!strcmp(ext, spot_printtype[j])) {
	    printcmd = spot_printcmd[j];
	    break;
	  }
	}
      }
      if (printcmd == NULL)
	printcmd = spot_defaultprintcmd;

      if (!(pid = fork())) {
        /* Execute the script */
        execlp(printcmd, printcmd, src, (char *) 0);

        /* This should never get executed */
        exit(errno);
      }
      else if (pid == -1) {
        /* Error from fork */
        spot_errmessage("Error forking print process");
	break;
      }
      else
        while ((exitpid = wait(&exitstat)) >= 0 && exitpid != pid)
          ;
#if FIT_COHERENT
      spot_scrinit();
#endif
    }
  }
  if (promptwin != NULL) {
    overwrite(promptwin, spot_promptwin);
    wrefresh(spot_promptwin);
    delwin(promptwin);
  }
}
