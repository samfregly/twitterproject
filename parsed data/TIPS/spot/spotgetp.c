/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot_getpulldown					spotgetp.c

Function : Gets menu option off from pulldown.

Author : A. Fregly

Abstract : This function will display a pulldown menu and then allow the user 
	to select an option off the pulldown menu. The function will recognize 
	the spot help key, and invoke the help function if it is pressed while 
	the user is accessing the menu.

Calling Sequence : int optselected = spot_getpulldown(men_s_menu *menu, 
  int *retopt, int *exitkey);

  menu		Menu to be displayed.
  retopt	Returned option selected by the user.
  exitkey	Key pressed by user to exit the menu.
  optselected	Non-zero value indicates user a valid selection key to exit
		the menu.

Notes: 

Change log : 
000	19-FEB-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>
#include <spot.h>

#if FIT_ANSI
int spot_getpulldown(struct men_s_menu *menu, int *retopt, int *exitkey)
#else
int spot_getpulldown(menu, retopt, exitkey)
  struct men_s_menu *menu;
  int *retopt, *exitkey;
#endif
{
  int optselected, timedout;
  long ropt;

  if (optselected = men_draw(menu))
    if (optselected = men_put(menu, spot_dispwin))
      for (*exitkey = spot_helpkey; *exitkey == spot_helpkey;) {
	*exitkey = men_select(menu, 0, &ropt, &optselected, &timedout);
	*retopt = ropt;
	if (*exitkey == spot_helpkey) spot_help();
      }
  return optselected;
}
