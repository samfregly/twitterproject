#!/usr/bin/perl
# Author: A. Fregly		June 13, 1997 
# Function: Highlight
#
# Function: Finds matches for terms from a TIPS query in the specified input
#	file. The file is printed to standard output with the highlighted terms
#	encompassed in double angle brackets.
#
# Calling Sequence:
#	highlight query_file textfile
#
# Notes:
#	If running from DOS, set the $isDOS variable to 1 so that handling
#	of newline characters will be taken care of correctly.
#

$isDOS = 0;
if (scalar(@ARGV) != 2) {
  print "Usage: $0 query_file textfile\n";
  print "Number of argument: ",scalar @ARGV, "\n";
}
else {
  $offsets = `offsets -q $ARGV[0] -f $ARGV[1]`;
  @pairs = split(/\n/, $offsets);
  $npairs = @pairs;

  $idx = 0;
  $i = 0;
  $linenum = 1;
  if ($npairs > 0) {
    ($off, $l) = split(/ /, $pairs[0]);
  }
  if (open (FILE, $ARGV[1])) {
    while (<FILE>) {
      $linein = $_;
      if ($isDOS) { $linein =~ s/\n/\r\n/; }
      $nextidx = $idx + length($linein);
      if ($i < $npairs && $off < $nextidx) {
        @elements = split(//,$linein);
        $sidx = $off - $idx; 
        $adj = 0;
        while ($i < $npairs && $off < $nextidx) {
          splice(@elements, $sidx, 0, "<<");
          splice(@elements, $sidx + $l + 1, 0, ">>");
          $adj += 2;
          $i += 1;
          if ($i < $npairs) {
    	  ($off, $l) = split(/ /, $pairs[$i]);
            $sidx = ($off - $idx) + $adj;
          }
        }
        $linein = join('', @elements);
      }
      if ($isDOS) {
	print $linein;
      }
      else {
        print "$linein\n";
      }
      $idx = $nextidx;
      $linenum += 1;
    }
    close FILE;
  }
  else {
    print "Unable to open text file: <EM>", $contents{'Doc'}, "</EM>\n";
  }
}
