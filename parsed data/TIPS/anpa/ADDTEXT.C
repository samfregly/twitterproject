/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_addtext					addtext.c

Function : Adds text to output buffer, cleaning out non-ASCII values and 
	preventing overflow.

Author : A. Fregly

Abstract : This routine is used to add text to the anpa output buffer. It will
	convert any non-printable characters to blanks, and make sure that
	the output buffer is not overfilled.

Calling Sequence : anpa_addtext(char *text, int len, int *outidx);

	text	Text to be added to output buffer.
	len	Length of text.
	outidx	Pointer at last used location if output buffer.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
void anpa_addtext(char *text, int len, int *outidx)
#else
void anpa_addtext(text,len,outidx)
  char *text;
  int len, *outidx;
#endif
{

static unsigned char map[256] = {
0,0,0,0,0,0,0,0x7,0,0x9,0xA,0,0xC,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,
0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,
0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,
0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,
0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,
0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

  int addlen;
  char *cptr;

  addlen = (ANPA_BUFFERSIZE - *outidx >  len) ? len : ANPA_BUFFERSIZE - *outidx;
  for (cptr = anpa_outbuffer + *outidx + 1; addlen > 0; --addlen, ++text)
    if (map[(unsigned) *text])
      *(cptr++) = map[(unsigned) *text];

  *outidx += cptr - (anpa_outbuffer + *outidx + 1);
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_addtext					addtext.c

Function : Adds text to output buffer, cleaning out non-ASCII values and 
	preventing overflow.

Author : A. Fregly

Abstract : This routine is used to add text to the anpa output buffer. It will
	convert any non-printable characters to blanks, and make sure that
	the output buffer is not overfilled.

Calling Sequence : anpa_addtext(char *text, int len, int *outidx);

	text	Text to be added to output buffer.
	len	Length of text.
	outidx	Pointer at last used location if output buffer.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
void anpa_addtext(char *text, int len, int *outidx)
#else
void anpa_addtext(text,len,outidx)
  char *text;
  int len, *outidx;
#endif
{

static unsigned char map[256] = {
0,0,0,0,0,0,0,0x7,0,0x9,0xA,0,0xC,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,
0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,
0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,
0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,
0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,
0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

  int addlen;
  char *cptr;

  addlen = (ANPA_BUFFERSIZE - *outidx >  len) ? len : ANPA_BUFFERSIZE - *outidx;
  for (cptr = anpa_outbuffer + *outidx + 1; addlen > 0; --addlen, ++text)
    if (map[(unsigned) *text])
      *(cptr++) = map[(unsigned) *text];

  *outidx += cptr - (anpa_outbuffer + *outidx + 1);
}
