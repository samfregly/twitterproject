/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_isnumeric					isnumeric.c

Function : Determines if a field is numeric.

Author : A. Fregly

Abstract : 

Calling Sequence : numeric =  anpa_isnumeric(char *value, int len);

	numeric		A non-zero value is returned if value is numeric.
	value		Characters string being checked.
	len		Number of characters in value to be checked.

Notes : 

Change log : 
000	21-MAY-91  AMF	Creaed.	
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
int anpa_isnumeric(char *text, int len)
#else
int anpa_isnumeric(text, len)
  char *text;
  int len;
#endif
{
  int i;

  /* Skip over leading blanks */
  for (i=0; i < len && text[i] == ' '; ++i)
    ;
  if (i >= len) return 0;

  /* Find end of number */
  for (; i < len && text[i] >= '0' && text[i] <= '9'; ++i)
    ;
  /* Skip over trailing blanks */
  for (; i < len && text[i] == ' '; ++i)
    ;

  return i == len;
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_isnumeric					isnumeric.c

Function : Determines if a field is numeric.

Author : A. Fregly

Abstract : 

Calling Sequence : numeric =  anpa_isnumeric(char *value, int len);

	numeric		A non-zero value is returned if value is numeric.
	value		Characters string being checked.
	len		Number of characters in value to be checked.

Notes : 

Change log : 
000	21-MAY-91  AMF	Creaed.	
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
int anpa_isnumeric(char *text, int len)
#else
int anpa_isnumeric(text, len)
  char *text;
  int len;
#endif
{
  int i;

  /* Skip over leading blanks */
  for (i=0; i < len && text[i] == ' '; ++i)
    ;
  if (i >= len) return 0;

  /* Find end of number */
  for (; i < len && text[i] >= '0' && text[i] <= '9'; ++i)
    ;
  /* Skip over trailing blanks */
  for (; i < len && text[i] == ' '; ++i)
    ;

  return i == len;
}
