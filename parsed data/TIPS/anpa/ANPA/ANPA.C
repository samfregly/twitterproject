/*******************************************************************************
		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/
/******************************************************************************
Module Name : anpa                                              anpa.c

Function : ANPA filter.

Author : A. Fregly

Abstract : This program takes as input from stdin a TIPS standard file feed 
	of ANPA raw input files, and writes to standard output a filtered
	version in which TIPS field codes have been inserted for the ANPA
	header fields.

Calling Sequence : anpa {-d delim} {-f field_file} {-l} {-q}

	-d      This switch is used to specify the character used as a
		beginning of field delimiter. Whenever the field delimiter
		character is seen in the input text stream, the following 
		character is interpreted as a field number. The delimiter is
		specified as a hexadecimal number in C language format. If
		field insertion is enabled, and -d has not been used to
		specify the field delimiter character, the default character
		0xFF is used.

	-f field_file

		Sets field definition file name. The field file should contain
		a list of field names in an order corresponding to the field
		numbers used within the input stream. The first entry in the
		field file corresponding to field 0, the second to field 1,...
		The field names are taken as the first word on each line of 
		the field file, the remainder of the line is an optional value
		used to set the text to be placed at the beginning of each
		field. This text may be a printf compatible format statement
		which does not have any data fields. If no field file is 
		defined, default ANPA field numbers as defined in anpa.h are 
		used, and text to be inserted at the start of each field is
		also defaulted.

	-h      Disables insertion of header text at the start of each field.

	-l      Write initiations, termination and processing messages to
		log file open on stderr.

	-u      Messages have upi header line.

Notes : 

Change log : 
000     07-MAY-91  AMF  Created.
001     03-MAR-92  AMF  Only log termination if logging is enabled.
002		04-AUG-97  AMF	Port to MS C++ 5.0 for Win32
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <anpa.h>
#include <anpaglbl.h>
#include <tips.h>

extern int errno;

/* Declare the exit handler */
void anpa_shutdown(int errnum);

/* Declare global variables */
int logging;
char logmsg[TIPS_DESCRIPTORMAX+50];
char progname[FIT_FSPECLEN+1];
char dummyspec[FIT_FSPECLEN+1];
#if FIT_DOS
#define WRITE "wb+"
#else
#define WRITE "w+"
#endif


#if FIT_ANSI
main(int argc, char *argv[])
#else
main(argc,argv)
  int argc;
  char *argv[];
#endif
{

  char *key, *docdescriptor, *workfile, *buffer;
  char *fieldfile, *infile;
  char *(*fields);
  int nfields, doflds, stat;
  long indoclen, outdoclen, inlen, nprocessed;
  int upiformat,ilen,olen,fielddelim,len, i;
  FILE *out;

  /* Initialize strings, pointers, counters */
  buffer = (char *) malloc(ANPA_BUFFERSIZE+1);
  if (buffer == NULL) goto ALLOCERR;
  infile = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (infile == NULL) goto ALLOCERR;
  workfile = (char *) malloc(L_tmpnam+1);
  if (workfile == NULL) goto ALLOCERR;
  fieldfile = (char *) malloc(FIT_FSPECLEN+1);
  if (fieldfile == NULL) goto ALLOCERR;
  docdescriptor = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (docdescriptor == NULL) goto ALLOCERR;
  key = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (key == NULL) goto ALLOCERR;

  stat = 0;
  upiformat = 0;
  logging = 0;
  fields = NULL;
  anpa_fielddata = NULL;
  doflds = 1;
  nprocessed = 0;
  tips_mapflds(anpa_fields,ANPA_NFIELDS,anpa_fields,ANPA_NFIELDS,
    anpa_fieldnums);

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (--argc && !strcmp(*argv,"?")) goto SYNTAX;
 
  /**** Pull off switches *****/
  for (; argc; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch (*((*argv)+1)) {
	case 'd' : case 'D' :
	  --argc;
	  ++argv;
	  if (argc) {
	    sscanf(*argv,"%x",&fielddelim);
	  }
	  break;
	case 'f' : case 'F' :
	  doflds = 1;
	  if (argc > 1 && *(*(argv+1)) != '-') {
	    --argc;
	    ++argv;
	    strcpy(fieldfile,*argv);
	    if (!tips_loadflds(fieldfile,&fields, &anpa_fielddata,&nfields)) {
	      fprintf(stderr,"Error loading field definitions\n");
	      goto ERREXIT;
	    }
	    tips_mapflds(anpa_fields,ANPA_NFIELDS,fields,nfields,
		anpa_fieldnums);
	  }
	  break;
	case 'h' : case 'H' :
	  doflds = 0;
	  break;
	case 'l' : case 'L' :
	  logging = 1;
	  break;
	case 'u' : case 'U' :
	  upiformat = 1;
	  break;
	default :
	  fprintf(stderr,"Invalid command option: %s\n",*argv);
	  goto SYNTAX;
      }
    }
  }

  if (anpa_fielddata == NULL) {
    anpa_fielddata = (char *(*)) calloc(ANPA_NFIELDS, sizeof(*anpa_fielddata));
    if (anpa_fielddata == NULL) goto ALLOCERR;
    for (i=0; i < ANPA_NFIELDS; ++i) {
      len = strlen(anpa_fields[i]);
      anpa_fielddata[i] = (char *) malloc(len+5);
      if (anpa_fielddata[i] == NULL) goto ALLOCERR;
      strcpy(anpa_fielddata[i]," <");
      strcat(anpa_fielddata[i], anpa_fields[i]);
      strcat(anpa_fielddata[i], "> ");
    }
  }

  /* Set work file name */
  tmpnam(workfile);

  if (logging) {
    /* Write initiation log messages */
    sprintf(logmsg,"**** %s initiated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    if (upiformat) fprintf(stderr,"  UPI header handling enabled\n");
    fprintf(stderr,"  work file name: %s\n",workfile);
  }

  signal(SIGTERM,anpa_shutdown);
  signal(SIGINT,anpa_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGQUIT,anpa_shutdown);
  signal(SIGPIPE,anpa_shutdown);
#else
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  while (!feof(stdin)) {

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);
    if (feof(stdin)) goto DONE;

    len = strlen(docdescriptor);
    if (len) docdescriptor[len-1] = 0;

    if (logging) fit_logmsg(stderr,docdescriptor,1);

    if (!tips_parsedsc(docdescriptor,infile,&indoclen,key)) {
      fit_logmsg(stderr,"*** Error parsing input descriptor\n",1);
      goto DONE;
    }

    /* Open work file */
    out = fopen(workfile, WRITE);
    if (out == NULL) {
      fprintf(stderr,"Error opening work file: %s\n",workfile);
      goto ERREXIT;
    }

    inlen = indoclen;
    anpa_newdoc = 1;

    if (upiformat) {
      /* Handle UPI header */
      if (!anpa_upihdr(&inlen,out)) goto ERREXIT;
    }

    /* Handle header line 1 */
    if (!anpa_hdrline1(doflds,&inlen,out)) goto ERREXIT;

    /* Handle header line 2 */
    if (!anpa_hdrline2(doflds,&inlen,out)) goto ERREXIT;

    /* text portion of document */
    if (!anpa_text(doflds,&inlen,out)) goto ERREXIT;

    /* Determine length of output document */
    outdoclen = ftell(out);

    /* Write file description to stdout */
    printf("%s,%ld,%s\n",infile,outdoclen,key);
    if (logging) fprintf(stderr,"  filtered length: %ld\n",outdoclen);

    /* Copy processed file to stdout */
    fseek(out,0L,SEEK_SET);
    while (!feof(out)) {
      ilen = fread(anpa_outbuffer,1,ANPA_BUFFERSIZE,out);
      if (ilen) {
	olen = fwrite(anpa_outbuffer,1,ilen,stdout);
	if (olen != ilen) {
	  fit_logmsg(stderr,"Error writing to stdout",1);
	  goto ERREXIT;
	}
      }
    }
    fclose(out);
    fit_fdelete(workfile);
    nprocessed += 1;
  }
  stat = 1;
  goto DONE;

SYNTAX:
  fprintf(stderr,"Usage: %s {switches}\n",progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -d delim\t\tSet beginning of field delimiter.\n");
  fprintf(stderr,"  -f\t{fieldfile}\tEnable field delimiter insertion.\n");
  fprintf(stderr,"  -l\t\t\tEnable logging.\n");
  fprintf(stderr,"  -u\t\t\tDocuments are prefixed with UPI header line.\n");
  return 0;

ALLOCERR:
  fprintf(stderr,"*** Memory allocation error\n");

ERREXIT:
  perror("");

DONE:
  if (logging) {
    sprintf(logmsg,"**** %s terminated ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (!stat)
    return errno;
  else
    return 0;
}


void anpa_shutdown(int errnum)
{
  if (logging) {
    /* Write initiation log messages */
    sprintf(logmsg,"**** %s aborted ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  exit(0);
}
/*******************************************************************************
		       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/
/******************************************************************************
Module Name : anpa                                              anpa.c

Function : ANPA filter.

Author : A. Fregly

Abstract : This program takes as input from stdin a TIPS standard file feed 
	of ANPA raw input files, and writes to standard output a filtered
	version in which TIPS field codes have been inserted for the ANPA
	header fields.

Calling Sequence : anpa {-d delim} {-f field_file} {-l} {-q}

	-d      This switch is used to specify the character used as a
		beginning of field delimiter. Whenever the field delimiter
		character is seen in the input text stream, the following 
		character is interpreted as a field number. The delimiter is
		specified as a hexadecimal number in C language format. If
		field insertion is enabled, and -d has not been used to
		specify the field delimiter character, the default character
		0xFF is used.

	-f field_file

		Sets field definition file name. The field file should contain
		a list of field names in an order corresponding to the field
		numbers used within the input stream. The first entry in the
		field file corresponding to field 0, the second to field 1,...
		The field names are taken as the first word on each line of 
		the field file, the remainder of the line is an optional value
		used to set the text to be placed at the beginning of each
		field. This text may be a printf compatible format statement
		which does not have any data fields. If no field file is 
		defined, default ANPA field numbers as defined in anpa.h are 
		used, and text to be inserted at the start of each field is
		also defaulted.

	-h      Disables insertion of header text at the start of each field.

	-l      Write initiations, termination and processing messages to
		log file open on stderr.

	-u      Messages have upi header line.

Notes : 

Change log : 
000     07-MAY-91  AMF  Created.
001     03-MAR-92  AMF  Only log termination if logging is enabled.
002		04-AUG-97  AMF	Port to MS C++ 5.0 for Win32
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <anpa.h>
#include <anpaglbl.h>
#include <tips.h>

extern int errno;

/* Declare the exit handler */
void anpa_shutdown(int errnum);

/* Declare global variables */
int logging;
char logmsg[TIPS_DESCRIPTORMAX+50];
char progname[FIT_FSPECLEN+1];
char dummyspec[FIT_FSPECLEN+1];
#if FIT_DOS
#define WRITE "wb+"
#else
#define WRITE "w+"
#endif


#if FIT_ANSI
main(int argc, char *argv[])
#else
main(argc,argv)
  int argc;
  char *argv[];
#endif
{

  char *key, *docdescriptor, *workfile, *buffer;
  char *fieldfile, *infile;
  char *(*fields);
  int nfields, doflds, stat;
  long indoclen, outdoclen, inlen, nprocessed;
  int upiformat,ilen,olen,fielddelim,len, i;
  FILE *out;

  /* Initialize strings, pointers, counters */
  buffer = (char *) malloc(ANPA_BUFFERSIZE+1);
  if (buffer == NULL) goto ALLOCERR;
  infile = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (infile == NULL) goto ALLOCERR;
  workfile = (char *) malloc(L_tmpnam+1);
  if (workfile == NULL) goto ALLOCERR;
  fieldfile = (char *) malloc(FIT_FSPECLEN+1);
  if (fieldfile == NULL) goto ALLOCERR;
  docdescriptor = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (docdescriptor == NULL) goto ALLOCERR;
  key = (char *) malloc(TIPS_DESCRIPTORMAX+1);
  if (key == NULL) goto ALLOCERR;

  stat = 0;
  upiformat = 0;
  logging = 0;
  fields = NULL;
  anpa_fielddata = NULL;
  doflds = 1;
  nprocessed = 0;
  tips_mapflds(anpa_fields,ANPA_NFIELDS,anpa_fields,ANPA_NFIELDS,
    anpa_fieldnums);

  fit_prsfspec(*(argv++),dummyspec,dummyspec,dummyspec,progname,dummyspec,
    dummyspec);
  if (--argc && !strcmp(*argv,"?")) goto SYNTAX;
 
  /**** Pull off switches *****/
  for (; argc; --argc, ++argv) {
    if ((*argv)[0] == '-') {
      switch (*((*argv)+1)) {
	case 'd' : case 'D' :
	  --argc;
	  ++argv;
	  if (argc) {
	    sscanf(*argv,"%x",&fielddelim);
	  }
	  break;
	case 'f' : case 'F' :
	  doflds = 1;
	  if (argc > 1 && *(*(argv+1)) != '-') {
	    --argc;
	    ++argv;
	    strcpy(fieldfile,*argv);
	    if (!tips_loadflds(fieldfile,&fields, &anpa_fielddata,&nfields)) {
	      fprintf(stderr,"Error loading field definitions\n");
	      goto ERREXIT;
	    }
	    tips_mapflds(anpa_fields,ANPA_NFIELDS,fields,nfields,
		anpa_fieldnums);
	  }
	  break;
	case 'h' : case 'H' :
	  doflds = 0;
	  break;
	case 'l' : case 'L' :
	  logging = 1;
	  break;
	case 'u' : case 'U' :
	  upiformat = 1;
	  break;
	default :
	  fprintf(stderr,"Invalid command option: %s\n",*argv);
	  goto SYNTAX;
      }
    }
  }

  if (anpa_fielddata == NULL) {
    anpa_fielddata = (char *(*)) calloc(ANPA_NFIELDS, sizeof(*anpa_fielddata));
    if (anpa_fielddata == NULL) goto ALLOCERR;
    for (i=0; i < ANPA_NFIELDS; ++i) {
      len = strlen(anpa_fields[i]);
      anpa_fielddata[i] = (char *) malloc(len+5);
      if (anpa_fielddata[i] == NULL) goto ALLOCERR;
      strcpy(anpa_fielddata[i]," <");
      strcat(anpa_fielddata[i], anpa_fields[i]);
      strcat(anpa_fielddata[i], "> ");
    }
  }

  /* Set work file name */
  tmpnam(workfile);

  if (logging) {
    /* Write initiation log messages */
    sprintf(logmsg,"**** %s initiated ****",progname);
    fit_logmsg(stderr,logmsg,1);
    if (upiformat) fprintf(stderr,"  UPI header handling enabled\n");
    fprintf(stderr,"  work file name: %s\n",workfile);
  }

  signal(SIGTERM,anpa_shutdown);
  signal(SIGINT,anpa_shutdown);
#if FIT_UNIX || FIT_COHERENT
  signal(SIGQUIT,anpa_shutdown);
  signal(SIGPIPE,anpa_shutdown);
#else
  fit_binmode(stdin);
  fit_binmode(stdout);
#endif

  while (!feof(stdin)) {

    *docdescriptor = 0;
    fgets(docdescriptor,TIPS_DESCRIPTORMAX+1,stdin);
    if (feof(stdin)) goto DONE;

    len = strlen(docdescriptor);
    if (len) docdescriptor[len-1] = 0;

    if (logging) fit_logmsg(stderr,docdescriptor,1);

    if (!tips_parsedsc(docdescriptor,infile,&indoclen,key)) {
      fit_logmsg(stderr,"*** Error parsing input descriptor\n",1);
      goto DONE;
    }

    /* Open work file */
    out = fopen(workfile, WRITE);
    if (out == NULL) {
      fprintf(stderr,"Error opening work file: %s\n",workfile);
      goto ERREXIT;
    }

    inlen = indoclen;
    anpa_newdoc = 1;

    if (upiformat) {
      /* Handle UPI header */
      if (!anpa_upihdr(&inlen,out)) goto ERREXIT;
    }

    /* Handle header line 1 */
    if (!anpa_hdrline1(doflds,&inlen,out)) goto ERREXIT;

    /* Handle header line 2 */
    if (!anpa_hdrline2(doflds,&inlen,out)) goto ERREXIT;

    /* text portion of document */
    if (!anpa_text(doflds,&inlen,out)) goto ERREXIT;

    /* Determine length of output document */
    outdoclen = ftell(out);

    /* Write file description to stdout */
    printf("%s,%ld,%s\n",infile,outdoclen,key);
    if (logging) fprintf(stderr,"  filtered length: %ld\n",outdoclen);

    /* Copy processed file to stdout */
    fseek(out,0L,SEEK_SET);
    while (!feof(out)) {
      ilen = fread(anpa_outbuffer,1,ANPA_BUFFERSIZE,out);
      if (ilen) {
	olen = fwrite(anpa_outbuffer,1,ilen,stdout);
	if (olen != ilen) {
	  fit_logmsg(stderr,"Error writing to stdout",1);
	  goto ERREXIT;
	}
      }
    }
    fclose(out);
    fit_fdelete(workfile);
    nprocessed += 1;
  }
  stat = 1;
  goto DONE;

SYNTAX:
  fprintf(stderr,"Usage: %s {switches}\n",progname);
  fprintf(stderr,"Switches:\n");
  fprintf(stderr,"  -d delim\t\tSet beginning of field delimiter.\n");
  fprintf(stderr,"  -f\t{fieldfile}\tEnable field delimiter insertion.\n");
  fprintf(stderr,"  -l\t\t\tEnable logging.\n");
  fprintf(stderr,"  -u\t\t\tDocuments are prefixed with UPI header line.\n");
  return 0;

ALLOCERR:
  fprintf(stderr,"*** Memory allocation error\n");

ERREXIT:
  perror("");

DONE:
  if (logging) {
    sprintf(logmsg,"**** %s terminated ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  if (!stat)
    return errno;
  else
    return 0;
}


void anpa_shutdown(int errnum)
{
  if (logging) {
    /* Write initiation log messages */
    sprintf(logmsg,"**** %s aborted ****",progname);
    fit_logmsg(stderr,logmsg,1);
  }
  exit(0);
}
