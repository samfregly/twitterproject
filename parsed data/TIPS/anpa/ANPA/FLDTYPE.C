/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_fldtype					fldtype.c

Function : Determines field type for second line of news wire header fields.

Author : A. Fregly

Abstract : This routine determines the GESCAN news wire field which 
	should be used for the supplied text.  This fields recognized 
	by this routine are the REF, KEY, FDAT, and WC fields.

Calling Sequence :   int fldtype = anpa_fldtype(char *outptr,int fldlen);

	outptr	Pointer at field value to be parsed.
	fldlen	Length of field value.
	fldtype	ANPA field index for field value, -1 if no match.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>
#include <anpa.h>

#if FIT_ANSI
int anpa_fldtype(char *fldtext, int fldlen)
#else
int anpa_fldtype(fldtext, fldlen)
  char *fldtext;
  int fldlen;
#endif
{
#define FLDMAX 21

  int fldtype,i,c,len;
  char value[FLDMAX+1];

  fldtype = -1;
  len = (fldlen <= FLDMAX) ? fldlen : FLDMAX;
  strncpy(value,fldtext,len);
  for (i = 0; i < len; ++i)
    if (value[i] < ' ' || value[i] > '~')
      value[i] = ' ';
  value[len] = 0;

  for (i=0; i < len; ++i)
    if (fldtext[i] != ' ') break;

  if (i < len)
    c = value[i];
  else
    c = ' ';

  if (c >= '0' && c <= '9') {
    if (anpa_isnumeric(value+i,len-i))
      fldtype = ANPA_WC;
    else {
      for (i=0; i < len; ++i)
	if (strchr("0123456789 -",value[i]) == NULL) break;
      if (i == len)
	fldtype = ANPA_FDAT;
    }
  }
  else if (!strncmp(value,"am",2) || !strncmp(value,"pm",2) ||
      !strncmp(value,"bc",2) || strchr(value,'-') != NULL)
    fldtype = ANPA_KEY;
  else if (!strcmp(value,"DataRecap"))
    fldtype = ANPA_REF;
  else {
    for (i=5; i < fldlen; ++i)
      if (value[i] != ' ') break;
    if (i == fldlen && anpa_isnumeric(value+1,4))
      fldtype = ANPA_REF;
  }

  return fldtype;
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_fldtype					fldtype.c

Function : Determines field type for second line of news wire header fields.

Author : A. Fregly

Abstract : This routine determines the GESCAN news wire field which 
	should be used for the supplied text.  This fields recognized 
	by this routine are the REF, KEY, FDAT, and WC fields.

Calling Sequence :   int fldtype = anpa_fldtype(char *outptr,int fldlen);

	outptr	Pointer at field value to be parsed.
	fldlen	Length of field value.
	fldtype	ANPA field index for field value, -1 if no match.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <tips.h>
#include <anpa.h>

#if FIT_ANSI
int anpa_fldtype(char *fldtext, int fldlen)
#else
int anpa_fldtype(fldtext, fldlen)
  char *fldtext;
  int fldlen;
#endif
{
#define FLDMAX 21

  int fldtype,i,c,len;
  char value[FLDMAX+1];

  fldtype = -1;
  len = (fldlen <= FLDMAX) ? fldlen : FLDMAX;
  strncpy(value,fldtext,len);
  for (i = 0; i < len; ++i)
    if (value[i] < ' ' || value[i] > '~')
      value[i] = ' ';
  value[len] = 0;

  for (i=0; i < len; ++i)
    if (fldtext[i] != ' ') break;

  if (i < len)
    c = value[i];
  else
    c = ' ';

  if (c >= '0' && c <= '9') {
    if (anpa_isnumeric(value+i,len-i))
      fldtype = ANPA_WC;
    else {
      for (i=0; i < len; ++i)
	if (strchr("0123456789 -",value[i]) == NULL) break;
      if (i == len)
	fldtype = ANPA_FDAT;
    }
  }
  else if (!strncmp(value,"am",2) || !strncmp(value,"pm",2) ||
      !strncmp(value,"bc",2) || strchr(value,'-') != NULL)
    fldtype = ANPA_KEY;
  else if (!strcmp(value,"DataRecap"))
    fldtype = ANPA_REF;
  else {
    for (i=5; i < fldlen; ++i)
      if (value[i] != ' ') break;
    if (i == fldlen && anpa_isnumeric(value+1,4))
      fldtype = ANPA_REF;
  }

  return fldtype;
}
