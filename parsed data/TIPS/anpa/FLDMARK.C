/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_addtext					addtext.c

Function : Adds text to output buffer, cleaning out non-ASCII values and 
	preventing overflow.

Author : A. Fregly

Abstract : This routine is used to insert a TIPS field marker into the
	output buffer. 

Calling Sequence : anpa_addtext(int fieldidx, int *outidx);

  fieldidx	Index of field in field list.
  outidx	Pointer at last used location if output buffer.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
void anpa_fldmark(int fieldidx, int *outidx)
#else
void anpa_fldmark(fieldidx, outidx)
  int fieldidx, *outidx;
#endif
{
  char *cptr;

  if (ANPA_BUFFERSIZE - *outidx > 2) {
    cptr = anpa_outbuffer + *outidx;
    *(++cptr) = 0xFF;
    *(++cptr) = anpa_fieldnums[fieldidx];
    *outidx += 2;
  }
}
/*******************************************************************************

                       Copyright 1991, FIT Systems Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of FIT Systems Inc.

    Fit Systems Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  Fit Systems Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa_addtext					addtext.c

Function : Adds text to output buffer, cleaning out non-ASCII values and 
	preventing overflow.

Author : A. Fregly

Abstract : This routine is used to insert a TIPS field marker into the
	output buffer. 

Calling Sequence : anpa_addtext(int fieldidx, int *outidx);

  fieldidx	Index of field in field list.
  outidx	Pointer at last used location if output buffer.

Notes : 

Change log : 
000	21-MAY-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <anpa.h>

#if FIT_ANSI
void anpa_fldmark(int fieldidx, int *outidx)
#else
void anpa_fldmark(fieldidx, outidx)
  int fieldidx, *outidx;
#endif
{
  char *cptr;

  if (ANPA_BUFFERSIZE - *outidx > 2) {
    cptr = anpa_outbuffer + *outidx;
    *(++cptr) = 0xFF;
    *(++cptr) = anpa_fieldnums[fieldidx];
    *outidx += 2;
  }
}
