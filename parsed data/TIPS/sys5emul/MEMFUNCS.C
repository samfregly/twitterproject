void *memmove(dest, source, len)
  void *dest, *source;
  int len;
{
  char *s, *d, *e;
  if (source > dest)
    for (s = (char *) source, d = (char *) dest, e = s+len; s < e;)
      *d++ = *s++;
  else if (source < dest)
    for (s = (char *) source + len, d = (char *) dest + len; s > source;)
      *--d = *--s;
  return dest;
}
void *memmove(dest, source, len)
  void *dest, *source;
  int len;
{
  char *s, *d, *e;
  if (source > dest)
    for (s = (char *) source, d = (char *) dest, e = s+len; s < e;)
      *d++ = *s++;
  else if (source < dest)
    for (s = (char *) source + len, d = (char *) dest + len; s > source;)
      *--d = *--s;
  return dest;
}
