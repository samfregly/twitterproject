Objectives 
	Automated publishing 
	WEB publishing 
	Client/Server architecture 
 
Automated Publishing Approach 
	Utlize RECONPlus database as source 
	Utilize FQM scripts to support DB access 
	Utilize Interleaf macros for composition 
 
WEB Publishing Approach 
	Composition for electronic publication 
	PDF generation with Acrobat Distiller 
	Automatic hyperlink creation with Acrobat Exchange plug-in 
	Update full text index 
	Provide access via a WEB server 
 
Implementing a Client/Server Architecture 
	Migrate publishing database to UNIX 
	Utilize UNIX based server software 
	Procure and develop clients 
 
Project Elements 
 
Automated Publishing Processing Stages 
	Precomposition 
	Composition 
	Output 
	Administration 
 
Precomposition Process 
	Citation item selection process 
	Extraction and ordering of citation elements for input into Interleaf 
 
Automating Citation Item Selection 
	Automate with FQM proc 
	Query's select items 
	Interactive review, addition, and deletion of selected items 
	Selected items saved 
	Allow repetitive review and modification 
 
Automating Extraction and Ordering of Citation Elements 
	Utilize saved selected item set 
	FQM proc extracts and orders citation data according to business rules 
	ASCII file created by FQM proc is marked up to work with automated citation 
composition macro 
 
Composition 
	Automated composition of citations within Interleaf 
	Quality control check for citations 
	Automated extraction of indexing information from Interleaf 
	Generation of indexes for loading Into Interleaf 
	Atuomated composition of indexes In Interleaf 
	Quality control check for Indexes 
	Final layout (table of contents,...) 
 
Interleaf citation composition macros 
	Written in Interleaf LISP 
	Load citation file prepared during precomposition 
	Utilize templates based on document type 
 
Interleaf index extraction macros 
	Written in Interleaf LISP 
	Executed after citation composition is complete 
	Extract information which will form basis for indexes 
	Create ASCII text files containing index data 
 
Generation of index load files 
	UNIX scripts using "awk" prepare index load files 
	Utilize ASCII text files containin index data as input 
	ASCII file created by UNIX script is marked up to work with automated index 
composition macro 
 
Interleaf index composition macros 
	Written in Interleaf LISP 
	Executed after index load files are prepared 
	Load index load files 
	Utilize templates based on document type 
 
Output Formats 
	Hardcopy 
	PDF 
	HTML 
 
Distribution 
	Hardcopy 
	WEB server 
	BASIS+ Database (RECONPlus) 
 
Electronic Publishing - Output 
	Content modification 
	PDF generation 
	For PDF, generation of PDF native hyperlinks 
	HTML generation 
	Generation of URL based hyperlinks for HTML and PDF 
 
Electronic Publishing Output - Content Modification 
	May change document format for electronic version 
	Single column 
	Font selection 
	Index to citation link via hyperlink 
	May remove indexes 
	Search capability in viewers fullfills index function 
 
Electronic Publishing Output - PDF 
	Create postscript output for document 
	Utilize Acrobat Distiller to create PDF from postscript 
	Utilize Acrobat Exchange with custom plug-in to create hyperlinks 
 
Electronic Publishing - HTML generation 
	Method to be determined 
 
Electronic Publishing - Distribution 
	Load documents onto server 
	Set security 
	Update full text index 
 
Administration 
	Backups 
	System Information 
	Security 
	Retention 
 
Implementation 
 
Strategy 
	Get a success ASAP 
	Implement critical elements first 
	Phase in other automation elements 
	Extended develpment planning 
 
Implementation Phasing 
	Citation selection and composition from database 
	Index creation 
	WEB publishing of simple PDF 
	Internal Hyperlinks in PDF 
	Loading of referenced documents 
	Hyperlinks from citations to documents 
	Client/Server GUI tool development 
 
Project Phases 
	Detailed system design 
	Implementation planning 
	Development 
	Taining 
	Documentation 
	Installation and test 
 
Automated Publication Implementation Schedule and Costs 
	FQM Item selection procs 
	FQM Extraction procs 
	Interleaf citation composition macros 
	Interleaf index extraction macros 
	UNIX index generation scripts 
	Interleaf index composition macros 
 
WEB Publishing Implementation Schedule and Costs 
	PDF generation 
	Loading on WEB Server 
	Hyperlink generation 
	Formatting for WEB 

