Mission Operations Center Routine Procedures  Automation
Concept and Approach
Agenda
Agenda
	Goals
	Problem definition
	A bigger picture
	Approach
	Design elements
Goals
	Simplify MOC reporting processes
	Provide better access to MOC data
	Integrate disparate data collected by MOC
Problem Definition
Problem Definition
	MOC reporting requirements
	Report forms used within a MOC
	MOC Functions to be Addressed
MOC Reporting Requirements
	Routine bookkeeping to keep track of table loads, tape dumps,...
	Daily and weekly summaries
	Problems and Incidents
	Record of pass plan changes
Report forms used witnin a MOC
	Routine bookkeeping
		Pass summaries
		Tape recorder dump log
		Daily status reports
		Daily operations support totals
		Weekly status reports
	Other reports and forms
		Problem and incident reports
		PPCR (Pass Plan Change Request)
MOC Functions to be Addressed
	Paper based reporting system
	Replication of data entry
	Manual quality control process
	Manual routing process
	Access to the reports
	Integration with scheduling process
A Bigger Picture
A Bigger Picture
	MOC processes
	MOC automation efforts
	Key elements
MOC Processes
	Pass plans
	Principal tasks within a pass
	Trend analysis
	Reporting
Pass Plans
	TDRS weekly schedule drives pass plan timing
	Daily schedule is provided to MOC
	Pass plans handle workload from schedule plus standard operations
	Typically 12 passes of 20 to 25 minutes duratiion per day
Principal Tasks Within a Pass
	Daily onboard computer loads
	Data acquisition - capture tape recorder dumps
	Health and safety monitoring
MOC Automation Efforts
	Automation of routine tasks performed within a pass
	Automatic handling of problems and incidents.
	Remote access
	Ground system interface
	Data archival and retrieval
Automation of Routine Tasks Performed Within a Pass
	Loads
	Data Acquisition
	Health and Safety Monitoring
Automatic Handling of Problems and Incidents.
	Detection
	Notification
	Logging
Remote Access
	Plans
	Health and safety reccords
	Reports
	Data
	Security Issues
Approach
Approach
	Immediate problem - Forms and report automation
	Bigger picture - Provide an information architecture integrating MOC data
	Out of scope
	Tool - Required Capabilities
Immediate Problem - Forms and report automation
	Replace paper based forms with electronic forms
	Have system fill in forms and reports with already known information
	Automate rollup for summary reports
	Simplify routing with integrated e-mail and easy repository access
Bigger Picture - Provide an Information Architecture Integrating MOC data
	Single repository which can contain all MOC data
	Information flows between related and dependent data elements should 
be supported by database structure
	Robust repository access to support
Out of Scope
	APOC, VMOC and other programs define solutions for these problems
		Automation of routine processes performed within a pass which 
interface with the spacecraft
		Automatic handling of problems and incidents
Tools - Required Capabilities
	Find tools which support:
		Database functions
		Workgroup interaction
		Remote access
	Utilize COTS products where possible
	Utilize low-cost tools
	Utilize tools compatible with existing infrastructure
Tools - Database Functions
	Storage
	Retrieval
	Reporting
	Document repository capabilities
Tools - Workgroup Interaction Capabilites
	E-mail
	Client/Server architecture
	Workflow support
Tools - Remote access
	Dial-up
	WAN
	Paging
Design Elements
Design Elements
	Toolset
	Information Architecture
	Process Flows
Design Elements - Toolset
	Lotus Notes as the workgroup and repository maintenance tool
	Network Infrastructure
	Notes server
	Client workstations
	Lotus Notes Pager Gateway
Design Elements - Information Architecture
	Notes databases organized by project and forms
	Common keys utilized across databases for selection and reporting 
support
		Project Name
		Pass Number
		Week Number
		Date
		Document Types
	Links to log files
	Links to project documentation
Design Elements - Process Flows
	Routine forms data entry
	Routine generation of reports
	Automatic initial creation of problem and incident reports
	Manual updating of forms and reports
	Support for health and safety monitoring
Process Flows - Routine Forms Data Entry
	Pass Summaries
	Tape logs
	Automatically fill in known information
		Date and time
		Project static data
		Propagation from daily schedule and TDRS schedule
Process Flows - Routine generation of reports
	Generate off from pass summaries and tape logs
	Automated selection of periods
	Summing of totals is automated
	Flexible formatting available
	Ad-hoc reporting capability
Process  Flows - Automatic Initial Creation of Problem and Incident Reports
	Created based on triggers from H & S monitoring system
	Automatically fill in known information
		Supplied by H & S monitoring system
		Date and time
		Project static data
Process Flows - Manual Updating of Forms and Reports
	Notes highlights unread forms which were automatically generated
	Pull-downs of allowable values for fields which are constrained
	Overwrites of pre-filled fields can be enabled
	Security mechanisms to restrict update capabilities
Process Flows - Support for health and safety monitoring
	The VMOC scenario
	Actions triggered based on e-mail interface
	Actions available
		E-mail
		Paging
		Updating of event databases
		Triggering of other work-flows
