From:	TREAS::LEDRSDVLP5    6-AUG-1993 12:52:41.73
To:	LEDRSDVLP4
CC:	
Subj:	Incorporated comments

Subj:   PLEASE REVIEW AND COMMENT ON OUR AGREEMENTS.

Discussions notes from 4 Aug meeting between CENTECH and MEREX.

1. The Administrator and Director Query and Maintenance functions use the same
interface.  When in the Query mode no modifications to the data are allowed.
When in Maintenance the user has the capability to both query (for retrieval)
and modify records. (This is to clarify the functionality of the Maintenance
mode.  No query function is specified but it is being provided.)

The user will have a menu pop-up at the bottom of the screen during "Query
Mode" (except for in the Documents Screen) which will have options like:

	Start-Query     Revise-Query    New-Query       Cancel

Query mode is the default mode when the screen is entered from both the ad-hoc
query function or the data maintenance function. After a query is completed, 
th user is placed into "Review Mode". If Review Mode is entered from an ad-hoc 
query following menu is presented:

	Next    Previous     Revise-Query       New-Query       Cancel

After completion of a query initiated in data maintenance mode, the Review
Mode menu is the same as the above menu except for an additional option,
"Modify-Entry" which is used to allow modification of the currently displayed
entry. If this is selected, the user may edit the entry, and on completion
is presented with a menu containing the following selections.

	Save-Changes    Abandon-Changes        Cancel

Exiting from this screen with Save-Changes or Abandon-Changes returns the
user to Review Mode. 

Note that selecting "Cancel" from any of the above menu's returns the user
to the either the main menu if they were performing an ad-hoc query, or to
the data maintenance screen if they came in that way.

2. The word "Review" which preceeds "Attorney" on several of the screen will be
removed.*

3. An additional "Attorney" field will be added to the screens which currently
display an "Attorney" and a "Review Attorney".*  The user will be required to
enter the Attorney code.  The Attorney name will be provided by the system.
The Attorney listed on the first line will be have the attorney role "LEAD
ATTORNEY".  Attorney role is never displayed but it it needed for processing
the reports - only list statistics for the LEAD ATTORNEY are included
in the reports.

4. When documents are being uploaded-downloaded, a message "loading document"
will be displayed. This is being done to assure the user that things are
happening.

5. Ad-hoc queries will be constructed using "and" as the connector between
fields.  For fields with inclusive indices, the structure will be "includes"
and each of the separate entries will be on a separate line in the display box.

6. On the Document screen, we plan to remove the sort parameter.  It does not
make sense to sort since only one document can be viewed at a time.

7. For document ad-hoc queries, the user will be given the choice of looking at
header information or text information.  If header information is selected, the
user will view the results from the document screen.  If text is selected it
will be as described in item 8. When viewing the headers, the menu's will be
the same as noted for item 2 above, except an option "View-Document" will
be added during Review Mode.

8. The document will be displayed and the user can browse through the text.

9. On the Data Maintenance menu for the administrator, Load Documents into
queue - The Attorney will load the document into the queue.  The Director will
be provided with a scrolling list of documents.  We plan to use the Document ID
and Subject as the scrolling list.  A function key will be supplied which
accepts the document for loading.  The Administrator will then kick-off the
process for loading.

10. On the Reports menu, an additional field, called "Special Reports" will be
provided.  To the left of it will be a popup which lists all special reports. 
If the user selects one of these reports, the report will be produced.

11. We decided that it was out-of-scope to provide a special screen for
document query.  (A reordering of fields was requested by the end user.* This
is a multi-use screen also used for Document Loading.)  If the user would like
for this feature to be added during maintenance, that can be done.

12. An additional screen is provided to the Attorneys to allow them to look at
and/or enter the date the draft was completed.  This date is needed on the
reports and the information is used in creating the reports. The screen will
include the following title and fields:

		LEDRS Assignment Maintenance / Query

		Attorney Code: 
		Document ID:
		Profile ID:
		Draft Submitted Date:    (The field name is "completed_date")
		
13.  The Attorney menu's are modified to support querying and maintenance of
assignments as follows:

	Menu                    Mod
	----                    ---
	Main                    Option "Modify Assignments" is added.
	Main->Ad-Hoc Queries    Option "Query Assignments" is added.

14. The Director's lower level menu processing is modified as follows:

Director->Data Maint->Mark (Documents or Profiles) For Delete

The director is placed into the appropriate query screen as though doing
an ad-hoc query. After querying and entering Review mode, the screen contains
an additional option, "Mark-For-Delete".

15. The following information is automatically provided by the system:
For Assignments:
  a. If ASSIGNMENT_COMPLETION_DATE is added, the COMPLETION ACTION is set to
	"DRAFT SUBMITTED".
For Profile:
  a. If a DUE_DATE is entered and no status is provided, the STATUS = ASSIGNED.
  b. If a profile is added and no due date is included, the STATUS =
	UNASSIGNED.
  c. If the status is assigned and no assigned date is entered the ASSIGNED
	DATE = current date.
  d. If an assigned date is entered and no status is provided, the STATUS =
	ASSIGNED.
  e. If a closed_date is entered, and no status is provided, the STATUS = 
	CLOSED.
  f. If an extended due date is entered and the status not = closed or
	canceled,  the STATUS = REVISE.
  g. If no status is entered when the profile is added and the assigned date
	is not entered, the status is set to UNASSIGNED.


* Note: At the request of Ken Smalzbach during the 3 Aug. 1993 discussions between 
Sharon Terango, Ken Schmalzbach, and Pat Braden  

