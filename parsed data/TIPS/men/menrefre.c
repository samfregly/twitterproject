/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_refresh					menrefre.c

Function : Refreshes displayed menu.

Author : A. Fregly

Abstract : This function is used to update the displayed version of a menu
	so that it matches the current in-memory version.

Calling Sequence : int stat = men_refresh(men_s_menu *men);

  men	Menu structure created by call to men_init.
  stat	Returned status, a 0 value indicates an error occurred.

Notes: 

Change log : 
000	24-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_refresh(struct men_s_menu *men)
#else
int men_refresh(men)
  struct men_s_menu *men;
#endif
{
  wrefresh(men->win);
  if (men->optwin != men->win) {
    wrefresh(men->optwin);
  }
}
