/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_addopt					menaddop.c

Function : Adds an option to a virtual menu.

Author : A. Fregly

Abstract : This function is used to add an option to a virtual menu. 

Calling Sequence : int stat =  men_addopt(struct men_s_menu *menu, char *optval,
  int datalen, void *data);

  menu		Menu to which option is to be added.
  optval	Value of option.
  datalen	Length of associated data buffer.
  data		Data buffer associated with option.

Notes: 

Change log : 
000  14-MAR-92  AMF	Created.
001	23-MAR-93  AMF	Compile under GNU C.  
002	26-MAR-93  AMF	Compile under MW C.
003	11-SEP-93  AMF	Fix return. Put in debug.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <men.h>

#if FIT_ANSI
int men_addopt(struct men_s_menu *menu, char *optval, int datalen, void *data)
#else
int men_addopt(menu, optval, datalen, data)
  struct men_s_menu *menu; 
  char *optval;
  int datalen; 
  char *data;
#endif
{

  int i, retstat, len, offset;
  struct men_s_option *opt;
  char *tempbuf;

  if (!(menu->menutype & MEN_ATTR_VIRTUAL)) {
#if MEN_DEBUG
    fprintf(fit_debug_handle, "men_addopt, add to non-virtual menu\n");
#endif
    return 0;
  }

  if (menu->nopts == 0) {
    /* No entries in menu, open work file */
    if (!(menu->vfile = drct_tmpopen())) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_addopt, error opening virtual buffer file\n");
#endif
      return 0;
    }

    /* Set size of option buffer to 10 times the number that can be
       displayed on the screen */
    menu->nvopt = menu->nrows * menu->ncols * 10;
    if (!(menu->options = (struct men_s_option *) calloc(menu->nvopt, 
	sizeof(*(menu->options))))) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_addopt, error allocating in memory buffer\n");
#endif
      goto ALLOCERR;
    }

    for (i=0; i < menu->nvopt; ++i) {
      /* Initialize the elements of the in memory buffer which are fixed in
	 length */
      if (!(menu->options[i].val = (char *) calloc(menu->maxoptlen+1, 1))) 
	goto ALLOCERR;
      if (!(menu->options[i].highmap = 
	  (unsigned char *) calloc(menu->maxoptlen / 8 + 1, 1))) {
#if MEN_DEBUG
	fprintf(fit_debug_handle, "men_addopt, error allocating option buffer entry\n");
#endif
	goto ALLOCERR;
      }
      menu->options[i].descr.len = 0;
      menu->options[i].descr.row = i / menu->ncols;
      menu->options[i].descr.col = menu->scol + ((i % menu->ncols) * 
	menu->colwidth);
      menu->options[i].descr.stat = 0;
      menu->options[i].descr.dispstat = 0;
      menu->options[i].descr.nrows = 1;
      menu->options[i].descr.hot = 0;
      menu->options[i].descr.datalen = 0;
      menu->options[i].data = NULL;
    }

    /* Set range of what options are buffered to nothing */
    menu->fvopt = 0;
    menu->lvopt = menu->nvopt - 1;
  }

  if (menu->nopts > menu->lvopt)
    /* Option to be added can't go in buffer, reset buffer so it can */
    men_setvopt(menu, menu->nopts - (menu->nvopt / 2));

  /* Find the option in the buffer which will be initialized */
  opt = &menu->options[menu->nopts - menu->fvopt];

  /* Set the value */
  strncpy(opt->val, optval, menu->maxoptlen);

  /* Initialize the highlight bit map to zeros */
  opt->val[menu->maxoptlen] = 0;
  opt->descr.len = menu->maxoptlen;
  fit_strim(opt->val, (unsigned int *) &(opt->descr.len));
  memset(opt->highmap, 0, menu->maxoptlen / 8 + 1);

  retstat = 1;

  /* Allocate the user data buffer */
  opt->descr.datalen = datalen;
  if (datalen) 
    if (opt->data != NULL) free(opt->data);
    if ((opt->data = (char *) calloc(datalen, 1)) == NULL) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_addopt, error allocating user data buffer entry\n");
#endif
      opt->descr.datalen = 0;
      goto ALLOCERR;
    }

  /* Copy the user data into the in memory buffer */
  if (datalen) memcpy(opt->data, data, datalen);

  /* Determine length of record to be written to work file */
  len = sizeof(opt->descr) + menu->maxoptlen + opt->descr.datalen +
      menu->maxoptlen / 8 + 1;

  /* Allocate a buffer for the record */
  if (retstat = ((tempbuf = (char *) calloc(len, 1)) != NULL)) {
    /* Fill the buffer */
    memcpy(tempbuf, &(opt->descr), sizeof(opt->descr));
    offset = sizeof(opt->descr);
    memcpy(tempbuf + offset, opt->val, menu->maxoptlen);
    offset += menu->maxoptlen;
    memcpy(tempbuf + offset, opt->highmap, menu->maxoptlen / 8 + 1);
    if (datalen) {
      offset += (menu->maxoptlen / 8 + 1);
      memcpy(tempbuf + offset, data, datalen);
    } 
    /* Add the record to the end of the work file */
    retstat = drct_append(menu->vfile, tempbuf, len, (long) len);

    /* Free the temporary buffer */
    free(tempbuf);
  }
  else {
#if MEN_DEBUG
    fprintf(fit_debug_handle, "men_addopt, error allocating record buffer\n");
#endif
    ;
  }
  goto DONE;

ALLOCERR:
  retstat = 0;

DONE:
  if (retstat) menu->nopts += 1;
  return retstat;
}
