/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_remove					menremov.c

Function : Removes a menu from the display.

Author : A. Fregly

Abstract : This function will remove a menu from the display. The menu
	window is erased, and if a parent window is specified, the area
	occupied by the menu in the parent window is restored with the
	saved value for the area.

Calling Sequence : int stat = men_remove(struct men_s_menu *men, 
    WINDOW *parent);

  men		Menu structure created by men_init.
  parent	Parent window to overlay on top of menu, or NULL if the
		menu has no parent.

Notes: 

Change log : 
000	24-AUG-91  AMF	Created.
001	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_remove(struct men_s_menu *men, WINDOW *parent)
#else
int men_remove(men, parent)
  struct men_s_menu *men;
  WINDOW *parent;
#endif
{

  if (parent) {
    overwrite(men->savewin, parent);
    touchwin(parent);
    wrefresh(parent);
  }
  else {
    werase(men->win);
    touchwin(men->win);
    wrefresh(men->win);
  }
  return 1;
}
