/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_repvopt					menrepvo.c

Function : Updates item in the virtual memory buffer for menu.

Author : A. Fregly

Abstract : This function is used to update an entry in the buffered options
	for a virtual menu.

Calling Sequence : int stat = men_repvopt(struct men_s_menu *menu, int idx, 
	struct men_s_option *opt);

  menu		Menu structure for menu.
  idx		Index of buffered option to replace.
  opt		New value to put into option.
  stat		Returned status, 0 indicates an error.

Notes: 

Change log : 
000	15-MAR-92  AMF	Created.
001	26-MAR-93  AMF	Compile under Coherent/MW C compiler.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <men.h>
#if FIT_BSD
#include <string.h>
#endif

#if FIT_ANSI
int men_repvopt(struct men_s_menu *menu, int idx, struct men_s_option *opt)
#else
int men_repvopt(menu, idx, opt)
  struct men_s_menu *menu;
  int idx;
  struct men_s_option *opt;
#endif
{

  if (idx < 0 || idx >= menu->nvopt) return 0;
  if (menu->options+idx == opt) return 1;

  if (menu->options[idx].descr.datalen && menu->options[idx].descr.datalen <
	opt->descr.datalen) {
    free(menu->options[idx].data);
    menu->options[idx].data = NULL;
    menu->options[idx].descr.datalen = 0;
  }

  menu->options[idx].descr = opt->descr;
  memcpy(menu->options[idx].val, opt->val, 
	min(menu->maxoptlen, opt->descr.len+1));
  menu->options[idx].val[menu->maxoptlen] = 0;
  memcpy(menu->options[idx].highmap, opt->highmap, menu->maxoptlen / 8 + 1);
  if (opt->descr.datalen) {
    if (menu->options[idx].data == NULL)
      if ((menu->options[idx].data = (char *) calloc(opt->descr.datalen, 1)) ==
	  NULL) {
	menu->options[idx].descr.datalen = 0;
        return 0;
    }
    memcpy(menu->options[idx].data, opt->data, opt->descr.datalen);
  }
}
