/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_reset						menrese.c

Function : Resets menu to "initial" state.

Author : A. Fregly

Abstract : This function is used to reset a menu to it's "init" state. In
	the init state, the contents of the menu window are reset so that
	the first available option is the current option in the menu, and
	is positioned at the top of the menu, and all selected options are 
	unselected. This function does not update the screen display however. 
	In order to do that, the caller should use the appropriate curses 
	functions.

Calling Sequence : int stat = men_reset(men_s_menu *men);

  men	Menu to be reset.
  stat	Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	23-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_reset(struct men_s_menu *men)
#else
int men_reset(men)
  struct men_s_menu *men;
#endif
{
  long i;

  /* Clear "on" setting for all options */
  for (i=0; i < men->nopts; ++i) {
    men_opt(men, i)->descr.stat = 0;
    if (men->menutype & MEN_ATTR_VIRTUAL)
      men_update(men, i);
  }

  /* Reset the menu so that the top option is the first option and the
     current option */
  men->firstopt = 0;
  men->curopt = 0;

  /* Now fill in the menu */
  return men_draw(men);
}
