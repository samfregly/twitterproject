/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_dealloc					mendeall.c

Function : Deallocates storage allocated to menu by men_init.

Author : A. Fregly

Abstract : This function is used to deallocate the storage allocated for
	a menu by men_init. Note that the window in which the menu was
	displayed is not deleted.

Calling Sequence : int stat = men_dealloc(struct men_s_menu *(*menu));

  menu		Pointer a menu structure created by call to men_init.
  stat		Returned status, 0 indicates an error.

Notes: 

Change log : 
000	02-SEP-91  AMF	Created.
001	13-MAR-92  AMF	Support virtual menus.
002	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_dealloc(struct men_s_menu *(*menu))
#else
int men_dealloc(menu)
  struct men_s_menu *(*menu);
#endif
{

  int i;

  if (*menu) {
    if ((*menu)->options) {
      for (i = 0; i < (*menu)->nvopt; ++i) {
        if ((*menu)->menutype & MEN_ATTR_VIRTUAL) {
          if ((*menu)->options[i].val)
	    free((*menu)->options[i].val);
	  if ((*menu)->options[i].data != NULL)
	    free((*menu)->options[i].data);
        }
        if ((*menu)->options[i].highmap)
	  free((*menu)->options[i].highmap);
      }
      free((*menu)->options);
    }
    if ((*menu)->label)
      free((*menu)->label);
    if ((*menu)->optwin != (*menu)->win && (*menu)->optwin)
      delwin((*menu)->optwin);
    if ((*menu)->savewin)
      delwin((*menu)->savewin);
    if ((*menu)->workbuf)
      free((*menu)->workbuf);
    if (((*menu)->menutype & MEN_ATTR_VIRTUAL) && (*menu)->vfile)
      drct_close((*menu)->vfile);
    free(*menu);
    *menu = (struct men_s_menu *) NULL;
  }
  return 1;
}
