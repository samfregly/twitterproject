/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_draw						mendraw.c

Function : Redraws menu contents to match current menu status.

Author : A. Fregly

Abstract : This function will update the contents of the menu window to match
	the  current state of the menu as defined by the current contents
	of the men structure. The on-screen version of the menu remains
	unchanged until the caller uses the appropriate curses functions
	on the menu window.

Calling Sequence : int stat = men_draw(struct men_s_menu *men);

  men	Menu to be updated.
  stat	Returned status, a 0 value indicates an error occurred.

Notes: 

Change log : 
000	23-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <win.h>
#include <men.h>
#if FIT_BSD
#include <string.h>
#endif

#if FIT_ANSI
int men_draw(struct men_s_menu *men)
#else
int men_draw(men)
  struct men_s_menu *men;
#endif
{
  int row, i, len, nrows, ncols;
  long optidx;

  if (men->optwin != men->win)
    touchwin(men->win);
#if FIT_TERMINFO
  wattrset(men->win, men->normal);
#else
  wstandend(men->win);
#endif
  win_fill(men->win, 0, 0, 0, 0, ' ');
  if (men->optwin != men->win) {
#if FIT_TERMINFO
    wattrset(men->optwin, men->normal);
#endif
    win_fill(men->optwin,0,0,0,0,' ');
  }

  if (men->menutype & MEN_ATTR_BOXED)
  {
    win_box(men->win);
  }

  if (men->label != NULL) {
    getmaxyx(men->win, nrows, ncols);
    len = max(0,strlen(men->label));
    mvwaddstr(men->win, 0, (ncols - len) / 2, men->label);
  }

  for (row = 0, optidx = men->firstopt; row < men->nrows &&
	optidx < men->nopts; ++row)
    for (i=0; i < men->ncols && optidx < men->nopts; ++i, ++optidx) {
      men_putopt(men, optidx);
    }

  return 1;
}
