/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_update					menupdat.c

Function : Updates workfile for virtual menu with contents of menu entry.

Author : A. Fregly

Abstract : This function will update the work file being used for a virtual
	menu with the  contents of the in memory version of a menu option.
	If the specified option is not currently in memory (the caller has
	screwed) or the menu is not a virtual menu, this function does
	nothing. It is anticipated that this function will be used to update
	the virtual file immediately after the user has modified the in
	memory version which was accessed with the "men_opt" function.

Calling Sequence : int stat = men_update(struct men_s_menu *menu, long idx);

  menu		Menu structure returned by men_init.
  idx		Index of entry to update.
  stat		Returned status, 0 indicates an error.

Notes: 

Change log : 
000	14-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_update(struct men_s_menu *menu, long idx)
#else
int men_update(menu, idx)
  struct men_s_menu *menu;
  long idx;
#endif
{

  int midx, retstat, offset, len;
  struct men_s_option *opt;
  char *tempbuf;

  if (!(menu->menutype & MEN_ATTR_VIRTUAL) || idx < menu->fvopt || 
	idx > menu->lvopt || idx >= menu->nopts)
    return !(menu->menutype & MEN_ATTR_VIRTUAL);

  midx = idx - menu->fvopt; 
  opt = &menu->options[midx];
  len = sizeof(opt->descr) + menu->maxoptlen + menu->maxoptlen/8 + 1 + 
	opt->descr.datalen;
  retstat = drct_setpos(menu->vfile, idx);
  if (retstat) {
    /* Allocate a buffer for the record */
    if (retstat = ((tempbuf = (char *) calloc(len, 1)) != NULL)) {
      /* Fill the buffer */
      memcpy(tempbuf, &(opt->descr), sizeof(opt->descr));
      offset = sizeof(opt->descr);
      memcpy(tempbuf + offset, opt->val, menu->maxoptlen);
      offset += menu->maxoptlen;
      memcpy(tempbuf + offset, opt->highmap, menu->maxoptlen / 8 + 1);
      if (opt->descr.datalen) {
        offset += (menu->maxoptlen / 8 + 1);
        memcpy(tempbuf + offset, opt->data, opt->descr.datalen);
      }

      retstat = drct_rewrite(menu->vfile, tempbuf, len, (long) len);
      free(tempbuf);
    }
  }
  return retstat;
}
