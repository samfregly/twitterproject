/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_select					menselec.c

Function : Allows user to select from a menu

Author : A. Fregly

Abstract : This function allows a user to select from a menu. If the menu
	allows multiple items to be selected, the user is left in the menu
	until an exit key is entered. Otherwise, the user is exited from
	the menu as soon as a menu entry is selected. The function keys
	which may be used during menu selection include:

	Right-arrow	Move to next option to right or below.
	Left-arrow	Move to next option to left or up.
	Up-arrow	Move to next option up or to left.
	Down-arrow	Move to next option down or to right.
	Page-up		Page up within scrolling menu or home to first.
	Page-down	Page down within scrolling menu or go to end.
	Home		Move to first entry.
	End		Move to last entry.
	Space		Toggle selection of option in multi-select menu.
	Return		Exit menu, selecting current entry if not multi-select
			menu.
	TERMINFO defined exit/quit keys
			Exit menu, not selecting current option.
	*		Toggle selection of all options in a multi-select menu.
	>		Toggle selections from current option to end of 
			consecutive selected or unselected entries.
	<		Toggle selections from first of cosecutive or unselected
			entries to current option.

	Menu selection options are enabled based on the bit settings in the
	menu type mask. The following bits are defined (in men.h>

	MEN_ATTR_HORIZONTALWRAP	Current option wraps to other side of the menu
			if user uses a left or right arrow which would move the
			curser past the left or right  side of the menu.

	MEN_ATTR_VERTICALWRAP	Current option wraps to other end of menu if 
			user uses an up or down arrow which would move the
			curser past an end of the menu.

	MEN_ATTR_MULTSEL Multiple items may be selected.


	MEN_ATTR_HOTKEYS Entering the "hot key" for a menu item causes an
	immediate jump to the next option which has the hot key.

	MEN_ATTR_INSTANT Menu is exited with the hot key item selected when
	a hot key is entered.


Calling Sequence : int stat = men_select(struct men_s_menu *menu, int timout,
  long *curopt, int *optselflag);

  menu		Menu structure returned by men_init.
  timout	>0, then number of seconds user must be inactive for a timeout
		to occur.
  curopt	Current option at time of return.
  optselflag	Non-zero indicates user exited with current option selection
		key.
  stat		Returned exit key, or -1 if a "quit" key was entered.

Notes: 

Change log : 
000	27-AUG-91  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_select(struct men_s_menu *menu, int timout, long *curopt, 
  int *optselflag, int *timedout)
#else
int men_select(menu, timout, curopt, optselflag, timedout)
  struct men_s_menu *menu;
  int timout;
  long *curopt;
  int *optselflag, *timedout;
#endif
{

  int key, stat;
#if MEN_DEBUG
  int tempkey;
#endif

  *timedout = 0;
  stat = 1;
  *optselflag = 0;

#if MEN_DEBUG
  fprintf(fit_debug_handle, "men_select, menu->nopts: %ld\n", menu->nopts);
#endif

  if (menu->nopts < 0) return 0;

  if (menu->curopt < 0)
    menu->curopt = 0;
  else if (menu->curopt >= menu->nopts)
    menu->curopt = menu->nopts - 1;

  men_putopt(menu, menu->curopt);
#if MEN_DEBUG
  fprintf(fit_debug_handle, "men_select, called men_potopt(%ld)\n", menu->curopt);
#endif
  men_refresh(menu);

  while(1) {
    if (timout > 0) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_select, setting alarm\n");
#endif
      signal(SIGALRM, men_timeralarm);
      alarm(timout);
    }

    key = wgetch(menu->win);

    if (timout > 0) *timedout = !alarm(0);

    if (*timedout || key != ERR) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_select, read key: %d\n", key);
      fflush(fit_debug_handle);
#endif
      *timedout = 0;
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_select, calling men_dokey\n");
      fflush(fit_debug_handle);
#endif
      stat = men_dokey(menu, key, optselflag);
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_select, calling men_refresh\n");
      fflush(fit_debug_handle);
#endif
      men_refresh(menu);
    }

    *curopt = menu->curopt;

#if MEN_DEBUG
    fprintf(fit_debug_handle, "men_select, end processing: %d...\n",
      !stat || *optselflag || *timedout);
    fprintf(fit_debug_handle, "  stat: %d, optselflag: %d, timedout: %d\n",
	stat, *optselflag, *timedout);
#endif

    if (!stat || *optselflag || *timedout) break;
  }
  return key; 
}
