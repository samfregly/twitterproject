/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_setvopt					mensetvo.c

Function : Sets contents of in memory buffer for virtual menu.

Author : A. Fregly

Abstract : This function is use to make sure that the in memory buffer of
	virtual menu options starts with a particular option.

Calling Sequence : int stat = men_setvopt(struct men_s_menu *menu, long sidx);

  menu		Menu structure.
  sidx		Starting index of first entry in buffer.
  stat		Returned status, a value of 0 indicates an error occurred.

Notes: 

Change log : 
000	14-MAR-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_setvopt(struct men_s_menu *menu, long sidx)
#else
int men_setvopt(menu, sidx)
  struct men_s_menu *menu;
  long sidx;
#endif
{

  long i, j, offset, eidx;

  if (sidx > menu->fvopt) {
    menu->fvopt = sidx;
    offset = menu->lvopt - menu->fvopt;
    for (i=0, j = menu->nvopt - offset - 1;  j < menu->nvopt; ++i, ++j)
      men_repvopt(menu, (int) i, &menu->options[j]);
    eidx = min(menu->nopts, menu->nvopt);
    for (; i < eidx; ++i)
      men_getvopt(menu, menu->fvopt + i);
  }
  else if (sidx < menu->fvopt) {
    offset = menu->fvopt - sidx;
    menu->fvopt = sidx;
    for (i = menu->nvopt - offset - 1, j = menu->nvopt - 1; i >= 0; --i, --j) 
      men_repvopt(menu, (int) j, &menu->options[i]);
    for (; j >= 0; --j)
      men_getvopt(menu, menu->fvopt + j);
  }
  menu->lvopt = menu->fvopt + menu->nvopt - 1;
  return 1;
}
