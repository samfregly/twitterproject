/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : Performs menu movement function based on key value.

Function : 

Author : A. Fregly

Abstract : This function will perform a "cursor movement" function within the
	menu, changing the current option based on the input key. The previous
	"current" option is unhighlighted, and the new one highlighted. If the
	new option is not currently displayed in the menu window, the window
	is updated so that the option is displayed. The function keys used
	for cursor movement are described in the header for men_select.

Calling Sequence : int stat = men_move(struct men_s_menu *menu, int inkey, 
    long *retopt, int *optselflag);

  menu		Menu structure returned by men_init.
  inkey 	Key which invoked movement function.
  retopt	Returned "current" option.
  stat		Returned status. A value of 0 indicates an error.

Notes: 

Change log : 
000	28-AUG-91  AMF	Created.
001	26-MAR-93  AMF	Remove unused variables.
******************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_move(struct men_s_menu *menu, int inkey, long *retopt, int *optselflag)
#else
int men_move(menu, inkey, retopt, optselflag)
  struct men_s_menu *menu;
  int inkey;
  long *retopt;
  int *optselflag;
#endif
{
  int count, stat;
  long curopt, nextopt, tempopt;
  int horizontalwrap, verticalwrap;
#if MEN_DEBUG
  int tempkey; 
  int r,c,y,x;
#endif

  curopt = menu->curopt;
  *retopt = -1;
  *optselflag = 0;
  stat = 1;

  horizontalwrap = menu->menutype & MEN_ATTR_HORIZONTALWRAP;
  verticalwrap = menu->menutype & MEN_ATTR_VERTICALWRAP;


  if (curopt >= menu->firstopt && curopt < menu->firstopt + menu->ncols *
      menu->nrows) {
#if MEN_DEBUG
  getbegyx(menu->win,y,x);
  getmaxyx(menu->win,r,c);
  fprintf(fit_debug_handle,"mem_move, menu->win coordinates: (x:%d,y:%d), rows: %d, columns: %d\n",
    x,y,r,c);
  getbegyx(menu->optwin,y,x);
  getmaxyx(menu->optwin,r,c);
  fprintf(fit_debug_handle,"mem_move, menu->optwin coordinates: (x:%d,y:%d), rows: %d, columns: %d\n",
    x,y,r,c);
  fflush(fit_debug_handle);
  fprintf(fit_debug_handle,"men_move, calling men_putopt(menu,%ld)\n", curopt); 
  fflush(fit_debug_handle);
#endif
    men_putopt(menu, curopt);
  }

  switch (inkey) {
    case KEY_DOWN :
      curopt += menu->ncols;
      stat = curopt < menu->nopts || verticalwrap;
      break;
    case KEY_UP :
      curopt -= menu->ncols;
      stat = curopt >= 0 || verticalwrap;
      break;
#if FIT_TERMINFO
    case KEY_RIGHT : case KEY_SRIGHT : case KEY_NEXT : case KEY_SNEXT :
#else
    case KEY_RIGHT : : case KEY_NEXT : 
#endif
      curopt += 1;
#if FIT_TERMINFO
      stat = ((inkey != KEY_RIGHT && inkey != KEY_SRIGHT) ||
#else
      stat = ((inkey != KEY_RIGHT) ||
#endif
	(curopt % menu->ncols) || horizontalwrap);
      break;
#if FIT_TERMINFO
    case KEY_LEFT : case KEY_SLEFT : case KEY_PREVIOUS : case KEY_SPREVIOUS :
      stat = ((inkey != KEY_LEFT && inkey != KEY_SLEFT) ||
#else
    case KEY_LEFT : case KEY_PREVIOUS : 
      stat = ((inkey != KEY_LEFT && inkey != KEY_SLEFT) ||
#endif
	(curopt % menu->ncols) || horizontalwrap);
      curopt = curopt - 1;
      break;
#if FIT_TERMINFO
    case KEY_HOME : case KEY_SHOME : case 'b' - 'a' + 1 :
#else
    case KEY_HOME :  case 'b' - 'a' + 1 :
#endif
      curopt = 0;
      break;
#if FIT_TERMINFO
    case KEY_END : case KEY_SEND : case 'e' - 'a' + 1 :
#else
    case KEY_END : case 'e' - 'a' + 1 :
#endif
      curopt = menu->nopts - 1;
      break;
    case KEY_NPAGE : case 'n' - 'a' + 1 :
      curopt += menu->nrows * menu->ncols;
      if (curopt >= menu->nopts) curopt = menu->nopts-1;
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_move, paging forward\n", curopt); 
  fflush(fit_debug_handle);
#endif
      men_putopt(menu, menu->firstopt + 2 * menu->ncols * menu->nrows - 1);
      break;
    case KEY_PPAGE : case 'p' - 'a' + 1 :
      curopt -= menu->nrows * menu->ncols;
      if (curopt < 0) curopt = 0;
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_move, paging back\n", curopt); 
  fflush(fit_debug_handle);
#endif
      men_putopt(menu, menu->firstopt - menu->ncols * menu->nrows);
      break;
#if FIT_TERMINFO
    case LF : case CR : case KEY_ENTER :
#else
    case KEY_ENTER :
#endif
      *optselflag = 1;
      break; 
#if FIT_TERMINFO
    case CTRLD : case CTRLZ : case KEY_EXIT : case KEY_CANCEL : case KEY_CLOSE :
    case KEY_SCANCEL : case KEY_SEXIT :
#else
    case KEY_EXIT : case KEY_CANCEL : case KEY_CLOSE : case CTRLD : case CTRLZ :
#endif
      stat = 0;
      break; 
#if FIT_TERMINFO
    case CTRLW : case KEY_REFRESH :
#else
    case KEY_REFRESH : case CTRLW :
#endif
      endwin();
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_move, refreshing window\n", curopt); 
  fflush(fit_debug_handle);
#endif
      wrefresh(menu->win);
      break;
    default :
      stat = 0;
      if (!(menu->menutype & MEN_ATTR_VIRTUAL) && (inkey & 0xFF) == inkey) {
        inkey = toupper(inkey);
        if (menu->menutype & MEN_ATTR_HOTKEYS) {
          for (nextopt = (curopt+1) % menu->nopts, count = 0; 
	      count < menu->nopts; nextopt = (nextopt+1) % menu->nopts, ++count)
	    if (toupper(menu->options[nextopt].descr.hot) == inkey) {
	      *optselflag = 1;
	      break;
	    }
	  if (*optselflag) {
	    curopt = nextopt;
          }
	  stat = *optselflag;
        }
      }
  }

  if (curopt != menu->curopt) {
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_move, selected option changed\n"); 
  fflush(fit_debug_handle);
#endif

    if (stat && menu->curopt >= menu->firstopt && 
	menu->curopt < menu->firstopt + menu->nrows * menu->ncols) {
      tempopt = menu->curopt;
      menu->curopt = -1;
      men_putopt(menu, tempopt);
    }

    if (stat) {
      if (curopt >= menu->nopts)
        curopt = 0;
      else if (curopt < 0)
        curopt = menu->nopts - 1;

      menu->curopt = curopt;
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_move, call men_putopt(menu,curopt=%ld)\n", curopt); 
  fflush(fit_debug_handle);
#endif
      stat = men_putopt(menu, curopt);
    }
  }
  wrefresh(menu->optwin);
  *retopt = menu->curopt;
  return stat;
}
