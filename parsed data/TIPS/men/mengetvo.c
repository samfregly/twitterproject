/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_getvopt					mengetvo.c

Function :  Get option from virtual work file and put into virtual buffer

Author : A. Fregly

Abstract : This function is used to load a specified virtual menu option into
	the virtual option buffer. The function requires the caller to make
	sure that the virtual buffer range of records includes the record to
	be loaded. This is done by setting the values of menu->fvopt and
	menu->lvopt. It is the responsible of the caller to then 
	fill in the virtual buffer with the correct option values. Normally,
	this function is called transparently to applications programs, with
	it's use being hidden in the men_setvopt function, whose use is also
	hidden in the men_opt and men_addopt functions.

Calling Sequence : int stat = men_getvopt(struct men_s_menu *menu, long idx);

  menu		Menu structure.
  idx		Index of option to load into virtual buffer.
  stat		Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	15-MAR-92  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <men.h>

#if FIT_ANSI
int men_getvopt(struct men_s_menu *menu, long idx)
#else
int men_getvopt(menu, idx)
  struct men_s_menu *menu;
  long idx;
#endif
{
  struct men_s_option opt;
  long reclen;
  char *tempbuf;
  int retstat, len;

  if (idx < menu->fvopt || idx >= (menu->fvopt + menu->nvopt) || 
      idx >= menu->nopts || !(menu->menutype & MEN_ATTR_VIRTUAL))
    return 0;

  retstat = drct_rdrec(menu->vfile, idx, (char *) &(opt.descr), 
	sizeof(opt.descr), &reclen);
  if (retstat) {
    len = menu->maxoptlen + menu->maxoptlen/8 + 1 + opt.descr.datalen;
    if (retstat = ((tempbuf = (char *) calloc(len, 1)) != NULL)) {
      retstat = drct_rdrec(menu->vfile, idx, tempbuf, len, &reclen);
      opt.val = tempbuf;
      opt.highmap = (unsigned char *) tempbuf + menu->maxoptlen;
      opt.data = (char *) ((unsigned char *) opt.highmap + menu->maxoptlen/8 + 
	1);
      retstat = men_repvopt(menu, (int) (idx - menu->fvopt), &opt);
      free(tempbuf);
    }
  }
  return retstat;
}
