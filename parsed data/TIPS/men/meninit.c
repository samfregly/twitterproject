/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_init						meninit.c

Function : Initializes a new menu.

Author : A. Fregly

Abstract : This function will initialize a new menu, returning a pointer
	to the new menu structure, or NULL if an error occurs.

Calling Sequence : struct men_s_menu *men = men_init(WINDOW *win, int menutype, 
  char *opts[], int attrnorm, int attrcurhigh, int attrselhigh, int attrhot,
  int curpointer, int checkmark, int minsep, int itemwidth);

  win		Curses window in which menu is to be displayed. Each menu 
		should have it's own window.
  menutype	Bit map of window configuration options. Masks for all 
		available options are found in men.h.
  label		Optional label for menu.
  opts		List of null terminated character strings containing values
		to be displayed in the menu. A 0 length string is used to
		mark the end of the list.
  attrnorm	Curses attributes to be applied to non-selected options and
		the  menu background.
  attrcurhigh	Curses attributes to be applied to "current" option. This
		attribute is added to the attributes for the item if the
		item is currently selected, otherwise it replace the 
		normal attributes of the item.
  attrselhigh	Curses attributes to be applied to selected options.
  attrhot	Curses attributes for menu hot keys.
  checkmark	For "check mark" enabled menus, this is the character to  use
		as a checkmark.
  minsep	Minimum number of characters to use as a separator between
		options in a horizontal menu, or if itemwidth is non-zero,
		the absolute specification of the number of characters to
		use between items. The minimum value of minsep is 1, and does
		not include the space reserved for a check mark if selection
		highlighing by that means is enabled.
  itemwidth	Maximum item width, or 0 if menu is to automatically determine
		the maximum item width.
  men		Returned pointer at created menu structure, or NULL if the
		menu couldn't be created.

Notes: 

Change log : 
000	23-AUG-91  AMF	Created.
001	23-MAR-93  AMF	Compile under Coherent/GNU C.
002	26-MAR-93  AMF	Remove unused variable.
003	11-SEP-93  AMF	Add debug.
******************************************************************************/

#include <curses.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <men.h>

#if FIT_ANSI
struct men_s_menu *men_init(WINDOW *win, int menutype, char *label, 
  char *opts[], int attrnorm, int attrcurhigh, int attrselhigh, int attrhot, 
  int curpointer, int checkmark, int minsep, int itemwidth)
#else
struct men_s_menu *men_init(win, menutype, label, opts, attrnorm, attrcurhigh, 
  attrselhigh, attrhot, curpointer, checkmark, minsep, itemwidth)
  WINDOW *win;
  int menutype;
  char *label;
  char *opts[];
  int attrnorm, attrcurhigh, attrselhigh, attrhot, curpointer, checkmark;
  int minsep,itemwidth;
#endif
{
  struct men_s_menu *men;
  int i, maxoptlen, optlen, label_L;
  int r, c, ncols, winrows, wincols, y, x;
  int availcols, availrows, hotoffset, margins, usescheckmark, colwidth;
  int usespointer;
  char *hotkeys, *hotend, *sval, *spaceptr;


  /* Allocate menu structure and dynamic sized elements of the structure */
  men = (struct men_s_menu *) calloc(1, sizeof(*men));
  if (!men) {
#if MEN_DEBUG
    fprintf(fit_debug_handle, "men_init, error in malloc for menu structure\n");
#endif
    goto ERREXIT;
  }

  men->nvopt = 0;

  margins = !(menutype & MEN_ATTR_LEFTJUST);
  usescheckmark = ((menutype & MEN_ATTR_CHECK) && 1);
  usespointer = ((menutype & MEN_ATTR_POINTER) && 1);

  if (!(menutype & MEN_ATTR_VIRTUAL)) {
    /* Determine length of options value buffer */
    for (men->nopts = 0, maxoptlen = 0; opts[men->nopts] && 
	*opts[men->nopts]; ++men->nopts) {
      optlen = strlen(opts[men->nopts]);
      if (optlen > maxoptlen)
        maxoptlen = optlen;
    } 
    men->nvopt = men->nopts;
  }
  else if (itemwidth)
    maxoptlen = itemwidth;
  else { 
    getmaxyx(win, y, maxoptlen);
    maxoptlen = maxoptlen - (usescheckmark != 0) - (usespointer != 0) - 1;
  }

  men->label = NULL;
  label_L = 0;
  if (label && *label) {
    label_L = strlen(label);
    if ((men->label = (char *) malloc(label_L + 1)))
      strcpy(men->label, label);
    else
      label_L = 0;
  }

  men->win = win;
  men->optwin = win;
  men->vfile = (struct drct_s_file *) NULL;
  men->fvopt = 0;
  men->lvopt = -1;
  getbegyx(win, y, x);
  getmaxyx(win, r, c);
#if MEN_DEBUG
  fprintf(fit_debug_handle, "men_init, creating menu - rows: %d, columns: %d, coordinates: (x=%d,y=%d)\n", r, c, x, y);
  fflush(fit_debug_handle);
#endif
  men->savewin = newwin(r, c, y, x);

  /* Allocate option descriptors and options value buffer */
  if (men->nopts) {
    men->options = (struct men_s_option *) calloc(men->nopts, 
	sizeof(*(men->options)));
    if (!men->options) goto ERREXIT;
  }
  else
    men->options = (struct men_s_option *) NULL;

  /* Initialize global menu parameters */
  men->menutype = menutype;
  men->normal = attrnorm;
  men->curhighlight = attrcurhigh;
  men->selhighlight = attrselhigh;
  men->hothighlight = attrhot;
  men->curpointer = curpointer;
  men->checkmark = checkmark;
  men->curopt = 0;
  men->firstopt = 0;

  if (!men->nopts && !(menutype & MEN_ATTR_VIRTUAL)) {
#if MEN_DEBUG
    fprintf(fit_debug_handle,"men_init, no options defined for static menu\n");
    fflush(fit_debug_handle);
#endif
    return men;
  }

  /* Get window dimensions */
  getmaxyx(win, winrows, wincols);

  /* Determine number of available rows and columns in the window */
  if (!(menutype & MEN_ATTR_BOXED) && !label_L) {
    availcols = c;
    availrows = r;
    men->optwin = win;
  }
  else if (menutype & MEN_ATTR_BOXED) {
    availcols = wincols - 2;
    availrows = winrows - 2;
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_init, creating option window for boxed menu...\n");
      fprintf(fit_debug_handle, "  size: %d rows by %d cols\n", availrows,
	availcols);
      fprintf(fit_debug_handle, "  at row %d, col: %d\n", y+1, x+1);
      fflush(fit_debug_handle);
#endif
    men->optwin = newwin(availrows, availcols, y+1, x+1);
    if (!men->optwin) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_init, error creating option window for boxed menu...\n");
      fflush(fit_debug_handle);
#endif
      goto ERREXIT;
    }
  }
  else {
    availcols = c;
    availrows = r - 1;
    men->optwin = newwin(availrows, availcols, y+1, x);
    if (!men->optwin) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_init, error creating option window for labeled menu\n");
#endif
      goto ERREXIT;
    }
  }

  if (availcols < 0 || availrows < 0) {
#if MEN_DEBUG
      fprintf(fit_debug_handle, "men_init, menu size invalid, availcols: %d, availrows: %d\n", availcols, availrows); 
#endif
    goto ERREXIT;
  }

  if (minsep < 0) minsep = 0;

  if (itemwidth)
    maxoptlen = itemwidth;

  if (maxoptlen <= 0) maxoptlen = 1;

  maxoptlen += ((usescheckmark != 0) + (usespointer != 0));

  /* Make sure maximum option length is not greater than menu width */
  if (maxoptlen > availcols) maxoptlen = availcols;

  colwidth = maxoptlen + minsep;
 
  /* Set maximum number of columns */
  if (!(menutype & MEN_ATTR_HORIZONTAL))
    /* Horizontal menu is disabled, menu will be 1 column */
    ncols = 1;

  else {
    /* Horizontal menu enabled, determine number of columns possible */
    ncols = (availcols + !margins * minsep) / colwidth;
    if (ncols < 1) ncols = 1;

    if (ncols > men->nopts && !(menutype & MEN_ATTR_VIRTUAL))
      ncols = men->nopts;
  } 

  if (!itemwidth && ncols > 1)
    minsep = (availcols - maxoptlen * ncols) / (ncols - !margins);

  colwidth = maxoptlen + minsep;

  men->scol = (usespointer != 0) + (usescheckmark != 0) +
    (minsep+1)/2 * margins;
  men->nrows = availrows;
  men->ncols = ncols;
  men->maxoptlen = maxoptlen - (usespointer + usescheckmark);
  men->workbuf = (char *) calloc(men->maxoptlen+1, 1);
  if (!men->workbuf) {
#if MEN_DEBUG
    fprintf(fit_debug_handle, "Error allocating work buf for menu...\n");
    fprintf(fit_debug_handle, "  size: %d\n",men->maxoptlen+1);
#endif
    goto ERREXIT;
  }
  men->colwidth = colwidth;

  /* Fill in  option descriptors and option value buffer */
  if (!(menutype & MEN_ATTR_VIRTUAL)) {
    for (i = 0, c = 0, r = 0; i < men->nopts; ++i) {
      men->options[i].val = opts[i];
      men->options[i].descr.len = strlen(opts[i]);
      if (men->options[i].descr.len > maxoptlen)
        men->options[i].descr.len = maxoptlen;
      if (!(men->options[i].highmap = 
	  (unsigned char *) calloc(men->maxoptlen/8 + 1, 1))) {
#if MEN_DEBUG
	fprintf(fit_debug_handle, "men_init, error allocating static menu hilite bitmap...\n");
	fprintf(fit_debug_handle, "  entry: %l, size: %d\n", i, men->maxoptlen/8+1);
#endif
	goto ERREXIT;
      }
      men->options[i].descr.row = i/ncols;
      men->options[i].descr.col = men->scol + ((i % ncols) * colwidth);
      men->options[i].descr.stat = 0;
      men->options[i].descr.dispstat = 0;
      men->options[i].descr.nrows = 1;
      men->options[i].descr.hot = 0;
      men->options[i].descr.datalen = 0;
      men->options[i].data = NULL;
    }

    if (menutype & MEN_ATTR_HOTKEYS) {
      /* Determine hot keys */
      hotkeys = (char *) calloc(men->nopts * 2 + 2, 1);

      if (hotkeys != NULL) {
        *(hotkeys) = ' ';
        for (i = 0, hotend = hotkeys+1; i < men->nopts; ++i) {
	  sval = men->options[i].val + strspn(men->options[i].val, " ");
	  hotoffset = strspn(sval, hotkeys);
	  if ((menutype & MEN_ATTR_HIGHFIRST) && 
	      ((spaceptr = strchr(sval,' ')) != NULL && 
      	      spaceptr < sval + hotoffset))
	    hotoffset = strlen(sval);
	  men->options[i].descr.hot = sval[hotoffset];

	  if (!men->options[i].descr.hot)
	    men->options[i].descr.hot = *sval;

	  if (men->options[i].descr.hot) {
            *(hotend++) = tolower(men->options[i].descr.hot);
	    *(hotend++) = toupper(men->options[i].descr.hot);
	  }
        }
        free(hotkeys);
      }
    }
  }

  /* Fill in the initial screen of the menu */
  men_reset(men);

  /* Turn on keypad parsing and turn off automatic scrolling for the window */
  keypad(men->optwin, TRUE);
  scrollok(men->optwin, FALSE);
  leaveok(men->optwin, TRUE);
  if (men->optwin != men->win) {
    keypad(men->win, TRUE);
    scrollok(men->win, FALSE);
    leaveok(men->win, TRUE);
  }

  goto DONE;

ERREXIT:
#if MEN_DEBUG
  fprintf(fit_debug_handle, "men_init, error creating menu, menu deallocated\n");
#endif
  if (men) {
    if (men->options)
      free(men->options);
    if (men->optwin != win && men->optwin)
      delwin(men->optwin);
    if (men->workbuf)
      free(men->workbuf);
    free(men);
    men = (struct men_s_menu *) NULL;
  } 

DONE:
#if MEN_DEBUG
  getbegyx(men->win,y,x);
  getmaxyx(men->win,r,c);
  fprintf(fit_debug_handle,"mem_init, menu->win coordinates: (x:%d,y:%d), rows: %d, columns: %d\n", x,y,r,c);
  getbegyx(men->optwin,y,x);
  getmaxyx(men->optwin,r,c);
  fprintf(fit_debug_handle,"mem_init, menu->optwin coordinates: (x:%d,y:%d), rows: %d, columns: %d\n ",
    x,y,r,c);
  fflush(fit_debug_handle);
#endif
  return men;
}

