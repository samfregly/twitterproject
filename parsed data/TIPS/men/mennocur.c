/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_nocurrent					mennocur.c

Function : Makes current option undefined, unhighlighting "current" option.

Author : A. Fregly

Abstract : This function is used for turning off highlighting of the "current"
	option. Only the highlighting used for "current" option is turned off.
	If the option is currently selected, any applicable highlighting is
	left on.

Calling Sequence : int stat = men_nocurrent(struct men_s_menu *menu)

  menu	Menu structure returned by call to men_init.
  stat	Returned status. A return of 0 indicates an error.

Notes: 

Change log : 
000	28-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_nocurrent(struct men_s_menu *menu)
#else
int men_nocurrent(menu)
  struct men_s_menu *menu;
#endif
{
  long curopt;

  curopt = menu->curopt;
  menu->curopt = -1;

  if (curopt >= menu->firstopt &&
      curopt < menu->firstopt + menu->ncols * menu->nrows)
    return men_putopt(menu, curopt);

  return 1;
}
