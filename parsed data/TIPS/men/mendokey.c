/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_dokey						mendokey.c

Function : Peforms menu movement/selection based on key value.

Author : A. Fregly

Abstract : This function performs an action appropriate for the supplied key.
	Actions which may be performed include movement of the current option,
	selection of an option or option, or unselection of an option or 
	option. The functions keys which may be used with menus are described
	in the header comment for men_select.

Calling Sequence : int stat = men_dokey(struct men_s_menu *menu, int key,
    int *optselflag);

  menu		Menu structure returned by men_init.
  key		Key for which appropriate action is to be performed.
  optselflag	non-zero value returned if exit key is a menu termination
		key which also selects the current option i.e. the enter
		key.

Notes: 

Change log : 
000	27-AUG-91  AMF	Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_dokey(struct men_s_menu *menu, int key, int *optselflag)
#else
int men_dokey(menu, key, optselflag)
  struct men_s_menu *menu;
  int key, *optselflag;
#endif
{

  int curstat, stat;
  long i, retopt;
  struct men_s_option *m;
#if MEN_DEBUG
  int tempkey;
#endif

  *optselflag = 0;

  if (menu->menutype & MEN_ATTR_MULTSEL) {
    if (key == ' ') {
      m = men_opt(menu, menu->curopt);
      m->descr.stat = !m->descr.stat;
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_dokey, Calling men_update\n");
  fflush(fit_debug_handle);
#endif
      men_update(menu, menu->curopt);
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_dokey, Calling men_putopt\n");
  fflush(fit_debug_handle);
#endif
      men_putopt(menu, menu->curopt);
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_dokey, Called men_putopt\n");
  fflush(fit_debug_handle);
#endif
      return 1;
    }
    else if (key == '*') {
      for (i=0; i < menu->nopts; ++i) {
	  men_opt(menu, i)->descr.stat = 1;
	  men_update(menu, i);
      }
      men_draw(menu);
      return 1;
    }
    else if (key == '<') {
      for (i = menu->curopt, m = men_opt(menu, menu->curopt), 
	  curstat = m->descr.stat; i >= 0 && m->descr.stat == curstat;) {
	m->descr.stat = !curstat;
	men_update(menu, i);
	if (--i >= 0) m = men_opt(menu, i);
      }
      men_draw(menu);
      return 1;
    }
    else if (key == '>') {
      for (i = menu->curopt, m = men_opt(menu, menu->curopt), 
	  curstat = m->descr.stat; i < menu->nopts && 
	  m->descr.stat == curstat;) {
	m->descr.stat = !curstat;
	men_update(menu, i);
	if (++i < menu->nopts) m = men_opt(menu, i);
      }
      men_draw(menu);
      return 1;
    }
  }

#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_dokey, Calling men_move\n");
  fflush(fit_debug_handle);
#endif
  stat=men_move(menu, key, &retopt, optselflag);
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_dokey, Calling men_draw\n");
  fflush(fit_debug_handle);
#endif
  //men_draw(menu);
  return(stat);
}
