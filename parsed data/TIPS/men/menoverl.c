/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_overlay					menoverl.c

Function : Overlays a menu on or underneath another menu.

Author : A. Fregly

Abstract : This function is used to dynamically place a vertical child menu on
	the screen relative to a parent menu and it's specified option. For 
	horizontal menu's, the child is placed directly below the specified
	option. For vertical menu's, the child is placed just below and is
	offset to the right of the specified option in the parent. 

Calling Sequence : int retstat = men_overlay(struct men_s_menu *child, 
	struct men_s_menu *parent, long optnum, int vertical);

  child		Menu to be repositioned.
  parent	Parent menu.
  optnum	Option number in parent under which child is to be placed.
  vertical	Specifies that parent menu is a "vertical" menu.

Notes: 

Change log : 
000	24-OCT-91  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_overlay(struct men_s_menu *child, struct men_s_menu *parent, 
  long optnum, int vertical)
#else
int men_overlay(child, parent, optnum, vertical)
  struct men_s_menu *child, *parent;
  long optnum;
  int vertical;
#endif
{

  int parent_x, parent_y, child_x, child_y, retstat;

  if (!parent || !child) return 0;

  optnum = optnum - parent->firstopt;

  getbegyx(parent->win, parent_y, parent_x);

  if (!vertical) {
    child_y = parent_y + 1;
    child_x = max(0, parent_x + parent->scol + optnum * parent->colwidth -
	((child->menutype & MEN_ATTR_BOXED) != 0));
  }
  else {
    child_y = parent_y + 1 + optnum;
    child_x = parent_x + parent->scol + 1;
  }

  child_y += ((parent->menutype & MEN_ATTR_BOXED) != 0);

  retstat = (mvwin(child->win, child_y, child_x) != ERR);
  retstat = (retstat && (mvwin(child->savewin, child_y, child_x) != ERR));
#if MEN_DEBUG
  fprintf(fit_debug_handle,"men_overlay, moved window to coordinates: (x=%d,y=%d)\n",child_x, child_y);
  fflush(fit_debug_handle);
#endif
  if (child->optwin != child->win) {
    retstat = (retstat && (mvwin(child->optwin, child_y + 1, child_x + 1) != ERR));
#if MEN_DEBUG
    fprintf(fit_debug_handle,"men_overlay, moved option window to coordinates: (x=%d,y=%d)\n",child_x+1, child_y+1);
    fflush(fit_debug_handle);
#endif
  }

  return retstat;
}
