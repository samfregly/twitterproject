#include <stdio.h>
#include <fitsydef.h>
#include <win.h>
#include <men.h>
#if FIT_ANSI
int men_dbg_msg(char *msg)
#else
int men_dbg_msg(msg)
  char *msg;
#endif
{
  win_center(men_win_debug, 0, msg);
  wrefresh(men_win_debug);
  getch();
  win_fill(men_win_debug, 0, 0, 0, 0, ' ');
  wrefresh(men_win_debug);
}
