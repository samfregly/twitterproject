/******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_putopt					menputop.c

Function : Updates/Puts option in menu window.

Author : A. Fregly

Abstract : This function is used to update and/or draw an option in the
	menu window. If the option is not in the displayed portion of the
	menu, the menu is scrolled so that the option will be displayed.
	This function does update the on-screend display of the menu. For
	this, the caller should use the appropriate curses functions.

Calling Sequence : int stat = men_putopt(struct men_s_menu *men, long optidx);

  men		Menu containing option to be put.
  optidx	Index of option within menu options.
  stat		Returned status, a 0 value indicates an error occurred.

Notes: 

Change log : 
000	23-AUG-91  AMF	Created.
001	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#include <men.h>
#if FIT_BSD
#include <string.h>
#endif

#if FIT_ANSI
men_putopt(struct men_s_menu *menu, long optidx)
#else
men_putopt(menu, optidx)
  struct men_s_menu *menu;
  int optidx;
#endif
{

  unsigned char *hotpos, *curchar, *opt, *spaceptr, map;
  long oidx, lastidx, rowdif, row;
  int attrmask, i;
  unsigned hotkey;
  struct men_s_option *m;

  /* Make sure option index is valid */
  if (menu->nopts < 1) return 0;

  if (optidx < 0)
    optidx = 0;
  else if (optidx >= menu->nopts)
    optidx = menu->nopts - 1;

  /* Make sure option is viewable */
  if (optidx < menu->firstopt) {
    /* Option precedes the top displayed option */
    rowdif = menu->firstopt / menu->ncols - optidx / menu->ncols;
    if (rowdif >= menu->nrows) {
      /* Option is more than a screen away, redraw with option at top */
      menu->firstopt -= (rowdif * menu->ncols);
      return men_draw(menu);
    }
    else {
      /* Option is less than a screen away, scroll to the option */
      for (; rowdif; --rowdif) {
        wmove(menu->optwin, 0, 0);
	winsertln(menu->optwin);
        wattrset(menu->optwin, menu->normal);
	win_fill(menu->optwin, 0, 0, 1, 0, ' ');
	wrefresh(menu->optwin);
	menu->firstopt -= menu->ncols;
	for (oidx = menu->firstopt; oidx < menu->firstopt + menu->ncols; ++oidx)
	  if (!men_putopt(menu, oidx)) return 0;
      }
    }
    return 1;
  }

  else if (optidx >= menu->firstopt + menu->nrows * menu->ncols) {
    /* Option is past the last displayed option */
    lastidx = menu->firstopt + (menu->nrows - 1) * menu->ncols;
    rowdif = optidx / menu->ncols - lastidx / menu->ncols;
    if (rowdif >= menu->nrows) {
      /* Option is more than a screen away, redraw with option at bottom */
      menu->firstopt = ((optidx / menu->ncols) - menu->nrows + 1) * menu->ncols;
      return men_draw(menu);
    }
    else {
      /* Option is less than a screen away, scroll to it */
      for (; rowdif; --rowdif) {
        wmove(menu->optwin, 0, 0);
	wdeleteln(menu->optwin);
	scrollok(menu->optwin, FALSE);
        wattrset(menu->optwin, menu->normal);
	win_fill(menu->optwin, menu->nrows-1, 0, 1, 0, ' ');
	wrefresh(menu->optwin);
	menu->firstopt += menu->ncols;
	lastidx = menu->firstopt + (menu->nrows - 1) * menu->ncols;
	for (oidx = lastidx; oidx < lastidx + menu->ncols &&
	    oidx < menu->nopts; ++oidx)
	  if (!men_putopt(menu, oidx)) return 0;
      }
    }
    return 1;
  }

  /* Option is in viewable part of window, draw it */
  row = (optidx - menu->firstopt) / menu->ncols;
  m = men_opt(menu, optidx);

  wattrset(menu->optwin, menu->normal);
  wmove(menu->optwin, row, m->descr.col - 
    (((menu->menutype & MEN_ATTR_POINTER) && 1) +
     ((menu->menutype & MEN_ATTR_CHECK) && 1)));

  if (menu->menutype & MEN_ATTR_POINTER) {
    if (optidx == menu->curopt)
      waddch(menu->optwin, menu->curpointer);
    else
      waddch(menu->optwin, ' ' | menu->normal);
  }

  if (menu->menutype & MEN_ATTR_CHECK) {
    if (m->descr.stat)
      /* Put checkmark in position preceeding option value */
      waddch(menu->optwin, menu->checkmark);
    else
      waddch(menu->optwin, ' ' | menu->normal);
  } 

  if (menu->menutype & MEN_ATTR_USEHIGHMAP) {
    /* Blank the area used by the option */
    win_fill(menu->optwin, row, m->descr.col -
      (((menu->menutype & MEN_ATTR_POINTER) && 1) +
      ((menu->menutype & MEN_ATTR_CHECK) && 1)), 1, menu->maxoptlen +
      (((menu->menutype & MEN_ATTR_POINTER) && 1) +
      ((menu->menutype & MEN_ATTR_CHECK) && 1)), ' ' | menu->normal);

    attrmask = menu->normal;
    for (i=0; i < m->descr.len; ++i) {
      map = m->highmap[i/8];
      if (map & (1 << (i % 8)))
	attrmask = menu->selhighlight;
      else
	attrmask = menu->normal;
      waddch(menu->optwin, (unsigned char) m->val[i] | attrmask);
    }
    wattrset(menu->optwin, menu->normal);
  }
  else {
    strncpy(menu->workbuf, m->val, menu->maxoptlen);
    opt = (unsigned char *) menu->workbuf;
    if (menu->menutype & MEN_ATTR_HIGHFIRST) {
      spaceptr = (unsigned char *) strchr((char *) opt,' ');
      if (spaceptr) *spaceptr = 0;
    }

    /* Set pointer at the "hot key" in the option value */
    hotpos = NULL;
    if (m->descr.hot) 
      hotpos = (unsigned char *) strchr((char *) opt, m->descr.hot);

    if ((menu->menutype & MEN_ATTR_HIGHHOT) && *hotpos) {
      /* Highlighting done on hot key only, set display attributes to normal */
      wattrset(menu->optwin, menu->normal);
      attrmask = menu->normal;
    }
    else if (m->descr.stat) {
      /* Option is currently selected, set selected highlighting attribute */
      wattrset(menu->optwin, menu->selhighlight); 
      attrmask = menu->selhighlight;
      if (optidx == menu->curopt) {
        /* Option is current, add the current highlighting attribute */
        wattron(menu->optwin, menu->curhighlight);
        attrmask |= menu->curhighlight;
      }
    } 
    else if (optidx != menu->curopt) {
      /* Option is neither selected or current, set normal attribute */
      wattrset(menu->optwin, menu->normal);
      attrmask = menu->normal;
    }
    else {
      /* Option is current option, set current highlighting attribute */
      wattrset(menu->optwin, menu->curhighlight);
      attrmask = menu->curhighlight;
    }

    if (!hotpos)
      /* No special highlighting for hot key, add the option to the display */
      waddstr(menu->optwin, (char *) opt);
    else {
      /* Special highlighting for hot key, add preceeding characters */
      if (hotpos > opt)
        for (curchar = opt; curchar < hotpos; ++curchar)
  	  waddch(menu->optwin, *curchar);

      /* Set highlighting for hot key */
      if (m->descr.stat)
        wattrset(menu->optwin, menu->selhighlight);
      else
        wattrset(menu->optwin, menu->hothighlight);

      if (optidx == menu->curopt)
        wattron(menu->optwin, menu->curhighlight);

      /* Add the hot key to the display */
      hotkey = (unsigned) *hotpos;
      waddch(menu->optwin, hotkey);

      /* Add any remaining characters in the option value */
      if (*(hotpos+1)) {
        wattrset(menu->optwin,attrmask);
        waddstr(menu->optwin, (char *) hotpos+1);
      }
    }

    /* Reset dispaly attributes to "normal" */
    wattrset(menu->optwin, menu->normal);

    if ((menu->menutype & MEN_ATTR_HIGHFIRST) && spaceptr) {
      *spaceptr = ' ';
      waddstr(menu->optwin, (char *) spaceptr);
    }
  }

  return 1;
}
