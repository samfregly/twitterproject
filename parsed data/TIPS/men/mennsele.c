/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_nselected					mennsele.c

Function : Returns number of items currently selected in menu.

Author : A. Fregly

Abstract : 

Calling Sequence : long n = men_nselected(struct men_s_menu *menu);

  menu		Menu for which count is desired.
  n		Returned number of entries currently selected in menu.

Notes: 

Change log : 
000	24-OCT-91  AMF	Created.
001	24-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
long men_nselected(struct men_s_menu *menu)
#else
long men_nselected(menu)
  struct men_s_menu *menu;
#endif
{
  long count, i;

  count = 0;
  if (menu)
    for (i = 0; i < menu->nopts; ++i)
      count += (men_opt(menu, i)->descr.stat != 0);
  return count;
}
