/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_put						menput.c

Function : Puts menu onto the screen.

Author : A. Fregly

Abstract : This function is used to put a menu onto the screen. 

Calling Sequence : int stat = men_put(struct men_s_menu *men, WINDOW *parent);

  men		Menu structure created by call to men_init.
  parent	Parent window in which menu is displayed. A value of NULL
		should be supplied if the menu does not have a parent window.
  stat		Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000	24-AUG-91  AMF	Created.
001	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <curses.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_put(struct men_s_menu *menu, WINDOW *parent)
#else
int men_put(menu, parent)
  struct men_s_menu *menu;
  WINDOW *parent;
#endif
{
  int retstat;

  if (parent) {
    overwrite(parent, menu->savewin);
    overwrite(menu->win, parent);
  }
  touchwin(menu->win);
  wrefresh(menu->win);
  if (menu->optwin != menu->win) {
    touchwin(menu->optwin);
    wrefresh(menu->optwin);
  }
  return (retstat != ERR);
}
