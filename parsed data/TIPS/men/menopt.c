/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_opt						menopt.c

Function :  Returns option structure for specified menu element.

Author : A. Fregly

Abstract : This function is used by applications programs to retrieve a
	menu option structure from a virtual menu. The men_opt function will
	transparently perform the swapping of data into the virtual options
	buffer if that is needed so the the specified element may be returned.

Calling Sequence : struct men_s_option *opt = men_opt(struct men_s_menu *menu, 
	long idx);

  menu		Menu structure.
  idx		Option index.
  opt		Returned pointer at an option structure containing the value
		of the desired option. The caller should never attempt to
		deallocate the value returned by men_opt. The storage for
		option values is allocated by men_addopt at the time the
		first option is added to a virtual menu, and is freed when
		the virtual menu is deleted with men_dealloc. A NULL value
		is returned if the option could not be retrieved.

Notes: 

Change log : 
000	15-MAR-92  AMF	Created.
001	23-MAR-93  AMF	Compile under Coherent/GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_ANSI
#include <stdlib.h>
#endif
#include <men.h>

#if FIT_ANSI
struct men_s_option *men_opt(struct men_s_menu *menu, long idx)
#else
struct men_s_option *men_opt(menu, idx)
  struct men_s_menu *menu;
  long idx;
#endif
{

  long sidx;

  if (!menu || idx >= menu->nopts) return (struct men_s_option *) NULL;

  if (menu->menutype & MEN_ATTR_VIRTUAL) {
    if (idx < menu->fvopt || idx > menu->lvopt) {
      sidx = idx - menu->nvopt / 2;
      if (sidx < 0) sidx = 0;
      if (!men_setvopt(menu, sidx)) return (struct men_s_option *) NULL;
    }

    idx -= menu->fvopt;
  }
  return menu->options + idx;
} 
