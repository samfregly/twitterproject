/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men_optsel					menoptse.c

Function : Marks option as selected and updates menu display if necessary.

Author : A. Fregly

Abstract : This function will mark a menu option as selected, and update
	the menu window if the option selected is currently visible. Display
	updating would consist of application of any highlighting attributes
	which are enabled for "selected" options.

Calling Sequence : int stat = men_optsel(struct men_s_menu *menu, long *optnum);

  menu		Menu structure returned by men_init.
  optnum	Option number of option to be marked as selected.

Notes: 

Change log : 
000	28-AUG-91  AMF	Created
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#include <men.h>

#if FIT_ANSI
int men_optsel(struct men_s_menu *menu, long optnum)
#else
int men_optsel(menu, optnum)
  struct men_s_menu *menu;
  long optnum;
#endif
{
  if (optnum >= 0 && optnum < menu->nopts) {
    men_opt(menu, optnum)->descr.stat = 1;
    men_update(menu, optnum);
    if (optnum >= menu->firstopt && optnum < menu->firstopt + menu->ncols *
	menu->nrows)
      return men_putopt(menu, optnum);
    else
      return 1;
  }
  else
    return 0;
}
