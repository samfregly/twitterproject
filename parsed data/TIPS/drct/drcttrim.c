/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_trimrec                                      trimrec.c

Function : This function is used to trim the allocation for a record index
	entry.

Author : A. Fregly

Abstract : This function will set the allocation for the curren record
	to the length of the data actually stored, or 0 if the record
	has been deleted. This function assumes a write lock has been
	placed on the record by the caller, and does no locking or unlocking.

Calling Sequence : stat = drct_trimrec(struct drct_s_file *handle);

  handle        Direct access file handle return by drct_open.
  stat          Returned status, 0 if an error occurred.

Notes: 

Change log : 
000     26-JUN-91  AMF  Created.
001     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg for lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

#if FIT_ANSI
int drct_trimrec(struct drct_s_file *handle)
#else
int drct_trimrec(handle)
  struct drct_s_file *handle;
#endif
{

  if (handle->drct.length <= 0)
    /* Record was deleted, set allocation to 0 */
    handle->drct.alloc = 0;
  else
    /*  Record was not deleted, set allocation to record length */
    handle->drct.alloc = handle->drct.length;

  /* Advance to record descriptor in index file */
  if (lseek(handle->idx, (off_t) (handle->currec * sizeof(handle->drct)), SEEK_SET) < 0)
    return 0;

  /* Write the record out */
#if !FIT_TURBOC
  if (write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) return 0;
#else
  if (_write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) return 0;
#endif
  return 1;
}
