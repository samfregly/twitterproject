/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_idxappnd                                     idxappnd.c

Function : Appends entry to record index

Author : A. Fregly

Abstract : This function is used to append an entry onto the record index
	for a direct access file. The appended record will be write locked
	on exit from this function.

Calling Sequence : off_t recnum = drct_idxappnd(struct drct_s_file *handle,
	off_t offset, size_t length, size_t allocation);

  handle        Direct access file handle returned by drct_open.
  offset        Offset of data record.
  length        Length  of data record.
  allocation    Allocation for data record.
  recnum        Returned record number for appended record, or -1 for failure.

Notes: 

Change log : 
000     26-MAY-91  AMF  Created.
001	26-AUG-03  AMF  Make it work under CYGWIN
002     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
003     22-FEB-07  AMF  Workaround for lseek bug on OSX.
******************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

#if FIT_ANSI
off_t drct_idxappnd(struct drct_s_file *handle, off_t offset, size_t length, 
  size_t allocation)
#else
off_t drct_idxappnd(handle, offset, length, allocation)
  struct drct_s_file *handle;
  long offset, length, allocation;
#endif
{
   off_t idxoffset, zero=0;
   size_t written, reclen;
   int idxhandle = handle->idx;
   reclen = sizeof(handle->drct);
   char z=0;

   int s = sizeof(idxoffset);
   z=s;

  int func;
  int flags;

  if (!drct_wlock(handle, zero, (size_t) 0)) return -1;
  if ((flags = fcntl(handle->idx, F_GETFD,0)) < 0) goto ERREXIT;
  flags |= O_APPEND;
  if (fcntl(handle->idx, F_SETFL, flags) < 0) goto ERREXIT;

   /*  Fill in the record descriptor and write it to the record index */
   handle->drct.offset = offset;
   handle->drct.length = length;
   handle->drct.alloc = allocation;
#if !FIT_TURBOC
   if ((written = write(idxhandle, &handle->drct, reclen)) != sizeof(handle->drct)) goto ERREXIT;
#else
   if (_write(handle->idx, &handle->drct, sizeof(handle->drct)) != sizeof(handle->drct)) goto ERREXIT;
#endif
  // Reposition to beginning of record
  flags ^= O_APPEND;
  if (fcntl(handle->idx, F_SETFL, flags) < 0) goto ERREXIT;
//  func = SEEK_END;
//  if ((idxoffset = lseek(handle->idx, zero, func)) < 0) goto ERREXIT;
  func = SEEK_CUR;
  if ((idxoffset = lseek(handle->idx, (off_t) (-sizeof(handle->drct)), func)) < 0) goto ERREXIT;
  if (handle->locked) drct_unlock(handle);

   /* Update handle */ 
   handle->inrecoff = 0;
   handle->currec = idxoffset / sizeof(handle->drct);

   /* Convert the lock to just the record being written */
   return handle->currec;

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  return -1;
}
