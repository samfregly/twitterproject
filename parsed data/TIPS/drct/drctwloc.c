/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_wlock                                                wlock.c

Function : Write locks records in direct access file.

Author : A. Fregly

Abstract : This function is used to place a discretionary write lock
	on one or more direct access records. All of the drct_xxxx routines
	use the discretionary locks, so applications restricting their
	data access to the drct_xxxx routines are assured of data
	integrity. Read only applications may directly read the data
	file with no worry about waiting on locks, with some minor chance
	of reading a corrupted record in the data file.

	drct_wlock will use the current write lock timeout value for the file
	handle in determining how long to wait for the file to be available
	for locking. The write lock timeout value is set when the file is
	opened using drct_open, or modified using drct_timeout.

Calling Sequence : int stat = drct_wlock(struct drct_s_file *handle,
	off_t startrec, size_t nrecs);

	handle          Handle for direct access file returned by drct_open.
	startrec        Starting record for lock (0L is the first record).
	nrecs           Number of records to be locked, 0L for all records.
	stat            Returned status, 0 indicates an error.

Notes:
001     Under DOS, all locks are exclusive. For write locks, the effect is
	not noticeable, as write locks are exclusive locks under the other
	implementations. Read locks are mutually exclusive to each other,
	and to write locks under DOS, as they use the same exclusive locks
	under DOS.

Change log :
000     13-JUN-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <drct.h>
#include <fcntl.h>

#if FIT_ANSI
int drct_wlock(struct drct_s_file *handle, off_t startrec, size_t nrecs)
#else
int drct_wlock(handle, startrec, nrecs)
  struct drct_s_file *handle;
  off_t startrec;
  size_t nrecs;
#endif
{
#if FIT_COHERENT || !FIT_UNIX
#define F_WRLCK 2
#endif

  return (handle->uselocks) ? drct_lock(handle, F_WRLCK, startrec, nrecs) : 1;
}
