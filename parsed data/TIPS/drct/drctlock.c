/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_lock                                         drctlock.c

Function : Locks records in direct access file.

Author : A. Fregly

Abstract : This function is used to place discretionary locks
	on one or more direct access records. All of the drct_xxxx routines
	use the discretionary locks, so applications restricting their
	update access to the drct_xxxx routines are assured of data
	integrity. Read only applications may directly read the data
	file with no worry about waiting on locks, with some minor chance
	of reading a corrupted record in the data file.

	drct_lock will use the current lock timeout values for the file 
	handle in determining how long to wait for the file to be available 
	for locking. The lock timeout values are set when the file is opened 
	using drct_open, or modified using drct_timeout.

Calling Sequence : int stat = drct_lock(struct drct_s_handle *handle, 
	int locktype, off_t startrec, size_t nrecs);

	handle          Handle for direct access file returned by drct_open.
	locktype        Lock type, either F_RDLCK or F_WRLCK (defined in
			fcntl.h in UNIX System V).
	startrec        Starting record for lock (0L is the first record).
	nrecs           Number of records to be locked, 0L for all records.
	stat            Returned status, 0 indicates an error.

Notes: 
001     This routine only permits one outstanding lock on a file. If called
	with a lock already present on the file, the previous lock is released
	prior to creation of the new lock. Under UNIX, any overlapping
	portion of the old lock is kept locked during the creation of the
	new lock.
002     The DOS version of this function does not differentiate between
	read and write locks, as all locks under DOS are "exclusive" locks.
	For this reason, the DOS version of the drct input/output functions
	do not leave locks in place, even for reads, except for the time
	period necessary to do the function.

Change log : 
000     13-JUN-91  AMF  Created.
001     17-JUL-92  AMF  DOS version.
002     14-DEC-92  AMF  COHERENT support.
003     22-MAR-93  AMF  Compile under GNU C.
004     27-JUL-94  AMF  Include stdlib.h to get defs for min and max.
005     19-FEB-07  AMF  Cast "offset" arg to lseek to off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>
#if !FIT_DOS
#include <fcntl.h>
#else
#include <fcntl.h>
#include <io.h>
#if FIT_ZORTECH || FIT_MSC
#include <sys\locking.h>
#endif
#endif

#if FIT_COHERENT
extern int errno;
#endif

#if FIT_ANSI
int drct_lock(struct drct_s_file *handle, int locktype, off_t startrec,
  size_t nrecs)
#else
int drct_lock(handle, locktype, startrec, nrecs)
  struct drct_s_file *handle;
  int locktype;
  off_t startrec;
  size_t nrecs;
#endif
{

#define RECLEN sizeof(handle->drct)

#if (!FIT_UNIX && !FIT_DOS && !FIT_COHERENT) || FIT_BSD
  return 1;
#endif

#if FIT_UNIX || FIT_COHERENT
  struct flock lck;
  int waittime, retstat;

  if (!handle) return 0;
  if (!handle->uselocks) return 1;

  retstat = 0;
  lck.l_whence = 0;

#if FIT_CYGWIN
  if (handle->locked) drct_unlock(handle);
#endif
  /* Only let the user have one lock at a time, so unlock any nonoverlapping
     portions from a previous lock */
  if (handle->locked &&
     (handle->lockstart != startrec || handle->locknrecs != nrecs)) {
    lck.l_type = F_UNLCK;

    if (handle->lockstart < startrec) {
      /* Unlock the part of the record preceeding the new lock section */
      lck.l_start = handle->lockstart * RECLEN;
      if (handle->locknrecs && handle->lockstart + handle->locknrecs <=
	  startrec)
	lck.l_len = handle->locknrecs * RECLEN;
      else
	lck.l_len = startrec * RECLEN - lck.l_start;

      if (fcntl(handle->idx, F_SETLK, &lck) < 0) goto ERREXIT;

      /* Update handle lock description */
      if (handle->locknrecs && handle->lockstart + handle->locknrecs <=
	  startrec)
	handle->locked = 0;
      else {
	handle->lockstart = startrec;
	handle->locknrecs -= (lck.l_len / RECLEN);
      }
    }

    if (nrecs && (!handle->locknrecs ||
	handle->lockstart + handle->locknrecs > startrec + nrecs)) {
      /* Unlock portion of file following end of section to be locked */
      if (handle->lockstart < startrec + nrecs) {
	lck.l_start = startrec * RECLEN + nrecs * RECLEN;
      }
      else
	lck.l_start = handle->lockstart * RECLEN;

      if (!handle->locknrecs)
	lck.l_len = 0L;
      else
	lck.l_len = handle->lockstart * RECLEN +
	  handle->locknrecs * RECLEN - lck.l_start;

      if (fcntl(handle->idx, F_SETLK, &lck) < 0) goto ERREXIT;

      /* Update handle lock description */
      if (handle->lockstart < startrec + nrecs) {
	if (!handle->locknrecs)
	  handle->locknrecs = nrecs - (handle->lockstart - startrec);
	else
	  handle->locknrecs -= (lck.l_len / RECLEN);
      }
      else
	handle->locked = 0;
    }
  }

  lck.l_type = locktype;
  lck.l_start = startrec * RECLEN;
  lck.l_len = nrecs * RECLEN;

  if (locktype == F_WRLCK)
    waittime = handle->wrtimeout;
  else
    waittime = handle->rdtimeout;

  if (waittime < 0) {
    /* Caller doesn't want to timeout, so wait until lock is available */

    retstat = fcntl(handle->idx, F_SETLKW, &lck);
  }
  else {
    /* Caller has specified a timeout, loop until we get lock or timeout
       expires */
    for (retstat = -1; retstat < 0 && waittime >= 0; --waittime) {
      retstat = fcntl(handle->idx, F_SETLK, &lck);
      if (retstat < 0 && waittime) sleep(1);
    }
  }

  if (retstat >= 0) {
    /* Got the lock, update internal descriptors in handle for the lock */
    handle->locked = 1;
    handle->locktype = locktype;
    handle->lockstart = startrec;
    handle->locknrecs = nrecs;
    return 1;
  }

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  return 0;
#endif

#if FIT_DOS
  int waittime, retstat, forever;
  long lockstart, locknbytes, curpos;

#if FIT_TURBOC || FIT_ZORTECH
  handle->uselocks = 0;
#endif
  if (handle == NULL) return 0;

  if (!handle->uselocks) return 1;

  waittime = max(handle->wrtimeout, handle->rdtimeout);
  lockstart  = startrec * RECLEN;
#if FIT_MSC
  if (nrecs <= 0) nrecs=2000000000/RECLEN;
#endif
  locknbytes = nrecs * RECLEN;
  forever = waittime < 0;

  if (handle->locked) {
	/* Release the old lock */
#if FIT_TURBOC
    retstat = !unlock(handle->idx, (long) (handle->lockstart * RECLEN),
	(long) (handle->locknrecs * RECLEN));
#endif
#if FIT_ZORTECH || FIT_MSC
    curpos = lseek(handle->idx, (off_t) (handle->lockstart * RECLEN),
	  SEEK_SET);
    retstat = !locking(handle->idx, LK_UNLCK,
	  (long) (handle->locknrecs * RECLEN));
	if (!retstat) perror("drct_lock, error from lseek to start of old lock\n");
#endif
    if (!retstat) return 0;
    handle->locked = 0;
  }

#if FIT_ZORTECH || FIT_MSC
  curpos = lseek(handle->idx, (off_t) lockstart, SEEK_SET);
#endif

  for (retstat = 0; !retstat && (forever || waittime >= 0);
      waittime -= !forever) {
    /* Try to apply the new lock */
#if FIT_TURBOC
    retstat = !lock(handle->idx, lockstart, locknbytes);
    if (!retstat && (errno == 5 || errno == 19)) break;
#endif
#if FIT_ZORTECH || FIT_MSC
    retstat = !locking(handle->idx, LK_NBLCK, locknbytes);
	if (!retstat) perror("\ndrct_lock, error locking are of file\n");
#endif
    if (!retstat && (waittime || forever)) {
/***      perror("\n*** Error getting lock ***"); ****/
#if !FIT_MSC
		sleep(1);
#else
		_sleep(1000L);
#endif
    }
  }

  if (retstat) {
    /* Got the lock, update internal descriptors in handle for the lock */
    handle->locked = 1;
    handle->locktype = locktype;
    handle->lockstart = startrec;
    handle->locknrecs = nrecs;
  }
  return retstat;
#endif
}
