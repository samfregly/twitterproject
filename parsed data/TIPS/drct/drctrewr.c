/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_rewrite                                      rewrite.c

Function : Rewrites record in direct access file

Author : A. Fregly

Abstract : This function is used to rewrite a record in a direct access
	file. The record must be the current record. If the previous 
	area used by the record in the data file is large enough, that 
	space is re-used, otherwise the record will be either inserted
	into a large enough free space in the data file, or appended to the
	data file. In either case, the record number remains the same.

Calling Sequence : int stat = drct_rewrite(struct drct_s_file *handle, 
  char *buffer, size_t buflen, size_t reclen);

  handle        Handle for direct access file returned by drct_open.
  buffer        Buffer containing data to be written.
  buflen        Length of buffer.
  reclen        Record length. If reclen is longer than buflen, the caller
		may use multiple consecutive calls to rewrite to write the
		record.
Notes: 

Change log : 
000     26-JUN-91  AMF  Created
001     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Rewrite record in direct access file */
#if FIT_ANSI
int drct_rewrite(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen)
#else
int drct_rewrite(handle, buffer, buflen, reclen)
  struct drct_s_file *handle;
  char *buffer;
  size_t buflen;
  size_t reclen;
#endif
{
  static off_t placeholder;
  off_t offset, idxoffset;
  size_t length, alloc, currec, recleft;

  int zero = 0;

  if (handle->curop != 'W') {
    handle->curop = 'W';
    currec = handle->currec;
    placeholder = -1;

    /* See if record will fit in it's current location */
    /* Lock the record */
    if (!drct_wlock(handle, handle->currec, (size_t) 1)) return 0;

    /* Reread the record index entry */
    if (!drct_setpos(handle, handle->currec)) goto ERREXIT; 

    /* Check the allocation */
    if (handle->drct.alloc < reclen) {
      /* New version won't fit */
      if (handle->drct.alloc > 0) {
	/* Append a placeholder index record for the current data area */
	if ((placeholder = drct_idxappnd(handle, handle->drct.offset,
	  handle->drct.length, handle->drct.alloc)) < 0) goto ERREXIT;
      }

      /* see if free space available in data file */
      if (!drct_findfree(handle, reclen, &offset, &alloc)) {
	/* No space, will have to allocate space at the end of the file */

	/* Lock record index */
	if (!drct_wlock(handle, (off_t) 0, (size_t) 0)) goto RECOVER;

	/* Extend file to length necessary to hold complete record */
	if ((idxoffset = lseek(handle->data, (off_t) (reclen - 1), SEEK_END)) < 0) goto RECOVER;
        if (idxoffset == 0 && lseek(handle->data, (off_t) (reclen - 1), SEEK_SET) < 0) goto RECOVER;

	/* Write some data so that the file will get extended */
#if !FIT_TURBOC
	if (write(handle->data, &zero, (size_t) 1) != 1) goto RECOVER;
#else
	if (_write(handle->data, &zero, (size_t) 1) != 1) goto RECOVER;
#endif

	/* Reset position in data file to beginning of record */
	if ((offset = lseek(handle->data, (off_t) -reclen, SEEK_CUR)) < 0) goto RECOVER;
	alloc = reclen;
      }

      else {
	/* Trim the allocation of the record which contained the unallocated
	   space. Note that offset and alloc were set by drct_findfree */
	if (!drct_trimrec(handle)) goto RECOVER;
	/* Set data file position to location of free space */
	if (lseek(handle->data, offset, SEEK_SET) != offset) goto RECOVER;
      }
    }
    else {
      /* Reusing old space, use old allocation and data file offset */
      alloc = handle->drct.alloc;
      offset = handle->drct.offset;
      /* Set data file position to location of free space */
      if (lseek(handle->data, offset, SEEK_SET) != offset) goto RECOVER;
    }

    /* Update the record index entry. Note that there is a possibility
       that two simultaneous rewrites will result in not only clobbering
       of one, but also will possibly cause the allocation for the
       clobbered record to become "lost". A fix for this requires the
       index record for the rewrite to be write locked for the duration of
       the reallocation process, but this is incompatible with the current
       locking utility functionality. */
    if (!drct_wlock(handle, currec, 1L)) goto RECOVER;
    handle->currec = currec;
    handle->drct.offset = offset;
    handle->drct.length = reclen;
    handle->drct.alloc = alloc;
    if (lseek(handle->idx, (off_t) (currec * sizeof(handle->drct)), SEEK_SET) < 0) 
      goto RECOVER;
#if !FIT_TURBOC
    if (write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) goto RECOVER;
#else
    if (_write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) goto RECOVER;
#endif

    handle->inrecoff = 0;
  }

  /* Add data for current buffer to record */
  recleft = handle->drct.length - handle->inrecoff;
  if (buflen > recleft) buflen = recleft;
  handle->inrecoff += buflen;

  if (buflen > 0)
    /* Add data to data file */
#if !FIT_TURBOC
    if (write(handle->data, buffer, (size_t) buflen) !=  buflen) goto ERREXIT;
#else
    if (_write(handle->data, buffer, (size_t) buflen) !=  buflen) goto ERREXIT;
#endif

  if (handle->inrecoff >= handle->drct.length) {
    handle->curop = 0;
    if (placeholder >= 0) {
      drct_setpos(handle, placeholder);
      drct_delrec(handle, (off_t) 1);
      placeholder = -1;
    }
    return drct_unlock(handle);
  }
  return 1;

RECOVER:
  /* Zap the placeholder record */
  if (!drct_wlock(handle, placeholder, (size_t) 1)) goto ERREXIT;
  if (!drct_setpos(handle, placeholder)) goto ERREXIT;
  if (lseek(handle->idx, (off_t) (placeholder * sizeof(handle->drct)), SEEK_SET) < 0)
    goto ERREXIT;
  offset = handle->drct.offset;
  length = handle->drct.length;
  alloc = handle->drct.alloc;
  handle->drct.length = 0;
  handle->drct.alloc = 0;
#if !FIT_TURBOC
  if (write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) goto ERREXIT;
#else
  if (_write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) goto ERREXIT;
#endif

  /* Restore the value of the index entry */
  if (!drct_wlock(handle, currec, (size_t) 1)) goto ERREXIT;
  if (lseek(handle->idx, (off_t) (currec * sizeof(handle->drct)), SEEK_SET) < 0) goto ERREXIT;
  handle->drct.offset = offset;
  handle->drct.length = length;
  handle->drct.alloc = alloc;
#if !FIT_TURBOC
  write(handle->idx, &handle->drct, sizeof(handle->drct));
#else
  _write(handle->idx, &handle->drct, sizeof(handle->drct));
#endif

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
