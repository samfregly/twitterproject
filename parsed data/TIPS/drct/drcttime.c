/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_timeout                                      timeout.c

Function : Sets timeouts for direct file access.

Author : A. Fregly

Abstract : This function is used to reset the tiemouts on waits for
	the read and write locks placed on the direct access file
	by the drct_xxxx functions. The timeouts are initially set when
	the files is opened.

Calling Sequence : int stat = drct_timeout(struct drct_s_file *handle, 
    int rdtimeout, int wrtimeout)

	handle          Handle for direct access file returned by drct_open.
	rdtimeout       Number of seconds to wait before timing out waiting
			on a read lock. A value of 0 means no wait, and -1
			is used for an infinite wait.
	wrtimeout       Number of seconds to wait before timing out waiting
			on a write lock. A value of 0 means no wait, and -1
			is used for an infinite wait.
	stat            Returned status, 0 if an error occurs.

Notes:
001     The DOS version of the DRCT functions do not have read and write
	locks, utilizing only a "exclusive" locking mechanism. The
	"wrtimeout" value is used for setting the timeout value for DOS
	applications, and the "rdtimeout" value is ignored.

Change log :
000     13-JUN-91  AMF  Created.
001     23-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <drct.h>

#if FIT_ANSI
int drct_timeout(struct drct_s_file *handle, int rdtimeout, int wrtimeout)
#else
int drct_timeout(handle, rdtimeout, wrtimeout)
  struct drct_s_file *handle;
  int rdtimeout, wrtimeout;
#endif
{
  if (handle) {
    handle->rdtimeout = rdtimeout;
    handle->wrtimeout = wrtimeout;
    return 1;
  }
  else
    return 0;
}
