/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fit_delrec                                        delrec.c

Function : This function is used to delete records in a direct access file.

Author : A. Fregly

Abstract : This function is used to delete the "current" record in the
	direct access file. This record may have been accessed either
	by a previous read operation, or by use of the drct_setpos function.

Calling Sequence : int stat = drct_delrec(struct drct_s_file *handle, 
	int destroyopt)

	handle          Handle for direct access file returned by drct_open.
	destroyopt      Non-zero, then data will actually be overwritten with
			nulls in the data file, otherwise the space used by
			the data is made available for re-allocation, but is
			not removed.
	stat            A value of zero is used to indicate an error occurred.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
002     19-FEB-07  AMF  Use SEEK_xxx as "whence" arg for lseek. Cast offset
                        to off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Delete record in direct access file */
#if FIT_ANSI
int drct_delrec(struct drct_s_file *handle, int destroyopt)
#else
int drct_delrec(handle, destroyopt)
  struct drct_s_file *handle;
  int destroyopt;
#endif
{
  off_t idxoffset;
  size_t length;
  int stat;
  char *nullbuf;
  unsigned nullbuflen;

  if (!handle) return 0;

  handle->curop = 0;

  if (handle->currec < 0 || handle->drct.length < 0) return 0;

  /* Lock the record to be deleted in the idx file */
  if (!drct_wlock(handle, handle->currec, (size_t) 1)) return 0;

  /* Position to desired record in idx file */
  idxoffset = lseek(handle->idx, (off_t) (handle->currec * sizeof(handle->drct)), SEEK_SET);
  if (!(stat = (idxoffset >= 0))) goto DONE;

  /* Reread the record */
#if !FIT_TURBOC
  if (!(stat = (read(handle->idx, &handle->drct, sizeof(handle->drct)) ==
	sizeof(handle->drct)))) goto DONE;
#else
  if (!(stat = (_read(handle->idx, &handle->drct, sizeof(handle->drct)) ==
	sizeof(handle->drct)))) goto DONE;
#endif

  if (!(stat = (handle->drct.length > 0))) goto DONE;

  /* Reposition to beginning of desired record in idx file */
  idxoffset = lseek(handle->idx, (off_t) - (long) sizeof(handle->drct), SEEK_CUR);
  if (!(stat = (idxoffset >= 0))) goto DONE;

  nullbuf = NULL;
  length = handle->drct.length;
  handle->drct.length = -handle->drct.length;

  /* Update the file */
#if !FIT_TURBOC
  stat = (write(handle->idx, &handle->drct, sizeof(handle->drct)) ==
	sizeof(handle->drct));
#else
  stat = (_write(handle->idx, &handle->drct, sizeof(handle->drct)) ==
	sizeof(handle->drct));
#endif

  if (stat && destroyopt) {
    /* Set data file position to start of record */
    if (!(stat = (lseek(handle->data, (off_t) handle->drct.offset, SEEK_SET) ==
	handle->drct.offset))) goto DONE;

    /* Allocate a null buffer */
    nullbuflen = 4096;
    if (nullbuflen > length) nullbuflen = length;
    if (!(stat = ((nullbuf = (char *) calloc(1,nullbuflen)) != NULL)))
      goto DONE;

    for (; length; length -= nullbuflen) {
      if (length < nullbuflen) nullbuflen = length;
#if !FIT_TURBOC
      if (!(stat = (write(handle->data, nullbuf, nullbuflen) == nullbuflen)))
#else
      if (!(stat = (_write(handle->data, nullbuf, nullbuflen) == nullbuflen)))
#endif
	break;
    }
  }

DONE:
  /* Unlock the record */
  if (handle->locked)
    stat = stat && drct_unlock(handle);

  /* Free the null buffer */
  if (nullbuf != NULL) free(nullbuf);
  return stat;
}
