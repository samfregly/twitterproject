/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_curpos                                       curpos.c

Function : Returns current record number in direct access file.

Author : A. Fregly

Abstract : This routine is used to determine the current record number in
	an open direct access file.

Calling Sequence : off_t currect = drct_curpos(struct drct_s_file *handle);

	handle  Handle for direct access file returned by drct_open.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <drct.h>

/* Get current record number */
#if FIT_ANSI
off_t drct_curpos(struct drct_s_file *handle)
#else
off_t drct_curpos(handle)
  struct drct_s_file *handle;
#endif
{
  return handle->currec;
}
