/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_rewind                                       drctrewi.c

Function : Rewinds direct access file so that next sequential read will read
  the  first record.

Author : A. Fregly

Abstract :

Calling Sequence : drct_rewind(struct drct_s_file *handle);

	handle  Direct access file handle returned by drct_open.

Notes:

Change log :
000     26-JUL-91  AMF  Created.
001     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <drct.h>
#include <lowlvlio.h>

#if FIT_ANSI
void drct_rewind(struct drct_s_file *handle)
#else
void drct_rewind(handle)
  struct drct_s_file *handle;
#endif
{
  handle->currec = -1;
  handle->curop = 0;
  handle->inrecoff = 0;
  handle->drct.length = 0;
  handle->drct.alloc = 0;
  handle->drct.offset = -1;
  lseek(handle->idx, (off_t) 0L, SEEK_SET);
  lseek(handle->data, (off_t) 0L, SEEK_SET);
}
