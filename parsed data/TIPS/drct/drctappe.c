/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_append                                       append.c

Function : Appends a record to a direct access file.

Author : A. Fregly

Abstract : This function is used to add records to the end of a direct
	access file. For programs which desire maximum efficiency, this
	is a much faster way of adding data to a direct access file.

	drct_append may be used to write records using several consecutive
	append operations. This function is invoked by specifying a record
	length which is longer than the initial and subsequent buffer length
	specifiers. The record length should be unchanged between successive
	calls, and the user is warned that access to the file must be limited
	to writing the long record during the time the record is being written.

	While drct_append is updating the file, it uses a discretionary
	write lock on the file record index to keep other processes from
	concurrently modifying the file. drct_append will use the current
	write timeout value for the file handle in determining how long
	to wait for the file to be available for locking. The write timeout
	value is set when the file is opened using drct_open, or modified
	using drct_timeout.

Calling Sequence : int stat = drct_append(struct drct_s_file *handle, 
  char *buffer, size_t buflen, size_t reclen)

  handle        Direct access file handle returned by drct_open.
  buffer        Buffer containing data to be appended.
  buflen        Current buffer length.
  reclen        Record length.
  stat          Returned status, 0 if an error occurs.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     08-SEP-93  AMF  HPUX compatibility
002     02-AUG-93  AMF  Fix for DOS.
003     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if !FIT_HP && !FIT_UNIX && !FIT_MSC
#include <lowlvlio.h>
#endif
#include <drct.h>

#if FIT_UNIX && !FIT_COHERENT
#include <sys/fcntl.h>
#else
#if FIT_DOS
#include <io.h>
#include <fcntl.h>
#include <stdlib.h>
#endif
#endif

/* Append record to direct access file */
#if FIT_ANSI
int drct_append(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen)
#else
int drct_append(handle, buffer, buflen, reclen)
  struct drct_s_file *handle;
  char *buffer;
  size_t buflen;
  size_t reclen;
#endif
{

  size_t recleft;
  off_t dataoffset, rlen;

  char zero = 0;
  rlen = reclen;

  if (handle->curop != 'A') {
    /* First block of record */
    handle->curop = 'A';

    /* Lock record index */
    if (!drct_wlock(handle, (off_t) 0, (size_t) 0)) {
      /* perror("drct_append, error from drct_wlock(handle, 0, 0)"); */
      return 0;
    }

    /* Extend file to length necessary to hold complete record */
    if (lseek(handle->data, (off_t) (rlen - 1), SEEK_END) < 0) {
      /* perror("drct_append, error in lseek during file extend"); */
      goto ERREXIT;
    }

    /* Write some data so that the file will get extended */
#if !FIT_TURBOC
    if (write(handle->data, &zero, (size_t) 1) < 1) goto ERREXIT;
#else
    if (_write(handle->data, &zero, 1) < 1) {
      /* perror("drct_append, error in writing data to extend file"); */
      goto ERREXIT;
    }
#endif

    /* Reset position in data file to beginning of record */
    if ((dataoffset = lseek(handle->data, -rlen, SEEK_CUR)) < 0) {
      /* perror ("drct_append, error in seeking to beginning of record"); */
      goto ERREXIT;
    }

    /* Write descriptor record to index file */
    if (drct_idxappnd(handle, dataoffset, reclen, reclen) < 0) {
      /* perror("drct_append, error returned from call to drct_idxappnd"); */
      goto ERREXIT;
    }
    handle->inrecoff = 0;
  }

  recleft = handle->drct.length - handle->inrecoff;
  if (buflen > recleft) buflen = recleft;
  handle->inrecoff += buflen;

  if (buflen > 0) {
    /* Add data to data file */
#if !FIT_TURBOC
    if (write(handle->data, buffer, buflen) !=  buflen) {
      /* perror("drct_append, error from write to data file"); */
      goto ERREXIT;
    }
#else
    if (_write(handle->data, buffer, buflen) !=  buflen) goto ERREXIT;
#endif
  }

  if (handle->inrecoff >= handle->drct.length) {
    int retstat;
    handle->curop = 0;
    if (!(retstat = drct_unlock(handle))) {
      ;
      /* perror("drct_append, error unlocking file before return"); */
    }
    return retstat;
  }
  return 1;

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
