/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_compress                                     compress.c

Function : Compresses a direct access file.

Author : A. Fregly

Abstract : This function is used to rewrite a "compressed" version of a
	direct access file. The compressed version of the file will have
	all space being taken up by deleted records removed, and will ensure
	that the physical ordering of the data records corresponds to their
	logical record number. This function increases the efficiency of
	accessing a data file which has had a substantial number of record
	deletions and subsequent record writes using drct_write.

Calling Sequence : int ss = drct_compress(struct drct_s_file *handle);

	handle          Handle for direct access file returned by drct_open.

Notes: stub for now.

Change log : 
000     13-JUN-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Compress direct access file */
#if FIT_ANSI
int drct_compress(struct drct_s_file *handle)
#else
int drct_compress(handle)
  struct drct_s_file *handle;
#endif
{
  return 1;
}
