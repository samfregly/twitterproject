/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_info                                         info.c

Function : Returns information about a direct access file.

Author : A. Fregly

Abstract : This function is used to retrieve information about an open
	direct access file.

Calling Sequence : int stat = drct_info(struct drct_s_file *handle, 
	struct drct_s_info *info);

	handle          Handle for direct access file returned by drct_open.
	info            Info data structure which is filled in by drct_info.
	stat            Returned status, 0 indicates an error.
Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     22-MAR-93  AMF  Compile under GNU C.
002     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg for lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Get information about direct access file */
#if FIT_ANSI
int drct_info(struct drct_s_file *handle, struct drct_s_info *info)
#else
int drct_info(handle, info)
  struct drct_s_file *handle;
  struct drct_s_info *info;
#endif
{
  int locked, locktype, retstat;
  off_t lockstart;
  size_t locknrecs;

  retstat = 0;
  locked = handle->locked;
  lockstart = handle->lockstart;
  locknrecs = handle->locknrecs;
  locktype = handle->locktype;

  if (handle) {
    handle->curop = 0;
    if (info) {
      if (drct_rlock(handle,0L,0L)) {
	info->datasize = lseek(handle->data, (off_t) 0, SEEK_END);
	info->nrecs = (lseek(handle->idx, (off_t) 0, SEEK_END) + 1) / sizeof(handle->drct);
	info->openmode = handle->mode;
	info->rtimeout = handle->rdtimeout;
	info->wtimeout = handle->wrtimeout;
	handle->currec = -1;
	handle->inrecoff = 0;
	handle->drct.offset = 0;
	handle->drct.length = 0;
	retstat = 1;
      }
      if (locked)
	drct_lock(handle, locktype, lockstart, locknrecs);
      else if (handle->locked)
	drct_unlock(handle);
    }
  }
  return retstat;
}
