/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_write                                        write.c

Function : Writes records in direct access file.

Author : A. Fregly

Abstract : This function writes records into a direct access file. It is
	different from the other write routine (drct_append) in that it
	seeks to re-use deallocated portions of the data file for records
	being added. This is done at considerable expense as the record
	index is scanned for available slots.

Calling Sequence : int restat = drct_write(struct drct_s_file *handle, 
    char *buffer, size_t buflen, size_t reclen)

	handle  Handle for direct access file returned by drct_open.
	buffer  Buffer containing data for be written.
	buflen  Length of current buffer.
	reclen  Length of record. Records larger than a single buffer may
		be written by setting reclen larger than buflen, and then
		using multiple calls to drct_write to write the record.
	retstat Returned status, 0 indicates an error occurred.

Notes: 

Change log : 
000     14-JUN-91  AMF  Created.
001     23-MAR-93  AMF  Compile under GNU C.
002     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Write record to direct access file */
#if FIT_ANSI
int drct_write(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen)
#else
int drct_write(handle, buffer, buflen, reclen)
  struct drct_s_file *handle;
  char *buffer;
  size_t buflen;
  size_t reclen;
#endif
{
  size_t freespace;
  off_t freeoffset;
  int retstat;

  if (!handle) return 0;

  if (handle->curop != 'W') {
    handle->curop = 'W';

    /* Look for unallocated space in data file */
    if (!drct_findfree(handle, reclen, &freeoffset, &freespace)) {
      /* Unable to find free space, do a record append */
      retstat = drct_append(handle, buffer, buflen, reclen);
      if (handle->curop == 'A')
	handle->curop = 'W';
      return retstat;
    }
    else {
      /* Adjust the allocation for the record which currently has the space */
      if (!drct_trimrec(handle)) goto ERREXIT;

      /* Now allocate a record at the end of the index file */
      if (!drct_idxappnd(handle, freeoffset, reclen, freespace)) goto ERREXIT;
    }
  }

  /* Add data to record */
  if (handle->inrecoff + buflen > handle->drct.length)
    buflen = handle->drct.length - handle->inrecoff;

  if (buflen > 0) {
    if (!handle->inrecoff)
      /* Set position in data file */
      if (lseek(handle->data, (off_t) handle->drct.offset, SEEK_SET) < 0) goto ERREXIT;

    handle->inrecoff += buflen;
#if !FIT_TURBOC
    if (write(handle->data, buffer, buflen) <= 0) goto ERREXIT;
#else
    if (_write(handle->data, buffer, buflen) <= 0) goto ERREXIT;
#endif
  }

  if (handle->inrecoff >= handle->drct.length) {
    /* Done writing the record, can unlock the file */
    handle->curop = 0;
    return drct_unlock(handle);
  }

  return 1;

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
