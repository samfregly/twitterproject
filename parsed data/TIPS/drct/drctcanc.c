/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_cancelio                                     cancelio.c

Function : Cancels currently in progress i/o operation.

Author : A. Fregly

Abstract : This functions is used to terminate a multiple call read or
	write prior to completion of processing of the record. This function
	must be called before issuing another read or write if a read or
	write is not completed.

Calling Sequence : drct_cancelio(struct drct_s_file *handle);

Notes:

Change log :
000     24-JUL-91  AMF  Created.
001     09-OCT-91  AMF  Unlock file.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <drct.h>

#define BUFSIZE 1024

#if FIT_ANSI
void drct_cancelio(struct drct_s_file *handle)
#else
void drct_cancelio(handle)
  struct drct_s_file *handle;
#endif
{

  int curop;
  size_t nbytes;
  char *buf;

  if (handle->curop) {
    curop = handle->curop;
    handle->curop = 0;
    if ((curop == 'A' || curop == 'W') &&
	handle->inrecoff < handle->drct.length) {
      if ((buf = (char *) calloc(1, BUFSIZE)) != NULL) {
	for (nbytes = BUFSIZE; handle->inrecoff < handle->drct.length;
		handle->inrecoff += nbytes) {
	  if ((handle->drct.length - handle->inrecoff) < BUFSIZE)
	    nbytes = handle->drct.length - handle->inrecoff;
#if !FIT_TURBOC
	  if (write(handle->data, buf, nbytes) != nbytes) break;
#else
	  if (_write(handle->data, buf, nbytes) != nbytes) break;
#endif
	}
	free(buf);
      }
    }
    handle->inrecoff = max(0,handle->drct.length);
  }
  if (handle->locked) drct_unlock(handle);
}
