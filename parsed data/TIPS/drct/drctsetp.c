/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_setpos                                       setpos.c

Function : Sets direct access file record to be accessed next.

Author : A. Fregly

Abstract : This function sets the file position in a direct access file to
	the specified record. This record will be the one accessed by the
	next call to drct_delrec or drct_rdseq.

Calling Sequence : int stat = drct_setpos(struct drct_s_file *handle, 
    off_t recnum)

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     23-MAR-93  AMF  Compile under GNU C.
002     19-FEB-07  AMF  Cast "offset" arg of lseek to off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Set current record in file */
#if FIT_ANSI
int drct_setpos(struct drct_s_file *handle, off_t recnum)
#else
int drct_setpos(handle, recnum)
  struct drct_s_file *handle;
  off_t recnum;
#endif
{
  off_t idxoffset;

  if (!handle) return 0;
  idxoffset = recnum * sizeof(handle->drct);
  if (lseek(handle->idx, idxoffset, SEEK_SET) != idxoffset) {
	  /* perror("drct_setpos, error setting position in direct access file"); */
	  return 0;
  }
  handle->currec = recnum;
  handle->inrecoff = 0;
#if !FIT_TURBOC
  if (read(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) {
	/* perror("drct_setpos, error reading entry from idx file"); */
	return 0;
  }
#else
  if (_read(handle->idx, &handle->drct, sizeof(handle->drct)) !=
      sizeof(handle->drct)) return 0;
#endif
  return 1;
}
