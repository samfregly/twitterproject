/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_close                                        close.c

Function : Closes direct access file.

Author : A. Fregly

Abstract : Closes a direct access file.

Calling Sequence : drct_close(struct drct_s_file *handle);

	handle          Handle for direct access file returned by
			drct_open.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT. Delete temp files under
			COHERENT.
002     22-MAR-93  AMF  Compile under GNU C.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Close direct access file */
#if FIT_ANSI
void drct_close(struct drct_s_file *handle)
#else
void drct_close(handle)
  struct drct_s_file *handle;
#endif
{
  int istemp = (handle->datahandle != (FILE *) NULL);
  if (handle) {
    if (handle->locked) drct_unlock(handle);
    if (handle->datahandle) {
      fclose(handle->datahandle);
#if FIT_COHERENT
      fit_fdelete(handle->dataspec);
#endif
    }
    else if (handle->data != -1) 
      close(handle->data);
    if (handle->idxhandle) {
      fclose(handle->idxhandle);
#if FIT_COHERENT
      fit_fdelete(handle->idxspec);
#endif
    }
    else if (handle->idx != -1) 
      close(handle->idx);
    if (handle->dataspec) free(handle->dataspec);
    if (handle->idxspec) free(handle->idxspec);
    free(handle);
  }
}
