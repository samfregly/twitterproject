/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_overwrite                                    drctover.c

Function : Writes records in direct access file, reusing deleted records.

Author : A. Fregly

Abstract : This function writes records into a direct access file, reusing
	the space use by deleted records in both the data file and the
	record index. This is done at considerable expense as the record
	index is scanned for available slots.

Calling Sequence : int stat = drct_overwrite(struct drct_s_file *handle,
    char *buffer, size_t buflen, size_t reclen)

	handle  Handle for direct access file returned by drct_open.
	buffer  Buffer containing data for be written.
	buflen  Length of current buffer.
	reclen  Length of record. Records larger than a single buffer may
		be written by setting reclen larger than buflen, and then
		using multiple calls to drct_write to write the record.
	stat    Returned status, 0 indicates an error occurred.

Notes:

Change log :
000     23-JUL-91  AMF  Created.
001     23-MAR-93  AMF  Compile under GNU C.
002     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg to lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <sys/stat.h>
#include <lowlvlio.h>
#include <drct.h>

/* Write record to direct access file */
#if FIT_ANSI
int drct_overwrite(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen)
#else
int drct_overwrite(handle, buffer, buflen, reclen)
  struct drct_s_file *handle;
  char *buffer;
  size_t buflen;
  size_t reclen;
#endif
{
  size_t nrecs;
  off_t currec, endrec;
  int ss;
  struct stat statbuf;

  if (!handle) return 0;

  if (handle->curop != 'W') {
    handle->curop = 'W';

    /* Look for deleted record with enough space */
    if (fstat(handle->idx, &statbuf)) goto ERREXIT;
    nrecs = statbuf.st_size / sizeof(handle->drct);
    ss = (nrecs > 0);

    if (nrecs) {
      if (handle->currec < 0)
	handle->currec = 0;
      else if (handle->currec >= nrecs)
	handle->currec = nrecs-1;

    /* Look for unallocated space in data file */
      for (ss = 0, endrec = handle->currec, currec = endrec+1;;++currec) {
	if (currec >= nrecs) currec = 0L;

	if (!drct_wlock(handle, currec, (size_t) 1)) goto ERREXIT;

	if (!drct_setpos(handle, currec)) goto ERREXIT;

	if (handle->drct.length < 0 && handle->drct.alloc >= reclen) {
	  ss = 1;
	  break;
	}
	if (currec == endrec) break;
      }
    }

    if (!ss) {
      /* Unable to find free space, do a record append */
      ss = drct_append(handle, buffer, buflen, reclen);
      if (handle->curop == 'A')
	handle->curop = 'W';
      return ss;
    }
    else {
      /* Reset position in idx file */
      if (lseek(handle->idx, (off_t) -(sizeof(handle->drct)), SEEK_CUR) !=
	  handle->currec * sizeof(handle->drct))
	goto ERREXIT;

      /* Set the  length for the record which currently has the space */
      handle->drct.length = reclen;
#if !FIT_TURBOC
     if (write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
       sizeof(handle->drct)) goto ERREXIT;
#else
     if (_write(handle->idx, &handle->drct, sizeof(handle->drct)) !=
       sizeof(handle->drct)) goto ERREXIT;
#endif
     /* Update handle */
     handle->inrecoff = 0L;
    }
  }

  /* Add data to record */
  if (handle->inrecoff + buflen > handle->drct.length)
    buflen = handle->drct.length - handle->inrecoff;

  if (buflen > 0) {
    if (!handle->inrecoff) {
      /* Set position in data file */
      if (lseek(handle->data, (off_t) handle->drct.offset, SEEK_SET) < 0) goto ERREXIT;
   }

    handle->inrecoff += buflen;
#if !FIT_TURBOC
    if (write(handle->data, buffer, buflen) <= 0) goto ERREXIT;
#else
    if (_write(handle->data, buffer, buflen) <= 0) goto ERREXIT;
#endif
  }

  if (handle->inrecoff >= handle->drct.length) {
    /* Done writing the record, can unlock the file */
    handle->curop = 0;
    return drct_unlock(handle);
  }

  return 1;

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
