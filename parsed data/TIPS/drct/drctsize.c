/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************Module Name : drct_findfree                                     findfree.c
Name : drct_sizeinfo

Function : Determines utilization of a direct access file.

Author : A. Fregly

Abstract : This function will return utilization and allocation information about
	an open direct access file.

Calling Sequence : int stat = drct_sizeinfo(struct drct_s_handle *handle, 
  size_t *numutilized, size_t *utilizedsize, siz_t *numallocated, size_t *allocatedsize);

  handle        Direct access file handle returned by drct_open.
  numutilized	Returned number of records utilized in the file.
  utilizedsize	Number of bytes utilized.
  numallocated  Returned number of records allocated in the file.
  allocatedsize Returned number of bytes allocated.
  numfragments	Returned number of utilized records with unallocated poritons
				of the record.
  fragmentsize	Returned number of bytes which are in unallocated portions of
				utilized records.

  returns 1 if file was successfully scanned for size information, 0 if an
  error occurred.

Notes:

Change log :
001	09-JUL-97  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>
#if !FIT_DOS
#include <sys/stat.h>
#else
#include <sys\stat.h>
#endif

#if FIT_ANSI
int drct_sizeinfo(struct drct_s_file *handle, size_t *numutilized,
  size_t *utilizedsize, size_t *numallocated, size_t *allocatedsize, 
  size_t *numfragments, size_t *fragmentsize)
#else
int drct_
int drct_sizeinfo(handle, numutilized, utilizedsize, numallocated,
	allocatedsize, numfragments, fragmentsize)
 size_t *numutilized, *utilizedsize, *numallocated, *allocatedsize, 
    *numfragments, *fragmentsize;
#endif
{
  int stat;
  off_t currec;
  size_t nrecs;
  struct stat statbuf;

  *numutilized = 0;
  *utilizedsize = 0;
  *numallocated = 0;
  *allocatedsize = 0; 
  *numfragments = 0;
  *fragmentsize = 0;

  if (fstat(handle->idx, &statbuf)) goto ERREXIT;
  if ((nrecs = statbuf.st_size / sizeof(handle->drct)) < 0) goto ERREXIT;

  for (stat = 0, currec=0L; currec < nrecs; ++currec) {
    if (!drct_setpos(handle, currec)) goto ERREXIT;
    ++*numallocated;
    *allocatedsize += handle->drct.alloc;

    if (handle->drct.length >= 0) {
      ++*numutilized;
      *utilizedsize += handle->drct.length;
	  *numfragments += (handle->drct.alloc != handle->drct.length);
      *fragmentsize += (handle->drct.alloc - handle->drct.length);
    }
  }
  return 1;

ERREXIT:
  return 0;
}
