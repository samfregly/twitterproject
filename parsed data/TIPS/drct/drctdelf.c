/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_delfile                                      delfile.c

Function : Deletes direct access file.

Author : A. Fregly

Abstract : This function is used to delete a direct access file. The specified
	data file is deleted along with it's record index file (.drc extension).

Calling Sequence : int stat = drct_delfile(char *fspec);

	fspec           Direct access file to be deleted.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <drct.h>

#if FIT_ANSI
int drct_delfile(char *fspec)
#else
int drct_delfile(fspec)
  char *fspec;
#endif
{
  char idxfspec[FIT_FULLFSPECLEN+1];

  if (fit_fdelete(fspec)) {
    fit_setext(fspec, ".drc",idxfspec);
    return fit_fdelete(idxfspec);
  }
  else
    return 0;
}
