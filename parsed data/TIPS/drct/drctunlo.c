/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_unlock                                       unlock.c

Function : Unlocks records in direct access file.

Author : A. Fregly

Abstract : This function is used to unlock records in a direct access file
	which were locked, either explicitly with calls to drct_rlock or
	drct_wlock, or implicitly with drct_write, drct_append, drct_rdrec,
	drct_rdseq (when in the middle of a multiple buffer i/o operation).

Calling Sequence : int stat = drct_unlock(struct drct_s_file *handle) 

	handle          Handle for direct access file returned by drct_open.
	stat            Returned status, 0 if an error occurs.

Notes: 

Change log : 
000     13-JUN-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
002     23-MAR-93  AMF  Compile under GNU C.
003     19-FEB-07  AMF  Cast "offset" arg of lseek to off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <fcntl.h>
#if FIT_ZORTECH || FIT_MSC
#include <sys\locking.h>
#include <io.h>
#endif
#include <drct.h>

/* Unlock direct access file */
#if FIT_ANSI
int drct_unlock(struct drct_s_file *handle)
#else
int drct_unlock(handle)
  struct drct_s_file *handle;
#endif
{
#define RECLEN sizeof(handle->drct)

#if !FIT_DOS
  struct flock lck;
#endif

#if FIT_BSD
  return 1;
#endif

  if (handle) {
#if FIT_TURBOC
    handle->uselocks = 0;
#endif
    if (!handle->locked || !handle->uselocks) return 1;
    handle->locked = 0;
    /* Unlock file */
#if !FIT_DOS
    lck.l_type = F_UNLCK;
    lck.l_whence = 0;
    lck.l_start = handle->lockstart * RECLEN;
    lck.l_len = handle->locknrecs * RECLEN;
    if (fcntl(handle->idx, F_SETLK, &lck) >= 0) return 1;
#else
#if FIT_TURBOC
    if (!unlock(handle->idx, (long) (handle->lockstart * RECLEN),
      (long) (handle->locknrecs * RECLEN))) return 1;
#endif
#if FIT_ZORTECH || FIT_MSC
    lseek(handle->idx, (off_t) (handle->lockstart * RECLEN), SEEK_SET);
    if (!locking(handle->idx, LK_UNLCK, handle->locknrecs * RECLEN))
      return 1;
#endif
#endif
  }
  return 0;
}
