/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_tmpopen                                      drcttmpo.c

Function : Opens a temporary direct access file.

Author : A. Fregly

Abstract : This function is used to open a direct access file for temporary
	use. The tmpfile() function is used to create the file, so the system
	will automatically delete it when the user closes the file. By default,
	file locking is not enabled for the file. The caller must set the
	"uselocks" flag in the handle if file locking is desired, and should
	set the read and write timeouts.

Calling Sequence : struct drct_s_file *handle = drct_tmpopen();

Notes: 

Change log : 
000     14-MAR-92  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
002     23-MAR-93  AMF  Compile under GNU C.
003     08-SEP-93  AMF  HPUX compatibility.
004     17-MAR-07  AMF  Don't disturb other settings for file when setting
                        close on EXEC bit.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <sys/stat.h>
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <errno.h>
#if !FIT_HP
#include <lowlvlio.h>
#endif
#include <fcntl.h>
#include <drct.h>

#if FIT_DOS
#define APPEND "wb+"
#else
#define APPEND "w+"
#endif

/* Open a direct access file */
#if FIT_ANSI
struct drct_s_file *drct_tmpopen()
#else
struct drct_s_file *drct_tmpopen()
#endif
{
  struct drct_s_file *handle;
  int flags;

  handle = (struct drct_s_file *) calloc(1,sizeof(*handle));
  if (!handle) return (struct drct_s_file *) NULL;

  handle->data = -1;
  handle->idx = -1;
  handle->idxhandle = (FILE *) NULL;
  handle->datahandle = (FILE *) NULL;
  handle->uselocks = 0;
  handle->dataspec = NULL;
  handle->idxspec = NULL;

#if !FIT_COHERENT
  if (!(handle->datahandle = tmpfile())) goto ERREXIT;
  if (!(handle->idxhandle = tmpfile())) goto ERREXIT;
#else
  if ((handle->dataspec = (char *) calloc(FIT_FULLFSPECLEN+1, 1)))
    tmpnam(handle->dataspec);
  if ((handle->idxspec = (char *) calloc(FIT_FULLFSPECLEN+1, 1)))
    tmpnam(handle->idxspec);
  if (!(handle->datahandle = fopen(handle->dataspec, APPEND))) goto ERREXIT;
  if (!(handle->idxhandle = fopen(handle->idxspec, APPEND))) goto ERREXIT;
#endif
  handle->data = fileno(handle->datahandle);
  handle->idx = fileno(handle->idxhandle);
#if FIT_UNIX || FIT_COHERENT
  if ((flags = fcntl(handle->data, F_GETFD, 0)) >= 0)
    flags = fcntl(handle->data, F_SETFD, flags | FD_CLOEXEC);
  if (flags < 0) goto ERREXIT;
#endif
#if FIT_UNIX || FIT_COHERENT
  if ((flags = fcntl(handle->idx, F_GETFD, 0)) >= 0)
    flags = fcntl(handle->idx, F_SETFD, flags | FD_CLOEXEC);
  if (flags < 0) goto ERREXIT;
#endif

  handle->mode = O_RDWR;
  handle->rdtimeout = -1;
  handle->wrtimeout = -1;
  handle->currec = -1;
  return handle;

ERREXIT:
  drct_close(handle);
  return (struct drct_s_file *) NULL;
}
