/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_rdseq                                        rdseq.c

Function : This function is used to sequentially read a direct access file.

Author : A. Fregly

Abstract : This function is used to perform sequential reads of records
	in a direct access file. The routine allows multiple successive calls
	to be performed to read records which are larger than the callers
	input buffer.

Calling Sequence : int nbytes = drct_rdseq(struct drct_s_handle *handle, 
  char *buffer, size_t buflen, size_t *reclen)

  handle        Direct access file handle returned by drct_open.
  buffer        Buffer in which to put data.
  buflen        Length of buffer.
  reclen        Returned record length. If reclen is larger than buflen,
		the application will need to perform multiple reads to retrieve
		the remainder of the record.
  nbytes        Number of bytes read into buffer. If the record is empty,
		or unreadable, a value of 0 is returned.

Notes: 
001     On non-DOS systems, a record will remain read locked until another
	record is accessed, or the record is explicitly unlocked with a call
	to drct_unlock.

Change log :
000     23-JUN-91  AMF  Created.
001     14-DEC-92  AMF  Compile under COHERENT.
002     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg for lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

#if FIT_ANSI
int drct_rdseq(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t *reclen)
#else
int drct_rdseq(handle, buffer, buflen, reclen)
  struct drct_s_file *handle;
  char *buffer;
  size_t buflen;
  size_t *reclen;
#endif
{
  size_t recleft;
  off_t currec;
  int stat;

  if (handle->curop != 'R') {
	  /* printf("drct_rdseq, scanning for next record\n"); */
    if (handle->inrecoff >= handle->drct.length)
      currec = handle->currec+1;
    else
      currec = handle->currec;

      /* Find next record */
    for (;; ++currec) {
      if (!drct_rlock(handle, currec, 1L)) {
		  /* printf("drct_rdseq, error readlocking file\n"); */
		  goto ERREXIT;
	  }
      if (!drct_setpos(handle, currec)) {
		  /* printf("drct_rdseq, error setting position in file\n"); */
		  goto ERREXIT;
	  }
      if (handle->drct.length >= 0) {
		  /* printf("drct_rdseq, found record #%ld\n", currec); */
		  break;
	  }
    }
    handle->curop = 'R';

    /* Set up for reading from the start of the data record */
    handle->inrecoff = 0;
    *reclen = handle->drct.length;
	/* printf("drct_rdseq, data record offset: %ld, record length: %ld\n", handle->drct.offset,
		handle->drct.length); */
    if (lseek(handle->data, (off_t) handle->drct.offset, SEEK_SET) != handle->drct.offset) {
		/* printf("drct_rdseq, error on lseek to start of data record\n"); */
      goto ERREXIT;
	}
  }

  /* See how much of the record is left to be read */
  recleft = handle->drct.length - handle->inrecoff;
  if (buflen > recleft) buflen = recleft;
  /* printf("drct_rdseq, attempting to read %d bytes at loc %ld\n", buflen, handle->inrecoff); */
  handle->inrecoff += buflen;

  if (buflen > 0) {
#if !FIT_TURBOC
#if FIT_ANSI
    stat = (read(handle->data, (void *) buffer, (unsigned) buflen) == buflen);
#else
    stat = (read(handle->data, buffer, (unsigned) buflen) == buflen);
#endif
#else
    stat = (_read(handle->data, (void *) buffer, (unsigned) buflen) == buflen);
#endif
	/*******************
	if (stat <= 0) {
		if (stat == 0)
			printf("drct_rdseq, reached eof on data file\n");
		else 
			perror("drct_rdseq, error on data file read\n");
	}
	else
		printf("drct_rdseq, read data file successfully\n");
	*******************/
  }
  else {
	/*  printf("drct_rdseq, buffer had length of: %d\n", buflen); */
    stat = 1;
  }

  if (handle->inrecoff >= handle->drct.length) {
    /* Read has completed for this record */
    handle->curop = 0;
#if FIT_DOS
    drct_unlock(handle);
#endif
  }

  return (stat) ? (int) buflen : 0;

ERREXIT:
  /* perror("drct_rdseq: aborted by error"); */
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
