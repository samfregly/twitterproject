/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_rdrec                                        rdrec.c

Function : Reads direct access data record from file.

Author : A. Fregly

Abstract : This function is used to perform a direct access read of a record
	in a direct access file. The routine allows multiple successive calls
	to be performed to read records which are larger than the callers
	input buffer.

Calling Sequence : int stat = drct_rdrec(struct drct_s_handle *handle, 
  off_t recnum, char *buffer, size_t buflen, size_t *reclen)

  handle        Direct access file handle returned by drct_open.
  recnum        Record to be read.
  buffer        Buffer in which to put data.
  buflen        Length of buffer.
  reclen        Returned record length. If reclen is larger than buflen,
		the application will need to perform multiple reads to retrieve
		the remainder of the record.

Notes: 
001     On non-DOS systems, a record will remain read locked until
	another record is accessed, or the record is explicitly unlocked with a
	call to drct_unlock.

Change log :
000     23-JUN-91  AMF  Created.
001     19-FEB-07  AMF  Use SEEK_xxx for "whence" arg for lseek. Cast offset to
                        off_t.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>

/* Read specified record in file */
#if FIT_ANSI
int drct_rdrec(struct drct_s_file *handle, off_t recnum,
  char *buffer, size_t buflen, size_t *reclen)
#else
int drct_rdrec(handle, recnum, buffer, buflen, reclen)
  struct drct_s_file *handle;
  off_t recnum;
  char *buffer;
  size_t buflen;
  size_t *reclen;
#endif
{
  size_t recleft;
  int stat;

  if (handle->curop != 'R' || handle->currec != recnum ||
      handle->inrecoff == 0 || handle->inrecoff >= handle->drct.length) {

    handle->curop = 'R';

    /* Read lock the record to be accessed */
    if (!drct_rlock(handle, recnum, (size_t) 1)) return 0;

    /* Read the index entry for the record */
    if (!drct_setpos(handle, recnum)) goto ERREXIT;
    *reclen = (handle->drct.length > 0) ? handle->drct.length : 0;
    if (*reclen == 0) goto ERREXIT;

    /* Set up for reading from the start of the data record */
    handle->inrecoff = 0;
    if (lseek(handle->data, (off_t) handle->drct.offset, SEEK_SET) != handle->drct.offset)
      goto ERREXIT;
  }

  /* See how much of the record is left to be read */
  recleft = handle->drct.length - handle->inrecoff;
  if (buflen > recleft) buflen = recleft;
  handle->inrecoff += buflen;

  if (buflen > 0) {
#if !FIT_TURBOC
    stat = (read(handle->data, buffer, buflen) == buflen);
#else
    stat = (_read(handle->data, (void *) buffer, (unsigned) buflen) == buflen);
#endif

#if FIT_DOS
    drct_unlock(handle);
#endif
  }
  else
    stat = 1;

  if (handle->inrecoff >= handle->drct.length)
    /* Read has completed for this record */
    handle->curop = 0;

  return (stat) ? buflen : 0;

ERREXIT:
  if (handle->locked) drct_unlock(handle);
  handle->curop = 0;
  return 0;
}
