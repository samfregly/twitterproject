/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************Module Name : drct_findfree                                     findfree.c

Function : Finds free space in direct access file.

Author : A. Fregly

Abstract : This function will search the record index for a unallocated
	space of at least the requested size. The search starts at the
	location following the current record, and continues circularly
	until a record is located or a wrap to the current position occurs.

	If a free space is allocated, the current record will be set to
	the record containing the free block, and drct_trimrec may be used
	to truncate the free space from the allocation for the record. Also,
	a write lock will be active on the located record.

Calling Sequence : int stat = drct_findfree(struct drct_s_handle *handle, 
  size_t reclen, off_t *freeoffset, size_t *freespace);

  handle        Direct access file handle returned by drct_open.
  reclen        Amount of free space to search for.
  freeoffset    Returned offset of free block in data file.
  freespace     Size of free block.

Notes:

Change log :
000     25-JUN-91  AMF  Created.
******************************************************************************/

#include <stdio.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#include <lowlvlio.h>
#include <drct.h>
#if !FIT_DOS
#include <sys/stat.h>
#else
#include <sys\stat.h>
#endif

#if FIT_ANSI
int drct_findfree(struct drct_s_file *handle, size_t reclen, off_t *freeoffset,
  size_t *freespace)
#else
int drct_findfree(handle, reclen, freeoffset,freespace)
  struct drct_s_file *handle;
  off_t *freeoffset;
  size_t reclen, *freespace;
#endif
{
  int stat;
  size_t nrecs;
  off_t endrec, currec;
  struct stat statbuf;

  if (fstat(handle->idx, &statbuf)) goto ERREXIT;
  if ((nrecs = statbuf.st_size / sizeof(handle->drct)) <= 0) goto ERREXIT;

  if (handle->currec < 0)
    handle->currec = 0;
  else if (handle->currec >= nrecs)
    handle->currec = nrecs-1;

  /* Look for unallocated space in data file */
  for (stat = 0, endrec = handle->currec, currec = endrec+1;;++currec) {
    if (currec >= nrecs)
      currec = 0;

    if (!drct_wlock(handle, currec, (size_t) 1)) goto ERREXIT;

    if (!drct_setpos(handle, currec)) goto ERREXIT;

    if (handle->drct.length < 0)
      *freespace = handle->drct.alloc;
    else
      *freespace = handle->drct.alloc - handle->drct.length;

    if (*freespace >= reclen) {
      stat = 1;
      break;
    }
    if (currec == endrec) break;
  }

  if (stat) {
    if (handle->drct.length >= 0)
      *freeoffset = handle->drct.offset + handle->drct.length;
    else
      *freeoffset = handle->drct.offset;
    return 1;
  }

ERREXIT:
  drct_unlock(handle);
  return 0;
}
