/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct_open                                         open.c

Function : Open a direct access file.

Author : A. Fregly

Abstract : This function is used to open a direct access file. Once opened
	by this routine, the file is available for manipulation be the
	other drct_xxxx funtions.

Calling Sequence : struct drct_s_file *handle = drct_open(char *fspec, 
  int access, unsigned prot, int rdtimeout, int wrtimeout);

	fspec           Name of data file to be opened. Note that implicit
			in the data file name is the name of the record
			index file, which will be the same as the data file
			name except for the file type being ".drc".
	access          File access options (as defined in <sys/fcntl.h>).
	prot            File protection for access = O_CREAT. If prot is
			0, a default value of u+rw, g+r is used. Protection
			bits are define in <sys/stat.h>.
	rdtimeout       Number of seconds to wait before timing out waiting
			on a read lock. A value of 0 means no wait, and -1
			is used for an infinite wait.
	wrtimeout       Number of seconds to wait before timing out waiting
			on a write lock. A value of 0 means no wait, and -1
			is used for an infinite wait.
	handle          Returned file handle for use with other drct_xxxx
			functions. A value of NULL is returned if an error
			prevents the file from being opened.

Notes:
001     On DOS systems, the only types of locks available are exclusive
	locks. Therefore, on DOS systems, the 'wrtimeout' value is used
	for both "read" and "write" locks, which are both implemented as
	exclusive locks. The major drawback to the DOS implementation is
	that locks cannot be left in place for any length of time, as they
	tend to totally exclude access by others.

Change log :
000     13-JUN-91  AMF  Created.
001     16-SEP-91  AMF  Return "File exists" error if attempted creation and
			a ".drc" file already exists.
002     14-DEC-92  AMF  Compile under COHERENT.
003     23-MAR-93  AMF  Compile under GNU C.
004     08-SEP-93  AMF  HPUX compatibility
005	27-JUN-97  AMF	Don't access errno directly under MSC.
006     17-MAR-07  AMF  Don't disturb other flags in file description when 
                        seting close on EXEC flag.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <fitsydef.h>
#if FIT_TURBOC
#include <alloc.h>
#endif
#if FIT_STDLIB
#include <stdlib.h>
#endif
#include <errno.h>
#if !FIT_HP & !FIT_MSC
#include <lowlvlio.h>
#endif
#if FIT_MSC
#include <io.h>
#endif
#if FIT_UNIX || FIT_COHERENT
#include <sys/stat.h>
#else
#include <sys\stat.h>
#endif
#include <fcntl.h>
#include <drct.h>

/* Open a direct access file */
#if FIT_ANSI
struct drct_s_file *drct_open(char *fspec, int access, unsigned prot,
  int rdtimeout, int wrtimeout)
#else
struct drct_s_file *drct_open(fspec, access, prot, rdtimeout, wrtimeout)
  char *fspec;
  unsigned prot;
  int access, rdtimeout, wrtimeout;
#endif
{
  char idxfspec[FIT_FULLFSPECLEN+1];
  struct drct_s_file *handle;
  struct stat statbuf;
  int flags;

  fit_setext(fspec,".drc", idxfspec);
  if (access & O_CREAT) {
    if (!stat(idxfspec, &statbuf)) {
#if FIT_UNIX || FIT_COHERENT
      errno = EEXIST;
#endif
      return (struct drct_s_file *) NULL;
    }
  }

  handle = (struct drct_s_file *) calloc(1,sizeof(*handle));
  if (!handle) return (struct drct_s_file *) NULL;

  handle->data = -1;
  handle->idx = -1;
  handle->datahandle = (FILE *) NULL;
  handle->idxhandle = (FILE *) NULL;
  handle->dataspec = (char *) NULL;
  handle->idxspec = (char *) NULL;
  handle->uselocks = 1;
  if (!prot) prot = 0640;
#if FIT_MSC
  access |= _O_BINARY;
#endif

  handle->data = open(fspec, access, prot);
  if (handle->data == -1) goto ERREXIT;
#if FIT_UNIX || FIT_COHERENT
  if ((flags = fcntl(handle->data, F_GETFD, 0)) >= 0) {
    flags |= FD_CLOEXEC;
    flags = fcntl(handle->data, F_SETFD, flags);
  }
  if (flags < 0) goto ERREXIT;
#endif

  handle->idx = open(idxfspec, access, prot);
  if (handle->idx == -1) goto ERREXIT;
#if FIT_UNIX || FIT_COHERENT
  if ((flags = fcntl(handle->idx, F_GETFD, 0)) >= 0) {
    flags |= FD_CLOEXEC;
    flags = fcntl(handle->idx, F_SETFD, flags);
  }
  if (flags < 0) goto ERREXIT;
#endif

  handle->dataspec = (char *) malloc(strlen(fspec)+1);
  if (!handle->dataspec) goto ERREXIT;
  strcpy(handle->dataspec, fspec);

  handle->idxspec = (char *) malloc(strlen(idxfspec)+1);
  if (!handle->idxspec) goto ERREXIT;
  strcpy(handle->idxspec,idxfspec);

  handle->mode = access;
  handle->rdtimeout = rdtimeout;
  handle->wrtimeout = wrtimeout;
  handle->currec = -1;
  return handle;

ERREXIT:
  drct_close(handle);
  return (struct drct_s_file *) NULL;
}
