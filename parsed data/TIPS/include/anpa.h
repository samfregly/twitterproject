/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa.h

Function : Program specific include file for the anpa program.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <anpa.h>

Notes : 

Change log : 
000	18-MAY-91  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++.
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Define ANPA document fields */
#define ANPA_NFIELDS 14
#define ANPA_FIELDS {"SRC","DATA","TEXT","SLVL","NUM","SLCT","PRIO","CAT", \
"KEY","VER","REF","FDAT","WC","TDAT"}

#define ANPA_SRC 0
#define ANPA_DATA 1
#define ANPA_TEXT 2
#define ANPA_SLVL 3
#define ANPA_NUM 4
#define ANPA_SLCT 5
#define ANPA_PRIO 6
#define ANPA_CAT 7
#define ANPA_KEY 8
#define ANPA_VER 9
#define ANPA_REF 10
#define ANPA_FDAT 11
#define ANPA_WC 12
#define ANPA_TDAT 13

extern char *anpa_fields[ANPA_NFIELDS];
extern int anpa_fieldnums[ANPA_NFIELDS];
extern char *(*anpa_fielddata);

/* Define characters used in parsing documents */
#define ANPA_NUL 0
#define ANPA_SOH 1
#define ANPA_STX 2
#define ANPA_ETX 3
#define ANPA_EOT 4
#define ANPA_ACK 6
#define ANPA_TLI 8
#define ANPA_LF 0xA
#define ANPA_CR 0xD
#define ANPA_NAK 0x15
#define ANPA_SYN 0x16
#define ANPA_ETB 0x17
#define ANPA_ESC 0x1B
#define ANPA_FS 0x1C
#define ANPA_DEL 0x7F

/* ANPA control characters */
#define ANPA_TTS 020
#define ANPA_EMS 031
#define ANPA_TSP 035
#define ANPA_ENS 036
#define ANPA_TFI 037
#define ANPA_ENL 043
#define ANPA_EML 052
#define ANPA_QL 074
#define ANPA_QC 075
#define ANPA_QR 076
#define ANPA_LRL 0100
#define ANPA_URL 0136
#define ANPA_EMD 0137

extern int anpa_newdoc;		/* New input document flag. This flag is
				   set to 1 at the start of a new document
				   so tips_getline will know to purge the
				   input buffer. */

/* The output buffer */
#define ANPA_BUFFERSIZE 4096
extern char anpa_outbuffer[ANPA_BUFFERSIZE+1];

#if FIT_ANSI
int anpa_upihdr(long *inlen, FILE *out);
int anpa_hdrline1(int doflds, long *inlen, FILE *out);
int anpa_hdrline2(int doflds, long *inlen, FILE *out);
int anpa_text(int doflds, long *inlen, FILE *out);
void anpa_setfld(int fieldidx, int *offset);
void anpa_fldmark(int fieldidx, int *offset);
int anpa_isnumeric(char *text, int len);
void anpa_addtext(char *text, int textlen, int *outidx);
void anpa_dofld(int doflds, char *(*bufptr), int *bufleft, int *fldlen, 
  int *fldtype, int *outidx);
int anpa_fldtype(char *fldtext, int fldlen);

#else
int anpa_upihdr();
int anpa_hdrline1();
int anpa_hdrline2();
int anpa_text();
void anpa_setfld();
void anpa_fldmark();
int anpa_isnumeric();
void anpa_addtext();
void anpa_dofld();
int anpa_fldtype();
#endif

#ifdef __cplusplus
};
#endif
/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpa.h

Function : Program specific include file for the anpa program.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <anpa.h>

Notes : 

Change log : 
000	18-MAY-91  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++.
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Define ANPA document fields */
#define ANPA_NFIELDS 14
#define ANPA_FIELDS {"SRC","DATA","TEXT","SLVL","NUM","SLCT","PRIO","CAT", \
"KEY","VER","REF","FDAT","WC","TDAT"}

#define ANPA_SRC 0
#define ANPA_DATA 1
#define ANPA_TEXT 2
#define ANPA_SLVL 3
#define ANPA_NUM 4
#define ANPA_SLCT 5
#define ANPA_PRIO 6
#define ANPA_CAT 7
#define ANPA_KEY 8
#define ANPA_VER 9
#define ANPA_REF 10
#define ANPA_FDAT 11
#define ANPA_WC 12
#define ANPA_TDAT 13

extern char *anpa_fields[ANPA_NFIELDS];
extern int anpa_fieldnums[ANPA_NFIELDS];
extern char *(*anpa_fielddata);

/* Define characters used in parsing documents */
#define ANPA_NUL 0
#define ANPA_SOH 1
#define ANPA_STX 2
#define ANPA_ETX 3
#define ANPA_EOT 4
#define ANPA_ACK 6
#define ANPA_TLI 8
#define ANPA_LF 0xA
#define ANPA_CR 0xD
#define ANPA_NAK 0x15
#define ANPA_SYN 0x16
#define ANPA_ETB 0x17
#define ANPA_ESC 0x1B
#define ANPA_FS 0x1C
#define ANPA_DEL 0x7F

/* ANPA control characters */
#define ANPA_TTS 020
#define ANPA_EMS 031
#define ANPA_TSP 035
#define ANPA_ENS 036
#define ANPA_TFI 037
#define ANPA_ENL 043
#define ANPA_EML 052
#define ANPA_QL 074
#define ANPA_QC 075
#define ANPA_QR 076
#define ANPA_LRL 0100
#define ANPA_URL 0136
#define ANPA_EMD 0137

extern int anpa_newdoc;		/* New input document flag. This flag is
				   set to 1 at the start of a new document
				   so tips_getline will know to purge the
				   input buffer. */

/* The output buffer */
#define ANPA_BUFFERSIZE 4096
extern char anpa_outbuffer[ANPA_BUFFERSIZE+1];

#if FIT_ANSI
int anpa_upihdr(long *inlen, FILE *out);
int anpa_hdrline1(int doflds, long *inlen, FILE *out);
int anpa_hdrline2(int doflds, long *inlen, FILE *out);
int anpa_text(int doflds, long *inlen, FILE *out);
void anpa_setfld(int fieldidx, int *offset);
void anpa_fldmark(int fieldidx, int *offset);
int anpa_isnumeric(char *text, int len);
void anpa_addtext(char *text, int textlen, int *outidx);
void anpa_dofld(int doflds, char *(*bufptr), int *bufleft, int *fldlen, 
  int *fldtype, int *outidx);
int anpa_fldtype(char *fldtext, int fldlen);

#else
int anpa_upihdr();
int anpa_hdrline1();
int anpa_hdrline2();
int anpa_text();
void anpa_setfld();
void anpa_fldmark();
int anpa_isnumeric();
void anpa_addtext();
void anpa_dofld();
int anpa_fldtype();
#endif

#ifdef __cplusplus
};
#endif
