/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  TIPS.H

Function :  TIPS public definitions file

Author : A. Fregly

Abstract :

Calling Sequence :

Notes :

Change log :
000	17-NOV-90  AMF	Created.
001	26-MAR-93  AMF	Fix prototype for tips_sendmsg to match function.
002	15-NOV-97  AMF	Modified to work better with C++
003	06-DEC-03  AMF  Added default port number for daemons.
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef TIPS_INC
#define TIPS_INC 1

#include <qc.h>

#if FIT_DOS
#include <drct.h>
#endif

#ifndef TIPS_DEBUG
#define TIPS_DEBUG 0
#endif

/* Default delimiter string		*/
/*   offset	usage 		defualt	*/
/*   ------	-----		_______	*/
/*     	0	Open paren 	   (	*/
/*	1	Close paren	   )	*/
/*	2	Quoting char	   \	*/
/*	3	SCAWC		   %	*/
/*	4	ASWC		   *	*/
/*	5	SCNWC		   #	*/
/*	6	NSWC		   @	*/
/*	7	SETWC open	   /	*/
/*	8	SETWC close	   /	*/
/*	9	Range separator	   :	*/
/*	10	AVLWC		   &	*/
/*	11	NVLWC		   !	*/
/*	12	AFLDC		   ?	*/
/*	13	NFLDC		   ;	*/
/*	14	Field name	   ^	*/
/*	15	Comma operator	   ,	*/

#define TIPS_DEFAULTDELIMS "()\\%*#@//:&!?;^,"

/* Default reserved words. Note that null entries are place holders */
#define TIPS_NRESERVED 12
#define TIPS_DEFRESERVED {"OR","AND","","","","","ANY","ALL","NOT", \
  "WITHIN","AHEAD",""}

#define TIPS_QUERYTEXTMAX 64536
#define TIPS_MAXFIELD (sizeof(long) * 8) 
#define TIPS_DESCRIPTORMAX 16384 
#define TIPS_FLDRECMAX 256 
#define TIPS_MAXTERMWORDLEN 24
#define TIPS_MAXOFFSET 16384

/* Define seed to be used in calls to ftok */
#define TIPS_FTOKSEED	(((unsigned) ('A'+'M'+'F')) % 256)

/* Communications parameters for message ports */
#if FIT_UNIX || FIT_COHERENT
struct tips_s_msghdr {
  long msgtype;
  int pid;
  unsigned datalen;
};
#endif
#if FIT_DOS
struct tips_s_msghdr {
  long msgtype;
  long seqnum;
  int pid;
  unsigned datalen;
};
#endif

#if FIT_UNIX | FIT_CYGWIN
typedef int tips_msgqueue;
#endif

#if FIT_DOS
typedef struct drct_s_file *tips_msgqueue;
#endif

/* Maximum message length */
#define TIPS_MAXMSGDATA 256
#define TIPS_MSGBUFLEN (TIPS_MAXMSGDATA + sizeof(tips_s_msghdr))

/* Default port number for daemons */
#define TIPS_SPOTTER_DEFAULT_PORT "4301"
#define TIPS_FPREP_DEFAULT_PORT "4302"

struct tips_s_msg {
  struct tips_s_msghdr hdr;
  char data[TIPS_MAXMSGDATA];
};

/* Reserved message types */
/* Default is a dummy which doesn't do anything */
#define TIPS_RQST_DEFAULT 1

/* Shut down */
#define TIPS_RQST_SHUTDOWN 2

/* Reset - all counters and temp files are reset */
#define TIPS_RQST_RESET 3

/* A dump is written to a dump file */
#define TIPS_RQST_DUMP 4

/* Enables status logging */
#define TIPS_RQST_ENABLOG 5

/* Disables status logging */
#define TIPS_RQST_DISABLOG 6

/* Sets input to specified file until EOF */
#define TIPS_RQST_SETINPUT 7

/* Sets output to specified file until EOF */
#define TIPS_RQST_SETOUTPUT 8

/* daemon specific request codes */
/* Request a spotter to load/reload queries in specified library */
#define TIPS_RQST_SETLIB 33


/* Maximum message number. Return messages for synchronous requests should
   have a message id = to the process id + TIPS_MAXMSGNUM + 1 */
#define TIPS_MAXMSGNUM 127L

/* Functions */

#if FIT_ANSI

/* Extract the TIPS document descriptor which is prefixing the text in an
buffer */
char *tips_extractdescr(char *buffer, char *descriptor, int *descriptorlen);

/* Get display attributes */
void tips_getattributes(int *usecolor, int *usepointer, int *usecheckmarks,
  int *usehotkeys, int *uselinedrawing, int *dispnorm, int *disphigh, 
  int *dispsel, int *disphot, int *mennorm, int *menhigh, int *mensel, 
  int *menhot);

/* Get function keys */
void tips_getfunckeys(int *menukey, char *(*menukeyname), int *helpkey, 
  char *(*helpkeyname), int *exitkey, char *(*exitkeyname), int *quitkey, 
  char *(*quitkeyname));

/* Get line of input during filtering of a descriptor defined input stream */
char *tips_getline(int *newdoc, FILE *in, long *docleft, int *len);

/* Get object query as specified in search result key field */
int tips_getqry(char *keyfld, struct qc_s_cmpqry *(*cmpqry));

/* Invoke help function for current program */
int tips_help(char *progname);

/* Load field names from a field definition file */
int tips_loadflds(char *fieldfile, char *(*(*fields)), char *(*(*fielddata)),
  int *nfields);

/* Make TIPS document descriptor */
void tips_makedescr(char *archive, long docnum, char *origname,
  long doclen, char *key, int buflen, char *buf, int *retlen);

/* Map fields known by application to those loaded from field definition file */
void tips_mapflds(char *appfields[], int nappfields,
	char *loadedfields[], int nloaded, int fieldnums[]);

/* Parse the name field of a TIPS file descriptor */
int tips_nameparse(char *namefld, char *dbname, char *docnum_s,
  char *docname);

/* Parse TIPS file descriptor */
int tips_parsedsc(char *descriptor, char *namefld, long *lenfld,
  char *keyfld);

/* Create a new message queue */
tips_msgqueue tips_newmsgqueue(char *key, unsigned prot);

/* Attach to an existing message queue */
/*
tips_msgqueue tips_attachmsgqueue(char *key);
*/

/* Delete a message queue */
/*
int tips_rmvmsgqueue(char *key);
*/

/* Clear out a message queue */
int tips_clrmsgqueue(tips_msgqueue msgqid);

/* Receive a message */
/* int tips_rcvmsg(tips_msgqueue msgqid, long msgtype, int timeout,
  char *msgbuf, unsigned maxmsglen, unsigned *msglen, long *retmsgtype,
  int *senderpid);
*/

/* Send a message */
/*
int tips_sendmsg(tips_msgqueue msgqid, long msgtype, char *msg, 
  unsigned msglen, int timeout, char *retmsg, unsigned maxmsglen, 
  unsigned *retlen, long *retmsgtype, int *receiverpid);
*/

#else
char *tips_extractdescr();
char *tips_getline();
int tips_getqry();
int tips_loadflds();
void tips_makedescr();
void tips_mapflds();
int tips_nameparse();
int tips_parsedsc();
/*****
tips_msgqueue tips_newmsgqueue();
tips_msgqueue tips_attachmsgqueue();
******/
int tips_clrmsgqueue();
int tips_rmvmsgqueue();
int tips_rcvmsg();
int tips_sendmsg();
#endif
#endif

#ifdef __cplusplus
};
#endif
