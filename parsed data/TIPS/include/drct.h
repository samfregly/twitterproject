/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : drct						drct.h

Function : Defines library for accessing direct access variable length
	record files.

Author : A. Fregly

Abstract : This file defines a library of routines for manipulation
	of direct access variable record length files. Record locking
	and timeouts on writes and reads are supported. Record locking
	is accomplished by use of temporary files at the location of
	accessed data files, so network operation should be supported.
	Performance will be poor when using record locking, so it's use
	should be restricted to programs which update the data file, and
	applications which must be guaranteed of the integrity of the
	data file.

Calling Sequence : #include <drct.h>

Notes:
	stdio.h, fitsydef.h, and lkmg.h must be included before the inclusion
	of this file.

Change log :
000  08-JUN-91  AMF	Created.
001  09-JUL-97  AMF	Added prototype for drct_sizeinfo.
002	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef DRCT_INC
#define DRCT_INC

/* Record structure for idx file */
struct drct_s_idx {
  off_t offset;		/* Offset of record in data file */
  size_t length;	/* Length of record in data file. Negative if record
			   has been deleted. */
  size_t alloc;		/* Number of bytes allocated for record in data file. */
};

/* Handle for a direct access file */
struct drct_s_file {
  unsigned int mode;	/* Open mode bit map */
  int uselocks;		/* Use locks */
  int rdtimeout;	/* Read timeout in seconds */
  int wrtimeout;	/* Write timeout in seconds */
  char *dataspec;	/* Data file specification */
  char *idxspec;	/* Record index file specification */
  int idx;		/* File descriptor for record index file */
  int data;		/* File descriptor for data file */
  FILE *idxhandle;	/* File handle for temp record index file */
  FILE *datahandle;	/* File handle for temp data file */
  int curop;		/* Current operation in progress:
			     0	No operation in progreess.
			    'A' Record append.
			    'R' Record read.
			    'W' Record write. */
  off_t currec;		/* Index for current record */
  struct drct_s_idx drct;/* Index entry for current record */
  off_t inrecoff;	/* Offset for data access within current record */
  int locked;		/* Flag indicating if lock is currently in place */
  int locktype;		/* Type of lock */
  off_t lockstart;	/* Index of first record locked. */
  size_t locknrecs;	/* Number of records locked. */
};

/* Info structure for direct access file */
struct drct_s_info {
  size_t datasize;	/* Size of data file in bytes */
  size_t nrecs;		/* Number of records in data file */
  unsigned int openmode;/* Current file open mode bit map */
  int uselocks;		/* Tells if lock usage is enabled */
  int rtimeout;		/* Read timeout in seconds */
  int wtimeout;		/* Write timeout in seconds */
};

#if FIT_ANSI
/* Append record to direct access file */
int drct_append(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen);

/* Cancel currently in progress i/o operation */
void drct_cancelio(struct drct_s_file *handle);

/* Close direct access file */
void drct_close(struct drct_s_file *handle);

/* Compress direct access file */
int drct_compress(struct drct_s_file *handle);

/* Get current record number */
off_t drct_curpos(struct drct_s_file *handle);

/* Delete direct access file */
int drct_delfile(char *fspec);

/* Delete record in direct access file */
int drct_delrec(struct drct_s_file *handle, int destroyopt);

/* PRIVATE, Attempt to find a unallocated space in the data file. */
int drct_findfree(struct drct_s_file *handle, size_t reclen, off_t *offset,
  size_t *length);

/* PRIVATE, Append entry to record index */
off_t drct_idxappnd(struct drct_s_file *handle, off_t offset, size_t reclen, 
  size_t allocation);

/* Get information about direct access file */
int drct_info(struct drct_s_file *handle, struct drct_s_info *info);

/* PRIVATE, Read or write lock a number of records in the direct access file. */
int drct_lock(struct drct_s_file *handle, int locktype, off_t startrec, 
  size_t nrecs);

/* Open a direct access file */
struct drct_s_file *drct_open(char *fspec, int access, unsigned prot, 
  int rdtimeout, int wrtimeout);

/* Overwrite record to direct access file */
int drct_overwrite(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen);

/* Read lock records in direct access file */
int drct_rlock(struct drct_s_file *handle, off_t startrec, size_t nrecs);

/* Read specified record in file */
int drct_rdrec(struct drct_s_file *handle, off_t recnum,
  char *buffer, size_t buflen, size_t *reclen);

/* Sequential read of file */
int drct_rdseq(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t *reclen);

/* PRIVATE, reset routine for use after a file access error */
int drct_reset(struct drct_s_file *handle);

/* Rewind direct access file so that next sequential read will read the
   first record in the file */
void drct_rewind(struct drct_s_file *handle);

/* Rewrite record in direct access file */
int drct_rewrite(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen);

/* Set current record in file */
int drct_setpos(struct drct_s_file *handle, off_t recnum);

/* Scans direct access file to Get size and allocation info */
int drct_sizeinfo(struct drct_s_file *handle, size_t *numutilized, 
  size_t *utilizedsize, size_t *numallocated, size_t *allocatedsize,
  size_t *numfragment, size_t *fragmentsize);

/* Open a temporary direct access file */
struct drct_s_file *drct_tmpopen();

/* Set timeouts */
int drct_timeout(struct drct_s_file *handle, int rdtime, int wrtime);

/* PRIVATE, Trim allocation for current record */
int drct_trimrec(struct drct_s_file *handle);

/* Unlock direct access file */
int drct_unlock(struct drct_s_file *handle);

/* Write lock records in direct access file */
int drct_wlock(struct drct_s_file *handle, off_t startrec, size_t nrecs);

/* Write record to direct access file */
int drct_write(struct drct_s_file *handle, char *buffer, size_t buflen,
  size_t reclen);

#else

int drct_append();
void drct_cancelio();
void drct_close();
int drct_compress();
off_t drct_curpos();
int drct_delfile();
int drct_delrec();
int drct_findfree();
of_t drct_idxappnd();
int drct_info();
int drct_lock();
struct drct_s_file *drct_open();
int drct_overwrite();
int drct_rdrec();
int drct_rdseq();
int drct_reset();
void drct_rewind();
int drct_rewrite();
int drct_rlock();
int drct_setpos();
int drct_sizeinfo();
struct drct_s_file *drct_tmpopen();
int drct_timeout();
int drct_trimrec();
int drct_unlock();
int drct_wlock();
int drct_write();

#endif

#endif

#ifdef __cplusplus
};
#endif
