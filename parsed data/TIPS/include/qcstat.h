/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : qcstat.h

Function : Global definitions for query compiler routines.

Author : A. Fregly

Abstract :

Calling Sequence : #include <qcstat.h>

Notes :

Change log :
000	19-MAR-90  AMF	Created
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Parse states */
#define QC_OPERAND 1
#define QC_OPERATOR 2

struct qc_s_token {
  int type;
  int subtype;
  long nvalue;
  char *value;
  int len;
  int offset;
  struct qc_s_token *next;
};

struct qc_s_termentry {
  struct qc_s_token *token;
  struct qc_s_termentry *next;
};

struct qc_s_logicentry {
  struct qc_s_token *token;
  struct qc_s_logicentry *prev;
};

struct qc_s_objentry {
  struct qc_s_token *token;
  struct qc_s_objentry *next;
};

struct qc_s_error {
  int errortype;
  int idx;
  struct qc_s_error *next;
};

#if FIT_ANSI

int qc_dealobj(struct qc_s_cmpqry *(*cmpqry));
struct qc_s_token *qc_gettoken(char *(*srcqry), int *expected);
int qc_tokntype(char *token, int len, int *subtype, int offset);
int qc_addtoqry(struct qc_s_token *token);
int qc_makeobj(struct qc_s_cmpqry *(*cmpqry), int nfields,
  char *fieldnames[]);
int qc_inittemp(char *srcqry,int nreserved,char *reserved[],char *delims);
void qc_dealtemp(void);
void qc_adderror(int errtype, int offset);
int qc_addobj(struct qc_s_token *token);
int qc_validtok(int tokentype, int *expected);
void qc_tokenerr(int tokentype, int offset);
void qc_skipspce(char *(*srcptr));
int qc_tokenlen(char *srcptr);
int qc_semantic(struct qc_s_cmpqry *cmpqry, struct qc_s_objentry *qrylogic[]);
int qc_makelist(struct qc_s_objentry *qrylogic[], int sidx, int eidx,
  int nterms, struct qc_s_termlist *(*list));
int qc_proximty(struct qc_s_token *token, struct qc_s_termlist *list1,
  struct qc_s_termlist *list2, struct qc_s_cmpqry *cmpqry);
int qc_isvldc(struct qc_s_token *token, int *nderivations, int *vldcoffset);
struct qc_s_token *qc_newtoken(int tokentype, int subtype, unsigned long nval,
  char *val, int vlen, int offset);

#else /* K&R compatible function definitions */

int qc_dealobj();
struct qc_s_token *qc_gettoken();
int qc_tokntype();
int qc_addtoqry();
int qc_makeobj();
int qc_inittemp();
void qc_dealtemp();
void qc_adderror();
int qc_addobj();
int qc_validtok();
void qc_tokenerr();
void qc_skipspce();
int qc_tokenlen();
int qc_semantic();
int qc_makelist();
int qc_proximty();
int qc_isvldc();
struct qc_s_token *qc_newtoken();

#endif

/* Globals used by qc_ routines */

extern char *qc_qrystart;	/* Pointer at start of source query */
extern int qc_qryidx;		/* Offset of current token in source query */

extern int qc_nerrs;				   /* Error list */
extern struct qc_s_error *qc_firsterr, *qc_lasterr;

extern int qc_textsize;		/* Combined length of all terms */
extern int qc_nterms;
extern struct qc_s_termentry *qc_firstterm, *qc_lastterm; /* temp term list */
extern struct qc_s_token *qc_firsttoken, *qc_lasttoken; /* temp token list */

extern int qc_fieldsize;
extern int qc_nfields;

extern struct qc_s_logicentry *qc_toplogic; /* temp parse stack */

extern struct qc_s_objentry *qc_firstobj, *qc_lastobj;  /* temp object query */
extern int qc_nobjentries;

extern char *qc_reserved[QC_MAXRESERVED];
extern int qc_reserved_tokennum[QC_MAXRESERVED];
extern char qc_delims[QC_MAXDELIM];

#ifdef __cplusplus
};
#endif
