/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpaglbl.h

Function : Allocates storage for anpa program globals.

Author : A. Fregly

Abstract : This file contains declarations for anpa global variables
	such that they are allocated storage.

Calling Sequence : 

Notes : 

Change log : 
000	18-MAY-91  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif


char *anpa_fields[ANPA_NFIELDS] = ANPA_FIELDS;
int anpa_fieldnums[ANPA_NFIELDS];
char *(*anpa_fielddata);

int anpa_newdoc;
char anpa_outbuffer[ANPA_BUFFERSIZE+1];

#ifdef __cplusplus
};
#endif

/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : anpaglbl.h

Function : Allocates storage for anpa program globals.

Author : A. Fregly

Abstract : This file contains declarations for anpa global variables
	such that they are allocated storage.

Calling Sequence : 

Notes : 

Change log : 
000	18-MAY-91  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif


char *anpa_fields[ANPA_NFIELDS] = ANPA_FIELDS;
int anpa_fieldnums[ANPA_NFIELDS];
char *(*anpa_fielddata);

int anpa_newdoc;
char anpa_outbuffer[ANPA_BUFFERSIZE+1];

#ifdef __cplusplus
};
#endif

