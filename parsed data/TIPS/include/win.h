/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : win.h

Function : Curses support function library include file.

Author : A. Fregly

Abstract : This is the include file defining the SeekWare supplemental
	functions for use with the curses screen handling package.

Calling Sequence : #include <win.h>

Notes: 
000	fitsydef.h must be included before this file.
001	curses.h is included by this file.

Change log : 
000	25-AUG-91  AMF	Created.
001	17-DEC-92  AMF	Define getyx, getbegyx, getmaxyx if not defined by
			curses.
002	26-MAR-93  AMF	Fix to work with GNU C.
003	11-SEP-93  AMF	Fix getmaxyx and getbegyx macros. Add WIN_DEBUG.
******************************************************************************/

#ifndef WIN_INCLUDE
#define WIN_INCLUDE

#include <curses.h>

#define WIN_UPPERLEFT  ((unsigned char) 218)
#define WIN_UPPERRIGHT ((unsigned char) 191)
#define WIN_HORIZONTAL ((unsigned char) 196) 
#define WIN_VERTICAL   ((unsigned char) 179)
#define WIN_LOWERLEFT  ((unsigned char) 192)
#define WIN_LOWERRIGHT ((unsigned char) 217)

#ifndef WIN_DEBUG
#define WIN_DEBUG 0
#endif

/* Function definitions */
#if FIT_ANSI

/* Center text in specified row of specified window */
int win_center(WINDOW *win, int row, char *text);

/* Create a snapshot of a window */
int win_dup(WINDOW *srcwin, WINDOW *(*savewin));

/* Edit field within window */
int win_edit(WINDOW *win, char *inbuf, int bufl, int *imode, int dmode,
  int emode, int row, int col, int *off, int *key);

/* Fill a square region of a window */
int win_fill(WINDOW *win, int startx, int starty, int nrows, int ncols,
  int fill);

/* Check key to see if it is line edit exit key or function key */
int win_isexit(int key);

/* Find last non-blank, non-null in buffer */
void win_lastch(char *buf, int *e);

/* Box a window */
#if !FIT_COHERENT
#define win_box(win) ((int) box(win, (chtype) 0, (chtype) 0))
#else
int win_box(WINDOW *win);
#endif

#else
int win_center();
int win_dup();
int win_edit();
int win_fill();
int win_isexit();
void win_lastch();
#if !FIT_COHERENT
#define win_box(win) box(win, (chtype) 0, (chtype) 0)
#else
int win_box();
#endif
#endif

#ifndef getmaxyx
#define getmaxyx(win, y, x) (1 | (y = (unsigned) win->_maxy + 1) | \
  (x = (unsigned) win->_maxx + 1))
#endif

#ifndef getyx
#define getyx(win, y, x) (1 | (y = (unsigned) win->_cury) | \
  (x = (unsigned) win->curx))
#endif

#ifndef getbegyx
#define getbegyx(win, y, x) (1 | (y = (unsigned) win->_begy) | \
  (x = (unsigned) win->_begx))
#endif

#endif
