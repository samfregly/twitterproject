/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : sphelp.h

Function : Header file for sphelp program.

Author : A. Fregly

Abstract : This is the program specific header file for the sphelp program.
	Note that sphelp share many of the spot functions, so this header
	is used only to define the sphelp specific functions.

Calling Sequence : #include <sphelp.h>

Notes: 

Change log : 
000	12-NOV-91  AMF	Created.
001	11-SEP-93  AMF	Fixed declaration of sphelp_browse.
******************************************************************************/

#ifndef SPHELP_DEBUG
#define SPHELP_DEBUG 0
#endif

#if FIT_ANSI

int sphelp_makedisplay(struct spot_s_env *env, struct drct_s_file *help);

int sphelp_browse(struct spot_s_env *env, struct drct_s_file *help, 
  long startdocnum, long *retdocnum);

#else
int sphelp_makedisplay();
int sphelp_browse();
#endif
