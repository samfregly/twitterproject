/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : abrowse.h

Function : Program specific include file for TIPS ABROWSE program.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <abrowse.h>

Notes : 

Change log :
000	1990?  		AMF	Created.
001	27-MAR-91       AMF	K&R transportable version.
******************************************************************************/

#define LINELEN 80

#if FIT_I32
#define BUFMAX 132000
#define MAXLINE 16384
#endif

#ifndef BUFMAX
#define BUFMAX 16384
#endif

#ifndef MAXLINE
#define MAXLINE 1000
#endif

struct browse_s_hit {
  int idx;
  struct browse_s_hit *next;
  struct browse_s_hit *prev;
  int len;
  char *name;
};

struct browse_s_query {
  char *name;
  struct browse_s_hit *firsthit, *lasthit;
  struct browse_s_query *next;
};

#if FIT_ANSI

void abrowse_draw(int srow, int erow, unsigned char *docbuf,
  unsigned int *lineoffset, int line, int nlines, int highlight,
  int noffsets, unsigned long *offsets, unsigned char *lengths);

int abrowse_view(int srow, int erow, struct browse_s_query *query);

void abrowse_viewinit(char *fname, int fieldmark, int bufmax, int *nbytes,
    unsigned char *docbuf,int maxline, int *nlines, unsigned int *lineoffsets,
    int highlight, char *exe, int maxoffset, int *noffsets,
    unsigned long *offsets, unsigned char *lengths);

void abrowse_viewhdr(int row, char *name);

void abrowse_viewfoot(int row, char *name,int curhit, int nhits);

void abrowse_dumphits(FILE *out, struct browse_s_query *(*querylist),
  int nqueries);

#else /* K&R compatible prototypes */

void abrowse_draw();
int abrowse_view();
void abrowse_viewinit();
void abrowse_viewhdr();
void abrowse_viewfoot();
void abrowse_dumphits();

#endif
/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : abrowse.h

Function : Program specific include file for TIPS ABROWSE program.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <abrowse.h>

Notes : 

Change log :
000	1990?  		AMF	Created.
001	27-MAR-91       AMF	K&R transportable version.
******************************************************************************/

#define LINELEN 80

#if FIT_I32
#define BUFMAX 132000
#define MAXLINE 16384
#endif

#ifndef BUFMAX
#define BUFMAX 16384
#endif

#ifndef MAXLINE
#define MAXLINE 1000
#endif

struct browse_s_hit {
  int idx;
  struct browse_s_hit *next;
  struct browse_s_hit *prev;
  int len;
  char *name;
};

struct browse_s_query {
  char *name;
  struct browse_s_hit *firsthit, *lasthit;
  struct browse_s_query *next;
};

#if FIT_ANSI

void abrowse_draw(int srow, int erow, unsigned char *docbuf,
  unsigned int *lineoffset, int line, int nlines, int highlight,
  int noffsets, unsigned long *offsets, unsigned char *lengths);

int abrowse_view(int srow, int erow, struct browse_s_query *query);

void abrowse_viewinit(char *fname, int fieldmark, int bufmax, int *nbytes,
    unsigned char *docbuf,int maxline, int *nlines, unsigned int *lineoffsets,
    int highlight, char *exe, int maxoffset, int *noffsets,
    unsigned long *offsets, unsigned char *lengths);

void abrowse_viewhdr(int row, char *name);

void abrowse_viewfoot(int row, char *name,int curhit, int nhits);

void abrowse_dumphits(FILE *out, struct browse_s_query *(*querylist),
  int nqueries);

#else /* K&R compatible prototypes */

void abrowse_draw();
int abrowse_view();
void abrowse_viewinit();
void abrowse_viewhdr();
void abrowse_viewfoot();
void abrowse_dumphits();

#endif
