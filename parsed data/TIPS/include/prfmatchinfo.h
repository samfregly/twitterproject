/* Searches text with search executable containing multiple queries and
returns match information */
int prf_search_matchinfo(int searchtate, unsigned int opts, char *exe,
  unsigned char *text, unsigned int textlen, char *(*matchinfo));

/*  paramters:
        searchstate		see prf_search for description.
        opts            see prf_search for description.
        exe             see prf_search for description.
        text            see prf_search for description.
        textlen         see prf_search for description.
        matchinfo       Returned pointer at match information. The
                        prf_matchinfo_* functions are used to extract desired
                        elements of match information from the match
                        information. 
                                        
                        *matchinfo should be NULL on the initial call to this 
                        function. *matchinfo is filled in at completion of 
                        searching of each document as indicated by the value of 
                        the "searchstate" parameter. This function will 
                        allocate/reallocate *matchinfo as necessary when being 
                        called repetitively to search a sequence of documents. 
                        The function prf_matchinfo_deallocate may be called to 
                        explicitly deallocate *matchinfo once searching is
                        completed.

        returns:
                        0 indicates an error of some sort.
*/

/**************************************************************************/

/* Returns number of queries described within matchinfo */
int prf_matchinfo_nqueries(char *matchinfo);

/*  parameters:
        matchinfo               matchinfo returned by a prior call to prf_search_matchinfo.
        returns:
                                        number of matching queries defined within matchinfo.
*/

/**************************************************************************/

/* Returns ID's of queries described in matchingo. prf_qname may be used to
retrieve the query name based on a query ID. Other prf_matchinfo_* functions
are used to get more detailed match information for specific query IDs.
*/

int prf_matchinfo_qrymatches(char *matchinfo, int *nmatched, int
*(*qryIDs));
/* parameters:
        matchinfo	matchinfo returned by call to prf_search_matchinfo.
        nmatched    Returned number of queries that matched.
        qryIDs      An array of query IDs for the matching queries. *qryIDs is
                    allocated by this function. If *qryIDs is not NULL at entry,
                    it is deallocated by this function at entry. The "cfree" 
                    function may be used to deallocate *qryIDs.
        returns:
                    0 indicates an error occurred.
*/


/**************************************************************************/


/* Returns information needed for highlighting words that matched the
query or queries use in a search by prf_search_matchinfo. */

int prf_matchinfo_highlightinfo(char *matchinfo, unsigned *nmatches, 
  unsigned long *(*offsets), unsigned char *(*lengths));

/*  parameters:
        matchinfo   matchinfo returned by call to prf_search_matchinfo.
        nmatches    Number of offset/length pairs being returned.
        offsets     An array of offsets into the document for strings that
                    matched. *offsets is allocated by this function. If 
                    *offsets is not NULL at entry, it is deallocated by this 
                    function at entry. The "cfree" function may be used to 
                    deallocate *offsets.
        lengths     An array of lengths of matching strings within the document.
                    offset/length pairs of form offset[i], length[i] identify
                    the matching strings and their length. *lengths is
                    allocated by this function. If *lengths is not NULL at
                    entry, it is deallocated by this function at entry. The
                    "cfree" function may be used to deallocate *offsets.

        returns:
					0 indicates an error occurred.
*/

/**************************************************************************/

/* Returns match information for a particular matching query */
char *prf_matchinfo_getqrymatchinfo(char *matchinfo, int qryID, 
			int	*nstringsmatched, int *nmatchableterms, int *ntermsmatched);

/*  parameters:
        matchinfo		matchinfo returned by call to prf_search_matchinfo.
        qryID			query ID for query whose match information is to be 
						returned. prf_matchinfo_qrymatches is used to get the 
						query IDs for queries that matched a document.
        nstrngsmatched  Returned number of strings within the document that were 
						matched.
        nmatchableterms Returned total number of terms in query that could have
                        have matched against the document.
    ntermsmatched       Returned number of terms in query that matched against
						the document. Information about each matching turn may be 
                        accessed with the prf_matchinfo_gettermmatches function.

        returns:
						0 indicates an error occurred.
*/

/**************************************************************************/

/* Returns match information for a query term that matched against a
document */
char *prf_matchinfo_gettermmatches(char *matchinfo, int qryID, 
		int termidx, int *nmatches, int *termID, int *(*fldmatched), 
		int *(*offsets), int *(*lengths));

/*  parameters:
        matchinfo	matchinfo returned by call to prf_search_matchinfo.
        qryID       query ID for query whose match information is to be returned.
                    prf_matchinfo_qrymatches is used to get the query IDs for
                    queries that matched a document.
        termidx     Index for term that matched for which information is to
                    be returned. termidx may have a value ranging from 0 to
                    ntermsmatched as returned by a call to
                    prf_matchinfo_getqrymatchinfo.
        nmatches    Returned number of strings that the term matched against 
                    within the document.
        termID      Returned Term ID for the term. The prf_gettermvalue
                    function may be used to return the search string that
                    is the value of the term.
        fldmatched  Returned array of field numbers. There is one entry in this
                    array for each string matched by the term. *fldmatched is
                    allocated by this function. If *fldmatched is not NULL at
                    entry, it is deallocated at entry. *fldmatched may be
                    deallocated with the cfree function.
        offsets     Returned array of matching string offsets. There is one entry 
                    in this array for each string matched by the term. *offsets is
                    allocated by this function. If *offsets is not NULL at
                    entry, it is deallocated at entry. *offsets may be
                    deallocated with the cfree function.
        lengths     Returned array of matching string lengths. There is one entry 
                    in this array for each string matched by the term. *lengths is
                    allocated by this function. If *lengths is not NULL at
                    entry, it is deallocated at entry. *lengths may be
                    deallocated with the cfree function.

  note:				fldmatched, offsets, and lengths form a set of triplets of form
					fldmatched[i], offsets[i], lengths[i] that define a matching
				    string.


        returns:
					0 indicates an error occurred.
*/

/**************************************************************************/


/* Returns character string value of term specified with termID */
char *prf_gettermvalue(char *exe, int termID);


/*  parameters:
        exe         Profile executable.
        termID      Term ID for which string is to be returned. The function
                    prf_matchinfo_gettermmatches returns termIDs to identify
                    the term within a query that matched against a document.

        returns:
                    Character string that is the value of the specified term.
                    This character string should never be deallocated by the
                    calling function as it is a pointer at a string stored
                    within the profile executable.
*/

/**************************************************************************/



/*	Deallocates storage utilized by matchinfo that was created with a 
	call to prf_search_matchinfo. This function only needs to be called at
	the completion of searching and/or when it is desired to recover the
	memory utilized by matchinfo. */
int prf_matchinfo_deallocate(char *(*matchinfo));

/*  parameters:
        matchinfo	matchinfo returned by a prior call to prf_search_matchinfo.
        returns:
                    0 indicates an error of some sort occurred.
*/