/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : findstat.h

Function : Defs needed for fit_findfile processing.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <findstat.h>

Notes :

Change log :
000	1990?  AMF	Created.
001	13-JUL-92  AMF	Support for UNIX.
002	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef FIT_FINDSTAT_DEF
#define FIT_FINDSTAT_DEF 1

/* Need to define UNIX version of FIND_CONTEXT. */
#if FIT_ZORTECH
#include <dos.h>
typedef struct FIND *FIND_CONTEXT;
#endif

#if FIT_TURBOC
#include <dir.h>
typedef struct ffblk FIND_CONTEXT;
#endif

#if FIT_MSC
#include <io.h>
typedef struct _finddata_t FIND_CONTEXT; 
#endif

#if FIT_VMS
typedef long FIND_CONTEXT;
#endif

#if FIT_UNIX || FIT_COHERENT
typedef int FIND_CONTEXT;
#endif

struct fit_findstat {
  char *searchlist;
  char *remaininglist;
  char curfspec[FIT_FULLFSPECLEN+1];
  char curdefault[FIT_FULLFSPECLEN+1];
  char *(*filelist);
  int nfiles;
  int idx;
  FIND_CONTEXT context;
};

#endif

#ifdef __cplusplus
};
#endif
