/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :	qcalloc.h

Function : Allocates global storage used by TIPS query compiler.

Author : A. Fregly

Abstract : This file is included by the top level routine of the TIPS query
  compiler to allocate space for the global variables used within the
  compiler.

Calling Sequence : #include <qcalloc.h>

Notes : 

Change log :
000	1990?  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Globals used by qc_ routines */

char *qc_qrystart;	/* Pointer at start of source query */
int qc_qryidx;		/* Offset of current token in source query */

int qc_nerrs;				   /* Error list */
struct qc_s_error *qc_firsterr, *qc_lasterr;

int qc_textsize;		/* Combined length of all terms */
int qc_nterms;
struct qc_s_termentry *qc_firstterm, *qc_lastterm; /* temp term list */
struct qc_s_token *qc_firsttoken, *qc_lasttoken; /* temp token list */

int qc_fieldsize;
int qc_nfields;

struct qc_s_logicentry *qc_botlogic, *qc_toplogic; /* temp parse stack */

struct qc_s_objentry *qc_firstobj, *qc_lastobj;  /* temp object query */
int qc_nobjentries;

char *qc_reserved[QC_MAXRESERVED];

int qc_reserved_tokennum[QC_MAXRESERVED] = {QC_OR, QC_AND, QC_COMMA, 
  QC_LPAREN, QC_RPAREN, QC_FIELD, QC_THRESHHOLD, QC_THRESHHOLD, QC_NOT, 
  QC_PROXIMITY, QC_PROXIMITY, QC_EOQ};

char qc_delims[QC_MAXDELIM];

#ifdef __cplusplus
};
#endif