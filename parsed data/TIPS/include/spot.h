/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : spot.h

Function : Defines spot globals, data structures, functions.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <spot.h>

Notes: 
001	stdio.h and fitsydef.h need to be include before this file.

Change log : 
000	03-SEP-91  AMF	Created.
001	11-SEP-93  AMF	Used unsigned char for some char structure elements.
			Use SPOT_DEBUG.
******************************************************************************/

#include <men.h>
#include <win.h>
#include <qc.h>
#include <prf.h>
#include <drct.h>
#include <tips.h>

#define SPOT_DEBUG 0

/* Length of label area of command menu line (not including separator) */
#define SPOT_LABELWINLEN 19

/* Define constants for current view mode */
#define SPOT_VIEWFILES 0
#define SPOT_VIEWSTREAMS 1
#define SPOT_VIEWARCHIVES 2
#define SPOT_VIEWRESULTS 3
#define SPOT_VIEWQUERIES 4
#define SPOT_VIEWSTREAMDETAIL 5
#define SPOT_VIEWDETAIL 6
#define SPOT_VIEWLINE 7
#define SPOT_VIEWFIELD 8
#define SPOT_MAXVIEW (SPOT_VIEWFIELD + 1)

/* Define constants for answers to prompts */
/* ... for command source file prompt */
#define SPOT_CANCEL 0
#define SPOT_CURRENT 1
#define SPOT_SELECTED 2

/* Define constants for some of the menu options */
/* Define Main File menu options */
#define OPEN 0
#define NEW 1
#define BROWSE 2
#define PICK 3
#define COPY 4
#define DELETE 5
#define PRINT 6
#define PATH 7
#define SHELL 8
#define EXIT 9

/* Define source selection menu options */
#define CURRENT 0
#define SELECTED 1

/* Define copy destination menu options */
#define FILEDEST 0
#define RESULTS 1
#define ARCHIVES 2
#define DIRECTORY 3
#define STREAM 4

/* Define summary menu types */
#define NAME 0
#define LINE 1
#define FIELD 2

/* Size of document buffer */
#if FIT_UNIX
#define SPOT_DOCBUFMAX 262144
#else
#define SPOT_DOCBUFMAX 4096
#endif

/* Maximum number of words or phrases to be highlighted */
#if FIT_UNIX
#define SPOT_MAXHIGH 65536
#else
#define SPOT_MAXHIGH 1024
#endif

#define SPOT_MAXFLDNAMELEN 16

/* Define structure for browse display */
struct spot_s_doc {
  char source[FIT_FULLFSPECLEN+1];		/* Name of summary source */
  struct drct_s_file *handle;		/* Handle for hit file or archive */
  struct drct_s_file *db;		/* Handle for referenced archive */
  int filetype;				/* File type indicator */
  int margin;				/* Left Margin */
  int wrapcol;				/* Wrap column */
  long ndocs;				/* Number of documents in file */
  long docnum;				/* Current document number */
  char docname[TIPS_DESCRIPTORMAX+1];	/* Current document name */
  char origdocname[FIT_FULLFSPECLEN+1];	/* Original Document name */
  char editor[FIT_FULLFSPECLEN+1];		/* Editor for current document */
  char keyfld[TIPS_DESCRIPTORMAX+1];	/* Key field from last hit result */
  char prevdbname[FIT_FULLFSPECLEN+1];	/* Referenced archive name */
  FILE *temp;				/* Handle for temp file */
  long doclen;				/* Number of bytes in document */
  int buflen;				/* Size of document buffer */
  int nlines;				/* Number of lines in document */
  int nhigh;				/* Number of items to highlight */
  int startline;			/* Start line in document buffer */
  int topline;				/* Top line in display */
  char *qryexe;				/* Query exe for highlighting */
  unsigned char *docbuf;		/* Document buffer */
  long *lineoffsets;			/* Offsets of lines in document */
  unsigned long *highoffsets;		/* Offset of highlight items */
  unsigned *highlengths;		/* Lengths of highlight items */
  char prevfieldfile[FIT_FULLFSPECLEN+1];   /* Last loaded field file */
  char *(*fieldnames);			/* Field names from last load */
  char *(*fielddata);			/* Field data from last load */
  int nfields;				/* Number of fields loaded */
};

/* Define structure for spot environment */
struct spot_s_env {
  int view;				/* Current view mode */
  int usepointer;			/* Use pointer to highlight cur opt */
  int usecheckmarks;			/* Use check for selected opts */
  int usecolor;				/* Use color in display */
  int usehotkeys;			/* Enable hot keys in menus */
  int uselinedrawing;			/* Use line drawing set */
  int updateinterval;			/* Number of secs for disp updates */
  char filedir[FIT_FULLFSPECLEN+1];     /* File directory */
  char arcdir[FIT_FULLFSPECLEN+1];		/* Archive directory */
  char streamdir[FIT_FULLFSPECLEN+1];	/* Streams directory */
  char searchdir[FIT_FULLFSPECLEN+1];	/* Query/Result directory */
  char streamname[FIT_FULLFSPECLEN+1];	/* Stream being accessed for view
					   mode of SPOT_VIEWSTREAMDETAIL */
  char printer[FIT_FULLFSPECLEN+1];		/* Printer */
  int pagelen;				/* Printer page length */
  int wrap;				/* Printer wrap column */
  int topmargin;			/* Printer top margin */
  int leftmargin;			/* Printer left margin */
  int separator;			/* Document separator for prints */
  int dispnorm;				/* Display normal attribute */
  int disphigh;				/* Display highlight attribute */
  int dispsel;				/* Display "selected" attribute */
  int disphot;				/* Display hot key attribute */
  int mennorm;				/* Menu normal attribute */
  int menhigh;				/* Menu highlight attribute */
  int mensel;				/* Menu "selected" attribute */
  int menhot;				/* Menu hot key attribute */
  int popupnorm;			/* Popup normal attribute */
  int popuphigh;			/* Popup highlight attribute */
  int popupsel;				/* Popup "selected" attribute */
  int popuphot;				/* Popup hot key attribute */
  struct spot_s_doc doc;		/* Document descriptor for 							   environment */
  struct men_s_menu *menu;		/* Document menu for environment */
  int prevview;				/* Previous view */
  char previtem[TIPS_DESCRIPTORMAX+1]; /* Descriptor for current file from
					  last view prior to summary */
  char *(*prevsel);			/* Selected files from last view
					   prior to summary */
  char summaryitem[SPOT_MAXFLDNAMELEN + 1]; /* Summary item specifier */
};

extern struct spot_s_env spot_env;

/* Define structure for defining function of action menus */
/* First define function options masks. These masks specify how the arguements
   are to be passed to the action command. By default, an action command
   is executed once for each parameter, with the paramter supplied as the
   first and only argument to the command. */

/* No arguments, the action command is executed once without arguments. */
#define ACTION_MASK_NOPARAMS 1

/* The arguments are piped to the action command. The action command is
   executed once, and the parameters are piped to it's standard input, one
   per line. */
#define ACTION_MASK_PIPEPARAMS 2

/* The parameters are supplied as a list of arguments to the action command,
   which is executed once */ 
#define ACTION_MASK_PARAMLIST 4

/* A maximum of one argument may be supplied */
#define ACTION_MASK_ONEPARAM 8

/* Define the structure */
struct spot_s_actiondescr {
  int nopts;
  int *opts;
  char *(*cmds);
  char *(*menuitems);
  int menuwidth;
  WINDOW *win;
  struct men_s_menu *menu;
};

/* Display size parameters (for functions not using men.h) */
extern int spot_dispwidth, spot_dispheight, spot_hardscroll;

/* Define windows used within spot */
/* Main screen */
extern WINDOW *spot_labelwin, *spot_cmdwin, *spot_dispwin, *spot_footerwin;
extern WINDOW *spot_promptwin, *spot_selectwin, *spot_copywin;
extern WINDOW *spot_filewin, *spot_viewwin, *spot_searchwin;
extern WINDOW *spot_detailwin, *spot_addwin, *spot_strcopywin;
extern WINDOW *spot_sumdelwin;

/* Browse screen */
extern WINDOW *spot_brfilewin, *spot_brviewwin, *spot_brcopywin;
extern WINDOW  *spot_brviewtypewin, *spot_brsummaryoptwin, *spot_brdeloptwin;

/* Miscellaneous windows */
extern WINDOW *spot_errwin, *spot_msgwin, *spot_srcwin;
extern WINDOW *spot_errwinsave, *spot_msgwinsave, *spot_promptwinsave;
extern WINDOW *spot_sumsrcwin;

/* Define menus used within spot */
/* Main menu and it's sub-menus */
extern struct men_s_menu *spot_dispmenu, *spot_cmdmenu, *spot_selectmenu;
extern struct men_s_menu *spot_filemenu, *spot_viewmenu, *spot_searchmenu;
extern struct men_s_menu *spot_detailmenu, *spot_addmenu;
extern struct men_s_menu *spot_copymenu, *spot_strcopymenu;
extern struct men_s_menu *spot_sumsrcmenu, *spot_sumdelmenu;

/* Declare action descriptors */
extern struct spot_s_actiondescr spot_actions[SPOT_MAXVIEW+1];

/* Browse menus */
extern struct men_s_menu *spot_brfilemenu, *spot_brviewmenu;
extern struct men_s_menu *spot_brcopymenu, *spot_brviewtypemenu;
extern struct men_s_menu *spot_brsummaryoptmenu, *spot_brdeloptmenu;

extern struct men_s_menu *spot_srcmenu;

/* Current directory path */
extern char spot_curpath[FIT_FULLFSPECLEN+1];

/* Copy destinations for file copies */
extern char spot_filedest[FIT_FULLFSPECLEN+1], spot_arcdest[FIT_FULLFSPECLEN+1];
extern char spot_searchdest[FIT_FULLFSPECLEN+1], spot_streamdest[FIT_FULLFSPECLEN+1];
extern char spot_streamdestname[FIT_FULLFSPECLEN+1];
extern char spot_filefilter[FIT_FULLFSPECLEN+1];

/* Special Function keys */
extern int spot_menukey, spot_exitkey, spot_quitkey, spot_helpkey;
extern char *spot_menukeyname, *spot_exitkeyname, *spot_quitkeyname;
extern char *spot_helpkeyname;

/* Help line for prompt line */
extern char spot_disphelp[128], spot_cmdhelp[128], spot_edithelp[128];
extern char spot_selectorhelp[128], spot_multselectorhelp[128];
extern char spot_brdisphelp[128], spot_fieldedithelp[128];
extern char spot_selectorcmdhelp[128], spot_multmenuhelp[128];

/* Editors and file types to which they apply */
extern int spot_neditor;
extern char *(*spot_editor), *(*spot_editortype);
extern char spot_defaulteditor[FIT_FULLFSPECLEN+1];

/* Print command and file types to which they apply */
extern int spot_nprintcmd;
extern char *(*spot_printcmd), *(*spot_printtype);
extern char spot_defaultprintcmd[FIT_FULLFSPECLEN+1];

/* Mailer program */
extern char spot_mailer[FIT_FULLFSPECLEN+1];

/* Program name used for spot on command line */
extern char *spot_progname;
extern char spot_shell[FIT_FULLFSPECLEN+1];
extern char spot_defaultfields[FIT_FULLFSPECLEN+1];
extern int spot_defaultdelim;

#define spot_fspec(opt, viewtype) (viewtype < SPOT_VIEWDETAIL) ? \
  (opt)->data : (char *) ((opt)->data) + FIT_BYTES_IN_LONG

/* Functions */
#if FIT_ANSI

/* Actions menu off main menu */
void spot_action(struct spot_s_env *env, int option);

/* Adds an item to a summary menu */
int spot_addtosummary(struct men_s_menu *menu, char *option, int optlen, 
  long docnum, char *docname);

/* Execute Action option off main menu */
void spot_aoption(struct spot_s_env *env, struct men_s_menu *menu, int opt);

/* Execute Action option off browse menu */
void spot_braoption(struct spot_s_env *env, struct spot_s_doc *doc, 
  struct men_s_menu *menu, int opt);

/* Process pulldowns off browse command menu */
int spot_brcmd(struct spot_s_env *env, struct spot_s_doc *doc, int option, 
  int optselected, int *menudown, int *exitflag);

/* Draw document display and footer line */
void spot_brdraw(struct spot_s_env *env, struct spot_s_doc *doc, int line); 

/* Display/update browse footer */
void spot_brfooter(char *docname, long docnum, long maxdoc, int topline, 
  int nlines);

/* Make browse menus */
int spot_brmakedisp(struct spot_s_env *env);

/* Browse command off from main menu */
void spot_browse(struct spot_s_env *env, struct spot_s_doc *doc, long curopt);

/* Initialize document display for relative or absolute document number */
int spot_brviewinit(struct spot_s_env *env, struct spot_s_doc *doc, 
  long move, int relative);

/* Copy a file */
int spot_copyfile(struct spot_s_env *env, char *srcspec, char *destspec,
  int querycopy, int replace);

/* Return default directory for specified view mode */
char *spot_curdefault(struct spot_s_env *env, int viewmode);

/* Return file extension for specified view mode */
char *spot_curext(int viewmode);

/* Update text of file display entries when viewing results, archives, or
   streams */
int spot_dispupdate(struct spot_s_env *env, char *dispitem, int dispidx);

/* Return copy destination directory for current view mode */
char *spot_destination(struct spot_s_env *env);

/* Execute action menu option for selected files */
int spot_doaction(struct spot_s_env *env, struct spot_s_actiondescr *action, 
  int menuopt, char *(*itemlist));

/* Deallocate data allocated for document structure */
int spot_docdeall(struct spot_s_doc *doc);

/* Initialize document structure for summary display */
int spot_docinit(struct spot_s_env *env, struct spot_s_doc *doc,
  int useselected);

/* Returns pointer at buffer containing document text at specified line */
char *spot_docline(struct spot_s_doc *doc, int line);

/* Performs copy operation, first prompting for destination */
int spot_docopy(struct spot_s_env *env, int copyopt, int useselected);

/* Create line offsets and highlighting descriptors for document */
int spot_docparse(struct spot_s_doc *doc);

/* Performs item delete operation from main file menu */
int spot_dodelete(struct spot_s_env *env, int useselected, int delqrys,
  int delhits);

/* Inovokes print for current or selected options */
int spot_doprint(struct spot_s_env *env, int useselected);

/* Invokes appropriate "editor" to edit/update specified file */
void spot_edit(char *docname, char *editor, int *updated);

/* Get a sequence of environment variables */
void spot_envseq(char *root, char *defapp, char *(*(*applist)),
  char *(*(*ftype)), int *n);

/* Display message, and most recent system error message, and prompt */
void spot_errmessage(char *message);

/* Prompts for field to use in field summary display */
int spot_fldprompt(struct spot_s_env *env, struct men_s_menu *parent,
  char *retfield);

/* Make sure appropriate field definitions are loaded for document */
int spot_fieldload(struct spot_s_env *env, char *prevfieldfile, int isfile,
  int viewtype, char *arcname, char *(*(*fieldnames)), char *(*(*fielddata)),
  int *nfields, char *newfieldfile);

/* Process command off main file menu */
void spot_filecmd(struct spot_s_env *env, int option);

/* Finds document for browser and summary display builder */
int spot_finddoc(struct spot_s_env *env, struct spot_s_doc *doc, long recnum, 
  int relative, int dosearch, int loadfields,  int doopen, FILE *(*in), 
  struct drct_s_file *(*arc), long *doclen, long *modtime);

/* Create file list menu */
struct men_s_menu *spot_fmenu(struct spot_s_env *env, int viewtype,
  WINDOW *dispwin, int menuattr, char *filelist[], char *summaryitem);

/* Formats output line for main display */
void spot_fmtdisp(int viewtype, char *expfspec, char *dispval);

/* Pull out file name portion of file display menu entry */
void spot_fname(char *optval, char *fname);

/* Execute File option off from main menu */
void spot_foption(struct spot_s_env *env, struct men_s_menu *menu, int opt,
  int *exitflag);

/* Prompts user to enter file name */
int spot_fprompt(struct spot_s_env *env, int opts, char *promptstr, 
  char *defname, char *defext, char *defpath, char *retfspec, char *retpath,
  int *ftype);

/* Allow user to use file selector */
void spot_fselect(struct spot_s_env *env, int multflag, int newflag,
  char *path, char *extension, char *(*(*retlist)), char *retpath);

/* Return application/editor to use for specified file name */
char *spot_getapplication(char *fspec);

/* Find document for result specifier and build temp doc and descriptor */
int spot_getdoc(struct spot_s_env *env, struct spot_s_doc *doc, long docid, 
  int format, int relative);

/* Get environment */
int spot_getenv(struct spot_s_env *env);

/* Loads buffer starting at specified line in document which was found using
   spot_finddoc */
void spot_getfield(struct spot_s_doc *doc, long doclen, char *buf, int buflen,
  char *field, FILE *in, struct drct_s_file *arc, int *retlen);

/* Get next line from input buffer, formatting for output */
int spot_getline(char *docbuf, int *bufoff, int buflen, int margin, int wrapcol,
  int fielddelim, char *outline, int *outlen);

/* Get option from pulldown menu */
int spot_getpulldown(struct men_s_menu *menu, int *retopt, int *exitkey);

/* Invokes help function */
void spot_help();

/* Append menu item to result file */
int spot_hitappend(struct men_s_menu *menu, long idx, int viewtype, 
  struct drct_s_file *handle);

/* Fill in screen label */
void spot_label(char *label);

/* Loads buffer starting at specified line in document which was found using
   spot_finddoc */
void spot_loadlines(struct spot_s_doc *doc, long doclen, char *buf, int buflen, 
  long startline, FILE *in, struct drct_s_file *arc, int *retlen);

/* Process option off main menu */
int spot_maincmd(struct spot_s_env *env, int option, int optselected,
  int immediate, int *subopt, int *menudown, int *exitflag);

/* Make windows and menus used by spot */
int spot_makedisplay(struct spot_s_env *env);

/* Display message and prompt user to continue */
int spot_message(char *message);

/* Initialize newly created document structure */
int spot_newdoc(struct spot_s_doc *doc);

/* Performs file open/new off main file menu */
int spot_open(struct spot_s_env *env, int optidx, int newflag);

/* Prompts for user input on bottom line of display */
int spot_prompt(char *promptstr, char *promptresp, int promptresp_l);

/* Stop profiling process for specified stream */
void spot_pstop(char *srcspec);

/* Update profiling process for specified stream so that it searches
   with latest contents of query library */
void spot_pupdate(char *srcspec);

/* Write centered help line in prompt line */
void spot_puthelpline(char *text);

/* Rebuild file display window */
void spot_rebuilddisp(struct spot_s_env *env);

/* Update real-time displays */
void spot_refresh(struct spot_s_env *env, struct men_s_menu *disp);

/* Print a document */
int spot_print(struct spot_s_env *env, char *docdescriptor);

/* Initialize screen and keyboard handling */
int spot_scrinit();

/* Restore contents of previously saved screen */
void spot_scrrestore(unsigned selmask, WINDOW *cmdwin, WINDOW *labelwin, 
  WINDOW *dispwin, WINDOW *footerwin, WINDOW *promptwin);

/* Save contents of current screen */
void spot_srcsave(WINDOW *(*cmdwin), WINDOW *(*labelwin), WINDOW *(*dispwin),
  WINDOW *(*footerwin), WINDOW *(*promptwin));

/* Process search menu command off main menu */
int spot_search(struct spot_s_env *env, struct spot_s_doc *doc, int option, 
    int prompt, int append, char *(*searchspec), int nsearchspec);

/* Set field file to use for search */
int spot_setfieldfile(char *defaultfields, char *archive, char *querylib, 
  char *fieldfile);

/* Execute Search option off main menu */
void spot_soption(struct spot_s_env *env, struct men_s_menu *menu, int opt);

/* Change directory gadget */
int spot_setpath(struct spot_s_env *env, char *inpath, char *outpath);

/* Prompt user for file source for command (current, selected, quit) */
int spot_srcprompt();

/* Stream display handler */
void spot_stream(struct spot_s_env *env, char *streamname);

/* Submit queries for profiling against stream */
int spot_submit(char *filelist[], char *defpath, char *libname, int reload, 
  int replace);

int spot_sumcmd(struct spot_s_env *env, struct spot_s_doc *doc, 
  struct men_s_menu *summarymenu, long cmdopt, int optselected, int immediate, 
  long *subopt, int *menudown, int *exitflag);

int spot_sumupdate(struct spot_s_env *env, struct spot_s_doc *doc, 
  struct men_s_menu *summarymenu, int summarytype, char *summaryitem);

/* Swap two file names */
void spot_swap(char *fname1, char *fname2);

/* Signal handling routine for timer alarm signals */
void spot_timeralarm(int sig);

/* Draw initial display for a view mode */
int spot_viewinit(struct spot_s_env *env, int viewtype, long summaryidx,
  char *summaryitem);

/* Execute View option off main menu */
void spot_voption(struct spot_s_env *env, struct men_s_menu *menu, int opt);

/* Create list of entries from either current or selected items in main 
   display */
char *(*spot_worklist(struct spot_s_env *env, int useselected, int curopt));

/* Prompts user to answer a y/n question */
int spot_ynprompt(struct spot_s_env *env, char *promptstr);

#else

void spot_action();
int spot_addtosummary();
void spot_aoption();
void spot_browse();
char *spot_curdefault();
char *spot_curext();
char *spot_destination();
int spot_doaction();
int spot_docdeall();
int spot_docinit();
char *spot_docline();
int spot_docopy();
int spot_dodelete();
int spot_doprint();
void spot_envseq();
void spot_errmessage();
int spot_fieldload();
void spot_filecmd();
int spot_finddoc();
int spot_fldprompt();
struct men_s_menu *spot_fmenu();
void spot_fmtdisp();
void spot_foption();
int spot_fprompt();
int spot_getenv();
void spot_getfield();
int spot_getpulldown();
void spot_help();
int spot_hitappend();
void spot_label();
void spot_loadlines();
int spot_maincmd();
int spot_makedisplay();
int spot_message();
int spot_newdoc();
int spot_open();
int spot_prompt();
void spot_results();
int spot_scrinit();
int spot_search();
int spot_setfieldfile();
void spot_soption();
void spot_streams();
int spot_sumcmd();
int spot_sumupdate();
int spot_viewinit();
void spot_voption();
void spot_timeralarm();
char *(*spot_worklist());

#endif
