/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : prf.h

Function : Include file for profile handling routines.

Author : A. Fregly

Abstract :

Calling Sequence : #include <prf.h>

Notes :
	Will include sysdef.h and qc.h if not already included.

Change log :
000	11-MAY-90  AMF	Created
001	27-MAR-91  AMF	Portability mods.
002   26-AUG-97  AMF    Added "matchinfo" functions to support more sophisticated
				relevance ranking and accurate match highlighting.
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PRF_INC

#ifndef FITSYDEF_INC
#include <fitsydef.h>
#endif

#ifndef QC_INC
#include <qc.h>
#endif

#define PRF_INC 1

/* Bit masks for search status */
#define PRF_MASK_INIT 1
#define PRF_MASK_ENDDOC 2
#define PRF_MASK_NEWDOC 4
#define PRF_MASK_DEALLOC 8

/* Bit masks for search options */
#define PRF_MASK_QRYTERMCOUNTS 1
#define PRF_MASK_EXETERMCOUNTS 2

#if FIT_ANSI


/* Subroutine to initialize a match executable */
int prf_init(int nrows, FIT_WORD *tx_xlate, int nfields, char *(*fields),
  char *(*exe));

/* Add a query to a match executable */
int prf_addqry(char *qname, struct qc_s_cmpqry *cmpqry, char *exe);

/* Delete a query from a match executable */
int prf_delqry(char *qname, char *exe);

/* Write a match executable to a file */
int prf_write(char *fname, char *exe);

/* Read a match executable from a file */
int prf_read(char *fname, char *(*exe));

/* Deallocate a profile exe and all storage used by it */
int prf_dealloc(char *(*exe));

/* Match text against profile exe, returning matches */
int prf_search(int searchstate, unsigned int opts, char *exe,
  unsigned char *text, unsigned int textlen, int *nmatches, int *(*matches),
  unsigned long *(*qrytermmatches), unsigned long *exetermmatches);

/* Searches text with search executable containing multiple queries and
returns match information */
int prf_search_matchinfo(int searchtate, unsigned int opts, char *exe,
  unsigned char *text, unsigned int textlen, char *(*matchinfo));

/* Returns number of queries described within matchinfo */
int prf_matchinfo_nqueries(char *matchinfo);

/* Returns ID's of queries described in matchingo. prf_qname may be used to
retrieve the query name based on a query ID. Other prf_matchinfo_* functions
are used to get more detailed match information for specific query IDs.
*/
/* return query summary information from match information */
int prf_matchinfo_qrymatches(char *matchinfo, int *nmatched, int *(*qryIDs));

/* Returns information needed for highlighting words that matched the
query or queries use in a search by prf_search_matchinfo. */
int prf_matchinfo_highlightinfo(char *matchinfo, unsigned *nmatches, 
  unsigned long *(*offsets), unsigned *(*lengths));

/* Extract match summary information for specified query from match information */
int prf_matchinfo_getqrymatchinfo(char *matchinfo, int qryID, 
    int	*nstringsmatched, int *nmatchableterms, int *ntermsmatched,
    unsigned long *(*offsets), unsigned *(*lengths));

/* Returns match information for a query term that matched against a
document */
int prf_matchinfo_gettermmatches(char *matchinfo, int qryID, 
		int termidx, int *nmatches, int *termID, int *(*fldmatched), 
		unsigned long *(*offsets), unsigned *(*lengths));

/* Returns character string value of term specified with termID */
char *prf_gettermvalue(char *exe, int termID);

/*	Deallocates storage utilized by matchinfo that was created with a 
	call to prf_search_matchinfo. This function only needs to be called at
	the completion of searching and/or when it is desired to recover the
	memory utilized by matchinfo. */
int prf_matchinfo_deallocate(char *(*matchinfo));

/* return number of terms tracked in matchinfo */
int prf_matchinfo_nterms(char *matchinfo);

/* Find offsets/lengths of terms in document */
int prf_offsets(int searchstate, unsigned int opts, char *exe,
  unsigned char *text, unsigned int textlen, int maxoffset, int *noffsets,
  unsigned long *offsets, unsigned *lengths);

/* Return name of query corresponding to index */
char *prf_qname(int index, char *exe);

/* Get current field definitions */
int prf_getflds(char *exe, int *nfields, char *(*(*fields)));

/* Set field defs */
int prf_setflds(char *exe, int nfields, char *(*fields));

/* Get current xlate table */
int prf_getxlate(char *exe, FIT_WORD *tx_xlate);

/* Set xlate table */
int prf_setxlate(unsigned int *term_xlate, FIT_WORD *tx_xlate, char *exe);

/* Get stats */
int prf_status(char *exe, int *nqueries, int *nfields, int *nterms,
  int *nids, int *maxwordlen);


#else /* K&R compatible function definitions */

int prf_init();
int prf_addqry();
int prf_delqry();
int prf_write();
int prf_read();
int prf_dealloc();
int prf_search();
int prf_offsets();
char *prf_qname();
int prf_getflds();
int prf_setflds();
int prf_getxlate();
int prf_setxlate();
int prf_status();
int prf_search_matchinfo();
int prf_matchinfo_nqueries();
int prf_matchinfo_qrymatches();
int prf_matchinfo_highlightinfo();
int prf_matchinfo_getqrymatchinfo();
int prf_matchinfo_gettermmatches();
char *prf_gettermvalue();
int prf_matchinfo_deallocate();
int prf_matchinfo_nterms();
#endif

#endif

#ifdef __cplusplus
};
#endif

