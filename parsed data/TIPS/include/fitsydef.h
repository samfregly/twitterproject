/*******************************************************************************

		       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : fitsydef.h

Function : Provides definitions used by all SeekWare software.

Author : A. Fregly

Abstract : This file is included as the first include file in all SeekWare
	software. The file contains those definitions necessary so that
	portable code may be developed. This file also defines function
	prototypes for all SeekWare general purpose library routines.
	SeekWare currently supports its library routines on the following
	machines/operating/compiler systems:

		PC/DOS/TurboC
		PC/DOS/Zortech
		PC/Windows NT/MS C++ 4.0
		PC/Linux 2.0
		VAX/VMS/DEC C
		MIPS or DECStation/UNIX or ULTRIX/MIPS C
		PC/SCO UNIX/MicroSoft C
		PC/COHERENT (Mark Williams UNIX clone)/K&R Portable C
		SUN OS
		SUN Solaris
		HP/UX

Calling Sequence : #include <fitsydef.h>

Notes :

Change log :
000     1990       AMF  Created
001     27-MAR-91  AMF  Portable version
002     07-FEB-92  AMF  Ported to SUN.
003     12-JUL-92  AMF  Zortech/DOS compatibility
004     12-DEC-92  AMF  K&R and Coherent compatibility
005     08-SEP-93  AMF  Port to HP-UX. Use FIT_DEBUG.
006	15-NOV-97  AMF	Modified to work better with C++
007	25-AUG-03  AMF	Modified to include ncurses under Unix.
008	26-AUG-03  AMF  Added system def for FIT_CYGWIN
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Set up macro so that other include file will know sysdef has been
   included */
#ifndef FITSYDEF_INC
#define FITSYDEF_INC    1

/* Debug code enable */
#ifndef FIT_DEBUG
#define FIT_DEBUG 0
#endif

#if FIT_DEBUG
extern FILE *fit_debug_handle;
#endif

/* Global definitions used in deciding optional code parts to compile */
/* C syntax, either ANSI or K&R */
#define FIT_ANSI        1
#define FIT_KR          0

/* Operating system or compiler */
#define FIT_VMS         0
#define FIT_UNIX        1
#define FIT_ULTRIX      0
#define FIT_COHERENT    0
#define FIT_TURBOC      0
#define FIT_DOS         0
#define FIT_MSC		0
#define FIT_ZORTECH     0
#define FIT_POSIX       0
#define FIT_SCO         0
#define FIT_SUN         0
#define FIT_HP          0
#define FIT_LINUX       1
#define FIT_SOLARIS	0
#define FIT_CYGWIN      0
#define FIT_BSD         1

/* Uses Curses screen library for TT handling */
#define FIT_CURSES      1
#define FIT_NCURSES	1
#define FIT_TERMINFO    1
#define FIT_TERMCAP     0
#define FIT_COLORCURSES 1

#if FIT_UNIX
#include <sys/types.h>
#endif
#if FIT_DOS
#include <sys\types.h>
#endif

/* 32 bit machine, then int is 32 bits */
#define FIT_I32         1

/* 64 bit machine, then long is 64 bits */
#define FIT_L64         1

/* stdlib.h is available, this appears to be a Turbo C and VMS file only.
   Maybe because their compilers are more recent ANSI implementations */
#define FIT_STDLIB      1

/* This is used to allow code to specify register variables. Turbo C is
   so good that we turn it off for it. */
#define FIT_USEREGISTERS 0

/* Define some psuedo types */
#if FIT_KR && !FIT_COHERENT
#ifndef void
#if !FIT_SCO && !FIT_SUN
#define void char
#else
#define void char *
#endif
#endif
#ifndef size_t
#define size_t unsigned int
#endif
#endif

/* Set to 1 if pointer comparisons work correctly. In Turbo C large models,
   they don't, so we need pointer comparison macro's or routines which
   make them work right */
#define FIT_PTRS_OK     1

/* Define min and max functions? */
#if FIT_ZORTECH || FIT_HP || FIT_CYGWIN || FIT_BSD
#define FIT_NOMINMAX    1
#else
#define FIT_NOMINMAX    0
#endif

/* Now define some machine dependent environmental variables */

/* 8 bit bytes seems is universal for the supported systems */
#define FIT_BITS_IN_BYTE 8

#if FIT_I32 || FIT_L64

#define FIT_BITS_IN_INT 32
#define FIT_INT_EXP 5L
#define FIT_BYTES_IN_INT 4

#else

#define FIT_BITS_IN_INT 16
#define FIT_INT_EXP 2L
#define FIT_BYTES_IN_INT 2

#endif

#if FIT_L64

#define FIT_BITS_IN_LONG 64
#define FIT_BYTES_IN_LONG 8
#define FIT_LONG_EXP 6L

#else

#define FIT_BITS_IN_LONG 32
#define FIT_BYTES_IN_LONG 4
#define FIT_LONG_EXP 5L

#endif

#define FIT_BITS_IN_REGISTER BITS_IN_LONG

/* Define some data types */
#if FIT_I32

#define FIT_WORD short
#define FIT_UWORD unsigned short
#define FIT_DWORD int
#define FIT_UDWORD unsigned int
#define FIT_BYTE signed char
#define FIT_UBYTE unsigned char

#else

#define FIT_WORD int
#define FIT_UWORD unsigned int
#define FIT_DWORD long
#define FIT_UDWORD unsigned long
#define FIT_BYTE signed char
#define FIT_UBYTE unsigned char

#endif

#define FIT_REG_IS_LONG 1

#if FIT_REG_IS_LONG

#define FIT_REG unsigned long
#define FIT_BITS_IN_REG FIT_BITS_IN_LONG
#define FIT_REG_EXP FIT_LONG_EXP

#else

#define FIT_REG unsigned int
#define FIT_BITS_IN_REG FIT_BITS_IN_INT
#define FIT_REG_EXP INT_EXP

#endif

#define ptr_eq(ptr1,ptr2) ((ptr1) == (ptr2))
#define ptr_ne(ptr1,ptr2) ((ptr1) != (ptr2))
#define ptr_gt(ptr1,ptr2) ((ptr1) > (ptr2))
#define ptr_ge(ptr1,ptr2) ((ptr1) >= (ptr2))
#define ptr_lt(ptr1,ptr2) ((ptr1) < (ptr2))
#define ptr_le(ptr1,ptr2) ((ptr1) <= (ptr2))
#define ptr_dif(ptr1,ptr2) ((ptr1) - (ptr2))

#if FIT_ANSI
#define void_caste (void *)
#else
#define void_caste (char *)
#endif

/* Define macros for functions which aren't supported by various systems. */

/* Define min and max for systems which don't provide them. Note that
   the arguements used with these versions must not have side effects. */
#if FIT_NOMINMAX

#define max(a,b) (((a) > (b)) ? a : b)
#define min(a,b) (((a) < (b)) ? a : b)

#endif

#if FIT_COHERENT
#define ftok fit_ftok
#define tmpfile fit_tmpfile
#endif

/* Define file name component lengths */
#if FIT_VMS

#define FIT_NODELEN 64
#define FIT_FSPECLEN 255
#define FIT_DEVLEN 64
#define FIT_PATHLEN 255
#define FIT_NAMELEN 255
#define FIT_EXTLEN 64
#define FIT_VERSIONLEN 5

#endif

#if FIT_DOS

#define FIT_NODELEN 0
#define FIT_FSPECLEN 255
#define FIT_DEVLEN 2
#define FIT_PATHLEN 255
#define FIT_NAMELEN 254
#define FIT_EXTLEN 64
#define FIT_VERSIONLEN 0

#endif

#ifndef FIT_NODELEN

/* Unix file name component lengths */
#define FIT_NODELEN 255
#define FIT_FSPECLEN 255 
#define FIT_DEVLEN 0
#define FIT_PATHLEN 255
#define FIT_NAMELEN 255
#define FIT_EXTLEN 255 
#define FIT_VERSIONLEN 0

#endif

#if !FIT_UNIX
#define FIT_FULLFSPECLEN 2048
#else
#define FIT_FULLFSPECLEN 4096
#endif

/* Define READ and WRITE open switches for "binary" file ops */
#if FIT_DOS
#define FIT_READ "rb"
#define FIT_WRITE "wb"
#define FIT_APPEND "ab"
#define FIT_UPDATE "rb+"
#define FIT_CREATEREADWRITE "wb+"
#define FIT_APPENDONCLOSE "ab+"
#else
#define FIT_READ "r"
#define FIT_WRITE "w"
#define FIT_APPEND "a"
#define FIT_UPDATE "r+"
#define FIT_CREATEREADWRITE "w+"
#define FIT_APPENDONCLOSE "a+"
#endif

/* Descriptor data structure */
typedef struct fit_s_descriptor {
  FIT_WORD len;
  FIT_WORD type;
  char *addr;
} fit_descriptor;

/* Time structure */
#if FIT_VMS
typedef struct fit_timestruc {
  int timesnap[2];
} fit_s_timebuf;

#else
#if !FIT_ZORTECH
#include <sys/timeb.h>
#else
#include <time.h>
#if FIT_HP
#include <sys/times.h>
/*******
       struct tms {
	   clock_t tms_utime;           /* user time */
	   clock_t tms_stime;           /* system time */
	   clock_t tms_cutime;          /* user time, children */
	   clock_t tms_cstime;          /* system time, children */
	 extern clock_t times(struct tms *);
*******/
#endif

struct timeb {
  long time;
  short millitm;
  short timezone;
  short dstflag;
};
#endif
typedef struct timeb fit_s_timebuf;

#endif

/* String lists */
struct fit_s_snode {
  char *val;
  unsigned len;
  struct fit_s_snode *next;
};

struct fit_s_slist {
  int nelements;
  unsigned long *nbytes;
  struct fit_s_snode *first;
  struct fit_s_snode *last;
};


/* Prototypes for FIT systems general purpose library routines */
#if FIT_ANSI

/* File name and command line manipulations */
  /* Convert path name to absolute path name */
  void fit_abspath(char *path, char *abspath, char *retpath);

  /* Build file spec from component parts */
  char *fit_bldfspec(char *node, char *dev, char *dir, char *name, char *ext,
    char *version, char *outspec);

  /* Delete file list (see fit_filelist) */
  void fit_delfilelist(char *(*(*filelist)));

  /* Create in memory directory list */
  char *(*fit_dirlist(char *path, int *ndirs));

  /* End fit_findfile scanning */
  void fit_endfind(void *(*context));

  /* Expand file spec to most specific version necessary for current node */
  char *fit_expfspec(char *inspec, char *defaultspec, char *defpath,
    char *outspec);

  /* Create in memory file list of files with specified extension */
  char *(*fit_filelist(char *path, char *ftype, unsigned attrib,
    char *curpath, int *nfiles, char *fullpath));

  /* Find files matching possibly wild carded entries in list */
  char *fit_findfile(void *(*context), char *searchspec, 
    unsigned attrib, char *defaultspec, char *defpath, char *retfile);

  /* Match a file name with a wild carded file name */
  int fit_match(unsigned char *fname, unsigned char *wild, int nchar);

  /* Get next arguement from arguement list where entries are separated by
     commas */
  char *fit_nextarg(char *list, char *retarg);

  /* Return component portions of a file specification */
  void fit_prsfspec(char *inspec, char *node, char *dev, char *path,
    char *name, char *ext, char *version);

  /* Change a file name extension */
  char *fit_setext(char *inname, char *ext, char *outname);

  /* Create a temporary file */
  FILE *fit_tmpfile();

  /* Close a temporary file */
  int fit_tempclose();

  /* Create in memory file list of files which match wild carded file names */
  char *(*fit_wildlist(char *path, char *wildlist, unsigned attrib,
    char *curpath, int *nfiles, char *fullpath));

  /* Change file access mode to binary (Borland C only) */
  int fit_binmode(FILE *handle);

/* File manipulations */
  /* Delete a file */
  int fit_fdelete(char *filespec);

  /* Rename a file */
  int fit_frename(char *oldname, char *newname);

/* Error reporting */
  /* Return last error code internally buffered by SeekWare utility
     routines */
  int fit_lasterr(void);

  /* Print message corresponding to SeekWare error code */
  char *fit_getmsg(int status);

  /* Print time stamped message with optional status message */
  void fit_logmsg(FILE *out, char *msg, int stat);

/* Create descriptor */
  /* Make a descriptor of a data structure */
  fit_descriptor *fit_descript(void *obj, FIT_UWORD len);

/* Time utilities */
  /* Zero a time buffer */
  void fit_zerotime(fit_s_timebuf *timebuf);

  /* Return current time */
  void fit_gettime(fit_s_timebuf *timebuf);

  /* Compute difference between two times */
  void fit_timedif(fit_s_timebuf *time1, fit_s_timebuf *time2,
    fit_s_timebuf *rettime);

  /* Add two times together */
  void fit_addtime(fit_s_timebuf *time1, fit_s_timebuf *time2,
    fit_s_timebuf *rettime);

  /* Return time as relative number of seconds since system zero time */
  unsigned long fit_seconds(fit_s_timebuf *timebuf);

  /* Return number of milliseconds occuring after the second specified by
     the time */
  unsigned int fit_millisecs(fit_s_timebuf *timebuf);

  long fit_cputime();

  /* Return seconds and milliseconds components of a time */
  void fit_timeunpk(fit_s_timebuf *timebuf, unsigned long *secs,
    unsigned int *millisecs);

/* String manipulation utilities */
  /* Add one string to another */
  void fit_sadd(char *dest, int len, char *src, int *offset);

  /* Assign a portion of a string to a portion of another string */
  void fit_sassign(char *dest, int ds, int de, char *src, int ss, int se);

  /* Blank a string */
  char *fit_sblank(char *str, int len);

  /* Center the printable text in a string */
  int fit_scenter(char *str, int width);

  /* Extend with blanks a string */
  void fit_sextend(char *str, int width);

  /* Left justify the printable text in a string */
  int fit_sljust(char *str, int width);

  /* Locate a substring within a string */
  int fit_slocate(char *str, char *substr);

  /* Right justify the printable text in a string */
  void fit_srjust(char *str, int width);

  /* Sort a list of strings */
  int fit_ssort(char *(*inlist), unsigned len[], int n);

  /* Upcase the text in a string */
  char *fit_supcase(char *str);

  /* Trim off trailing whitespace in a string */
  char *fit_strim(char *str, unsigned int *len);

/* String list manipulation functions */
  /* Initialize a new string list */
  struct fit_s_slist *fit_init_slist(void);

  /* Add an entry to a string list */
  int fit_add_slist(struct fit_s_slist *slist, char *val);

  /* Sort a string list */
  int fit_sort_slist(struct fit_s_slist *slist);

  /* Create an array of string from a string list */
  char *(*fit_arr_slist(struct fit_s_slist *slist));

  /* Remove the duplicates from a sorted string list */
  void fit_nodu_slist(struct fit_s_slist *slist);
  
  /* Delete a string list */
  void fit_del_slist(struct fit_s_slist *(*slist));


/* Emulation routines */
  /* emulate ftok */
  int fit_ftok(char *spec, char modifier);

#else /* K&R compatible function definitions */

/* File name and command line manipulations */
  void fit_abspath();

  char *fit_bldfspec();

  void fit_delfilelist();

  char *(*fit_dirlist());

  void fit_endfind();

  char *fit_expfspec();

  char *(*fit_filelist());

  char *fit_findfile();

  int fit_match();

  char *fit_nextarg();

  void fit_prsfspec();

  char *fit_setext();

  FILE *fit_tmpfile();

  int fit_tempclose();

  char *(*fit_wildlist());

  int fit_binmode();

/* File manipulations */
  /* Delete a file */
  int fit_fdelete();

  /* Rename a file */
  int fit_frename();

/* Error reporting */
  int fit_lasterr();

  char *fit_getmsg();

  void fit_logmsg();

/* Create descriptor */
  fit_descriptor *fit_descript();


/* Time utilities */
  void fit_zerotime();

  void fit_gettime();

  void fit_timedif();

  void fit_addtime();

  unsigned long fit_seconds();

  unsigned int fit_millisecs();

  void fit_timeumpk();

/* String manipulation utilities */
  void fit_sadd();

  void fit_sassign();

  char *fit_sblank();

  int fit_scenter();

  void fit_sextend();

  int fit_sljust();

  int fit_slocate();

  void fit_srjust();

  char *fit_supcase();

  char *fit_strim();

  int fit_ssort();

/* String list manipulations */
  int fit_add_slist();

  struct fit_s_slist *fit_init_slist();

  int fit_sort_slist();

  char *(*fit_arr_slist());

/* Emulation routines */
  int fit_ftok();

#endif
#endif
#ifdef __cplusplus
};
#endif
