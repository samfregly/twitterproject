/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : lovlvlio.h

Function : Defines low level i/o functions not defined by standard C header
	files.

Author : A. Fregly

Abstract : Curiously enough, it appears that several low level i/o functions
	are not defined in any of the standard C header files. This becomes
	a factor or signifigance for functions such as "lseek" which return
	a long integer result. This include file provides definitions for
	these functions.

Calling Sequence : #include <lowlvlio.h>

Notes:
	fitsydef.h must be included before the inclusion of this file.

Change log : 
000	03-JUL-91  AMF	Created.
******************************************************************************/

#if FIT_ANSI

int close(int handle);
#if !FIT_LINUX
int creat(char *path, int mode);
#endif
int dup(int handle);
long lseek(int handle, long offset, int whence);
#if !FIT_LINUX
int open(char *path, int oflags, int mode);
#endif
int read(int handle, void *buffer, unsigned len);
int write(int handle, void *buffer, unsigned len);

#if FIT_TURBOC
int _read(int handle, void *buffer, unsigned len);
int _write(int handle, void *buffer, unsigned len);
#endif

#else

int close();
int creat();
int dup();
long lseek();
int open();
int read();
int write();

#if FIT_TURBOC
int _read();
int _write();
#endif

#endif
/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : lovlvlio.h

Function : Defines low level i/o functions not defined by standard C header
	files.

Author : A. Fregly

Abstract : Curiously enough, it appears that several low level i/o functions
	are not defined in any of the standard C header files. This becomes
	a factor or signifigance for functions such as "lseek" which return
	a long integer result. This include file provides definitions for
	these functions.

Calling Sequence : #include <lowlvlio.h>

Notes:
	fitsydef.h must be included before the inclusion of this file.

Change log : 
000	03-JUL-91  AMF	Created.
******************************************************************************/

#if FIT_ANSI

int close(int handle);
#if !FIT_LINUX
int creat(char *path, int mode);
#endif
int dup(int handle);
long lseek(int handle, long offset, int whence);
#if !FIT_LINUX
int open(char *path, int oflags, int mode);
#endif
int read(int handle, void *buffer, unsigned len);
int write(int handle, void *buffer, unsigned len);

#if FIT_TURBOC
int _read(int handle, void *buffer, unsigned len);
int _write(int handle, void *buffer, unsigned len);
#endif

#else

int close();
int creat();
int dup();
long lseek();
int open();
int read();
int write();

#if FIT_TURBOC
int _read();
int _write();
#endif

#endif
