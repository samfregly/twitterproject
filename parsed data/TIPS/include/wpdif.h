/******************************************************************************
Module Name : wpdef.h

Function : Application specific include file for wpdif program.

Author : A. Fregly

Abstract : This routine defines the application specifice constants,
  data types, and structures used by the wpdef program.

Calling Sequence : #include <wpdef.h>

Notes : 
	stdio.h must be included in front of the include statement for this
	file.

Change log :
000	21-MAR-91  AMF	Created.
******************************************************************************/

#define WP_BUFMAX 16384
#define WP_HEADERMAX 80

int wpdif_getpar(FILE *in, int bufmax, long *offset, char *buf, int *len);

int wpdif_converge(FILE *old, long olddifoff, char *oldbuf, FILE *new,
  long newdifoff, char *newbuf, long *oldconverge, long *newconverge);

int wpdif_redline(FILE *in, long paroffset, long difoff, long convergeoff,
  FILE *out);

int wpdif_hilight(FILE *in, long paroffset, long difoff, long convergeoff,
  FILE *out);

int wpdif_putsep(FILE *out, char delim, int nlines, int linelen);

int wpdif_puthead(FILE *out, char *headingbuf, char delim, int headmax);

int wpdif_puttext(FILE *in, long offset, long difoff, long enddifoff,
  long endtextoff, char startdelim, char enddelim, FILE *out);

char *wpdif_nextword(char *buf, char *curptr, int len);

int wpdif_ishead(char *buf, int buflen);

int wpdif_cmpbufs(char *buf1, char *buf2, int buflen);

int wpdif_gwphead(FILE *in, int bufmax, char *buf, int *retlen);

int wpdif_pwphead(char *buf, int len, char *fname, FILE *out);
/******************************************************************************
Module Name : wpdef.h

Function : Application specific include file for wpdif program.

Author : A. Fregly

Abstract : This routine defines the application specifice constants,
  data types, and structures used by the wpdef program.

Calling Sequence : #include <wpdef.h>

Notes : 
	stdio.h must be included in front of the include statement for this
	file.

Change log :
000	21-MAR-91  AMF	Created.
******************************************************************************/

#define WP_BUFMAX 16384
#define WP_HEADERMAX 80

int wpdif_getpar(FILE *in, int bufmax, long *offset, char *buf, int *len);

int wpdif_converge(FILE *old, long olddifoff, char *oldbuf, FILE *new,
  long newdifoff, char *newbuf, long *oldconverge, long *newconverge);

int wpdif_redline(FILE *in, long paroffset, long difoff, long convergeoff,
  FILE *out);

int wpdif_hilight(FILE *in, long paroffset, long difoff, long convergeoff,
  FILE *out);

int wpdif_putsep(FILE *out, char delim, int nlines, int linelen);

int wpdif_puthead(FILE *out, char *headingbuf, char delim, int headmax);

int wpdif_puttext(FILE *in, long offset, long difoff, long enddifoff,
  long endtextoff, char startdelim, char enddelim, FILE *out);

char *wpdif_nextword(char *buf, char *curptr, int len);

int wpdif_ishead(char *buf, int buflen);

int wpdif_cmpbufs(char *buf1, char *buf2, int buflen);

int wpdif_gwphead(FILE *in, int bufmax, char *buf, int *retlen);

int wpdif_pwphead(char *buf, int len, char *fname, FILE *out);
