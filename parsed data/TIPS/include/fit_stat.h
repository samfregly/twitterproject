/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name :  fit_stat.h

Function :  Defines symbolics for SeekWare internal status codes.

Author : A. Fregly

Abstract :

Calling Sequence :

Notes :

Change log :
000	06-FEB-91  AMF	Created.
001	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/* Status codes. Success codes have bit 0 set. Warnings, Errors, and Fatals
   have bit 0 clear (ala VAX VMS). Warnings have bit 1 set, Errors have bit 
   2 set, Fatal errors have bit 3 set */

/* Success codes */
#define FIT_STAT_NORMAL	1

/* Warnings */

/* Errors */
/* Compilation error codes */
#define FIT_ERR_OPERAND	0x04
#define FIT_ERR_OPERATOR	0x14
#define FIT_ERR_PAREN	        0x24
#define FIT_ERR_NULLTERM	0x34
#define FIT_ERR_PROXIMITY	0x44
#define FIT_ERR_FIELD		0x54
#define FIT_ERR_THRESHHOLD	0x64

/* I/O errors */
#define FIT_ERR_OPEN		0x74
#define FIT_ERR_READ		0x84
#define FIT_ERR_WRITE		0x94
#define FIT_ERR_CLOSE		0xA4
#define FIT_ERR_ASSIGN		0xB4

/* Fatals, i.e. program should abort on seeing one of these */ 
#define FIT_FATAL_MEM		0x08

/* Status code value for most recent error */
extern int fit_status;

#ifdef __cplusplus
};
#endif

