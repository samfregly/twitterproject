/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************

Module Name : qc.h

Function : Declares query compiler functions available for general use.
	Declares various structure types used by compiler.  Declares
	constants.

Author : A. Fregly

Abstract :

Calling Sequence : #include <qc.h>

Notes :

Change log :
000	23-MAR-90  AMF	Created
001	27-MAR-91  AMF	K&R compatibility.
002	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef QC_INC
#define QC_INC

#ifndef FITSYDEF_INC
#include <fitsydef.h>
#endif

#ifndef DRCT_INC
#include <drct.h>
#endif

/* Maximum length a term word with an embedded VLDC may be expanded to */
#define QC_MAXTERMWORDLEN 24

/* Token types.  The operator token values correspond with the values used
in the logic equation of a compiled query */
#define QC_TERM 0
#define QC_LPAREN -1
#define QC_FIELD -2
#define QC_THRESHHOLD -3
#define QC_NOT -4
#define QC_PROXIMITY -5
#define QC_AND -6
#define QC_OR  -7
#define QC_COMMA -8
#define QC_RPAREN -9
#define QC_BOQ -10
#define QC_EOQ -11
#define QC_ERRTOKEN -99

/* Offsets of delimiters in delimiter string */
#define QC_MAXDELIM	16
#define QC_OFF_OPENPAREN	0
#define QC_OFF_CLOSEPAREN	1
#define QC_OFF_QUOTE		2
#define QC_OFF_SCAWC		3
#define QC_OFF_ASWC		4
#define QC_OFF_SCNWC		5
#define QC_OFF_NSWC		6
#define QC_OFF_SETWC_OPEN	7
#define QC_OFF_SETWC_CLOSE      8
#define QC_OFF_RANGEDELIM       9
#define QC_OFF_AVLWC            10
#define QC_OFF_NVLWC            11
#define QC_OFF_AFLDC            12
#define QC_OFF_NFLDC            13
#define QC_OFF_FIELDDELIM       14
#define QC_OFF_DELIM_COMMA	15

/* Offsets of reserved words in reserved word list */
#define QC_MAXRESERVED 12
#define QC_OFF_OR	0
#define QC_OFF_AND	1
#define QC_OFF_COMMA	2
#define QC_OFF_LPAREN	3
#define QC_OFF_RPAREN	4
#define QC_OFF_FIELD	5
#define QC_OFF_ANY	6
#define QC_OFF_ALL	7
#define QC_OFF_NOT	8
#define QC_OFF_WITHIN	9
#define QC_OFF_AHEAD	10
#define QC_OFF_EOQ	11

/* Structure which defines a threshhold search.  Threshhold search groups
   are also used in proximity relationships */
struct qc_s_termlist {
  int nelements;		/* Number of terms in list */
  int *element;			/* Term indexes of the component elements of */
				/* the threshhold search */
};


/* Definitions for proximity type bit mask */
  /* If this bit is not set, bidirectional proximity is assumed */
#define QC_BIT_UNIDIRECTIONAL 0
  /* If this bit is not set, distance is assumed to be in units of words */
#define QC_BIT_CHARACTERS 1

/* Structure which defines a proximity relationship */
struct qc_s_proximity {
  int termnum;			/* Term number representing proximity */
				/* relationship in logic equation */
  int relation;	     		/* Bit mask defining proximity relationship.*/
				/* see QC_BIT_xxxx bit defs. */
  long distance;		/* Distance which terms must fall at or */
				/* within in order for proximity to match */
  struct qc_s_termlist *list0;	/* First group of terms in relationship. If */
				/* proximity is unidirectional, these terms */
				/* must be the preceding terms. */
  struct qc_s_termlist *list1;	/* Second group of terms in relationship. */
				/* If proximity is unidirectional, these */
				/* terms must be the following terms. */
};


/* Structure def for a compiled query */
struct qc_s_cmpqry {
  int nterms;		/* Number of terms in query */
  int terms_L;		/* Length of term buffer */
  int noperands;	/* Number of operands in query, terms+threshholds+*/
			/* proximity relations */
  int nfields;		/* Number of fields defined for query */
  int fields_L;		/* Length of field names buffer */
  char *qrytext;	/* Character string containing terms of query */
  char *fields;		/* Character string containing field names for query */
  int nlogic;		/* Number of elements in logic equation */
  int *qrylogic;	/* Logic equation for query */
  int *termlen;		/* Number of characters in each query term */
  int *fieldlen;	/* Lengths of field names */
  long *fieldmap;	/* Field enabled bitmap for each query term */
  int *offsets;		/* Offsets of query terms in query text */
  int *fldoffsets;	/* Offset of fields in fields character string */
  char delims[QC_MAXDELIM]; /* Delimiters used in query compile */
  char *reserved[QC_MAXRESERVED];/* Reserved words used in query compile */
  int nproximity;	/* Number of proximity relation ships in query */
  struct qc_s_proximity *(*proximity);	/* Proximity relationships */
};


/**************************** Function Definitions ************************/

/* Close query object library */
#define qc_closelib(lib) drct_close(lib)

/* Delete library entry */
#define qc_dellib(lib, entry) ((drct_setpos(lib, entry)) ? \
  drct_delrec(lib, 1) : 0)


#if FIT_ANSI

/* The query compiler */
int qc_compile(char *srcqry, int nfields, char *fieldnames[],
  int nreserved, char *reserved[], char *delims,
  struct qc_s_cmpqry *(*cmpqry));

/* Compiled query deallocation */
void qc_dealloc(struct qc_s_cmpqry *(*cmpqry));

/* Dumps a compiled query to an output stream */
void qc_dumpqry(FILE *stream, struct qc_s_cmpqry *cmpqry);

/* Find specific entry in query library */
long qc_findlib(struct drct_s_file *lib, char *qryname);

/* Retrieves a query compilation error message */
char *qc_geterr(int erridx, int *errnum, int *offset);

/* Get specified entry from query library */
long qc_getlib(struct drct_s_file *lib, long entrynum, char *qryname,
  struct qc_s_cmpqry *(*cmpqry));

/* Retrieves name of next query in library */
long qc_getqrylib(struct drct_s_file *lib, char *qryname);

/* Get next entry from query library */
long qc_nextlib(struct drct_s_file *lib, char *qryname,
  struct qc_s_cmpqry *(*cmpqry));

/* Open query object library */
struct drct_s_file *qc_openlib(char *libname, int openmode, int access);

/* Pack a compiled query into a buffer */
int qc_packobj(struct qc_s_cmpqry *objqry, char *objbuf, int bufmax,
  int *retlen);

/* Writes a descriptive error message for a query compilation error */
void qc_puterr(FILE *stream, char *srcqry, char *errmsg, int offset);

/* Put opbject query into library */
long qc_putlib(struct drct_s_file *lib, char *qryname,
  struct qc_s_cmpqry *cmpqry);

/* Reads a compiled query which was written by qc_writeqry */
int qc_readqry(char *qryname, char *(*qrytext), struct qc_s_cmpqry *(*cmpqry));

/* Writes a compiled query to a stream oriented output file */
int qc_writeqry(char *fname, char *qrytext, struct qc_s_cmpqry *cmpqry);

/* Unpack a compiled query */
void qc_unpackob(char *objbuf, int buflen, struct qc_s_cmpqry *(*objqry));

#else /* K&R compatible prototypes */

int qc_compile();
void qc_dealloc();
void qc_dumpqry();
long qc_findlib();
char *qc_geterr();
long qc_getlib();
long qc_getqrylib();
long qc_nextlib();
int qc_packobj();
void qc_puterr();
long qc_putlib();
int qc_writeqry();
int qc_readqry();
void qc_unpackob();
struct drct_s_file *qc_openlib();

#endif

#endif

#ifdef __cplusplus
};
#endif
