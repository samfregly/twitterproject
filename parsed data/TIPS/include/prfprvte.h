/******************************************************************************
Module Name : prfprvte.h

Function : Defines private profile routines used by TIPS software.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <prfprvte.h>

Notes :
	fitsydef.h, qc.h, and prf.h must be include before this file.

Change log :
000	27-MAR-91  AMF	Created by extracting from prf.h.
001	23-OCT-97  AMF	Added matchinfo functions and data structs. Made typedefs
				for all structures.
002	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PRFPRVTE_INC

#ifndef FITSYDEF_INC
#include <fitsydef.h>
#endif

#ifndef QC_INC
#include <qc.h>
#endif

#ifndef PRF_INC
#include <prf.h>
#endif

#define PRFPRVTE_INC 1

typedef struct prf_s_matchtable {
  FIT_REG *(*registers);   /* nrow * ncols match register table */
  FIT_REG *eotflag;	   /* One bit per term per row to indicate */
} prf_s_matchtable;

#define PRF_MATCHED 1
#define PRF_ISPHRASEWORD 2
#define PRF_ISPROX1WORD 4
#define PRF_ISPROX2WORD 8

#define PRF_BIT_WORD 0
#define PRF_BIT_PHRASE 1
#define PRF_BIT_WPROXIMITY 2
#define PRF_BIT_CPROXIMITY 3

typedef struct prf_s_term {
  FIT_WORD ttype;		   /* Term type mask */
  FIT_WORD nid;                    /* Num field maps, logic IDs used */
  FIT_REG *fieldmap;		   /* Field Maps */
  int *logicid;		           /* Logic ID for term/field combo */
  int len;                         /* Length of term */
  char *val;			   /* Term value */
  int lson;	 		   /* Left binary tree pointer */
  int rson;	  		   /* Right binary tree pointer */
  FIT_WORD nwords;		   /* Number of words in phrase */
  int nextword;			   /* Index of next following word for
				      multiword term */
} prf_s_term;

typedef struct prf_s_phraseterm {
  int len;
  char *val;
  int nextword;
  int termnum;
  FIT_REG termmask;
} prf_s_phraseterm;


typedef struct prf_s_proximity {
  int id;
  int relation;
  long distance;
} prf_s_proximity;

typedef struct prf_s_termmap {
  FIT_WORD termtype;
  int listnum;
  int termidx;
} prf_s_termmap;

typedef struct prf_s_exe {
  FIT_UWORD *term_xlate;	   /* 256 entry translate table for query
				      terms, values should range from 0 to
				      number of column in matchtable minus
				      1.  Values above 255 are reserved for
				      wild cards and other special
				      characters.  These values are defined
				      in tips.h */
  FIT_WORD *tx_xlate;	  	   /* 256 entry translate table for text
				      being searched. Should map chars to
				      values corresponding to those in
				      term_xlate.  Characters mapped to
				      0 or -1 are considered as white space
				      delimiters, and any other negative
				      number is a start of field marker */
  prf_s_matchtable table;   		/* Term match table */
  prf_s_matchtable phrasetbl; 	/* Term match table for phrase
				      2nd and greater terms */
  FIT_REG *phrasemask;		   /* Bit mask for terms which are phrases */
  FIT_REG *prox1mask;		   /* Bit mask for terms which are first term
				      of a proximity relationship */
  FIT_REG *phraseprox1mask;	   /* Bit mask for last word of phrases which
				      are part of the first term of a proximity
				      relationship. */
  FIT_REG *prox2mask;	  	   /* Bit mask for terms which are second
				      term of a proximity relationship */
  FIT_REG *phraseprox2mask;	   /* Bit mask for last word of phrases which
				      are part of the second term of a proximity
				      relationship. */
  FIT_REG *phraseproxmask;	   /* Bit mask of all terms which are either
				      part of a phrase or proximity
				      relation */
  unsigned int nterms;             /* Number of entries in term list, number
				      columns used in match table */
  unsigned int nphraseterms;	   /* Number of entries in phrase table */
  unsigned int nproximity;	   /* Number of proximity relationships */
  unsigned int termalloc;	   /* Number of terms allocated */
  unsigned int nlogicids;	   /* Number logic IDs for all terms */
  unsigned int nqueries;           /* Number of queries */
  unsigned int qryalloc;	   /* Number of query slots allocated */
  unsigned int nfields;            /* Number of fields defined for stream */
  unsigned int nrows;		   /* Number of rows in match table */
  unsigned int ncols;		   /* Number of columns in matchtable */
  unsigned int regwidth;	   /* Width in long words of match table regs */
  unsigned int phraseregwidth;	   /* Width in long words of phrase match table
				      registers */
  prf_s_term *(*term);	   		/* Array of pointers at unique terms */
  prf_s_phraseterm *(*phraseterm); 	/* Array of pointers at following words
					in phrases */
  prf_s_proximity *proximity;	 /* Proximity relationships */
  int *proxterm1map;		   /* Maps proximity first terms to proximity
				      relations */
  int *proxterm2map;		   /* Maps proximity second terms to proximity
				      relations */
  int *logiclen;		   /* Lengths of logic equations */
  int *termtype;		   /* Array of term types */
  int *(*logic);                   /* Array of pointers at logic equations */
  char *(*qfiles);		   /* Array of pointers at query names */
  char *(*fields);		   /* Array of pointers at field names */
} prf_s_exe;

typedef struct prf_s_match {
  int termidx;			  /* Term index in unique terms list */
  int matchtype;		  /* Type of term match (word, proximity first word,
				     proximity trailing word, phrase) */
  unsigned long offset;		  /* Offset of match within document */
  int length;			  /* Length of match within document */
  long wordnum;			  /* Word number for match within document */
  int fldnum;			  /* Field number for field containing match */
  struct prf_s_match *nextmatch;  /* Points at next match within term match list */
} prf_s_match;

typedef struct prf_s_proxtoterm {
  int termidx;
  struct prf_s_proxtoterm *next;
} prf_s_proxtoterm;


typedef struct prf_s_qrytermmatch {
  int termid;
  int used;
  int nmatched;
  struct prf_s_match *match;
} prf_s_qrytermmatch;

typedef struct prf_s_qrysearchmatches {
  int qryID;
  unsigned nstringsmatched;
  unsigned nmatchableterms;
  unsigned ntermsmatched;
  prf_s_qrytermmatch *term;
} prf_s_qrysearchmatches;

/* typedef struct prf_s_match prf_s_match; */
typedef struct prf_s_matchinfo {
  unsigned int nterms;		  /* Number of unique terms */
  unsigned int nqueries;	  /* Number of queries */
  unsigned int nlogicids;	  /* Number of term ID's contained in queries */
  unsigned int nqrymatches;	  /* Number of matching queries */
  unsigned int nwords;            /* Number of words in document */
  unsigned int nproximity;        /* Number of proximity relationships in query */
  FIT_REG *fldmap;		  /* Map of fields that unique terms matched on */
  FIT_REG *idmatch;		  /* Logic equation term IDs list. Field maps for matches */
  int *idmap;		          /* Maps logic equation terms to unique terms list */
  int *logiclen;		  /* List of logic equation lengths */
  int *(*logic);		  /* List of logic equations */
  int *qrymatch;		  /* List of matching query IDs */
  prf_s_match *(*term);		  /* List of term match lists */
  prf_s_proxtoterm *proxtoterm;   /* Proximity relationship to terms map */
  prf_s_qrysearchmatches *qrysearchmatches; /* Search matches for most recently requested
                                              query */
} prf_s_matchinfo;

typedef struct prf_s_matchdescriptor {
  unsigned long offset;
  unsigned int length;
  int fldnum;
} prf_s_matchdescriptor;

typedef struct prf_s_operand {
  int logicid;
  FIT_REG val;
  struct prf_s_operand *next;
} prf_s_operand;
  
typedef struct prf_s_logicstack {
  FIT_REG val;
  prf_s_operand *first;
  prf_s_operand *last;
} prf_s_logicstack;

#if FIT_ANSI

/* Allocate and initialize matchinfo structure */
int prf_matchinfo_allocate(prf_s_exe *exe, char *(*matchinfo));

/* Reset match info structure. This should be done prior to searching
   a new document with the matchinfo structure */
int prf_matchinfo_reset(prf_s_exe *exe, char *(*matchinfo));

/* Add word or first word of phrase match to matchinfo structure */
int prf_matchinfo_addmatch(prf_s_matchinfo *matchinfo, 
  int matchtype, int termidx, unsigned long matchoffset, int matchlen, 
  long curwordnum, int nextfldnum);

/* Complete phrase match within matchinfo structure */
int prf_search_addphraseterm(prf_s_matchinfo *matchinfo,
    prf_s_exe *exe, int termidx, int endwordnum, unsigned long endoffset);

/* Dump a match executable */
int prf_dump(prf_s_exe *exe);

/* Add a non-range term to the profile executable */
int prf_addterm(char *term, char delims[QC_MAXDELIM],
  prf_s_termmap *termmap, FIT_REG fields,
  prf_s_exe *exe, int *retidx);

/* Expand profile executable to accomodate more terms */
int prf_expterms(prf_s_exe *exe, int nlong);

/* Expand profile executable to accomodate more queries */
int prf_expqry(prf_s_exe *exe, int n);

/* Add logic equation to profile executable */
int prf_addlogic(int *logic, int nelements, int exeidx, int *(*logiclist));

/* Add name to profile executable */
int prf_addname(char *name, int idx, char *(*namelist));

/* Look up term in term list */
int prf_lookup(char *term, FIT_WORD termtype, prf_s_exe *exe,
  int *idx, int *compare);

/* Allocate a term, linking it to specified term */
int prf_alloctrm(prf_s_exe *exe, char delims[QC_MAXDELIM], char *term, 
  prf_s_termmap *termmap, int idx, int relation, int *retidx);

/* Add new identifier/field combination to term list entry */
int prf_addid(prf_s_term *term, unsigned int id, FIT_REG fields);

/* Find next word in phrase */
int prf_findword(char *val, int len, int *idx, FIT_UWORD *term_xlate,
  char delims[QC_MAXDELIM], FIT_WORD *tx_xlate, int *off, int *wordlen);

/* Put term into match table */
int prf_filltbl(FIT_REG *registers, FIT_REG *eotflag, int nrows, int ncols,
  int regwidth, FIT_UWORD *term_xlate, char delims[QC_MAXDELIM],
  FIT_WORD *tx_xlate, int idx, char *val, int len);

/* Add word to phrase tables and initialize phrase word descriptor */
int prf_addphrse(prf_s_exe *exe, char delims[QC_MAXDELIM], char *val, 
  int len, int *idx);

int prf_compare_matches(const void *p1, const void *p2);

int prf_setqrysearchmatches(prf_s_matchinfo *matchinfo, int qryID, 
  prf_s_qrysearchmatches *(*retsearchmatches));

int prf_s_addqrytermmatch(prf_s_qrysearchmatches *qrysearchmatches, prf_s_match *match,
  int termid);

int prf_getqrysearchmatches(prf_s_matchinfo *matchinfo, int qryID);

int prf_deallocateqrysearchmatches(prf_s_qrysearchmatches *qrysearchmatches);

#else /* K&R compatible function definitions */

int prf_dump();
int prf_addterm();
int prf_expterms();
int prf_expqry();
int prf_addlogic();
int prf_addname();
int prf_lookup();
int prf_alloctrm();
int prf_addid();
int prf_findword();
int prf_filltbl();
int prf_addphrse();
int prf_matchinfo_allocate();
int prf_matchinfo_reset();
int prf_matchinfo_addmatch();
int prf_search_addphraseterm();
int prf_compare_matches();

#endif

#endif

#ifdef __cplusplus
};
#endif

