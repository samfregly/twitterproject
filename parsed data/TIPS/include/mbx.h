/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : mbx.h

Function : Defines function library for process mailboxes.

Author : A. Fregly

Abstract : This header file defines a function library for interprocess
	communications using mailboxes. The intent is to provide transparent
	communications between processes, even if they must communicate
	over a network.

Calling Sequence : #include <mbx.h>

Notes: 

	Network operation is not currently supported.
	sys/types.h and sys/msg.h must be included before this file.

Change log : 
000	09-JUN-91  AMF	Created
******************************************************************************/

/* Bit definitions for mailbox creation options */
#define MBX_BIT_NET	0

/* Masks for mailbox creations options */
#define MBX_MASK_NET	(1 << MBX_BIT_NET)

/* Bit defs for i/o options */
#define MBX_BIT_ASYNCH	0

/* Mask for i/o options */
#define MBX_MASK_ASYNCH (1 << MBX_BIT_ASYNCH)

/* Mailbox structure */
struct mbx_s_mbx {
  char *mbxname;
  key_t msgqkey;
  int msgqid;
  int retmsgqid;
  int size;
};

/* Mailbox message header */
struct mbx_s_msgheader {
  long msgtype;
  unsigned long msgid;
  int buflen;
  int retmsgqid;
};

#if FIT_ANSI
/* Create mailbox */
struct mbx_s_mbx *mbx_create(char *mbxname, int mbxsize);

/* Send data via mailbox */
int mbx_send(struct mbx_s_mbx *handle, unsigned int opt, int timeout, 
  void *buffer, int buflen, void *action());

/* Receive data from mailbox */
int mbx_receive(struct mbx_s_mbx *handle, unsigned int opt, 
  int timeout, void *buffer, int bufmax, void *action());

/* Delete mailbox */
int mbx_delete(struct mbx_s_mbx *handle);

/************************** Private Routines ********************************/
void mbx_wakeup();

void mbx_receive_relay();

#else

struct mbx_s_mbx *mbx_create();
int mbx_send();
int mbx_receive();
int mbx_delete();
void mbx_wakeup();
void mbx_receive_relay();

#endif
/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : mbx.h

Function : Defines function library for process mailboxes.

Author : A. Fregly

Abstract : This header file defines a function library for interprocess
	communications using mailboxes. The intent is to provide transparent
	communications between processes, even if they must communicate
	over a network.

Calling Sequence : #include <mbx.h>

Notes: 

	Network operation is not currently supported.
	sys/types.h and sys/msg.h must be included before this file.

Change log : 
000	09-JUN-91  AMF	Created
******************************************************************************/

/* Bit definitions for mailbox creation options */
#define MBX_BIT_NET	0

/* Masks for mailbox creations options */
#define MBX_MASK_NET	(1 << MBX_BIT_NET)

/* Bit defs for i/o options */
#define MBX_BIT_ASYNCH	0

/* Mask for i/o options */
#define MBX_MASK_ASYNCH (1 << MBX_BIT_ASYNCH)

/* Mailbox structure */
struct mbx_s_mbx {
  char *mbxname;
  key_t msgqkey;
  int msgqid;
  int retmsgqid;
  int size;
};

/* Mailbox message header */
struct mbx_s_msgheader {
  long msgtype;
  unsigned long msgid;
  int buflen;
  int retmsgqid;
};

#if FIT_ANSI
/* Create mailbox */
struct mbx_s_mbx *mbx_create(char *mbxname, int mbxsize);

/* Send data via mailbox */
int mbx_send(struct mbx_s_mbx *handle, unsigned int opt, int timeout, 
  void *buffer, int buflen, void *action());

/* Receive data from mailbox */
int mbx_receive(struct mbx_s_mbx *handle, unsigned int opt, 
  int timeout, void *buffer, int bufmax, void *action());

/* Delete mailbox */
int mbx_delete(struct mbx_s_mbx *handle);

/************************** Private Routines ********************************/
void mbx_wakeup();

void mbx_receive_relay();

#else

struct mbx_s_mbx *mbx_create();
int mbx_send();
int mbx_receive();
int mbx_delete();
void mbx_wakeup();
void mbx_receive_relay();

#endif
