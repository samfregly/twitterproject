/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : lkmg.h

Function : Defines functions for manipulating locks.

Author : A. Fregly

Abstract : This include files defines a library of functions for use
	in lock management.

Calling Sequence : #include <lkmg.h>

Notes: 
	stdio.h and fitsydef.h need to be included before this file.

Change log : 
000	09-JUN-91  AMF	Created.
******************************************************************************/

/* Bit definitions for lock options/types */
#define LKMG_BIT_FILE		0
#define LKMG_BIT_NET		1
#define LKMG_BIT_MEMORY		2
#define LKMG_BIT_RESERVED3	3
#define LKMG_BIT_RESERVED4	4
#define LKMG_BIT_RESERVED5	5
#define LKMG_BIT_RESERVED6	6
#define LKMG_BIT_RESERVED7	7
#define LKMG_BIT_RESERVED8	8
#define LKMG_BIT_RESERVED9	9
#define LKMG_BIT_USER1		0xA
#define LKMG_BIT_USER2		0xB
#define LKMG_BIT_USER3		0xC
#define LKMG_BIT_USER4		0xD
#define LKMG_BIT_USER5		0xE
#define LKMG_BIT_USER6		0xF

/* Lock types as bit masks */
#define LKMG_MASK_FILE		(1 << 0)
#define LKMG_MASK_NET		(1 << 1)
#define LKMG_MASK_MEMORY	(1 << 2)
#define LKMG_MASK_RESERVED3	(1 << 3)
#define LKMG_MASK_RESERVED4	(1 << 4)
#define LKMG_MASK_RESERVED5	(1 << 5)
#define LKMG_MASK_RESERVED6	(1 << 6)
#define LKMG_MASK_RESERVED7	(1 << 7)
#define LKMG_MASK_RESERVED8	(1 << 8)
#define LKMG_MASK_RESERVED9	(1 << 9)
#define LKMG_MASK_USER1		(1 << 0xA)
#define LKMG_MASK_USER2		(1 << 0xB)
#define LKMG_MASK_USER3		(1 << 0xC)
#define LKMG_MASK_USER4		(1 << 0xD)
#define LKMG_MASK_USER5		(1 << 0xE)
#define LKMG_MASK_USER6		(1 << 0xF)


/* Define lock types, for now only EXCLUSIVE locks are supported */
#define LKMG_EXCLUSIVE		1
#define LKMG_WRITE		2
#define LKMG_READ		3
#define LKMG_SHAREDREAD		4
#define LKMG_NULL		5


/* Define the lock structure */
struct lkmg_s_lock {
  char *lockname;		/* Name of lock */
  unsigned int opts;		/* Options supplied during creation */
  unsigned int locklevel;	/* Current lock level */
  char *uservalue;		/* User maintained data */
};


#if FIT_ANSI

/* Create a lock */
struct lkmg_s_lock *lkmg_create(char *name, unsigned int opts, int userfunc(),
  void *userargs);

/* Delete a lock */
int lkmg_delete(struct lkmg_s_lock *lock, int userfunc(), 
  void *userargs);

/* Dequeue a lock request */
int lkmg_dequeue(struct lkmg_s_lock *lock, unsigned int rqstopt, 
  int userfunc(), void *userargs);

/* Enqueue a lock request */
int lkmg_enqueue(struct lkmg_s_lock *lock, unsigned int rqstopt, int timeout,
  int userfunc(), void *userargs);

#else

struct lkmg_s_lock *lkmg_create();
int lkmg_delete();
int lkmg_dequeue();
int lkmg_enqueue();

#endif
/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : lkmg.h

Function : Defines functions for manipulating locks.

Author : A. Fregly

Abstract : This include files defines a library of functions for use
	in lock management.

Calling Sequence : #include <lkmg.h>

Notes: 
	stdio.h and fitsydef.h need to be included before this file.

Change log : 
000	09-JUN-91  AMF	Created.
******************************************************************************/

/* Bit definitions for lock options/types */
#define LKMG_BIT_FILE		0
#define LKMG_BIT_NET		1
#define LKMG_BIT_MEMORY		2
#define LKMG_BIT_RESERVED3	3
#define LKMG_BIT_RESERVED4	4
#define LKMG_BIT_RESERVED5	5
#define LKMG_BIT_RESERVED6	6
#define LKMG_BIT_RESERVED7	7
#define LKMG_BIT_RESERVED8	8
#define LKMG_BIT_RESERVED9	9
#define LKMG_BIT_USER1		0xA
#define LKMG_BIT_USER2		0xB
#define LKMG_BIT_USER3		0xC
#define LKMG_BIT_USER4		0xD
#define LKMG_BIT_USER5		0xE
#define LKMG_BIT_USER6		0xF

/* Lock types as bit masks */
#define LKMG_MASK_FILE		(1 << 0)
#define LKMG_MASK_NET		(1 << 1)
#define LKMG_MASK_MEMORY	(1 << 2)
#define LKMG_MASK_RESERVED3	(1 << 3)
#define LKMG_MASK_RESERVED4	(1 << 4)
#define LKMG_MASK_RESERVED5	(1 << 5)
#define LKMG_MASK_RESERVED6	(1 << 6)
#define LKMG_MASK_RESERVED7	(1 << 7)
#define LKMG_MASK_RESERVED8	(1 << 8)
#define LKMG_MASK_RESERVED9	(1 << 9)
#define LKMG_MASK_USER1		(1 << 0xA)
#define LKMG_MASK_USER2		(1 << 0xB)
#define LKMG_MASK_USER3		(1 << 0xC)
#define LKMG_MASK_USER4		(1 << 0xD)
#define LKMG_MASK_USER5		(1 << 0xE)
#define LKMG_MASK_USER6		(1 << 0xF)


/* Define lock types, for now only EXCLUSIVE locks are supported */
#define LKMG_EXCLUSIVE		1
#define LKMG_WRITE		2
#define LKMG_READ		3
#define LKMG_SHAREDREAD		4
#define LKMG_NULL		5


/* Define the lock structure */
struct lkmg_s_lock {
  char *lockname;		/* Name of lock */
  unsigned int opts;		/* Options supplied during creation */
  unsigned int locklevel;	/* Current lock level */
  char *uservalue;		/* User maintained data */
};


#if FIT_ANSI

/* Create a lock */
struct lkmg_s_lock *lkmg_create(char *name, unsigned int opts, int userfunc(),
  void *userargs);

/* Delete a lock */
int lkmg_delete(struct lkmg_s_lock *lock, int userfunc(), 
  void *userargs);

/* Dequeue a lock request */
int lkmg_dequeue(struct lkmg_s_lock *lock, unsigned int rqstopt, 
  int userfunc(), void *userargs);

/* Enqueue a lock request */
int lkmg_enqueue(struct lkmg_s_lock *lock, unsigned int rqstopt, int timeout,
  int userfunc(), void *userargs);

#else

struct lkmg_s_lock *lkmg_create();
int lkmg_delete();
int lkmg_dequeue();
int lkmg_enqueue();

#endif
