/*******************************************************************************
                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : men.h						men.h

Function : Include file for menu functions.

Author : A. Fregly

Abstract : This include file defines the constants, structures, and functions
	used by the SeekWare Curses based menu function library.

Calling Sequence : #include <men.h>

Notes: 
000	fitsydef.h needs to be included before men.h.
001	men.h will include curses.h.

Change log : 
000	23-AUG-91  AMF	Created.
001	13-MAR-92  AMF	Support virtual menus.
002	17-DEC-92  AMF	Define getyx, getmaxyx, getbegyx if not defined by
			curses.
003	26-MAR-93  AMF	Fix K&R prototype for men_nselected.
004	08-SEP-93  AMF	HPUX compatibility
005	11-SEP-93  AMF	Fix getmaxyx and getbegyx macros. Add MEN_DEBUG.
******************************************************************************/

#if !defined(MEN_INCLUDE)
#define MEN_INCLUDE

#if FIT_CURSES
#if FIT_NCURSES
#include <ncurses.h>
#else
#include <curses.h>
#endif
#endif

#include <drct.h>

#ifndef MEN_DEBUG
#define MEN_DEBUG 0
#endif

/* Define some function control keys */
#define CTRLD '\04' 
#define CR '\r'
#define LF '\n'
#define CTRLW '\27'
#define CTRLZ '\32'

#if FIT_COHERENT || FIT_HP || FIT_LINUX || FIT_UNIX
#define KEY_F1 KEY_F(1)
#define KEY_F2 KEY_F(2)
#define KEY_F3 KEY_F(3)
#define KEY_F4 KEY_F(4)
#define KEY_F5 KEY_F(5)
#define KEY_F6 KEY_F(6)
#define KEY_F7 KEY_F(7)
#define KEY_F8 KEY_F(8)
#define KEY_F9 KEY_F(9)
#define KEY_F10 KEY_F(10)
#define KEY_F11 KEY_F(11)
#define KEY_F12 KEY_F(12)

#if FIT_COHERENT || FIT_HP
#define KEY_SRIGHT (KEY_RIGHT + 1024)
#define KEY_NEXT (KEY_F1 + 1024)
#define KEY_SNEXT (KEY_NEXT + 1024)
#define KEY_SLEFT (KEY_LEFT + 1024)
#define KEY_PREVIOUS (KEY_F1 + 1025)
#define KEY_SPREVIOUS (KEY_PREVIOUS + 1024)
#define KEY_SHOME (KEY_HOME + 1024)
#define KEY_END (KEY_F1 + 1026)
#define KEY_SEND (KEY_END + 1024)
#define KEY_EXIT (KEY_F1 + 1027)
#define KEY_SEXIT (KEY_EXIT + 1024)
#define KEY_CANCEL (KEY_F1 + 1028)
#define KEY_SCANCEL (KEY_CANCEL + 1024)
#define KEY_REFRESH (KEY_F1 + 1029)
#define KEY_CLOSE (KEY_F1 + 1030)
#define KEY_BEG (KEY_F1 + 1031)
#define KEY_SBEG (KEY_BEG + 1024)
#define KEY_FIND (KEY_F1 + 1032)
#define KEY_SFIND (KEY_FIND + 1024)
#define KEY_HELP (KEY_F1 + 1033)
#define KEY_SHELP (KEY_HELP + 1024)
#define KEY_SDC (KEY_DC + 1024)

#define ACS_RARROW 0xAE
#define ACS_DIAMOND 0xEC
#define ACS_LTEE 0xC3
#define ACS_HLINE 0xC4
#define ACS_RTEE 0xB4
#endif
#endif

#if FIT_TERMCAP
#define KEY_DOWN 14
#define KEY_UP 21
#define KEY_RIGHT 12
#define KEY_LEFT 10
#define KEY_NEXT 9
#define KEY_PREVIOUS 22
#define KEY_HOME 2
#define KEY_END 5
#define KEY_NPAGE 6
#define KEY_PPAGE 16
#define KEY_ENTER '\n'
#define KEY_CANCEL 4
#define KEY_CLOSE 3
#define KEY_EXIT 26
#define KEY_DC 27
#define KEY_REFRESH 23
#define KEY_BACKSPACE 8
#define KEY_IC 1
#define KEY_EIC 1
#define KEY_EOL 5
#define KEY_HELP 11
#endif

/* Menu has horizontal attribute (multi-column) */
#define MEN_ATTR_HORIZONTAL 1

/* Specifies that current selection shoud wrap when a horizontal movement 
   would move the selector out of the vertical edges of the menu. If this 
   option is not exercised, the menu is exited when this occurs */
#define MEN_ATTR_HORIZONTALWRAP 2

/* Box should be drawn around the menu */
#define MEN_ATTR_BOXED 4

/* Multiple options can be selected from menu */
#define MEN_ATTR_MULTSEL 8

/* Selected options have a check mark displayed just left of first char */
#define MEN_ATTR_CHECK 0x10

/* Hot key selection is enabled for menu options. Hot keys are determined
   by scanning the option list, allocating hot keys as the first letter
   of the option, or if a conflict with other options, then the next 
   unique letter in the option, with precedence for a given letter based
   on the option position in the option list. If no unique letter can
   be determined for the option, no hot key will be defined for the option. */
#define MEN_ATTR_HOTKEYS 0x20

/* Enables exit from the menu when a hot key is used to select an option */
#define MEN_ATTR_INSTANT 0x40

/* Specifies that only the hot key for each option is to be highlighted */
#define MEN_ATTR_HIGHHOT 0x80

/* Specifies that left column of menu is to be left justified */
#define MEN_ATTR_LEFTJUST 0x100

/* Specifies that only the first word of a multi-word option should be 
   highlighted. */
#define MEN_ATTR_HIGHFIRST 0x200

/* Specifies that a pointer is to be used to point at an option rather
   than highlighting */
#define MEN_ATTR_POINTER 0x400

/* Specifies that current selection shoud wrap when a vertical movement 
   would move the selector out of the horizontal edges of the menu. If this 
   option is not exercised, the menu is exited when this occurs */
#define MEN_ATTR_VERTICALWRAP 0x800

/* Specifies that a menu is a virtual menu. Virtual menus are used for
   manipulating lists which may change in size, and which are stored in
   a temporary file on the file system */
#define MEN_ATTR_VIRTUAL 0x1000

/* Specifies that highligh map should be used in determining portions of
   option which should be highlighted during display. */
#define MEN_ATTR_USEHIGHMAP 0x2000

/* Specifies that a menu item is viewable, but not available for selection */
#define MENOPT_ATTR_DIM 1

/* Specifies that a menu item is not to be placed on the screen */
#define MENOPT_ATTR_UNAVAIL 2

struct men_s_optdescr {
  int stat;
  int hot;
  int dispstat;
  int row;
  int nrows;
  int col;
  int len;
  int datalen;
};

/* Option descriptor structure */
struct men_s_option {
  struct men_s_optdescr descr;
  char *val;
  unsigned char *highmap;
#if FIT_ANSI
  void *data;
#else
  char *data;
#endif
};

struct men_s_menu {
  int menutype;				/* Menu attribute mask */	
  char *label;				/* Menu label */
  struct men_s_option *options;		/* Option descriptors */
  WINDOW *win;				/* Window in which menu appears */	
  WINDOW *optwin;			/* Sub-window for options */
  WINDOW *savewin;			/* Window for saving overwritten data */
  int normal;				/* Normal display attributes */
  int curhighlight;			/* Current option display attributes */
  int selhighlight;			/* Selected option display attributes */
  int hothighlight;			/* Hot key display attributes */
  int checkmark;			/* Checkmark character */
  int curpointer;			/* Current option pointer */
  long curopt;				/* Current option */
  long firstopt;			/* Option number at top left */
  long nopts;				/* Number of options in menu */
  int scol;				/* Starting column for options */
  int nrows;				/* Number of option rows in window */
  int rowsep;				/* Number of blank rows between rows */
  int ncols;				/* Number of option columns in window */
  int maxoptlen;			/* Maximum option length */
  int colwidth;				/* Column width */
  char *workbuf;			/* Work buffer */
  int nvopt;				/* Number of options which can be placed
					   in options */
  long fvopt;				/* Index of first option currently in 
					   options */
  long lvopt;				/* Index of last option currently in
					   options */
  struct drct_s_file *vfile;		/* Work file handle for virtual menus */
};

#if FIT_ANSI

/* Add option to menu */
int men_addopt(struct men_s_menu *menu, char *optval, int datalen, void *data);

/* Copy a menu */
struct men_s_menu *men_copy(struct men_s_menu *menu, int copyopt);

/* Deallocates storage used by menu */
int men_dealloc(struct men_s_menu *(*menu));

/* Performs menu movement or option selection based on input key */
int men_dokey(struct men_s_menu *menu, int key, int *optselflag);

/* Draws/Redraws menu in menu window */
int men_draw(struct men_s_menu *menu);

/* Get option from virtual work file and put into virtual buffer */
int men_getvopt(struct men_s_menu *menu, long idx);

/* Initialize a new menu */
struct men_s_menu *men_init(WINDOW *win, int menutype, char *label, 
  char *opts[], int attrnorm, int attrcurhigh, int attrselhigh, int attrhot, 
  int curpointer, int checkmark, int minsep, int itemwidth);

/* Performs menu movement/selection function base on key value */
int men_move(struct men_s_menu *menu, int inkey, long *retopt,
  int *optselflag);

/* Makes current option undefined, unhighlighting "current" option */
int men_nocurrent(struct men_s_menu *menu);

/* Return number of items currently selected in menu */
long men_nselected(struct men_s_menu *menu);

/* Returns option structure for specified menu element */
struct men_s_option *men_opt(struct men_s_menu *menu, long idx);

/* Marks a specified option as selected */
int men_optsel(struct men_s_menu *menu, long optnum);

/* Marks a specified option as unselected */
int men_optunsel(struct men_s_menu *menu, long optnum);

/* Overlays a menu on top of or underneath another menu */
int men_overlay(struct men_s_menu *menu, struct men_s_menu *parent, 
  long optnum, int vertical);

/* Draws menu on screen */
int men_put(struct men_s_menu *menu, WINDOW *parent);

/* Put option into menu window */
int men_putopt(struct men_s_menu *menu, long optidx);

/* Updates displayed menu */
int men_refresh(struct men_s_menu *menu);

/* Removes menu from screen */
int men_remove(struct men_s_menu *menu, WINDOW *parent);

/* Set option value in memory buffer only */
int men_repvopt(struct men_s_menu *menu, int idx, struct men_s_option *opt);

/* Reset menu to init stat */
int men_reset(struct men_s_menu *menu);

/* Allows user to select from menu, returning when the user exits the menu */
int men_select(struct men_s_menu *menu, int timeout, long *curopt, 
  int *optselflag, int *timedout);

/* Set option value */
int men_setopt(struct men_s_menu *menu, long idx, struct men_s_option *opt);

/* Set which virtual options are in memory */
int men_setvopt(struct men_s_menu *menu, long sidx);

/* Handle timeout on menu selection */
void men_timeralarm(int sig);

/* Update virtual option */
int men_update(struct men_s_menu *menu, long idx);

#else

int men_addopt();
struct men_s_menu *men_copy();
int men_dealloc();
int men_dokey();
int men_draw();
int men_getvopt();
struct men_s_menu *men_init();
int men_move();
int men_nocurrent();
long men_nselected();
struct men_s_option *men_opt();
int men_optsel();
int men_optunsel();
int men_overlay();
int men_put();
int men_putopt();
int men_refresh();
int men_remove();
int men_repvopt();
int men_reset();
int men_select();
int men_setopt();
int men_setvopt();
void men_timeralarm();
int men_update();

#endif

#ifndef getmaxyx
#define getmaxyx(win, y, x) ((y = (unsigned) win->_maxy + 1) | \
  (x = (unsigned) win->_maxx + 1) | 1)
#endif

#ifndef getyx
#define getyx(win, y, x) ((y = (unsigned) win->_cury) | \
  (x = (unsigned) win->curx) | 1)
#endif

#ifndef getbegyx
#define getbegyx(win, y, x) ((y = (unsigned) win->_begy) | \
  (x = (unsigned) win->_begx) | 1)
#endif

#if !FIT_COHERENT
#define MEN_BOX(win) box(win, 0, 0)
#else
#define MEN_BOX(win) win_box(win)
#endif

WINDOW *men_win_debug;

#endif
