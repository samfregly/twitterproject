/*******************************************************************************

                       Copyright 1997, SeekWare Inc.

    All rights reserved.  The following source code, or any part there-of,
    may not be duplicated, modified, or resold without the express written
    consent of SeekWare Inc.

    SeekWare Inc. makes no claims as to the correctness or applicability of
    this source code.  The source code is supplied as is.  SeekWare Inc.
    is not responsible for damages, consequential or otherwise, caused by the
    use of this source code.

*******************************************************************************/

/******************************************************************************
Module Name : prox.h

Function : Defines structures and functions used in proximity resolution.

Author : A. Fregly

Abstract : 

Calling Sequence : #include <prox.h>

Notes : 

Change log : 
000	18-APR-91  AMF	Created by extracting from prf_search.
001	08-MAY-91  AMF	Changed freeprox "exe" parameter to "nproximity".
002	15-NOV-97  AMF	Modified to work better with C++
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PROX_INC
#define PROX_INC 1

/* List element used for describing a proximity term match */
typedef struct prf_s_proxmatch {
  long swordnum;
  long ewordnum;
  prf_s_match *match;
  struct prf_s_proxmatch *next;
} prf_s_proxmatch;

/* Structure defining two match lists for proximity preceeding and following
   terms */
typedef struct prf_s_proxmatchlist {
  long n_1;
  long n_2;
  struct prf_s_proxmatch *first_1;
  struct prf_s_proxmatch *last_1;
  struct prf_s_proxmatch *first_2;
  struct prf_s_proxmatch *last_2;
} prf_s_proxmatchlist;

#if FIT_ANSI
/* Frees proximity relationships */
void prf_search_freeprox(register prf_s_proxmatchlist *proxmatchlist,
    int nproximity);

/* Adds proximity match to proximity match list */
void prf_search_addprox(register prf_s_proxmatchlist *proxmatchlist,
  long swordnum, long ewordnum, int preceeding);

/* Determines which proximity terms have matched. */
void prf_search_resolveprox(prf_s_exe *exe, int idx,
  register prf_s_proxmatchlist *matchlist, char *idmatches);

/* Adds proximity match to proximity match list */
void prf_matchinfo_addprox(register prf_s_proxmatchlist *proxmatchlist,
  long swordnum, long ewordnum, int preceeding, prf_s_match *match);

/* Determines which proximity terms have matched. */
void prf_matchinfo_resolveprox(prf_s_exe *exe, int idx,
  register prf_s_proxmatchlist *matchlist, prf_s_matchinfo *matchinfo);
#else
void prf_search_freeprox();
void prf_search_addprox();
void prf_search_resolveprox();
void prf_matchinfo_addprox();
void prf_matchinfo_resolveprox();
#endif

#endif

#ifdef __cplusplus
};
#endif